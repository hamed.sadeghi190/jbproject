﻿using System.Collections.ObjectModel;
using iTextSharp.text;
using Jb.Framework.Common.Forms;
using Kendo.Mvc.Infrastructure;
using SportsClub.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using SportsClub.Infrastructure;
using SportsClub.Infrastructure.UIHelper;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SportsClub.Infrastructure.JBMembership;
using System.ComponentModel;
using SportsClub.Infrastructure.Helper;
using SportsClub.Models.FormModels;
using System.Xml.Linq;
using Utilities = SportsClub.Infrastructure.Utilities;
using Jb.Framework.Web.Model;

namespace SportsClub.Models
{
    #region Display Orders
    public class DisplayOrdersViewModel
    {
        public string ClubDomain { get; set; }

        public string SeasonDomain { get; set; }

        public string Domain { get; set; }

        public bool CanCancel { get; set; }

        public bool CanEdit { get; set; }

        public int TotalRecord { get; set; }

        [Display(Name = "Event Name:")]
        public string EventName { get; set; }

        public Domain.TimeZone? TimeZone { get; set; }

        public SearchViewModelBase Search { get; set; }

        public ViewOrderMode Status { get; set; }

        public List<OrderItemViewModelBase> OrderItems { get; set; }

        public DisplayOrdersViewModel()
        {

        }

        public DisplayOrdersViewModel(string clubDomain)
        {
            if (string.IsNullOrEmpty(clubDomain))
            {
                ClubDomain = JBMembership.GetActiveClubBaseInfo().Domain;
            }
            else
            {
                ClubDomain = clubDomain;
            }

            var club = Ioc.IClubDbService.GetClub(ClubDomain, false);

            // load club config
            if (string.IsNullOrEmpty(club.Configuration) == false)
            {
                ClubConfigurationModel config = new ClubConfigurationModel().DeserializeConfig(System.Xml.Linq.XDocument.Parse(club.Configuration), ClubDomain);
                CanCancel = config.CanAdminCancelOrder;
                CanEdit = config.CanAdminEditOrder;
            }
        }

        public DisplayOrdersViewModel(string domain, string clubDomain)
        // : this()
        {
            ClubDomain = clubDomain;
            Domain = domain;

            var club = Ioc.IClubDbService.GetClub(clubDomain, false);

            // load club config
            if (string.IsNullOrEmpty(club.Configuration) == false)
            {
                ClubConfigurationModel config = new ClubConfigurationModel().DeserializeConfig(System.Xml.Linq.XDocument.Parse(club.Configuration), ClubDomain);
                CanCancel = config.CanAdminCancelOrder;
                CanEdit = config.CanAdminEditOrder;
            }

            //Get club time zone
            TimeZone = club.TimeZone;

            OrderItems = new List<OrderItemViewModelBase>();
        }

        public DisplayOrdersViewModel(SearchViewModelBase search)
            : this(search.Domain, search.ClubDomain)
        {
            Search = search;
        }
    }

    public class OrderItemViewModelBase
    {
        public int OrderId { get; set; }

        public string RegisteredUser { get; set; }

        public int? ProfileId { get; set; }

        public string Date { get; set; }

        public decimal Amount { get; set; }

        public string EventName { get; set; }

        public SubDomainCategories EventType { get; set; }

        public string Description { get; set; }

        public string ConfirmationNumber { get; set; }

        public string Discounts { get; set; }

        public PaymentStatusCategories PaymentStatus { get; set; }

        public PaymentMethod PaymentMethod { get; set; }

        public PaymentPlanType PaymentPlanType { get; set; }

        public int CategoryId { get; set; }

        public string PaymentStatusDescription { get; set; }

        public OrderItemViewModelBase()
        {
        }
    }

    public class SearchViewModelBase
    {
        [Display(Name = "Payment Status:")]
        public SearchPaymentStaus PaymentStatus { get; set; }

        [Display(Name = "Player Name:")]
        public string PlayerName { get; set; }

        [Display(Name = "Date:")]
        [DataType(DataType.DateTime, ErrorMessage = "The type of date is incorrect")]
        public Nullable<DateTime> StartDate { get; set; }

        [DataType(DataType.DateTime, ErrorMessage = "The type of date is incorrect")]
        public Nullable<DateTime> EndDate { get; set; }

        [Display(Name = "Order mode:")]
        public OrderMode? OrderMode { get; set; }

        [Display(Name = "Confirmation Id:")]
        public string ConfirmationNumber { get; set; }

        public string ClubDomain { get; set; }
        public string Domain { get; set; }

        public SearchViewModelBase()
        {

        }

        public SearchViewModelBase(string domain, string clubDomain)
        {
            ClubDomain = clubDomain;
            Domain = domain;
        }
    }

    public class EventOrderViewModel
    {

        public int Id { get; set; }

        public string EventName { get; set; }

        public int CategoryId { get; set; }

        public string ClubDomain { get; set; }

        public string Domain { get; set; }

        public string EventType { get; set; }

        public string TotalAmount { get; set; }

        public string TotalNumber { get; set; }

    }

    public class DisplayAllOrdersViewModel : DisplayOrdersViewModel
    {
        public new List<AllOrderItemViewModel> OrderItems { get; set; }

        public new AllOrderSearchViewModel Search { get; set; }

        private void SeedItems(string clubDomain, string confirmationId, string playerName, DateTime? startDate, DateTime? endDate, EventType eventType, int paymentStatus, OrderMode? orderMode, int pageSize, int currentPage)
        {
            int total = 0;
            IEnumerable<ClubPaymentItem> items = Ioc.IOrderDbService.ClubPayments(clubDomain, confirmationId, playerName, startDate, endDate, eventType, paymentStatus, orderMode, pageSize, currentPage, ref total);
            TotalRecord = total;
            OrderItems = items.Select(i => new AllOrderItemViewModel
            {
                ClubDomain = i.ClubDomain,
                EventName = i.EventName,
                Domain = i.Domain,
                EventType = i.EventType,
                RegisteredUser = string.Format("{0} {1}", i.FirstName, i.LastName),
                Amount = i.TotalPrice, //Utilities.FormatCurrencyWithoutPenny(i.TotalPrice),
                Description = i.Description,
                ProfileId = i.ProfileId,
                Date = Utilities.ShortFormatDateYearMonthDayWithLocalTime(i.Date, TimeZone),
                ConfirmationNumber = i.ConfirmationId,
                OrderId = i.OrderId,
                PaymentStatus = i.PaymentStatus,
                PaymentMethod = i.PaymentMethod,
                CategoryId = i.CategoryId,
                PaymentStatusDescription = string.Format("{0} ({1})", i.PaymentStatus.ToDescription(), i.PaymentMethod.ToDescription()),
                PaymentPlanType = i.PaymentPlanType
            })
            .ToList();
        }

        private void SeedItems(string clubDomain, string seasonDomain, string confirmationId, string playerName, DateTime? startDate, DateTime? endDate, EventType eventType, int paymentStatus, OrderMode? orderMode, int pageSize, int currentPage, string sortField, ListSortDirection sortDirection)
        {
            int total = 0;
            IEnumerable<ClubPaymentItem> items = Ioc.IOrderDbService.ClubPayments(clubDomain, seasonDomain, confirmationId, playerName, startDate, endDate, eventType, paymentStatus, orderMode, pageSize, currentPage, sortField, sortDirection, ref total);
            TotalRecord = total;
            OrderItems = items.Select(i => new AllOrderItemViewModel
            {
                ClubDomain = i.ClubDomain,
                EventName = i.EventName,
                Domain = i.Domain,
                EventType = i.EventType,
                RegisteredUser = string.Format("{0} {1}", i.FirstName, i.LastName),
                Amount = i.TotalPrice,//Utilities.FormatCurrencyWithoutPenny(i.TotalPrice),
                Description = i.Description,
                ProfileId = i.ProfileId,
                Date = Utilities.ShortFormatDateYearMonthDayWithLocalTime(i.Date, TimeZone),
                ConfirmationNumber = i.ConfirmationId,
                OrderId = i.OrderId,
                PaymentStatus = i.PaymentStatus,
                PaymentMethod = i.PaymentMethod,
                CategoryId = i.CategoryId,
                PaymentStatusDescription = string.Format("{0} ({1})", i.PaymentStatus.ToDescription(), i.PaymentMethod.ToDescription()),
                PaymentPlanType = i.PaymentPlanType
            })
            .ToList();
        }

        public DisplayAllOrdersViewModel()
            : base(string.Empty)
        {
            Search = new AllOrderSearchViewModel();
            Status = ViewOrderMode.AllOrders;

            OrderItems = new List<AllOrderItemViewModel>();
            //SeedItems(ClubDomain, null, null, null, null, null, false);
        }

        public DisplayAllOrdersViewModel(AllOrderSearchViewModel search, bool loadItems, int pageSize, int currentPage)
            : base(string.Empty)
        {
            if (search != null)
            {
                Search = search;
            }
            else
            {
                Search = new AllOrderSearchViewModel();
            }
            Status = ViewOrderMode.AllOrders;

            if (loadItems)
            {
                SeedItems(ClubDomain, Search.ConfirmationNumber, Search.PlayerName, Search.StartDate, Search.EndDate, Search.EventType, (int)Search.PaymentStatus, Search.OrderMode, pageSize, currentPage);
            }
        }

        public DisplayAllOrdersViewModel(AllOrderSearchViewModel search, bool loadItems, int pageSize, int currentPage, string sortField, ListSortDirection sortDirection, string seasonDomain)
            : base(string.Empty)
        {
            if (search != null)
            {
                Search = search;
            }
            else
            {
                Search = new AllOrderSearchViewModel();
            }
            Status = ViewOrderMode.AllOrders;

            if (loadItems)
            {
                SeedItems(ClubDomain, seasonDomain, Search.ConfirmationNumber, Search.PlayerName, Search.StartDate, Search.EndDate, Search.EventType, (int)Search.PaymentStatus, Search.OrderMode, pageSize, currentPage, sortField, sortDirection);
            }
        }
    }

    public class AllOrderItemViewModel : OrderItemViewModelBase
    {
        public string ClubDomain { get; set; }

        public string Domain { get; set; }

        public AllOrderItemViewModel()
        {
        }
    }

    public class AllOrderSearchViewModel : SearchViewModelBase
    {
        [Display(Name = "Event Type:")]
        public EventType EventType { get; set; }

        public AllOrderSearchViewModel()
        {
            PaymentStatus = SearchPaymentStaus.All;
            EventType = SportsClub.Domain.EventType.None;
        }
    }

    //public class DisplayTTTourneyOrdersViewModel : DisplayOrdersViewModel
    //{
    //    public new List<TTTourneyOrderItemViewModel> OrderItems { get; set; }

    //    public new TTTourneySearchViewModel Search { get; set; }

    //    void SeedItems(string clubDomain, string domain, DateTime? startDate, DateTime? endDate, string textSearch, SportsClub.Db.OrderDb.TableTennisTourneyPaymentSearchOption searchType, List<string> eventNames, string confirmationId, OrderMode? orderMode, int pageSize, int page)
    //    {
    //        //Return tttourney order items
    //        int total = 0;
    //        IEnumerable<TableTennisTourneyPaymentItem> items = Ioc.IOrderDbService.TableTennisTourneyPayments(clubDomain, domain, startDate, endDate, textSearch, searchType, eventNames, confirmationId, orderMode, pageSize, page, ref total);

    //        //Get total records for paging 
    //        TotalRecord = total;

    //        OrderItems = new List<TTTourneyOrderItemViewModel>();

    //        if (items != null)
    //        {
    //            OrderItems = items.Select(i => new TTTourneyOrderItemViewModel
    //            {
    //                OrderId = i.OrderId,
    //                State = i.State,
    //                Gender = i.Gender,
    //                RegisteredUser = string.Format("{0} {1}", i.FirstName, i.LastName),
    //                EventName = i.Events,
    //                Partner = i.Partner,
    //                ClubAffiliation = i.ClubAffiliation,
    //                ProfileId = i.ProfileId,
    //                CategoryId = CategoryModel.TableTennisId,
    //                //Discounts = Utilities.DisplayCampDiscounts(item.Discounts);
    //                TotalPrice = i.TotalPrice.ToString(),//Utilities.FormatCurrencyWithoutPenny(i.TotalPrice),
    //                Date = Utilities.ShortFormatDateYearMonthDayWithLocalTime(i.Date, TimeZone),
    //                ConfirmationNumber = i.ConfirmationId,
    //                PaymentStatus = i.PaymentStatus,
    //                PaymentMethod = i.PaymentMethod,
    //                PaymentStatusDescription = string.Format("{0} ({1})", i.PaymentStatus.ToDescription(), i.PaymentMethod.ToDescription())
    //            })
    //             .ToList();

    //            //foreach (var item in items)
    //            //{
    //            //    OrderItems.Add(new TTTourneyOrderItemViewModel(item));
    //            //}
    //        }
    //    }

    //    public DisplayTTTourneyOrdersViewModel()
    //        : base()
    //    {
    //        OrderItems = new List<TTTourneyOrderItemViewModel>();

    //        Status = ViewOrderMode.TTTourney;

    //        Search = new TTTourneySearchViewModel();

    //    }

    //    public DisplayTTTourneyOrdersViewModel(string domain, string clubDomain)
    //        : base(domain, clubDomain)
    //    {
    //        Search = new TTTourneySearchViewModel();
    //        Search.Domain = Domain;

    //        Status = ViewOrderMode.TTTourney;

    //        //SeedItems(ClubDomain, Domain, null, null, string.Empty, 0, null, null, false);
    //    }

    //    public DisplayTTTourneyOrdersViewModel(TTTourneySearchViewModel search, bool loadItems, int pageSize, int page)
    //        : base(search)
    //    {
    //        Status = ViewOrderMode.TTTourney;

    //        Search = new TTTourneySearchViewModel(Domain, ClubDomain, search.EventNames);
    //        Search.SearchText = search.SearchText;
    //        Search.ConfirmationNumber = search.ConfirmationNumber;

    //        if (loadItems)
    //        {
    //            SeedItems(ClubDomain, Domain, search.StartDate, search.EndDate, search.SearchText, SportsClub.Db.OrderDb.TableTennisTourneyPaymentSearchOption.Name, search.EventNames, search.ConfirmationNumber, search.OrderMode, pageSize, page);
    //        }
    //    }
    //}

    //public class TTTourneyOrderItemViewModel : OrderItemViewModelBase
    //{
    //    public bool IsChecked { get; set; }

    //    public new string Discounts { get; set; }

    //    public string Partner { get; set; }

    //    public string ClubAffiliation { get; set; }

    //    public string TotalPrice { get; set; }

    //    public new int ProfileId { get; set; }

    //    public string State { get; set; }

    //    public GenderCategories Gender { get; set; }

    //    public TTTourneyOrderItemViewModel()
    //    {
    //    }

    //    public TTTourneyOrderItemViewModel(TableTennisTourneyPaymentItem item)
    //        : this()
    //    {

    //    }
    //}

    //public class TTTourneySearchViewModel : SearchViewModelBase
    //{
    //    [Display(Name = "Player Name")]
    //    public string SearchText { get; set; }

    //    [Display(Name = "Events")]
    //    public List<SelectListItem> Events { get; set; }

    //    public List<string> EventNames { get; set; }

    //    public TTTourneySearchViewModel()
    //    {
    //        Events = new List<SelectListItem>();
    //    }

    //    public TTTourneySearchViewModel(string domain, string clubDomain, List<string> eventNames)
    //        : base(domain, clubDomain)
    //    {
    //        EventNames = eventNames;
    //        SeedEvents();
    //    }

    //    void SeedEvents()
    //    {
    //        Events = new List<SelectListItem>();

    //        //Prevent null exception for EventIds
    //        if (EventNames == null)
    //        {
    //            EventNames = new List<string>();
    //        }

    //        var _ttTourneyEvent = Ioc.ITTTourneyDbService.GetEventsWithoutRoundType(ClubDomain, Domain).ToList();

    //        //Add event names to Events Property
    //        foreach (var item in _ttTourneyEvent)
    //        {
    //            Events.Add(new SelectListItem { Value = item.EventName, Text = item.EventName, Selected = (EventNames.Any(s => s.Equals(item.EventName))) });
    //        }
    //    }
    //}

    //public class DisplayDefaultTourneyOrdersViewModel : DisplayOrdersViewModel
    //{
    //    public new List<DefaultTourneyOrderItemViewModel> OrderItems { get; set; }

    //    public new DefaultTourneySearchViewModel Search { get; set; }

    //    private void SeedItems(string clubDomain, string domain, string confirmationId, string playerName, DateTime? startDate, DateTime? endDate, OrderMode? orderMode, int pageSize, int page)
    //    {
    //        int total = 0;
    //        IEnumerable<TourneyPaymentItem> items = Ioc.IOrderDbService.TourneyPayments(clubDomain, domain, confirmationId, playerName, startDate, endDate, orderMode, pageSize, page, ref total);
    //        //Get total records for paging 
    //        TotalRecord = total;

    //        OrderItems = items.Select(i => new DefaultTourneyOrderItemViewModel
    //        {
    //            OrderId = i.OrderId,
    //            ConfirmationNumber = i.ConfirmationId,
    //            ProfileId = i.ProfileId,
    //            RegisteredUser = string.Format("{0} {1}", i.FirstName, i.LastName),
    //            Date = Utilities.ShortFormatDateYearMonthDayWithLocalTime(i.Date, TimeZone),
    //            Section = i.Section,
    //            Schedule = i.Schedule,
    //            Byes = i.Byes,
    //            Options = i.Options,
    //            Amount = i.TotalPrice,//Utilities.FormatCurrencyWithoutPenny(i.TotalPrice),
    //            PaymentStatus = i.PaymentStatus,
    //            PaymentMethod = i.PaymentMethod,
    //            PaymentStatusDescription = string.Format("{0} ({1})", i.PaymentStatus.ToDescription(), i.PaymentMethod.ToDescription()),
    //            CategoryId = i.CategoryId,
    //        })
    //        .ToList();
    //    }

    //    public DisplayDefaultTourneyOrdersViewModel()
    //        : base()
    //    {
    //        Status = ViewOrderMode.ChessTourney;

    //        Search = new DefaultTourneySearchViewModel();
    //    }

    //    public DisplayDefaultTourneyOrdersViewModel(DefaultTourneySearchViewModel search, bool loadItems, int pageSize, int page)
    //        : base(search)
    //    {
    //        Status = ViewOrderMode.ChessTourney;

    //        Search = search;

    //        if (loadItems)
    //        {
    //            SeedItems(ClubDomain, Domain, search.ConfirmationNumber, search.PlayerName, search.StartDate, search.EndDate, search.OrderMode, pageSize, page);
    //        }
    //    }
    //}

    //public class DefaultTourneyOrderItemViewModel : OrderItemViewModelBase
    //{
    //    public string Section { get; set; }

    //    public string Schedule { get; set; }

    //    public string Byes { get; set; }

    //    public string Options { get; set; }
    //}

    //public class DefaultTourneySearchViewModel : SearchViewModelBase
    //{
    //    public DefaultTourneySearchViewModel()
    //    {
    //    }

    //    public DefaultTourneySearchViewModel(string domain, string clubDomain)
    //    {
    //        ClubDomain = clubDomain;
    //        Domain = domain;
    //    }
    //}

    public class DisplayDonationOrdersViewModel : DisplayOrdersViewModel
    {

        public new List<DonationOrderItemViewModel> OrderItems { get; set; }

        public new DonationSearchViewModel Search { get; set; }

        public DisplayDonationOrdersViewModel()
            : base()
        {
            Status = ViewOrderMode.Donation;
            Search = new DonationSearchViewModel();
        }

        public DisplayDonationOrdersViewModel(DonationSearchViewModel search, bool loadOtems, int pageSize, int page)
            : base(search)
        {
            Status = ViewOrderMode.Donation;
            Search = search;

            if (loadOtems)
            {
                int total = 0;
                IEnumerable<DonationPaymentItem> items = Ioc.IOrderDbService.DonationPayments(ClubDomain, Domain, Search.PlayerName, Search.ConfirmationNumber, Search.StartDate, Search.EndDate, Search.OrderMode, pageSize, page, ref total);

                //Get total records for paging 
                TotalRecord = total;

                OrderItems = items.Select(i => new DonationOrderItemViewModel
                {
                    OrderId = i.OrderId,
                    ConfirmationNumber = i.ConfirmationId,
                    RegisteredUser = string.Format("{0} {1}", i.FirstName, i.LastName),
                    Date = Utilities.ShortFormatDateYearMonthDayWithLocalTime(i.Date, TimeZone),
                    Amount = i.TotalPrice,//Utilities.FormatCurrencyWithoutPenny(i.TotalPrice),
                    PaymentStatus = i.PaymentStatus,
                    PaymentMethod = i.PaymentMethod,
                    PaymentPlanType = i.PaymentPlanType,
                    PaymentStatusDescription = string.Format("{0} ({1})", i.PaymentStatus.ToDescription(), i.PaymentMethod.ToDescription())
                })
                .ToList();
            }
        }
    }

    public class DisplayCourseOrdersViewModel : DisplayOrdersViewModel
    {
        public new List<CourseOrderItemViewModel> OrderItems { get; set; }

        public new CourseSearchViewModel Search { get; set; }

        public DisplayCourseOrdersViewModel()
            : base()
        {
            Status = ViewOrderMode.Course;
            Search = new CourseSearchViewModel();
        }

        public DisplayCourseOrdersViewModel(CourseSearchViewModel search, bool loadItems, int pageSize, int page)
            : base(search)
        {
            Status = ViewOrderMode.Course;

            Search = search;

            OrderItems = new List<CourseOrderItemViewModel>();

            if (loadItems)
            {
                int total = 0;
                IEnumerable<CoursePaymentItem> items = new List<CoursePaymentItem>();// Ioc.IOrderDbService.CoursePayments(ClubDomain, Domain, Search.PlayerName, Search.ConfirmationNumber, Search.StartDate, Search.EndDate, Search.OrderMode, pageSize, page, ref total);

                //Get total records for paging 
                TotalRecord = total;

                OrderItems = items.Select(i => new CourseOrderItemViewModel
                {
                    OrderId = i.OrderId,
                    ConfirmationNumber = i.ConfirmationId,
                    ProfileId = i.ProfileId,
                    RegisteredUser = string.Format("{0} {1}", i.FirstName, i.LastName),
                    Date = Utilities.ShortFormatDateYearMonthDayWithLocalTime(i.Date, TimeZone),
                    Discounts = Utilities.DisplayCampDiscounts(i.Discounts),
                    RegistrationType = i.RegistrationType,
                    Amount = i.TotalPrice,//Utilities.FormatCurrencyWithoutPenny(i.TotalPrice),
                    CategoryId = i.CategoryId,
                    PaymentStatus = i.PaymentStatus,
                    PaymentMethod = i.PaymentMethod,
                    PaymentPlanType = i.PaymentPlanType,
                    PaymentStatusDescription = string.Format("{0} ({1})", i.PaymentStatus.ToDescription(), i.PaymentMethod.ToDescription())
                })
                .ToList();
            }
        }
    }

    public class CourseOrderItemViewModel : OrderItemViewModelBase
    {
        public RegistrationType RegistrationType { get; set; }

        public CourseOrderItemViewModel()
            : base()
        {

        }
    }

    public class CourseSearchViewModel : SearchViewModelBase
    {
        public CourseSearchViewModel()
        {
        }

        public CourseSearchViewModel(string domain, string clubDomain)
        {
            ClubDomain = clubDomain;
            Domain = domain;
        }
    }

    //public class DisplayCampOrdersViewModel : DisplayOrdersViewModel
    //{
    //    public new List<CampOrderItemViewModel> OrderItems { get; set; }

    //    public new CampSearchViewModel Search { get; set; }

    //    public DisplayCampOrdersViewModel()
    //    {
    //        Status = ViewOrderMode.Camp;
    //        Search = new CampSearchViewModel();
    //    }

    //    //public DisplayCampOrdersViewModel(CampSearchViewModel search, bool loadItems, int pageSize, int page)
    //    //    : base(search)
    //    //{
    //    //    Status = ViewOrderMode.Camp;

    //    //    Search = new CampSearchViewModel(search.Domain, search.ClubDomain, search.ScheduleId, search.SectionId);
    //    //    Search.PlayerName = search.PlayerName;
    //    //    Search.ConfirmationNumber = search.ConfirmationNumber;

    //    //    OrderItems = new List<CampOrderItemViewModel>();

    //    //    if (loadItems)
    //    //    {
    //    //        int total = 0;
    //    //        IEnumerable<CampPaymentItem> items = Ioc.IOrderDbService.CampPayments(ClubDomain, Domain, search.ScheduleId, search.SectionId, search.PlayerName, search.ConfirmationNumber, search.StartDate, search.EndDate, search.OrderMode, pageSize, page, ref total);

    //    //        //Get total records for paging 
    //    //        TotalRecord = total;

    //    //        OrderItems = items.Select(i => new CampOrderItemViewModel
    //    //        {
    //    //            OrderId = i.OrderId,
    //    //            ConfirmationNumber = i.ConfirmationId,
    //    //            ProfileId = i.ProfileId,
    //    //            RegisteredUser = string.Format("{0} {1}", i.FirstName, i.LastName),
    //    //            Date = Utilities.ShortFormatDateYearMonthDayWithLocalTime(i.Date, TimeZone),
    //    //            Discounts = Utilities.DisplayCampDiscounts(i.Discounts),
    //    //            SectionName = i.SectionName,
    //    //            Schedule = Utilities.DisplayScheduleDates(i.StartDate, i.EndDate),
    //    //            Amount = i.TotalPrice,//Utilities.FormatCurrencyWithoutPenny(i.TotalPrice),
    //    //            PaymentStatus = i.PaymentStatus,
    //    //            PaymentMethod = i.PaymentMethod,
    //    //            PaymentPlanType = i.PaymentPlanType,
    //    //            CategoryId = i.CategoryId,
    //    //            PaymentStatusDescription = string.Format("{0} ({1})", i.PaymentStatus.ToDescription(), i.PaymentMethod.ToDescription())
    //    //        })
    //    //        .ToList();
    //    //    }
    //    //}
    //}

    public class DisplayCalendarOrderViewModel : DisplayOrdersViewModel
    {
        public new List<CalendarOrderItemViewModel> OrderItems { get; set; }

        public new CalendarSearchViewModel Search { get; set; }

        public DisplayCalendarOrderViewModel()
            : base()
        {
            Status = ViewOrderMode.Calendar;
            Search = new CalendarSearchViewModel();
        }

        public DisplayCalendarOrderViewModel(CalendarSearchViewModel search, bool loadItems, int pageSize, int page)
            : base(search)
        {
            Status = ViewOrderMode.Calendar;

            Search = search;

            OrderItems = new List<CalendarOrderItemViewModel>();

            if (loadItems)
            {
                int total = 0;
                IEnumerable<EventPaymentItem> items = Ioc.IOrderDbService.EventPayments(ClubDomain, Domain, search.ConfirmationNumber, search.PlayerName, search.StartDate, search.EndDate, search.OrderMode, pageSize, page, ref total);

                //Get total records for paging 
                TotalRecord = total;

                OrderItems = items.Select(i => new CalendarOrderItemViewModel
                {
                    OrderId = i.OrderId,
                    ConfirmationNumber = i.ConfirmationId,
                    ProfileId = i.ProfileId,
                    RegisteredUser = string.Format("{0} {1}", i.FirstName, i.LastName),
                    //Date = Utilities.ShortFormatDateYearMonthDayWithLocalTime(i.Date, TimeZone),
                    //Discounts = Utilities.DisplayCampDiscounts(i.Discounts),
                    Amount = i.TotalPrice,//Utilities.FormatCurrencyWithoutPenny(i.TotalPrice),
                    PaymentStatus = i.PaymentStatus,
                    PaymentMethod = i.PaymentMethod,
                    PaymentPlanType = i.PaymentPlanType,
                    PaymentStatusDescription = string.Format("{0} ({1})", i.PaymentStatus.ToDescription(), i.PaymentMethod.ToDescription()),
                    Date = i.StartTime.ToShortDateString(),
                    Times = string.Format("{0} - {1}", i.StartTime.ToShortTimeString(), i.EndTime.HasValue ? i.EndTime.Value.ToShortTimeString() : "")
                })
                .ToList();
            }
        }
    }

    public class CalendarOrderItemViewModel : OrderItemViewModelBase
    {
        public string Date { get; set; }

        public string Times { get; set; }
    }

    public class DonationOrderItemViewModel : OrderItemViewModelBase
    {
        public DonationOrderItemViewModel()
        {

        }
        public string Date { get; set; }
    }

    public class CalendarSearchViewModel : SearchViewModelBase
    {

        public CalendarSearchViewModel()
        {
        }

        public CalendarSearchViewModel(string domain, string clubDomain)
        {
            ClubDomain = clubDomain;
            Domain = domain;
        }
    }

    public class DonationSearchViewModel : SearchViewModelBase
    {
        public DonationSearchViewModel()
        {

        }
        public DonationSearchViewModel(string domain, string clubDomain)
        {
            ClubDomain = clubDomain;
            Domain = domain;
        }

    }

    //public class CampOrderItemViewModel : OrderItemViewModelBase
    //{
    //    public string SectionName { get; set; }

    //    public string Schedule { get; set; }

    //    public CampOrderItemViewModel()
    //        : base()
    //    {
    //    }
    //}

    //public class CampSearchViewModel : SearchViewModelBase
    //{
    //    public int? ScheduleId { get; set; }

    //    public int? SectionId { get; set; }

    //    public List<SelectListItem> Schedules { get; set; }

    //    public List<SelectListItem> Sections { get; set; }

    //    void SeedCampItems(int? scheduleId, int? sectionId, string clubDomain)
    //    {

    //        var _campSchedules = Ioc.ICampDbService.Get(Domain, clubDomain).Schedules;

    //        //Add "All Schedules" to dropdown 
    //        Schedules.Add(new SelectListItem { Value = "-1", Text = "All Schedules", Selected = true });
    //        //Add Schedules  to Schedule Property
    //        foreach (var item in _campSchedules)
    //        {
    //            Schedules.Add(new SelectListItem { Value = item.Id.ToString(), Text = Utilities.DisplayScheduleDates(item), Selected = (item.Id == (scheduleId != null ? scheduleId : -2)) });
    //        }

    //        var _campSections = Ioc.ICampDbService.Get(Domain, clubDomain).Sections;

    //        //Add "All Sections" item to dropdown 
    //        Sections.Add(new SelectListItem { Value = "-1", Text = "All Sections", Selected = true });
    //        //Add Sections  to Schedule Property
    //        foreach (var item in _campSections)
    //        {
    //            Sections.Add(new SelectListItem { Value = item.Id.ToString(), Text = item.Name, Selected = (item.Id == (sectionId != null ? sectionId : -2)) });
    //        }
    //    }

    //    public CampSearchViewModel()
    //        : base()
    //    {
    //        Schedules = new List<SelectListItem>();
    //        Sections = new List<SelectListItem>();
    //    }

    //    public CampSearchViewModel(string domain, string clubDomain, int? scheduleId, int? sectionId)
    //        : base(domain, clubDomain)
    //    {
    //        ScheduleId = scheduleId;
    //        SectionId = sectionId;

    //        Schedules = new List<SelectListItem>();
    //        Sections = new List<SelectListItem>();

    //        SeedCampItems(scheduleId, sectionId, clubDomain);
    //    }
    //}
    #endregion

    #region Order Details
    public class EventBaseInfoMetaData
    {
        public EventBaseInfoMetaData()
        {
            HealthInfo = new PlayerProfileComplitionFormPlayerProfileViewModel();
            Contact = new PlayerProfileComplitionFormContactViewModel();
            Skill = new PlayerProfileComplitionFormSkillViewModel();
            Doctor = new PlayerProfileComplitionFormDoctorContactViewModel();
            School = new PlayerProfileSchoolViewModel();
            EmergencyContact = new PlayerProfileComplitionFormEmergencyContactViewModel();
            Insurance = new InsuranceViewModel();
            ParentContact = new PlayerProfileParentInfoViewModel();
        }

        public ChessProfileInfoViewModel ChessProfile { get; set; }

        public TableTennisProfileInfoViewModel TableTennisProfile { get; set; }

        public PlayerProfileComplitionFormContactViewModel Contact { get; set; }

        public PlayerProfileParentInfoViewModel ParentContact { get; set; }

        public PlayerProfileComplitionFormSkillViewModel Skill { get; set; }

        public PlayerProfileComplitionFormDoctorContactViewModel Doctor { get; set; }

        public PlayerProfileSchoolViewModel School { get; set; }

        public PlayerProfileComplitionFormEmergencyContactViewModel EmergencyContact { get; set; }

        public PlayerProfileComplitionFormPlayerProfileViewModel HealthInfo { get; set; }

        public InsuranceViewModel Insurance { get; set; }



    }


    public static class OrderMetaDataHelper
    {
        public static PlayerProfileComplitionFormContactViewModel GetPlayerContactInfo(int orderId)
        {
            var eventBaseInfo = new EventBaseInfoMetaData();
            var orderdb = Ioc.IOrderDbService;
            //var baseInfo = orderdb.GetOrder(orderId, true).EventBaseInfoMetaData;

            //if (!string.IsNullOrEmpty(baseInfo))
            //    eventBaseInfo = SerializeHelper.Deserialize<EventBaseInfoMetaData>(baseInfo);

            return eventBaseInfo.Contact;
        }

        public static EventBaseInfoMetaData GetEventBaseInfo(int orderId)
        {
            var eventBaseInfo = new EventBaseInfoMetaData();
            var orderdb = Ioc.IOrderDbService;
            //var baseInfo = orderdb.GetOrder(orderId, true).EventBaseInfoMetaData;

            //if (!string.IsNullOrEmpty(baseInfo))
            //    eventBaseInfo = SerializeHelper.Deserialize<EventBaseInfoMetaData>(baseInfo);

            return eventBaseInfo;
        }

        public static Models.FormModels.FormViewModel GetCustomFieldMetaData(int orderId, CustomTemplateFor type)
        {
            var customFieldMetaData = new Models.FormModels.FormViewModel();
            var orderdb = Ioc.IOrderDbService;

            // get order to change XML
            var newXml = orderdb.GetOrder(orderId, true);

            string metadata = null;

            //if (type == CustomTemplateFor.JumbulaAdmin)
            //{
            //    metadata = orderdb.GetOrder(orderId, true).CustomFieldsMetaData;
            //}
            //else if (type == CustomTemplateFor.ClubAdmin)
            //{
            //    metadata = orderdb.GetOrder(orderId, true).ClubOwnerCustomFieldMetaData;
            //}

            // hardcode for change old custome model("CustomModelMetaData") fields to new custom model("FormViewModel")
            if (!string.IsNullOrEmpty(metadata) &&
                metadata.Trim() ==
                ("<CustomModelMetaData>\r\n  <Fields />\r\n  <Sections />\r\n</CustomModelMetaData>").Trim())
            {
                metadata = "<FormViewModel>\r\n  <Questions />\r\n</FormViewModel>";

                //if (type == CustomTemplateFor.JumbulaAdmin)
                //{
                //    newXml.CustomFieldsMetaData = metadata;
                //}
                //else
                //{
                //    newXml.ClubOwnerCustomFieldMetaData = metadata;
                //}
                orderdb.Update(newXml);
            }

            if (metadata != null && metadata.Contains("CustomModelMetaData"))
            {
                metadata = metadata.Replace("CustomModelMetaData", "FormViewModel");
                metadata = metadata.Replace("Fields", "Questions");
                metadata = metadata.Replace("CustomFieldValidation", "QuestionValidator");
                metadata = metadata.Replace("/CustomFieldValidation", "/QuestionValidator");
                metadata = metadata.Replace("CustomField", "Question");
                metadata = metadata.Replace("/CustomField", "/Question");
                //metadata = metadata.Replace("SectionId", "");
                //metadata = metadata.Replace("/SectionId", "");
                //if (type == CustomTemplateFor.JumbulaAdmin)
                //{
                //    newXml.CustomFieldsMetaData = metadata;
                //}
                //else
                //{
                //    newXml.ClubOwnerCustomFieldMetaData = metadata;
                //}
                orderdb.Update(newXml);

            }

            if (!string.IsNullOrEmpty(metadata))
                customFieldMetaData = SerializeHelper.Deserialize<Models.FormModels.FormViewModel>(metadata);

            return customFieldMetaData;
        }

        public enum CustomTemplateFor
        {
            JumbulaAdmin,
            ClubAdmin
        }

    }


    public class CourseOrderDetailsViewModel
    {
        public string CourseName { get; set; }

        public string PlayerName { get; set; }

        public string TotalPrice { get; set; }

        public string Charges { get; set; }

        public string Donation { get; set; }

        public string Discounts { get; set; }

        public string TotalAllPrice { get; set; }

        public string ConfirmationID { get; set; }

        public string Date { get; set; }

        public List<CourseOrderDetailsItemViewModel> OrderDetails { get; set; }

        public List<ChargeDiscount> OrderCharges { get; set; }

        public List<ChargeDiscount> OrderDiscounts { get; set; }

        public CourseOrderDetailsViewModel(int orderId, string eventName, string playerName)
        {
            CourseName = eventName;

            PlayerName = playerName;

            //Get order 
            Order order = Ioc.IOrderDbService.GetOrder(orderId, true);


            //Charges = Utilities.FormatCurrencyWithPenny(
            //                        order.Charges.Where(x => x.Category != ChargeDiscountCategories.EF && x.Category != ChargeDiscountCategories.Donation).Sum(x => x.Amount)
            //                        +
            //                         order.OrderItems.First().Charges.Where(x => x.Category != ChargeDiscountCategories.EF && x.Category != ChargeDiscountCategories.Donation).Sum(x => x.Amount)
            //                         );

            Date = order.OrderDate.ToShortDateString();
            ConfirmationID = order.ConfirmationId;

            // order and orderitems charges
            OrderCharges = new List<ChargeDiscount>();
            //OrderCharges.AddRange(order.Charges);
            //OrderCharges.AddRange(order.OrderItems.First().Charges);

            foreach (var charge in order.OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Charge))
            {
                OrderCharges.Add(new ChargeDiscount
                {
                    Amount = charge.Amount,
                    Desc = charge.Description,
                    Category = charge.Category
                });
            }

            foreach (var item in order.OrderItems)
            {
                foreach (var charge in item.OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Charge))
                {
                    OrderCharges.Add(new ChargeDiscount
                    {
                        Amount = charge.Amount,
                        Desc = charge.Description,
                        Category = charge.Category
                    });
                }
            }


            Donation = Utilities.FormatCurrencyWithoutPenny(order.OrderCharges.Where(c => c.Category == ChargeDiscountCategories.Donation).Sum(c => c.Amount));

            // Calculate sum of discounts(per orders and per order items)
            //Discounts = Utilities.FormatCurrencyWithoutPenny(order.Discounts.Sum(d => d.Amount) + order.OrderItems.FirstOrDefault().Discounts.Sum(o => o.Amount));

            // order and orderitems discounts
            OrderDiscounts = new List<ChargeDiscount>();
            //OrderDiscounts.AddRange(order.Discounts);
            //OrderDiscounts.AddRange(order.OrderItems.First().Discounts);

            foreach (var discount in order.OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Discount))
            {
                OrderDiscounts.Add(new ChargeDiscount
                {
                    Amount = discount.Amount,
                    Desc = discount.Description,
                    Category = discount.Category
                });
            }

            OrderDiscounts.AddRange(
                order.OrderItems.SelectMany(
                    orderItem =>
                        orderItem.OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Discount)
                            .Select(m => m.ToChargeDiscount())).ToList());

            //For security reasons
            int userId = TheMembership.GetCurrentUserId();

            List<CourseOrderDetailItem> items = new List<CourseOrderDetailItem>();// Ioc.IOrderDbService.CourseOrderDetails(orderId, userId).ToList();

            if (items != null)
            {
                OrderDetails = items.Select(i => new CourseOrderDetailsItemViewModel
                {
                    DropInSectionDate = i.DropInSectionDate,
                    RegistrationType = Utilities.FormatRegistrationType(i.RegistrationType),
                    SkillLevel = i.SkillLevel,
                    SkillRating = i.SkillRating,
                    Fees = i.Fees
                })
                .ToList();

                TotalPrice = Utilities.FormatCurrencyWithPenny(OrderDetails.Sum(i => i.Fees));

                TotalAllPrice = Utilities.FormatCurrencyWithPenny(order.OrderAmount);
            }
        }
    }

    public class CourseOrderDetailsItemViewModel
    {
        public string RegistrationType { get; set; }

        public DateTime? DropInSectionDate { get; set; }

        public int? SkillRating { get; set; }

        public SkillLevelCategories SkillLevel { get; set; }

        public decimal Fees { get; set; }
    }

    public class CampOrderDetailsViewModel
    {
        public string CampName { get; set; }

        public string PlayerName { get; set; }

        public string Charges { get; set; }

        public string Donation { get; set; }

        public string Discounts { get; set; }

        public string TotalPrice { get; set; }

        public string TotalAllPrices { get; set; }

        public ICollection<OrderItem> OrderItemsForView { get; set; }
        public string ConfirmationID { get; set; }
        public string Date { get; set; }
        public List<ChargeDiscount> OrderDiscounts { get; set; }

        public List<CampOrderDetailsItemViewModel> OrderDetails { get; set; }

        //public CampOrderDetailsViewModel(int orderId, string eventName, string playerName)
        //{
        //    CampName = eventName;

        //    PlayerName = playerName;

        //    //Get order 
        //    Order order = Ioc.IOrderDbService.GetOrder(orderId, true);

        //    OrderItemsForView = order.OrderItems;
        //    // order and orderitems discounts
        //    OrderDiscounts = new List<ChargeDiscount>();
        //    //OrderDiscounts.AddRange(order.Discounts);
        //    //OrderDiscounts.AddRange(order.OrderItems.First().Discounts);

        //    //foreach (var discount in order.OrderCharges.Where(m=>m.Subcategory==ChargeDiscountSubcategory.Discount))
        //    //{
        //    //    OrderDiscounts.Add(new ChargeDiscount
        //    //    {
        //    //        Amount = discount.Amount,
        //    //        Desc = discount.Description,
        //    //        Category = discount.Category,
        //    //    });
        //    //}

        //    OrderDiscounts.AddRange(
        //        order.OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Discount)
        //            .Select(m => new ChargeDiscount
        //            {
        //                Amount = m.Amount,
        //                Desc = m.Description,
        //                Category = m.Category,
        //            }).ToList());

        //    //foreach (var orderItem in order.OrderItems)
        //    //{
        //    //    foreach (var discount in orderItem.OrderCharges.Where(m=>m.Subcategory==ChargeDiscountSubcategory.Discount))
        //    //    {
        //    //        OrderDiscounts.Add(new ChargeDiscount
        //    //        {
        //    //            Amount = discount.Amount,
        //    //            Desc = discount.Description,
        //    //            Category = discount.Category
        //    //        });
        //    //    }
        //    //}

        //    OrderDiscounts.AddRange(
        //        order.OrderItems.SelectMany(
        //            orderItem =>
        //                orderItem.OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Discount)
        //                    .Select(m => new ChargeDiscount
        //                    {
        //                        Amount = m.Amount,
        //                        Desc = m.Description,
        //                        Category = m.Category
        //                    })).ToList());

        //    // charges = sum of oredr.charges + sum of each orderitems charge
        //    //var charges = order.Charges.Where(c => c.Category != ChargeDiscountCategories.Donation && c.Category != ChargeDiscountCategories.EF).Sum(c => c.Amount)
        //    //    +
        //    //    order.OrderItems.SelectMany(orderItem => orderItem.Charges.Where(c => c.Category != ChargeDiscountCategories.EF && c.Category != ChargeDiscountCategories.Donation)).Sum(item => item.Amount);
        //    var charges = order.OrderCharges.Where(
        //        m =>
        //            m.Subcategory == ChargeDiscountSubcategory.Charge && m.Category != ChargeDiscountCategories.Donation &&
        //            m.Category != ChargeDiscountCategories.EF).Sum(m => m.Amount)
        //                  +
        //                  order.OrderItems.SelectMany(
        //                      orderItem =>
        //                          orderItem.OrderCharges.Where(
        //                              m =>
        //                                  m.Subcategory == ChargeDiscountSubcategory.Charge &&
        //                                  m.Category != ChargeDiscountCategories.Donation &&
        //                                  m.Category != ChargeDiscountCategories.EF)).Sum(c => c.Amount);


        //    Charges = Utilities.FormatCurrencyWithPenny(charges);

        //    //Donation = Utilities.FormatCurrencyWithoutPenny(order.Charges.Where(c => c.Category == ChargeDiscountCategories.Donation).Sum(c => c.Amount));
        //    Donation =
        //        Utilities.FormatCurrencyWithoutPenny(
        //            order.OrderCharges.Where(
        //                m =>
        //                    m.Subcategory == ChargeDiscountSubcategory.Charge &&
        //                    m.Category == ChargeDiscountCategories.Donation).Sum(c => c.Amount));

        //    // discount  = sum of order discounts + sum of each orderitems discounts
        //    //var discounts = order.Discounts.Sum(d => d.Amount) + order.OrderItems.SelectMany(orderItem => orderItem.Discounts).Sum(item => item.Amount);
        //    var discounts =
        //        order.OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Discount).Sum(c => c.Amount) +
        //        order.OrderItems.SelectMany(
        //            orderItem => orderItem.OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Discount))
        //            .Sum(c => c.Amount);
        //    Discounts = Utilities.FormatCurrencyWithoutPenny(discounts);

        //    //For security reasons
        //    int userId = TheMembership.GetCurrentUserId();

        //    Date = order.OrderDate.ToShortDateString();
        //    ConfirmationID = order.ConfirmationId;

        //    List<CampOrderDetailItem> items = Ioc.IOrderDbService.CampOrderDetails(orderId, userId).ToList();

        //    if (items != null)
        //    {
        //        OrderDetails = items.Select(i => new CampOrderDetailsItemViewModel
        //        {

        //            Fees = i.Fees,
        //            Discounts = Utilities.DisplayCampDiscounts(i.Discounts, i.EbDiscount, 0),
        //            Name = i.Name,
        //            Hour = i.Hour,
        //            Description = i.Description,
        //            Schedule = Utilities.DisplayScheduleDates(i.StartDate, i.EndDate),
        //        })
        //        .ToList();

        //        TotalPrice = Utilities.FormatCurrencyWithPenny(OrderDetails.Sum(i => i.Fees));

        //        TotalAllPrices = Utilities.FormatCurrencyWithPenny(order.OrderAmount);
        //    }
        //}
    }

    public class CampOrderDetailsItemViewModel
    {
        public decimal Fees { get; set; }

        public string Discounts { get; set; }

        public string Name { get; set; }

        public string Hour { get; set; }

        public string Description { get; set; }

        public string Schedule { get; set; }
    }

    public class CalendarOrderDetailsViewModel
    {
        public string EventName { get; set; }

        public string PlayerName { get; set; }

        public string Charges { get; set; }

        public string Donation { get; set; }

        public string Discounts { get; set; }

        public string TotalPrice { get; set; }

        public string TotalAllPrices { get; set; }

        public ICollection<OrderItem> OrderItemsForView { get; set; }
        public string ConfirmationID { get; set; }
        public string Date { get; set; }

        // public List<ChargeDiscount> OrderDiscounts { get; set; }

        public List<CalendarOrderDetailsItemViewModel> OrderDetails { get; set; }

        public CalendarOrderDetailsViewModel(int orderId, string eventName, string playerName)
        {
            EventName = eventName;

            PlayerName = playerName;

            //Get order 
            Order order = Ioc.IOrderDbService.GetOrder(orderId, true);

            OrderItemsForView = order.OrderItems;
            // order and orderitems discounts
            //OrderDiscounts = new List<ChargeDiscount>();
            //OrderDiscounts.AddRange(order.Discounts);
            //OrderDiscounts.AddRange(order.OrderItems.First().Discounts);

            var charges = order.OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Charge).Sum(m => m.Amount);
            charges +=
                order.OrderItems.SelectMany(
                    m => m.OrderCharges.Where(s => s.Subcategory == ChargeDiscountSubcategory.Charge))
                    .Sum(item => item.Amount);
            // charges = sum of oredr.charges + sum of each orderitems charge
            //var charges = order.Charges.Where(c => c.Category != ChargeDiscountCategories.Donation && c.Category != ChargeDiscountCategories.EF).Sum(c => c.Amount)
            //    +
            //    order.OrderItems.SelectMany(orderItem => orderItem.Charges.Where(c => c.Category != ChargeDiscountCategories.EF && c.Category != ChargeDiscountCategories.Donation)).Sum(item => item.Amount);

            Charges = Utilities.FormatCurrencyWithPenny(charges);

            Donation = Utilities.FormatCurrencyWithoutPenny(order.OrderCharges.Where(c => c.Category == ChargeDiscountCategories.Donation).Sum(c => c.Amount));

            // discount  = sum of order discounts + sum of each orderitems discounts
            //var discounts = order.Discounts.Sum(d => d.Amount) + order.OrderItems.SelectMany(orderItem => orderItem.Discounts).Sum(item => item.Amount);
            var discounts = order.OrderCharges.Where(p => p.Subcategory == ChargeDiscountSubcategory.Discount).Sum(p => p.Amount);
            discounts +=
                order.OrderItems.SelectMany(
                    orderItem =>
                        orderItem.OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Discount)).Sum(m => m.Amount);

            Discounts = Utilities.FormatCurrencyWithoutPenny(discounts);

            Date = order.OrderDate.ToShortDateString();
            ConfirmationID = order.ConfirmationId;

            List<EventOrderDetailItem> items = new List<EventOrderDetailItem>();// Ioc.IOrderDbService.EventOrderDetails(orderId).ToList();

            //if (items != null)
            //{
            //    OrderDetails = items.Select(i => new CalendarOrderDetailsItemViewModel
            //    {
            //        Start = i.Start,
            //        End = i.End,
            //        EventOrderItemId = i.EventOrderItemId,
            //        Fee = i.Fee,
            //        Description = string.Format("{0} - {1}-{2}: ({3})", i.Start.ToShortDateString(), i.Start.ToShortTimeString(), i.End.ToShortTimeString(), Utilities.FormatCurrencyWithoutPenny(i.Fee))
            //    })
            //    .ToList();

            //    TotalPrice = Utilities.FormatCurrencyWithPenny(OrderDetails.Sum(i => i.Fee));

            //    TotalAllPrices = Utilities.FormatCurrencyWithPenny(order.OrderAmount);
            //}
        }
    }

    public class CalendarOrderDetailsItemViewModel
    {
        public string EventOrderItemId { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public decimal Fee { get; set; }

        public string Description { get; set; }
    }

    //public class TourneyOrderDetailsViewModel
    //{
    //    public string TourneyName { get; set; }

    //    public string PlayerName { get; set; }

    //    public string TotalPrice { get; set; }

    //    public string Charges { get; set; }

    //    public string Donation { get; set; }

    //    public string Discounts { get; set; }

    //    public string TotalAllPrice { get; set; }

    //    public string ConfirmationID { get; set; }

    //    public string Date { get; set; }

    //    public List<TourneyOrderDetailsItemViewModel> OrderDetails { get; set; }

    //    public List<ChargeDiscount> OrderCharges { get; set; }

    //    public List<ChargeDiscount> OrderDiscounts { get; set; }

    //    public TourneyOrderDetailsViewModel(int orderId, string eventName, string playerName)
    //    {
    //        TourneyName = eventName;

    //        PlayerName = playerName;

    //        //Get order 
    //        Order order = Ioc.IOrderDbService.GetOrder(orderId, true);

    //        // order and orderitems charges
    //        OrderCharges = new List<ChargeDiscount>();
    //        foreach (var charge in order.OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Charge))
    //        {
    //            OrderCharges.Add(charge.ToChargeDiscount());
    //        }

    //        foreach (var charge in order.OrderItems.First().OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Charge))
    //        {
    //            OrderCharges.Add(charge.ToChargeDiscount());
    //        }
    //        //OrderCharges.AddRange(order.Charges);
    //        //OrderCharges.AddRange(order.OrderItems.First().OrderCharges.Where(m=>m.Subcategory== ChargeDiscountSubcategory.Charge));

    //        // order and orderitems discounts

    //        OrderDiscounts = new List<ChargeDiscount>();
    //        //OrderDiscounts.AddRange(order.Discounts);
    //        //OrderDiscounts.AddRange(order.OrderItems.First().Discounts);
    //        foreach (var dis in order.OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Discount))
    //        {
    //            OrderDiscounts.Add(dis.ToChargeDiscount());
    //        }

    //        foreach (var dis in order.OrderItems.First().OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Discount))
    //        {
    //            OrderCharges.Add(dis.ToChargeDiscount());
    //        }

    //        Date = order.OrderDate.ToShortDateString();
    //        ConfirmationID = order.ConfirmationId;

    //        var charges = order.OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Charge).Sum(c => c.Amount)
    //                        +
    //                     order.OrderItems.SelectMany(orderItem => orderItem.OrderCharges.Where(c => c.Subcategory == ChargeDiscountSubcategory.Charge)).Sum(item => item.Amount);

    //        Charges = Utilities.FormatCurrencyWithPenny(charges);

    //        Donation = Utilities.FormatCurrencyWithoutPenny(order.OrderCharges.Where(c => c.Category == ChargeDiscountCategories.Donation).Sum(c => c.Amount));

    //        var discount = order.OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Discount).Sum(d => d.Amount)
    //                           + order.OrderItems.SelectMany(orderitem => orderitem.OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Discount)).Sum(item => item.Amount);
    //        Discounts = Utilities.FormatCurrencyWithoutPenny(discount);


    //        //For security reasons
    //        int userId = TheMembership.GetCurrentUserId();

    //        List<TourneyOrderDetailItem> items = Ioc.IOrderDbService.TourneyOrderDetails(orderId, userId).ToList();

    //        if (items != null)
    //        {
    //            OrderDetails = items.Select(i => new TourneyOrderDetailsItemViewModel
    //            {
    //                Section = i.Section,
    //                Schedule = i.Schedule,
    //                Options = i.Options,
    //                Byes = i.Byes,
    //                Fees = i.Fees
    //            })
    //            .ToList();

    //            TotalPrice = Utilities.FormatCurrencyWithPenny(OrderDetails.Sum(i => i.Fees));

    //            TotalAllPrice = Utilities.FormatCurrencyWithPenny(order.OrderAmount);
    //        }
    //    }
    //}

    //public class TourneyOrderDetailsItemViewModel
    //{
    //    public string Section { get; set; }

    //    public string Schedule { get; set; }

    //    public string Byes { get; set; }

    //    public string Options { get; set; }

    //    public decimal Fees { get; set; }
    //}

    //public class TableTennisTourneyOrderDetailsViewModel
    //{
    //    public string TourneyName { get; set; }

    //    public string PlayerName { get; set; }

    //    public List<TableTennisTourneyOrderDetailsItemViewModel> OrderDetails { get; set; }

    //    public int PlayerId { get; set; }

    //    public int OrderID { get; set; }

    //    public string Domain { get; set; }

    //    public string SubDomain { get; set; }

    //    public string TotalPrice { get; set; }

    //    public string Charges { get; set; }

    //    public string Donation { get; set; }

    //    public string Discounts { get; set; }

    //    public string TotalAllPrice { get; set; }

    //    //public TableTennisTourneyOrderDetailsViewModel(int orderId, string eventName, string playerName)
    //    //{
    //    //    TourneyName = eventName;
    //    //    PlayerName = playerName;
    //    //    OrderID = orderId;

    //    //    //Get order 
    //    //    Order order = Ioc.IOrderDbService.GetOrder(orderId, true);
    //    //    Domain = order.Domain;
    //    //    SubDomain = order.SubDomain;
    //    //    PlayerId = order.PlayerId.Value;


    //    //    var charges = order.OrderCharges.Where(c => c.Category != ChargeDiscountCategories.Donation && c.Category != ChargeDiscountCategories.EF && c.Subcategory == ChargeDiscountSubcategory.Charge).Sum(c => c.Amount)
    //    //                        +
    //    //                        order.OrderItems.SelectMany(orderItem => orderItem.OrderCharges.Where(c => c.Category != ChargeDiscountCategories.EF && c.Category != ChargeDiscountCategories.Donation && c.Subcategory == ChargeDiscountSubcategory.Charge)).Sum(item => item.Amount);
    //    //    Charges = Utilities.FormatCurrencyWithPenny(charges);

    //    //    Donation = Utilities.FormatCurrencyWithoutPenny(order.OrderCharges.Where(c => c.Category == ChargeDiscountCategories.Donation).Sum(c => c.Amount));

    //    //    var discount = order.OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Discount)
    //    //        .Sum(d => d.Amount)
    //    //                   +
    //    //                   order.OrderItems.SelectMany(
    //    //                       orderitem =>
    //    //                           orderitem.OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Discount))
    //    //                       .Sum(item => item.Amount);
    //    //    Discounts = Utilities.FormatCurrencyWithoutPenny(discount);


    //    //    List<TableTennisTourneyOrderDetailItem> items;

    //    //    //For security reasons, check user role
    //    //    if (TheRoles.GetCurrentUserRole() == RoleCategories.Player)
    //    //    {
    //    //        int userId = TheMembership.GetCurrentUserId();
    //    //        items = Ioc.IOrderDbService.TableTennisTourneyOrderDetails(orderId, userId).ToList();
    //    //    }
    //    //    else
    //    //    {
    //    //        string clubDomain = JBMembership.GetActiveClubBaseInfo().Domain;
    //    //        items = Ioc.IOrderDbService.TableTennisTourneyOrderDetails(orderId, clubDomain).ToList();
    //    //    }

    //    //    if (items != null)
    //    //    {
    //    //        OrderDetails = items.Select(i => new TableTennisTourneyOrderDetailsItemViewModel
    //    //        {
    //    //            EventName = i.EventName,
    //    //            Date = Utilities.GetEventDateRange(i.StartDate, i.EndDate),
    //    //            Time = Utilities.FormatTimeWithoutSecond(i.Time),
    //    //            Fees = i.Fees,
    //    //            Prizes = Utilities.JoinPrizes(i.Prizes),
    //    //            ItemId = i.ItemID,
    //    //            //OrderId = i.OrderID
    //    //            Partner = i.Partner,
    //    //            PaymentStatus = i.PaymentStatus,


    //    //        })
    //    //        .ToList();

    //    //        TotalPrice = Utilities.FormatCurrencyWithPenny(OrderDetails.Sum(i => i.Fees));

    //    //        TotalAllPrice = Utilities.FormatCurrencyWithPenny(order.OrderAmount);
    //    //    }
    //    //}
    //}

    //public class TableTennisTourneyOrderDetailsItemViewModel
    //{
    //    public int ItemId { get; set; }

    //    public int OrderId { get; set; }

    //    public string EventName { get; set; }

    //    public string Date { get; set; }

    //    public string Time { get; set; }

    //    public decimal Fees { get; set; }

    //    public string Prizes { get; set; }

    //    public PaymentStatusCategories PaymentStatus { get; set; }

    //    public string Partner { get; set; }
    //}
    #endregion

    #region Entries
    public class ChessTourneyEntriesViewModel
    {
        /// <summary>
        /// Tourney name ( used in dialog header )
        /// </summary>
        public string Name { get; set; }

        public string ClubDomain { get; set; }

        public string Domain { get; set; }

        /// <summary>
        /// list of tournament entries
        /// </summary>
        public ICollection<ChessTourneyEntryViewModel> Entries { get; set; }

        public TourneyRewardType TourneyType { get; set; }

        public ChessTourneyEntriesViewModel()
        {
            Entries = new List<ChessTourneyEntryViewModel>();
        }

        public ChessTourneyEntriesViewModel(string clubDomain, string domain, TourneyRewardType type, string name)
            : this()
        {

            TourneyType = type;
            Name = name;
            this.Domain = domain;
            this.ClubDomain = clubDomain;

            IEnumerable<ChessTourneyEntryItem> entries = Ioc.IOrderDbService.ChessTourneyEntries(clubDomain, domain);

            try
            {
                foreach (var item in entries)
                {
                    var entry = new ChessTourneyEntryViewModel(item);


                    if (TourneyType == TourneyRewardType.Quad && entry.UscfRating != null)
                    {
                        // calculate rating number for quad tournaments

                        try
                        {
                            int index = entry.UscfRating.IndexOf("*");
                            var rating = int.Parse(entry.UscfRating.Substring(0, index));
                            entry.RatingNumber = rating;

                            Entries.Add(entry);

                            continue;
                        }
                        catch
                        {
                        }

                        try
                        {
                            int index = entry.UscfRating.IndexOf("/");
                            var rating = int.Parse(entry.UscfRating.Substring(0, index));
                            entry.RatingNumber = rating;

                            Entries.Add(entry);

                            continue;
                        }
                        catch
                        {
                        }

                        try
                        {
                            int rating;
                            if (int.TryParse(entry.UscfRating.Trim(), out rating))
                            {
                                entry.RatingNumber = rating;
                            }
                            else
                            {
                                entry.RatingNumber = null;
                            }


                            Entries.Add(entry);

                            continue;
                        }
                        catch
                        {
                        }

                    } // end of if
                    else // other type of tourneys
                    {
                        Entries.Add(entry);
                    }

                } // end of foreach

                // if it's quad, resort by rating number
                if (TourneyType == TourneyRewardType.Quad)
                {
                    IEnumerable<ChessTourneyEntryViewModel> sorted = Entries.OrderByDescending(e => e.RatingNumber);

                    Entries = new List<ChessTourneyEntryViewModel>(sorted);
                }
            }
            catch (Exception e)
            {

                throw;
            }

        } // end of constructor
    }

    public class ChessTourneyEntryViewModel
    {
        public string Date { get; set; }

        public string FullName { get; set; }

        public string UscfId { get; set; }

        public string UscfExpirationDate { get; set; }

        public string UscfRating { get; set; }

        public int? RatingNumber { get; set; }

        public string Section { get; set; }

        public string Schedule { get; set; }

        public string Byes { get; set; }

        public int? ProfileId { get; set; }

        public string Options { get; set; }

        public JbForm JbForm { get; set; }

        public ChessTourneyEntryViewModel() { }

        // public SportInUserProfile SportProfile { get; set; }

        public ChessTourneyEntryViewModel(ChessTourneyEntryItem item)
            : this()
        {
            Date = item.Date.FormatDateSeparateWithComma();

            FullName = string.Format("{0} {1}", item.FirstName, item.LastName);

            Section = item.Section;

            Schedule = item.Schedule;

            Byes = item.Byes;

            Options = item.Options;

            ProfileId = item.ProfileId;

            if (item.JbForm != null)
            {
                foreach (var section in item.JbForm.Elements.Where(e => e is JbSection).Select(e => e as JbSection))
                {
                    foreach (var element in section.Elements)
                    {
                        if (element is USCFInfo)
                        {
                            var uscf = element as USCFInfo;

                            UscfId = uscf.UscfId;

                            UscfRating = uscf.UscfRatingReg;

                            UscfExpirationDate = uscf.UscfExpiration.HasValue ? uscf.UscfExpiration.Value.ToShortDateString() : string.Empty;// section.Elements.Single(e => e.Name == "UscfExpiration").GetValue().ToString();
                        }
                    }
                }
            }
        }
    }

    public enum TableTennisTourneyEntriesSort
    {
        Player,
        Event
    }
    //public class TableTennisTourneyEntriesViewModel
    //{
    //    public string TourneyName { get; set; }

    //    public string ClubDomain { get; set; }

    //    public string Domain { get; set; }

    //    public TableTennisTourneyEntriesSort Sort { get; set; }

    //    public ICollection<TableTennisTourneyEntryViewModel> Entries { get; set; }

    //    public TableTennisTourneyEntriesViewModel()
    //    {
    //        Entries = new List<TableTennisTourneyEntryViewModel>();

    //    }

    //    public TableTennisTourneyEntriesViewModel(string clubDomain, string domain, TableTennisTourneyEntriesSort sort)
    //        : this()
    //    {
    //        //TTTourney tourney = DependencyResolverHelper.ITTTourneyDbService.Get(domain, clubDomain);

    //        //TourneyName = tourney.Name;

    //        this.ClubDomain = clubDomain;

    //        this.Domain = domain;

    //        Sort = Sort;
    //        IEnumerable<TableTennisTourneyEntryItem> entries = null;

    //        if (sort == TableTennisTourneyEntriesSort.Player)
    //        {
    //            entries = Ioc.IOrderDbService.TableTennisEntriesSortByPlayer(clubDomain, domain);
    //        }
    //        else
    //        {
    //            entries = Ioc.IOrderDbService.TableTennisEntriesSortByEvent(clubDomain, domain);
    //        }

    //        foreach (var item in entries)
    //        {
    //            Entries.Add(new TableTennisTourneyEntryViewModel(item));
    //        }


    //    }
    //}

    //public class TableTennisTourneyEntryViewModel
    //{
    //    public string FullName { get; set; }

    //    public string EventName { get; set; }

    //    public string Date { get; set; }

    //    public string Partner { get; set; }

    //    public string ClubAffiliation { get; set; }

    //    public string UsTTId { get; set; }

    //    public string UsTTRating { get; set; }

    //    public GenderCategories Gender { get; set; }

    //    public string State { get; set; }

    //    public TableTennisTourneyEntryViewModel()
    //    {

    //    }

    //    public TableTennisTourneyEntryViewModel(TableTennisTourneyEntryItem item)
    //    {
    //        FullName = string.Format("{0} {1}", item.FirstName, item.LastName);

    //        Date = item.Date.FormatDateSeparateWithComma();

    //        Partner = item.Partner;

    //        ClubAffiliation = item.ClubAffiliation;

    //        Gender = item.Gender;

    //        State = item.State;

    //        EventName = item.Events;

    //        // deserialize sport profile
    //        if (item.SportProfile != null)
    //        {
    //            TableTennisProfileInfoViewModel sport = SerializeHelper.Deserialize<TableTennisProfileInfoViewModel>(item.SportProfile.SportInfo);

    //            UsTTId = sport.UsTTId;

    //            UsTTRating = sport.UsTTRating;
    //        }
    //    }
    //}
    #endregion

    #region Payments
    //public class CampPaymentsModel
    //{
    //    public string Domain { get; set; }

    //    public string ClubDomain { get; set; }

    //    public string CampName { get; set; }

    //    public SubDomainCategories SubDomain { get; set; }

    //    public int CategoryId { get; set; }

    //    public ICollection<CampPaymentItemViewModel> Payments { get; set; }

    //    public decimal TotalPayments { get; set; }

    //    public ClubFeeRateCodes ClubFeeRateCode { get; set; }

    //    public int ScheduleNo { get; set; }

    //    public bool IsClubReport { get; set; }

    //    public CampPaymentsModel(string clubDomain, string domain)
    //    {
    //        int total = 0;
    //        IEnumerable<CampPaymentItem> items = DependencyResolverHelper.IOrderDbService.CampPayments(clubDomain, domain, null, null, null, null, null, null, false, int.MaxValue, 0, ref total);

    //        Domain = domain;

    //        ClubDomain = clubDomain;

    //        Camp camp = DependencyResolverHelper.ICampDbService.Get(domain, clubDomain);

    //        Club club = DependencyResolverHelper.IClubDbService.Get(ClubDomain);

    //        ClubFeeRateCode = club.ClubFeeRateCode;

    //        Payments = new List<CampPaymentItemViewModel>();

    //        foreach (var item in items)
    //        {
    //            Payments.Add(new CampPaymentItemViewModel(item));

    //            TotalPayments += item.TotalPrice;
    //        }

    //        CategoryId = camp.CategoryId;

    //        SubDomain = SubDomainCategories.Camp;

    //        //SectionName = camp.Sections.FirstOrDefault(s => s.Category == (CampSectionCategories)scheduleNo).Name;
    //    }
    //}

    //public class CampPaymentItemViewModel
    //{
    //    public string Date { get; set; }

    //    public string FullName { get; set; }

    //    public decimal TotalPrice { get; set; }

    //    public string Discounts { get; set; }

    //    public string Services { get; set; }

    //    public int ProfileId { get; set; }

    //    public string Program { get; set; }

    //    public string SectionName { get; set; }

    //    public CampPaymentItemViewModel() { }

    //    public CampPaymentItemViewModel(CampPaymentItem payment)
    //        : this()
    //    {

    //        Date = payment.Date.FormatDateSeparateWithComma();

    //        FullName = string.Format("{0} {1}", payment.FirstName, payment.LastName);

    //        TotalPrice = payment.TotalPrice;

    //        Discounts = Utilities.DisplayCampDiscounts(payment.Discounts);

    //        Services = Utilities.DisplayCampServices(payment.Services);

    //        ProfileId = payment.ProfileId;

    //        Program = payment.Description;

    //        SectionName = payment.SectionName;
    //    }
    //}

    //public class TourneyPaymentsModel
    //{
    //    public string ClubDomain { get; set; }

    //    public string Domain { get; set; }

    //    public string TourneyName { get; set; }

    //    public ClubFeeRateCodes ClubFeeRateCode { get; set; }

    //    public ICollection<TourneyPaymentItemViewModel> Payments { get; set; }

    //    public int CategoryId { get; set; }

    //    public decimal TotalPayments { get; set; }

    //    public TourneyPaymentsModel()
    //    {
    //        Payments = new List<TourneyPaymentItemViewModel>();
    //    }

    //    public TourneyPaymentsModel(string clubDomain, string domain)
    //        : this()
    //    {
    //        ClubDomain = clubDomain;

    //        Domain = domain;

    //        Tourney tourney = DependencyResolverHelper.ITourneyDbService.Get(domain, clubDomain);

    //        TourneyName = tourney.Name;

    //        CategoryId = tourney.CategoryId;

    //        Club club = DependencyResolverHelper.IClubDbService.Get(ClubDomain);

    //        ClubFeeRateCode = club.ClubFeeRateCode;

    //        int total = 0;
    //        IEnumerable<TourneyPaymentItem> items = DependencyResolverHelper.IOrderDbService.TourneyPayments(clubDomain, domain, null, null, null, null, false, int.MaxValue, 0, ref total);

    //        foreach (var item in items)
    //        {
    //            Payments.Add(new TourneyPaymentItemViewModel(item));

    //            TotalPayments += item.TotalPrice;
    //        }
    //    }
    //}

    //public class TourneyPaymentItemViewModel
    //{
    //    public bool IsChecked { get; set; }
    //    public string Date { get; set; }

    //    public string FullName { get; set; }

    //    public string Section { get; set; }

    //    public string Schedule { get; set; }

    //    public string Byes { get; set; }

    //    public string Options { get; set; }

    //    public string TotalPrice { get; set; }

    //    public int ProfileId { get; set; }

    //    public string UserEmail { get; set; }

    //    public string PlayerEmail { get; set; }

    //    public TourneyPaymentItemViewModel(TourneyPaymentItem payment)
    //    {
    //        FullName = string.Format("{0} {1}", payment.FirstName, payment.LastName);

    //        Date = payment.Date.FormatDateSeparateWithComma();

    //        Section = payment.Section;

    //        Schedule = payment.Schedule;

    //        Byes = payment.Byes;

    //        Options = payment.Options;

    //        if (string.IsNullOrEmpty(Options))
    //        {
    //            Options = null;
    //        }

    //        TotalPrice = Utilities.FormatCurrencyWithoutPenny(payment.TotalPrice);

    //        ProfileId = payment.ProfileId;
    //    }
    //}

    //public class CoursePaymentsModel
    //{
    //    public string ClubDomain { get; set; }

    //    public string Domain { get; set; }

    //    public string CourseName { get; set; }

    //    public ICollection<CoursePaymentItemViewModel> Payments { get; set; }

    //    public ClubFeeRateCodes ClubFeeRateCode { get; set; }

    //    public decimal TotalPayments { get; set; }

    //    public int CategoryId { get; set; }

    //    public SkillViewModel Skill { get; set; }

    //    public string EventNames { get; set; }

    //    public CoursePaymentsModel()
    //    {
    //        Payments = new List<CoursePaymentItemViewModel>();
    //    }

    //    public CoursePaymentsModel(string clubDomain, string domain)
    //        : this()
    //    {
    //        ClubDomain = clubDomain;

    //        Domain = domain;

    //        Course course = DependencyResolverHelper.ICourseDbService.Get(clubDomain, domain);

    //        CourseName = course.Name;

    //        Club club = DependencyResolverHelper.IClubDbService.Get(clubDomain);

    //        ClubFeeRateCode = club.ClubFeeRateCode;

    //        CategoryId = course.CategoryId;

    //        int total = 0;
    //        IEnumerable<CoursePaymentItem> items = DependencyResolverHelper.IOrderDbService.CoursePayments(clubDomain, domain, null, null, null, null, false, int.MaxValue, 0, ref total);

    //        foreach (var item in items)
    //        {
    //            Payments.Add(new CoursePaymentItemViewModel(item));

    //            TotalPayments += item.TotalPrice;
    //        }
    //    }
    //}

    //public class CoursePaymentItemViewModel
    //{
    //    public string ItemId { get; set; }

    //    public int ProfileId { get; set; }

    //    public string FullName { get; set; }

    //    public string Date { get; set; }

    //    public string Discounts { get; set; }

    //    public string RegistrationType { get; set; }

    //    public string TotalPrice { get; set; }

    //    public CoursePaymentItemViewModel()
    //    {

    //    }

    //public CoursePaymentItemViewModel(CoursePaymentItem payment)
    //    : this()
    //{
    //    FullName = string.Format("{0} {1}", payment.FirstName, payment.LastName);

    //    Date = payment.Date.FormatDateSeparateWithComma();

    //    Discounts = Utilities.DisplayCampDiscounts(payment.Discounts);

    //    RegistrationType = Utilities.FormatRegistrationType(payment.RegistrationType);

    //    TotalPrice = Utilities.FormatCurrencyWithoutPenny(payment.TotalPrice);

    //    ProfileId = payment.ProfileId;

    //    ItemId = payment.ItemId;
    //}
    //}

    //public class SelectCampSectionViewModel
    //{
    //    public Camp Camp { get; set; }

    //    public ICollection<CampScheduleViewModel> Schedules { get; set; }

    //    public SelectCampSectionViewModel()
    //    {
    //        Schedules = new List<CampScheduleViewModel>();
    //    }

    //    public SelectCampSectionViewModel(Camp camp)
    //        : this()
    //    {
    //        this.Camp = camp;

    //        foreach (var item in camp.Schedules)
    //        {
    //            Schedules.Add(new CampScheduleViewModel(item));
    //        }
    //    }
    //}

    //public class TableTennisTourneyPaymentsViewModel
    //{
    //    public string ClubDomain { get; set; }

    //    public string Domain { get; set; }

    //    public string TourneyName { get; set; }

    //    public decimal TotalPayments { get; set; }

    //    public ClubFeeRateCodes ClubFeeRateCode { get; set; }

    //    public TableTennisTourneyPaymentSearchViewModel SearchModel { get; set; }

    //    public List<TableTennisTourneyPaymentItemViewModel> Payments { get; set; }

    //    public TableTennisTourneyPaymentsViewModel()
    //    {
    //        Payments = new List<TableTennisTourneyPaymentItemViewModel>();

    //        TotalPayments = 0;
    //    }

    //    public TableTennisTourneyPaymentsViewModel(string clubDomain, string domain)
    //        : this()
    //    {
    //        ClubDomain = clubDomain;
    //        Domain = domain;

    //        SearchModel = new TableTennisTourneyPaymentSearchViewModel(clubDomain, domain);

    //        Club club = DependencyResolverHelper.IClubDbService.Get(ClubDomain);
    //        ClubFeeRateCode = club.ClubFeeRateCode;

    //        TourneyName = DependencyResolverHelper.ITTTourneyDbService.GetName(Domain, ClubDomain, false);

    //        SeedItems(clubDomain, domain, null, null, string.Empty, 0, null);
    //    }

    //    public TableTennisTourneyPaymentsViewModel(TableTennisTourneyPaymentSearchViewModel searchModel)
    //    {
    //        Payments = new List<TableTennisTourneyPaymentItemViewModel>();

    //        SearchModel = searchModel;

    //        SeedItems(SearchModel.ClubDomain, SearchModel.Domain, SearchModel.StartDate, SearchModel.EndDate, SearchModel.SearchText, SearchModel.SearchTypes, searchModel.EventNames);
    //    }

    //    void SeedItems(string clubDomain, string domain, DateTime? startDate, DateTime? endDate, string textSearch, SportsClub.Db.OrderDb.TableTennisTourneyPaymentSearchOption searchType, List<string> eventNames)
    //    {
    //        //Return tttourney payment items
    //        int total = 0;
    //        IEnumerable<TableTennisTourneyPaymentItem> items = DependencyResolverHelper.IOrderDbService.TableTennisTourneyPayments(clubDomain, domain, startDate, endDate, textSearch, searchType, eventNames, null, false, int.MaxValue, 0, ref total);

    //        if (items != null)
    //        {
    //            TotalPayments = items.Sum(t => t.TotalPrice);

    //            foreach (var item in items)
    //            {
    //                Payments.Add(new TableTennisTourneyPaymentItemViewModel(item));
    //            }
    //        }
    //    }

    //}

    //public class TableTennisTourneyPaymentItemViewModel
    //{
    //    public bool IsChecked { get; set; }

    //    public string FullName { get; set; }

    //    public string UsTTId { get; set; }

    //    public string UsTTRating { get; set; }

    //    public string UsTTExpiration { get; set; }

    //    public string Date { get; set; }

    //    public string EventName { get; set; }

    //    public string Discounts { get; set; }

    //    public string Partner { get; set; }

    //    public string ClubAffiliation { get; set; }

    //    public string TotalPrice { get; set; }

    //    public int ProfileId { get; set; }

    //    public string State { get; set; }

    //    public GenderCategories Gender { get; set; }

    //    public string UserEmail { get; set; }

    //    public string PlayerEmail { get; set; }

    //    public TableTennisTourneyPaymentItemViewModel()
    //    {

    //    }

    //    public TableTennisTourneyPaymentItemViewModel(TableTennisTourneyPaymentItem item)
    //        : this()
    //    {
    //        FullName = string.Format("{0} {1}", item.FirstName, item.LastName);

    //        // loading sport profile xml string => object
    //        if (item.SportProfile != null)
    //        {
    //            TableTennisProfileInfoViewModel sportProfile = SerializeHelper.Deserialize<TableTennisProfileInfoViewModel>(item.SportProfile.SportInfo);

    //            UsTTId = sportProfile.UsTTId;

    //            UsTTRating = sportProfile.UsTTRating;

    //            UsTTExpiration = sportProfile.ExpirationDate.HasValue ? sportProfile.ExpirationDate.Value.FormatDateSeparateWithComma() : string.Empty;
    //        }

    //        EventName = item.Events;

    //        Date = item.Date.FormatDateSeparateWithComma();

    //        Discounts = Utilities.DisplayCampDiscounts(item.Discounts);

    //        Partner = item.Partner;

    //        ClubAffiliation = item.ClubAffiliation;

    //        TotalPrice = Utilities.FormatCurrencyWithoutPenny(item.TotalPrice);

    //        ProfileId = item.ProfileId;

    //        State = item.State;

    //        Gender = item.Gender;
    //    }
    //}

    //public class TableTennisTourneyPaymentSearchViewModel
    //{
    //    public string ClubDomain { get; set; }

    //    public string Domain { get; set; }

    //    [Display(Name = "Search:")]
    //    public string SearchText { get; set; }

    //    [Display(Name = "Type:")]
    //    public SportsClub.Db.OrderDb.TableTennisTourneyPaymentSearchOption SearchTypes { get; set; }

    //    [Display(Name = "Start Date:")]
    //    public DateTime? StartDate { get; set; }

    //    [Display(Name = "End Date:")]
    //    public DateTime? EndDate { get; set; }

    //    [Display(Name = "Event:")]
    //    public List<SelectListItem> Events { get; set; }

    //    public List<string> EventNames { get; set; }

    //    public TableTennisTourneyPaymentSearchViewModel()
    //    {
    //        Events = new List<SelectListItem>();
    //    }

    //    public TableTennisTourneyPaymentSearchViewModel(string clubDomain, string domain)
    //    {
    //        ClubDomain = clubDomain;
    //        Domain = domain;

    //        SeedEvents();
    //    }

    //    void SeedEvents()
    //    {
    //        Events = new List<SelectListItem>();

    //        var _ttTourneyEvent = DependencyResolverHelper.ITTTourneyDbService.GetEvents(ClubDomain, Domain).ToList();

    //        //Add event names to Events Property
    //        foreach (var item in _ttTourneyEvent)
    //        {
    //            Events.Add(new SelectListItem { Value = item.Id.ToString(), Text = item.EventName });
    //        }
    //    }
    //}
    #endregion

    public class OrderConfirmationViewModel
    {
        public List<CartOrderViewModel> Orders { get; set; }

        public RegisterPlayerLightViewModel RegisterPlayerModel { get; set; }

        public string ConfirmationID { get; set; }

        public DateTime OrderDate { get; set; }

        public OrderConfirmationMode OrderMode { get; set; }

        public RoleCategories Role { get; set; }

        public decimal TotalAmount
        {
            get
            {
                // this change to sum of paid amount
                return Orders.Sum(p => p.PaymentMethod == PaymentMethod.Cash ? p.CalculateOrderWithoutNegetiveAmount : p.PaidAmount);
            }
        }
    }

    public class OrderCancelConfirmationViewModel
    {
        public Order Order { get; set; }
        public int OrderID { get; set; }
    }

    public class DetailedOrderEntryViewModel
    {
        public UserProfile UserProfile { get; set; }
        public PlayerProfileViewModel Player { get; set; }

        public string ClubDomain { get; set; }

        public string Domain { get; set; }

        //public SportCategories Sport { get; set; }
        public int CategoryId { get; set; }
    }

    //public class CampDetailedOrderEntryViewModel : DetailedOrderEntryViewModel
    //{
    //    public int ScheduleNo { get; set; }

    //    public bool IsClubReport { get; set; }
    //}

    //public class ChessCampDetailedOrderEntryViewModel : CampDetailedOrderEntryViewModel
    //{
    //    public ChessProfileInfoViewModel SportProfile { get; set; }
    //}

    //public class TableTennisCampDetailedOrderEntryViewModel : CampDetailedOrderEntryViewModel
    //{
    //    public TableTennisProfileInfoViewModel SportProfile { get; set; }
    //}

    public class CourseDetailedOrderEntryViewModel : DetailedOrderEntryViewModel
    {
        public SkillViewModel Skill { get; set; }
    }

    public class ChessCourseDetailedOrderEntryViewModel : CourseDetailedOrderEntryViewModel
    {

    }

    //public class TableTennisCourseDetailedOrderEntryViewModel : CourseDetailedOrderEntryViewModel
    //{

    //}

    public class WrestlingCourseDetailedOrderEntryViewModel : CourseDetailedOrderEntryViewModel
    {
    }

    //public class TourneyDetailedOrderEntryViewModel : DetailedOrderEntryViewModel
    //{
    //    public ChessProfileInfoViewModel SportProfile { get; set; }
    //}

    //Display Order Detailed of Camps & Courses
    public class PlayerOrderedDetailedViewModel : BaseViewModel<Domain.Order>
    {
        public PlayerOrderedDetailedViewModel()
        {
            SportProfile = new SportProfileViewModel();
            Player = new EventBaseInfoMetaData();
            JumbulaAdminFields = new Models.FormModels.FormViewModel();
            ClubAdminFields = new Models.FormModels.FormViewModel();
            FollowUpForms = new List<PlayerFormsViewModel>();
        }

        //Get it from PlayerProfile.Contact
        public string PlayerProfileFullName { get; set; }

        //Get it from PlayerProfile.Contact
        public string Nickname { get; set; }

        //Get it from PlayerProfile.Contact
        public string PlayerProfileEmail { get; set; }

        public string PlayerAvatar { get; set; }

        public SportProfileViewModel SportProfile { get; set; }

        public EventBaseInfoMetaData Player { get; set; }

        public Models.FormModels.FormViewModel JumbulaAdminFields { get; set; }

        public Models.FormModels.FormViewModel ClubAdminFields { get; set; }

        [UIHint("FollowUpForms")]
        public List<PlayerFormsViewModel> FollowUpForms { get; set; }

        public JbForm JbForm { get; set; }


    }

    //public class TourneyOrderPlayerDetailViewModel
    //{
    //    public TourneyOrderPlayerDetailViewModel()
    //    {
    //        SportProfile = new SportProfileViewModel();
    //        Player = new PalyerViewModel();
    //    }

    //    //Get it from PlayerProfile.Contact
    //    public string PlayerProfileFullName { get; set; }

    //    //Get it from PlayerProfile.Contact
    //    public string PlayerProfileEmail { get; set; }

    //    public SportProfileViewModel SportProfile { get; set; }

    //    public PalyerViewModel Player { get; set; }

    //    public class PalyerViewModel
    //    {
    //        public string Avatar { get; set; }

    //        //Get it from EmergrncyContact
    //        public string FullName { get; set; }

    //        public string CellPhone { get; set; }

    //        public string WorkPhone { get; set; }

    //        public string HomePhone { get; set; }

    //        //Get it from EmergrncyContact
    //        public string Email { get; set; }

    //        public bool IsAdult { get; set; }

    //        public string Address { get; set; }
    //    }
    //}

    public class SportProfileViewModel
    {
        public string Name { get; set; }

        public string UscfId { get; set; }

        public string UscfRegularRating { get; set; }

        public DateTime? UscfExpirationDate { get; set; }

        //public SportCategories Sport { get; set; }

        public int CategoryId { get; set; }

        public Skill Skill { get; set; }

    }

    public class TableTennisTourneyDetailedOrderEntryViewModel : DetailedOrderEntryViewModel
    {
        public TableTennisProfileInfoViewModel SportProfile { get; set; }
    }

    public class CouponOrderItemViewModel
    {
        public int OrderId { get; set; }

        public string RegisteredUser { get; set; }

        public string Date { get; set; }

        public decimal Amount { get; set; }

        public string EventName { get; set; }

        public SubDomainCategories EventType { get; set; }

        public string ConfirmationNumber { get; set; }
    }


    public class CartOrderViewModel : BaseViewModel<Domain.Order>
    {
        public RegistrationType RegistrationType { get; set; }
        private decimal _totalAmount;

        public int Id { get; set; }
        public int UserId { get; set; }

        public int CardSecurityCode { get; set; }
        public OrderMode OrderMode { get; set; }

        public string Name { get; set; }
        public string FullName
        {
            get
            {
                return string.Format("{0} {1}", FirstName, LastName);
            }
        }
        public string Domain { get; set; }

        public string SubDomain { get; set; }

        public string SeasonDomain { get; set; }

        // if you need just in time Calculate OrderAmount Use this property 
        public decimal CalculateOrderAmount
        {
            get { return TotalAmount; }

        }

        // just int time calc orderAmount ignore negetive
        public decimal CalculateOrderWithoutNegetiveAmount
        {
            get
            {
                var amount = TotalAmount;
                if (amount < 0)
                {
                    amount = 0;
                    return amount;
                }
                else
                {
                    if (OrderInstallments != null && OrderInstallments.Any())
                    {

                        return OrderInstallments.First().Amount;
                    }
                }
                return amount;
            }

        }

        // amount of order

        // [Range(0, int.MaxValue)] 
        public decimal PaidAmount { get; set; }

        // amount paid by payment system, should be the same as the OrderAmount

        // Payment that user should pay now(If payment plan is installment, payable amount if different with total amount otherwise they will have same value)
        public decimal PayableAmount
        {

            get
            {
                if (this.PaymentPlanType == SportsClub.Domain.PaymentPlanType.FullPaid)
                {
                    return TotalAmount;
                }
                if (OrderInstallments != null && OrderInstallments.Any())
                {
                    return OrderInstallments.First().Amount;
                }

                return 0;
            }
        }

        public decimal TotalAmount
        {
            get
            {
                //decimal price = Charges.Where(charge => charge.EditedStatus != EditedOrderMemberStatus.NotChanged).Sum(charge => charge.Amount);
                // add general charges
                decimal price =
                    OrderCharges.Where(charge => charge.Subcategory == ChargeDiscountSubcategory.Charge)
                        .Sum(m => m.Amount);

                // min general discount
                //price = Discounts.Where(discount => discount.EditedStatus != EditedOrderMemberStatus.NotChanged).Aggregate(price, (current, discount) => current - discount.Amount);
                price =
                    OrderCharges.Where(
                        discount => discount.Subcategory == ChargeDiscountSubcategory.Discount)
                        .Aggregate(price, (current, discount) => current - discount.Amount);

                // add all the charges
                foreach (var item in OrderItems)
                {
                    // if item has canceled already, 
                    if (item.PaymentStatus == PaymentStatusCategories.canceled)
                    {
                        continue;
                    }

                    // skip canceled item
                    if (item.EditedStatus == EditedOrderMemberStatus.NotChanged)
                    {
                        continue;
                    }
                    // just charges assign to orderItems
                    foreach (var charge in item.OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Charge))
                    {
                        if (charge.Category == ChargeDiscountCategories.EF && charge.EditedStatus == EditedOrderMemberStatus.IsCanceled)
                        {
                            price -= charge.Amount;
                        }
                        else
                        {
                            price += charge.Amount;
                        }
                    }

                    foreach (var discount in item.OrderCharges.Where(m => m.Subcategory == ChargeDiscountSubcategory.Discount))
                    {
                        if (discount.EditedStatus == EditedOrderMemberStatus.IsCanceled)
                        {
                            price += discount.Amount;
                        }
                        else
                        {
                            price -= discount.Amount;
                        }
                    }
                }

                return price;
            }
        }

        [Required]
        public bool UserAnanymous { get; set; }

        // User status at the time he placed the ordoer

        [Required]
        [StringLength(128)]
        public string OrderId { get; set; }


        [Required]
        [StringLength(64)]
        public string ConfirmationId { get; set; }

        // for player reference, uniqueness in question?

        [StringLength(128)]
        public string TransactionId { get; set; }

        [StringLength(128)]
        public string TransactionMessage { get; set; }

        // optional field to capture any messages from payment gateway

        [StringLength(128)]
        public string TimeStamp { get; set; }

        [StringLength(32)]
        public string Currency { get; set; }

        [StringLength(128)]
        public string CardFirstName { get; set; }

        // Name on the card may be different than player

        [StringLength(128)]
        public string CardLastName { get; set; }

        // Name on the card may be different than player

        [StringLength(128)]
        public string CardEmail { get; set; }

        [StringLength(128)]
        public string CardNumber { get; set; }

        public int? PlayerId { get; set; }

        [Required]
        [StringLength(128)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(128)]
        public string LastName { get; set; }

        public DateTime OrderDate { get; set; } // time order was placed 

        public DateTime OrderPaidDate { get; set; } // time order was paid for

        public DateTime CardExpireDate { get; set; }

        public PaymentMethod PaymentMethod { get; set; }

        public PaymentStatusCategories PaymentStatus { get; set; }

        [StringLength(128)]
        public string Custom1 { get; set; }

        [StringLength(128)]
        public string Custom2 { get; set; }

        [StringLength(128)]
        public string Custom3 { get; set; }

        [StringLength(128)]
        public string Custom4 { get; set; }

        [StringLength(128)]
        public string Custom5 { get; set; }

        [StringLength(128)]
        public string PrimaryOrderEmail { get; set; }

        [StringLength(128)]
        public string SecondaryOrderEmail { get; set; }

        public string CustomFieldsMetaData { get; set; }

        public string EventBaseInfoMetaData { get; set; }

        public string ClubOwnerCustomFieldMetaData { get; set; }

        public PostalAddress BillingAddress { get; set; }

        public ICollection<OrderItem> OrderItems { get; set; }
        public ICollection<OrderInstallment> OrderInstallments { get; set; }

        //public ICollection<ChargeDiscount> Charges { get; set; }
        //public ICollection<ChargeDiscount> Discounts { get; set; }

        public List<ChargeDiscount> Charges { get; set; }
        public List<ChargeDiscount> Discounts { get; set; }

        public ICollection<SettlementReportOrders> SettlementReportOrders { get; set; }

        public SubDomainCategories SubDomainCategory { get; set; }

        [Required]
        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }

        public virtual Coupon Coupon { get; set; }

        public int? CouponId { get; set; }

        public bool IsEditedOrder { get; set; }

        public PaymentPlanType PaymentPlanType { get; set; }

        // use for new Table orderCharges
        //public List<ChargeDiscount> OrderCharges { get; set; }

        public ICollection<OrderCharges> OrderCharges { get; set; }

        #region Constructor

        public CartOrderViewModel(Order order)
        {
            BillingAddress = order.BillingAddress;
            CardEmail = order.CardEmail;
            CouponId = order.CouponId;
            //Currency = order.Currency;
            //Custom1 = order.Custom1;
            //Custom2 = order.Custom2;
            //Custom3 = order.Custom3;
            //Custom4 = order.Custom4;
            //Custom5 = order.Custom1;
            //CustomFieldsMetaData = order.CustomFieldsMetaData;
            //Discounts = order.Discounts;
            Domain = order.Domain;
            //EventBaseInfoMetaData = order.EventBaseInfoMetaData;
            //FirstName = order.FirstName;
            //LastName = order.LastName;
            CardLastName = order.CardLastName;
            IsEditedOrder = order.IsEditedOrder;
            Name = order.Name;
            OrderDate = order.OrderDate;
            OrderId = order.OrderId;
            OrderItems = order.OrderItems;
            OrderInstallments = order.OrderInstallment;
            OrderPaidDate = order.OrderPaidDate;
            PaidAmount = order.PaidAmount;
            PaymentStatus = order.PaymentStatus;
            PrimaryOrderEmail = order.PrimaryOrderEmail;
            ConfirmationId = order.ConfirmationId;
            SecondaryOrderEmail = order.SecondaryOrderEmail;
            PlayerId = order.PlayerId;
            SettlementReportOrders = order.SettlementReportOrders;
            SubDomain = order.SubDomain;
            SubDomainCategory = order.SubDomainCategory;
            TimeStamp = order.TimeStamp;
            TransactionId = order.TransactionId;
            TransactionMessage = order.TransactionMessage;
            UserId = order.UserId;
            UserAnanymous = order.UserAnanymous;
            //Charges = order.Charges;
            //ClubOwnerCustomFieldMetaData = order.ClubOwnerCustomFieldMetaData;
            CategoryId = order.CategoryId;
            PaymentMethod = order.PaymentMethod;
            CardExpireDate = order.CardExpireDate;
            CardNumber = order.CardNumber;
            CardSecurityCode = order.CardSecurityCode;
            CardFirstName = order.CardFirstName;
            Id = order.Id;
            PaymentPlanType = order.PaymentPlanType;
            OrderCharges = order.OrderCharges;
            SeasonDomain = order.SeasonDomain;
        }

        public CartOrderViewModel()
        {
            Charges = new List<ChargeDiscount>();
            Discounts = new List<ChargeDiscount>();
            OrderItems = new Collection<OrderItem>();
            OrderInstallments = new Collection<OrderInstallment>();
            SettlementReportOrders = new Collection<SettlementReportOrders>();
            OrderCharges = new HashSet<OrderCharges>();
        }

        #endregion

        #region Methods

        public decimal CalcCartOrderAmount
        {
            get
            {
                if (OrderInstallments != null && OrderInstallments.Any())
                {
                    return this.PayableAmount;
                }

                return TotalAmount;
            }
        }

        public decimal CalcCartOrderAmountWithoutNegativeAmount
        {
            get
            {
                return CalculateOrderWithoutNegetiveAmount;
            }
        }

        public Order ToOrder()
        {

            return new Order
            {
                BillingAddress = BillingAddress,
                CardEmail = CardEmail,
                CouponId = CouponId,
                //Currency = Currency,
                //Custom1 = Custom1,
                //Custom2 = Custom2,
                //Custom3 = Custom3,
                //Custom4 = Custom4,
                //Custom5 = Custom1,
                //CustomFieldsMetaData = CustomFieldsMetaData,
                //Discounts = Discounts,
                Domain = Domain,
                //EventBaseInfoMetaData = EventBaseInfoMetaData,
                //FirstName = FirstName,
                //LastName = LastName,
                CardLastName = CardLastName,
                IsEditedOrder = IsEditedOrder,
                Name = Name,
                OrderAmount = TotalAmount,
                OrderDate = OrderDate,
                OrderId = OrderId,
                OrderItems = OrderItems,
                OrderPaidDate = OrderPaidDate,
                PaidAmount = PaidAmount,
                PaymentStatus = PaymentStatus,
                //  Player = Player,
                PrimaryOrderEmail = PrimaryOrderEmail,
                ConfirmationId = ConfirmationId,
                SecondaryOrderEmail = SecondaryOrderEmail,
                PlayerId = PlayerId,
                SettlementReportOrders = SettlementReportOrders,
                SubDomain = SubDomain,
                SubDomainCategory = SubDomainCategory,
                TimeStamp = TimeStamp,
                TransactionId = TransactionId,
                TransactionMessage = TransactionMessage,
                UserId = UserId,
                UserAnanymous = UserAnanymous,
                //Charges = Charges,
                //ClubOwnerCustomFieldMetaData = ClubOwnerCustomFieldMetaData,
                CategoryId = CategoryId,
                PaymentMethod = PaymentMethod,
                CardExpireDate = CardExpireDate,
                CardNumber = CardNumber,
                CardSecurityCode = CardSecurityCode,
                CardFirstName = CardFirstName,
                Id = Id,
                PaymentPlanType = PaymentPlanType,
                OrderInstallment = OrderInstallments,
                OrderCharges = OrderCharges
            };
        }


        #endregion

        public JbForm JbForm { get; set; }

    }

    #region CalculationModels
    public class CalculatedTableTennisTourneyViewModel
    {
        public CalculatedTableTennisTourneyViewModel()
        {
            Charges = new List<ChargeDiscount>();
            Discounts = new List<ChargeDiscount>();
        }

        public string OptionsRequested { get; set; }

        public ICollection<ChargeDiscount> Charges { get; set; }

        public ICollection<ChargeDiscount> Discounts { get; set; }

        public List<RegisterTTTourneyEventViewModels> SelectedProgram { get; set; }

        public decimal Donation { get; set; }

        public ChargeDiscount DonationCharge { get; set; }

        public decimal OrderItemAmount { get; set; }

        public decimal Total
        {
            get
            {
                return OrderItemAmount + Donation;
            }
        }
    }


    public class CalculatedChessTourneyViewModel
    {
        public CalculatedChessTourneyViewModel()
        {
            Charges = new List<ChargeDiscount>();
            Discounts = new List<ChargeDiscount>();
        }

        public string OptionsRequested { get; set; }

        public ICollection<ChargeDiscount> Charges { get; set; }

        public ICollection<ChargeDiscount> Discounts { get; set; }

        public decimal Donation { get; set; }

        public ChargeDiscount DonationCharge { get; set; }

        public decimal OrderItemAmount { get; set; }

        public decimal Total
        {
            get
            {
                return OrderItemAmount + Donation;
            }
        }
    }

    public class CalculatedCourseViewModel
    {
        public CalculatedCourseViewModel()
        {
            Charges = new List<ChargeDiscount>();
            Discounts = new List<ChargeDiscount>();
        }

        public List<ChargeDiscount> Charges { get; set; }

        public List<ChargeDiscount> Discounts { get; set; }

        public decimal Donation { get; set; }

        public ChargeDiscount DonationCharge { get; set; }

        public decimal OrderItemAmount { get; set; }

        public decimal Total
        {
            get
            {
                return OrderItemAmount + Donation;
            }
        }
    }

    //public class CalculatedCampOrderViewModel
    //{
    //    public CalculatedCampOrderViewModel()
    //    {
    //        RegisteredSections = new List<RegisterCampSectionModel>();
    //    }

    //    public List<RegisterCampSectionModel> RegisteredSections { get; set; }

    //    public decimal MultipleWeek { get; set; }

    //    public decimal Donation { get; set; }

    //    public ChargeDiscount DonationCharge { get; set; }

    //    public ChargeDiscount MultipleWeekDicount { get; set; }

    //    public decimal Total
    //    {
    //        get
    //        {
    //            decimal amount = RegisteredSections.Sum(item => item.Total);

    //            amount = amount + Donation - MultipleWeek;

    //            return amount;
    //        }

    //    }

    //}

    public class CheckoutBoxViewModel
    {
        public CheckoutBoxViewModel()
        {
            Sections = new List<Section>();
            Root = new List<RegisterClientViewModel>();
        }

        public IList<Section> Sections { get; set; }
        public IList<RegisterClientViewModel> Root { get; set; }

        public decimal Total { get; set; }
    }

    public class Section
    {
        public Section()
        {
            RegisterClient = new List<RegisterClientViewModel>();
        }

        public string SectionName { get; set; }

        public IList<RegisterClientViewModel> RegisterClient { get; set; }
    }

    public class RegisterClientViewModel
    {
        public ChargOrDiscount Type { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Amount { get; set; }

    }

    public enum ChargOrDiscount
    {
        Charge,
        Discount
    }

    public class PaymentPlanMetaData
    {
        public int OrderId { get; set; }
        public PaymentPlanType PaymentPlanType { get; set; }
        public List<DueDate> DueDates { get; set; }

        public PaymentPlanMetaData()
        {
            DueDates = new List<DueDate>();
        }
    }

    public class DueDate
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public DateTime? PaidDate { get; set; }
        public PaymentPlanStatus PaymentPlanStatus { get; set; }
        public decimal? Amount { get; set; }
        public decimal? PaidAmount { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public bool NoticeSent { get; set; }
    }

    public enum PaymentPlanStatus
    {
        [Description("Paid")]
        Paid = 0,

        [Description("Unpaid")]
        UnPaid = 1,
    }

    public class OrdersInstallmentViewModel
    {
        public int Id { get; set; }

        public List<string> DueDates { get; set; }


    }

    #endregion


}