﻿using Jumbula.Common.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Mvc.UIHelpers;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SchoolSeasonGeneralSettingViewModel : BaseViewModel<SchoolSeasonGeneralSetting>
    {
        public SchoolSeasonGeneralSettingViewModel()
        {
            SeasonStageList = DropdownHelpers.ToSelectList<SeasonStage>();
        }

        public DateTime? MakeUpOpeningDate { get; set; }
        public DateTime? MakeUpClosingDate { get; set; }
        public bool AMClassesPossible { get; set; }
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? AMClassesStartTime { get; set; }
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? PMClassesStartTime { get; set; }
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? PMClassesStartTimeEarlyDismissal { get; set; }
        public string SelectedSeasonStage { get; set; }
        public List<SelectKeyValue<string>> SeasonStageList { get; set; }
    }

    public class SchoolSeasonDetailSettingViewModel : BaseViewModel<SchoolSeasonDetailSetting>
    {
        public SchoolSeasonDetailSettingViewModel()
        {
            AllBackpackFlierDates = new List<SelectKeyValue<string>>();
            BackpackFlierDates = new List<string>();

            AllEmailBlastDates = new List<SelectKeyValue<string>>();
            EmailBlastDates = new List<string>();
        }
        public List<SelectKeyValue<string>> AllBackpackFlierDates { get; set; }
        public List<string> BackpackFlierDates { get; set; }
        public List<SelectKeyValue<string>> AllEmailBlastDates { get; set; }
        public List<string> EmailBlastDates { get; set; }
        
        public DateTime? InstructorAssignmentDeadline { get; set; }
        public DateTime? InstructorListDelivery { get; set; }
        public DateTime? RostersDue { get; set; }
        public DateTime? PaymentsIssued { get; set; }
        public DateTime? SurveyOpens { get; set; }
        public DateTime? SurveyCloses { get; set; }
        public DateTime? SurveyResultsDue { get; set; }
    }

    public class SchoolSeasonscheduleSettingViewModel : BaseViewModel<SchoolSeasonscheduleSetting>
    {

        public SchoolSeasonscheduleSettingViewModel()
        {
            AllMondayDates = new List<SelectKeyValue<string>>();
            MondayDates = new List<string>();

            AllTuesdayDates = new List<SelectKeyValue<string>>();
            TuesdayDates = new List<string>();

            AllWednesdayDates = new List<SelectKeyValue<string>>();
            WednesdayDates = new List<string>();

            AllThursdayDates = new List<SelectKeyValue<string>>();
            ThursdayDates = new List<string>();

            AllFridayDates = new List<SelectKeyValue<string>>();
            FridayDates = new List<string>();

            AllNoClassDates = new List<SelectKeyValue<string>>();
            NoClassDates = new List<string>();
        }
        public bool Monday { get; set; }
        [Display(Name = "total day count during season")]
        public int MondayNumberOfClasses { get; set; }
        public List<SelectKeyValue<string>> AllMondayDates { get; set; }
        public List<string> MondayDates { get; set; }

        public bool Tuesday { get; set; }
        [Display(Name = "total day count during season")]
        public int TuesdayNumberOfClasses { get; set; }
        public List<SelectKeyValue<string>> AllTuesdayDates { get; set; }
        public List<string> TuesdayDates { get; set; }

        public bool Wednesday { get; set; }
        [Display(Name = "total day count during season")]
        public int WednesdayNumberOfClasses { get; set; }
        public List<SelectKeyValue<string>> AllWednesdayDates { get; set; }
        public List<string> WednesdayDates { get; set; }

        public bool Thursday { get; set; }
        [Display(Name = "total day count during season")]
        public int ThursdayNumberOfClasses { get; set; }
        public List<SelectKeyValue<string>> AllThursdayDates { get; set; }
        public List<string> ThursdayDates { get; set; }

        public bool Friday { get; set; }
        [Display(Name = "total day count during season")]
        public int FridayNumberOfClasses { get; set; }
        public List<SelectKeyValue<string>> AllFridayDates { get; set; }
        public List<string> FridayDates { get; set; }

        public List<SelectKeyValue<string>> AllNoClassDates { get; set; }
        public List<string> NoClassDates { get; set; }
    }

    public class SeasonProgramInfoSettingViewModel : BaseViewModel<SeasonProgramInfoSetting>
    {
        [Display(Name = "start date")]
        public DateTime? StartDate { get; set; }
        [Display(Name = "end date")]
        public DateTime? EndDate { get; set; }

        public bool HasEarlyRegistration { get; set; }

        [Display(Name = "early registration open date")]
        public DateTime? EarlyRegistrationOpenDate { get; set; }
        
        [JsonConverter(typeof(CustomTimeConverter))]
        [Display(Name = "early registration open time")]
        public DateTime? EarlyRegistrationOpenTime { get; set; }

        [Display(Name = "early registration close date")]
        public DateTime? EarlyRegistrationCloseDate { get; set; }

        [JsonConverter(typeof(CustomTimeConverter))]
        [Display(Name = "early registration close time")]
        public DateTime? EarlyRegistrationCloseTime { get; set; }

        public bool HasEarlyRegisterPIN { get; set; }

        [MaxLength(64)]
        [Display(Name = "password")]
        public string EarlyRegisterPIN { get; set; }

        [MaxLength(1024)]
        [Display(Name = "password message")]
        public string EarlyRegisterPINMessage { get; set; }


        //public bool HasGeneralRegistration { get; set; }
        [Display(Name = "general registration open date")]
        public DateTime? GeneralRegistrationOpenDate { get; set; }

        [JsonConverter(typeof(CustomTimeConverter))]
        [Display(Name = "general registration open time")]
        public DateTime? GeneralRegistrationOpenTime { get; set; }

        [Display(Name = "general registration close date")]
        public DateTime? GeneralRegistrationCloseDate { get; set; }

        [JsonConverter(typeof(CustomTimeConverter))]
        [Display(Name = "general registration close time")]
        public DateTime? GeneralRegistrationCloseTime { get; set; }

        public bool HasGeneralRegisterPIN { get; set; }

        [MaxLength(64)]
        [Display(Name = "password")]
        public string GeneralRegisterPIN { get; set; }

        [MaxLength(1024)]
        [Display(Name = "password message")]
        public string GeneralRegisterPINMessage { get; set; }

        public bool HasLateRegistration { get; set; }

        [Display(Name = "late registration open date")]
        public DateTime? LateRegistrationOpenDate { get; set; }

        [JsonConverter(typeof(CustomTimeConverter))]
        [Display(Name = "late registration open time")]
        public DateTime? LateRegistrationOpenTime { get; set; }

        [Display(Name = "late registration close date")]
        public DateTime? LateRegistrationCloseDate { get; set; }

        [JsonConverter(typeof(CustomTimeConverter))]
        [Display(Name = "late registration close time")]
        public DateTime? LateRegistrationCloseTime { get; set; }

        public bool EnableLateRegistrationFee { get; set; }

        [DisplayName("late registration fee")]
        [Required(ErrorMessage = "{0} is required.")]
        public decimal LateRegistrationFee { get; set; }

        [MaxLength(256, ErrorMessage = "{0} cannot be longer than 256 characters.")]
        [Display(Name = "late registration fee message")]
        public string LateRegistrationFeeMessage { get; set; }

        public bool HasLateRegisterPIN { get; set; }

        [MaxLength(64)]
        [Display(Name = "password")]
        public string LateRegisterPIN { get; set; }

        [MaxLength(1024)]
        [Display(Name = "password message")]
        public string LateRegisterPINMessage { get; set; }


        public bool HasPreRegistration { get; set; }

        

        [Display(Name = "pre registration open date")]
        public DateTime? PreRegistrationOpenDate { get; set; }

        [JsonConverter(typeof(CustomTimeConverter))]
        [Display(Name = "pre registration open time")]
        public DateTime? PreRegistrationOpenTime { get; set; }

        [Display(Name = "pre registration close date")]
        public DateTime? PreRegistrationCloseDate { get; set; }

        [JsonConverter(typeof(CustomTimeConverter))]
        [Display(Name = "pre registration close time")]
        public DateTime? PreRegistrationCloseTime { get; set; }



        public bool HasLottery { get; set; }
        [Display(Name = "lottery open date")]
        public DateTime? LotteryOpenDate { get; set; }

        [JsonConverter(typeof(CustomTimeConverter))]
        [Display(Name = "lottery open time")]
        public DateTime? LotteryOpenTime { get; set; }

        [Display(Name = "lottery close date")]
        public DateTime? LotteryCloseDate { get; set; }

        [JsonConverter(typeof(CustomTimeConverter))]
        [Display(Name = "lottery close time")]
        public DateTime? LotteryCloseTime { get; set; }





        [Display(Name = "miscellaneous information rosters due")]
        public DateTime? MiscellaneousInformationRostersDueDate { get; set; }
        [Display(Name = "miscellaneous information payments issued")]
        public DateTime? MiscellaneousInformationPaymentsIssuedDate  { get; set; }
    }

    public class SchoolSettingSeasonFormsViewModel : BaseViewModel<SeasonFormsSetting>
    {
        public List<SelectListItem> Forms { get; set; }

        public string SelectedForm { get; set; }

        public List<SelectListItem> Waivers { get; set; }

        public List<string> SelectedWaivers { get; set; }
    }

    public class SchoolSettingSeasonAdditionalViewModel : BaseViewModel<SeasonProgramInfoSetting>
    {
        public bool IsWaitListAvailable { get; set; }

        public string WaitlistPolicy { get; set; }

        public bool ShowRemainingSpots { get; set; }

        public int CapacityLeftNumbers { get; set; }
    }

    public class SeasonPaymentPlanViewModel : BaseViewModel<SeasonProgramInfoSetting>
    {
        public List<SelectKeyValue<string>> PaymentPlanList { get; set; }

        public List<string> SelectedPaymentPlans { get; set; }

        public long SeasonId { get; set; }

    }
}
