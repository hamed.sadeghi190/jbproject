﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('campaignAddStep4controller', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'campaigns-add-step3';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + "/Dashboard/Campaign";

        $scope.campaignId = $scope.MC.stateParams.campaignId;

        $scope.Model = {};

        $http.get(baseUrl + '/CampaignAddEditStep4', { params: { campaignId: $scope.campaignId } })
            .success(function (data, status, headers, config) {
                $scope.Model = data;
                if ($scope.Model.TemplateNames && $scope.Model.SelectedTemplateId != 0) {
                    $http.get(baseUrl + '/GetTemplate', { params: { id: $scope.Model.SelectedTemplateId } })
                        .success(function (data, stats, headers, config) {
                            $scope.Model.MailTemplate = data;
                            if ($scope.Model.MailTemplate.Name == "") {
                                $scope.Model.MailTemplate.ChangeTemplate = true;
                            } else {
                                $scope.Model.MailTemplate.ChangeTemplate = false;
                            }
                        });
                }
            });

        $scope.saveStep4 = function () {
            $http.post(baseUrl + '/CampaignAddEditStep4', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $state.go('EmailCampaigns');
                    }
                    else if (data.Status == false && data.Message == "You do not have recipients email.") {
                        $scope.MC.handleErrors($scope, data);
                        console.log(date.Message)
                    }
                    else{
                        $scope.MC.handleErrors($scope, data);
                        $state.go('EmailCampaigns');
                    }
                });
        }

        $scope.editRecipients = function () {
            $state.go('EmailCampaignDisplayAllRecipients', { campaignId: $scope.MC.stateParams.campaignId, campaignType: $scope.MC.stateParams.campaignType});
        }
    }]);
})();