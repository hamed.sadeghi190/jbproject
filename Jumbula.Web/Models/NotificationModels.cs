﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace SportsClub.Models
{
    public static class NotificationHelper
    {
        static NotificationItemViewModel item = new NotificationItemViewModel();

        public static void Add(string title, string content)
        {
            item = new NotificationItemViewModel { Title = title, Content = content };
        }

        public static void Add(string title, string content, NotificationItemControlViewModel actionControl)
        {
            item = new NotificationItemViewModel { Title = title, Content = content, ActionButton = actionControl };
        }

        public static NotificationItemViewModel GetNotification()
        {
            NotificationItemViewModel tempItem = null;
            if (item != null)
            {
                tempItem = item;
                item = new NotificationItemViewModel();
            }

            return tempItem;
        }
    }

    public class NotificationItemViewModel
    {

        public NotificationItemViewModel()
        {

        }
        public string Title { get; set; }

        public string Content { get; set; }

        public NotificationItemControlViewModel ActionButton { get; set; }

        public bool HasValue
        {
            get
            {
                return (string.IsNullOrEmpty(Title) && string.IsNullOrEmpty(Content) ? false : true);
            }
        }
    }

    public class NotificationItemControlViewModel
    {
        public NotificationItemControlViewModel()
        {

        }
        public NotificationItemControlViewModel(string text, string actionname, string controller)
        {
            Text = text;
            ActionName = actionname;
            Controller = controller;
        }

        public NotificationItemControlViewModel(string text, string actionname, string controller, object routeValues)
        {
            Text = text;
            ActionName = actionname;
            Controller = controller;
            RouteValues = routeValues;
        }

        public string Text { get; set; }

        public string ActionName { get; set; }

        public string Controller { get; set; }

        public object RouteValues { get; set; }
    }
}