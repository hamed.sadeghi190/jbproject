﻿using System.Data.Entity.ModelConfiguration;
using Jumbula.Core.Domain;

namespace Jumbula.Data.DbConfigs
{
    public class OrderSessionConfig : EntityTypeConfiguration<OrderSession>
    {
        public OrderSessionConfig()
        {
            HasRequired(i => i.OrderItem).WithMany(c => c.OrderSessions).HasForeignKey(i => i.OrderItemId);
        }
    }
}
