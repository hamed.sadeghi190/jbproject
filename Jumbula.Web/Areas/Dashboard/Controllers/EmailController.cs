﻿using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Model.Email;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.WebConfig;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Jumbula.Common.Constants;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class EmailController : DashboardBaseController
    {
        #region Constants

        private const string AttachmentFolder = "EmailAttachments";
        #endregion

        #region Field
        private readonly IEmailBusiness _emailBusiness;
        private readonly IEmailTemplateBusiness _emailTemplateBusiness;
        private readonly IFileBusiness _fileBusiness;
        #endregion

        #region Constractor
        public EmailController(IEmailBusiness emailBusiness, IEmailTemplateBusiness emailTemplateBusiness, IFileBusiness fileBusiness)
        {
            _emailBusiness = emailBusiness;
            _emailTemplateBusiness = emailTemplateBusiness;
            _fileBusiness = fileBusiness;
        }
        #endregion

        #region Public Methods
        [HttpPost]
        public JsonNetResult SendEmail(EmailViewModel email)
        {
            CheckSendEmailValidation(email);

            if (!ModelState.IsValid) return JsonNet(false);

            if (email.HasTemplate)
            {
                var result = _emailTemplateBusiness.SaveTemplate(email.MailTemplate, ActiveClub.Id, email.TemplateId);
                email.TemplateId = int.Parse(result.Data.ToString());
            }
            else
                email.TemplateId = null;

            _emailBusiness.Send(email);

            return JsonNet(new OperationStatus { Status = true });
        }

        [HttpPost]
        public JsonNetResult UploadFile(IEnumerable<HttpPostedFileBase> model)
        {
            var attachmentFolderAddress = $"{ActiveClub.Domain}\\{AttachmentFolder}";
            var data = new List<AttachmentModel>();

            var models = model as HttpPostedFileBase[] ?? model.ToArray();
            foreach (var item in models)
            {
                using (var binaryReader = new BinaryReader(item.InputStream))
                {
                    var storageManager = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);

                    var contentType = string.Format(Constants.F_AttachmentMemeType, Path.GetExtension(item.FileName)?.Substring(1));

                    Stream fileStream = new MemoryStream(binaryReader.ReadBytes(item.ContentLength));

                    var newName = $"{Guid.NewGuid()}.{Path.GetExtension(item.FileName)?.Substring(1)}";

                    storageManager.UploadBlob(attachmentFolderAddress, newName, fileStream, contentType);

                    data.Add(new AttachmentModel { FileName = item.FileName, FileSize = item.ContentLength, FileUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, attachmentFolderAddress, newName).AbsoluteUri });

                    fileStream.Close();
                }
            }

            return JsonNet(new OperationStatus { Status = true, Data = data });
        }

        [HttpGet]
        public JsonNetResult GetEmailTemplate(int? templateId)
        {
            return JsonNet(_emailTemplateBusiness.GetEmailTemplate(templateId));
        }

        public JsonNetResult GetLimitedExtensions()
        {
            return JsonNet(_fileBusiness.GetLimitExtensions());
        }

        [HttpPost]
        public JsonNetResult EmailPreview(EmailViewModel model)
        {
            return JsonNet(_emailBusiness.GetPreview(model));
        }

        [HttpPost]
        public JsonNetResult ValidateFiles(EmailViewModel model)
        {
            var result = CheckSendEmailValidation(model);

            if (result.Status && ModelState.IsValid)
                return JsonNet(_fileBusiness.ValidateAttachment(model.Attachments));

            if (!string.IsNullOrWhiteSpace(result.Message))
                return JsonNet(result);

            return JsonNet(false);
        }
        #endregion


        #region Validations
        private OperationStatus CheckSendEmailValidation(EmailViewModel model)
        {
            if (!string.IsNullOrWhiteSpace(model.ReplyTo) && model.ReplyTo.Trim().Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList().Count > 1)
                ModelState.AddModelError(nameof(model.ReplyTo), "Please enter only one email address.");

            if (!ModelState.ContainsKey(nameof(model.ReplyTo)) && !string.IsNullOrWhiteSpace(model.ReplyTo) && !new Regex(Validation.EmailRegex).Match(model.ReplyTo.Replace(",", "").Trim()).Success)
                ModelState.AddModelError(nameof(model.ReplyTo), "Email format is not valid.");

            if (model.HasTemplate && !model.TemplateId.HasValue && string.IsNullOrWhiteSpace(model.MailTemplate.NewName))
                ModelState.AddModelError("MailTemplate.NewName", string.Format(Validation.RequiredMessage, nameof(model.MailTemplate.NewName)));

            if (!model.Recipients.Any())
                return new OperationStatus { Status = false, Message = "Recipient list cannot be empty." };

            var ccList = !model.IsCcListEmailSet ? model.Ccs.FirstOrDefault()?.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList() : model.Ccs;
            if (ccList != null)
            {
                foreach (var item in ccList)
                {
                    if (!new Regex(Validation.EmailRegex).Match(item.Trim()).Success)
                        ModelState.AddModelError(nameof(model.Ccs), "Email format is not valid.");

                    if (model.Recipients.Any(x => x.EmailAddress.Contains(item.Trim())))
                        return new OperationStatus
                        { Status = false, Message = "Duplicate email detected between the To, Cc or Bcc list." };
                }
            }

            var bccList = !model.IsBccListEmailSet
                ? model.Bccs.FirstOrDefault()?.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList()
                : model.Bccs;
            if (bccList != null)
            {
                foreach (var item in bccList)
                {
                    if (!new Regex(Validation.EmailRegex).Match(item.Trim()).Success)
                        ModelState.AddModelError(nameof(model.Bccs), "Email format is not valid.");

                    if (model.Recipients.Any(x => x.EmailAddress.Contains(item.Trim())) ||
                        (ccList != null && ccList.Any(x => x.Contains(item.Trim()))))
                        return new OperationStatus
                        { Status = false, Message = "Duplicate email detected between the To, Cc or Bcc list." };
                }
            }

            foreach (var item in model.Recipients.Select(x => x.EmailAddress).ToList())
            {
                if (!new Regex(Validation.EmailRegex).Match(item).Success)
                    return new OperationStatus { Status = false, Message = "Email format is not valid." };
            }

            return new OperationStatus { Status = ModelState.IsValid };
        }

        #endregion

        //todo: we should be remove these methods
        [HttpGet]
        public virtual ActionResult HeaderEmail()
        {
            return View("_HeaderEmail");
        }

        [HttpGet]
        public virtual ActionResult FooterEmail()
        {
            return View("_FooterEmail");
        }
    }
}