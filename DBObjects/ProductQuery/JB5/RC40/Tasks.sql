
INSERT INTO [dbo].[ScheduleTasks]
           ([Name]
           ,[Seconds]
           ,[Type]
           ,[Enabled]
           ,[StopOnError]
           ,[LeasedByMachineName]
           ,[LeasedUntilUtc]
           ,[LastStartUtc]
           ,[LastEndUtc]
           ,[LastSuccessUtc])
     VALUES
           ('Email'
           ,60
           ,'SportsClub.Infrastructure.QueuedMessagesSendTask, SportsClub'
           ,1
           ,0
           ,null
           ,null
           ,null
           ,null
           ,null)
GO

INSERT INTO [dbo].[ScheduleTasks]
           ([Name]
           ,[Seconds]
           ,[Type]
           ,[Enabled]
           ,[StopOnError]
           ,[LeasedByMachineName]
           ,[LeasedUntilUtc]
           ,[LastStartUtc]
           ,[LastEndUtc]
           ,[LastSuccessUtc])
     VALUES
           ('Weekly agenda'
           ,300
           ,'SportsClub.Infrastructure.WeeklyAgendaTask, SportsClub'
           ,1
           ,0
           ,null
           ,null
           ,null
           ,null
           ,null)
GO


