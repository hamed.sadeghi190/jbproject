﻿namespace Jumbula.Core.Identity
{
    public interface IAuthenticationAdditionalHelpers
    {
        string GetAccessToken(string userName, int userId);
    }
}
