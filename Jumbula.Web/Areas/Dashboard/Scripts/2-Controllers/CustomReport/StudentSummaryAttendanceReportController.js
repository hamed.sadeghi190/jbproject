﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('StudentSummaryAttendanceReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.Model = customReportService.getModelFromStrorage();

        var programId = $scope.Model._SelectedPrograms;

        if (programId == 'undefined' || programId == null) {
            programId = 0;
        }

        $scope.SessionAttendanceReportDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/StudentSummaryAttendanceReportRun',
                        type: "POST",
                        data: function () {
                            return {
                                programId: programId,
                            };
                        },
                    },

                },
            schema: {
                total: "TotalCount",
                data: function (response) {

                    $scope.ProgramName = response.ProgramName;
                    return response.DataSource;
                },
            },

            serverSorting: false,
            serverPaging: true,
            pageSize: 50,
        });

        var columns = [];
        $scope.oldColumns = [];

        $scope.StudentAttendanceReportGridOptions = {

            dataSource: $scope.SessionAttendanceReportDataSource,

            sortable: true,
            scrollable: true,
            resizable: true,
            columnMenu: true,

            pageable: {
                refresh: true,
                pageSizes:
                    [
                        5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            dataBound: function (e) {
                customReportService.initializeDoubleScroll(e);

                $scope.oldColumns = $("#StudentAttendanceReportGrid").data("kendoGrid").columns;
            },
            excel: {
                allPages: true,
                fileName: "Student attendance - summary.xlsx",
                proxyURL: "/Dashboard/PortalReport/Save"
            },
            excelExport: function (e) {

                var rows = e.workbook.sheets[0].rows;
                var columns = e.workbook.sheets[0].columns;

                columns.forEach(function (column) {
                    // also delete the width if it is set
                    delete column.width;
                    column.autoWidth = true;
                });

                rows.unshift({
                    cells: [{
                        value: ""
                    }]
                });

                rows.unshift({
                    cells: [{
                        value: "Student attendance - summary report (" + $scope.ProgramName + ")"
                    }]
                });

                cfpLoadingBar.complete();
            },
        }


        $(document).bind("ajaxComplete.changeColumns", function () {


            if ($scope.oldColumns && $scope.oldColumns.length > 0) {
                for (var i = 0; i < $scope.oldColumns.length; i++) {
                    var column = [];

                    column = {
                        field: $scope.oldColumns[i].field,
                        title: $scope.oldColumns[i].field.replace(/_SLASH_/g, "/").replace(/_BRAKETOPEN_/g, "<br>(").replace(/_BRAKETOPEN_/g, "(").replace(/_BRAKETCLOSE_/g, ")").replace(/_SDASH_/g, "-").replace(/_COMMA_/g, ",").replace(/_/g, " "),
                        width: $scope.oldColumns.length == 4 ? "300px" : "250px",
                        locked: i == 0 ? true : false
                    }

                    columns.push(column);
                }
                $("#StudentAttendanceReportGrid").data("kendoGrid").setOptions({
                    columns: columns
                });
            }
            $(document).unbind("ajaxComplete.changeColumns");
        });

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#StudentAttendanceReportGrid").getKendoGrid().saveAsExcel();
        }
    }
    ]);

})();