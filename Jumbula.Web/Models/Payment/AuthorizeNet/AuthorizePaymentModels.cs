﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Web.Mvc;
using System;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.Mvc.WebConfig;

namespace SportsClub.Models
{
    public class AuthorizePaymentModels
    {
        public AuthorizePaymentModels()
        {

        }

        public string PublishableKey
        {
            get
            {
                return IsTestMode ? WebConfigHelper.Stripe_PublishableKey_Test : WebConfigHelper.Stripe_PublishableKey_Live;

            }
        }

        public bool IsTestMode { get; set; }
        public string ConfirmationId { get; set; }
        public List<string> ConfirmatiocIds { get; set; }
        public string StrConfirmatiocIds {
            get
            {
                if (ConfirmatiocIds != null && ConfirmatiocIds.Any())
                {
                    return string.Join(", ", ConfirmatiocIds);
                }
                return string.Empty;
            }
                }
        public long OrderId { get; set; }
        public int? InvoiceId { get; set; }
        public string Token { get; set; }
        
        public PaymentMethod PaymentMethod { get; set; }
        public string CreateTokenResponse { get; set; }
        public string Brand { get; set; }

        [DisplayName("Cardholder name")]
        [Required(ErrorMessage = "The cardholder name is required")]
        public string CardHolderName { get; set; }

        [Display(Name = "Card number")]
        [Required(ErrorMessage = "{0} is required")]
        [RegularExpression("[0-9]+$", ErrorMessage = "Please enter a valid card number.")]
        [StringLength(17, ErrorMessage = "{0} is too long.")]
        public string CardNumber { get; set; }
        public int? UserId { get; set; }

        [Display(Name = "Address")]
        [Required(ErrorMessage = "Address is required.")]
        public string StrAddress { get; set; }
        public virtual PostalAddress Address { get; set; }

        [Display(Name = "CVC")]
        [Required(ErrorMessage = "{0} is required")]
        [RegularExpression("[0-9]+$", ErrorMessage = "Please enter a valid CVC number.")]
        [StringLength(5, ErrorMessage = "{0} is too long.")]
        public string CVV { get; set; }

        [Display(Name = "year")]
        public string Year { get; set; }

        [Display(Name = "year")]
        public int Year2Char { get; set; }

        [Display(Name = "month")]
        [Required(ErrorMessage = "Expiration date is required")]
        public string Month { get; set; }

        public bool IsDefault { get; set; }

        public bool IsPreferrd { get; set; }
        public bool BackToDetail { get; set; }

        public CurrencyCodes Currency { get; set; }
        public decimal PayableAmount { get; set; }
        public SelectList AccountType
        {
            get
            {
                return new SelectList(DropdownHelpers.ToSelectList<AchPaymentAccountType>(), dataValueField: "Value", dataTextField: "Text", selectedValue: "");
            }
        }       
        public string SelectedAccountType { get; set; }

        public JumbulaSubSystem ActionCaller { get; set; }

        public int CreditId { get; set; }
        public string CustomerId { get; set; }
        public string CardId { get; set; }

    }


}