
ALTER TABLE [dbo].[JbPages] add [SaveType] int DEFAULT ((0))

ALTER TABLE [dbo].[JbPages] add [ClubId] int Not NULL

ALTER TABLE [dbo].[JbPages]  WITH CHECK ADD  CONSTRAINT [FK_dbo.JbPages_dbo.Clubs_ClubId] FOREIGN KEY([ClubId])
REFERENCES [dbo].[Clubs] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[JbPages] CHECK CONSTRAINT [FK_dbo.JbPages_dbo.Clubs_ClubId]
GO

ALTER TABLE [jbpages] DROP CONSTRAINT [FK_dbo.JbPages_dbo.Seasons_SeasonId]
go

alter table jbpages DROP COLUMN SeasonId
go
