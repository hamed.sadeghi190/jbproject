﻿using Jumbula.Common.Enums;
using System;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ConfirmFamilyInvoceViewModel : BaseViewModel<OrderItem, long>
    {
        public int UserId { get; set; }
        public string Attendee { get; set; }
        public DateTime OrderDate { get; set; }
        public string StrOrderDate
        {
            get
            {
                return OrderDate.ToString("MMMM dd, yyyy");
            }

        }

        public string ConfirmationId { get; set; }
        public PaymentPlanType PaymentplanType { get; set; }
        public decimal InvoiceAmount { get; set; }

       
    }

}