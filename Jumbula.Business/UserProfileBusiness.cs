﻿using Jumbula.Common.Attributes;
using Jumbula.Common.Enums;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Constants = Jumbula.Common.Constants.Constants;


namespace Jumbula.Business
{
    public class UserProfileBusiness : IUserProfileBusiness
    {
        private readonly IEntitiesContext _dataContext;

        private IOrderItemBusiness _orderItemBusiness;

        public UserProfileBusiness(IEntitiesContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IOrderItemBusiness  OrderItemBusiness
        {
            get { return _orderItemBusiness; }
            set { _orderItemBusiness = value; }
        }

        public JbRole GetRole(RoleCategory role)
        {
            return _dataContext.Set<JbRole>().SingleOrDefault(c => c.Name.Equals(role.ToString(), StringComparison.OrdinalIgnoreCase));
        }

        public OperationStatus AddUsersToRole(string username, RoleCategory role, int clubId)
        {
            var user = Get(username);
            var roleObj = _dataContext.Set<JbRole>().Single(c => c.Name == role.ToString());
            var club = _dataContext.Set<Club>().Single(c => c.Id == clubId);

            if (RoleTypeAttribute.GetRoleType(role) == RoleCategoryType.Dashboard)
            {
                club.ClubStaffs.Add(
                    new ClubStaff()
                    {
                        ClubId = clubId,
                        JbUserRole = new JbUserRole()
                        {
                            UserId = user.Id,
                            RoleId = roleObj.Id
                        }
                    });
            }
            else if (RoleTypeAttribute.GetRoleType(role) == RoleCategoryType.Parent)
            {
                user.Roles.Add(new JbUserRole()
                {
                    UserId = user.Id,
                    RoleId = roleObj.Id
                });

            }

            return Update(user);
        }

        public bool IsUserInRole(string userName, RoleCategory role)
        {
            return _dataContext.Set<JbUser>().Any(u => u.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase) && u.Roles.Select(r => r.Role.Name).Contains(role.ToString()));
        }


        public JbUser Get(int userId)
        {
            return GetList().SingleOrDefault(u => u.Id == userId);
        }

        public JbUser Get(string userName)
        {
            return GetList().SingleOrDefault(u => u.UserName.Equals(userName));
        }

        public IQueryable<JbUser> GetList()
        {
            return _dataContext.Set<JbUser>();
        }

        public IQueryable<JbUser> GetList(IEnumerable<string> userprofileUsernames)
        {
            userprofileUsernames = userprofileUsernames.Select(e => e.ToLower()).Distinct().ToList();
            return GetList().Where(e => userprofileUsernames.Contains(e.UserName.ToLower()));
        }

        public IQueryable<JbUser> GetList(IEnumerable<int> users)
        {
            return GetList().Where(e => users.Contains(e.Id));
        }

        public OperationStatus Update(JbUser userProfile)
        {
            try
            {
                _dataContext.Entry(userProfile).State = System.Data.Entity.EntityState.Modified;

                var result = _dataContext.SaveChanges();
                return new OperationStatus() { Status = result > 0 };
            }
            catch (Exception ex)
            {
                return new OperationStatus() { Status = false, Exception = ex, Message = ex.Message };
            }
        }

        public bool IsUserOwnerOfProfile(int userId, int profileId)
        {
            return _dataContext.Set<PlayerProfile>().Any(p => p.Id == profileId && p.UserId == userId);// base.Include("PlayerProfiles").FirstOrDefault(p => p.Id == userId).PlayerProfiles.FirstOrDefault(p => p.Id == profileId) != null;//.Get(userId).PlayerProfiles.FirstOrDefault(p => p.Id == profileId) != null;
        }

        public void EnsureUserIsProfileOwner(int userId, string userName, int profileId)
        {
            if (!IsUserOwnerOfProfile(userId, profileId))
            {
                throw new Exception(string.Format(Constants.PlayerProfile_UserIsNotOwnerForProfile, userName, profileId));
            }
        }

        public IQueryable<PlayerProfile> GetPlayerProfiles(int userId)
        {
            return _dataContext.Set<PlayerProfile>().Where(p => p.UserId == userId);
        }

        public IQueryable<PlayerProfile> GetPlayerProfiles(List<int> userIds)
        {
            return _dataContext.Set<PlayerProfile>().Where(p => userIds.Contains(p.UserId));
        }
        public IQueryable<PlayerProfile> GetUserParents(int userId)
        {
            return GetPlayerProfiles(userId).Where(p => p.Relationship == RelationshipType.Parent);
        }

        public OperationStatus UpdateNotBrandedToken(string userName, string token)
        {
            var result = new OperationStatus();

            var user = Get(userName);

            user.NotBrandedToken = token;

            result.Status = _dataContext.SaveChanges() > 0;

            return result;
        }

        public JbUser GetUserByToken(string token)
        {
            var result = new JbUser();

            result = _dataContext.Set<JbUser>().SingleOrDefault(t => t.NotBrandedToken == token);

            return result;
        }

        public string GetLastLoggedinClubDomain(string userName)
        {
            var result = string.Empty;

            var user = _dataContext.Set<JbUser>().SingleOrDefault(t => t.UserName.Equals(userName, StringComparison.CurrentCultureIgnoreCase));

            result = user.LastLoggedInClubDomain;

            return result;
        }

        public void UpdateLastLoggedInClub(string userName, string clubDomain)
        {
            var result = string.Empty;

            var user = _dataContext.Set<JbUser>().SingleOrDefault(t => t.UserName.Equals(userName, StringComparison.CurrentCultureIgnoreCase));

            user.LastLoggedInClubDomain = clubDomain;

            _dataContext.SaveChanges();
        }

        public string GenerateTokenForSwitchLogin(string userName)
        {
            var result = string.Empty;

            var token = Guid.NewGuid().ToString();

            token += string.Concat("-", StringCipher.Encrypt(DateTime.UtcNow.ToString().Replace("/", "-").Replace("\\", "--").Replace(" ", "+"), DateTime.UtcNow.ToShortDateString().Replace("/", "")));

            var res = UpdateNotBrandedToken(userName, token);

            if (res.Status)
            {
                result = token;
            }

            return result;
        }

        public bool IsTokenValid(string token)
        {
            var encriptedTime = token.Split('-')[5];

            if (!string.IsNullOrWhiteSpace(encriptedTime))
            {
                var decriptedTime = StringCipher.Decrypt(encriptedTime, DateTime.UtcNow.ToShortDateString().Replace("/", ""));

                var time = DateTime.Parse(decriptedTime.Replace("-", "/").Replace("+", " ").Replace("--", "\\"));

                if (time.AddMinutes(1) > DateTime.UtcNow)
                {
                    return true;
                }
            }

            return false;
        }

        public string GetUrlForLogin(string clubDomain, string userName)
        {
            var result = string.Empty;

            var token = GenerateTokenForSwitchLogin(userName);
            var url = string.Empty;

            if (!string.IsNullOrEmpty(token))
            {
                result = GetUrlForLogin(token);
            }

            return result;
        }

        public string GetUrlForLogin(string token)
        {
            return $"Account/LoginByToken?token={HttpUtility.UrlEncode(token)}";
        }

        public string GetStudentImageFolder(GenderCategories gender = GenderCategories.None)
        {
            var imageFolder = "/Images/Mobile";
            string imageFileName = string.Empty;

            switch (gender)
            {
                case GenderCategories.None:
                    {
                        imageFileName = "allgenders.png";
                    }
                    break;
                case GenderCategories.Male:
                    {
                        imageFileName = "male.png";
                    }
                    break;
                case GenderCategories.Female:
                    {
                        imageFileName = "female.png";
                    }
                    break;
                case GenderCategories.All:
                    {
                        imageFileName = "allgenders.png";
                    }
                    break;
                default:
                    break;
            }

            return string.Concat(imageFolder, "/", imageFileName);
        }

        public bool HasPaidApplicationFeeInItems(int clubId, int userId, long seasonId, bool isTestMode)
        {
            var allOrderItems = new List<OrderItem>();
            var userOrders = _orderItemBusiness.GetClubUserOrderItemsInSeason(clubId, userId, seasonId, isTestMode).ToList();

            allOrderItems.AddRange(userOrders.Where(i => i.ItemStatus == OrderItemStatusCategories.completed || i.ItemStatus == OrderItemStatusCategories.initiated));

            var changedItems = userOrders.Where(i => i.ItemStatus == OrderItemStatusCategories.changed);
            foreach (var item in changedItems)
            {
                if (item.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare)
                {
                }
                else if (item.OrderChargeDiscounts.Any(o => !o.IsDeleted && o.Amount != 0))
                {
                    allOrderItems.Add(item);
                }
            }

            var orderItemsWithApplicationFee = allOrderItems.Where(i => i.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Amount > 0 && (c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.PartnerApplicationFee))).ToList();
            var result = orderItemsWithApplicationFee.Any();

            return result;
        }

        public bool HasPaidApplicationFeeParticipant(int playerId, long seasonId, bool isTestMode)
        {
            var allOrderItems = new List<OrderItem>();

            var userOrders = _orderItemBusiness.GetParticipantItemsInSeason(playerId, seasonId, isTestMode);

            allOrderItems.AddRange(userOrders.Where(i => i.ItemStatus == OrderItemStatusCategories.completed || i.ItemStatus == OrderItemStatusCategories.initiated));

            var changedItems = userOrders.Where(i => i.ItemStatus == OrderItemStatusCategories.changed);
            foreach (var item in changedItems)
            {
                if (item.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare)
                {
                }
                else if (item.OrderChargeDiscounts.Any(o => !o.IsDeleted && o.Amount != 0))
                {
                    allOrderItems.Add(item);
                }
            }

            var result = false;
            foreach (var item in allOrderItems)
            {
                if (item.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Amount > 0 && (c.Category == ChargeDiscountCategory.ApplicationFee)))
                {
                    result = true;
                }
            }

            return result;
        }
        public bool IsParentDashboardAccesibleByAdmin(int clubId, int userId, bool isPartner)
        {
            var result = false;

            if (isPartner)
            {
                result = _dataContext.Set<Club>().Any(c => c.PartnerId.HasValue && c.PartnerId == clubId && c.Users.Select(u => u.UserId).Contains(userId));
            }
            else
            {
                result = _dataContext.Set<ClubUser>().Any(u => u.ClubId == clubId && u.UserId == userId);
            }

            return result;
        }

    }
}
