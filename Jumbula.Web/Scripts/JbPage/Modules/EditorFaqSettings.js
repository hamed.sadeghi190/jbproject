﻿var editorFaqSettings = (function () {

    var activeAccForEdit;
    var titleBackup;

    function moveUp(element) {
        var acc = $(element).closest('acc')
        if ($(element).closest('acc').prev().length != 0) {
            acc.insertBefore(acc.prev()).slideUp(0).slideDown(300);
        }
        else {
            acc.insertAfter($(element).closest(".editor-acc-list-container").find('acc:last-of-type')).slideUp(0).slideDown(300);
        }
    }
    function moveDown(element) {
        var acc = $(element).closest('acc')
        if ($(element).closest('acc').next().length != 0) {
            acc.insertAfter(acc.next()).slideUp(0).slideDown(300);
        }
        else {
            acc.insertBefore($(element).closest(".editor-acc-list-container").find('acc:first-of-type')).slideUp(0).slideDown(300);
        }
    }
    function deleteAccItem(element) {
        var acc = $(element).closest('acc')
        acc.remove();
    }
    function editAccItem(element) {
        activeAccForEdit = $(element).closest('acc');
        titleBackup = activeAccForEdit.find(".panel-heading").find('.accordion-toggle').text();
        $('#editAccordionBox').slideDown();
        $("#editAccordionBox").find('[name=title]').val(titleBackup);

    }
    function saveEditAcc(element) {
        var titleEdited = $(element).closest('#editAccordionBox').find('[name=title]').val();
        console.log(activeAccForEdit.find(".panel-heading").find(':contains(' + titleBackup + ')').last());
        activeAccForEdit.find(".panel-heading").find(':contains(' + titleBackup + ')').last().text(titleEdited);
        $(element).closest('#editAccordionBox').slideUp();
    }
    function cancelEditAcc(element) {
        $(element).closest('#editAccordionBox').slideUp();
    }
    return {
        "moveUp": moveUp,
        "moveDown": moveDown,
        "deleteAccItem": deleteAccItem,
        "editAccItem": editAccItem,
        "saveEditAcc": saveEditAcc,
        "cancelEditAcc": cancelEditAcc,
    }
}());
