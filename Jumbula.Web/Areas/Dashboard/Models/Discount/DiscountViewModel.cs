﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class DiscountViewModel : BaseViewModel<Discount, long>
    {

        public DiscountViewModel()
        {
            ApplyType = 0;
            AmountType = ChargeDiscountType.Fixed;
            IsAllProgram = true;
            ProgramsList = new List<SelectKeyValue<string>>();
        }

        [MaxLength(256, ErrorMessage = "{0} cannot be longer than 256 characters.")]
        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        public ChargeDiscountCategory Category { get; set; }

        public string CategoryDescription
        {
            get { return Category.ToDescription(); }
        }

        public string CategoryToString
        {
            get { return Category.ToString(); }
        }

        public string Description { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Amount")]
        public decimal Amount { get; set; }

        public ChargeDiscountType AmountType { get; set; }

        public EligibleMode EligibleMode { get; set; }

        public SelectKeyValue<string> SelectedBenefit { get; set; }

        public List<SelectKeyValue<string>> BenefitItems { get; set; }

        public bool IsAllProgram { get; set; }

        public long SeasonId { get; set; }

        public bool IsDeleted { get; set; }

        [Required(ErrorMessage = "Select at least one program.")]
        //public List<long> SelectedPrograms { get; set; }
        public List<string> SelectedPrograms { get; set; }

        public List<SelectKeyValue<string>> ProgramsList { get; set; }
        public List<SelectKeyValue<string>> NopItems { get; set; }

        public SelectKeyValue<string> NOP { get; set; }

        public List<SelectKeyValue<string>> ApplyItems { get; set; }

        public int ApplyType { get; set; }

        public string SeasonDomain { get; set; }
    }
}