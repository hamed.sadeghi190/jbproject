﻿
using System.ComponentModel;


namespace Jumbula.Common.Enums
{
    public enum HowToHearAboutUs : byte
    {


        [Description("Online search")]
        OnlineSearch = 0,
        [Description("Facebook online advertisement")]
        FacebookAds = 1,
        [Description("Google online advertisement")]
        GoogleAds = 2,
        [Description("Capterra directory")]
        CapterraDirectory = 3,
        [Description("Referral")]
        Referral = 4,
        [Description("Other")]
        Other = 5
    }
}
