﻿using Jumbula.Common.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace Jumbula.Common.Helper
{
    public static class EnumHelper
    {
        public static T ParseEnum<T>(string value) 
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public static T GetValueFromDescription<T>(string description)
        {
            if (string.IsNullOrEmpty(description))
            {
                return default(T);
            }

            try
            {
                var type = typeof(T);
                if (!type.IsEnum) throw new InvalidOperationException();
                foreach (var field in type.GetFields())
                {
                    if (Attribute.GetCustomAttribute(field,
                        typeof(DescriptionAttribute)) is DescriptionAttribute attribute)
                    {
                        if (attribute.Description == description)
                            return (T)field.GetValue(null);
                    }
                    else
                    {
                        if (field.Name == description)
                            return (T)field.GetValue(null);
                    }
                }

                throw new ArgumentException("Not found.", nameof(description));
            }
            catch
            {
                return default(T);
            }
        }

        public static string ToDescription(this Enum value)
        {
            return GetEnumDescription(value);
        }

        public static string GetEnumDescription<TEnum>(TEnum value)
        {
            if (value == null)
                return string.Empty;

            try
            {
                var field = value.GetType().GetField(value.ToString());

                var attributes =
                    (DescriptionAttribute[])field.GetCustomAttributes(typeof(DescriptionAttribute), false);

                return attributes.Length > 0 ? attributes[0].Description : value.ToString();
            }
            catch
            {
                return value.ToString();
            }
        }

        public static string DisplayName(this Enum value)
        {
            var enumType = value.GetType();
            var enumValue = Enum.GetName(enumType, value);
            var member = enumType.GetMember(enumValue)[0];

            var attrs = member.GetCustomAttributes(typeof(DisplayAttribute), false);
            var outString = ((DisplayAttribute)attrs[0]).Name;

            if (((DisplayAttribute)attrs[0]).ResourceType != null)
            {
                outString = ((DisplayAttribute)attrs[0]).GetName();
            }

            return outString;
        }

        public static List<SelectListItem> GetEnumList<T>(string defaultItem = null) where T : struct, IComparable, IFormattable, IConvertible
        {

            var values = Enum.GetValues(typeof(T))
                .Cast<T>()
                .Select(d => new SelectListItem { Text = GetEnumDescription(d), Value = d.ToString(CultureInfo.InvariantCulture) })
                .ToList();

            if (!string.IsNullOrEmpty(defaultItem))
            {
                values.Insert(0, new SelectListItem { Text = defaultItem, Value = defaultItem.Trim().Replace(" ", string.Empty) });
            }

            return values;
        }

        public static List<string> GetEnumDescriptionList<T>(string defaultItem = null) where T : struct, IComparable, IFormattable, IConvertible
        {

            var values = Enum.GetValues(typeof(T))
                .Cast<T>()
                .Select(GetEnumDescription)
                .ToList();

            return values;
        }

        public static List<T> GetEnumValues<T>() where T : struct, IComparable, IFormattable, IConvertible
        {

            var values = Enum.GetValues(typeof(T))
                .Cast<T>()
                .ToList();

            return values;
        }

        public static TAttribute GetAttribute<TAttribute>(this Enum value)
            where TAttribute : Attribute
        {
            var type = value.GetType();
            var name = Enum.GetName(type, value);
            return type.GetField(name) // I prefer to get attributes this way
                .GetCustomAttributes(false)
                .OfType<TAttribute>()
                .SingleOrDefault();
        }

        public static List<SelectListItem> GetWithOrder(this Enum enumVal)
        {
            return enumVal.GetType().GetWithOrder();
        }

        public static List<SelectListItem> GetWithOrder(this Type type)
        {
            if (!type.IsEnum)
            {
                throw new ArgumentException("Type must be an enum");
            }
            // caching for result could be useful
            return type.GetFields()
                                   .Where(field => field.IsStatic)
                                   .Select(field => new
                                   {
                                       field,
                                       attribute = field.GetCustomAttribute<EnumOrderAttribute>(),
                                       descriptionAttribute = field.GetCustomAttribute<DescriptionAttribute>()
                                   })
                                    .Select(fieldInfo => new
                                    {
                                        name = fieldInfo.field.Name,
                                        order = fieldInfo.attribute?.Order ?? 0,
                                        decription = fieldInfo.descriptionAttribute != null ? fieldInfo.descriptionAttribute.Description : fieldInfo.field.Name
                                    })
                                   .OrderBy(field => field.order)
                                   .Select(field => new SelectListItem { Value = field.name, Text = field.decription }).ToList();
        }

        public static int? GetOrder(this Enum value)
        {
            try
            {
                if (value == null) { return null; }

                var attributes = (EnumOrderAttribute[])value.GetType().GetField(Convert.ToString(value)).GetCustomAttributes(typeof(EnumOrderAttribute), false);

                return attributes.Length > 0 ? attributes[0].Order : value.GetHashCode();
            }
            catch
            {
                if (value != null)
                {
                    return value.GetHashCode();
                }
            }

            return null;
        }

        public static string ToAbbreviation(this Enum value)
        {
            return GetEnumAbbreviation(value);
        }

        public static string GetEnumAbbreviation<TEnum>(TEnum value)
        {
            if (value == null)
                return string.Empty;

            try
            {
                var field = value.GetType().GetField(value.ToString());

                var attributes =
                    (AbbreviationAttribute[])field.GetCustomAttributes(typeof(AbbreviationAttribute), false);

                return attributes.Length > 0 ? attributes[0].Abbreviation : value.ToString();
            }
            catch
            {
                return value.ToString();
            }
        }
    }
}
