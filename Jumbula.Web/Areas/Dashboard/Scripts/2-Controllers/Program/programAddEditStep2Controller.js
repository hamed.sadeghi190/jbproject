﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('programAddEditStep2Controller', ['$scope', '$http', '$log', '$stateParams', '$state', 'programService', 'ProgramSchedulePartsService', 'jbToast', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, programService, ProgramSchedulePartsService, jbToast, jbConfirmation) {
        $scope.baseUrl = window.location.origin + "/Dashboard/Program";

        window.app.globalObjects.pageClass = 'seasons-view programs-add ' + $stateParams.programType;
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;


    }])
        .controller('programDefaultAddEditStep2Controller', ['$scope', '$http', '$log', '$stateParams', '$state', 'programService', 'jbToast', 'jbConfirmation', 'jbRandom', function ($scope, $http, $log, $stateParams, $state, programService, jbToast, jbConfirmation, jbRandom) {

            $scope.baseUrl = window.location.origin + "/Dashboard/Program";

            window.app.globalObjects.pageClass = 'seasons-view programs-add ' + $stateParams.programType;
            window.app.globalObjects.initializeView();

            $scope.MC.stateParams = $stateParams;

            angular.forEach($scope.MC.seasons, function (value, key) {
                if (value.Domain == $stateParams.seasonDomain)
                    $scope.MC.currentSeason = value;
            });

            $scope.initDropDown = function () {
                $.Metro.initDropdowns();
            }

            $scope.initAccordions = function () {
                $.Metro.initAccordions();
            }


            $scope.ShowInfo = function () {

                $(".infoCamp").slideToggle();

            }
            $scope.seasonName = $stateParams.seasonDomain;
            $scope.programName = $stateParams.programDomain;

            $scope.oldSchedules = [];

            $scope.setRegisterEndDate = function (scheduleIndex) {

                $scope.Model.Schedules[scheduleIndex].RegistrationPeriod.RegisterEndDate = !$scope.Model.Schedules[scheduleIndex].RegistrationPeriod.RegisterEndDate ? $scope.Model.Schedules[scheduleIndex].StartDate : $scope.Model.Schedules[scheduleIndex].RegistrationPeriod.RegisterEndDate;
            }

            $scope.dayOfWeek = function (index) {
                return index.toDayOfWeekAbbreviation();
            }

            $scope.saveCampSchedule = function () {

                $scope.campScheduleChanged = false;

                for (var i = 0; i < $scope.Model.Schedules.length; i++) {

                    $scope.CampSchedulesChanged($scope.Model.Schedules[i].Id)
                }

                $http.post($scope.baseUrl + '/ProgramScheduleValidate', { model: $scope.Model })
                    .success(function (data, status, headers, config) {

                        if (data.Status == false && data.Message == "Can't change schedule when program has order.") {

                            $scope.MC.handleErrors($scope, data);
                        }
                        else {
                            if (data.Status == true) {

                                $scope.showQuestion1 = true;
                            }
                            else {
                                $scope.MC.handleErrors($scope, data);
                            }
                        }
                    });
            }

            $scope.formatDate = function (date) {

                if (date != '' && date != null && date != 'undefined') {
                    return new Date(date).format('mm/dd/yyyy H:mm');
                }
            }

            $scope.formatCampDate = function (date) {
                if (date != '' && date != null && date != 'undefined') {
                    return new Date(date).format('mm/dd/yyyy');
                }
            }

            $scope.addEB = function (tuition) {

                if (tuition.HasEarlyBird) {
                    tuition.EarlyBirds.push({ Id: jbRandom.Generate(10, true, true, true, true, false), ExpireDate: '', Price: '', IsMandatory: false });
                }
            };

            $scope.changeEB = function (tuition) {

                tuition.EarlyBirds = [];

                if (tuition.HasEarlyBird) {
                    $scope.addEB(tuition);
                }
            };

            $scope.changeDropIn = function (schedule) {

                if (!schedule.EnableDropIn) {

                    for (var i = 0; i < schedule.Tuitions.length; i++) {

                        schedule.Tuitions[i].DropInName = null;
                        schedule.Tuitions[i].DropInPrice = null;
                    }

                }
            }

            $scope.addTo = function (schedule) {
                if (!schedule.Tuitions) {
                    schedule.Tuitions = [];
                }

                schedule.Tuitions.push({ Id: jbRandom.Generate(10, true, true, true, true, false), Category: 5, Price: '', Capacity: '0' });
            };

            $scope.removeTo = function (schedule, tuitionId) {

                var canDeleteTution = true;
                for (var i = 0; i < schedule.Tuitions.length; i++) {

                    if (schedule.Tuitions[i].Id == tuitionId && schedule.Tuitions[i].HasOrder && $scope.Model.TypeCategory == 'Camp') {

                        canDeleteTution = false;
                    }
                }

                if (canDeleteTution) {
                    jbConfirmation
                        .yes(function () {

                            var tempTuitions = schedule.Tuitions;

                            schedule.Tuitions = null;
                            schedule.Tuitions = [];

                            for (var i = 0; i < tempTuitions.length; i++) {
                                if (tempTuitions[i].Id != tuitionId) {
                                    schedule.Tuitions.push(tempTuitions[i]);
                                }
                            }

                        })
                        .title('Tuition delete')
                        .message('Are you sure you want to delete this tuition option?')
                        .confirm();
                }
                else
                {
                    jbToast.error("You cannot delete a tuition if it has orders. ");
                }

            };

            $scope.addCharge = function (schedule, category) {
                if (!schedule.Charges) {
                    schedule.Charges = [];
                }

                schedule.Charges.push({ Id: jbRandom.Generate(10, true, true, true, true, false), Category: category, Price: '', Capacity: '0' });
            };

            $scope.removeCharge = function (chargeId) {

                jbConfirmation
                    .yes(function () {

                        for (var i = 0; i < $scope.Model.Schedules.length; i++) {
                            for (var j = 0; j < $scope.Model.Schedules[i].Charges.length; j++) {
                                if ($scope.Model.Schedules[i].Charges[j].Id == chargeId) {
                                    $scope.Model.Schedules[i].Charges.splice(j, 1);
                                }
                            }
                        }
                    })
                    .title('Charge delete')
                    .message('Are you sure you want to delete this charge?')
                    .confirm();
            };

            $scope.showHideChargeOptions = function (tuition) {
                tuition.ShowAdvancedOptions = !tuition.ShowAdvancedOptions;
            };

            $scope.removeEB = function (tuition, earlyBirdId) {

                jbConfirmation
                    .yes(function () {

                        var tempEarlyBirds = tuition.EarlyBirds;

                        tuition.EarlyBirds = null;
                        tuition.EarlyBirds = [];

                        for (var i = 0; i < tempEarlyBirds.length; i++) {
                            if (tempEarlyBirds[i].Id != earlyBirdId) {
                                tuition.EarlyBirds.push(tempEarlyBirds[i]);
                            }
                        }

                        if (tuition.EarlyBirds.length < 1) {
                            tuition.HasEarlyBird = false
                        }

                    })
                    .title('Early bird delete')
                    .message('Are you sure you want to delete this early bird option?')
                    .confirm();
            };



            $scope.removeEC = function (schedule, index) {
                schedule.charges.removeByIndex(index);
            };

            $scope.firstCollapsed = false;

            $scope.collapseFirstSchedule = function (schedule) {

                $http.post($scope.baseUrl + '/CopySchedule', { schedule: schedule, scheduleCount: $scope.Model.newSchedulesNumber })
                    .success(function (data, status, headers, config) {
                        $scope.Model.Schedules = data;
                    });

                $scope.firstCollapsed = true;
            };

            $scope.addSchedule = function () {

                $http.get('Dashboard/Program/AddSchedule').success(function (data, status, headers, config) {

                    $scope.Model.Schedules.push(data);
                });
            };

            $scope.copySchedule = function (schedule) {
                $scope.Model.Schedules.push(angular.copy(schedule));
                $scope.Model.Schedules[$scope.Model.Schedules.length - 1].Id = 0;
            }

            $scope.removeSchedule = function (index, scheduleId) {

                var canDeleteSchedule = true;
                for (var i = 0; i < $scope.Model.Schedules.length; i++) {

                    if ($scope.Model.Schedules[i].Id == scheduleId && $scope.Model.Schedules[i].HasOrder) {

                        canDeleteSchedule = false;
                    }
                }

                if (canDeleteSchedule) {

                    jbConfirmation
                        .yes(function () {
                            $scope.Model.Schedules.removeByIndex(index);
                        })
                        .title('Schedule delete')
                        .message('Are you sure you want to delete this schedule?')
                        .confirm();
                } else {
                    jbToast.error("You can't delete schedule when it has orders.");
                }

            }

            programService.getStep2(function (data, status, headers, config) {


                $scope.Model = data;

                $scope.oldSchedules = clone(data.Schedules);

                $scope.generateEbIds();

                if ($scope.Model.LastCreatedPage.split("Step")[1] < 1) {
                    jbToast.warning("Please setup your program step by step.");
                    $state.go('ProgramAddEditStep' + (Number($scope.Model.LastCreatedPage.split("Step")[1]) + 1), { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $scope.MC.stateParams.programId });
                }

                if (data.Schedules.length > 1) {
                    $scope.firstCollapsed = true;
                }
                else {
                    $scope.firstCollapsed = false;
                }

            }, function () {
                jbToast.error("Something went wrong!");
            },
                {
                    programType: $stateParams.programType, seasonDomain: $stateParams.seasonDomain, programId: $stateParams.programId
                });

            $scope.isScheduleChanged = function (scheduleId) {

                var currentSchedule = {};
                var newSchedule = {};
                var scheduleIndex = 0;

                for (var i = 0; i < $scope.oldSchedules.length; i++) {
                    if ($scope.oldSchedules[i].Id == scheduleId) {
                        currentSchedule = $scope.oldSchedules[i];
                        break;
                    }
                }

                if (currentSchedule != null & currentSchedule != 'undefined') {
                    for (var i = 0; i < $scope.Model.Schedules.length; i++) {
                        if ($scope.Model.Schedules[i].Id == scheduleId) {
                            newSchedule = $scope.Model.Schedules[i];
                            scheduleIndex = i;
                            break;
                        }
                    }
                }


                if (newSchedule.StartDate != currentSchedule.StartDate) {
                    $scope.Model.Schedules[scheduleIndex].IsScheduleChanged = true;
                    return true;
                }

                if (newSchedule.EndDate != currentSchedule.EndDate) {
                    $scope.Model.Schedules[scheduleIndex].IsScheduleChanged = true;
                    return true;
                }

                if (newSchedule.SelectedContinueType != currentSchedule.SelectedContinueType) {
                    $scope.Model.Schedules[scheduleIndex].IsScheduleChanged = true;
                    return true;
                }

                if (newSchedule.SelectedSession != currentSchedule.SelectedSession) {
                    $scope.Model.Schedules[scheduleIndex].IsScheduleChanged = true;
                    return true;
                }

                if (newSchedule.Days.length != currentSchedule.Days.length) {
                    $scope.Model.Schedules[scheduleIndex].IsScheduleChanged = true;
                    return true;
                }

                for (var i = 0; i < newSchedule.Days.length; i++) {

                    if (newSchedule.Days[i].StartTime != currentSchedule.Days[i].StartTime) {
                        $scope.Model.Schedules[scheduleIndex].IsScheduleChanged = true;
                        return true;
                    }

                    if (newSchedule.Days[i].EndTime != currentSchedule.Days[i].EndTime) {
                        $scope.Model.Schedules[scheduleIndex].IsScheduleChanged = true;
                        return true;
                    }

                    if (newSchedule.Days[i].IsChecked != currentSchedule.Days[i].IsChecked) {
                        $scope.Model.Schedules[scheduleIndex].IsScheduleChanged = true;
                        return true;
                    }
                }

                return false;
            }

            $scope.isSessionsChanged = function (scheduleId) {

                if ($scope.Model.TypeCategory != 'Class') {
                    return false;
                }

                var newSchedule = {};
                var isSessionsChanged2 = true;

                for (var i = 0; i < $scope.Model.Schedules.length; i++) {
                    if ($scope.Model.Schedules[i].Id == scheduleId) {
                        newSchedule = $scope.Model.Schedules[i];
                        break;
                    }
                }

                $http.post('Dashboard/Program/IsSessionsChanged', { scheduleId: newSchedule.Id })
                    .success(function (data, status, headers, config) {

                        if (data.Status) {
                            isSessionsChanged = data.Changed;

                            if (isSessionsChanged) {

                                jbConfirmation
                                    .yes(function () {
                                        $scope.saveStep2AfterConfirm();
                                    })
                                    .title('Overwrite sessions')
                                    .message('If you save your edited data, it will overwrite the session days and times that you have edited for this class. Do you want to proceed?')
                                    .confirm();
                            }
                            else {
                                $scope.saveStep2AfterConfirm();
                            }

                        } else {
                            $scope.MC.handleErrors($scope, data);
                        }
                    });

            }

            $scope.generateEbIds = function () {
                for (var i = 0; i < $scope.Model.Schedules.length; i++) {
                    for (var j = 0; j < $scope.Model.Schedules[i].Tuitions.length; j++) {
                        if ($scope.Model.Schedules[i].Tuitions[j].EarlyBirds) {
                            for (var k = 0; k < $scope.Model.Schedules[i].Tuitions[j].EarlyBirds.length; k++) {
                                $scope.Model.Schedules[i].Tuitions[j].EarlyBirds[k].Id = jbRandom.Generate(10, true, true, true, true, false);
                            }
                        }
                    }
                }
            }

            $scope.saveStep2 = function () {
                var scheduleId = $scope.Model.Schedules[0].Id;

                var newSchedule = {};
                var isSessionsChanged = true;

                for (var i = 0; i < $scope.Model.Schedules.length; i++) {
                    if ($scope.Model.Schedules[i].Id == scheduleId) {
                        newSchedule = $scope.Model.Schedules[i];
                        break;
                    }
                }

                if ($scope.Model.TypeCategory == 'Class' && $scope.isScheduleChanged(scheduleId)) {

                    $http.post('Dashboard/Program/IsSessionsChanged', { scheduleId: newSchedule.Id })
                        .success(function (data, status, headers, config) {

                            if (data.Status) {
                                isSessionsChanged = data.Changed;

                                if (isSessionsChanged) {

                                    jbConfirmation
                                        .yes(function () {
                                            $scope.saveStep2AfterConfirm();
                                        })
                                        .title('Overwrite sessions')
                                        .message('If you save your edited data, it will overwrite the session days and times that you have edited for this class. Do you want to proceed?')
                                        .confirm();
                                }
                                else {
                                    $scope.saveStep2AfterConfirm();
                                }

                            } else {
                                $scope.MC.handleErrors($scope, data);
                            }
                        });
                } else {
                    $scope.saveStep2AfterConfirm();
                }

            };

            $scope.saveStep2AfterConfirm = function () {
                programService.setModel($scope.Model);

                $scope.campScheduleChanged = false;

                if ($scope.Model.TypeCategory == 'Camp') {
                    for (var i = 0; i < $scope.Model.Schedules.length; i++) {
                        $scope.CampSchedulesChanged($scope.Model.Schedules[i].Id);
                    }

                    var count = 0;
                    $http.post($scope.baseUrl + '/ProgramScheduleValidate', { model: $scope.Model })
                        .success(function (data, status, headers, config) {

                            if (data.Status == false) {
                                $scope.MC.handleErrors($scope, data);
                            }
                            else {
                                if (data.Status == true) {

                                    programService.saveStep2(function (data, status, headers, config) {
                                        if (data.Status == true) {

                                            $state.go('ProgramAddEditStep3', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $stateParams.programId })
                                        }
                                        else {
                                            $scope.MC.handleErrors($scope, data);
                                        }
                                    });
                                }
                            }
                        });
                } else {
                    programService.saveStep2(function (data, status, headers, config) {
                        if (data.Status == true) {

                            $state.go('ProgramAddEditStep3', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $stateParams.programId })
                        }
                        else {
                            $scope.MC.handleErrors($scope, data);
                        }
                    });
                }
            }

            var clone = function (obj) {
                var copy;

                // Handle the 3 simple types, and null or undefined
                if (null == obj || "object" != typeof obj) return obj;

                // Handle Date
                if (obj instanceof Date) {
                    copy = new Date();
                    copy.setTime(obj.getTime());
                    return copy;
                }

                // Handle Array
                if (obj instanceof Array) {
                    copy = [];
                    for (var i = 0, len = obj.length; i < len; i++) {
                        copy[i] = clone(obj[i]);
                    }
                    return copy;
                }

                // Handle Object
                if (obj instanceof Object) {
                    copy = {};
                    for (var attr in obj) {
                        if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
                    }
                    return copy;
                }

                throw new Error("Unable to copy obj! Its type isn't supported.");
            }

            $scope.CampSchedulesChanged = function (scheduleId) {

                var currentSchedule = {};
                var newSchedule = {};
                var scheduleIndex = 0;

                for (var i = 0; i < $scope.oldSchedules.length; i++) {
                    if ($scope.oldSchedules[i].Id == scheduleId) {
                        currentSchedule = $scope.oldSchedules[i];
                        break;
                    }
                }

                if (currentSchedule != null & currentSchedule != 'undefined') {
                    for (var i = 0; i < $scope.Model.Schedules.length; i++) {
                        if ($scope.Model.Schedules[i].Id == scheduleId) {
                            newSchedule = $scope.Model.Schedules[i];
                            scheduleIndex = i;
                            break;
                        }
                    }
                }

                $scope.Model.Schedules[scheduleIndex].IsScheduleChanged = false;

                if (newSchedule.Id != 0) {

                    if (newSchedule.StartDate != currentSchedule.StartDate) {
                        $scope.Model.Schedules[scheduleIndex].IsScheduleChanged = true;
                        return true;
                    }

                    if (newSchedule.EndDate != currentSchedule.EndDate) {
                        $scope.Model.Schedules[scheduleIndex].IsScheduleChanged = true;
                        return true;
                    }

                    if (newSchedule.Days.length != currentSchedule.Days.length) {
                        $scope.Model.Schedules[scheduleIndex].IsScheduleChanged = true;
                        return true;
                    }

                    for (var i = 0; i < newSchedule.Days.length; i++) {

                        if (newSchedule.Days[i].IsChecked != currentSchedule.Days[i].IsChecked) {
                            $scope.Model.Schedules[scheduleIndex].IsScheduleChanged = true;
                            return true;
                        }
                    }
                }

                return false;
            }

        }])
        .controller('TournamentAddEditStep2Controller', ['$scope', '$http', '$log', '$stateParams', '$state', 'programService', 'jbToast', 'jbConfirmation', 'jbRandom', function ($scope, $http, $log, $stateParams, $state, programService, jbToast, jbConfirmation, jbRandom) {

            $scope.baseUrl = window.location.origin + "/Dashboard/Program";

            window.app.globalObjects.pageClass = 'seasons-view programs-add ' + $stateParams.programType;
            window.app.globalObjects.initializeView();

            $scope.MC.stateParams = $stateParams;

            angular.forEach($scope.MC.seasons, function (value, key) {
                if (value.Domain == $stateParams.seasonDomain)
                    $scope.MC.currentSeason = value;
            });

            $scope.initDropDown = function () {
                $.Metro.initDropdowns();
            }

            $scope.seasonName = $stateParams.seasonDomain;
            $scope.programName = $stateParams.programDomain;

            $scope.addEB = function (tuition) {

                if (tuition.HasEarlyBird) {
                    tuition.EarlyBirds.push({ ExpireDate: '', Price: '', IsMandatory: false });
                }
            };

            $scope.changeEB = function (tuition) {

                tuition.EarlyBirds = [];

                if (tuition.HasEarlyBird) {
                    $scope.addEB(tuition);
                }
            };

            $scope.addSection = function () {
                $scope.Model.TourneySections.push({ Name: '', Rating: '', Prize: '' });
            };

            $scope.addSchedule = function () {
                $scope.Model.TourneySchedules.push({ StartDate: '', EndDate: '', Label: '' });
            };

            $scope.removeSection = function (index) {

                jbConfirmation
                    .yes(function () {
                        $scope.Model.TourneySections.removeByIndex(index);
                    })
                    .title('Section delete')
                    .message('Are you sure you want to delete this section?')
                    .confirm();
            }


            $scope.removeSchedule = function (index) {

                jbConfirmation
                    .yes(function () {
                        $scope.Model.TourneySchedules.removeByIndex(index);
                    })
                    .title('Schedule delete')
                    .message('Are you sure you want to delete this schedule?')
                    .confirm();
            }

            $scope.addTo = function (schedule) {
                if (schedule.Tuitions)
                    schedule.Tuitions.push({
                        Category: 5, Price: '', Capacity: '0'
                    });
                else {
                    schedule.Tuitions = [];
                    schedule.Tuitions.push({ Category: 5, Price: '', Capacity: '0' });
                }
            };
            $scope.removeTo = function (schedule, index) {

                jbConfirmation
                    .yes(function () {
                        schedule.Tuitions.removeByIndex(index);
                    })
                    .title('Tuition delete')
                    .message('Are you sure you want to delete this tuition option?')
                    .confirm();
            };

            $scope.addCharge = function (schedule, category) {
                if (!schedule.Charges) {
                    schedule.Charges = [];
                }
                schedule.Charges.push({ Category: category, Price: '', Capacity: '0' });
            };

            $scope.removeCharge = function (schedule, index) {

                jbConfirmation
                    .yes(function () {
                        schedule.Charges.removeByIndex(index);
                    })
                    .title('Charge delete')
                    .message('Are you sure you want to delete this charge?')
                    .confirm();
            };

            $scope.showHideChargeOptions = function (tuition) {
                tuition.ShowAdvancedOptions = !tuition.ShowAdvancedOptions;
            };

            $scope.removeEB = function (tuition, index) {

                jbConfirmation
                    .yes(function () {

                        tuition.EarlyBirds.removeByIndex(index);

                        if (tuition.EarlyBirds.length < 1) {
                            tuition.HasEarlyBird = false
                        }
                    })
                    .title('Early bird delete')
                    .message('Are you sure you want to delete this early bird option?')
                    .confirm();
            };


            $scope.removeEC = function (schedule, index) {
                schedule.charges.removeByIndex(index);
            };

            programService.getTournamentStep2(function (data, status, headers, config) {

                $scope.Model = data;

                if ($scope.Model.LastCreatedPage.split("Step")[1] < 1) {
                    jbToast.warning("Please setup your program step by step.");
                    $state.go('ProgramAddEditStep' + (Number($scope.Model.LastCreatedPage.split("Step")[1]) + 1), { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $scope.MC.stateParams.programId });
                }

                if (data.Schedules.length > 1) {
                    $scope.firstCollapsed = true;
                }
                else {
                    $scope.firstCollapsed = false;
                }

            }, function () {
                jbToast.error("Something went wrong!");
            }, { programType: $stateParams.programType, seasonDomain: $stateParams.seasonDomain, programId: $stateParams.programId });

            $scope.saveStep2 = function () {

                programService.setModel($scope.Model);

                programService.saveTournamentStep2(function (data, status, headers, config) {
                    if (data.Status == true) {

                        $state.go('ProgramAddEditStep3', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $stateParams.programId })
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });

            };
        }])
        .controller('CalendarAddEditStep2Controller', ['$scope', '$http', '$log', '$stateParams', '$state', 'programService', 'jbToast', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, programService, jbToast, jbConfirmation) {

            $scope.baseUrl = window.location.origin + "/Dashboard/Program";

            window.app.globalObjects.pageClass = 'seasons-view programs-add ' + $stateParams.programType;
            window.app.globalObjects.initializeView();

            $scope.MC.stateParams = $stateParams;

            angular.forEach($scope.MC.seasons, function (value, key) {
                if (value.Domain == $stateParams.seasonDomain)
                    $scope.MC.currentSeason = value;
            });

            $scope.initDropDown = function () {
                $.Metro.initDropdowns();
            }

            $scope.initAccordions = function () {
                $.Metro.initAccordions();
            }

            $scope.seasonName = $stateParams.seasonDomain;
            $scope.programName = $stateParams.programDomain;

            $scope.formatDate = function (date) {

                if (date != null && date != 'undefined') {
                    return new Date(date).format('mm/dd/yyyy H:mm');
                }
            }

            $scope.addSchedule = function (schedule) {

                $http.get('Dashboard/Program/AddSchedule').success(function (data, status, headers, config) {

                    $scope.Model.Schedules.push(data);
                });
            };

            $scope.copySchedule = function (schedule) {

                $scope.Model.Schedules.push(schedule);
                $scope.Model.Schedules[$scope.Model.Schedules.length - 1].Id = 0;
            }

            $scope.removeSchedule = function (index) {

                jbConfirmation
                    .yes(function () {
                        $scope.Model.Schedules.removeByIndex(index);
                    })
                    .title('Schedule delete')
                    .message('Are you sure you want to delete this schedule?')
                    .confirm();
            }

            programService.getCalendarStep2(function (data, status, headers, config) {

                $scope.Model = data;

                if ($scope.Model.LastCreatedPage.split("Step")[1] < 1) {
                    jbToast.warning("Please setup your program step by step.");
                    $state.go('ProgramAddEditStep' + (Number($scope.Model.LastCreatedPage.split("Step")[1]) + 1), { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $scope.MC.stateParams.programId });
                }


            }, function () {
                jbToast.error("Something went wrong!");
            }, { programType: $stateParams.programType, seasonDomain: $stateParams.seasonDomain, programId: $stateParams.programId });


            $scope.back = function () {

                if ($scope.Model.ViewCalendar) {

                    $scope.errors = null;
                    $scope.Model.ViewCalendar = false;

                }
                else {
                    $state.go('ProgramAddEditStep1', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $stateParams.programId });
                }
            }

            $scope.calendarDataSource = [];

            $scope.saveStep2 = function () {

                programService.setModel($scope.Model);

                programService.saveCalendarStep2(function (data, status, headers, config) {
                    if (data.Status == true) {

                        $state.go('CalendarAddEditStep2Point5', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $stateParams.programId });
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
            };



        }])
        .controller('SubscriptionAddEditStep2Controller', ['$scope', '$http', '$log', '$stateParams', '$state', 'programService', 'jbToast', 'jbConfirmation', 'jbRandom', function ($scope, $http, $log, $stateParams, $state, programService, jbToast, jbConfirmation, jbRandom) {

            $scope.baseUrl = window.location.origin + "/Dashboard/Program";

            window.app.globalObjects.pageClass = 'seasons-view programs-add ' + $stateParams.programType;
            window.app.globalObjects.initializeView();

            $scope.MC.stateParams = $stateParams;

            var oldModel = {};

            angular.forEach($scope.MC.seasons, function (value, key) {
                if (value.Domain == $stateParams.seasonDomain)
                    $scope.MC.currentSeason = value;
            });

            $scope.initDropDown = function () {
                $.Metro.initDropdowns();
            }

            $scope.initAccordions = function () {
                $.Metro.initAccordions();
            }

            $scope.seasonName = $stateParams.seasonDomain;
            $scope.programName = $stateParams.programDomain;

            $scope.formatDate = function (date) {

                if (date != null && date != 'undefined') {
                    return new Date(date).format('mm/dd/yyyy H:mm');
                }
            }

            $scope.scheduleTypes = [];
            $scope.scheduleTypes.push({ Text: 'Monthly', Value: 0 });
            $scope.scheduleTypes.push({ Text: 'Weekly', Value: 1 });


            $scope.generateSubscriptionSchedule = function () {

                var schedule = $scope.Model.Schedules[0];

                $http.get('Dashboard/Program/GenerateSubscriptionSchedules', { params: { startDate: schedule.StartDate, endDate: schedule.EndDate, weeks: $scope.Model.Subscription.Weeks, amount: $scope.Model.Subscription.Amount, billingCycleShift: $scope.Model.Subscription.DaysBeforeBilling } }).success(function (data, status, headers, config) {

                    $scope.Model.SubscriptionItems = data;



                });
            }

            $scope.addSchedule = function (schedule) {

                $http.get('Dashboard/Program/GenerateSubscriptionSchedules').success(function (data, status, headers, config) {

                    $scope.Model.Schedules.push(data);
                });
            };

            $scope.copySchedule = function (schedule) {

                $scope.Model.Schedules.push(schedule);
                $scope.Model.Schedules[$scope.Model.Schedules.length - 1].Id = 0;
            }


            $scope.addPriceOption = function (model) {
                if (!model.Subscriptions) {
                    model.Subscriptions = [];
                }
                $scope.Model.Subscriptions.push({ Id: Math.floor(Math.random() * 999999999), DaysBeforeBilling: '10', Occurances: '1', Amount: 0 });
            }

            $scope.removeSubscription = function (subscriptionId) {
                jbConfirmation
                    .yes(function () {

                        var subscriptions = $scope.Model.Subscriptions;

                        var index = 0;
                        var shouldDelete = false;

                        for (var i = 0; i < $scope.Model.Subscriptions.length; i++) {
                            if ($scope.Model.Subscriptions[i].Id == subscriptionId) {
                                index = i;
                                shouldDelete = true;
                                break;
                            }
                        }

                        if (shouldDelete) {
                            $scope.Model.Subscriptions.splice(index, 1);
                        }

                    })
                    .title('Tuition delete')
                    .message('Are you sure you want to delete this tuition option?')
                    .confirm();
            };

            $scope.removeSchedule = function (index) {

                jbConfirmation
                    .yes(function () {
                        $scope.Model.Schedules.removeByIndex(index);
                    })
                    .title('Schedule delete')
                    .message('Are you sure you want to delete this schedule?')
                    .confirm();
            }

            programService.getSubscriptionStep2(function (data, status, headers, config) {

                $scope.Model = data;

                $scope.updateWeeksCount();

                oldModel.StartDate = data.StartDate;
                oldModel.EndDate = data.EndDate;

                oldModel.Days = [];

                for (var i = 0; i < data.Days.length; i++) {
                    var startTime = data.Days[i].StartTime;
                    var endTime = data.Days[i].EndTime;
                    var isChecked = data.Days[i].IsChecked;
                    var dayOfWeek = data.Days[i].DayOfWeek;

                    oldModel.Days.push({ DayOfWeek: dayOfWeek, StartTime: startTime, EndTime: endTime, IsChecked: isChecked })
                }

                if ($scope.Model.LastCreatedPage.split("Step")[1] < 1) {
                    jbToast.warning("Please setup your program step by step.");
                    $state.go('ProgramAddEditStep' + (Number($scope.Model.LastCreatedPage.split("Step")[1]) + 1), { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $scope.MC.stateParams.programId });
                }

            }, function () {
                jbToast.error("Something went wrong!");
            }, { programType: $stateParams.programType, seasonDomain: $stateParams.seasonDomain, programId: $stateParams.programId });


            $scope.back = function () {

                if ($scope.Model.ViewCalendar) {

                    $scope.errors = null;
                    $scope.Model.ViewCalendar = false;

                }
                else {
                    $state.go('ProgramAddEditStep1', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $stateParams.programId });
                }
            }

            $scope.calendarDataSource = [];


            $scope.saveStep2 = function () {
                save();
            };

            var save = function () {
                programService.setModel($scope.Model);

                var isSchemaChanged = isSubscriptionSchemaChanged();

                programService.saveSubscriptionStep2(isSchemaChanged, function (data, status, headers, config) {
                    if (data.Status) {
                        $state.go('SubscriptionAddEditStep2Point5', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $stateParams.programId });
                    }
                    else {

                        $scope.MC.handleErrors($scope, data);
                    }
                });
            }

            var isEditMode = function () {
                if ($scope.Model.LastCreatedPage == 'Step2' || $scope.Model.LastCreatedPage == 'Step3' || $scope.Model.LastCreatedPage == 'Step4') {
                    return true;
                }
                else {
                    return false;
                }
            }

            var isSubscriptionSchemaChanged = function () {

                if (oldModel.StartDate != $scope.Model.StartDate) {
                    return true;
                }

                if (oldModel.EndDate != $scope.Model.EndDate) {
                    return true;
                }

                for (var i = 0; i < oldModel.Days.length; i++) {

                    for (var j = 0; j < $scope.Model.Days.length; j++) {
                        if ($scope.Model.Days[j].DayOfWeek == oldModel.Days[i].DayOfWeek) {

                            if ($scope.Model.Days[j].StartTime != oldModel.Days[i].StartTime) {
                                return true;
                            }

                            if ($scope.Model.Days[j].EndTime != oldModel.Days[i].EndTime) {
                                return true;
                            }

                            if ($scope.Model.Days[j].IsChecked != oldModel.Days[i].IsChecked) {
                                return true;
                            }
                        }
                    }
                }

                return false;
            }

            var isSchedulesPreviouslyEdited = function () {

                $http.post('Dashboard/Program/IsSubscriptionSchedulesPreviouslyEdited', { programId: $stateParams.programId })
                    .success(function () {

                    })
                    .error(function () {
                    });
            }

            $scope.MonthDays = [];

            for (var i = 1; i <= 28; i++) {
                $scope.MonthDays.push({ Text: i, Value: i });
            }

            $scope.updateWeeksCount = function () {

                var weeksCount = 0;

                $scope.WeeksCount = [];

                for (var i = 0; i < $scope.Model.Days.length; i++) {
                    if ($scope.Model.Days[i].IsChecked) {
                        weeksCount++;
                    }
                }

                for (var i = 1; i <= weeksCount; i++) {
                    $scope.WeeksCount.push({ Text: i, Value: i });
                }
            }

        }])
        .controller('BeforeAfterCareAddEditStep2Controller', ['$scope', '$http', '$log', '$stateParams', '$state', 'programService', 'ProgramSchedulePartsService', 'jbToast', 'jbConfirmation', 'jbRandom', function ($scope, $http, $log, $stateParams, $state, programService, ProgramSchedulePartsService, jbToast, jbConfirmation, jbRandom) {

            $scope.baseUrl = window.location.origin + "/Dashboard/Program";

            window.app.globalObjects.pageClass = 'seasons-view programs-add ' + $stateParams.programType;
            window.app.globalObjects.initializeView();

            $scope.MC.stateParams = $stateParams;

            var oldModel = {};

            angular.forEach($scope.MC.seasons, function (value, key) {
                if (value.Domain == $stateParams.seasonDomain)
                    $scope.MC.currentSeason = value;
            });

            $scope.initDropDown = function () {
                $.Metro.initDropdowns();
            }

            $scope.initAccordions = function () {
                $.Metro.initAccordions();
            }

            $scope.seasonName = $stateParams.seasonDomain;
            $scope.programName = $stateParams.programDomain;

            $scope.formatDate = function (date) {

                if (date != null && date != 'undefined') {
                    return new Date(date).format('mm/dd/yyyy H:mm');
                }
            }

            $scope.scheduleTypes = [];
            $scope.scheduleTypes.push({ Text: 'Monthly', Value: 0 });
            $scope.scheduleTypes.push({ Text: 'Weekly', Value: 1 });




            programService.getBeforeAfterCareStep2(function (data, status, headers, config) {

                $scope.Model = data;

                $scope.OldnumberOfClassDay = $scope.Model.NumberOfClassDays;

                $scope.updateWeeksCount();

                if ($scope.OldnumberOfClassDay != null) {

                    for (var i = 0; i < $scope.OldnumberOfClassDay.length; i++) {
                        if ($scope.OldnumberOfClassDay[i].Value == 1) {

                            $scope.OldnumberOfClassDay[i].Value = true;
                        } else {
                            $scope.OldnumberOfClassDay[i].Value = false;
                        }
                    }

                    $scope.Model.NumberOfClassDays = $scope.OldnumberOfClassDay;
                }

                if ($scope.Model.IsBeforeSchool == false) {
                    $scope.morningTime = true;

                }

                if ($scope.Model.IsAfterSchool == false) {
                    $scope.afternoonTime = true;
                }


                oldModel.StartDate = data.StartDate;
                oldModel.EndDate = data.EndDate;
                oldModel.SelectedPaymentSchaduleMode = data.SelectedPaymentSchaduleMode;
                oldModel.SelectedDueDate = data.SelectedDueDate;
                oldModel.SelectedPaymentSchaduleType = data.SelectedPaymentSchaduleType;
                oldModel.HasProgramActiveOrder = data.HasProgramActiveOrder;

                oldModel.Days = [];

                for (var i = 0; i < data.Days.length; i++) {
                    var beforeStartTime = data.Days[i].MorningStartTime;
                    var beforEendTime = data.Days[i].MorningEndTime;
                    var afterStartTime = data.Days[i].AfternoonStartTime;
                    var afterEndTime = data.Days[i].AfternoonEndTime;
                    var isChecked = data.Days[i].IsChecked;
                    var dayOfWeek = data.Days[i].DayOfWeek;

                    oldModel.Days.push({ DayOfWeek: dayOfWeek, BeforeStartTime: beforeStartTime, BeforeEndTime: beforEendTime, AfterStartTime: afterStartTime, AfterEndTime: afterEndTime, IsChecked: isChecked })
                }

                if ($scope.Model.LastCreatedPage.split("Step")[1] < 1) {
                    jbToast.warning("Please setup your program step by step.");
                    $state.go('ProgramAddEditStep' + (Number($scope.Model.LastCreatedPage.split("Step")[1]) + 1), { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $scope.MC.stateParams.programId });
                }

            }, function () {
                jbToast.error("Something went wrong!");
            }, { programType: $stateParams.programType, seasonDomain: $stateParams.seasonDomain, programId: $stateParams.programId });

            $scope.back = function () {

                if ($scope.Model.ViewCalendar) {

                    $scope.errors = null;
                    $scope.Model.ViewCalendar = false;

                }
                else {
                    $state.go('ProgramAddEditStep1', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $stateParams.programId });
                }
            }

            $scope.calendarDataSource = [];


            $scope.saveStep2 = function () {
                save();
            };

            var IsSchedulePartChanged = false;
            var isSchemaChanged = false;
            var save = function () {
                programService.setModel($scope.Model);

                isSchemaChanged = isBeforeAfterCareSchemaChanged();

                $http.post('Dashboard/Program/ProgramHaveOrders', { programId: $stateParams.programId, isSchemaChanged: isSchemaChanged })
                    .success(function (data, status, headers, config) {
                        if (data.Status) {
                            jbToast.error("Can't change schedule when program has order.");
                        } else {

                            if (oldModel.HasProgramActiveOrder && ($scope.Model.IsProgramEndDateChanged || $scope.Model.IsProgramStartDateChanged)) {

                                $http.post('Dashboard/Program/CheckOrderItemsRelationWithDeletedSchedulePart', { programId: $stateParams.programId, model: $scope.Model, startDate: $scope.Model.StartDate, endDate: $scope.Model.EndDate, isProgramStartDateChanged: $scope.Model.IsProgramStartDateChanged, isProgramEndDateChanged: $scope.Model.IsProgramEndDateChanged })
                                    .success(function (data, status, headers, config) {
                                        if (data.Status) {
                                            if (data.HasDeletedSchedulePart) {
                                                jbConfirmation
                                                    .yes(function () {
                                                        console.log(data.Message)
                                                        jbToast.error(data.Message, "Conflicting existing orders ", { autoHideAfter: 60000 });
                                                    })
                                                    .title('Delete schedules?')
                                                    .message(data.DeletedScheduleParts +'Are you sure you want to proceed?')
                                                    .confirm();
                                            } else {
                                                jbToast.error(data.Message, "Conflicting existing orders ", { autoHideAfter: 60000 });
                                            }
                                            
                                        } else {

                                            $scope.Model.DeletedProgramSchedulePartsByStartDate = data.DeletedPartsByStartDate;
                                            $scope.Model.DeletedProgramSchedulePartsByEndDate = data.DeletedPartsByEndDate;
                                            $scope.CheckSchedulePartChanged($stateParams.programId, $scope.Model);
                                        }
                                    });

                            } else {
                                $scope.CheckSchedulePartChanged($stateParams.programId, $scope.Model);
                            }
                        }
                    });
            }


            $scope.CheckSchedulePartChanged = function (programId, step2Model) {

                $http.post('Dashboard/Program/IsProgramSchedulePartChanged', { programId: programId, model: step2Model })
                    .success(function (data, status, headers, config) {
                        if (data.status) {
                            IsSchedulePartChanged = data.Changed;
                            $scope.nextStep();
                        }
                    });
            }

            $scope.nextStep = function () {

                if (IsSchedulePartChanged) {

                    jbConfirmation
                        .yes(function () {
                            ProgramSchedulePartsService.set(IsSchedulePartChanged);
                            var isSchemaChanged = isBeforeAfterCareSchemaChanged();
                            programService.saveBeforeAfterCareStep2(isSchemaChanged, IsSchedulePartChanged, function (data, status, headers, config) {
                                if (data.Status) {
                                    $state.go('BeforeAfterCareAddEditStep2Fees', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $stateParams.programId });
                                }
                                else {

                                    $scope.MC.handleErrors($scope, data);
                                }
                            });

                        })
                        .title('Overwrite schedule?')
                        .message('If you save these changes, it will overwrite the edits to the schedule dates that you had previously made. Are you sure you want to continue?')
                        .confirm();

                } else {
                    ProgramSchedulePartsService.set(IsSchedulePartChanged);
                    var isSchemaChanged = isBeforeAfterCareSchemaChanged();
                    programService.saveBeforeAfterCareStep2(isSchemaChanged, IsSchedulePartChanged, function (data, status, headers, config) {
                        if (data.Status) {
                            $state.go('BeforeAfterCareAddEditStep2Fees', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $stateParams.programId });
                        }
                        else {

                            $scope.MC.handleErrors($scope, data);
                        }
                    });
                }
            }
            var isEditMode = function () {
                if ($scope.Model.LastCreatedPage == 'Step2' || $scope.Model.LastCreatedPage == 'Step3' || $scope.Model.LastCreatedPage == 'Step4') {
                    return true;
                }
                else {
                    return false;
                }
            }

            var isBeforeAfterCareSchemaChanged = function () {

                if (oldModel.StartDate != $scope.Model.StartDate) {
                    if (oldModel.HasProgramActiveOrder) {
                        $scope.Model.IsProgramStartDateChanged = true;
                    } else {
                        $scope.Model.IsProgramStartDateChanged = true;
                        return true;
                    }
                }

                if (oldModel.EndDate != $scope.Model.EndDate) {

                    if (oldModel.HasProgramActiveOrder) {
                        $scope.Model.IsProgramEndDateChanged = true;
                    } else {
                        $scope.Model.IsProgramEndDateChanged = true;
                        return true;
                    }
                }

                if (oldModel.SelectedPaymentSchaduleMode != $scope.Model.SelectedPaymentSchaduleMode) {
                    return true;
                }
                if (oldModel.SelectedDueDate != $scope.Model.SelectedDueDate) {
                    return true;
                }

                if (oldModel.SelectedPaymentSchaduleMode == 'Monthly' && oldModel.SelectedPaymentSchaduleType != $scope.Model.SelectedPaymentSchaduleType) {
                    return true;
                }

                for (var i = 0; i < oldModel.Days.length; i++) {

                    for (var j = 0; j < $scope.Model.Days.length; j++) {

                        if ($scope.Model.Days[j].DayOfWeek == oldModel.Days[i].DayOfWeek) {

                            if ($scope.Model.Days[j].IsChecked != oldModel.Days[i].IsChecked) {
                                return true;
                            }
                        }
                    }
                }

                return false;
            }

            $scope.MonthDays = [];

            for (var i = 1; i <= 28; i++) {
                $scope.MonthDays.push({ Text: i, Value: i });
            }

            $scope.ShowNumbeerOfClass = false;

            $scope.updateWeeksCount = function () {

                var weeksCount = 0;

                $scope.WeeksCount = [];

                for (var i = 0; i < $scope.Model.Days.length; i++) {
                    if ($scope.Model.Days[i].IsChecked) {
                        weeksCount++;
                    }
                }

                for (var i = 1; i <= weeksCount; i++) {
                    $scope.WeeksCount.push({ Text: i, Value: false });
                }
                if ($scope.WeeksCount.length > 0) {
                    $scope.ShowNumbeerOfClass = true;
                }
                $scope.Model.NumberOfClassDays = $scope.WeeksCount;

                if ($scope.OldnumberOfClassDay != null) {

                    for (var i = 0; i < $scope.Model.NumberOfClassDays.length; i++) {

                        for (var j = 0; j < $scope.OldnumberOfClassDay.length; j++) {

                            if ($scope.Model.NumberOfClassDays[i].Text == $scope.OldnumberOfClassDay[j].Text) {

                                $scope.Model.NumberOfClassDays[i].Value = $scope.OldnumberOfClassDay[j].Value;
                            }

                        }
                    }
                }
            }

            $scope.DisabaleBeforeSchool = function (IsBeforeSchool, scheduleId) {


                if ($scope.Model.IsCombo && !IsBeforeSchool) {

                    jbToast.error("You cannot delete this program because you have a combo program.");
                    $scope.Model.IsBeforeSchool = true;
                } else {

                    if (!IsBeforeSchool && scheduleId > 0) {

                        $http.post('Dashboard/Program/IsScheduleHaveOrder', { programId: $stateParams.programId, scheduleId: scheduleId })
                            .success(function (data, status, headers, config) {
                                if (data.Status) {
                                    jbToast.error("You cannot delete this program because it has orders.");
                                    $scope.Model.IsBeforeSchool = true;
                                    $scope.morningTime = true;
                                } else {
                                    $scope.Model.IsBeforeSchool = false;
                                    $scope.morningTime = true;
                                }
                            });

                    } else {

                        if (IsBeforeSchool) {
                            $scope.morningTime = false;

                        } else {
                            $scope.Model.IsBeforeSchool = false;
                            $scope.morningTime = true;
                        }
                    }
                }
            }

            $scope.DisabaleAfterSchool = function (IsAfterSchool, scheduleId) {

                if ($scope.Model.IsCombo && !IsAfterSchool) {

                    jbToast.error("You cannot delete this program because you have a combo program.");
                    $scope.Model.IsAfterSchool = true;
                } else {

                    if (!IsAfterSchool && scheduleId > 0) {

                        $http.post('Dashboard/Program/IsScheduleHaveOrder', { programId: $stateParams.programId, scheduleId: scheduleId })
                            .success(function (data, status, headers, config) {

                                if (data.Status) {

                                    jbToast.error("You cannot delete this program because it has orders.");
                                    $scope.Model.IsAfterSchool = true;
                                    $scope.afternoonTime = false;

                                } else {
                                    $scope.Model.IsAfterSchool = false;
                                    $scope.afternoonTime = true;
                                }
                            });

                    } else {

                        if (IsAfterSchool) {

                            $scope.afternoonTime = false;
                        } else {

                            $scope.Model.IsAfterSchool = false;
                            $scope.afternoonTime = true;

                        }
                    }
                }
            }

            $scope.DisableCombo = function (isCombo, scheduleId) {
                if (!$scope.Model.IsAfterSchool || !$scope.Model.IsBeforeSchool) {

                    jbToast.error("In order to choose the combo program, you must first choose both the before-school and after-school programs.");
                    $scope.Model.IsCombo = false;
                }
                else {

                    if (!isCombo && scheduleId > 0) {

                        $http.post('Dashboard/Program/IsScheduleHaveOrder', { programId: $stateParams.programId, scheduleId: scheduleId })
                            .success(function (data, status, headers, config) {
                                if (data.Status) {
                                    jbToast.error("You cannot delete this program because it has orders.")
                                    $scope.Model.IsCombo = true;
                                } else {
                                    $scope.Model.IsCombo = false;

                                }
                            });

                    } else {

                        if (!isCombo) {
                            $scope.Model.IsCombo = false;
                        } else {
                            $scope.Model.IsCombo = true;
                        }
                    }
                }
            }
        }]);
})();