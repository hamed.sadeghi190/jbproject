﻿using System;

namespace Jumbula.Common.Exceptions
{
    public class JumbulaEmailNotMatchedException : JumbulaClubImportException
    {
        public JumbulaEmailNotMatchedException(Exception e)
            : base(e)
        {

        }
    }
}
