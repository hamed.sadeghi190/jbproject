﻿using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;

namespace Jumbula.Core.Domain
{
    public class FamilyContact : BaseEntity, IProtectedResource
    {
        [ForeignKey("FamilyId")]
        public virtual Family Family { get; set; }

        public int FamilyId { get; set; }

        [ForeignKey("ContactId")]
        public virtual ContactPerson Contact { get; set; }

        public int ContactId { get; set; }

        public FamilyContactType Type { get; set; }
        public Relationship? Relationship { get; set; }
        public FamilyContactStatusType Status { get; set; }
        public bool IsEmergencyContact { get; set; }

        public bool DoesTheClubOwnIt(int clubId)
        {
            return false;
        }

        public bool DoesTheParentOwnIt(int userId)
        {
            if (Family == null || (Family != null && Family.UserId == userId))
                return true;

            return false;
        }
    }
    public enum FamilyContactStatusType
    {
        Active,
        Deleted = 255 // to avoid changing after adding any item to this enum
    }
    public enum FamilyContactType : byte
    {
        Contact,
        Emergency,
        AuthorizedPickup,

    }
}
