﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class PageMetaDataViewModel
    {
        public string PageTitle { get; set; }

        public string MetaDesc { get; set; }

        public string MetaKeywords { get; set; }

        public PageMetaDataViewModel()
        {
        }
    }
}