﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum OrderItemStatusCategories : byte
    {
        [Description("Not initiated")]
        notInitiated = 0,
        [Description("Initiated")]
        initiated = 1,
        [Description("Completed")]
        completed = 3,
        [Description("Changed")]
        changed = 4,
        [Description("Deleted")]
        deleted = 5,
        [Description("ShowAll")]
        showAll = 101
    }
}
