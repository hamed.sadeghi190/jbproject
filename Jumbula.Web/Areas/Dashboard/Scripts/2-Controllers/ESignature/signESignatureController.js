﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('signESignatureController', ['$scope', '$http', '$stateParams', '$state', 'jbToast', function ($scope, $http, $stateParams, $state, jbToast) {
        window.app.globalObjects.pageClass = 'invoice_View';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + '/Dashboard/ESignature';


        $scope.Model = {};


        $http.get($scope.baseUrl + '/GetDocumentSigned?documentId=' + $stateParams.documentId).success(function (data, status, headers, config) {

            $scope.Model = data;

            if ($scope.Model.Signature != null) {

                var data = $("#" + "JbSignature480796241").text("").jSignature({
                    lineWidth: 1,
                    height: 120,
                }).jSignature("setData", $scope.Model.Signature, "base30").jSignature("getData");
                $("#Image_JbSignature480796241").attr("src", data);
            }
             
        });
        function showAllParents(element) {
            $(element).parents().each(function () {
                var oldDisplay = $(this).css("display");
                if (oldDisplay == "none") {
                    $(this).data("old-display", oldDisplay).css("display", "block")
                }
            });
        }
        function hideAllParents(element) {
            $(element).parents().each(function () {
                if ($(this).data("old-display") == "none") {
                    $(this).css("display", "")
                }
            });
        }
        showAllParents("#" + "JbSignature781852516");
        $("#" + "JbSignature781852516").jSignature({
            lineWidth: 1,
            height: 120,
            width: $("#" + "JbSignature781852516").outerWidth() - 2
        }).bind('change', function (e) {
            $('.clear-signature[data-for="JbSignature781852516"]').show(0);
            $("#" + "JbSignature781852516" + "_Value").val($(this).jSignature("getData", "base30"));
        });
        hideAllParents("#" + "JbSignature781852516");

        $('.clear-signature[data-for="JbSignature781852516"]').click(function (event) {
            event.preventDefault();
            $("#" + $(this).attr("data-for")).jSignature('clear');
        }).hide(0);

        $scope.SaveSignature = function () {

            $scope.Model.Signature = document.getElementById("JbSignature781852516_Value").value;
           
            $http.post($scope.baseUrl + '/SaveSignatureDocument', { model: $scope.Model })
         .success(function (data, status, headers, config) {
             if (data.Status == true) {
                 jbToast.success(data.Message);
                 $state.go('ESignature');
             }
             else {
                 
                 $scope.MC.handleErrors($scope, data);
             }
         });
        }
       
        $scope.cancelAction = function () {
            if ("comesFromOrderItem" in window) {
                if (window.comesFromOrderItem) {
                    window.comesFromOrderItem = false;
                    $state.go("ESignature", null);
                }
            }
            else {
                $state.go("ESignature", null);
            }
        }

    }])

})();