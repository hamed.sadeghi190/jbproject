﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('toursJbHomeSiteController', ['$scope', '$http', '$sce', '$stateParams', function ($scope, $http, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'settings-tours-view';
        window.app.globalObjects.initializeView();

        //window.app.globalObjects.pageClass = 'tours';

        $scope.MC.stateParams = $stateParams;
        $scope.Model = {
            Tours: [
                {
                    YouTubeId: "-Bcnga0qMNI",
                    Title: "How to create a class",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "TCSFh4ymbUA",
                    Title: "How to create a custom discount?",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },

                {
                    YouTubeId: "B0UffieZ5X4",
                    Title: "How to create a multi-program discount?",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "cVi7DX2jLN4",
                    Title: "How to create a sibling discount?",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
            ]
        };

    }]);
})();