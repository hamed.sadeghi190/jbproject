﻿

using System.ComponentModel.DataAnnotations;

namespace Jumbula.Common.Enums
{
    public enum OrderMode : byte
    {
        [Display(Name = "Online")]
        Online = 0,
        [Display(Name = "Offline")]
        Offline = 1,
    }
}
