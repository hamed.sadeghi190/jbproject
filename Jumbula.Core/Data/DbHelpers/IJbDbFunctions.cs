﻿using System;

namespace Jumbula.Core.Data.DbHelpers
{
    public interface IJbDbFunctions
    {
        DateTime? TruncateTime(DateTime? dateValue);
    }
}
