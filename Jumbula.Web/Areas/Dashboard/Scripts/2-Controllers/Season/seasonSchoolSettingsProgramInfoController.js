﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('seasonSchoolSettingsProgramInfoController', ['$scope', '$http', 'jbToast', '$stateParams', function ($scope, $http, jbToast, $stateParams) {
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/SeasonSchoolSettingsRegistration", order: 4, menu: "SEASONS" });
        $scope.MC.stateParams = $stateParams;

        $http.get('Dashboard/Season/GetSchoolSettingProgramInfo?seasonDomain=' + $stateParams.seasonDomain).success(function (data, status, headers, config) {

            $scope.Model = data;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.save = function () {

            $http.post('Dashboard/Season/SaveSchoolSettingProgramInfo', { model: $scope.Model, seasonDomain: $stateParams.seasonDomain })
             .success(function (data, status, headers, config) {

                 if (data.Status == true) {
                     $scope.errors = null;
                     jbToast.success("Program information updated successfully.", "");
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }
    }])

})();