﻿using Jb.Framework.Common;
using System.Collections.Generic;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IFieldSettingBusiness
    {
        JbFieldSetting GetFieldSettings(JbClass jbClass, int clubId, string fieldName);

        List<JbFieldSetting> GetFieldSettings(JbClass jbClass, int clubId);

        bool GetFieldActiveStatus(List<JbFieldSetting> fieldSettings, string fieldName);
        bool GetFieldShowInGridStatus(List<JbFieldSetting> fieldSettings, string fieldName);

        OperationStatus SaveSettings(List<JbFieldSetting> fieldSettings);

    }
}
