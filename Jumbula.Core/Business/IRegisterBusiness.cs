﻿using Jb.Framework.Common.Forms;
using System.Collections.Generic;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Business
{
    public interface IRegisterBusiness
    {
        void MultiRegisterAddEditStep4(List<JbForm> followUpForms, long ists, bool hasWaiver, bool hasPaymentPlant, int userId);

        void CheckDuplicateEnrollment(List<OrderItem> regularOrderItems);

        string IsConflictPrograms(long currentProgramId, List<long> programIds);
    }
}
