﻿using System;

namespace Jumbula.Common.Exceptions
{
    public class JumbulaAccountCannotResetPasswordException : JumbulaAccountException
    {
        public JumbulaAccountCannotResetPasswordException(Exception e)
            : base(e)
        {
        }
    }

}
