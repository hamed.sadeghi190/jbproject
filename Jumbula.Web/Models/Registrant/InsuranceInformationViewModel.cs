﻿using Jb.Framework.Common.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SportsClub.Models
{
    public class InsuranceInformationViewModel
    {
        public int Id { get; set; }

        public int UserId { get; set; }
        public string InsuranceCompanyName { get; set; }

        public string InsurancePolicyNumber { get; set; }

        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "The phone number is invalid.")]
        public string InsurancePhone { get; set; }
    }
    
}