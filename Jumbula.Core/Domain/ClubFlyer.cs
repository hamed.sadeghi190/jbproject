﻿using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class ClubFlyer : BaseEntity
    {
        public int ClubId { get; set; }
        public long SeasonId { get; set; }

        [ForeignKey("SeasonId")]
        public virtual Season Season { get; set; }

        [ForeignKey("ClubId")]
        public virtual Club  Club { get; set; }

        public int FlyerId { get; set; }

        [ForeignKey("FlyerId")]
        public virtual JbFlyer Flyer { get; set; }
    }
}
