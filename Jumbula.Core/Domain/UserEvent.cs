﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class UserEvent : BaseEntity
    {
        public long? ProgramId { get; set; }

        [ForeignKey("ProgramId")]
        public virtual Program Program { get; set; }

        public int? SessionId { get; set; }

        public DateTime LocalDateTime { get; set; }

        public DateTime ClubDateTime { get; set; }

        [ForeignKey("UserId")]
        public virtual JbUser User { get; set; }

        public int UserId { get; set; }

        [ForeignKey("AddressId")]
        public virtual PostalAddress Address { get; set; }

        public int AddressId { get; set; }

        public int? ClubId { get; set; }

        [ForeignKey("ClubId")]
        public virtual Club Club { get; set; }

        public UserEventType Type { get; set; }

    }

    public enum UserEventType : byte
    {
        Checkin = 0,
        Checkout = 1
    }

}
