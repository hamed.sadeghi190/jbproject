﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Jumbula.Web.Infrastructure;

namespace Jumbula.Web.Controllers.Api
{
    public class CategoryController : ApiController
    {
        public IEnumerable<KeyValuePair<int, string>> GetAllCategories()
        {

            var categories = Ioc.CategoryBuisness.GetList().Select(s =>
                new KeyValuePair<int, string>(s.Id, s.Name)).ToList();

            return categories;
        }
    }
}
