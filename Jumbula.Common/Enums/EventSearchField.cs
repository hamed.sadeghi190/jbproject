﻿

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jumbula.Common.Enums
{
    public class EventSearchField
    {
        [DisplayName("Minimum")]
        [Range(1, Constants.Constants.M_MaxAge, ErrorMessage = "{0} should be a number between {1} and {2}")]
        public int? MinAge { get; set; }

        [DisplayName("Maximum")]
        [DefaultValue(120)]
        [Range(1, Constants.Constants.M_MaxAge, ErrorMessage = "{0} should be a number between {1} and {2}")]
        public int? MaxAge { get; set; }

        [DisplayName("Minimum")]
        public SchoolGradeType? MinGrade { get; set; }

        [DisplayName("Maximum")]
        public SchoolGradeType? MaxGrade { get; set; }

        public Level? Level { get; set; }

        public Genders? Gender { get; set; }

        public EventSearchField()
        {
            Gender = Genders.NoRestriction;
        }

    }
}
