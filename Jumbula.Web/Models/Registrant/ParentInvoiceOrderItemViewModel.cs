﻿using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class ParentInvoiceOrderItemViewModel : BaseViewModel<OrderItem, long>
    {
        public string Attendee { get; set; }
        public decimal EntryFee { get; set; }
        public decimal Balance { get; set; }
        public decimal InvoiceAmount { get; set; }
        public decimal PaidAmount { get; set; }
        public string EntryFeeName { get; set; }
        public PaymentPlanType PaymentplanType { get; set; }
        public long OrderId { get; set; }
        public long ProgramScheduleId { get; set; }
        public string ProgramName { get; internal set; }
        public bool IsCancel { get; set; }
    }
}