﻿using Jb.Framework.Common;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class StripeWebhookBusiness : IStripeWebhookBusiness
    {
        private readonly IRepository<StripeWebhook, long> _stripeWebhookRepository;

        public StripeWebhookBusiness(IRepository<StripeWebhook, long> stripeWebhookRepository)
        {
            _stripeWebhookRepository = stripeWebhookRepository;
        }

        public OperationStatus Create(StripeWebhook stripeWebhook)
        {
            return _stripeWebhookRepository.Create(stripeWebhook);
        }
    }
}
