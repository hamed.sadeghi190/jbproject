﻿using System;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure.DataMigration;
using NPOI.Util;

namespace Jumbula.Web.Controllers
{
    [Authorize(Roles = "SysAdmin, Support")]
    public class DataManipulationController : Controller
    {
        IDataManipulationService Service;
        public DataManipulationController(IDataManipulationService service)
        {
            Service = service;

            Service.SetControllerContext(this.ControllerContext);
        }

        [HttpGet]
        public ActionResult Index()
        {
            var model = new DataManipulationHomeViewModel()
            {
                MigrationItems = Service.GetDataManipulationClasses()
                 .Select(d =>
                   new DataManipulationItemViewModel()
                   {
                       Id = 0,
                       Name = d.Name.ToString(),
                       Title = d.Title
                   })
                   .ToList()
            };

            return View(model);
        }

        [HttpGet]
        public virtual ActionResult Run(string name, int? id, int? startPoint = 1, int? endPoint = null)
        {
            var model = new DataManipulationRunViewModel();

            var dataManipulation = Service.GetDataManipulationClass(name);

            var dataManipulationResults = Service.GetResults(name);

            model.Id = Service.GetIdOfLastItem(name);
            model.Name = dataManipulation.Name;
            model.Title = dataManipulation.Title;
            model.HasSource = dataManipulation.HasSource;
            model.PageMode = RunPageMode.Run;
            model.Upload = new DataMigrationUploadViewModel();
            model.StartPoint = startPoint;
            model.EndPoint = endPoint;

            if (dataManipulationResults.Any())
            {
                model.ResultItems = dataManipulationResults.Select(d =>
                    new DataMigrationResultItemViewModel
                    {
                        Id = d.Id,
                        RunDateTime = d.RunDate.Value
                    })
                    .ToList();
            }

            return View(model);
        }

        [HttpPost]
        [ActionName("RunManipulation")]
        public virtual ActionResult Run(int id, int? from = null, int? endPoint = null)
        {

            StaticValueClass.Reset();

            var result = Service.Manipulate(id, from, endPoint);

            return Json(result);
        
        }
  
        [HttpGet]
        public ActionResult RunReport(int id, string token = null)
        {
            var result = Service.RunReport(id, token);

            return View("Report", result);
        }

        [HttpPost]
        public virtual ActionResult Upload()
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    byte[] fileBytes = IOUtils.ToByteArray(file.InputStream);

                    MvcApplication.DataManipulationSourceFile = fileBytes;
                }
            }

            return Redirect(Request.UrlReferrer.PathAndQuery);
        }

        [HttpGet]
        public virtual ActionResult DownloadSource(int id)
        {
            var dataMigration = Service.Get(id);
            return File(dataMigration.Source, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("{0}_Source.xlsx", dataMigration.Name));
        }

        [HttpGet]
        public virtual ActionResult DownloadResult(int id)
        {
            var dataMigration = Service.Get(id);
            return File(dataMigration.Result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format("{0}_Result.xlsx", dataMigration.Name));
        }


        public ActionResult GetWrongMedicalElements(bool rasOnly, int from = 0, int to = int.MaxValue)
        {
            var result = string.Empty;

            result = Service.GetWrongMedicalElements(rasOnly, from, to);

            return Content(result);
        }

        public ActionResult GetWrongSchoolElements(int from = 0, int to = int.MaxValue)
        {
            var result = string.Empty;

            result = Service.GetWrongSchoolElements(from, to);

            return Content(result);
        }

        public ActionResult GetWrongMedicalNutElements(bool rasOnly, int from = 0, int to = int.MaxValue)
        {
            var result = string.Empty;

            result = Service.GetWrongMedicalNutElements(rasOnly, from, to);

            return Content(result);
        }

        public ActionResult GetWrongParent1Elements(bool rasOnly, int from = 0, int to = int.MaxValue)
        {
            var result = string.Empty;

            result = Service.GetWrongParent1Elements(rasOnly, from, to);

            return Content(result);
        }

        public ActionResult GetWrongParent2Elements(bool rasOnly, int from = 0, int to = int.MaxValue)
        {
            var result = string.Empty;

            result = Service.GetWrongParent2Elements(rasOnly, from, to);

            return Content(result);
        }

        public ActionResult GetWrongEmergencyElements(bool rasOnly, int from = 0, int to = int.MaxValue)
        {
            var result = string.Empty;

            result = Service.GetWrongEmergencyElements(rasOnly, from, to);

            return Content(result);
        }

        public ActionResult GetWrongAuthorizePickup1Elements(bool rasOnly, int from = 0, int to = int.MaxValue)
        {
            var result = string.Empty;

            result = Service.GetWrongAuthorizePickup1Elements(rasOnly, from, to);

            return Content(result);
        }

        public ActionResult GetWrongAuthorizePickup2Elements(bool rasOnly, int from = 0, int to = int.MaxValue)
        {
            var result = string.Empty;

            result = Service.GetWrongAuthorizePickup2Elements(rasOnly, from, to);

            return Content(result);
        }

        public ActionResult GetWrongAuthorizePickup3Elements(bool rasOnly, int from = 0, int to = int.MaxValue)
        {
            var result = string.Empty;

            result = Service.GetWrongAuthorizePickup3Elements(rasOnly, from, to);

            return Content(result);
        }

        public ActionResult GetWrongAuthorizePickup4Elements(bool rasOnly, int from = 0, int to = int.MaxValue)
        {
            var result = string.Empty;

            result = Service.GetWrongAuthorizePickup4Elements(rasOnly, from, to);

            return Content(result);
        }

        public ActionResult GetWrongMedical2Elements(bool rasOnly, int from = 0, int to = int.MaxValue)
        {
            var result = string.Empty;

            result = Service.GetWrongMedical2Elements(rasOnly, from, to);

            return Content(result);
        }

        public ActionResult GetWrongInsuranceElements(bool rasOnly, int from = 0, int to = int.MaxValue)
        {
            var result = string.Empty;

            result = Service.GetWrongInsuranceElements(rasOnly, from, to);

            return Content(result);
        }

        public ActionResult GetWrongPermissionPhotographyElements(bool rasOnly, int from = 0, int to = int.MaxValue)
        {
            var result = string.Empty;

            result = Service.GetWrongPermissionPhotographyElements(rasOnly, from, to);

            return Content(result);
        }

        public ActionResult GetWrongSchool2Elements(bool rasOnly, int from = 0, int to = int.MaxValue)
        {
            var result = string.Empty;

            result = Service.GetWrongSchool2Elements(rasOnly, from, to);

            return Content(result);
        }

        public ActionResult GetAllEmFormElements()
        {
            var result = string.Empty;

            result = Service.GetAllEmFormElements();

            return Content(result);
        }
    }
}