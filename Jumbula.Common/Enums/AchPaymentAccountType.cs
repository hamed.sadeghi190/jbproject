﻿

namespace Jumbula.Common.Enums
{
    public enum AchPaymentAccountType : byte
    {
        Individual = 1,
        Company = 2
    }
}
