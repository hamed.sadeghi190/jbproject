﻿using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class FlyerTemplate1ViewModel : BaseFlyerTemplateViewModel
    {
        public FlyerTemplate1ViewModel()
            : base()
        {
            Body = new FlyerTemplate1BodyViewModel();
        }
        public new FlyerTemplate1BodyViewModel Body { get; set; }
    }

    public class FlyerTemplate1BodyViewModel : BaseFlyerBodyViewModel
    {
        public FlyerTemplate1BodyViewModel()
        {
            ProgramSchedulesPM = new List<FlyerProgramItemViewModel>();
            ProgramSchedulesAM = new List<FlyerProgramItemViewModel>();

        }
        public List<FlyerProgramItemViewModel> ProgramSchedulesPM { get; set; }
        public List<FlyerProgramItemViewModel> ProgramSchedulesAM { get; set; }

        public string MinMaxTimeAM { get; set; }
        public string MinMaxTimePM { get; set; }

        public string StrschedulePMTimes {

            get
            {
                if (SchedulePMTimesWithDistinct != null && SchedulePMTimesWithDistinct.Any())
                {
                    return string.Join(", ", SchedulePMTimesWithDistinct);
                }
                return string.Empty;
            }
        }


        public List<string> SchedulePMTimesWithDistinct
        {
            get
            {
                var listPMTimes = new List<string>();
                foreach (var time in SchedulePMTimes)
                {
                    listPMTimes.Add(string.Format("{0} to {1}", DateTime.Today.Add(time[0]).ToString("h:mmtt"), DateTime.Today.Add(time[1]).ToString("h:mmtt")));
                }
                return listPMTimes.Distinct().OrderBy(q => q).ToList();
            }
            }
        public string  StrscheduleAMTimes
        {
            get
            {
                if (ScheduleAMTimesWithDistinct != null && ScheduleAMTimesWithDistinct.Any())
                {
                    return string.Join(", ", ScheduleAMTimesWithDistinct);
                }
                return string.Empty;
            }
            
        }

        public List<string> ScheduleAMTimesWithDistinct
        {
            get
            {
                var listAMTimes = new List<string>();
                foreach (var time in ScheduleAMTimes)
                {
                    listAMTimes.Add(string.Format("{0} to {1}", DateTime.Today.Add(time[0]).ToString("h:mmtt"), DateTime.Today.Add(time[1]).ToString("h:mmtt")));
                }
                return listAMTimes.Distinct().OrderBy(q => q).ToList();

            }

        }

        public List<TimeSpan[]> SchedulePMTimes
        {
            get
            {
                var result = ProgramSchedulesPM.SelectMany(p => p.Details).Where(p => p.ProgramName != "").Select(d => new[]{d.StartTime, d.EndTime}).ToList();

                return result;
            }
        }
        public List<TimeSpan[]> ScheduleAMTimes
        {
            get
            {
                var result = ProgramSchedulesAM.SelectMany(p => p.Details).Distinct().Where(p => p.ProgramName != "").Select(d => new[] { d.StartTime, d.EndTime}).ToList();
        
                return result;
            }
        }

        public int CountPMAndAM { get; set; }
        public string Comments { get; set; }
        public bool IsGreenColor { get; set; }
        public string RootSiteUrl { get; set; }
        public string ClubUrl { get; set; }
    }
}