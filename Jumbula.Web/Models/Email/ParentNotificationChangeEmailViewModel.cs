﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models.Email
{
    public class ParentNotificationChangeEmailViewModel
    {
        public string NewUserEmail { get; set; }
        public string UserEmail { get; set; }
        public string HomeTitle { get; set; }

        public string LogoUrl { get; set; }

    }
}