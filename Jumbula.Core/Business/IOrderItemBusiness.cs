﻿using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IOrderItemBusiness
    {
        OrderItem GetItem(long orderItemId);

        IQueryable<ChessTourneyEntryItem> ChessTourneyEntries(long programId, long seasonId, int clubId);
        IEnumerable<OrderItem> GetProgramOrderItems(long programId, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed);
        IQueryable<OrderItem> GetReportOrderItems(List<long> programIds, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed);
        IQueryable<OrderItem> GetReportOrderItemsBySchedule(long scheduleId, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed);
        IQueryable<OrderItem> GetCustomReportOrderItems(List<long> scheduleIds, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed);
        List<OrderItem> GetSubscriptionReportOrderItems(long programId, DateTime date);
        int RegisteredCount(long chargeId, bool isTestMode);
        IQueryable<OrderItem> GetOrderItems(long orderId, int userId, bool? isTestMode);
        IQueryable<OrderItem> GetOrderItems(List<long> orderItemIds);
        IQueryable<OrderItem> GetCompletetCancelOrderItems(long orderId);
        IQueryable<OrderItem> GetCompletetCancelOrderItems(List<int> clubIds);
        IQueryable<OrderItem> GetClubCompletetCancelOrderItems(int clubId);
        IQueryable<OrderItem> GetOrderItems(long programId, bool isTestMode, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed);
        IQueryable<OrderItem> GetOrderItems(long programId, int userId, bool isTestMode, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed);
        IQueryable<OrderItem> GetOrderItems(int userId, long seasonId, bool isTestMode, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed);
        IQueryable<OrderItem> GetOrderItems(long clubId, long? seasonId, bool? isTestMode, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed, bool hasPartner = false);
        IQueryable<OrderItem> GetOrderItems(long clubId, string term, bool isTestMode, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed, int? instructorId = null, ParticipantSearchType? searchType = null, string firstName = null, string lastName = null, bool hasPartner = false);
        IQueryable<OrderItem> GetUserOrderItems(int userId, bool? isTestMode, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed);
        IQueryable<OrderItem> GetClubUserOrderItems(int clubId, int userId, bool? isTestMode, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed);
        IQueryable<OrderItem> GetList();
        IQueryable<OrderItem> GetClubOrderItems(int clubId);
        IQueryable<OrderItem> GetOrderItemByISTS(long ists, int userId, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed);
        IQueryable<OrderItem> GetOrderItemByISTS(long ists);
        IQueryable<OrderItem> GetOrderItemByTransactionDate(List<long> ids, DateTime startDate, DateTime endDate, TransactionStatus transactionStatus = TransactionStatus.Success);
        OperationStatus Delete(long orderItemId);
        OperationStatus DeleteItems(List<OrderItem> orderItems);
        OperationStatus Update(OrderItem orderItem);
        OperationStatus Edit(OrderItem orderItem, List<long> forDeleteIds, List<long> forDeleteSessionsIds, List<long> forActiveIds, int userId, List<OrderSession> deletedOrderSessions, string editMemo, List<ProgramSession> addProrgramSessions, List<DayOfWeek> days, List<int> forDeletePaidInFullIds);
        OperationStatus Cancel(long orderItemId, int userId, string cancelMemo, decimal refundFee = 0, decimal prorateFee = 0);
        TransactionActivity SetRefundTransaction(OrderItem orderItem, long? installmentId, PaymentDetail paymentDetail, TransactionStatus transactionStatus, decimal transactionAmount, DateTime date, string note, HandleMode handleMode = HandleMode.Online);
        TransactionActivity SetPayTransaction(OrderItem orderItem, long? installmentId, PaymentDetail paymentDetail, TransactionStatus transactionStatus, decimal transactionAmount, DateTime date, string note);
        OperationStatus DeleteByISTS(long ists);
        decimal CalculateTotalAmount(OrderItem orderItem, bool realResultIfNegative = false);
        OperationStatus StopInatallmentsPayment(int id);
        OperationStatus ResumeInatallmentsPayment(int id);
        void InitiateOrderItem(OrderItem orderItem, bool hasWaiver = false, bool hasPaymentPlant = false, bool hasFollowUpForm = false);
        IQueryable<OrderItem> GetOrderItems(List<int> clubIds, bool? isTestMode, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed);

        OperationStatus UpdateOrderItems(List<OrderItem> orderItems);
        OperationStatus EditDropIn(OrderItem orderItem, List<OrderSession> deletedSessions, List<OrderSession> UpdatedSessions, int userId, string stringDates, string editMemo);
        IQueryable<OrderItem> GetUserOrderItems(int userId);
        OperationStatus CancelDropIn(OrderItem orderItem, int userId, decimal cancellationFee, string cancelMemo);
        IQueryable<OrderItem> GetClubUserOrderItemsInSeason(int clubId, int userId, long seasonId, bool? isTestMode);
        OperationStatus RemoveOrderItemCharges(List<OrderChargeDiscount> charges);
        decimal CalculateOrderItemSurCharge(OrderItem orderItem);
        IQueryable<OrderItem> GetFSAReport(string filterType, int userId, ref int? year, ref DateTime? startDate, ref DateTime? endDate);
        List<OrderItemSchedulePart> SetOrderItemsScheduleParts(OrderItem orderItem, DateTime? desiredStartDate, long selectedChargeId = 0);
        decimal CalculateOneSurCharge(OrderItem orderItem, OrderChargeDiscount surCharge, decimal partAmount = 0);
        decimal UpdateOrderItemChargeDiscounts(OrderItem orderItem, int installmentCount, DateTime? newDesiredStartDate, List<ProgramSchedulePart> listItemsParts = null, bool installmentIsDeleted = false);
        decimal calculateDiscountAmount(List<OrderChargeDiscount> discounts, decimal amount);
        decimal CalculateSessionAmount(List<OrderChargeDiscount> chargeDiscount, OrderSession session, int sessionsCount);
        IQueryable<OrderItem> GetComplatedOrderitems();
        IQueryable<OrderItem> GetDelinquentFullPaidItems(int clubId, DelinquentPolicy delinquentPolicy);
        IQueryable<OrderInstallment> GetDelinquentOrderInstallmentItems(int clubId, DelinquentPolicy delinquentPolicy);

        IQueryable<OrderItem> GetParticipantItemsInSeason(long playerId, long seasonId, bool? isTestMode);
        bool HasTutionOrderItems(Charge charge);
        List<string> ShowItemSessions(OrderItem orderItem);

        IQueryable<OrderItem> GetInitiatedScheduleOrderitems(ProgramSchedule schedule);

        string GetOrderItemConfirmationIdMessage(long programId, List<ProgramSchedulePart> deletedProgramScheduleParts, DateTime newProgramDate, bool editEndDate, bool onlyDropInPunchcard = false);

        decimal CalculatScheduleChargeDiscount(OrderChargeDiscount chargeDiscount, decimal installmentAmount, bool hasAddedInstallment = false, int countInstallment = 0);

        void UpdateOrderItemSchedulePart(OrderItem orderItem, ProgramSchedulePart schedulePart, List<OrderItemSchedulePartModel> orderParts, decimal newScheduleAmount, bool hasAddedInstallment = false, int deletedInstallment = 0);

        void updateInstallmentAmount(OrderItem orderItem, ProgramSchedulePart installmentPart, List<OrderItemSchedulePartModel> orderParts, decimal newAmount, bool hasAddedInstallment = false, int deletedInstallment = 0);
        decimal GetProgramSchedulePartAmount(ProgramSchedulePart schedulePart, ProgramSchedule schedule, Charge selectedCharge, List<DayOfWeek> days, DateTime? desiredStartDate);

        decimal UpdateOrderItemChargeDiscounts(OrderItem orderItem, int installmentCount, DateTime? newDesiredStartDate, List<OrderItemSchedulePartModel> orderItemScheduleParts = null, List<ProgramSchedulePart> listItemsParts = null, bool installmentIsDeleted = false);

        OperationStatus AssignNewFormToOrders(OrderItem orderItem, int userId, List<long> orderItemIds);

        IQueryable<OrderItem> GetNonDonationItems(Order order);

        IQueryable<OrderItem> GetDonationItems(Order order);
        decimal GetPayableBalance(OrderItem orderItem);
        OrderChargeDiscount GetLastCancellationCharge(OrderItem orderItem);
        IEnumerable<UserPaymentsItemViewModel> GetUserPaymentItems(int userId, int clubId);
        bool AddItemToCart(ConfirmPaymentAmountViewModel model);
        ConfirmPaymentAmountViewModel ConfirmPaymentAmount(long id, PaymentPlanType paymentType, string returnUrl);
        UserPaymentsItemViewModel GetParentPayments(long? id, PaymentPlanType? paymentType, bool? status, int userId);
        Tuple<bool, string> CheckPayableAmountValidation(decimal payableAmount, decimal totalAmount);

        string GetSubscriptionSchedulePartDateRange(OrderInstallment installment);

        IQueryable<OrderItem> GetRegisteredProfileBySeason(int? playerId, long? seasonId);

        IQueryable<OrderItem> GetRegisteredProfile(int playerId);

        void CreateAddOnChargeToOrderItem(OrderItem orderItem, ChargeDiscountCategory category, decimal amount, string name, string description, long? chargeId = null);
        void UpdateAddOnInstallment(OrderInstallment item, OrderInstallment installmentCharge, string userName);

        OperationStatus UncancelOrder(OrderItem orderItem, int userId);

        UndoFamilyCreditToOrderViewModel GetUndoFamilyCreditViewModel(OrderItem orderItem, long transactionId);

        OperationStatus UndoFamilyRefundToOrder(OrderItem orderItem, UndoFamilyCreditToOrderViewModel model, int userId);

        OperationStatus CompleteDraftItem(long orderItemId, int userId);
        RefundResultModel Refund(OrderItemsViewModel model, ref decimal refundedAmount);
        void SetToUserCredit(OrderItem orderItem, decimal RefundAmount);
        decimal GetRefundAmount(TransactionActivity transaction, OrderItem orderItem, decimal remainAmount, bool isTestMode, IEnumerable<TransactionActivity> refunded, bool isPaypalTransaction);
        OperationStatus UpdateRefund(bool status, OrderItem orderItem, decimal refundedAmount, OrderItemsViewModel model, string historyDescription);
        decimal GetAmountAutomaticallyTransactions(OrderItem orderItem);

        string GetClassDays(OrderItem orderItem);
    }
}
