﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class DistrictSeasonReplicateViewModel
    {
        public DistrictSeasonReplicateViewModel()
        {
            Seasons = new List<SelectKeyValue<long?>>();
        }

        public List<SelectKeyValue<long?>> Seasons { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name ="Season")]
        public long? SelectedSeason { get; set; }

        public int DistrictId { get; set; }

        public int? MemberId { get; set; }

        public string MemberName { get; set; }
    }
}