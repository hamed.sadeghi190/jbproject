﻿using Jb.Framework.Common.Forms.JsonFormatter;
using Jumbula.Common.Base;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using JsonHelper = Jumbula.Common.Helper.JsonHelper;


namespace Jb.Framework.Common.Forms
{
    [JsonConverter(typeof(JbFormConverter))]
    public class JbForm : BaseEntity
    {
        #region Fields
        private List<IJbBaseElement> _elements;
        private AccessMode _currentMode;
        private VisibilityMode _visibleMode;
        private string _stringElements;
        #endregion

        #region Constractors
        public JbForm()
        {
            _elements = new List<IJbBaseElement>();
            AccessRole = new Dictionary<string, AccessMode>();
            //CreatedDate = DateTime.Now;
            LastModifiedDate = DateTime.Now;
        }
        public JbForm(VisibilityMode visibleMode)
            : this()
        {
            this.VisibleMode = visibleMode;
        }
        #endregion

        #region Properties

        [NotMapped]
        public VisibilityMode VisibleMode
        {
            get { return _visibleMode; }
            set
            {
                _visibleMode = value;
                //SetElementsVisibleMode(value);
            }
        }

        [NotMapped]
        public AccessMode CurrentMode
        {
            get { return _currentMode; }
            set
            {
                _currentMode = value;
                //SetElementsCurrentMode();
            }
        }

        public string RefEntityName { get; set; }
        public int RefEntityId { get; set; }

        public int? ParentId { get; set; }


        [NotMapped]
        [JsonProperty(IsReference = true)]
        public List<IJbBaseElement> Elements
        {
            get
            {
                if (!_elements.Any() && !string.IsNullOrEmpty(_stringElements))
                {
                    var jbc = new JbElementConverter();
                    var jsonObject = JArray.Parse(_stringElements);
                    var props = jsonObject.ToObject<List<object>>();
                    _elements = props.Select(prop => jbc.GetJbElement(prop as JObject)).ToList();
                }

                ManipulateConditionalElementsOnGet(_elements);

                return _elements;
            }
            set
            {
                ManipulateConditionalElementsOnSet(value);

                _elements = value;
            }
        }

        [JsonIgnore]
        public string JsonElements
        {
            get
            {
                if (string.IsNullOrEmpty(_stringElements) && Elements != null && Elements.Any())
                    _stringElements = JsonHelper.JsonSerializer(Elements);
                return _stringElements;
            }
            set
            {
                _stringElements = value;
            }
        }

        public string Title { get; set; }

        #region metaData

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public virtual DateTime CreatedDate { get; set; }

        public virtual DateTime LastModifiedDate { get; set; }

        #endregion

        public string HelpText { get; set; }

        [NotMapped]
        public Dictionary<string, AccessMode> AccessRole { get; set; }

        #endregion

        #region Methods
        public void SetElementsCurrentMode(AccessMode currentMode)
        {
            Elements.ForEach(e =>
             {
                 e.CurrentMode = currentMode;
                 if (e is JbSection)
                 {
                     (e as JbSection).Elements.ForEach(e2 =>
                     {
                         e2.CurrentMode = currentMode;
                         if (e2 is JbSection)
                         {
                             (e2 as JbSection).Elements.ForEach(e3 => { e3.CurrentMode = currentMode; });
                         }
                     });
                 }
             });
        }
        public void ChangeElementsMode(AccessMode fromMode, AccessMode toMode)
        {
            Elements.ForEach(e =>
            {
                if (e.CurrentMode == fromMode)
                    e.CurrentMode = toMode;
                if (e is JbSection)
                {
                    (e as JbSection).Elements.ForEach(e2 =>
                    {
                        if (e2.CurrentMode == fromMode)
                            e2.CurrentMode = toMode;
                        if (e2 is JbSection)
                        {
                            (e2 as JbSection).Elements.Where(e2e => e2e.CurrentMode == fromMode).ToList()
                                .ForEach(e3 =>
                                    {
                                        e3.CurrentMode = toMode;
                                    });
                        }
                    });
                }
            });
        }
        public void SetElementsVisibleMode(VisibilityMode visibleMode)
        {
            Elements.ForEach(e =>
            {
                e.VisibleMode = visibleMode;
                if (e is JbSection)
                {
                    (e as JbSection).Elements.ForEach(e2 =>
                    {
                        e2.VisibleMode = visibleMode;
                        if (e2 is JbSection)
                        {
                            (e2 as JbSection).Elements.ForEach(e3 => { e3.VisibleMode = visibleMode; });
                        }
                    });
                }
            });
        }

        public IJbBaseElement GetElement(string elementName)
        {
            var element = this.Elements.SingleOrDefault(e => e.Name == elementName);
            if (element == null)
            {
                var sections = this.Elements.Where(e => e is JbSection);
                foreach (var section in sections)
                {
                    var elements = (section as JbSection).Elements;
                    foreach (var element2 in elements)
                    {
                        if (element2.Name == elementName)
                        {
                            element = element2;
                            break;
                        }
                    }
                }
            }
            return element;
        }

        public TElement GetElement<TElement>(string elementName) where TElement : JbBaseElement
        {
            var element = this.Elements.SingleOrDefault(e => e is TElement && e.Name == elementName);
            if (element == null)
            {
                var sections = this.Elements.Where(e => e is JbSection);
                foreach (var section in sections)
                {
                    var elements = (section as JbSection).Elements;
                    foreach (var element2 in elements)
                    {
                        if (element2 is TElement && element2.Name == elementName)
                        {
                            element = element2;
                            break;
                        }
                    }
                }
            }

            return element as TElement;
        }
        public TElement GetElementBySectionName<TElement>(string elementName, string sectionName) where TElement : JbBaseElement
        {
            var section = GetElement<JbSection>(sectionName);
            var element = section.Elements.SingleOrDefault(e => e is TElement && e.Name == elementName);
            if (element == null)
                return null;
            return element as TElement;
        }

        private void ManipulateConditionalElementsOnGet(List<IJbBaseElement> elements)
        {
            var conditionalElements = elements.Where(e => e.IsConditional).ToList();
            conditionalElements.AddRange(elements.Where(e => e is JbSection).SelectMany(e => (e as JbSection).Elements).Where(e => e.IsConditional));

            foreach (var item in conditionalElements)
            {
                var relatedElementId = item.RelatedElementId;
                var relatedElementValue = item.RelatedElementValue;

                var relatedElement = elements.Where(e => e is JbSection).SelectMany(e => (e as JbSection).Elements).FirstOrDefault(e => e.Name == relatedElementId);

                if (relatedElement is JbDropDown && !(relatedElement as JbDropDown).Items.Any(i => i.Value == relatedElementValue))
                {
                    item.IsConditional = false;
                }
            }
        }

        private void ManipulateConditionalElementsOnSet(List<IJbBaseElement> elements)
        {
            var conditionalElements = elements.Where(e => e.IsConditional).ToList();
            conditionalElements.AddRange(elements.Where(e => e is JbSection).SelectMany(e => (e as JbSection).Elements).Where(e => e.IsConditional));

            foreach (var item in conditionalElements)
            {
                var relatedElementId = item.RelatedElementId;

                var relatedElement = elements.Where(e => e is JbSection).SelectMany(e => (e as JbSection).Elements).FirstOrDefault(e => e.Name == relatedElementId);

                var relatedElementValue = relatedElement?.GetValue()?.ToString();

                if (relatedElement != null && relatedElement is JbDropDown)
                    relatedElementValue = (relatedElement as JbDropDown).GetKey()?.ToString();

                if (((relatedElement != null && relatedElement.GetValue() == null) || relatedElement?.GetValue() != null && relatedElementValue != item.RelatedElementValue)  && item.GetValue() != null)
                    item.SetValue(null);
            }
        }
        #endregion
    }
}
