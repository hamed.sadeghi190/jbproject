﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('accountSettingsBillingListController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbConfirmation, jbToast) {
        window.app.globalObjects.pageClass = 'account-settings-view';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/AccountSettings", order: 2, menu: "SETTINGS" });
        $scope.baseUrl = window.location.origin + "/Dashboard/AccountSetting";


        $scope.mainGridDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: $scope.baseUrl + '/GetAllBillingInfo',
                        cache: false
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: '',
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: null
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            sortable: false,
            serverFiltering: true,
            serverSorting: false,
            serverPaging: true,
            pageSize: 10,
        });

        $scope.mainGridOptions = {
            dataSource: $scope.mainGridDataSource,
            sortable: false,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            columns: [
                {
                    field: "Method",
                    title: "Method",
                    width: "200px",
                    template: "#= Method # #if(IsDefault) {# <i class='grid-label label bg-green  fg-white'>Active</i> #}#"

                },
                {
                field: "CardHolderName",
                title: "Cardholder name"
            }, {
                field: "ExpiryDate",
                title: "Expiration",
            },
             {
                 field: "Address",
                 title: "Address",
             }

            , {
                title: "Actions",
                width: "70px",
                headerTemplate: "<span class='actions-title'>Actions</span>",
                template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                        <li> ... \
                            <ul>\
                                    <li ui-sref='AccountSettingManageBilling({type:\"Edit\",cardId: #=Id#})'>\
                                          <span class='k-link jbi-pencil'>Edit</span>\
                                    </li>\
                                    <li ng-hide='#=IsDefault#' ng-click='activeCard( #=Id# ,#=ClientId#)'>\
                                          <span class='k-link jbi-pencil'>Set as active</span>\
                                    </li>\
                                    <li ng-hide='#=IsDefault#' ng-click='deleteCard( #=Id# )'>\
                                          <span class='k-link jbi-remove'>Remove</span>\
                                    </li>\
                            </ul>\
                        </li>\
                    </ul>"
            }],
        };

        $scope.deleteCard = function (id) {
            jbConfirmation
                .yes(function () {
                    $http.post($scope.baseUrl + '/DeleteCard', { cardId: id }).success(function (data, status, headers, config) {
                        if (data.Status == true) {

                            $scope.mainGridOptions.dataSource.read();

                        }
                        else {
                            jbToast.error(data.Message);
                        }
                    });
                })
              .title('Removing billing method')
              .message('Are you sure you want to remove this billing method?')
              .confirm();
        };

        $scope.activeCard = function (id,clientId) {
            jbConfirmation
                .yes(function () {
                    $http.post($scope.baseUrl + '/ActivateCard', { cardId: id, clientId: clientId }).success(function (data, status, headers, config) {
                        if (data.Status == true) {

                            $scope.mainGridOptions.dataSource.read();

                        }
                        else {
                            jbToast.error(data.Message);
                        }
                    });
                })
               .title('Activate billing method')
              .message('When you change your active billing method, you may automatically be charged for any outstanding balance. Are you sure you want to proceed with this action?')
              .confirm();
        };
    }]);
})();