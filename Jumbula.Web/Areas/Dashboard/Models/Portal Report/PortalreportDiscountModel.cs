﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class PortalreportDiscountModel
    {
        public string Name { get; set; }

        public decimal Fee { get; set; }

        public string Type { get; set; }
    }

    public class PortalReportDiscountItems
    {
        public long Id { get; set; }

        public string ItemName { get; set; }

        public string User { get; set; }

        public string Date { get; set; }

        public string Confirmation { get; set; }

        public decimal Total { get; set; }
    }
}