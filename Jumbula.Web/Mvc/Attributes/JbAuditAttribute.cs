﻿using System;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.WebLogic.UserService;
using ActionFilterAttribute = System.Web.Mvc.ActionFilterAttribute;

namespace Jumbula.Web.Mvc.Attributes
{
    public class JbAuditAttribute : ActionFilterAttribute
    {
        private readonly string _action;
        private readonly string _description;
        private Guid _id;
        private readonly IAuditBusiness _auditService;
        private JbZone _zone;
        private string _ownerKey;

        public JbAuditAttribute(JbAction action, string description = "")
        {
            _action = action.ToDescription();
            _description = description;
            _auditService = Ioc.AuditBusiness;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                var request = filterContext.HttpContext.Request;
                var controller = filterContext.Controller as JbBaseController;

                _id = Guid.NewGuid();
                _zone = getZone(controller);
                _ownerKey = GetOwnerKey(controller);

                var audit = new JbAudit()
                {
                    Id = _id,
                    UserName = (request.IsAuthenticated) ? filterContext.HttpContext.User.Identity.Name : string.Empty,
                    IpAddress = request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? request.UserHostAddress,
                    Url = request.RawUrl,
                    ClubDomain = JbUserService.GetCurrentClubDomain(),
                    DateTime = DateTime.UtcNow,

                    Browser =
                        $"{(request.Browser != null ? request.Browser.Browser : string.Empty)} {(request.Browser != null ? request.Browser.Version : string.Empty)}",
                    Device = _auditService.GetDevice(request),

                    Action = _action,
                    Description = _description,

                    Zone = _zone.ToDescription(),
                    OwnerKey = _ownerKey
                };

                _auditService.Add(audit);
            }
            catch
            {
                // ignored
            }

            base.OnActionExecuting(filterContext);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            try
            {
                var response = filterContext.HttpContext.Response;
                var controller = filterContext.Controller as JbBaseController;

                if (controller != null)
                {
                    var activityDescription = controller.ActivityDescription;

                    if (string.IsNullOrEmpty(activityDescription))
                    {
                        activityDescription = _description;
                    }

                    var ownderKey = controller.OwnerKey;

                    if (string.IsNullOrEmpty(ownderKey))
                    {
                        ownderKey = _ownerKey;
                    }

                    if (controller.Status)
                    {
                        _auditService.SetSuccess(_id, response.StatusCode, activityDescription, ownderKey);
                    }
                    else
                    {
                        _auditService.SetResult(_id, response.StatusCode, activityDescription, ownderKey);
                    }
                }
            }
            catch
            {
                // ignored
            }

            base.OnResultExecuted(filterContext);
        }

        private JbZone getZone(JbBaseController controller)
        {
            if (controller.Zone.HasValue)
            {
                return controller.Zone.Value;
            }

            var area = controller.ControllerContext.RouteData.DataTokens["area"];

            if (area == null)
            {
                return JbZone.ParentDashboard;
            }
            else if (area.ToString() == "Dashboard")
            {
                return JbZone.AdminDashboard;
            }
            else
            {
                throw new NotSupportedException("The zone is not supported");
            }
        }

        private string GetOwnerKey(JbBaseController controller)
        {
            if (!string.IsNullOrEmpty(controller.OwnerKey))
            {
                return controller.OwnerKey;
            }

            switch (_zone)
            {
                case JbZone.ParentDashboard:
                    {
                        var parentController = controller as ParentBaseController;

                        if (parentController != null)
                        {
                            return parentController.CurrentUserId.ToString();
                        }
                        else
                        {
                            return controller.GetCurrentUserId().ToString();
                        }
                    }
                case JbZone.AdminDashboard:
                    {
                        var adminController = controller as DashboardBaseController;
                        return adminController.GetCurrentClubDomain();
                    }
                default:
                    {
                        return null;
                    }
            }
        }
    }
}