﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Common.Utilities;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Newtonsoft.Json;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CatalogInvitationViewModel
    {
        public CatalogInvitationViewModel()
        {

        }

        public string CatalogName { get; set; }
        [Display(Name ="Program name")]
        [Required(ErrorMessage = "{0} is required.")]
        public string ProgramName { get; set; }
        public int CatalogId { get; set; }

        public int ProviderId { get; set; }
        public string ProviderName { get; set; }

        public long ProviderSeasonId { get; set; }
        public IEnumerable<SelectKeyValue<long>> ProviderSeasons { get; set; }

        public int SchoolId { get; set; }
        public IEnumerable<SelectKeyValue<int>> Schools { get; set; }

        public long SchoolSeasonId {
            get

            {
                if (SelectedSchoolSeason.HasValue)
                    return SelectedSchoolSeason.Value;
                else
                    return 0;
            }
        }
        public List<SelectKeyValue<string>> WeekDays {get; set;}

        [Required(ErrorMessage = "Week Days is required.")]
        public List<DayOfWeek> SelectedWeekDays { get; set; }

        public List<SelectKeyValue<string>> Grades { get; set; }

        [Required(ErrorMessage = "Min Grade is required.")]
        public SchoolGradeType? SelectedMinGrade { get; set; }

        [Required(ErrorMessage = "Max Grade is required.")]
        public SchoolGradeType? SelectedMaxGrade { get; set; }
        public List<SelectKeyValue<long>> SchoolSeasons { get; set; }
        public long? SelectedSchoolSeason { get; set; }
        
        public string SpecialRequests { get; set; }

        public WeekDaysMode WeekDaysMode { get; set; }

        [JsonConverter(typeof(CustomTimeConverter))]
        [Display(Name = "Start time")]
        public DateTime? StartTime { get; set; }

        [JsonConverter(typeof(CustomTimeConverter))]
        [Display(Name = "End time")]
        public DateTime? EndTime { get; set; }

        public bool SendMessageToProvider { get; set; }

    }
}