﻿namespace Jumbula.Common.Enums
{
    public enum AbbreviatedDayNameMode
    {
        FullName = 0,
        OneLetter = 1,
        TwoLetter = 2,
        ThreeLetter = 3
    }
}
