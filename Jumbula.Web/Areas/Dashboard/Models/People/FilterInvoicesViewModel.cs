﻿using System.Collections.Generic;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class FilterInvoicesViewModel
    {

        public List<SelectKeyValue<string>> InvoiceTypes { get; set; }
    }
    public enum PeopelInvoiceType : byte
    {
        All,
        Paid,
        Unpaid
    }
}