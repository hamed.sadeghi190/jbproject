﻿

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jumbula.Common.Enums
{
    public enum SkillLevelCategories
    {
        [Display(Name = "Select level")]
        [Description("Select level")]
        None,
        Beginner,
        Intermediate,
        Advanced
    }
}
