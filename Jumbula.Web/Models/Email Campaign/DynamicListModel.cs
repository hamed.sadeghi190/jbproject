﻿using Jumbula.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Models
{
    public class DynamicListModel : BaseViewModel<Campaign>
    {
        public DynamicListModel()
        {
            SeasonsList = new List<SelectKeyValue<string>>();
            ProgramsList = new List<SelectKeyValue<string>>();
            DateOptionList = new List<SelectKeyValue<string>>();
            AgesList = new List<SelectKeyValue<string>>();
        }

        public List<SelectKeyValue<string>> SeasonsList { get; set; }

        public List<SelectKeyValue<string>> ProgramsList { get; set; }

        public List<SelectKeyValue<string>> DateOptionList { get; set; }

        public List<SelectKeyValue<string>> AgesList { get; set; }

        [Display(Name = "Seasons List")]
        public List<string> Seasons { get; set; }

        [Display(Name = "Programs List")]
        public List<string> Programs { get; set; }

        public DateConditionStep1 DateOption { get; set; }

        [Display(Name = "Start Date")]
        public DateTime? CustomDateStart { get; set; }

        [Display(Name = "End Date")]
        public DateTime? CustomDateEnd { get; set; }

        //[Range(0, 100, ErrorMessage = "{0} must be a positive number.")]
        [Display(Name = "Min Age")]
        public string MinAge { get; set; }

        //[Range(0, 100, ErrorMessage = "{0} must be a positive number.")]
        [Display(Name = "Max Age")]
        public string MaxAge { get; set; }

        public GenderCategories Gender { get; set; }

        public string Graduate { get; set; }

        public bool IsAllSeasons { get; set; }

        public bool IsAllPrograms { get; set; }

        public int? ListId { get; set; }

    }
  

}

