﻿using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SeasonProgramsViewModel : BaseViewModel< Jumbula.Core.Domain.Season,long>
    {
        public SeasonProgramsViewModel()
        {
        }
        [MaxLength(200)]
        [MinLength(3)]
        public string Title { get; set; }

        [MaxLength(200)]
        [MinLength(3)]
        [Display(Name = "Season Domain")]
        public string Domain { get; set; }

        public SeasonStatus Status { get; set; }

        public bool IsCopy { get; set; }
        public bool IsTestMode { get; set; }

        public string programName { get; set; }
        public string Room { get; set; }
        public long programId { get; set; }
    }
}