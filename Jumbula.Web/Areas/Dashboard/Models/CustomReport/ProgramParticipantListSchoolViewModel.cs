﻿using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ProgramParticipantListSchoolViewModel
    {
        public string ParticipantName { get; set; }
        public string TeacherName { get; set; }
        public string Phone { get; set; }
        public string TransportHome { get; set; }
        public string Grade { get; set; }
        public int GradeOrder { get; set; }
        public int Number { get; set; }
        public string DoB { get; set; }
        public string Classroom { get; set; }
        public OrderItemMode Mode { get; set; }
        public string AuthorizedAdultNames { get; set; }
        public string MorningProgramArrival { get; set; }
    }
}