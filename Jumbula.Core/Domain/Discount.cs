﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;

namespace Jumbula.Core.Domain
{
    public class Discount : BaseEntity<long>, IProtectedResource
    {
        public Discount()
        {
            Category = ChargeDiscountCategory.CustomDiscount;
            Programs = new List<Program>();
        }

        [MaxLength(256)]
        public string Name { get; set; }

        [DefaultValue(ChargeDiscountCategory.CustomDiscount)]
        public ChargeDiscountCategory Category { get; set; }
        public decimal Amount { get; set; }
        public ChargeDiscountType AmountType { get; set; }
        public int NOP { get; set; }
        public DiscountBenefitType BenefitType { get; set; }
        public DiscountApplyType ApplyType { get; set; }
        public bool IsAllProgram { get; set; }
        public long SeasonId { get; set; }
        public MetaData MetaData { get; set; }
        public bool IsDeleted { get; set; }
        [MaxLength(1000)]
        public string Description { get; set; }
        public EligibleMode EligibleMode { get; set; }
        public virtual Season Season { get; set; }
        public virtual ICollection<Program> Programs { get; set; }

        public bool DoesTheClubOwnIt(int clubId)
        {
            if (Season.ClubId == clubId)
            {
                return true;
            }

            return false;
        }

        public bool DoesTheParentOwnIt(int userId)
        {
            return false;
        }
    }

    

    public enum DiscountBenefitType : byte
    {
        Organization = 0,
        User = 1,
    }
    public enum DiscountApplyType : byte
    {
        [Description("Every session")]
        Every = 0,
         [Description("The {0} session")]
        SelectedNumOnly = 1,
         [Description("For the {0} session, plus all additional")]
        SelectedNumAndAddition = 2,

    }
    public enum EligibleMode : byte
    {
        [Description("different program")]
        DifferentProgram=0,
        [Description("same program")]
        SameProgram=1
       
    }
    public enum ProgramMode : byte
    {
        [Description("Select")]
        Select = 0,
        [Description("All programs")]
        AllPrograms = 1,
        [Description("Selected programs")]
        SelectedPrograms = 2,
    }
}
