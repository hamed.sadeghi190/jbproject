﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;
using Newtonsoft.Json;

namespace Jumbula.Core.Domain
{
    public class ScheduleAttendance : BaseEntity
    {
        public ScheduleAttendance()
        {

        }

        public long ScheduleId { get; set; }

        public int SessionId { get; set; }

        [ForeignKey("ScheduleId ")]
        public virtual ProgramSchedule Schedule { get; set; }

        public int ProfileId { get; set; }

        [ForeignKey("ProfileId")]
        public virtual PlayerProfile PlayerProfile { get; set; }

        public AttendanceStatus? Status { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        [StringLength(4000)]
        public string Note { get; set; }

        [ForeignKey("ClubStaffId")]
        public virtual ClubStaff ClubStaff { get; set; }

        public int? ClubStaffId { get; set; }

        public bool IsPickedUp { get; set; }

        public DateTime? AttendanceDate { get; set; }

        public DateTime? PickedUpDate { get; set; }

        [Column("Pickup")]
        [StringLength(4000)]
        public string PickupSerialized { get; set; }

        private ScheduleAttendancePickup _pickup;

        [NotMapped]
        public ScheduleAttendancePickup Pickup
        {
            get
            {
                if (_pickup == null && !string.IsNullOrEmpty(PickupSerialized))
                {
                    _pickup = JsonConvert.DeserializeObject<ScheduleAttendancePickup>(PickupSerialized);
                }

                return _pickup;
            }

        }

        public class ScheduleAttendancePickup
        {
            public string FullName { get; set; }
            public DateTime LocalDateTime { get; set; }
            public DateTime ClubDateTime { get; set; }
            public string DismissalFromEnrichment { get; set; }
            public string Phone { get; set; }
        }

        
    }
    public enum AttendanceStatus
    {
        Present,
        Absent,
        Late
    }
}
