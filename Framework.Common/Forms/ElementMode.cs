﻿namespace Jb.Framework.Common.Forms
{
    public enum AccessMode
    {
        Hidden = -10,
        ReadOnly = -5,
        Edit = 0,
        Design = 5,
        Print = 10
    }
}
