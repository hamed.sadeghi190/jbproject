﻿

using System.ComponentModel.DataAnnotations;

namespace Jumbula.Common.Enums
{
    public enum PricePlanType : byte
    {
        [Display(Name = "Free trial")]
        FreeTrial = 0,
        [Display(Name = "Pay as you go")]
        PayAsYouGo = 1,
        [Display(Name = "Subscription")]
        Subscription = 2,
        [Display(Name = "Enterprise")]
        Enterprise = 3,
        [Display(Name = "Subscription (Custom)")]
        Custom = 4
    }
}
