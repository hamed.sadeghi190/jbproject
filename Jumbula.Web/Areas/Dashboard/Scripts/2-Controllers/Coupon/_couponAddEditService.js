﻿(function () {
    'use strict';

    angular.module('dashboardApp').service("couponAddEditService", ['$state', '$http', 'jbToast', 'jbConfirmation', function ($state, $http, jbToast, jbConfirmation) {
        return function AddEditCoupon($scope, $http, $stateParams) {

            $scope.baseUrl = window.location.origin + "/Dashboard/Coupon";
            $scope.baseModel = null;

            $scope.MC.stateParams = $stateParams;

            $scope.couponId = 0;
            if ($stateParams.couponId)
                $scope.couponId = $stateParams.couponId;

            $http.get($scope.baseUrl + '/GetBaseData').success(function (data, status, headers, config) {
                $scope.baseModel = data;
            });

            $scope.eventDataSource = new kendo.data.DataSource({
                type: "json",
                serverFiltering: true,
                transport: {
                    read: {
                        url: "Dashboard/Program/GetListSelector",
                        cache: false,
                    }, parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                seasonDomain: $scope.MC.stateParams.seasonDomain,
                                term: document.activeElement.value
                            }
                        }
                    }
                }
            });

            $http.get($scope.baseUrl + '/CreateEditJson?id=' + $scope.couponId).success(function (data, status, headers, config) {
                $scope.Model = data;
            });

            $scope.eventsOptions = {
                placeholder: "Select programs...",
                dataTextField: "Title",
                dataValueField: "Value",
                dataSource: $scope.eventDataSource,
            };

            $scope.saveRow = function () {
                $scope.Model.SeasonDomain = $scope.MC.stateParams.seasonDomain;
                $http.post($scope.baseUrl + '/CreateEdit', { model: $scope.Model })
                    .success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            $state.go('Coupons', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        }
                        else {
                            $scope.MC.handleErrors($scope, data);
                        }
                    });
            };

            $scope.deleteRow = function (id) {
                jbConfirmation
                    .yes(function () {
                        $http.post($scope.baseUrl + '/Delete', { couponId: id }).success(function (data, status, headers, config) {
                            if (data.Status == true) {
                                jbToast.success('Coupon deleted successfully.');
                                $state.go('Coupons', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                                $scope.couponsSearch();
                            }
                            else if (data.Status == false && data.Message == null) {
                                jbToast.error("Update Completed! No Records Affected");
                            }
                            else {
                                jbToast.error(data.Message);
                            }
                        });
                    })
                    .confirm();
            };
        };
    }]);
})();