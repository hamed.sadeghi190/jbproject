﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ReminderInvoicesViewModel
    {
        public List<int> InvoiceIds { get; set; }
        public string NoteToRecipients { get; set; }
        public string ClubName { get; set; }
        public string Date { get; set; }
    }
}