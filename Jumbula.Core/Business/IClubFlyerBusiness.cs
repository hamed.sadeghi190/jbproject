﻿using Jb.Framework.Common;
using System.Collections.Generic;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IClubFlyerBusiness
    {
        OperationStatus Create(ClubFlyer clubflyer);

        List<int> GetListIds(List<int> schoolId);

        List<ClubFlyer> GetClubFlyerList(long clubId);
        List<ClubFlyer> GetPartnerFlyerList(long partnerId);

    }
}
