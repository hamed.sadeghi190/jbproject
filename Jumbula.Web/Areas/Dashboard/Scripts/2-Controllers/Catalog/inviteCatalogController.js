﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('inviteCatalogController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbToast) {

        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;
        $scope.schoolId = -1;

        var catalogId = $stateParams.catalogId;

        $http.get('Dashboard/Catalog/InviteCatalog', { params: { catalogId: catalogId } }).success(function (data, status, headers, config) {
            $scope.Model = data;
        }).error(function (data) {
            jbToast.error("error " + data);
        });


        $scope.schoolSeasonOptions = [];

        $scope.getSchoolSeason = function () {

            $http.get('Dashboard/Club/GetClubSeasons', { params: { clubId: $scope.Model.SchoolId } }).success(function (data, status, headers, config) {

                $scope.schoolSeasonOptions = data;

            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });

        }

        $scope.inviteCatalog = function () {

            $http.post('Dashboard/Catalog/InviteCatalog', { model: $scope.Model })
                .success(function (data) {

                    if (data.Status) {

                        jbToast.success("The draft session is created successfully.", "");
                        $state.go('Catalog');
                    } else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        }

        $scope.seasonSelected = function () {

            if ($scope.Model.SelectedSchoolSeason != null && $scope.Model.SelectedSchoolSeason != 'undefined')
                $http.get('Dashboard/Catalog/GetSeasonSettings', { params: { seasonId: $scope.Model.SelectedSchoolSeason} }).success(function (data) {

                console.log(data);

                $scope.Model.StartTime = data.StartTime;
                $scope.Model.EndTime = data.EndTime;

            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
    
        }

    }]);
})();

