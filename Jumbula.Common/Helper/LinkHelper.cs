﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Jumbula.Common.Helper
{
    public class LinkHelper
    {
        public static string UiSrefBuilder(string state, Dictionary<string, object> parameters, string fieldName, string target = "_blank")
        {
            var parameter = string.Empty;

            foreach (var item in parameters)
            {
                parameter += $"{item.Key}: {item.Value}, ";
            }

            TagBuilder aTagBuilder = new TagBuilder("a");
            aTagBuilder.Attributes["ui_sref"] = state + "({" + parameter + "})";
            aTagBuilder.Attributes["target"] = target;
            aTagBuilder.InnerHtml = "#= " + fieldName + " #";

            return aTagBuilder.ToString();
        }
    }
}
