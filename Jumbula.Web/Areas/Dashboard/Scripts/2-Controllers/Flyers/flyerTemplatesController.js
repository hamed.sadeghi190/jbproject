﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('flyerTemplatesController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, jbConfirmation) {
        window.app.globalObjects.pageClass = 'campaign-templates';
        window.app.globalObjects.initializeView();

        var baseUrl = window.location.origin + '/Dashboard/Flyer';


        $scope.templateSearch = function () {
            $scope.gridOptions.dataSource.read();
        }

        $scope.templatesDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetAllTemplates/',
                        type: "POST",
                        data: function () {
                            return {
                                term: $scope.searchTerm,
                            };
                        },
                    },
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverPaging: true,
            pageSize: 10,
        });

        $scope.FillTemplateGrid = {
            dataSource: $scope.templatesDataSource,
            sortable: true,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                        5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            columns: [
                {
                    field: "Name",
                    title: "Name",
                    template: "",
                    width: "200px",
                },
                {
                    field: "Id",
                    title: "id",
                    width: "110px",
                    hidden: true,
                },
                {
                    field: "",
                    title: "Actions",
                    width: "70px",
                    headerTemplate: "<span class='actions-title'>Actions</span>",
                    template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                        <li> ... \
                            <ul   >\
                                <li  ng-if='#=Id#==1' class='k-item k-state-default k-first' role='menuitem' ui-sref='TemplateAfterSchoolClassesPreview1'>\
                                            <span class='k-link'>View</span>\
                                      </li>\
                                 <li  ng-if='#=Id#==2'  class='k-item k-state-default k-first' role='menuitem' ui-sref='TemplateClassCatalogPreview1'>\
                                            <span class='k-link'>View</span>\
                                      </li>\
                                <li  ng-if='#=Id#==3'  class='k-item k-state-default k-first' role='menuitem' ui-sref='TemplateAfterSchoolClassesPreview2'>\
                                            <span class='k-link'>View</span>\
                                      </li>\
                                <li  ng-if='#=Id#==4'  class='k-item k-state-default k-first' role='menuitem' ui-sref='TemplateClassCatalogPreview2'>\
                                            <span class='k-link'>View</span>\
                                      </li>\
                                <li  ng-if='#=Id#==5'  class='k-item k-state-default k-first' role='menuitem' ui-sref='TemplateAfterSchoolClassesPreview3'>\
                                            <span class='k-link'>View</span>\
                                      </li>\
                            </ul>\
                        </li>\
                    </ul>",
                }
            ],
        };

    }]);
})();
