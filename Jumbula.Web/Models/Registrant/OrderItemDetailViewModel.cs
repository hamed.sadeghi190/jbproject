﻿using Jb.Framework.Common.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class OrderItemDetailViewModel
    {
        public long OrderItemId { get; set; }
        public bool Editable { get; set; }
        [Display(Name = "Program Name")]
        public string ProgramName { get; set; }
        public ProgramTypeCategory  ProgramType { get; set; }

        [Display(Name = "Price")]
        public decimal Price { get; set; }

        [Display(Name = "PaidAmount")]
        public decimal PaidAmount { get; set; }

        [Display(Name = "TotalAmount")]
        public decimal TotalAmount { get; set; }

        [Display(Name = "Balance")]
        public decimal Balance => Price - PaidAmount;

        [Display(Name = "Schedule date")]
        public string ScheduleDate { get; set; }

        [Display(Name = "Order date")]
        public DateTime OrderDate { get; set; }

        [Display(Name = "Tuition name")]
        public string TuitionName { get; set; }

        [Display(Name = "Tuition price")]
        public decimal TuitionPrice { get; set; }

        [Display(Name = "Attendee name")]
        public string AttendeeName { get; set; }
        public CurrencyCodes Currency { get; set; }
        public string Confirmation { get; set; }

        public OrderItemStatusReasons ItemStatusReasons { get; set; }

        public List<ChargeDiscountItem> Charges { get; set; }

        public List<ChargeDiscountItem> Discounts { get; set; }

        public List<InstallmentItemDetailViewModel> Installments { get; set; }
        public OrderItemStatusCategories Status { get; set; }
        public JbForm Form { get; set; }

        public List<UserFollowupFormDetails> FollowUpForms { get; set; }

        public List<OrderItemWaiver> ClubWaivers { get; set; }

    }

    public class InstallmentItemDetailViewModel
    {
        public InstallmentItemDetailViewModel()
        {

        }

        public InstallmentItemDetailViewModel(OrderInstallment orderInstallment)
        {
            Amount = orderInstallment.Amount;
            PaymentStatus = orderInstallment.Status;
            DueDate =
                $"{ DateTimeHelper.FormatShortDateSeperetedByCamma(orderInstallment.InstallmentDate)} {(orderInstallment.Type != InstallmentType.Noraml ? $"({ orderInstallment.Type.ToDescription() } )" : string.Empty)}";
        }

        public string DueDate { get; set; }

        public decimal Amount { get; set; }

        public OrderStatusCategories PaymentStatus { get; set; }
    }
    public class UserFollowupFormDetails
    {
        public string Guid { get; set; }
        public int Id { get; set; }
        public FormType Type { get; set; }

        public string Title { get; set; }

        public FollowupStatus Status { get; set; }

        public string Link { get; set; }

    }

    public class ClubWaiverItem
    {
        public string Name { get; set; }
        public string content { get; set; }
    }
}