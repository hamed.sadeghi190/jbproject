﻿using Newtonsoft.Json;

namespace Jumbula.Core.Model.Email
{
    public class AutoChargeFailEmailParameterModel
    {
        public long InstallmentId { get; set; }

        public int AttemptNumber { get; set; }

        public long TransactionActivityId { get; set; }

        [JsonIgnore]
        public int ClubId { get; set; }

        [JsonIgnore]
        public string FailMessage { get; set; }

        [JsonIgnore]
        public int? NextAttemptIntervalDays { get; set; }
    }
}
