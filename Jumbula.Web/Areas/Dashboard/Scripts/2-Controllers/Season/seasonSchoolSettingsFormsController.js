﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('seasonSchoolSettingsFormsController', ['$scope', '$http', 'jbToast', '$stateParams', '$state', 'seasonSettingsService', 'formService', 'waiverService', function ($scope, $http, jbToast, $stateParams, $state, seasonSettingsService, formService, waiverService) {
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/SeasonSchoolSettingsRegistration", order: 4, menu: "SEASONS" });
        $scope.MC.stateParams = $stateParams;

        if ($state.returnByBack()) {

            $scope.Model = seasonSettingsService.getFormsAndWaivers();
        }
        else {
            $http.get('Dashboard/Season/GetSchoolSettingForms?seasonDomain=' + $stateParams.seasonDomain).success(function (data, status, headers, config) {

                $scope.Model = data;

            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }

        $scope.save = function () {

            $http.post('Dashboard/Season/SaveSchoolSettingForms', { model: $scope.Model, seasonDomain: $stateParams.seasonDomain })
             .success(function (data, status, headers, config) {

                 if (data.Status == true) {
                     $scope.errors = null;
                     jbToast.success("Program information updated successfully.", "");
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }

        $scope.goCustomizeForm = function () {

            seasonSettingsService.setFormsAndWaivers($scope.Model);

            $state.forward('Form', { seasonDomain: $scope.MC.stateParams.seasonDomain, formType: "Registration", templateId: $scope.Model.SelectedForm }, 'SeasonSchoolSettingForms', { seasonDomain: $scope.MC.stateParams.seasonDomain }
                , function () {

                    var formModel = formService.getModel();

                    $scope.Model.SelectedForm= formModel.FormTemplate;

                    $scope.Model.Forms = formModel.FormTemplates;
                });
        }

        $scope.goWaivers = function () {

            seasonSettingsService.setFormsAndWaivers($scope.Model);

            $state.forward('Waivers', { seasonDomain: $scope.MC.stateParams.seasonDomain}, 'SeasonSchoolSettingForms', { seasonDomain: $scope.MC.stateParams.seasonDomain },
                function () {

                    var waiverModel = waiverService.getModel();

                    $scope.Model.Waivers = waiverModel.AllWaivers;
                });
        }

        if ($state.returnByBack()) {
            $scope.MC.backQueueItems.pop();
        }
    }])

})();