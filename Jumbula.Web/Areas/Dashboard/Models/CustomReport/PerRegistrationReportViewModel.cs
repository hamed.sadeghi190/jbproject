﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class PerRegistrationReportViewModel
    {
        public long Id { get; set; }

        public string ProgramName { get; set; }

        public string ParticipantName { get; set; }

        public string UserName { get; set; }
        public string Capacity { get; set; }

        public string ProviderName { get; set; }
        public int Count { get; set; }

        public DateTime OrderDate { get; set; }
        public string StrOrderDate {
            get {
                return OrderDate.ToString("MMM dd, yyyy");
            }
        }
    }
}