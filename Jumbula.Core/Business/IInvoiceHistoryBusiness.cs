﻿using Jb.Framework.Common;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;


namespace Jumbula.Core.Business
{
    public interface IInvoiceHistoryBusiness
    {

     OperationStatus Create(InvoiceHistory invoiceHistory);
        IQueryable<InvoiceHistory> GetList();

    }
}
