alter TABLE [dbo].[Clubs] add [PartnerCommisionRate] [int] NOT NULL default(0)

go
alter TABLE [dbo].[PricePlans] alter column  [TransactionFee] [decimal](18, 2) NOT NULL
go
alter TABLE [dbo].[programs] drop column  PartnerCommisionRate
go
alter  TABLE [dbo].[Programs] add [CustomFields] [nvarchar](max) NULL