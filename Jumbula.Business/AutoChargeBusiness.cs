﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Infrastructure;
using Jumbula.Core.Model.Email;
using Jumbula.Core.Model.Payment;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Jumbula.Business
{
    public class AutoChargeBusiness : IAutoChargeBusiness
    {
        #region Fields
        private readonly IRepository<OrderInstallment, long> _orderInstallmentRepository;
        private readonly IClubRepository _clubRepository;
        private readonly IPaymentBusiness _paymentBusiness;
        private readonly IOrderInstallmentBusiness _installmentBusiness;
        private readonly ITransactionActivityBusiness _transactionActivityBusiness;
        private readonly IClubSettingBusiness _clubSettingBusiness;
        private readonly IEmailBusiness _emailBusiness;
        private readonly IJbDateTime _jbDateTime;
        private readonly IJbLocker _jbLocker;
        public ILogger Logger = Log.ForContext<AutoChargeBusiness>();
        #endregion

        #region Constractors
        public AutoChargeBusiness(IRepository<OrderInstallment, long> orderInstallmentRepository, IClubRepository clubRepository, IPaymentBusiness paymentBusiness, IOrderInstallmentBusiness installmentBusiness, ITransactionActivityBusiness transactionActivityBusiness, IClubSettingBusiness clubSettingBusiness, IEmailBusiness emailBusiness, IJbDateTime jbDateTime, IJbLocker jbLocker)
        {
            _orderInstallmentRepository = orderInstallmentRepository;
            _clubRepository = clubRepository;
            _paymentBusiness = paymentBusiness;
            _installmentBusiness = installmentBusiness;
            _transactionActivityBusiness = transactionActivityBusiness;
            _clubSettingBusiness = clubSettingBusiness;
            _emailBusiness = emailBusiness;
            _jbDateTime = jbDateTime;
            _jbLocker = jbLocker;
        }
        #endregion

        #region Public Methods

        public void Run(DateTime dueDate)
        {

            var lockWasTaken = false;

            try
            {

                if (_jbLocker.TryLock(JbWorker.AutoCharge))
                {
                    var clubsSendSummary = new Dictionary<int, bool>();
                    lockWasTaken = true;

                    var autoChargeStopWatch = new Stopwatch();
                    autoChargeStopWatch.Start();

                    var startTime = _jbDateTime.UtcNow;

                    /* We need to set time of auto charge date to 23:59:59  because auto charge run at 00:00:00 but some installments have time value bigger than 00:00:00 */
                    dueDate = dueDate.Date.AddDays(1).AddSeconds(-1);

                    var allDueclubsInstallmentsPair = _installmentBusiness.GetAllInstallmentsByDueDate(dueDate);
                    var installmentCount = allDueclubsInstallmentsPair.SelectMany(i => i.Value).Count();

                    Logger.Information("{LogCategory} {AutoChargeMessageType} {StartTime} {installmentCount} ", "Auto Charge", "Start", _jbDateTime.UtcNow, installmentCount);

                    var installmentChargeResults = new List<AutoChargeResult>();

                    foreach (var clubInstallmentsPair in allDueclubsInstallmentsPair)
                    {

                        var club = _clubRepository.Get(clubInstallmentsPair.Key);
                        var clubPolicy = _clubSettingBusiness.ReadAutoChargePolicy(club);
                        clubsSendSummary.Add(club.Id, clubPolicy.SendSummeryEmail);

                        var stopwatch = new Stopwatch();

                        foreach (var installmentId in clubInstallmentsPair.Value)
                        {
                            stopwatch.Restart();

                            var orderInstallment = _installmentBusiness.GetForAutoCharge(installmentId);
                            var chargeResult = Charge(orderInstallment, club, clubPolicy, dueDate);

                            installmentChargeResults.Add(chargeResult);

                            stopwatch.Stop();
                            CreateChargeLog(orderInstallment, chargeResult, dueDate, stopwatch.Elapsed.TotalMilliseconds);
                        }
                    }

                    SendReports(installmentChargeResults, clubsSendSummary);

                    autoChargeStopWatch.Stop();

                    Logger.Information("{LogCategory} {AutoChargeMessageType} {DueDate} {StartTime} {FinishTime} {AutoChargeDuration}  {installmentCount}",
                        "Auto Charge", "Summary", dueDate, startTime, _jbDateTime.UtcNow, autoChargeStopWatch.Elapsed.TotalMilliseconds, installmentCount);
                }
                else
                {
                    Logger.Error("{LogCategory} {AutoChargeMessageType} {DateTime}", "Auto Charge", "Duplicate Call", _jbDateTime.UtcNow);
                }

            }
            catch (Exception ex)
            {
                Logger.Error("{LogCategory} {AutoChargeMessageType} {ExceptionType} {Message} {Data}", "Auto Charge",
                    "AutoChargeException", "ChargeException", ex.Message, JsonHelper.JsonSerializer(ex));
            }
            finally
            {
                if (lockWasTaken)
                {
                    _jbLocker.UnLock(JbWorker.AutoCharge);
                }
            }

        }

        #endregion

        #region Private Methods

        private AutoChargeResult Charge(OrderInstallment installment, Club club, AutoChargePolicy clubPolicy, DateTime dueDate)
        {

            AutoChargeResult result;

            try
            {

                // Check payment policies for this installment ( when installment approved by  policy checker this function returns true and out current attempt number)
                if (!PassAutoChargePolicy(installment, dueDate, clubPolicy, out var currentAttempt, out var errorMessage))
                {
                    result = string.IsNullOrEmpty(errorMessage)
                        ? new AutoChargeResult(installment.Id, installment.OrderItem.Order.ClubId, null, AutoChargeResultType.NotAttempted, null)
                        : new AutoChargeResult(installment.Id, installment.OrderItem.Order.ClubId, null, AutoChargeResultType.AttemptError, errorMessage);
                    return result;
                }

                var paymentResult = _paymentBusiness.ChargeInstallmentNew(installment, club, dueDate);


                if (paymentResult.Succeed)
                {
                    var transactionId = FinalizeCharge(installment, currentAttempt, clubPolicy, true, false);
                    result = new AutoChargeResult(installment.Id, club.Id, paymentResult, AutoChargeResultType.Succsess, null, transactionId);

                    var successEmailParameters = new AutoChargeSuccessEmailParameterModel
                    {
                        InstallmentId = installment.Id,
                        TransactionActivityId = transactionId

                    };
                    _emailBusiness.Send(EmailCategory.AutoChargeSuccess, successEmailParameters);
                }
                else
                {

                    var isUserFailed = EnumHelper.GetEnumDescriptionList<StripeErrorMessage>().Any(e => paymentResult.Message.Trim().ToLower().Contains(e.Trim().ToLower()));

                    long transactionId;
                    if (isUserFailed)
                    {
                        transactionId = FinalizeCharge(installment, currentAttempt, clubPolicy, false);
                        result = new AutoChargeResult(installment.Id, club.Id, paymentResult, AutoChargeResultType.Faild, paymentResult.Message, transactionId);

                        var failEmailParameter = new AutoChargeFailEmailParameterModel
                        {
                            InstallmentId = installment.Id,
                            AttemptNumber = currentAttempt,
                            TransactionActivityId = transactionId
                        };

                        _emailBusiness.Send(EmailCategory.AutoChargeFail, failEmailParameter);

                    }
                    else
                    {
                        transactionId = FinalizeCharge(installment, currentAttempt, clubPolicy, false, false);
                        result = new AutoChargeResult(installment.Id, club.Id, paymentResult, AutoChargeResultType.SystemFaild, paymentResult.Message, transactionId);
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Error("{LogCategory} {AutoChargeMessageType} {ExceptionType} {Message} ", "Auto Charge", "AutoChargeException", "ChargeException", ex.Message);

                var paymentResult = new JbPaymentResult { Succeed = false, Message = ex.Message };
                result = new AutoChargeResult(installment.Id, club.Id, paymentResult, AutoChargeResultType.SystemFaild, paymentResult.Message);
            }
            return result;
        }

        private long FinalizeCharge(OrderInstallment installment, int currentAttempt, AutoChargePolicy clubPolicy, bool paymentSucceed, bool isUserFailed = true)
        {

            if (paymentSucceed)
            {
                installment.AutoChargeAttemps = currentAttempt;
                _installmentBusiness.Update(installment);
            }
            else if (isUserFailed)
            {
                installment.AutoChargeAttemps = currentAttempt;
                _installmentBusiness.Update(installment);

                if (currentAttempt >= GetAutochargePolicyMaxAttempt(clubPolicy))
                {
                    SuspendInstallment(installment);
                }
            }
            else
            {

                installment.AutoChargeAttemps = currentAttempt - 1;
                _installmentBusiness.Update(installment);

            }

            var latestAutochargeTransaction = GetLatestAutochargeTransaction(installment);

            if (latestAutochargeTransaction == null)
            {

                Log.Error("{LogCategory} {AutoChargeMessageType} {OrderInstallmentId} {Message}",
                    "Auto Charge", "latest Transaction null", installment.Id, "Finalize Charge latest Transaction null");

                return 0;
            }

            latestAutochargeTransaction.IsAutoCharge = true;
            latestAutochargeTransaction.AutoChargePolicyAttempt = currentAttempt;

            _transactionActivityBusiness.Update(latestAutochargeTransaction);
            return latestAutochargeTransaction.Id;
        }

        private TransactionActivity GetLatestAutochargeTransaction(OrderInstallment installment)
        {
            return installment.TransactionActivities.LastOrDefault(x => x.TransactionCategory == TransactionCategory.Installment
                                                               && x.TransactionDate.Date == _jbDateTime.UtcNow.Date);
        }

        public bool PassAutoChargePolicy(OrderInstallment installment, DateTime dueDate, AutoChargePolicy policy, out int currentAttempt, out string errorMessage)
        {
            currentAttempt = installment.AutoChargeAttemps + 1;
            errorMessage = string.Empty;

            if (installment.Balance <= 0)
            {
                return false;
            }

            if (currentAttempt == 1)
            {
                return true;
            }

            if (currentAttempt <= GetAutochargePolicyMaxAttempt(policy))
            {
                var lastAutochargeTransaction = GetLastFailedAutochargeTransaction(installment);

                if (lastAutochargeTransaction == null)
                {
                    errorMessage = $"Auto Charge latest Failed Transaction is null {installment.Id}";
                    return false;
                }

                var isUserFailed = EnumHelper.GetEnumDescriptionList<StripeErrorMessage>().Any(e => lastAutochargeTransaction.PaymentDetail.TransactionMessage.Trim().ToLower().Contains(e.Trim().ToLower()));

                if (lastAutochargeTransaction.AutoChargePolicyAttempt != installment.AutoChargeAttemps && isUserFailed)
                {
                    errorMessage = " last failed auto charge transaction attempt number is wrong ";
                    return false;
                }

                // Automatic charge scheduler ignores this installment if it runs twice  or more in one day
                if (lastAutochargeTransaction.TransactionDate.Date == _jbDateTime.UtcNow.Date)
                    return false;

                var currentAttemptPolicy = GetCurrentAttemptPolicy(policy, currentAttempt);
                if (currentAttemptPolicy == null)
                {
                    errorMessage = $"any policy not founded for this attempt(attempt number: { currentAttempt} )";
                    return false;
                }

                var currentAttemptDueDate = GetAttemptDueDate(lastAutochargeTransaction, currentAttemptPolicy);
                return currentAttemptDueDate.Date <= dueDate.Date;
            }

            // This part will happen if  club admin reduces the maximum number of charge attempts.
            SuspendInstallment(installment);
            return false;

        }

        private static DateTime GetAttemptDueDate(TransactionActivity lastAutochargeTransaction, AutoChargeAttempt currentAttemptPolicy)
        {
            return lastAutochargeTransaction.TransactionDate.AddDays(currentAttemptPolicy.IntervalDays);
        }

        private static AutoChargeAttempt GetCurrentAttemptPolicy(AutoChargePolicy policy, int currentAttempt)
        {
            return policy.Attempts.SingleOrDefault(q => q.AttemptOrder == currentAttempt && q.Enabled);
        }

        private static int GetAutochargePolicyMaxAttempt(AutoChargePolicy policy)
        {
            return policy.Attempts?.Count(x => x.Enabled) ?? 0;
        }

        private static TransactionActivity GetLastFailedAutochargeTransaction(OrderInstallment installment)
        {
            return installment.TransactionActivities
                .OrderBy(x => x.TransactionDate)
                .LastOrDefault(z => z.IsAutoCharge
                                    && z.TransactionCategory == TransactionCategory.Installment
                                    && z.TransactionStatus == TransactionStatus.Failure);
        }

        private void SuspendInstallment(OrderInstallment installment)
        {
            installment.SuspendDate = _jbDateTime.UtcNow;
            installment.Status = OrderStatusCategories.error;
            _orderInstallmentRepository.Update(installment);
        }

        //todo  function parameters must changed to string
        private void CreateChargeLog(OrderInstallment installment, AutoChargeResult chargeResult, DateTime dueDate, double chargeTime)
        {
            try
            {

                switch (chargeResult.ResultType)
                {
                    case AutoChargeResultType.NotAttempted:

                        Logger.Information(
                            "{LogCategory} {AutoChargeMessageType} {ResultType} {InstallmentID} {InstallmentDueDate} {SchedulerDate} " +
                            "{ParentUsername} {School} {Partner} {Participant} {ProgramName} {Confirmation} {AutoChargeAttempts}",
                            "Auto Charge",
                            "ChargeResult",
                            "NotAttempted",
                            installment.Id,
                            installment.InstallmentDate,
                            dueDate,
                            installment.OrderItem.Order.User.UserName,
                            installment.OrderItem.Order.Club.Domain,
                            installment.OrderItem.Order.Club.PartnerId.HasValue
                                ? installment.OrderItem.Order.Club.PartnerClub.Domain
                                : "",
                            installment.OrderItem.Player.Contact.FullName,
                            installment.OrderItem.ProgramSchedule.Program.Name,
                            installment.OrderItem.Order.ConfirmationId,
                            installment.AutoChargeAttemps
                        );

                        break;
                    case AutoChargeResultType.AttemptError:
                        Logger.Error(
                            "{LogCategory} {AutoChargeMessageType} {ResultType} {InstallmentID} {InstallmentDueDate} {SchedulerDate} " +
                            "{ParentUsername} {School} {Partner} {Participant} {ProgramName} {Confirmation} {AutoChargeAttempts} {Message}  {StartDateTime} {TimeDuration}",
                            "Auto Charge",
                            "ChargeResult",
                            "AttemptError",
                            installment.Id,
                            installment.InstallmentDate,
                            dueDate,
                            installment.OrderItem.Order.User.UserName,
                            installment.OrderItem.Order.Club.Domain,
                            installment.OrderItem.Order.Club.PartnerId.HasValue
                                ? installment.OrderItem.Order.Club.PartnerClub.Domain
                                : "",
                            installment.OrderItem.Player.Contact.FullName,
                            installment.OrderItem.ProgramSchedule.Program.Name,
                            installment.OrderItem.Order.ConfirmationId,
                            installment.AutoChargeAttemps,
                            chargeResult.ErrorMessage
                        );
                        break;
                    case AutoChargeResultType.Succsess:
                        Logger.Information(
                            "{LogCategory} {AutoChargeMessageType} {ResultType} {InstallmentID} {InstallmentDueDate} {SchedulerDate} " +
                            "{ParentUsername} {School} {Partner} {Participant} {ProgramName} {Confirmation} {AutoChargeAttempts} {ChargeTime} ",
                            "Auto Charge",
                            "ChargeResult",
                            "Success",
                            installment.Id,
                            installment.InstallmentDate,
                            dueDate,
                            installment.OrderItem.Order.User.UserName,
                            installment.OrderItem.Order.Club.Domain,
                            installment.OrderItem.Order.Club.PartnerId.HasValue
                                ? installment.OrderItem.Order.Club.PartnerClub.Domain
                                : "",
                            installment.OrderItem.Player.Contact.FullName,
                            installment.OrderItem.ProgramSchedule.Program.Name,
                            installment.OrderItem.Order.ConfirmationId,
                            installment.AutoChargeAttemps,
                            chargeTime

                        );
                        break;
                    case AutoChargeResultType.SystemFaild:
                        Logger.Error(
                            "{LogCategory} {AutoChargeMessageType} {ResultType} {InstallmentID} {InstallmentDueDate} {SchedulerDate} " +
                            "{ParentUsername} {School} {Partner} {Participant} {ProgramName} {Confirmation} {AutoChargeAttempts} {Message} {ChargeTime} ",
                            "Auto Charge",
                            "ChargeResult",
                            "SystemPaymentFailed",
                            installment.Id,
                            installment.InstallmentDate,
                            dueDate,
                            installment.OrderItem.Order.User.UserName,
                            installment.OrderItem.Order.Club.Domain,
                            installment.OrderItem.Order.Club.PartnerId.HasValue
                                ? installment.OrderItem.Order.Club.PartnerClub.Domain
                                : "",
                            installment.OrderItem.Player.Contact.FullName,
                            installment.OrderItem.ProgramSchedule.Program.Name,
                            installment.OrderItem.Order.ConfirmationId,
                            installment.AutoChargeAttemps,
                            chargeResult.ErrorMessage,
                            chargeTime
                        );
                        break;
                    case AutoChargeResultType.Faild:
                        Logger.Error(
                            "{LogCategory} {AutoChargeMessageType} {ResultType} {InstallmentID} {InstallmentDueDate} {SchedulerDate} " +
                            "{ParentUsername} {School} {Partner} {Participant} {ProgramName} {Confirmation} {AutoChargeAttempts} {Message} {ChargeTime} ",
                            "Auto Charge",
                            "ChargeResult",
                            "UserPaymentFailed",
                            installment.Id,
                            installment.InstallmentDate,
                            dueDate,
                            installment.OrderItem.Order.User.UserName,
                            installment.OrderItem.Order.Club.Domain,
                            installment.OrderItem.Order.Club.PartnerId.HasValue
                                ? installment.OrderItem.Order.Club.PartnerClub.Domain
                                : "",
                            installment.OrderItem.Player.Contact.FullName,
                            installment.OrderItem.ProgramSchedule.Program.Name,
                            installment.OrderItem.Order.ConfirmationId,
                            installment.AutoChargeAttemps,
                            chargeResult.ErrorMessage,
                            chargeTime
                        );
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("{LogCategory} {AutoChargeMessageType} {ExceptionType} {Message} ",
                    "Auto Charge", "AutoChargeException", "SendLogException", ex.Message);
            }
        }

        private void SendReports(IEnumerable<AutoChargeResult> installmentsChargeResults, Dictionary<int, bool> clubsSendSummary)
        {
            var clubs = installmentsChargeResults.GroupBy(x => x.ClubId);
            var supportSummaryEmailParameter = new AutoChargeSupportSummaryEmailParameterModel();

            foreach (var club in clubs)
            {
                var clubSummaryEmailParameter = new AutoChargeSummaryEmailParameterModel(club.Key, DateTime.UtcNow);

                foreach (var result in club)
                {
                    switch (result.ResultType)
                    {
                        case AutoChargeResultType.NotAttempted:
                            supportSummaryEmailParameter.NotAttemptedInstallments.Add(result.InstallmentId);
                            break;
                        case AutoChargeResultType.AttemptError:
                            supportSummaryEmailParameter.OtherErrorInstallments.Add(result.InstallmentId);
                            break;
                        case AutoChargeResultType.SystemFaild:
                            supportSummaryEmailParameter.SystemFailedInstallments.Add(new Tuple<long, long>(result.InstallmentId, result.TransActionActivityId));
                            break;

                        case AutoChargeResultType.Faild:
                            clubSummaryEmailParameter.FailedInstallments.Add(new Tuple<long, long>(result.InstallmentId, result.TransActionActivityId));
                            supportSummaryEmailParameter.FailedInstallments.Add(new Tuple<long, long>(result.InstallmentId, result.TransActionActivityId));
                            break;
                        case AutoChargeResultType.Succsess:
                            clubSummaryEmailParameter.SuccessInstallments.Add(new Tuple<long, long>(result.InstallmentId, result.TransActionActivityId));
                            supportSummaryEmailParameter.SuccessInstallments.Add(result.InstallmentId);
                            break;
                        default:
                            continue;
                    }
                }

                var hasValue = clubsSendSummary.TryGetValue(club.Key, out var sendSummary);
                if (hasValue && sendSummary && (clubSummaryEmailParameter.SuccessInstallments.Count + clubSummaryEmailParameter.FailedInstallments.Count) > 0)
                    _emailBusiness.Send(EmailCategory.AutoChargeSummary, clubSummaryEmailParameter);
            }

            _emailBusiness.Send(EmailCategory.AutoChargeSupportSummary, supportSummaryEmailParameter);
        }


        #endregion
    }
}

