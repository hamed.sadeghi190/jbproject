
CREATE TABLE [dbo].[ProgramSchedulePartCharges](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SchedulePartId] [int] NOT NULL,
	[ChargeId] [bigint] NOT NULL,
	[ChargeAmount] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_dbo.ProgramSchedulePartCharges] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProgramSchedulePartCharges]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProgramSchedulePartCharges_dbo.Charges_ChargeId] FOREIGN KEY([ChargeId])
REFERENCES [dbo].[Charges] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ProgramSchedulePartCharges] CHECK CONSTRAINT [FK_dbo.ProgramSchedulePartCharges_dbo.Charges_ChargeId]
GO

ALTER TABLE [dbo].[ProgramSchedulePartCharges]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProgramSchedulePartCharges_dbo.ProgramScheduleParts_SchedulePartId] FOREIGN KEY([SchedulePartId])
REFERENCES [dbo].[ProgramScheduleParts] ([Id])
GO

ALTER TABLE [dbo].[ProgramSchedulePartCharges] CHECK CONSTRAINT [FK_dbo.ProgramSchedulePartCharges_dbo.ProgramScheduleParts_SchedulePartId]
GO


