﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;
using Jumbula.Core.Business;

namespace Jumbula.Core.Domain
{
    public class Invoice : BaseEntity, IProtectedResource
    {

        public int ClubId { get; set; }
        [ForeignKey("ClubId")]
        public virtual Club Club { get; set; }

        public decimal Amount { get; set; }

        public int? DueDate { get; set; }

        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual JbUser User { get; set; }

        public decimal PaidAmount { get; set; }

        public long InvoiceNumber { get; set; }
        [StringLength(2000)]
        public string Memo { get; set; }
        [StringLength(4000)]
        public string NotToRecipient { get; set; }
        [StringLength(256)]
        public string EmailToCc { get; set; }
        public string Reference { get; set; }
        public DateTime InvoicingDate { get; set; }

        public InvoiceStatus Status { get; set; }
        public virtual ICollection<InvoiceHistory> InvoiceHistories { get; set; }

        public bool DoesTheClubOwnIt(int clubId)
        {
            if (ClubId == clubId)
                return true;

            return false;
        }

        public bool DoesTheParentOwnIt(int userId)
        {
            if (UserId == userId)
                return true;

            return false;
        }
    }

    public enum InvoiceStatus : byte
    {
        Unpaid = 0,
        Paid = 1,
        Canceled = 2

    }
}
