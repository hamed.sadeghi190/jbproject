﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Newtonsoft.Json;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class DiscountController : DashboardBaseController
    {
        public virtual ActionResult Discounts()
        {
            return View();
        }

        public virtual ActionResult MultiScheduleDiscountManage()
        {
            return View();
        }

        public virtual ActionResult GeneralDiscountManage()
        {
            return View();
        }
        public virtual ActionResult DiscountsAddChooseTypeBlade()
        {
            return View();
        }


        public virtual JsonNetResult CreateEditGeneralDiscount(long id, string seasonDomain)
        {
            var model = new GeneralDiscountViweModel();
            List<SelectKeyValue<string>> applyList = null;
            var club = ActiveClub;
            if (id > 0)
            {
                var discount = Ioc.DiscountBusiness.Get(id);
                CheckResourceForAccess(discount);

                applyList = ToSelectListType();
                model = discount.ToViewModel<GeneralDiscountViweModel>();

                model.SelectedPrograms = new List<string>();
                foreach (var item in discount.Programs.Select(c => c.Id).ToList())
                {
                    model.SelectedPrograms.Add(item.ToString());
                }

                model.SelectedBenefit = new SelectKeyValue<string>() { Text = discount.BenefitType.ToString(), Value = (discount.BenefitType).ToString() };
                model.NOP = new SelectKeyValue<string>() { Text = discount.NOP.ToString(), Value = discount.NOP.ToString() };
                model.ApplyType = (int)discount.ApplyType;

                model.SelectedAmountType = new SelectKeyValue<string>() { Text = discount.AmountType.ToString(), Value = discount.AmountType.ToString() };
                if (model.SelectedAmountType.Value == "Fixed")
                {
                    model.SelectedAmountType = new SelectKeyValue<string>() { Text = ChargeDiscountType.Fixed.ToString(), Value = "0" };
                }
                else
                {
                    model.SelectedAmountType = new SelectKeyValue<string>() { Text = ChargeDiscountType.Percent.ToString(), Value = "1" };
                }

                model.SelectedProgramMode = new SelectKeyValue<string>() { Text = discount.IsAllProgram.ToString(), Value = discount.IsAllProgram.ToString() };

                if (model.SelectedProgramMode.Value == "False")
                {
                    model.SelectedProgramMode = new SelectKeyValue<string>() { Text = ProgramMode.SelectedPrograms.ToString(), Value = (ProgramMode.SelectedPrograms.ToString()) }; ;
                }
                else
                {
                    model.SelectedProgramMode = new SelectKeyValue<string>() { Text = ProgramMode.AllPrograms.ToString(), Value = (ProgramMode.AllPrograms.ToString()) }; ;
                }
                if (discount.Category == ChargeDiscountCategory.AllProgramAllParticipant)
                {
                    model.SelectedConditionProgram = new SelectKeyValue<string>() { Text = ProgramConditions.NoCondition.ToString(), Value = (ProgramConditions.NoCondition.ToString()) };
                    model.SelectedConditionParticipent = new SelectKeyValue<string>() { Text = ParticipentConditions.NoCondition.ToString(), Value = (ParticipentConditions.NoCondition.ToString()) };
                }

            }
            else
            {
                applyList = ToSelectListType();
                model.SelectedBenefit = new SelectKeyValue<string>() { Text = DiscountBenefitType.Organization.ToString(), Value = (DiscountBenefitType.Organization).ToString() };
                model.NOP = new SelectKeyValue<string>() { Text = "Select", Value = "0" };
                model.SeasonDomain = seasonDomain;
                model.SelectedConditionProgram = new SelectKeyValue<string>() { Text = ProgramConditions.SelectCondition.ToString(), Value = (ProgramConditions.SelectCondition.ToString()) };
                model.SelectedConditionParticipent = new SelectKeyValue<string>() { Text = ParticipentConditions.SelectCondition.ToString(), Value = (ParticipentConditions.SelectCondition.ToString()) };
                model.SelectedProgramMode = new SelectKeyValue<string>() { Text = ProgramMode.Select.ToString(), Value = (ProgramMode.Select.ToString()) };
                model.SelectedAmountType = new SelectKeyValue<string>() { Text = "Select a Amount", Value = "-1" };

            }

            var programs = GetListPrograms(seasonDomain, null);
            model.ProgramsList = new List<SelectKeyValue<string>>();

            foreach (var program in programs)
            {
                model.ProgramsList.Add(new SelectKeyValue<string>
                {
                    Text = program.Name,
                    Value = program.Id.ToString()
                });
            }

            model.ApplyItems = applyList;
            model.BenefitItems = DropdownHelpers.ToSelectList<DiscountBenefitType>();
            model.ConditionProgram = DropdownHelpers.ToSelectList<ProgramConditions>();
            model.ConditionParticipent = DropdownHelpers.ToSelectList<ParticipentConditions>();
            model.IsAllPrograms = DropdownHelpers.ToSelectList<ProgramMode>();
            model.NopItems = SeedNopForGeneralDiscount(5);

            return JsonNet(model, new JsonSerializerSettings());
        }
        public virtual JsonNetResult GetNop()
        {
            List<JbTitleValue<string>> numberOfUser = new List<JbTitleValue<string>>();
            for (int i = 2; i < 11; i++)
            {
                numberOfUser.Add(new JbTitleValue<string>() { Title = i.ToString(), Value = i.ToString() });
            }

            return JsonNet(numberOfUser);
        }

        public virtual JsonNetResult GetApplyType()
        {
            var applyType = Enum.GetValues(typeof(DiscountApplyType))
                .Cast<DiscountApplyType>()
                .Select(v => new JbTitleValue<string>() { Title = v.ToString(), Value = ((int)v).ToString() })
                .ToList();
            return JsonNet(applyType);
        }

        public virtual JsonNetResult CreateEditDiscount(long id, string seasonDomain, ChargeDiscountCategory discountCategory = ChargeDiscountCategory.CustomDiscount, ProgramTypeCategory type = ProgramTypeCategory.Class)
        {
            var model = new DiscountViewModel();
            List<SelectKeyValue<string>> applyList = null;

            if (id > 0)
            {
                var discount = Ioc.DiscountBusiness.Get(id);
                CheckResourceForAccess(discount);

                applyList = ToSelectList(discount.Category);
                model = discount.ToViewModel<DiscountViewModel>();

                model.SelectedPrograms = new List<string>();
                foreach (var item in discount.Programs.Select(c => c.Id).ToList())
                {
                    model.SelectedPrograms.Add(item.ToString());
                }

                model.SelectedBenefit = new SelectKeyValue<string>() { Text = discount.BenefitType.ToString(), Value = (discount.BenefitType).ToString() };
                model.NOP = new SelectKeyValue<string>() { Text = discount.NOP.ToString(), Value = discount.NOP.ToString() };
                model.ApplyType = (int)discount.ApplyType;
            }
            else
            {
                applyList = ToSelectList(discountCategory);
                model.SelectedBenefit = new SelectKeyValue<string>() { Text = DiscountBenefitType.Organization.ToString(), Value = (DiscountBenefitType.Organization).ToString() };
                model.NOP = new SelectKeyValue<string>() { Text = "2", Value = "2" };
                model.SeasonDomain = seasonDomain;
                model.Category = discountCategory;
            }

            var programs = type == ProgramTypeCategory.Class ? GetListPrograms(seasonDomain, null) : GetListPrograms(seasonDomain, null, ProgramTypeCategory.Camp);
            model.ProgramsList = new List<SelectKeyValue<string>>();

            foreach (var program in programs)
            {
                model.ProgramsList.Add(new SelectKeyValue<string>
                {
                    Text = program.Name,
                    Value = program.Id.ToString()
                });
            }

            model.ApplyItems = applyList;
            model.BenefitItems = DropdownHelpers.ToSelectList<DiscountBenefitType>();
            model.NopItems = SeedNop(10);
            return JsonNet(model, new JsonSerializerSettings());
        }

        [HttpPost]
        public virtual ActionResult SaveDiscount(DiscountViewModel model)
        {
            CheckModelIntegrity(ModelState, model);
            if (ModelState.IsValid)
            {
                Discount discount;
                OperationStatus res;
                var club = ActiveClub;

                if (model.Id > 0)
                {
                    discount = model.ToModel<Discount>(Ioc.DiscountBusiness.Get(model.Id));
                    CheckResourceForAccess(discount);
                }
                else
                {
                    discount = model.ToModel<Discount>();
                }

                discount.NOP = int.Parse(model.NOP.Value);
                discount.BenefitType = (DiscountBenefitType)Enum.Parse(typeof(DiscountBenefitType), model.SelectedBenefit.Text, true);
                discount.Season = Ioc.SeasonBusiness.Get(model.SeasonDomain, club.Id);
                discount.SeasonId = discount.Season.Id;
                discount.ApplyType = (DiscountApplyType)model.ApplyType;

                if (!model.IsAllProgram && model.SelectedPrograms != null && model.SelectedPrograms.Any())
                {
                    discount.Programs.Clear();

                    foreach (var id in model.SelectedPrograms)
                    {
                        var programId = long.Parse(id);
                        discount.Programs.Add(Ioc.ProgramBusiness.Get(programId));
                    }
                    discount.IsAllProgram = false;
                }
                else
                {
                    discount.Programs.Clear();
                    discount.IsAllProgram = true;
                }

                discount.Description = GenerateDescription(discount.Category, discount.NOP, discount.Amount, discount.AmountType, discount.ApplyType);

                if (discount.Id == 0)
                {
                    discount.MetaData = new MetaData()
                    {
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow
                    };

                    res = Ioc.DiscountBusiness.Create(discount);
                }
                else
                {
                    discount.MetaData.DateUpdated = DateTime.UtcNow;
                    res = Ioc.DiscountBusiness.Update(discount);
                }

                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return JsonFormResponse();
        }
        [HttpPost]
        public virtual ActionResult SaveGeneralDiscount(GeneralDiscountViweModel model)
        {
            CheckDiscountModelIntegrity(ModelState, model);
            if (ModelState.IsValid)
            {
                Discount discount;
                OperationStatus res;
                var club = ActiveClub;

                if (model.Id > 0)
                {
                    discount = model.ToModel<Discount>(Ioc.DiscountBusiness.Get(model.Id));
                }
                else
                {
                    discount = model.ToModel<Discount>();
                }
                discount.NOP = int.Parse(model.NOP.Value);
                discount.AmountType = (ChargeDiscountType)Enum.Parse(typeof(ChargeDiscountType), model.SelectedAmountType.Text, true);
                discount.BenefitType = (DiscountBenefitType)Enum.Parse(typeof(DiscountBenefitType), model.SelectedBenefit.Text, true);
                discount.Season = Ioc.SeasonBusiness.Get(model.SeasonDomain, club.Id);
                discount.SeasonId = discount.Season.Id;
                //Add AplayType
                discount.ApplyType = (DiscountApplyType)model.ApplyType;
                //Add category
                if (model.SelectedConditionParticipent.Value == "NoCondition" && model.SelectedConditionProgram.Value == "NoCondition")
                {
                    discount.Category = ChargeDiscountCategory.AllProgramAllParticipant;
                }
                //Add daiscount for program
                if (model.SelectedProgramMode.Value != "AllPrograms" && model.SelectedPrograms.Any())
                {
                    discount.Programs.Clear();
                    foreach (var id in model.SelectedPrograms)
                    {
                        var programId = long.Parse(id);
                        discount.Programs.Add(Ioc.ProgramBusiness.Get(programId));
                    }
                    discount.IsAllProgram = false;
                }
                else
                {
                    discount.Programs.Clear();
                    discount.IsAllProgram = true;
                }
                discount.Description = GenerateDescription(discount.Category, discount.NOP, discount.Amount, discount.AmountType, discount.ApplyType);

                if (discount.Id == 0)
                {
                    discount.MetaData = new MetaData()
                    {
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow
                    };

                    res = Ioc.DiscountBusiness.Create(discount);
                }
                else
                {
                    discount.MetaData.DateUpdated = DateTime.UtcNow;
                    res = Ioc.DiscountBusiness.Update(discount);
                }

                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return JsonFormResponse();
        }
        private void CheckDiscountModelIntegrity(ModelStateDictionary modelState, GeneralDiscountViweModel model)
        {

            if (model.SelectedConditionParticipent.Value == "SelectCondition")
            {
                modelState.AddModelError("model.SelectedConditionParticipent", "Please select participant condition.");
            }
            if (model.SelectedConditionProgram.Value == "SelectCondition")
            {
                modelState.AddModelError("model.SelectedConditionProgram", "Please select program condition.");
            }
            if (model.SelectedProgramMode.Value == "SelectedPrograms" && (model.SelectedPrograms == null || model.SelectedPrograms.Count == 0))
            {
                modelState.AddModelError("model.SelectedPrograms", Constants.E_SelectedProgram);
            }
            else
            {
                modelState.Remove("model.SelectedPrograms");
            }
            if (model.SelectedAmountType.Value == "-1")
            {
                modelState.AddModelError("model.SelectedAmountType", "Please select discount type.");
            }
            if (model.SelectedAmountType.Value == "1" && (model.Amount == 0 || model.Amount > 100))
            {
                modelState.AddModelError("model.Amount", Constants.E_PercentageAmount);
            }
            if (model.SelectedAmountType.Value == "0" && (model.Amount == 0))
            {
                modelState.AddModelError("model.Amount", Constants.E_Amount);
            }
            if (model.NOP.Value == "0")
            {
                modelState.AddModelError("model.NOP", "Please select number of orders.");
            }
            if (model.SelectedProgramMode.Value == "Select")
            {
                modelState.AddModelError("model.SelectedProgramMode", "Please select eligible programs.");
            }
        }
        public virtual JsonResult DeleteDiscount(long discountId)
        {

            var res = Ioc.DiscountBusiness.Delete(discountId);
            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }

        public virtual ActionResult DiscountManage()
        {

            return View();
        }

        [HttpGet]
        public virtual JsonNetResult GetAllDiscounts(string seasonDomain, int skip, int take, int page, int pageSize, List<GridSort> sort = null)
        {
            var termValue = Request.Params["term"] ?? string.Empty;
            var club = ActiveClub;
            var seasonId = Ioc.SeasonBusiness.Get(seasonDomain, club.Id).Id;
            var discounts = Ioc.DiscountBusiness.
                GetList(seasonId).Select(c => new DiscountViewModel() { Id = c.Id, Name = c.Name, Description = c.Description, Category = c.Category }).ToList();
            int total = 0;
            if (sort != null)
            {
                foreach (var s in sort)
                {
                    switch (s.field)
                    {
                        case "Name":
                            {
                                discounts = (s.dir == "asc") ? discounts.OrderBy(o => o.Name).ToList() : discounts.OrderByDescending(o => o.Name).ToList();
                                break;
                            }
                        case "Category":
                            {
                                discounts = (s.dir == "asc") ? discounts.OrderBy(o => o.Category).ToList() : discounts.OrderByDescending(o => o.Category).ToList();
                                break;
                            }
                        default:
                            break;
                    }
                }
            }

            List<DiscountViewModel> filteredData = null;

            if (string.IsNullOrEmpty(termValue))
            {
                filteredData = discounts.Skip(skip).Take(take).ToList();
                total = discounts.Count();
            }
            else
            {
                var filters = discounts.Where(r => r.Name.StartsWith(termValue)).OrderBy(o => o.Id);
                total = filters.Count();
                filteredData = filters.Skip(skip).Take(take).ToList();
            }


            return JsonNet(new { DataSource = filteredData, TotalCount = total });
        }

        private List<SelectKeyValue<string>> SeedNop(int max)
        {
            var result = new List<SelectKeyValue<string>>();
            for (int i = 2; i < max + 1; i++)
            {
                result.Add(new SelectKeyValue<string> { Text = i.ToString(), Value = i.ToString() });
            }

            return result;
        }

        private List<SelectKeyValue<string>> SeedNopForGeneralDiscount(int max)
        {
            var result = new List<SelectKeyValue<string>>();
            result.Add(new SelectKeyValue<string> { Text = "Select", Value = 0.ToString() });
            for (int i = 2; i < max + 1; i++)
            {
                result.Add(new SelectKeyValue<string> { Text = i.ToString(), Value = i.ToString() });
            }

            return result;
        }
        private List<SelectKeyValue<string>> ToSelectList(ChargeDiscountCategory discountCategory)
        {
            List<SelectKeyValue<string>> values = new List<SelectKeyValue<string>>();

            if (discountCategory == ChargeDiscountCategory.MultiPerson)
            {
                values.Add(new SelectKeyValue<string>() { Text = EveryPerson, Value = ((int)DiscountApplyType.Every).ToString() });
                values.Add(new SelectKeyValue<string>() { Text = SelectedNumOnlyPerson, Value = ((int)DiscountApplyType.SelectedNumOnly).ToString() });
                values.Add(new SelectKeyValue<string>() { Text = SelectedNumAndAdditionPerson, Value = ((int)DiscountApplyType.SelectedNumAndAddition).ToString() });
            }
            else if (discountCategory == ChargeDiscountCategory.MultiSchedule)
            {
                values.Add(new SelectKeyValue<string>() { Text = EverySchedule, Value = ((int)DiscountApplyType.Every).ToString() });
                values.Add(new SelectKeyValue<string>() { Text = SelectedNumOnlySchedule, Value = ((int)DiscountApplyType.SelectedNumOnly).ToString() });
                values.Add(new SelectKeyValue<string>() { Text = SelectedNumAndAdditionSchedule, Value = ((int)DiscountApplyType.SelectedNumAndAddition).ToString() });
            }
            else
            {
                values.Add(new SelectKeyValue<string>() { Text = EveryProgram, Value = ((int)DiscountApplyType.Every).ToString() });
                values.Add(new SelectKeyValue<string>() { Text = SelectedNumOnlyProgram, Value = ((int)DiscountApplyType.SelectedNumOnly).ToString() });
                values.Add(new SelectKeyValue<string>() { Text = SelectedNumAndAdditionProgram, Value = ((int)DiscountApplyType.SelectedNumAndAddition).ToString() });
            }


            return values;
        }
        private List<SelectKeyValue<string>> ToSelectListType()
        {
            List<SelectKeyValue<string>> values = new List<SelectKeyValue<string>>();

            values.Add(new SelectKeyValue<string>() { Text = EveryOrder, Value = ((int)DiscountApplyType.Every).ToString() });
            //values.Add(new SelectKeyValue<string>() { Text = SelectedNumOnlyOrder, Value = ((int)DiscountApplyType.SelectedNumOnly).ToString() });
            //values.Add(new SelectKeyValue<string>() { Text = SelectedNumAndAdditionOrder, Value = ((int)DiscountApplyType.SelectedNumAndAddition).ToString() });

            return values;
        }
        private void CheckModelIntegrity(ModelStateDictionary modelState, DiscountViewModel model)
        {

            if (!model.IsAllProgram && (model.SelectedPrograms == null || model.SelectedPrograms.Count == 0))
            {
                modelState.AddModelError("model.SelectedPrograms", Constants.E_SelectedProgram);
            }
            else
            {
                modelState.Remove("model.SelectedPrograms");
            }
            if (model.AmountType == ChargeDiscountType.Percent && (model.Amount == 0 || model.Amount > 100))
            {
                modelState.AddModelError("model.Amount", Constants.E_PercentageAmount);
            }
            if (model.AmountType == ChargeDiscountType.Fixed && (model.Amount == 0))
            {
                modelState.AddModelError("model.Amount", Constants.E_Amount);
            }
        }

        public virtual List<ProgramsViewModel> GetListPrograms(string seasonDomain, string term, ProgramTypeCategory type = ProgramTypeCategory.Class)
        {
            var termValue = Request.Params["term"] ?? string.Empty;
            var allprograms =
               Ioc.ProgramBusiness.GetList()
                   .Where(
                       p =>
                           p.ClubId == ActiveClub.Id && p.Season.Domain.ToLower() == seasonDomain.ToLower() &&
                           p.Status == ProgramStatus.Open && p.Name.StartsWith(termValue));

            var model = new List<ProgramsViewModel>();

            if (type != ProgramTypeCategory.Camp)
            {
                model = allprograms.Select(p =>
                    new ProgramsViewModel
                    {
                        Id = p.Id,
                        ClubDomain = p.Club.Domain,
                        SeasonDomain = p.Season.Domain,
                        Name = p.Name,
                        TypeCategory = p.TypeCategory
                    })
                    .ToList();
            }
            else
            {
                model = allprograms.Where(p => p.TypeCategory == ProgramTypeCategory.Camp).Select(p =>
                    new ProgramsViewModel
                    {
                        Id = p.Id,
                        ClubDomain = p.Club.Domain,
                        SeasonDomain = p.Season.Domain,
                        Name = p.Name,
                        TypeCategory = p.TypeCategory
                    })
                    .ToList();
            }

            return model;
        }

        public virtual JsonNetResult GetListProgramsForCamp(string seasonDomain, string term, ProgramTypeCategory type = ProgramTypeCategory.Class)
        {
            var termValue = Request.Params["term"] ?? string.Empty;
            var allprograms =
                Ioc.ProgramBusiness.GetList()
                    .Where(
                        p =>
                            p.ClubId == ActiveClub.Id && p.Season.Domain.ToLower() == seasonDomain.ToLower() &&
                            p.Status == ProgramStatus.Open && p.Name.StartsWith(termValue));

            var model = new List<ProgramsViewModel>();

            model = allprograms.Where(p => p.TypeCategory == ProgramTypeCategory.Camp).Select(p =>
                new ProgramsViewModel
                {
                    Id = p.Id,
                    ClubDomain = p.Club.Domain,
                    SeasonDomain = p.Season.Domain,
                    Name = p.Name,
                    TypeCategory = p.TypeCategory
                })
                .ToList();

            var programModel = new List<SelectKeyValue<string>>();
            foreach (var program in model)
            {
                programModel.Add(new SelectKeyValue<string>
                {
                    Text = program.Name,
                    Value = program.Id.ToString()
                });
            }

            return JsonNet(programModel, new JsonSerializerSettings(), Constants.DefaultDateTimeFormat);

        }
        private string GenerateDescription(ChargeDiscountCategory discountCategory, int nop, decimal amount, ChargeDiscountType amountType, DiscountApplyType applyType)
        {
            var desc = "{0} off ";

            if (amountType == ChargeDiscountType.Fixed)
            {
                desc = string.Format(desc, CurrencyHelper.FormatCurrencyWithPenny(amount, ActiveClub.Currency));
            }
            else
            {
                desc = string.Format(desc, amount, "%");
            }

            if (discountCategory == ChargeDiscountCategory.CustomDiscount)
            {
                return desc;
            }
            else
            {
                if (discountCategory == ChargeDiscountCategory.MultiPerson)
                {
                    switch (applyType)
                    {
                        case DiscountApplyType.Every:
                            return desc + "for " + EveryPerson;
                        case DiscountApplyType.SelectedNumOnly:
                            return string.Format(desc + "for " + SelectedNumOnlyPerson, NumberHelper.ToOrdinal(nop));
                        case DiscountApplyType.SelectedNumAndAddition:
                            return string.Format(desc + "for " + SelectedNumAndAdditionPerson, NumberHelper.ToOrdinal(nop));
                    }
                }
                else if (discountCategory == ChargeDiscountCategory.MultiSchedule)
                {
                    switch (applyType)
                    {
                        case DiscountApplyType.Every:
                            return desc + "for " + EverySchedule;
                        case DiscountApplyType.SelectedNumOnly:
                            return string.Format(desc + "for " + SelectedNumOnlySchedule, NumberHelper.ToOrdinal(nop));
                        case DiscountApplyType.SelectedNumAndAddition:
                            return string.Format(desc + "for " + SelectedNumAndAdditionSchedule, NumberHelper.ToOrdinal(nop));
                    }
                }
                else if (discountCategory == ChargeDiscountCategory.MultiProgram)
                {
                    switch (applyType)
                    {
                        case DiscountApplyType.Every:
                            return desc + "for " + EveryProgram;
                        case DiscountApplyType.SelectedNumOnly:
                            return string.Format(desc + "for " + SelectedNumOnlyProgram, NumberHelper.ToOrdinal(nop));
                        case DiscountApplyType.SelectedNumAndAddition:
                            return string.Format(desc + "for " + SelectedNumAndAdditionProgram, NumberHelper.ToOrdinal(nop));
                    }
                }
                else
                {
                    switch (applyType)
                    {
                        case DiscountApplyType.Every:
                            return desc + "for " + EveryOrder;
                        case DiscountApplyType.SelectedNumOnly:
                            return string.Format(desc + "for " + SelectedNumOnlyOrder, NumberHelper.ToOrdinal(nop));
                        case DiscountApplyType.SelectedNumAndAddition:
                            return string.Format(desc + "for " + SelectedNumAndAdditionOrder, NumberHelper.ToOrdinal(nop));
                    }
                }
            }
            return desc;

        }

        private const string EveryPerson = "Every person";
        private const string SelectedNumOnlyPerson = "The {0} person";
        private const string SelectedNumAndAdditionPerson = "The {0} person and all additional";

        private const string EveryProgram = "Every program";
        private const string SelectedNumOnlyProgram = "The {0} program";
        private const string SelectedNumAndAdditionProgram = "The {0} program and all additional";

        private const string EverySchedule = "Every schedule";
        private const string SelectedNumOnlySchedule = "The {0} schedule";
        private const string SelectedNumAndAdditionSchedule = "The {0} schedule and all additional";


        //Add for gneral discount
        private const string Select = "Select";

        private const string EveryOrder = "Every order";
        private const string SelectedNumOnlyOrder = "The {0} order";
        private const string SelectedNumAndAdditionOrder = "The {0} order and all additional";


    }
}