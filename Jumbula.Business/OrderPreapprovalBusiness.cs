﻿
using Jb.Framework.Common;
using System;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;


namespace Jumbula.Business
{
    public class OrderPreapprovalBusiness : IOrderPreapprovalBusiness
    {
        private readonly IRepository<OrderPreapproval, long> _orderPreapprovalRepository;
        public OrderPreapprovalBusiness(IRepository<OrderPreapproval, long> orderPreapprovalRepository)
        {
            _orderPreapprovalRepository = orderPreapprovalRepository;
        }

        public  bool IsPreapprovalKeyExist(string preapprovalKey)
        {
            return _orderPreapprovalRepository.GetList().Any(c => c.PaypalPreapprovalID == preapprovalKey);
        }
        public IQueryable<OrderPreapproval> GetListByOrderId(long orderId)
        {
            return _orderPreapprovalRepository.GetList().Where(i => i.OrderId == orderId);
        }
        public string GetPreapprovalKeyByOrderId(long orderId)
        {
            var preapprovalList = _orderPreapprovalRepository.GetList().Where(i => i.OrderId == orderId && i.PaypalPreapprovalStartDate.Value <= DateTime.UtcNow && i.PaypalPreapprovalEndDate.Value >= DateTime.UtcNow).ToList();
            if (preapprovalList != null && preapprovalList.Count > 0)
            {
                return preapprovalList.LastOrDefault().PaypalPreapprovalID;
            }
            return string.Empty;
        }

        public OperationStatus Update(OrderPreapproval orderPreapproval)
        {
            return _orderPreapprovalRepository.Update(orderPreapproval);
        }

        public OperationStatus Create(OrderPreapproval orderPreapproval)
        {
            return _orderPreapprovalRepository.Create(orderPreapproval);
        }
    }
}
