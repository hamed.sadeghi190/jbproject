﻿using Jb.Framework.Common;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IOrderPreapprovalBusiness
    {
        IQueryable<OrderPreapproval> GetListByOrderId(long orderId);
        OperationStatus Update(OrderPreapproval orderPreapproval);
        OperationStatus Create(OrderPreapproval orderPreapproval);
        bool IsPreapprovalKeyExist(string preapprovalKey);
        string GetPreapprovalKeyByOrderId(long orderId);
    }
}
