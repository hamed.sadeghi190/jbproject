alter  TABLE [dbo].[PricePlans] add [Revenue] [tinyint]  NULL
go
update [dbo].[PricePlans] set Revenue=1
Go
alter TABLE [dbo].[PricePlans] 	alter column [Revenue] [tinyint] Not NULL
Go
alter  TABLE [dbo].[PricePlans] add	[Type] [tinyint]  NULL
go
update [dbo].[PricePlans] set [Type]=1
Go
alter TABLE [dbo].[PricePlans] 	alter column [Type] [tinyint] Not NULL
Go
alter TABLE [dbo].[Clubs] 	add [Revenue] [tinyint]  NULL
Go
update clubs set Revenue=1
Go
alter TABLE [dbo].[Clubs] 	alter column [Revenue] [tinyint] Not NULL
Go
CREATE TABLE [dbo].[ClientCreditCards](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ClientId] [bigint] NOT NULL,
	[CardHolderName] [nvarchar](max) NULL,
	[CardNumber] [nvarchar](max) NULL,
	[ExpiryYear] [nvarchar](4) NULL,
	[ExpiryMonth] [nvarchar](2) NULL,
	[Brand] [nvarchar](20) NULL,
	[IsDefault] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[Address_Id] [int] NULL,
 CONSTRAINT [PK_dbo.ClientCreditCards] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[ClientCreditCards]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClientCreditCards_dbo.Clients_ClientId] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Clients] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ClientCreditCards] CHECK CONSTRAINT [FK_dbo.ClientCreditCards_dbo.Clients_ClientId]
GO

ALTER TABLE [dbo].[ClientCreditCards]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClientCreditCards_dbo.PostalAddresses_Address_Id] FOREIGN KEY([Address_Id])
REFERENCES [dbo].[PostalAddresses] ([Id])
GO

ALTER TABLE [dbo].[ClientCreditCards] CHECK CONSTRAINT [FK_dbo.ClientCreditCards_dbo.PostalAddresses_Address_Id]
GO







Go




CREATE TABLE [dbo].[UserVerifications](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](4) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[IsVerified] [bit] NOT NULL,
	[Token] [nvarchar](max) NULL,
	[ClubDomain] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.UserVerifications] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

INSERT INTO [dbo].[PricePlans] VALUES (0,40 ,0,'' ,0 ,0 ,2)
Go
INSERT INTO [dbo].[PricePlans] VALUES (0,50 ,0,'' ,0 ,1 ,2)
Go
INSERT INTO [dbo].[PricePlans] VALUES (0,60 ,0,'' ,0 ,2 ,2)
Go
INSERT INTO [dbo].[PricePlans] VALUES (0,70 ,0,'' ,0 ,3 ,2)
Go
INSERT INTO [dbo].[PricePlans] VALUES (0,80 ,0,'' ,0 ,4 ,2)
Go
INSERT INTO [dbo].[PricePlans] VALUES (0,90 ,0,'' ,0 ,5 ,2)
Go
INSERT INTO [dbo].[PricePlans] VALUES (0,100 ,0,'' ,0 ,6 ,2)
Go
INSERT INTO [dbo].[PricePlans] VALUES (0,110 ,0,'' ,0 ,7 ,2)
Go
INSERT INTO [dbo].[PricePlans] VALUES (0,40 ,0,'' ,0 ,8,2)
Go
INSERT INTO [dbo].[PricePlans] VALUES (0,120 ,0,'' ,0 ,9 ,2)
Go
INSERT INTO [dbo].[PricePlans] VALUES (0,130 ,0,'' ,0 ,10 ,2)
Go
INSERT INTO [dbo].[PricePlans] VALUES (0,140 ,0,'' ,0 ,11 ,2)

Go


