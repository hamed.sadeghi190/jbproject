﻿using System.Collections.Generic;

namespace Jumbula.Core.Model.Report
{
    public class ReportGridModel
    {
        public ReportGridModel()
        {
            Pageable = new
            {
                pageSizes = new[] { 50, 100, 200},
                buttonCount = 5,
                refresh = true,
                serverPaging = false
            };
            PageSize = 50;
            IsColumnMenu = true;
            IsSortable = true;
            IsResizable = true;
            IsScrollable = true;
        }
        
        public bool IsResizable { get; set; }
        public object Pageable { get; set; }
        public bool IsColumnMenu { get; set; }
        public bool IsSortable { get; set; }
        public bool IsScrollable { get; set; }
        public int PageSize { get; set; }
    }

}
