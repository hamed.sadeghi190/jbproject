﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('seasonSchoolSettingsController', ['$scope', '$http', function ($scope, $http) {

        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/SeasonSchoolSetting", order: 2, menu: "SEASONS" });
    }]);
})();