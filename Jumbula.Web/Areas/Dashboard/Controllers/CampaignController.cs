﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Campaign;
using Jumbula.Core.Model.Club;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.UserService;
using LINQtoCSV;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SendGrid;
using SendGrid.Helpers.Mail;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using SportsClub.Models.Email_Campaign;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class CampaignController : DashboardBaseController
    {
        public virtual ActionResult EmailCampaignSomePeopleStep1()
        {
            return View();
        }

        public virtual ActionResult EmailCampaignUserListStep1()
        {
            return View();
        }

        public virtual ActionResult EmailCampaignStep2()
        {
            return View();
        }

        public virtual ActionResult EmailCampaignStep3()
        {
            return View();
        }
        public virtual ActionResult EmailCampaignStep4()
        {
            return View();
        }
        public virtual ActionResult ReviewSentCampaign()
        {
            return View();
        }

        public virtual ActionResult CreateNewEmailList()
        {
            return View();
        }

        public virtual ActionResult ParticipantEmail()
        {
            return View("ParticipantEmailDesigner");
        }

        [HttpGet]
        public virtual ActionResult CreateCampaign(CampaignReciepientsMode campaignType)
        {
            return RedirectToAction("CampaignAddEditStep1", new { pageMode = PageMode.Create, type = campaignType });
        }

        [HttpGet]
        public virtual ActionResult EditCampaign(int id, CampaignReciepientsMode campaignType)
        {
            return RedirectToAction("CampaignAddEditStep1", "Campaign",
                new { pageMode = PageMode.Edit, campaignId = id, type = campaignType });
        }

        [HttpGet]
        public virtual JsonNetResult CampaignAddEditStep1(PageMode pageMode, int? campaignId, CampaignReciepientsMode type)
        {
            var model = new CampaignViewModelStep1();

            switch (pageMode)
            {
                case PageMode.Create:
                    {
                        //model.CampaignName = club.Name;
                        switch (type)
                        {
                            case CampaignReciepientsMode.SomePeople:
                                {
                                    model.PageMode = pageMode;
                                    model.Mode = CampaignReciepientsMode.SomePeople;
                                    break;
                                }
                            case CampaignReciepientsMode.UserList:
                                {
                                    model.PageMode = pageMode;
                                    model.Mode = CampaignReciepientsMode.UserList;
                                    break;
                                }
                        }

                    }
                    break;
                case PageMode.Edit:
                    {
                        var campaign = Ioc.CampaignBusiness.Get(campaignId.Value);
                        CheckResourceForAccess(campaign);

                        model = campaign.ToViewModel<CampaignViewModelStep1>();
                        model.PageMode = pageMode;
                        model.Id = campaignId.Value;
                        model.Mode = (type == CampaignReciepientsMode.UserList)
                            ? CampaignReciepientsMode.UserList
                            : CampaignReciepientsMode.SomePeople;
                    }
                    break;
            }

            return JsonNet(model, new JsonSerializerSettings());
        }

        public virtual JsonNetResult GetDataForSomePeople()
        {

            var somePeolpe = new DynamicListModel
            {
                IsAllPrograms = true,
                IsAllSeasons = true,
                Gender = GenderCategories.All,
                MinAge = "0",
                MaxAge = "0",
                Graduate = "All"
            };

            var seasons = Ioc.SeasonBusiness.GetList(ActiveClub.Id);
            foreach (var season in seasons)
            {
                somePeolpe.SeasonsList.Add(new SelectKeyValue<string>
                {
                    Text = season.Title,
                    Value = season.Id.ToString()
                });
            }

            var programs =
                Ioc.ProgramBusiness.GetList().Where(p => (p.ClubId == ActiveClub.Id || (p.OutSourceSeasonId.HasValue && p.OutSourceSeason != null && p.OutSourceSeason.ClubId == ActiveClub.Id)) && p.LastCreatedPage == ProgramPageStep.Step4);
            foreach (var program in programs)
            {
                somePeolpe.ProgramsList.Add(new SelectKeyValue<string>
                {
                    Text = program.Name,
                    Value = program.Id.ToString()
                });
            }

            somePeolpe.DateOptionList = DropdownHelpers.ToSelectList<DateConditionStep1>();
            somePeolpe.AgesList = SeedAges(120);

            return JsonNet(somePeolpe);
        }

        public virtual JsonNetResult GetSeasonPrograms(int seasonId, bool isAllPrograms = false)
        {
            var isProvider = (ActiveClub.ClubType != null && ((ActiveClub.ClubType.EnumType == ClubTypesEnum.Provider) || (ActiveClub.ClubType.ParentType != null && ActiveClub.ClubType.ParentType.EnumType == ClubTypesEnum.Provider)));
            var programs = !isAllPrograms
                ? Ioc.ProgramBusiness.GetList()
                    .Where(m => (m.SeasonId == seasonId || m.OutSourceSeasonId == seasonId) && m.LastCreatedPage == ProgramPageStep.Step4)
                    .ToList()
                : Ioc.ProgramBusiness.GetList()
                    .Where(p => (p.ClubId == ActiveClub.Id || (p.OutSourceSeasonId.HasValue && p.OutSourceSeason != null && p.OutSourceSeason.ClubId == ActiveClub.Id)) && p.LastCreatedPage == ProgramPageStep.Step4)
                    .ToList();
            if (isProvider)
            {

                var data = programs.Select(program => new SelectKeyValue<string>
                {
                    Text = program.OutSourceSeason != null && program.OutSourceSeasonId.HasValue ? program.Name + "," + " " + program.Club.Name : program.Name,
                    Value = program.Id.ToString()
                })
       .ToList();
                return JsonNet(data);
            }
            else
            {
                var data = programs.Select(program => new SelectKeyValue<string>
                {
                    Text = program.Name,
                    Value = program.Id.ToString()
                })
         .ToList();
                return JsonNet(data);
            }
        }

        [HttpPost]
        public virtual ActionResult CampaignAddEditStep1(CampaignViewModelStep1 model)
        {
            var campaign = new Campaign();

            var result = new OperationStatus();

            CheckModelValidationStep1SomePeople(model, model.Mode);


            if (ModelState.IsValid)
            {
                var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

                switch (model.PageMode)
                {
                    case PageMode.Create:
                        {
                            campaign.Status = CampaignScheduleType.Draft;
                            campaign.CreateDate = DateTime.UtcNow;
                            campaign.SendDate = null;
                            campaign.LastCreatedPage = EmailCampaignStep.Step1;
                            campaign.Club_Id = club.Id;
                            switch (model.Mode)
                            {
                                case CampaignReciepientsMode.SomePeople:
                                    {
                                        var recipients =
                                            ApplayFilterOnUsers(model.SomePeople, club, model.ListName, null).ToList();

                                        if (recipients.Any())
                                        {
                                            campaign.Recipients = recipients;

                                            campaign.CampaignName = model.CampaignName;
                                            campaign.CampaignType = CampaignReciepientsMode.SomePeople;
                                            result = Ioc.CampaignBusiness.SaveCampaignData(campaign);
                                        }
                                        else
                                        {
                                            ModelState.AddModelError("NoUser",
                                                "Your search criteria doesn't seem to contain any recipient.");
                                            return JsonFormResponse();
                                        }
                                    }
                                    break;
                                case CampaignReciepientsMode.UserList:
                                    {
                                        var list =
                                            Ioc.ClubBusiness.Get(club.Domain).EmailLists.Single(e => e.Id == model.ListId);

                                        switch (list.ListType)
                                        {
                                            case MailListType.Static:
                                                foreach (var item in list.ListContact)
                                                {
                                                    campaign.Recipients.Add(new CampaignRecipients
                                                    {
                                                        EmailAddress = item.EmailAddress,
                                                        RecipientName = item.RecipientName,
                                                        RecipientLastName = item.RecipientLastName,
                                                        IsClicked = false,
                                                        IsOpened = false,
                                                        EmailStatus = EmailStatus.processed
                                                    });
                                                }
                                                break;
                                            case MailListType.Dynamic:
                                                {
                                                    var dynamicModel =
                                                        JsonConvert.DeserializeObject<DynamicListModel>(list.QueryString);

                                                    var recipients = ApplayFilterOnUsers(dynamicModel, club, "", null).ToList();
                                                    if (recipients.Any())
                                                    {
                                                        campaign.Recipients = recipients;
                                                    }
                                                    else
                                                    {
                                                        ModelState.AddModelError("NoUser",
                                                            "Your search criteria doesn't seem to contain any recipient.");
                                                        return JsonFormResponse();
                                                    }
                                                }
                                                break;
                                        }

                                        campaign.CampaignName = string.Format("Campaign base on list {0}", list.ListName);
                                        campaign.CampaignType = CampaignReciepientsMode.UserList;

                                        result = Ioc.CampaignBusiness.SaveCampaignData(campaign);
                                    }
                                    break;
                            }
                        }
                        break;
                    case PageMode.Edit:
                        {
                            campaign = model.ToModel<Campaign>(Ioc.CampaignBusiness.Get(model.Id));
                            var recipients = new List<CampaignRecipients>();

                            switch (model.Mode)
                            {
                                case CampaignReciepientsMode.UserList:
                                    {
                                        break;
                                    }
                                case CampaignReciepientsMode.SomePeople:
                                    {
                                        recipients =
                                            ApplayFilterOnUsers(model.SomePeople, club, model.ListName,
                                                model.ListId).ToList();
                                        if (recipients.Any())
                                        {
                                            //campaign.Recipients = recipients;
                                        }
                                        else
                                        {
                                            ModelState.AddModelError("NoUser",
                                                "Your search criteria doesn't seem to contain any recipient.");
                                            return JsonFormResponse();
                                        }
                                        break;
                                    }
                            }

                            result = Ioc.CampaignBusiness.UpdateCampaign(campaign, recipients);
                        }
                        break;
                }
                return
                    JsonNet(new JResult
                    {
                        Data = campaign.Id.ToString(),
                        Message = result.Message,
                        Status = result.Status,
                        RecordsAffected = result.RecordsAffected
                    });
            }
            return JsonFormResponse();
        }

        [HttpGet]
        public virtual JsonNetResult CampaignAddEditStep2(int campaignId)
        {
            var club = Ioc.ClubBusiness.Get(ActiveClub.Domain);

            var campaign = Ioc.CampaignBusiness.Get(campaignId);

            var model = campaign.ToViewModel<CampaignViewModelStep2>();

            var body = WebUtility.HtmlDecode(model.Body);

            model.Body = body;
          
            var currentUserRole = this.GetCurrentUserRole();
            model.IsUserRestrictedManager = currentUserRole == RoleCategory.RestrictedManager ? true : false;

            model.From = string.Format(Constants.W_JumbulaEmailFormat, club.Domain);

            var clubContact = club.ContactPersons.FirstOrDefault();

            var clubStaffs = Ioc.ClubBusiness.GetAllUserWithSpecificRolesForClubs(ActiveClub.Id,
               new List<RoleCategory> { RoleCategory.Admin, RoleCategory.Manager })
               .Select(c => new SelectKeyValue<int>() { Text = c.UserName, Value = c.Id }).ToList();

            model.ClubAdmins = new List<SelectKeyValue<string>>();

            var clubStaffKeys = clubStaffs.Select(p => p.Value).ToList();

            model.ClubAdmins.Add(new SelectKeyValue<string>() { Text = clubContact.Email, Value = clubContact.Email });

            var listActiveStaff = club.ClubStaffs.Where(s => s.Status == StaffStatus.Active && !s.IsFrozen && clubStaffKeys.Contains(s.JbUserRole.UserId)).Select(s => new SelectKeyValue<string> { Value = s.JbUserRole.User.UserName, Text = s.JbUserRole.User.UserName }).ToList();

            model.ClubAdmins.AddRange(listActiveStaff);
            model.ClubAdmins = model.ClubAdmins.DistinctBy(d => d.Text).ToList();

            model.ListUsersReplyTo = new List<SelectKeyValue<string>>();
            model.ListUsersReplyTo.Add(new SelectKeyValue<string>() { Text = clubContact.Email, Value = clubContact.Email });

            foreach (var item in listActiveStaff)
            {
                model.ListUsersReplyTo.Add(new SelectKeyValue<string>() { Text = item.Text, Value = item.Text });
            }

            model.ListUsersReplyTo = model.ListUsersReplyTo.DistinctBy(d => d.Text).ToList();

            model.ReplyTo = string.IsNullOrEmpty(campaign.ReplyTo)
                ? clubContact.Email
                : campaign.ReplyTo;


            var ccList = campaign.Recipients.Where(r => r.RecipeintType == RecipeientType.CC).ToList();

            if (ccList != null)
            {
                model.SelectedUser = new List<string>();
                var selectedUsers = model.ClubAdmins.Where(e => ccList.Select(c => c.EmailAddress).ToList().Contains(e.Text));

                foreach (var item in selectedUsers)
                {
                    model.SelectedUser.Add(item.Value);

                }

                foreach (var email in ccList)
                {
                    model.Ccs += email.EmailAddress + ",";
                }

            }

            if (!string.IsNullOrEmpty(campaign.Attachments))
            {
                model.Attachments = JsonConvert.DeserializeObject<List<AttachmentModel>>(campaign.Attachments);
                model.EditMode = true;
            }

            var templates =
                Ioc.EmailCampaignTemplate.GetAllTemplates(club.Id).ToList();

            var defaultTemplate = Ioc.EmailCampaignTemplate.GetDefaultClubTemplate();

            if (defaultTemplate != null)
            {
                templates.Add(defaultTemplate);
            }

            var templateNames = (templates.Where(temp => temp != null)
                .Select(temp => new SelectKeyValue<string> { Value = temp.Id.ToString(), Text = temp.TemplateName })).ToList();

            templateNames.Add(new SelectKeyValue<string> { Value = "0", Text = "New Template ..." });

            model.TemplateNames = templateNames;

            if (campaign.EmailTemplate_Id > 0)
            {
                model.SelectedTemplateId = campaign.EmailTemplate_Id.ToString();
            }
            else
            {
                model.SelectedTemplateId = "0";
                model.MailTemplate.Locations.AddRange(club.ClubLocations.Select(location => new ClubAddressViewModel
                {
                    Address = location.PostalAddress.Address,
                    AddressPoint = location.PostalAddress.Lat.ToString() + "," + location.PostalAddress.Lng.ToString()
                }));
            }

            model.EmailStyle = campaign.EmailTemplate_Id != 0 ? EmailStyle.Template : EmailStyle.Text;

            if (model.EmailStyle == EmailStyle.Template)
            {
                var campaignTemplate = Ioc.EmailCampaignTemplate.GetTemplateById(campaign.EmailTemplate_Id);

                model.MailTemplate = JsonConvert.DeserializeObject<EmailTemplateModel>(campaignTemplate.Template);
            }

            model.IsReview = false;

            // add clubLogo address.
            model.MailTemplate.ClubLogo = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);
            //model.MailTemplate.ClubLogo = Ioc.ClubBusiness.GetClubLogo(club);

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult CampaignAddEditStep2(CampaignViewModelStep2 model)
        {
            CheckModelValidationStep2(model);

            if (ModelState.IsValid)
            {
                var campaign = Ioc.CampaignBusiness.Get(model.Id);
                var ccEmails = campaign.Recipients.Where(e => e.RecipeintType == RecipeientType.CC).ToList();

                if (model.SelectedUser != null || model.Ccs != null)
                {
                    //var listCcs = new List<string>();
                    var listSelectetCc = new List<CampaignRecipients>();

                    if (model.SelectedUser != null && model.IsCcListEmailSet)
                    {
                        var selectedUsers = model.ClubAdmins.Where(e => model.SelectedUser.Select(s => s).ToList().Contains(e.Value));

                        foreach (var item in selectedUsers)
                        {
                            listSelectetCc.Add(new CampaignRecipients()
                            {
                                EmailAddress = item.Text,
                                RecipeintType = RecipeientType.CC,
                            });
                        }
                    }
                    else if (model.Ccs != null && !model.IsCcListEmailSet)
                    {
                        string s = model.Ccs;
                        string[] values = s.Split(',');
                        for (int i = 0; i < values.Length; i++) { values[i] = values[i].Trim(); }

                        foreach (var email in values)
                        {
                            if (email != "")
                            {
                                listSelectetCc.Add(new CampaignRecipients()
                                {
                                    EmailAddress = email,
                                    RecipeintType = RecipeientType.CC,
                                });
                            }
                        }
                    }

                    Ioc.CampaignBusiness.AddNewCampaignRecipients(campaign.Id, listSelectetCc);
                }
                else
                {
                    if (ccEmails.Count > 0)
                    {
                        Ioc.CampaignBusiness.DeleteCcRecipients(ccEmails);
                    }
                }

                if (model.EmailStyle == EmailStyle.Template)
                {
                    if (!string.IsNullOrEmpty(model.MailTemplate.Body))
                    {
                        model.Body = model.MailTemplate.Body;
                    }
                }

                campaign.ReplyTo = model.ReplyTo;
                campaign.Subject = model.Subject;
                campaign.Body = model.Body;
                campaign.Sender = model.From;
                campaign.Id = model.Id;
                campaign.EmailTemplate_Id = int.Parse(model.SelectedTemplateId);
                campaign.CampaignName = model.CampaignName;

                if (model.LastCreatedPage < EmailCampaignStep.Step2)
                {
                    model.LastCreatedPage = EmailCampaignStep.Step2;
                }

                campaign.LastCreatedPage = model.LastCreatedPage;

                if (model.Attachments != null && model.Attachments.Count > 0)
                {
                    campaign.Attachments = JsonConvert.SerializeObject(model.Attachments);
                }
                else if (model.Attachments == null || model.Attachments.Count == 0)
                {
                    campaign.Attachments = string.Empty;
                }

                if (model.EmailStyle == EmailStyle.Template)
                {

                    if (model.MailTemplate.ChangeTemplate && string.IsNullOrEmpty(model.MailTemplate.Name))
                    {
                        model.MailTemplate.ChangeTemplate = false;
                        model.MailTemplate.Name = model.MailTemplate.NewName;
                        model.MailTemplate.NewName = string.Empty;

                        // save image to storage
                        var clubDomain = model.MailTemplate.ClubDomain != null ? model.MailTemplate.ClubDomain : campaign.Club.Domain;

                        model.MailTemplate.ClubDomain = campaign.Club.Domain;

                        if (!string.IsNullOrEmpty(model.MailTemplate.HeaderBackground) && !model.MailTemplate.HeaderBackground.Contains("http:") && !model.MailTemplate.HeaderBackground.Contains("https:"))
                        {
                            var headerPath = Ioc.EmailCampaignTemplate.UploadImageTemplate(model.MailTemplate.HeaderBackground, clubDomain, "emailTemplate");
                            model.MailTemplate.HeaderBackground = headerPath;
                        }
                        if (!string.IsNullOrEmpty(model.MailTemplate.FooterBackground) && !model.MailTemplate.FooterBackground.Contains("http:") && !model.MailTemplate.FooterBackground.Contains("https:"))
                        {
                            var headerPath = Ioc.EmailCampaignTemplate.UploadImageTemplate(model.MailTemplate.FooterBackground, clubDomain, "emailTemplate");
                            model.MailTemplate.FooterBackground = headerPath;
                        }


                        var newTemplate = new EmailTemplate
                        {
                            Template = JsonConvert.SerializeObject(model.MailTemplate),
                            TemplateName = model.MailTemplate.Name,
                            Type = TemplateType.Email,
                            Club_Id = ActiveClub.Id,
                        };
                        //campaign.MailTemplate = newTemplate;
                        Ioc.EmailCampaignTemplate.SaveCampaignTemplate(newTemplate);
                        campaign.EmailTemplate_Id = newTemplate.Id;
                    }
                    else if (model.MailTemplate.ChangeTemplate && !string.IsNullOrEmpty(model.MailTemplate.Name))
                    {
                        // save image to storage
                        var clubDomain = model.MailTemplate.ClubDomain != null ? model.MailTemplate.ClubDomain : campaign.Club.Domain;
                        model.MailTemplate.ClubDomain = campaign.Club.Domain;

                        if (!string.IsNullOrEmpty(model.MailTemplate.HeaderBackground) && !model.MailTemplate.HeaderBackground.Contains("http:") && !model.MailTemplate.HeaderBackground.Contains("https:"))
                        {
                            var headerPath = Ioc.EmailCampaignTemplate.UploadImageTemplate(model.MailTemplate.HeaderBackground, clubDomain, "emailTemplate");
                            model.MailTemplate.HeaderBackground = headerPath;
                        }
                        if (!string.IsNullOrEmpty(model.MailTemplate.FooterBackground) && !model.MailTemplate.FooterBackground.Contains("http:") && !model.MailTemplate.FooterBackground.Contains("https:"))
                        {
                            var headerPath = Ioc.EmailCampaignTemplate.UploadImageTemplate(model.MailTemplate.FooterBackground, clubDomain, "emailTemplate");
                            model.MailTemplate.FooterBackground = headerPath;
                        }

                        var newTemp = new EmailTemplate
                        {
                            Template = JsonConvert.SerializeObject(model.MailTemplate),
                            Type = TemplateType.Email,
                            TemplateName = model.MailTemplate.Name,
                            Id = int.Parse(model.SelectedTemplateId)
                        };
                        Ioc.EmailCampaignTemplate.UpdateCampaignTemplate(newTemp);
                    }
                }
                else
                {
                    campaign.EmailTemplate_Id = 0;
                }

                if (campaign.Recipients.Count > 0)
                {
                    var result = Ioc.CampaignBusiness.UpdateCampaign(campaign);
                    return
                        JsonNet(new JResult
                        {
                            Data = campaign.Id.ToString(),
                            Message = result.Message,
                            Status = result.Status,
                            RecordsAffected = result.RecordsAffected
                        });
                }
                else
                {
                    return
                       JsonNet(new JResult
                       {
                           Data = campaign.Id.ToString(),
                           Message = "You do not have recipients email.",
                           Status = false,
                       });
                }

            }
            return JsonFormResponse();
        }

        [HttpGet]
        public virtual JsonNetResult CampaignAddEditStep3(int campaignId)
        {
            var campaign = Ioc.CampaignBusiness.Get(campaignId);

            var model = new CampaignViewModelStep3
            {
                Id = campaignId,
                SendDate = (campaign.SendDate.HasValue) ? campaign.SendDate.Value : DateTime.UtcNow,
                ScheduleType =
                    (campaign.ScheduleDate.HasValue) ? CampaignScheduleType.Scheduled : CampaignScheduleType.Sent
            };

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult CampaignAddEditStep3(CampaignViewModelStep3 model)
        {
            CheckModelValidationStep3(model);

            if (ModelState.IsValid)
            {
                var campaign = Ioc.CampaignBusiness.Get(model.Id);
                campaign.ScheduleDate = null;
                campaign.SendDate = null;

                if (model.ScheduleType == CampaignScheduleType.Scheduled)
                {
                    var scheduleTime = new DateTime(model.SendDate.Value.Year, model.SendDate.Value.Month,
                                      model.SendDate.Value.Day, model.SendTime.Value.Hour, model.SendTime.Value.Minute,
                                      model.SendTime.Value.Second);

                    campaign.ScheduleDate = scheduleTime;
                    campaign.SendDate = scheduleTime;
                }
                else
                {
                    campaign.SendDate = model.SendDate;
                }

                var campaignRecipients = campaign.Recipients;

                if (model.LastCreatedPage < EmailCampaignStep.Step3)
                {
                    model.LastCreatedPage = EmailCampaignStep.Step3;
                }

                campaign.LastCreatedPage = model.LastCreatedPage;

                var result = Ioc.CampaignBusiness.UpdateCampaign(campaign);

                var fileName = ActiveClub.Domain + "UploadedList.csv";

                if (System.IO.File.Exists(Server.MapPath(fileName)))
                {
                    System.IO.File.Delete(Server.MapPath(fileName));
                }

                return
                    JsonNet(new JResult
                    {
                        Data = campaign.Id.ToString(),
                        Message = result.Message,
                        Status = result.Status,
                        RecordsAffected = result.RecordsAffected
                    });
            }

            return JsonFormResponse();
        }
        [HttpGet]
        public virtual JsonNetResult CampaignAddEditStep4(int campaignId)
        {
            var model = new CampaignViewModelStep4();
            var campaign = Ioc.CampaignBusiness.Get(campaignId);
            var Attachments = JsonConvert.DeserializeObject<List<AttachmentModel>>(campaign.Attachments);

            var currentUserRole = this.GetCurrentUserRole();

            model = new CampaignViewModelStep4
            {
                CampaignName = campaign.CampaignName,
                ReplyTo = campaign.ReplyTo,
                Subject = campaign.Subject,
                Body = WebUtility.HtmlDecode(campaign.Body),
                ScheduleType = campaign.ScheduleDate.HasValue ? CampaignScheduleType.Scheduled : CampaignScheduleType.Sent,
                ScheduleMethod = campaign.ScheduleDate.HasValue ? "Scheduled" : "Send now",
                From = campaign.Sender,
                IsReview = true,
                LastCreatedPage = EmailCampaignStep.Step4,
                SendDate = campaign.ScheduleDate,
                Id = campaignId,
                Attachments = Attachments,
                IsUserRestrictedManager = currentUserRole == RoleCategory.RestrictedManager ? true : false
            };



            return JsonNet(model);
        }
        [HttpPost]
        public virtual ActionResult CampaignAddEditStep4(CampaignViewModelStep4 model)
        {
            var campaign = Ioc.CampaignBusiness.Get(model.Id);
            var campaignRecipients = campaign.Recipients;
            var club = Ioc.ClubBusiness.Get(ActiveClub.Domain);
            if (campaignRecipients.Count == 0)
            {
                return JsonNet(new JResult { Data = campaign.Id.ToString(), Message = "Your campaign email list is empty.", Status = false });
            }

            EmailTemplateModel template = null;
            var sentCampaign = false;
            var message = "";
            dynamic code;

            if (campaign.EmailTemplate_Id > 0)
            {

                var campaignTemplate = Ioc.EmailCampaignTemplate.GetTemplateById(campaign.EmailTemplate_Id);
                CheckResourceForAccess(campaignTemplate);

                var templateDeserialize =
                    JsonConvert.DeserializeObject<EmailTemplateModel>(campaignTemplate.Template);

                template = new EmailTemplateModel
                {
                    BodyBackColor = templateDeserialize.BodyBackColor == null ? "#fff" : templateDeserialize.BodyBackColor,

                    HasLogo = templateDeserialize.HasLogo,
                    ClubLogo = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo, true),

                    HasHeaderText = templateDeserialize.HasHeaderText,
                    HeaderBackground = templateDeserialize.HeaderBackground,
                    HeaderText = templateDeserialize.HeaderText,

                    MapLocation = templateDeserialize.MapLocation,
                    MapZoom = templateDeserialize.MapZoom,

                    HasFooterText = templateDeserialize.HasFooterText,
                    FooterBackground = templateDeserialize.FooterBackground,
                    FooterText = templateDeserialize.FooterText,

                    HasMap = templateDeserialize.HasMap,

                    HasFooter = templateDeserialize.HasFooter,
                    HasHeader = templateDeserialize.HasHeader,

                    IsRegisterationEmail = false,

                    ClubDomain = templateDeserialize.ClubDomain,
                };
            }
            else
            {
                template = new EmailTemplateModel
                {
                    IsRegisterationEmail = false,
                    ClubDomain = campaign.Club.Domain
                };
            }

            if (!string.IsNullOrEmpty(template.HeaderBackground) && !template.HeaderBackground.Contains("http:") && !template.HeaderBackground.Contains("https:"))
            {
                template.HeaderBackground = GetImageURL(template.HeaderBackground, template.ClubDomain, "emailTemplate");
            }
            if (!string.IsNullOrEmpty(template.FooterBackground) && !template.FooterBackground.Contains("http:") && !template.FooterBackground.Contains("https:"))
            {
                template.FooterBackground = GetImageURL(template.FooterBackground, template.ClubDomain, "emailTemplate");
            }

            const int limit = 1000;
            if (campaignRecipients.Count > limit)
            {
                var tmp = campaignRecipients.Count / (limit + 0.0);

                tmp += 0.4;

                var recipientgroupNum = (int)Math.Round(tmp, MidpointRounding.AwayFromZero);
                // for skip recipients.
                int skip = 0;
                // for take new gruop of recipients.
                int take = 0;
                // total of recipients.
                int total = campaignRecipients.Count;
                // 
                int currentGroup = 1;

                // for last group.
                for (int i = 0; i < recipientgroupNum; i++)
                {
                    skip = (currentGroup - 1) * limit;

                    if ((skip + limit) > total)
                    {
                        take = total - skip;
                    }
                    else
                    {
                        take = limit;
                    }

                    var someRecipient = campaignRecipients.Skip(skip).Take(take);

                    var res = SendMail(campaign, template, someRecipient, model).Result;
                    campaign.Response = JsonConvert.SerializeObject(res);

                    if (res.StatusCode == HttpStatusCode.Accepted)
                    {
                        sentCampaign = true;
                    }
                    else
                    {
                        switch (res.StatusCode)
                        {
                            case HttpStatusCode.Unauthorized:
                                {
                                    message = "You do not have authorization to make the request.";
                                    break;
                                }
                            case HttpStatusCode.NotFound:
                                {
                                    message = "The resource you tried to locate could not be found or does not exist.";
                                    break;
                                }
                            case HttpStatusCode.InternalServerError:
                                {
                                    message = "An error occurred on a SendGrid server.";
                                    break;
                                }
                            case HttpStatusCode.ServiceUnavailable:
                                {
                                    message = "The SendGrid v3 Web API is not available.";
                                    break;
                                }
                            case HttpStatusCode.OK:
                                {
                                    message = "Your message is valid, but it is not queued to be delivered.";
                                    break;
                                }
                        }
                    }
                    currentGroup++;
                }
            }
            else
            {
                var res = SendMail(campaign, template, campaignRecipients, model).Result;
                campaign.Response = JsonConvert.SerializeObject(res);

                if (res.StatusCode == HttpStatusCode.Accepted)
                {
                    sentCampaign = true;
                }
                else
                {
                    switch (res.StatusCode)
                    {
                        case HttpStatusCode.Unauthorized:
                            {
                                message = "You do not have authorization to make the request.";
                                break;
                            }
                        case HttpStatusCode.NotFound:
                            {
                                message = "The resource you tried to locate could not be found or does not exist.";
                                break;
                            }
                        case HttpStatusCode.InternalServerError:
                            {
                                message = "An error occurred on a SendGrid server.";
                                break;
                            }
                        case HttpStatusCode.ServiceUnavailable:
                            {
                                message = "The SendGrid v3 Web API is not available.";
                                break;
                            }
                        case HttpStatusCode.OK:
                            {
                                message = "Your message is valid, but it is not queued to be delivered.";
                                break;
                            }
                    }
                }
            }
            if (sentCampaign)
            {
                if (model.LastCreatedPage < EmailCampaignStep.Step4)
                {
                    model.LastCreatedPage = EmailCampaignStep.Step4;
                }
                campaign.LastCreatedPage = model.LastCreatedPage;
                var result = Ioc.CampaignBusiness.UpdateCampaign(campaign);
                return
                         JsonNet(new JResult
                         {

                             Data = campaign.Id.ToString(),
                             Message = result.Message,
                             Status = true,
                             RecordsAffected = result.RecordsAffected
                         });
            }
            else
            {
                campaign.LastCreatedPage = EmailCampaignStep.Step3;
                campaign.Status = CampaignScheduleType.Draft;
                dynamic data = JObject.Parse(campaign.Response);
                code = data.StatusCode.Value;

                var result = Ioc.CampaignBusiness.UpdateCampaign(campaign);
                message = message != string.Empty ? message : "SendGrid has encountered the error code:" + code + " and did not send your campaign. Please contact Jumbula Support and report this issue.";
                return JsonNet(new JResult { Data = campaign.Id.ToString(), Message = message, Status = false });
            }
        }
        public virtual ActionResult GetSentCampaign(int campaignId)
        {
            var campaign = Ioc.CampaignBusiness.Get(campaignId);
            CheckResourceForAccess(campaign);

            var campaignViewModel = campaign.ToViewModel<CampaignViewModelStep2>();
            campaignViewModel.EmailStyle = (campaign.EmailTemplate_Id > 0) ? EmailStyle.Template : EmailStyle.Text;
            campaignViewModel.IsReview = true;
            campaignViewModel.Body = WebUtility.HtmlDecode(campaign.Body);

            campaignViewModel.From = string.Format(Constants.W_JumbulaEmailFormat, campaign.Club.Domain);

            if (campaign.EmailTemplate_Id > 0)
            {
                campaignViewModel.SelectedTemplateId = campaign.EmailTemplate_Id.ToString();

                var templateDb = Ioc.EmailCampaignTemplate.GetTemplateById(campaign.EmailTemplate_Id);
                campaignViewModel.MailTemplate = JsonConvert.DeserializeObject<EmailTemplateModel>(templateDb.Template);
            }

            return JsonNet(campaignViewModel);
        }

        public virtual ActionResult GetNewRecipientModel()
        {
            return JsonNet(new NewRecipient());
        }

        public virtual ActionResult AddNewRecipientDirect(int campaignId, NewRecipient model)
        {
            if (ModelState.IsValid)
            {
                var result = Ioc.CampaignBusiness.AddNewCampaignRecipient(campaignId, model.Email, model.FirstName,
                    model.LastName, RecipeientType.To);

                return JsonNet(new JResult { Status = result.Status, Message = result.Message });
            }
            return JsonFormResponse();
        }

        [HttpPost]
        public virtual ActionResult SaveUploadedList(string listName)
        {
            var club = ActiveClub;
            var fileName = club.Domain + "UploadedList.csv";
            StreamReader csvreader = null;
            var recipients = new List<UploadedListModel>();

            var storageManager = new StorageManager(WebConfigHelper.AzureStorageConnection,
                Constants.Path_ClubFilesContainer);
            var sourcePath = StorageManager.PathCombine(club.Domain, fileName);
            var targetPath = StorageManager.PathCombine(club.Domain, fileName);

            if (
                StorageManager.IsExist(
                    StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain,
                        fileName).AbsoluteUri))
            {
                //csvreader =
                //    new StreamReader(new StreamInput())
                var blobReader = new WebClient();
                var uploadedFile = blobReader.DownloadString(
                    StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, fileName).AbsoluteUri);

                csvreader = new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(uploadedFile)));
            }
            else
            {
                ModelState.AddModelError("model.NoFileUploaded", "Please select a file to create list base on.");
            }

            if (string.IsNullOrEmpty(listName))
            {
                ModelState.AddModelError("model.ListName", "Please enter a name for uploaded list.");
            }

            if (ModelState.IsValid)
            {

                var csvFile = new CsvContext();
                if (!csvFile.IsInCorrectFormat<UploadedListModel>(csvreader))
                {
                    csvreader.Close();
                    csvreader.Dispose();

                    ModelState.AddModelError("model.FileNotInFormat",
                        "Your file must have the correct syntax. Please download the sample file and use it as a reference.");
                    if (
                        StorageManager.IsExist(
                            StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, fileName).AbsoluteUri))
                    {
                        storageManager.ReadFile(sourcePath, targetPath);
                        storageManager.DeleteBlob(club.Domain, fileName);
                    }
                    return JsonFormResponse();
                }
                else
                {
                    recipients = csvFile.Read<UploadedListModel>(csvreader).ToList();
                    if (recipients.Any(i => i.Email == null))
                    {
                        ModelState.AddModelError("FileContentError",
                            "Your file must have the correct syntax. Please download the sample file and use it as a reference.");
                        if (
                            StorageManager.IsExist(
                                StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, fileName)
                                    .AbsoluteUri))
                        {
                            storageManager.ReadFile(sourcePath, targetPath);
                            storageManager.DeleteBlob(club.Domain, fileName);
                        }
                        return JsonFormResponse();
                    }
                    csvreader.Close();
                    csvreader.Dispose();
                }

                var listContacts = new List<MailListContact>();
                var incorrectEmails = new List<string>();

                foreach (var item in recipients)
                {
                    var email = item.Email.Trim();

                    if (Regex.IsMatch(email,
                        "^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$"))
                    {
                        listContacts.Add(new MailListContact
                        {
                            EmailAddress = email,
                            RecipientLastName = item.LastName,
                            RecipientName = item.FirstName
                        });
                    }
                    else
                    {
                        incorrectEmails.Add(item.Email);
                    }
                }

                if (incorrectEmails.Count > 0)
                {
                    var emailes = string.Join(", ", incorrectEmails);

                    ModelState.AddModelError("FileContentError",
                           "Your file must be in correct syntax, Please check below emails in uploaded file:</br>" + emailes);
                    if (
                        StorageManager.IsExist(
                            StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, fileName)
                                .AbsoluteUri))
                    {
                        storageManager.ReadFile(sourcePath, targetPath);
                        storageManager.DeleteBlob(club.Domain, fileName);
                    }
                    return JsonFormResponse();
                }

                var list = new MailList
                {
                    CreateDate = DateTime.UtcNow,
                    ListName = listName,
                    ListContact = listContacts,
                    ListType = MailListType.Static,
                    QueryString = null,
                    Club_Id = club.Id
                };

                var result = Ioc.MailListBusiness.CreateList(list);

                return JsonNet(new JResult { Status = result.Status });
            }
            return JsonFormResponse();
        }

        public virtual JsonNetResult GetAllCampaigns(List<GridSort> sort = null)
        {
            var termValue = Request.Params["term"] ?? string.Empty;

            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);

            var club = ActiveClub;

            var allCampaigns = Ioc.CampaignBusiness.GetAllCampaigns(club.Id);

            if (!string.IsNullOrEmpty(termValue))
            {
                allCampaigns = allCampaigns
                    .Where(
                        c =>
                            c.CampaignName.ToLower().StartsWith(termValue) ||
                            c.CampaignName.ToLower().EndsWith(termValue));
            }

            var data = allCampaigns.OrderByDescending(c => c.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            var today = DateTime.UtcNow.AddMinutes(20);
            var dateTime = Ioc.ClubBusiness.GetClubDateTime(ActiveClub.Id, today);
            var finalData = data.Select(campaign => new CampaignDisplayViewModel
            {
                CampaignName = campaign.CampaignName,
                CampaignType = campaign.CampaignType,
                CreateDate =
                   DateTimeHelper.ConvertUtcDateTimeToLocal(campaign.CreateDate, club.TimeZone),
                SendDate =
                    (campaign.ScheduleDate.HasValue)
                        ? campaign.ScheduleDate.Value
                        : (campaign.SendDate.HasValue)
                            ? DateTimeHelper.ConvertUtcDateTimeToLocal(
                                campaign.SendDate.Value, club.TimeZone)
                            : (DateTime?)null,
                Status = campaign.Status,
                Id = campaign.Id,
                LastCreatedPage = campaign.LastCreatedPage,
                HaveTimeForCancel = (campaign.ScheduleDate >= dateTime) ? true : false
            }).ToList();

            #region sort code
            //if (sort != null)
            //{
            //    foreach (var s in sort)
            //    {
            //        switch (s.field)
            //        {
            //            case "CampaignName":
            //                {
            //                    filteredData = (s.dir == "asc") ? filteredData.OrderBy(o => o.CampaignName) : filteredData.OrderByDescending(o => o.CampaignName).ToList();
            //                    break;
            //                }
            //            case "Status":
            //                {
            //                    filteredData = (s.dir == "asc") ? filteredData.OrderBy(o => o.Status) : filteredData.OrderByDescending(o => o.Status);
            //                    break;
            //                }
            //            default:
            //                break;
            //        }
            //    }
            //}
            #endregion

            var total = allCampaigns.Count();

            var dataSource = finalData;

            return JsonNet(new { DataSource = dataSource, TotalCount = total });
        }

        public virtual JsonNetResult GetAllClubMailList(List<GridSort> sort = null)
        {
            var club = ActiveClub;

            var termValue = Request.Params["term"] ?? string.Empty;

            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);

            var allLists = Ioc.MailListBusiness.GetAllLists(club.Id);

            if (!string.IsNullOrEmpty(termValue))
            {
                allLists = allLists
                    .Where(
                        c =>
                            c.ListName.ToLower().StartsWith(termValue.ToLower()) ||
                            c.ListName.ToLower().EndsWith(termValue.ToLower()));
            }

            var filteredData =
                allLists.Skip((page - 1) * pageSize).Take(pageSize).ToList().Select(m => new MailListsViewModel
                {
                    CreateDate = DateTimeHelper.ConvertUtcDateTimeToLocal(m.CreateDate, club.TimeZone),
                    Id = m.Id,
                    ListName = m.ListName,
                    ListType = m.ListType,
                });

            var total = allLists.Count();
            var dataSource = filteredData.ToList();

            return JsonNet(new { DataSource = dataSource, TotalCount = total });
        }

        public virtual JsonNetResult GetCampaign(int id)
        {
            var campaignViewModel = Ioc.CampaignBusiness.Get(id).ToViewModel<CampaignDisplayViewModel>();
            var campaignRates = Ioc.CampaignBusiness.GetCampaignRates(id);

            campaignViewModel.OpenedNum = campaignRates.OpenedNum;
            campaignViewModel.OpenedRate = campaignRates.OpenedRate;

            campaignViewModel.ClickedNum = campaignRates.ClickedNum;
            campaignViewModel.ClickedRate = campaignRates.ClickedRate;

            campaignViewModel.DeliveredNum = campaignRates.DeliveredNum;
            campaignViewModel.DeliveredRate = campaignRates.DeliveredRate;

            campaignViewModel.LastOpenedDate = (campaignRates.LastOpened.HasValue)
                ? campaignRates.LastOpened.Value.ToString(Constants.DefaultDateTimeFormat)
                : string.Empty;

            campaignViewModel.LastClickedDate = (campaignRates.LastClicked.HasValue)
                ? campaignRates.LastClicked.Value.ToString(Constants.DefaultDateTimeFormat)
                : string.Empty;

            //campaign.OpenedRate = Ioc.CampaignBusiness.GetCampaignOpenRate(id);
            //campaign.ClickedRate = Ioc.CampaignBusiness.GetCampaignClickRate(id);

            return JsonNet(campaignViewModel);
        }
        [JbAuthorize(JbAction.CampaignRecipients_View)]
        public virtual JsonNetResult GetAllRecipients(int campaignId, List<GridSort> sort)
        {

            var termValue = Request.Params["term"] ?? string.Empty;
            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);

            var members = Ioc.CampaignBusiness.Get(campaignId).Recipients;

            //IEnumerable<ClubMembers> filteredMembers = null;

            var allClubMembers = members.Select(member => new ClubMembers
            {
                Email = member.EmailAddress,
                Id = member.Id,
                Name = string.Format("{0} {1}", member.RecipientName, member.RecipientLastName)
            }).ToList();


            allClubMembers =
                allClubMembers.Where(
                    m =>
                        m.Email.ToLower().StartsWith(termValue.ToLower()) ||
                        m.Email.ToLower().EndsWith(termValue.ToLower())).ToList();

            var dataSource =
                allClubMembers.OrderByDescending(u => u.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList();

            var total = allClubMembers.Count;

            return JsonNet(new { DataSource = dataSource, TotalCount = total });
        }

        public virtual JsonNetResult GetTemplatesNameList(List<GridSort> sort = null)
        {
            var termValue = Request.Params["term"] ?? string.Empty;

            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);

            //var clubDomain = ActiveClub.Domain;
            var templates = Ioc.EmailCampaignTemplate.GetAllTemplates(ActiveClub.Id).ToList();

            templates.Remove(templates.Find(t => t.TemplateName == "Default"));


            var filteredData = templates
                .Where(
                    c =>
                        !string.IsNullOrEmpty(c.TemplateName) &&
                        (c.TemplateName.ToLower().StartsWith(termValue.ToLower()) ||
                        c.TemplateName.ToLower().EndsWith(termValue.ToLower())))
                .Select(m => m);

            if (sort != null)
            {
                foreach (var s in sort)
                {
                    switch (s.field)
                    {
                        case "TemplateName":
                            {
                                filteredData = (s.dir == "asc")
                                    ? filteredData.OrderBy(o => o.TemplateName)
                                    : filteredData.OrderByDescending(o => o.TemplateName);
                                break;
                            }
                    }
                }
            }

            var total = filteredData.Count();

            var dataSource =
                filteredData.Skip((page - 1) * pageSize)
                    .Take(pageSize)
                    .Select(m => m.ToViewModel<CampaignTemplateNamesViewModel>())
                    .ToList();

            return JsonNet(new { DataSource = dataSource, TotalCount = total }, new JsonSerializerSettings());
        }

        public virtual JsonNetResult GetTemplate(int? id)
        {
            var club = Ioc.ClubBusiness.Get(ActiveClub.Domain);

            EmailTemplateModel model;
            EmailTemplate template;

            if (id.HasValue && id.Value > 0)
            {
                template = Ioc.EmailCampaignTemplate.GetTemplateById(id.Value);
                CheckResourceForAccess(template);

                model = JsonConvert.DeserializeObject<EmailTemplateModel>(template.Template);

                // add clubLogo address.
                model.ClubLogo = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo, true);

                //Get image from storage
                if (!string.IsNullOrEmpty(model.HeaderBackground) && !model.HeaderBackground.Contains("http:") && !model.HeaderBackground.Contains("https:"))
                {
                    model.HeaderBackground = GetImageURL(model.HeaderBackground, model.ClubDomain, "emailTemplate");
                }
                if (!string.IsNullOrEmpty(model.FooterBackground) && !model.FooterBackground.Contains("http:") && !model.FooterBackground.Contains("https:"))
                {
                    model.FooterBackground = GetImageURL(model.FooterBackground, model.ClubDomain, "emailTemplate");
                }


                if (model.HasHeaderText && string.IsNullOrEmpty(model.HeaderText))
                {
                    // add clubName as default for Header Text.
                    model.HeaderText = club.Name;
                }
                model.Locations.Clear();
                foreach (var location in club.ClubLocations)
                {
                    model.Locations.Add(new ClubAddressViewModel
                    {
                        Address = location.PostalAddress.Address,
                        AddressPoint =
                            location.PostalAddress.Lat.ToString() + "," + location.PostalAddress.Lng.ToString()
                    });
                }

                return JsonNet(model);
            }
            else
            {
                template = Ioc.EmailCampaignTemplate.GetDefaultClubTemplate();

                model = template != null
                    ? JsonConvert.DeserializeObject<EmailTemplateModel>(template.Template)
                    : new EmailTemplateModel();

                // add clubLogo address.
                model.ClubLogo = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo, true);

                if (model.HasHeaderText && !string.IsNullOrEmpty(model.HeaderText))
                {
                    // add clubName as default for Header Text.
                    model.HeaderText = club.Name;
                }

                model.Name = "";

                foreach (var location in club.ClubLocations)
                {
                    model.Locations.Add(new ClubAddressViewModel
                    {
                        Address = location.PostalAddress.Address,
                        AddressPoint =
                            location.PostalAddress.Lat.ToString() + "," + location.PostalAddress.Lng.ToString()
                    });
                }
                return JsonNet(model);
            }
        }

        [HttpPost]
        public virtual JsonNetResult UploadAttachments(IEnumerable<HttpPostedFileBase> attachment)
        {
            // async ;)
            var clubDomain = ActiveClub.Domain;
            this.EnsureUserIsAdminForClub(clubDomain);
            AttachmentModel attach;
            var file = attachment.First();
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
                var contentType = string.Format(Constants.F_AttachmentMemeType,
                    Path.GetExtension(file.FileName).Substring(1));
                var fileData = binaryReader.ReadBytes(file.ContentLength);

                var fileSize = fileData.Count() / 1024;

                Stream fileStream = new MemoryStream(fileData);

                sm.UploadBlob(clubDomain, file.FileName, fileStream, contentType);

                var url =
                    StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain, file.FileName).AbsoluteUri;

                attach = new AttachmentModel
                {
                    FileName = file.FileName,
                    FileUrl = url,
                    FileSize = fileSize
                };

                fileStream.Close();
            }

            return JsonNet(new { data = attach, action = "Add" });
        }

        public virtual JsonNetResult DeleteUploadedFile(string[] fileNames)
        {
            var fileName = fileNames[0];
            var clubDomain = ActiveClub.Domain;
            var sourcePath = StorageManager.PathCombine(clubDomain, fileName);
            var storageManager = new StorageManager(WebConfigHelper.AzureStorageConnection,
                Constants.Path_ClubFilesContainer);
            var targetPath = StorageManager.PathCombine(clubDomain, fileName);
            if (
                StorageManager.IsExist(
                    StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain,
                        fileName).AbsoluteUri))
            {
                storageManager.ReadFile(sourcePath, targetPath);
                storageManager.DeleteBlob(clubDomain, fileName);
            }

            var file = StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain, fileName).AbsoluteUri;
            return JsonNet(new { data = file, action = "Remove" });
        }

        [HttpPost]
        public virtual JsonNetResult UploadedList(HttpPostedFileBase uploadedList)
        {
            var clubDomain = ActiveClub.Domain;
            this.EnsureUserIsAdminForClub(clubDomain);

            var file = uploadedList;
            var fileName = clubDomain + "UploadedList.csv";
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
                var contentType = string.Format(Constants.F_AttachmentMemeType,
                    Path.GetExtension(file.FileName).Substring(1));
                var fileData = binaryReader.ReadBytes(file.ContentLength);

                Stream fileStream = new MemoryStream(fileData);

                sm.UploadBlob(clubDomain, fileName, fileStream, contentType);

                fileStream.Close();

                return JsonNet(new { Data = "Add", uploadedList = fileName, Status = true });
            }

        }

        public virtual JsonNetResult DeleteUploadedList(string[] fileNames)
        {
            var clubDomain = ActiveClub.Domain;
            var fileName = clubDomain + "UploadedList.csv";
            var sourcePath = StorageManager.PathCombine(clubDomain, fileName);
            var storageManager = new StorageManager(WebConfigHelper.AzureStorageConnection,
                Constants.Path_ClubFilesContainer);
            var targetPath = StorageManager.PathCombine(clubDomain, fileName);
            if (
                StorageManager.IsExist(
                    StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain,
                        fileName).AbsoluteUri))
            {
                storageManager.ReadFile(sourcePath, targetPath);
                storageManager.DeleteBlob(clubDomain, fileName);
            }

            //var file = StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain, fileName).AbsoluteUri;

            return JsonNet(new JResult { Data = "Remove", Status = true });
        }

        [HttpPost]
        public virtual JsonResult SaveUpdateTemplate(EmailTemplateModel model, int? templateId)
        {
            var clubDomain = ActiveClub.Domain;
            model.ClubDomain = clubDomain;

            if (!string.IsNullOrEmpty(model.HeaderBackground) && !model.HeaderBackground.Contains("http:") && !model.HeaderBackground.Contains("https:"))
            {
                var headerPath = Ioc.EmailCampaignTemplate.UploadImageTemplate(model.HeaderBackground, clubDomain, "emailTemplate");
                model.HeaderBackground = headerPath;
            }
            if (!string.IsNullOrEmpty(model.FooterBackground) && !model.FooterBackground.Contains("http:") && !model.FooterBackground.Contains("https:"))
            {
                var headerPath = Ioc.EmailCampaignTemplate.UploadImageTemplate(model.FooterBackground, clubDomain, "emailTemplate");
                model.FooterBackground = headerPath;
            }

            var newTemp = new EmailTemplate
            {
                Template = JsonConvert.SerializeObject(model),
                TemplateName = model.Name,
                Type = TemplateType.Email,
                Club_Id = ActiveClub.Id
            };

            if (templateId.HasValue && templateId.Value > 0)
            {
                newTemp.Id = templateId.Value;
                var result = Ioc.EmailCampaignTemplate.UpdateCampaignTemplate(newTemp);
                return Json(new JResult { Status = result.Status, Data = "TemplateList" });
            }
            else
            {
                var result = Ioc.EmailCampaignTemplate.SaveCampaignTemplate(newTemp);
                return Json(new JResult { Status = result.Status, Data = "TemplateList" });
            }
        }

        public virtual JsonResult DeleteCampaign(int campaignId)
        {
            var result = Ioc.CampaignBusiness.DeleteCampaign(campaignId);
            return Json(new JResult { Status = result.Status });
        }

        public virtual JsonResult CancelScheduledCampaign(int campaignId)
        {
            var result = false;
            var errorMessage = "";
            try
            {

                var campaign = Ioc.CampaignBusiness.Get(campaignId);
                var batchId = campaign.BatchId;
                var today = DateTime.UtcNow.AddMinutes(20);
                var dateTime = Ioc.ClubBusiness.GetClubDateTime(ActiveClub.Id, today);

                if (campaign.ScheduleDate > dateTime)
                {
                    var res = CancelCampaignWithbatchId(batchId);
                    var sendgridResult = new CancelResult();

                    sendgridResult = JsonConvert.DeserializeObject<CancelResult>(res);

                    if (sendgridResult.Status == "cancel")
                    {
                        result = true;
                        campaign.Status = CampaignScheduleType.Cancel;
                        campaign.SendDate = null;
                        campaign.ScheduleDate = null;
                        Ioc.CampaignBusiness.UpdateCampaign(campaign);
                    }
                    else
                    {
                        var message = sendgridResult.Errors.First().Message;
                        errorMessage = "We cannot cancel your campaign at this time, please try again within 12 hours.";
                        Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(message));
                        EmailService.Get(this.ControllerContext).SendEmailForHandelErrorFromSendgrid(campaign, message);

                    }
                }
                else
                {
                    errorMessage = "you cannot cancel your campaign";
                }

            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }

            return Json(new JResult { Status = result, Message = errorMessage });
        }
        [JbAuthorize(JbAction.CampaignRecipients_View)]
        public virtual JsonNetResult GetCampaignReport(int campaignId, string type)
        {
            //var campRecipients = Ioc.CampaignBusiness.GetCampaignRecipients(campaignId);
            //var startDate = campRecipients.Min(c => c.OpenDate.Value).AddDays(-1);
            //var fakeStartDate = startDate.ToString("yyyy-MM-dd");
            //var endDate = campRecipients.Max(c => c.OpenDate.Value).ToString("yyyy-MM-dd");

            //// Curl
            //var httpWebRequest =
            //    (HttpWebRequest)
            //        WebRequest.Create("https://api.sendgrid.com/v3/categories/stats?start_date=" + fakeStartDate + "&end_date=" +
            //                          endDate + "&categories=learnerschess");
            //httpWebRequest.ContentType = "application/json";
            //httpWebRequest.Accept = "*/*";
            //httpWebRequest.Method = "GET";
            //httpWebRequest.Headers.Add("Authorization", "Bearer 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");

            //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            ////List<SendgridStatistic> data;
            //using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            //{
            //    var dataLists = JArray.Parse(streamReader.ReadToEnd());
            //    //data = JsonConvert.DeserializeObject<List<SendgridStatistic>>(dataLists.ToString());
            //    return JsonNet(dataLists);
            //}

            var campaign = Ioc.CampaignBusiness.Get(campaignId);
            CheckResourceForAccess(campaign);

            switch (type)
            {
                case "Line":
                    {
                        var result = Ioc.CampaignBusiness.GetOpenedClickedRateInRang(campaignId);
                        return JsonNet(result);
                    }
                //case "Pie":
                //    {
                //        var result = Ioc.CampaignBusiness.GetCampaignRates(campaignId);

                //        return JsonNet(result);
                //    }
                default:
                    {
                        throw new NotImplementedException();
                    }
            }
        }
        [JbAuthorize(JbAction.CampaignRecipients_View)]
        public virtual JsonNetResult GetRecipientReport(int campaignId)
        {

            var termValue = Request.Params["term"] ?? string.Empty;

            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);

            var allRecipients = Ioc.CampaignBusiness.GetCampaignRecipients(campaignId);

            if (!string.IsNullOrEmpty(termValue) &&
                Regex.IsMatch(termValue,
                    "^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$"))
            {
                allRecipients =
                    allRecipients.Where(
                        r =>
                            r.EmailAddress.ToLower().StartsWith(termValue.ToLower()) ||
                            r.EmailAddress.ToLower().EndsWith(termValue.ToLower())).Select(r => r);
            }

            var recipients = allRecipients.OrderBy(c => c.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList();

            var recipientList = new List<RecipientReport>();

            recipientList.AddRange(recipients.Select(r => new RecipientReport
            {
                EmailAddress = r.EmailAddress,
                IsClicked = (r.IsClicked) ? "Yes" : "No",
                IsOpened = (r.IsOpened || r.IsClicked) ? "Yes" : "No",
                IsDeliveryed =
                    (r.EmailStatus == EmailStatus.delivered || r.EmailStatus == EmailStatus.open ||
                     r.EmailStatus == EmailStatus.click)
                        ? "Yes"
                        : "No"
            }));

            var total = allRecipients.Count();
            var dataSource = recipientList;

            return JsonNet(new { DataSource = dataSource, TotalCount = total });
        }

        private async Task<Response> SendMail(Campaign campaign, EmailTemplateModel template,
            IEnumerable<CampaignRecipients> campaignRecipients, CampaignViewModelStep4 model)
        {

            // To
            var toList = new List<string>();
            var replaceNames = new List<string>();
            var recipientEmails = new List<EmailAddress>();
            var ccRrecipientEmails = new List<EmailAddress>();

            foreach (var recipient in campaignRecipients)
            {
                var recipientName = string.Format("{0} {1}", recipient.RecipientName, recipient.RecipientLastName);
                toList.Add(recipient.EmailAddress);
                replaceNames.Add(recipientName);

                var recipientEmail = new EmailAddress();
                recipientEmail.Email = recipient.EmailAddress;

                if (recipient.RecipeintType == RecipeientType.To)
                {
                    recipientEmails.Add(recipientEmail);
                }
                else
                {
                    ccRrecipientEmails.Add(recipientEmail);
                }

            }

            var strFrom = string.Format(Constants.W_JumbulaEmailFormat, campaign.Club.Domain);
            var replay = new EmailAddress(campaign.ReplyTo, campaign.Club.Name);
            // From
            var From = new EmailAddress(strFrom, campaign.Club.Name);

            var filters = new Dictionary<string, dynamic>
            {
                {
                    "clicktrack", new Dictionary<string, dynamic>
                    {
                        {
                            "settings", new Dictionary<string, dynamic>
                            {
                                {
                                    "enable", 1
                                }
                            }
                        }
                    }
                },
                {
                    "opentrack", new Dictionary<string, dynamic>
                    {
                        {
                            "settings", new Dictionary<string, dynamic>
                            {
                                {
                                    "enable", 1
                                }
                            }
                        }
                    }
                },
            };

            var body = campaign.Body;

            if (!string.IsNullOrEmpty(campaign.Attachments))
            {
                var attachments = JsonConvert.DeserializeObject<List<AttachmentModel>>(campaign.Attachments);

                body += HttpUtility.HtmlEncode("<br />");

                body = attachments.Aggregate(body,
                    (current, item) =>
                        current +
                        HttpUtility.HtmlEncode(string.Format(
                            "<a href=\"{0}\" target=\"_blank\">{1} ({2}Kb)</a> <br />", item.FileUrl,
                            item.FileName, item.FileSize)));
            }

            template.Body = body;

            var htmlContent = HttpUtility.HtmlDecode(this.RenderViewToString(Constants.Path_EmailTemplate, template));

            var msg = MailHelper.CreateSingleEmailToMultipleRecipients(From, recipientEmails, campaign.Subject, body, htmlContent);

            msg.ReplyTo = replay;


            var i = 0;

            if (campaign.CampaignType == CampaignReciepientsMode.ClassConfirmation)
            {
                foreach (var recipient in campaignRecipients)
                {
                    if (recipient.Attribute is ClassConfirmationAttribute)
                    {
                        var attribute = recipient.Attribute as ClassConfirmationAttribute;
                        msg.Personalizations[i].Substitutions = new Dictionary<string, string>();

                        foreach (var property in ReflectionHelper.GetClassProperties(attribute))
                        {
                            msg.Personalizations[i].Substitutions.Add($"%{property.ToUpper()}%", PropertyHelper.GetPropertyValue(attribute, property)?.ToString());
                        }
                    }

                    i++;
                }
            }

            if (ccRrecipientEmails.Any())
            {
                msg.AddCcs(ccRrecipientEmails);

            }

            if (model.ScheduleType == CampaignScheduleType.Scheduled)
            {
                var scheduleTime = campaign.ScheduleDate;

                var destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                var sourceTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ActiveClub.TimeZone.ToDescription());

                var localDateTime = DateTimeHelper.ConvertLocalDateTimeToUtc(scheduleTime.Value, ActiveClub.TimeZone ?? Common.Enums.TimeZone.UTC);

                // New method
                campaign.Status = model.ScheduleType;
                var timeStamp = DateTimeHelper.ConvertToUnixTimeStamp(localDateTime);
                msg.SendAt = timeStamp;
                //get batchId 
                var batchId = GetBatchId();

                msg.BatchId = batchId;
                campaign.BatchId = msg.BatchId;
            }
            else
            {
                campaign.Status = model.ScheduleType;
                campaign.ScheduleDate = null;
                campaign.SendDate = DateTime.UtcNow;
            }

            // New method 
            var values = new Dictionary<string, dynamic>
            {
                {
                    "CampaignId", model.Id
                },
            };

            var args = values.ToDictionary(k => (string)k.Key, k => (string)Convert.ToString(k.Value));
            msg.AddGlobalCustomArgs(args);

            var categories = new List<string>();
            categories.Add(ActiveClub.Domain);
            msg.Categories = categories;

            msg.SetOpenTracking(true, "");
            msg.SetClickTracking(true, true);
            msg.SetSpamCheck(false);

            var res = Ioc.CampaignBusiness.UpdateCampaign(campaign);

            var apiKey = WebConfigHelper.Sendgrid_ApiKey;
            var client = new SendGridClient(apiKey);

            var response = client.SendEmailAsync(msg);

            Response result = response.Result;
            return result;

        }
        public string GetBatchId()
        {
            var apiKey = WebConfigHelper.Sendgrid_ApiKey;
            var client = new SendGridClient(apiKey);

            var response = client.RequestAsync(SendGridClient.Method.POST, urlPath: "mail/batch");
            string jsonContent = response.Result.Body.ReadAsStringAsync().Result.ToString();

            var result = jsonContent.Substring(13, (jsonContent.Length - 13) - 2);

            return result;
        }
        public string CancelCampaignWithbatchId(string batchId)
        {
            var apiKey = WebConfigHelper.Sendgrid_ApiKey;
            var client = new SendGridClient(apiKey);

            var data = @"{
                          'batch_id':" + "'" + batchId + "'" + @",
                          'status': 'cancel'
                           }";

            Object json = JsonConvert.DeserializeObject<Object>(data);

            data = json.ToString();

            var response = client.RequestAsync(method: SendGridClient.Method.POST, urlPath: "user/scheduled_sends", requestBody: data);
            string jsonContent = response.Result.Body.ReadAsStringAsync().Result;
            return jsonContent;
        }

        private static IEnumerable<CampaignRecipients> ApplayFilterOnUsers(DynamicListModel model, Club club, string listName, int? listId)
        {
            // New codes

            var allOrderItems =
                Ioc.OrderItemBusiness.GetList()
                    .Where(oi => (oi.Order.ClubId == club.Id && oi.Order.IsLive != (oi.Season.Status == SeasonStatus.Test) || (oi.ProgramSchedule.Program.OutSourceSeasonId.HasValue && oi.ProgramSchedule.Program.OutSourceSeason.ClubId == club.Id && oi.Order.IsLive != (oi.Season.Status == SeasonStatus.Test))) && oi.ItemStatus == OrderItemStatusCategories.completed);
            var isAllRecipients = model.IsAllPrograms && model.IsAllSeasons && int.Parse(model.MinAge) == 0 &&
                                  int.Parse(model.MaxAge) == 0 && model.DateOption == DateConditionStep1.All &&
                                  model.Gender == GenderCategories.All && model.Graduate == "All";

            if (!isAllRecipients)
            {

                if (!model.IsAllSeasons)
                {
                    var seasonsIds = model.Seasons.Select(long.Parse).ToList();
                    allOrderItems = allOrderItems.Where(oi => seasonsIds.Any(s => s == oi.SeasonId || (oi.ProgramSchedule.Program.OutSourceSeasonId.HasValue && seasonsIds.Contains(oi.ProgramSchedule.Program.OutSourceSeasonId.Value))));
                }
                if (!model.IsAllPrograms)
                {
                    var programsIds = model.Programs.Select(int.Parse).ToList();
                    allOrderItems = allOrderItems.Where(oi => programsIds.Any(p => p == oi.ProgramSchedule.ProgramId));
                }
                if (model.DateOption != DateConditionStep1.All)
                {
                    DateTime? startDate;

                    switch (model.DateOption)
                    {
                        case DateConditionStep1.Last30Days:
                            {
                                startDate = DateTime.UtcNow.AddDays(-30);
                                allOrderItems =
                                    allOrderItems.Where(
                                        oi =>
                                            DbFunctions.TruncateTime(oi.Order.CompleteDate) >= startDate.Value &&
                                            DbFunctions.TruncateTime(oi.Order.CompleteDate) <= DateTime.UtcNow);
                                break;
                            }
                        case DateConditionStep1.Last3Months:
                            {
                                startDate = DateTime.UtcNow.AddMonths(-3);
                                allOrderItems =
                                    allOrderItems.Where(
                                        oi =>
                                            DbFunctions.TruncateTime(oi.Order.CompleteDate) >= startDate.Value &&
                                            DbFunctions.TruncateTime(oi.Order.CompleteDate) <= DateTime.UtcNow);
                                break;
                            }
                        case DateConditionStep1.LastYear:
                            {
                                startDate = DateTime.UtcNow.AddYears(-1);
                                allOrderItems =
                                    allOrderItems.Where(
                                        oi =>
                                            DbFunctions.TruncateTime(oi.Order.CompleteDate) >= startDate.Value &&
                                            DbFunctions.TruncateTime(oi.Order.CompleteDate) <= DateTime.UtcNow);
                                break;
                            }
                        case DateConditionStep1.ThisYear:
                            {
                                var year = DateTime.UtcNow.Year;
                                allOrderItems = allOrderItems.Where(oi => oi.Order.CompleteDate.Year == year);
                                break;
                            }
                        case DateConditionStep1.Custom:
                            {
                                startDate = model.CustomDateStart.Value;
                                DateTime? endDate = model.CustomDateEnd.Value;
                                allOrderItems =
                                    allOrderItems.Where(
                                        oi =>
                                            DbFunctions.TruncateTime(oi.Order.CompleteDate) >= startDate.Value &&
                                            DbFunctions.TruncateTime(oi.Order.CompleteDate) <= endDate.Value);
                                break;
                            }
                    }
                }
                if (int.Parse(model.MinAge) > 0)
                {
                    var maxAge = int.Parse(model.MinAge);
                    var minAge = int.Parse(model.MaxAge);
                    if (int.Parse(model.MaxAge) > 0)
                    {
                        allOrderItems =
                            allOrderItems.Where(
                                oi =>
                                    oi.Player.Contact.DoB.HasValue &&
                                    (DateTime.UtcNow.Year - oi.Player.Contact.DoB.Value.Year) >= maxAge &&
                                    (DateTime.UtcNow.Year - oi.Player.Contact.DoB.Value.Year) <= minAge);
                    }
                    else
                    {
                        allOrderItems =
                            allOrderItems.Where(
                                oi =>
                                    oi.Player.Contact.DoB.HasValue &&
                                    (DateTime.UtcNow.Year - oi.Player.Contact.DoB.Value.Year) >=
                                    minAge);
                    }
                }
                if (model.Gender != GenderCategories.All)
                {
                    allOrderItems = allOrderItems.Where(oi => oi.Player.Gender == model.Gender);
                }
                if (model.Graduate != "All" && model.Graduate == "Graduateds")
                {
                    allOrderItems = allOrderItems.Where(oi => oi.Player.EducationalStatus == EducationalStatus.graduated);
                }
                if (model.Graduate != "All" && model.Graduate == "NotGraduateds")
                {
                    allOrderItems = allOrderItems.Where(oi => oi.Player.EducationalStatus != EducationalStatus.graduated);
                }
                var conditions = new DynamicListModel
                {
                    MinAge = model.MinAge,
                    MaxAge = model.MaxAge,
                    Gender = model.Gender,
                    IsAllPrograms = model.IsAllPrograms,
                    IsAllSeasons = model.IsAllSeasons,
                    Seasons = model.Seasons,
                    Programs = model.Programs,
                    DateOption = model.DateOption,
                    CustomDateStart = model.CustomDateStart,
                    CustomDateEnd = model.CustomDateEnd,
                    Graduate = model.Graduate
                };

                var newListWithQuery = new MailList
                {
                    Club_Id = club.Id,
                    CreateDate = DateTime.UtcNow,
                    ListName = listName,
                    ListType = MailListType.Dynamic,
                    QueryString = JsonConvert.SerializeObject(conditions),
                    ListContact = null
                };

                if (listId.HasValue && allOrderItems.Any())
                {
                    newListWithQuery.Id = listId.Value;
                    Ioc.MailListBusiness.UpdateMailList(newListWithQuery);
                }
                else if (!listId.HasValue && !string.IsNullOrEmpty(listName) && allOrderItems.Any())
                {
                    Ioc.MailListBusiness.CreateList(newListWithQuery);
                }

            }

            var campaignRecipients = new List<CampaignRecipients>();
            if (isAllRecipients)
            {
                var allRecipients = Ioc.ClubBusiness.Get(club.Id).Users;

                var allUsers = Ioc.UserProfileBusiness.GetPlayerProfiles(allRecipients.Select(a => a.UserId).ToList());

                foreach (var user in allUsers.DistinctBy(p => p.UserId).ToList())
                {
                    campaignRecipients.Add(new CampaignRecipients
                    {
                        EmailAddress = user.User.UserName,
                        RecipientName = "",
                        RecipientLastName = user.Contact.LastName,
                        IsClicked = false,
                        IsOpened = false,
                        EmailStatus = EmailStatus.processed,
                    });
                }
            }
            else
            {
                campaignRecipients.AddRange(
                    allOrderItems.DistinctBy(c => c.Order.UserId).Select(orderItem => new CampaignRecipients
                    {
                        EmailAddress = orderItem.Order.User.UserName,
                        RecipientName = "",
                        RecipientLastName = orderItem.LastName,
                        IsClicked = false,
                        IsOpened = false,
                        EmailStatus = EmailStatus.processed,
                    }));
            }

            return campaignRecipients;
        }

        public virtual JsonNetResult DeleteCampaignTemplate(int templateId)
        {
            var result = Ioc.EmailCampaignTemplate.DeleteCampaignTemplate(templateId);

            return JsonNet(new JResult { Status = result.Status });
        }

        public virtual JsonNetResult GetAllListRecipients(int listId, List<GridSort> sort = null)
        {
            var termValue = Request.Params["term"] ?? string.Empty;

            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);

            var listRecipients = Ioc.MailListBusiness.GetListRecipients(listId);

            if (!string.IsNullOrEmpty(termValue))
            {
                listRecipients = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Match(termValue).Success
                    ? listRecipients.Where(p => p.EmailAddress.ToLower().Equals(termValue.ToLower()))
                    : listRecipients.Where(
                        p => p.RecipientName.ToLower().StartsWith(termValue.ToLower()) || p.RecipientLastName.ToLower().StartsWith(termValue.ToLower()));
            }

            #region sort code
            //if (sort != null)
            //{
            //    foreach (var s in sort)
            //    {
            //        switch (s.field)
            //        {
            //            case "EmailAddress":
            //                {
            //                    filteredData = (s.dir == "asc")
            //                        ? filteredData.OrderBy(o => o.EmailAddress)
            //                        : filteredData.OrderByDescending(o => o.EmailAddress);
            //                    break;
            //                }
            //            case "FullName":
            //                {
            //                    filteredData = (s.dir == "asc")
            //                        ? filteredData.OrderBy(o => o.FullName)
            //                        : filteredData.OrderByDescending(o => o.FullName);
            //                    break;
            //                }
            //        }
            //    }
            //}
            #endregion

            var data = listRecipients.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            var finalData = data.Select(r => r.ToViewModel<EmailListRecipientsViewModel>());

            var total = listRecipients.Count();

            var dataSource = finalData;

            return JsonNet(new { DataSource = dataSource, TotalCount = total });
        }

        public virtual JsonNetResult DeleteCampaignMailList(int listId)
        {
            var result = Ioc.MailListBusiness.DeleteMailList(listId);

            return JsonNet(new JResult { Status = result.Status });
        }

        public virtual JsonNetResult DeleteEmailListRecipient(int listId, int recipientId)
        {
            var result = Ioc.MailListBusiness.DeleteMailListRecipient(listId, recipientId);

            return JsonNet(new JResult { Status = result.Status });
        }

        public virtual JsonNetResult DeleteCampaignRecipient(int campaignId, int recipientId)
        {
            var result = Ioc.CampaignBusiness.DeleteCampaignRecipient(campaignId, recipientId);
            return JsonNet(new JResult { Status = result.Status });
        }

        public virtual FileResult DownloadSampleCsv()
        {
            var file = Server.MapPath("~/Content/sample.csv");
            if (System.IO.File.Exists(file))
            {
                return File(file, "application/csv", "Sample Csv For Upload.csv");
            }
            else
            {
                throw new HttpException(404, "File not found");
            }
        }

        [HttpGet]
        public virtual JsonNetResult SendParticipantEmail(int userId, int programId, long? orderItemId)
        {
            var club = new Club();

            if (orderItemId.HasValue)
            {
                var orderItem = Ioc.OrderItemBusiness.GetItem(orderItemId.Value);
                club = orderItem.Order.Club;
            }
            else
            {
                club = Ioc.ClubBusiness.Get(ActiveClub.Domain);
            }

            var user = Ioc.UserProfileBusiness.Get(userId);

            var model = new ParticipantEmailViewModel
            {
                EmailStyle = EmailStyle.Text,
                SelectedTemplateId = "0",
                From = club.ContactPersons.FirstOrDefault().Email,
                To = user.UserName,
                ProgramId = programId,
                ClubDomain = club.Domain

            };

            var templatesNames =
                club.Templates.Where(t => t.Type == TemplateType.Email)
                    .Select(temp => new SelectKeyValue<string>
                    {
                        Value = temp.Id.ToString(),
                        Text = temp.TemplateName
                    })
                    .ToList();

            //templatesNames.Add(new SelectKeyValue<string> { Value = "0", Text = "New Template ..." });

            model.TemplateNames = templatesNames;

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult SendParticipantEmail(ParticipantEmailViewModel model)
        {

            if (model.EmailStyle == EmailStyle.Template)
            {
                if (model.SelectedTemplateId == "0")
                {
                    ModelState.AddModelError("model.SelectedTemplateId", "Please select one of email templates.");
                }

                if (!string.IsNullOrEmpty(model.MailTemplate.Body))
                    ModelState.Remove("model.Body");

                if (!string.IsNullOrEmpty(model.Body) && string.IsNullOrEmpty(model.MailTemplate.Body))
                    ModelState.AddModelError("model.Body", "Email Body is required");
            }

            if (ModelState.IsValid)
            {
                string body;

                if (model.EmailStyle == EmailStyle.Text)
                {
                    body =
                        HttpUtility.HtmlDecode(this.RenderViewToString(Constants.Path_EmailTemplate,
                            new EmailTemplateModel { Body = model.Body }));
                }
                else
                {
                    body =
                         HttpUtility.HtmlDecode(this.RenderViewToString(Constants.Path_EmailTemplate, model.MailTemplate));
                }

                var cc = new List<MailAddress> { new MailAddress(model.From) };

                var from = new MailAddress(model.From);
                var to = new MailAddress(model.To);

                Ioc.OldEmailBusiness.SendEmail(from, to, model.Subject, body, true, null, cc, model.ClubDomain, null);

                if (model.ProgramId != 0)
                {
                    return JsonNet(new JResult { Status = true, Message = "Program" });
                }
                else
                {
                    return JsonNet(new JResult { Status = true, Message = "Donation" });
                }

            }
            return JsonFormResponse();
        }

        public virtual ActionResult AddNewListToTheCampaign(int campaignId, HttpPostedFileBase uploadedList)
        {
            var club = ActiveClub;
            var file = uploadedList;
            var fileName = club.Domain + "UploadedList.csv";
            var storageManager = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);

            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                var contentType = string.Format(Constants.F_AttachmentMemeType, Path.GetExtension(file.FileName).Substring(1));
                var fileData = binaryReader.ReadBytes(file.ContentLength);

                Stream fileStream = new MemoryStream(fileData);

                storageManager.UploadBlob(club.Domain, fileName, fileStream, contentType);

                fileStream.Close();
            }

            StreamReader csvreader = null;
            var sourcePath = StorageManager.PathCombine(club.Domain, fileName);
            var targetPath = StorageManager.PathCombine(club.Domain, fileName);

            if (StorageManager.IsExist(StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, fileName).AbsoluteUri))
            {
                var blobReader = new WebClient();
                var uploadedFile = blobReader.DownloadString(StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, fileName).AbsoluteUri);

                csvreader = new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(uploadedFile)));
            }
            else
            {
                ModelState.AddModelError("model.NoFileUploaded", "Please select a file to create list base on.");
            }

            if (ModelState.IsValid)
            {
                var csvFile = new CsvContext();
                List<UploadedListModel> recipients;
                if (!csvFile.IsInCorrectFormat<UploadedListModel>(csvreader))
                {
                    if (csvreader != null)
                    {
                        csvreader.Close();
                        csvreader.Dispose();
                    }

                    ModelState.AddModelError("model.FileNotInFormat",
                        "Your file must be in correct syntax, Please download the sample file and use it as a reference.");
                    if (
                        StorageManager.IsExist(
                            StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, fileName).AbsoluteUri))
                    {
                        storageManager.ReadFile(sourcePath, targetPath);
                        storageManager.DeleteBlob(club.Domain, fileName);
                    }
                    return JsonFormResponse();
                }
                else
                {
                    recipients = csvFile.Read<UploadedListModel>(csvreader).ToList();
                    if (recipients.Any(i => i.Email == null))
                    {
                        ModelState.AddModelError("FileContentError",
                            "Your file must be in correct syntax, Please check all fields have a value.");
                        if (
                            StorageManager.IsExist(
                                StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, fileName)
                                    .AbsoluteUri))
                        {
                            storageManager.ReadFile(sourcePath, targetPath);
                            storageManager.DeleteBlob(club.Domain, fileName);
                        }
                        return JsonFormResponse();
                    }
                    if (csvreader != null)
                    {
                        csvreader.Close();
                        csvreader.Dispose();
                    }
                }

                var newRecipientses = new List<CampaignRecipients>();
                var incorrectEmails = new List<string>();

                foreach (var item in recipients)
                {
                    var email = Regex.Replace(item.Email, @"\s+", ""); ;

                    if (Regex.IsMatch(email,
                        "^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$"))
                    {
                        // Add User email to the campaignRecipients.
                        newRecipientses.Add(new CampaignRecipients
                        {
                            EmailAddress = email,
                            RecipientName = item.FirstName,
                            RecipientLastName = item.LastName,
                            IsClicked = false,
                            IsOpened = false,
                            EmailStatus = EmailStatus.processed,
                        });
                    }
                    else
                    {
                        incorrectEmails.Add(email);
                    }
                }

                if (incorrectEmails.Count > 0)
                {
                    var emails = string.Join(", ", incorrectEmails);
                    ModelState.AddModelError("FileContentError",
                           "Your file must be in correct syntax, Please check " + emails + " in uploaded file.");
                    if (
                        StorageManager.IsExist(
                            StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, fileName)
                                .AbsoluteUri))
                    {
                        storageManager.ReadFile(sourcePath, targetPath);
                        storageManager.DeleteBlob(club.Domain, fileName);
                    }
                    return JsonFormResponse();
                }

                if (StorageManager.IsExist(StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, fileName).AbsoluteUri))
                {
                    storageManager.ReadFile(sourcePath, targetPath);
                    storageManager.DeleteBlob(club.Domain, fileName);
                }
                var result = Ioc.CampaignBusiness.AddNewCampaignRecipients(campaignId, newRecipientses);

                return JsonNet(new JResult { Status = result.Status, Message = result.Message });
            }
            return JsonFormResponse();
        }

        public string GetImageURL(string fileName, string clubDomain, string folderName)
        {
            string imageUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, string.Format(@"{0}\{1}", clubDomain, folderName), fileName).AbsoluteUri;

            imageUri = string.Concat(imageUri, "?", "rnd=", Guid.NewGuid());

            if (string.IsNullOrEmpty(fileName))
            {
                imageUri = string.Empty;
            }

            return imageUri;
        }


        #region Class confirmatioin
        public ActionResult GetClassConfirmationStep1()
        {
            var model = Ioc.CampaignBusiness.GetClassConfirmationModel(ActiveClub.Id);

            return JsonNet(model);
        }

        [HttpPost]
        public ActionResult SaveClassConfirmationStep1(ClassConfirmationCampaignModel model)
        {
            validationcheckClassConfirmation(model);

            if (ModelState.IsValid)
            {
                return JsonNet(true);
            }

            return JsonNet(false);
        }

        [HttpPost]
        public ActionResult GetClassConfirmationItems(ClassConfirmationCampaignModel model, PaginationModel paginationModel)
        {
            var result = Ioc.CampaignBusiness.GetClassConfirmationItems(model, ActiveClub.Id, paginationModel.Page, paginationModel.PageSize);

            return JsonNet(result);
        }

        public ActionResult GetClassConfirmationDetails(int programId, PaginationModel paginationModel)
        {
            var result = Ioc.CampaignBusiness.GetClassConfirmationDetails(programId, paginationModel.Page, paginationModel.PageSize);

            return JsonNet(result);
        }

        [HttpPost]
        public ActionResult MakeCampaignForClassConfiramtion(ClassConfirmationCampaignModel model)
        {
            var result = Ioc.CampaignBusiness.MakeCampaignForConfirmationClass(model, ActiveClub.Id);
            var campaignId = result.RecordsAffected;

            return JsonNet(new { result.Status, Data = campaignId, Message = result.Message });
        }

        #endregion

        #region private methods

        private void CheckModelValidationStep1SomePeople(CampaignViewModelStep1 model, CampaignReciepientsMode mode)
        {

            switch (mode)
            {
                case CampaignReciepientsMode.SomePeople:
                    {
                        if (int.Parse(model.SomePeople.MinAge) == 0)
                        {
                            ModelState.Remove("model.SomePeople.Age_In");
                        }
                        if (int.Parse(model.SomePeople.MaxAge) == 0)
                        {
                            ModelState.Remove("model.SomePeople.Age_To");
                        }

                        if (model.SomePeople.DateOption == DateConditionStep1.Custom)
                        {
                            if (!model.SomePeople.CustomDateStart.HasValue)
                            {
                                ModelState.AddModelError("model.SomePeople.CustomDateStart",
                                    string.Format("Start date is required."));
                            }
                            if (!model.SomePeople.CustomDateEnd.HasValue)
                            {
                                ModelState.AddModelError("model.SomePeople.CustomDateEnd",
                                    string.Format("End date is required."));
                            }
                            if (model.SomePeople.CustomDateStart.HasValue && model.SomePeople.CustomDateEnd.HasValue &&
                                model.SomePeople.CustomDateStart.Value.Date > model.SomePeople.CustomDateEnd.Value.Date)
                            {
                                ModelState.AddModelError("model.SomePeople.CustomDateEnd",
                                    string.Format("End date must be greater than end date."));
                            }
                        }

                        if ((!model.SomePeople.IsAllSeasons && model.SomePeople.Seasons == null) ||
                            (!model.SomePeople.IsAllSeasons && model.SomePeople.Seasons.Count == 0))
                        {
                            ModelState.AddModelError("model.SomePeople.Seasons",
                                string.Format("Please select one or more of seasons."));
                        }

                        if ((!model.SomePeople.IsAllPrograms && model.SomePeople.Programs == null) ||
                            (!model.SomePeople.IsAllPrograms && model.SomePeople.Programs.Count == 0))
                        {
                            ModelState.AddModelError("model.SomePeople.Programs",
                                string.Format("Please select one or more of programs."));
                        }

                        //if (string.IsNullOrEmpty(model.ListName) && !model.SomePeople.IsAllSeasons || !model.SomePeople.IsAllPrograms || model.SomePeople.Age_In > 0 || model.SomePeople.Age_To > 0 || model.SomePeople.DateOption != DateConditionStep1.All || model.SomePeople.Gender != GenderCategories.All)
                        //{
                        //    ModelState.AddModelError("model.ListName",
                        //        string.Format("Please enter a name for new list."));
                        //}

                        break;
                    }
                case CampaignReciepientsMode.UserList:
                    {
                        ModelState.Remove("model.CampaignName");
                        break;
                    }
            }

        }

        private void CheckModelValidationStep2(CampaignViewModelStep2 model)
        {
            if ((model.MailTemplate.ChangeTemplate && int.Parse(model.SelectedTemplateId) == 0 &&
                 string.IsNullOrEmpty(model.MailTemplate.NewName)))
            {
                ModelState.AddModelError("model.MailTemplate.NewName", "please enter a name for new templates.");
            }

            if (model.EmailStyle == EmailStyle.Template)
            {
                if (!string.IsNullOrEmpty(model.MailTemplate.Body))
                    ModelState.Remove("model.Body");

                if (!string.IsNullOrEmpty(model.Body) && string.IsNullOrEmpty(model.MailTemplate.Body))
                    ModelState.AddModelError("model.Body", "Email Body is required");
            }


            if (!string.IsNullOrEmpty(model.ReplyTo) &&
                !Regex.IsMatch(model.ReplyTo,
                    "^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$"))
            {
                ModelState.AddModelError("model.ReplyTo", " ");
            }
            if (!string.IsNullOrEmpty(model.Ccs) && !model.IsCcListEmailSet)
            {
                string s = model.Ccs;
                string[] values = s.Split(',');
                for (int i = 0; i < values.Length; i++) { values[i] = values[i].Trim(); }
                var isWrong = false;
                foreach (var item in values)
                {
                    if (item != "" && !Regex.IsMatch(item, "^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$"))
                    {
                        isWrong = true;
                        break;
                    }
                }

                if (isWrong)
                {
                    ModelState.AddModelError("model.Ccs", "Email format is invalid.");
                }

            }
        }

        private void CheckModelValidationStep3(CampaignViewModelStep3 model)
        {
            if (model.ScheduleType == CampaignScheduleType.Scheduled && !model.SendTime.HasValue)
            {
                ModelState.AddModelError("model.SendTime", "Send time is required.");
            }
            if (model.ScheduleType == CampaignScheduleType.Scheduled)
            {


                if (model.SendDate.HasValue)
                {
                    var now = Ioc.ClubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow);

                    var treeDayFromNow = now.AddDays(3);

                    if (model.SendDate.Value > treeDayFromNow)
                    {
                        ModelState.AddModelError("model.SendDate", "Schedule date can't be more than 3 days from today.");
                    }

                    if (model.SendDate.Value.Date < DateTime.UtcNow.Date)
                    {
                        ModelState.AddModelError("model.SendDate", "Schedule Date can not be past day.");
                    }
                }
                else
                {
                    ModelState.AddModelError("model.SendDate", "Schedule date is required.");
                }
            }

        }

        private static List<SelectKeyValue<string>> SeedAges(int max)
        {
            var result = new List<SelectKeyValue<string>> { new SelectKeyValue<string> { Text = "Select age", Value = "0" } };

            for (var i = 1; i < max + 1; i++)
            {
                result.Add(new SelectKeyValue<string> { Text = i.ToString(), Value = i.ToString() });
            }

            return result;
        }

        private void validationcheckClassConfirmation(ClassConfirmationCampaignModel model)
        {
            var today = DateTime.UtcNow.Date;
            var club = Ioc.ClubBusiness.Get(ActiveClub.Domain);

            today = Ioc.ClubBusiness.GetClubDateTime(club, today);

            if (model.SeasonName == SeasonNames.SelectSeason)
            {
                ModelState.AddModelError("model.SeasonName", "You should select the season.");
            }
            if (model.Year == 0)
            {
                ModelState.AddModelError("model.Year", "You should select the year.");
            }

            if (!model.IsAllSchools && !model.SelectedSchools.Any())
            {
                ModelState.AddModelError("model.SelectedSchools", "You should select the school.");
            }

            if (model.ClassDateTime.HasValue && model.ClassDateTime.Value.Date < today)
            {
                ModelState.AddModelError("model.ClassDateTime", "Class date can not be before today.");
            }

        }

        #endregion
    }

    public class CancelResult
    {
        public string Status { get; set; }

        public List<CancelResultError> Errors { get; set; }

        public class CancelResultError
        {
            public string Field { get; set; }

            public string Message { get; set; }
        }
    }

}