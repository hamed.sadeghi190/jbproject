﻿using Jumbula.Common.Attributes;
using Jumbula.Common.Enums;
using Jumbula.Common.Exceptions;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Core.Identity;
using Jumbula.Core.Model.Email;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.ClubsService;
using Jumbula.Web.WebLogic.UserService;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using SportsClub.Models.Email;
using System;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Controllers
{
    [Authorize]
    public class AccountController : JbBaseController
    {
        private readonly IClubBusiness _clubBusiness;
        private readonly IApplicationUserManager<JbUser, int> _applicationUserManager;
        private readonly IApplicationSignInManager _applicationSignInManager;
        private readonly IEmailBusiness _emailBusiness;

        public AccountController(IClubBusiness clubBusiness, IApplicationUserManager<JbUser, int> applicationUserManager, IApplicationSignInManager applicationSignInManager, IEmailBusiness emailBusiness)
        {
            _clubBusiness = clubBusiness;
            _applicationUserManager = applicationUserManager;
            _applicationSignInManager = applicationSignInManager;
            _emailBusiness = emailBusiness;

            _applicationUserManager.UserTokenProvider = new DataProtectorTokenProvider<JbUser, int>(
                new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Sample").Create(
                    "EmailConfirmation"))
            {
                TokenLifespan = TimeSpan.FromDays(15.0)
            };
        }

        [AllowAnonymous]
        public virtual ActionResult LoginBranded(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            var model = new LoginRegister
            {
                Login = new LoginModel(),
                Register = new RegisterModel(),
                LogoUrl = this.GetCurrentRequestType() == RequestType.Branded ? this.GetCurrentClubBaseInfo().Logo : "/Images/jb-logo.png",
            };

            var currentClub = _clubBusiness.Get(this.GetCurrentClubId());

            if (currentClub.PartnerId.HasValue)
            {
                Club club = null;
                Club partner = null;
                Club clubMessage = null;

                partner = currentClub.PartnerClub;
                club = currentClub;
                clubMessage = currentClub;

                if (partner != null && partner.Setting != null)
                {
                    var partnerSetting = _clubBusiness.GetPartnerSetting(partner.Id);
                    var readLoginTabFromPartner = partnerSetting.PortalParameters.ReadLoginTabFromPartner;
                    var readMessageNewFamilyFromPartner = partnerSetting.PortalParameters.ReadMessageNewFamilyFromPartner;

                    if (readLoginTabFromPartner)
                    {
                        club = partner;
                    }
                    if (readMessageNewFamilyFromPartner)
                    {
                        clubMessage = partner;
                    }
                    model.Register.NewFamilyNotification = clubMessage.Setting.NewFamilyNotification;

                    model.RegisterMode = club.Setting != null ? club.Setting.IsNewFamily ? true : false : false;
                }

            }
            else if (currentClub.ClubType.EnumType == ClubTypesEnum.Partner)
            {
                var club = currentClub;

                if (club != null && club.Setting != null)
                {
                    model.RegisterMode = club.Setting != null ? club.Setting.IsNewFamily ? true : false : false;

                    model.Register.NewFamilyNotification = club.Setting.NewFamilyNotification;
                }

            }
            else
            {
                var club = currentClub;
                model.RegisterMode = club.Setting != null ? club.Setting.IsNewFamily ? true : false : false;
                model.Register.NewFamilyNotification =  club.Setting.NewFamilyNotification;
            }


            return View(model);
        }

        [AllowAnonymous]
        public virtual ActionResult LoginNotBranded(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public virtual async Task<ActionResult> LoginNotBranded(LoginModel model, string returnUrl)
        {
            if (!_applicationUserManager.IsUserExist(model.UserNameLogin))
            {
                ModelState.AddModelError("UserNameLogin", Constants.M_UsernameOrPasswordIsIncorrect);
            }

            if (ModelState.IsValid)
            {
                var club = _clubBusiness.GetNotBrandedUserClub(model.UserNameLogin);

                var clubBaseInfo = ClubService.Get().GetClubBaseInfo(club);

                CurrentUser userBaseInfo = GetCurrentUser(model.UserNameLogin, club);

                // To enable password failures to trigger account lockout, change to shouldLockout: true
                var result =
                    await
                    _applicationSignInManager.PasswordSignInAsync(model.UserNameLogin, model.PasswordLogin, clubBaseInfo, userBaseInfo, model.RememberMe, false);

                switch (result)
                {
                    case SignInStatus.Success:
                        {
                            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                            var token = Ioc.UserProfileBusiness.GenerateTokenForSwitchLogin(model.UserNameLogin);

                            if (!string.IsNullOrEmpty(token))
                            {
                                var url = string.Empty;

                                var host = Request.Url.Host;
                                var domain = club.Domain;
                                var scheme = Request.Url.Scheme;
                                var loginByTokenUrl = Ioc.UserProfileBusiness.GetUrlForLogin(token);

                                url = string.Concat(scheme, "://", domain, ".", host, "/", loginByTokenUrl);

                                return Redirect(url);
                            }
                            break;
                        }
                    case SignInStatus.Failure:
                        {

                            ModelState.AddModelError("UserNameLogin", Constants.M_UsernameOrPasswordIsIncorrect);

                            return View(model);
                        }
                    case SignInStatus.LockedOut:
                        {
                            return View("Lockout");
                        }
                    case SignInStatus.RequiresVerification:
                        {
                            return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, model.RememberMe });
                        }
                }


                return View();
            }

            return View(model);
        }

        [@JbAuthorize(JbAction.ParentDashboard_View)]
        [JbAudit(JbAction.Family_View_Admin)]
        public ActionResult LoginByAdmin(int userId)
        {
            var currentClubId = this.GetCurrentClubId();

            var currentClub = this.GetCurrentClubBaseInfo();

            var isPartner = currentClub.ClubType.EnumType == ClubTypesEnum.Partner;

            var isParentDashboardAccesibleByAdmin = Ioc.UserProfileBusiness.IsParentDashboardAccesibleByAdmin(currentClubId, userId, isPartner);
            if (!isParentDashboardAccesibleByAdmin) // Check if this user can be access from admin
            {
                throw new Exception("Admin can't access to this parent dashboard");
            }
            else
            {
                SetActivityDescription("Admin {0} logged in.", this.GetCurrentUserName());
                SetOwnerKey(userId.ToString());
                Status = true;
                // set cookie
                var currentUserId = this.GetCurrentUserId();
                Response.Cookies["SwitchedParent"][currentUserId.ToString()] = userId.ToString();
                Response.Cookies["SwitchedParent"].Expires = DateTime.Now.AddDays(1d);
            }

            return RedirectToAction("GetFamiliesProfile", "Registrant");
        }

        [AllowAnonymous]
        public virtual async Task<ActionResult> LoginByToken(string token)
        {
            var user = Ioc.UserProfileBusiness.GetUserByToken(token);

            if (user == null || !Ioc.UserProfileBusiness.IsTokenValid(token))
            {
                return RedirectPermanent("/Login");
            }

            var currentClubDomain = this.GetCurrentClubDomain();

            var club = _clubBusiness.Get(currentClubDomain);

            var clubBaseInfo = ClubService.Get().GetClubBaseInfo(club);

            CurrentUser userBaseInfo = GetCurrentUser(user.UserName, club);

            await _applicationSignInManager.SignInAsync(user, clubBaseInfo, userBaseInfo, false, false).ContinueWith(c =>
            {
                if (userBaseInfo.Role == RoleCategory.Admin)
                {
                    SetCookieForFreeTrialViewFirstTimeLogin(club);
                }

            }
         );

            Ioc.UserProfileBusiness.UpdateLastLoggedInClub(user.UserName, currentClubDomain);

            return RedirectPermanent("/");
        }

        public virtual ActionResult RedirectToClub(string clubDomain)
        {
            var url = string.Empty;
            var userName = this.GetCurrentUserName();

            var host = Request.Url.Host;
            var scheme = Request.Url.Scheme;
            var loginByTokenUrl = Ioc.UserProfileBusiness.GetUrlForLogin(clubDomain, userName);

            host = string.Concat(host.Split('.')[1], ".", host.Split('.')[2]);

            url = string.Concat(scheme, "://", clubDomain, ".", host, "/", loginByTokenUrl);

            return Redirect(url);
        }

        public virtual ActionResult GetUrlForLogin(string clubDomain)
        {
            var url = string.Empty;
            var userName = this.GetCurrentUserName();

            var host = Request.Url.Host;
            var scheme = Request.Url.Scheme;
            var loginByTokenUrl = Ioc.UserProfileBusiness.GetUrlForLogin(clubDomain, userName);

            host = string.Concat(host.Split('.')[1], ".", host.Split('.')[2]);

            url = string.Concat(scheme, "://", clubDomain, ".", host, "/", loginByTokenUrl);

            var model = new { Url = url };

            return JsonNet(model);
        }

        [AllowAnonymous]
        [HttpPost]
        [JbAudit(JbAction.Login)]
        public virtual async Task<ActionResult> LoginBranded(LoginModel model, string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            var club = _clubBusiness.Get(ClubSubDomain);
            var staff = _clubBusiness.GetStaff(model.UserNameLogin, club.Id);
            var clubBaseInfo = ClubService.Get().GetClubBaseInfo(club);

            if (!_applicationUserManager.IsUserExist(model.UserNameLogin))
            {
                ModelState.AddModelError("UserNameLogin", Constants.M_UsernameOrPasswordIsIncorrect);
            }
            else
            {
                if (club.ClubType != null && club.ClubType.EnumType == ClubTypesEnum.Partner && staff != null && EnumHelper.ParseEnum<RoleCategory>(staff.JbUserRole.Role.Name) != RoleCategory.Partner)
                {
                    ModelState.AddModelError("UserNameLogin", "Invalid login attempt.");
                }
            }

            if (!ModelState.IsValid)
            {
                var loginModel = new LoginRegister
                {
                    Login = model,
                    Register = new RegisterModel(),
                    LogoUrl = this.GetCurrentClubBaseInfo().Logo,
                };

                return View(loginModel);
            }

            CurrentUser userBaseInfo = GetCurrentUser(model.UserNameLogin, club);

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result =
                await
                _applicationSignInManager.PasswordSignInAsync(model.UserNameLogin, model.PasswordLogin, clubBaseInfo, userBaseInfo, model.RememberMe, false);

            switch (result)
            {
                case SignInStatus.Success:
                    {
                        Status = true;

                        Ioc.UserProfileBusiness.UpdateLastLoggedInClub(model.UserNameLogin, club.Domain);

                        if (string.IsNullOrEmpty(returnUrl))
                        {
                            switch (RoleTypeAttribute.GetRoleType(userBaseInfo.Role))
                            {
                                case RoleCategoryType.Parent:
                                    {
                                        SetOwnerKey(userBaseInfo.Id.ToString());
                                        SetActivityDescription("{0} was logged in", userBaseInfo.UserName);
                                        SetZone(JbZone.ParentDashboard);
                                        returnUrl = "/";
                                    }
                                    break;
                                case RoleCategoryType.Dashboard:
                                    {
                                        SetActivityDescription("Admin {0} was logged in", userBaseInfo.UserName);
                                        SetZone(JbZone.AdminDashboard);
                                        returnUrl = Constants.Jumbula_Dashbaord_Url;

                                        SetCookieForFreeTrialViewFirstTimeLogin(club);
                                    }
                                    break;
                                case RoleCategoryType.System:
                                    {
                                        if (userBaseInfo.Role == RoleCategory.Support)
                                        {
                                            returnUrl = Url.Action("Index", "Support");
                                        }

                                        if (userBaseInfo.Role == RoleCategory.SysAdmin)
                                        {
                                            returnUrl = Constants.Jumbula_Dashbaord_Url;
                                        }
                                    }
                                    break;
                            }
                        }

                        return Redirect(returnUrl);
                    }
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, model.RememberMe });
                case SignInStatus.Failure:
                    {
                        var loginModel = new LoginRegister
                        {
                            Login = model,
                            Register = new RegisterModel(),
                            LogoUrl = this.GetCurrentClubBaseInfo().Logo,
                        };

                        ModelState.AddModelError("UserNameLogin", Constants.M_UsernameOrPasswordIsIncorrect);
                        return View(loginModel);
                    }
                default:
                    {
                        var loginModel = new LoginRegister
                        {
                            Login = model,
                            Register = new RegisterModel(),
                            LogoUrl = this.GetCurrentClubBaseInfo().Logo,
                        };

                        ModelState.AddModelError("UserNameLogin", "Invalid login attempt.");
                        return View(loginModel);
                    }
            }
        }

        public void SetCookieForFreeTrialViewFirstTimeLogin(Club club)
        {
            if (club.Client.PricePlan.Type == PricePlanType.FreeTrial)
            {
                Response.Cookies["FreeTrialViewFirstTimeLogin"].Value = "true";
            }
        }
        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [JbAudit(JbAction.Login)]
        public virtual async Task<ActionResult> Login(LoginModel model, string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            var club = _clubBusiness.Get(ClubSubDomain);
            var staff = _clubBusiness.GetStaff(model.UserNameLogin, club.Id);

            if (staff.IsDeleted || !_applicationUserManager.IsUserExist(model.UserNameLogin))
            {
                ModelState.AddModelError("UserNameLogin", Constants.M_UsernameOrPasswordIsIncorrect);
            }

            if (!ModelState.IsValid)
            {
                var loginModel = new LoginRegister
                {
                    Login = model
                };

                return View(loginModel);
            }

            var clubBaseInfo = ClubService.Get().GetClubBaseInfo(club);

            CurrentUser userBaseInfo = GetCurrentUser(model.UserNameLogin, club);

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result =
                await
                _applicationSignInManager.PasswordSignInAsync(model.UserNameLogin, model.PasswordLogin, clubBaseInfo, userBaseInfo, model.RememberMe, false);

            switch (result)
            {
                case SignInStatus.Success:
                    {
                        Status = true;
                        SetActivityDescription("{0} was logged in", model.UserNameLogin);

                        if (string.IsNullOrEmpty(returnUrl))
                        {
                            switch (RoleTypeAttribute.GetRoleType(userBaseInfo.Role))
                            {
                                case RoleCategoryType.Parent:
                                    {

                                        SetZone(JbZone.ParentDashboard);
                                        returnUrl = "/";
                                    }
                                    break;
                                case RoleCategoryType.Dashboard:
                                    {
                                        SetZone(JbZone.AdminDashboard);
                                        returnUrl = Constants.Jumbula_Dashbaord_Url;
                                    }
                                    break;
                                case RoleCategoryType.System:
                                    {
                                        if (userBaseInfo.Role == RoleCategory.Support)
                                        {
                                            returnUrl = Url.Action("Index", "Support");
                                        }

                                        if (userBaseInfo.Role == RoleCategory.SysAdmin)
                                        {
                                            returnUrl = Constants.Jumbula_Dashbaord_Url;
                                        }
                                    }
                                    break;
                            }
                        }

                        return Redirect(returnUrl);
                    }
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, model.RememberMe });
                case SignInStatus.Failure:
                    {
                        var loginModel = new LoginRegister
                        {
                            Login = model
                        };

                        ModelState.AddModelError("UserNameLogin", Constants.M_UsernameOrPasswordIsIncorrect);
                        return View(loginModel);
                    }
                default:
                    {
                        var loginModel = new LoginRegister
                        {
                            Login = model
                        };

                        ModelState.AddModelError("UserNameLogin", "Invalid login attempt.");
                        return View(loginModel);
                    }
            }
        }

        public CurrentUser GetCurrentUser(string userNameLogin, Club club)
        {
            CurrentUser result = null;

            ClubStaff clubStaff = null;

            if (club != null && club.ClubStaffs != null)
            {
                clubStaff = _clubBusiness.GetStaff(userNameLogin, club.Id);
            }

            // Login as club staff
            if (clubStaff != null)
            {
                var role = clubStaff.RoleOnClub;

                if (role == RoleCategory.Partner && !club.IsPartner)
                {
                    role = RoleCategory.Admin;
                }

                result = new CurrentUser()
                {
                    UserName = clubStaff.JbUserRole.User.UserName,
                    Id = clubStaff.JbUserRole.User.Id,
                    Role = role
                };
            }
            else
            {
                var userId = _applicationUserManager.FindByNameAsync(userNameLogin).Result.Id;

                result = new CurrentUser()
                {
                    UserName = userNameLogin,
                    Id = userId,
                };

                //Login as Support
                if (_applicationUserManager.IsInRole(userId, RoleCategory.Support.ToString()))
                {
                    result.Role = RoleCategory.Support;

                    return result;
                }

                //Login as SysAdmin
                if (_applicationUserManager.IsInRole(userId, RoleCategory.SysAdmin.ToString()))
                {
                    result.Role = RoleCategory.SysAdmin;

                    return result;
                }

                // Login as Parent
                if (!_applicationUserManager.IsInRole(userId, RoleCategory.Parent.ToString()))
                {
                    _applicationUserManager.AddToRole(userId, RoleCategory.Parent.ToString());
                }

                result.Role = RoleCategory.Parent;
            }

            return result;
        }

        //[RequireHttps]
        [AllowAnonymous]
        public virtual JsonResult CheckIsUserNameExist(string userName)
        {
            if (!_applicationUserManager.IsUserExist(userName))
            {
                return Json(new { Status = "UserIsNotExist" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Status = "UserIsExist", Message = string.Format("Sorry, it looks like {0} belongs to an existing user.", userName) }, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public virtual ActionResult Register(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            var model = new LoginRegister
            {
                Login = new LoginModel(),
                Register = new RegisterModel(),
                LogoUrl = this.GetCurrentRequestType() == RequestType.Branded ? this.GetCurrentClubBaseInfo().Logo : "/Images/jb-logo.png",
                RegisterMode = true
            };

            return View("LoginBranded", model);
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [JbAudit(JbAction.Signup)]
        //[ValidateAntiForgeryToken]
        public virtual async Task<ActionResult> Register(RegisterModel model, string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            if (ModelState.IsValid)
            {
                var user = new JbUser { UserName = model.UserName, Email = model.UserName };

                var result = await _applicationUserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    SetOwnerKey(user.Id.ToString());
                    SetActivityDescription("{0} was signed up", user.UserName);
                    SetZone(JbZone.ParentDashboard);
                    Status = true;

                    _applicationUserManager.AddToRole(user.Id, RoleCategory.Parent.ToString());

                    _applicationUserManager.UserTokenProvider = new DataProtectorTokenProvider<JbUser, int>(new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Sample").Create("EmailConfirmation"));
                    string token = _applicationUserManager.GenerateEmailConfirmationTokenAsync(user.Id).Result;
                    _applicationUserManager.ConfirmEmail(user.Id, token);

                    var club = _clubBusiness.Get(ClubSubDomain);
                    var clubBaseInfo = ClubService.Get().GetClubBaseInfo(club);
                    CurrentUser userBaseInfo = new CurrentUser() { Id = user.Id, Role = RoleCategory.Parent, UserName = user.UserName };

                    await _applicationSignInManager.SignInAsync(user, clubBaseInfo, userBaseInfo, false, false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await _applicationUserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    if (string.IsNullOrEmpty(returnUrl))
                    {
                        return RedirectToAction("Dashboard", "Registrant");
                    }

                    return Redirect(returnUrl);
                }

                AddErrors(result);
            }

            var loginRegisterModel = new LoginRegister()
            {
                Login = new LoginModel(),
                Register = model,
                RegisterMode = true
                // LogoUrl = (RequestType == RequestType.External) ? GetExternalClub().ExtenalNavBar.ImageUrl : "/Images/jb-logo.png"
            };

            // If we got this far, something failed, redisplay form
            return View("LoginBranded", loginRegisterModel);
        }


        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public virtual async Task<ActionResult> ConfirmEmail(int userId, string code)
        {
            if (code == null)
            {
                return View("Error");
            }
            var result = await _applicationUserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [RequireHttps]
        [AllowAnonymous]
        //[AjaxRequest]
        public virtual ActionResult ForgotPassword(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            var model = new ForgotPasswordViewModel()
            {
                LogoUrl = this.GetCurrentRequestType() == RequestType.Branded ? this.GetCurrentClubBaseInfo().Logo : "/Images/jb-logo.png"
            };

            return View("ForgetPassword", model);
        }

        [RequireHttps]
        [HttpPost]
        [AllowAnonymous]
        public virtual JsonResult ForgotPassword(ForgotPasswordViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (_applicationUserManager.IsUserExist(model.Email))
                    {
                        var requestType = this.GetCurrentRequestType();

                        var parameterModel = new ResetPasswordParameterModel
                        {
                            ClubId = requestType == RequestType.Branded ? this.GetCurrentClubId() : (int?)null,
                            RequestType = requestType,
                            ResetPasswordUrl = Url.Action("ResetPassword", "Account", new { user = "jbemail", returnUrl, token = "jbtoken" }, WebConfigHelper.SecureHttpProtocol),
                            UserName = model.Email,
                            ReturnUrl = returnUrl
                        };

                        _emailBusiness.Send(EmailCategory.ResetPassword, parameterModel);

                        return Json(new { Status = "Success", Message = string.Format(Constants.F_ResetPasswordEmailSentNotificationDesc, model.Email) }, JsonRequestBehavior.AllowGet);

                    }
                    return Json(new { Message = Constants.M_ResetPasswordCannotFindAccount }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            //// If we got this far, something failed, redisplay form
            return Json(new { Status = "Erorr", Message = Constants.M_ResetPasswordCannotFindAccount }, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public virtual ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [RequireHttps]
        [AllowAnonymous]
        public virtual ActionResult ResetPassword(string user, string token, string returnUrl)
        {
            string userFullName = user;
            var logoUrl = this.GetCurrentRequestType() == RequestType.Branded ? this.GetCurrentClubBaseInfo().Logo : "/Images/jb-logo.png";
            var model = new ResetPasswordViewModel { Email = user, UserFullName = userFullName, Token = token, LogoUrl = logoUrl, ReturnUrl = Server.UrlEncode(returnUrl) };
            return View(model);
        }

        [RequireHttps]
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            try
            {
                if (!_applicationUserManager.IsUserExist(model.Email))
                {
                    throw new JumbulaNoLocalAccountException(new Exception());
                }

                var user = _applicationUserManager.FindByNameAsync(model.Email).Result;

                var resetPasswordResult = _applicationUserManager.ResetPassword(user.Id, model.Token, model.Password);

                if (!resetPasswordResult.Succeeded)
                {
                    throw new Exception(); // is this true????????????
                }

                #region Send reset successfaully mail

                var requestType = this.GetCurrentRequestType();

                var resetPasswordSuccessEmailParameterModel = new ResetPasswordSuccessParameterModel
                {
                    ClubId = requestType == RequestType.Branded ? this.GetCurrentClubId() : null as int?,
                    RequestType = requestType,
                    UserName = model.Email
                };

                _emailBusiness.Send(EmailCategory.ResetPasswordSuccess, resetPasswordSuccessEmailParameterModel);
                #endregion

                return RedirectToAction("ResetPasswordSuccess", new { user = model.Email, returnUrl = Server.UrlEncode(model.ReturnUrl) });

            }
            catch (JumbulaTokenExpiredException)
            {
                return RedirectToAction("ErrorResetPasswordTokenExpired", new { user = model.Email });
            }
            catch (System.Net.Mail.SmtpException e)
            {
                LogHelper.LogException(e);
                return RedirectToAction("ResetPasswordFailure", new { user = model.Email });
            }
            catch (Exception e)
            {
                LogHelper.LogException(e);
                return RedirectToAction("ResetPasswordFailure", new { user = model.Email });
            }
        }

        [RequireHttps]
        [AllowAnonymous]
        public virtual ActionResult ResetPasswordSuccess(string user, string returnUrl)
        {


            string userFullName = user;

            ViewData["user"] = userFullName;
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [RequireHttps]
        [AllowAnonymous]
        public virtual ActionResult ErrorResetPasswordTokenExpired()
        {
            return View();
        }

        [RequireHttps]
        [AllowAnonymous]
        public virtual ActionResult ResetPasswordFailure(string user)
        {
            string userFullName = user;

            ViewData["user"] = userFullName;
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public virtual ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider,
                Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        [AllowAnonymous]
        [JbAudit(JbAction.Logout)]
        public virtual ActionResult LogOff()
        {
            if (this.IsUserAuthenticated())
            {
                SetOwnerKey(this.GetCurrentUserId().ToString());
            }

            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            SetActivityDescription("User was logged out");
            SetZone(JbZone.ParentDashboard);
            Status = true;

            var club = _clubBusiness.Get(ClubSubDomain);

            string redirectUrl = string.Empty;

            if (club.Setting != null)
            {
                redirectUrl = UrlHelpers.GetProperUrl(club.Setting.LogOutRedirectUrl, club.Site, Request.Url.GetLeftPart(UriPartial.Authority));
            }
            else
            {
                redirectUrl = UrlHelpers.GetProperUrl(new ClubRedirectUrl(), club.Site, Request.Url.GetLeftPart(UriPartial.Authority));
            }

            if (!string.IsNullOrEmpty(redirectUrl))
            {
                return Redirect(redirectUrl);
            }

            return Redirect("/");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public virtual ActionResult ExternalLoginFailure()
        {
            return View();
        }

        #region Staff
        [RequireHttps]
        [AllowAnonymous]
        [HttpGet]
        public virtual ActionResult ConfirmStaffEmail(string userName, string token, bool updateMode = false)
        {
            var model = new ConfirmInvitationViewModel();

            var club = _clubBusiness.Get(this.GetCurrentClubDomain());
            var clubStaff = club.ClubStaffs.SingleOrDefault(s => !s.IsDeleted && s.JbUserRole.User.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase));

            if (clubStaff == null)
            {
                return Redirect(string.Concat(Request.Url.Scheme, "://", Request.Url.Host));
            }

            // If user confirmed before, we don't show confirmation page
            if (_applicationUserManager.IsEmailConfirmed(clubStaff.JbUserRole.UserId))
            {
                if (clubStaff.Status != StaffStatus.Pending)
                {
                    return RedirectToAction("ConfirmEmailAlreadyConfirmed", new { userName = userName });
                }
                else
                {
                    return RedirectToAction("ConfirmEmailAlreadyConfirmed", new { userName = userName, token = token });
                }
            }

            ViewBag.UpdateMode = updateMode;

            model.UserName = clubStaff.JbUserRole.User.UserName;

            if (club.ClubType.EnumType == ClubTypesEnum.Partner && club.ClubStaffs.Any(s => !s.IsDeleted && s.JbUserRole.User.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase) && s.RoleOnClub != RoleCategory.Partner))
            {
                model.IsNotPartnerRoleInPartnerClub = true;
            }

            return View(model);
        }

        [RequireHttps]
        [AllowAnonymous]
        [HttpPost]
        public virtual async Task<ActionResult> ConfirmStaffEmail(ConfirmInvitationViewModel model, bool updateMode = false)
        {
            var token = Request.QueryString["token"].ToString();

            if (ModelState.IsValid)
            {

                if (_applicationUserManager.IsUserExist(model.UserName))
                {
                    var user = Ioc.UserProfileBusiness.Get(model.UserName);
                    var club = _clubBusiness.Get(this.GetCurrentClubDomain());


                    _applicationUserManager.UserTokenProvider =
                        new DataProtectorTokenProvider<JbUser, int>(
                            new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Sample").Create(
                                "EmailConfirmation"));

                    IdentityResult identityResult = new IdentityResult();

                    if (updateMode)
                    {
                        identityResult = _applicationUserManager.RemovePassword(user.Id);
                    }
                    else
                    {
                        identityResult = _applicationUserManager.ConfirmEmail(user.Id, token);
                    }

                    if (identityResult.Succeeded)
                    {
                        var staff = club.ClubStaffs.Where(s => !s.IsDeleted).Single(s => s.JbUserRole.User.UserName.Equals(model.UserName, StringComparison.OrdinalIgnoreCase));
                        var changePasswordSucceeded = _applicationUserManager.AddPassword(user.Id, model.Password).Succeeded;

                        if (changePasswordSucceeded)
                        {
                            if (staff.Contact == null)
                            {
                                staff.Contact = new ContactPerson();
                            }

                            staff.Status = staff.IsFrozen ? staff.Status : StaffStatus.Active;

                            var result = _clubBusiness.Update(club);

                            if (!result.Status)
                            {
                                ModelState.AddModelError("", Constants.Notification_Error);

                                return View(model);
                            }

                            var clubBaseInfo = ClubService.Get().GetClubBaseInfo(club);
                            CurrentUser userBaseInfo = GetCurrentUser(user.UserName, club);

                            if (_clubBusiness.IsNotPartnerRoleInPartnerClub(staff.Id))
                            {
                                return RedirectToActionPermanent("ConfirmStaffEmailSuccess", new { userName = model.UserName });
                            }
                            else
                            {
                                await _applicationSignInManager.SignInAsync(user, clubBaseInfo, userBaseInfo, model.Rememberme, model.Rememberme);
                            }
                        }

                        if (staff.IsFrozen || staff.IsDeleted)
                        {
                            return Redirect("/");
                        }
                        else
                        {
                            return Redirect(Constants.Jumbula_Dashbaord_Url);
                        }
                    }

                }
                else
                {
                    ModelState.AddModelError("Password", Constants.M_UsernameOrPasswordIsIncorrect);
                    return View(model);
                }

            }

            return View(model);
        }


        [RequireHttps]
        [AllowAnonymous]
        [HttpGet]
        public virtual ActionResult ConfirmStaffAlreadyConfirmedEmail(string userName, string token)
        {
            var club = _clubBusiness.Get(this.GetCurrentClubDomain());
            var clubStaff = club.ClubStaffs.Single(s => s.JbUserRole.User.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase));

            if (clubStaff.Status != StaffStatus.Pending)
            {
                return RedirectToAction("ConfirmEmailAlreadyConfirmed", new { userName = userName });
            }

            return View();
        }

        [RequireHttps]
        [AllowAnonymous]
        [HttpPost]
        public virtual ActionResult ConfirmStaffAlreadyConfirmedEmail(bool updateMode)
        {
            var userName = Request.QueryString["userName"].ToString();
            var token = Request.QueryString["token"].ToString();

            if (updateMode)
            {
                return RedirectToAction("ConfirmStaffEmail", new { userName = userName, token = token, updateMode = true });
            }

            var club = _clubBusiness.Get(this.GetCurrentClubDomain());
            var clubStaff = club.ClubStaffs.Single(s => s.JbUserRole.User.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase));

            clubStaff.Status = StaffStatus.Active;

            var result = _clubBusiness.Update(club);

            if (result.Status)
            {
                return Redirect(Constants.Jumbula_Dashbaord_Url);
            }

            return View();
        }

        [RequireHttps]
        [AllowAnonymous]
        public virtual ActionResult ConfirmStaffEmailSuccess(string userName)
        {
            var club = this.GetCurrentClubBaseInfo();

            ViewBag.Message = "Your account is now confirmed!";

            ViewBag.Logo = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);

            if (_clubBusiness.IsNotPartnerRoleInPartnerClub(userName, club.Id))
            {
                ViewBag.DonotShowRedirectButton = true;
            }

            if (_clubBusiness.IsNotPartnerRoleInPartnerClub(userName, club.Id))
            {
                return View("ConfirmStaffEmailSuccess2");
            }

            return View("ConfirmStaffEmailSuccess");
        }

        #endregion

        [RequireHttps]
        [AllowAnonymous]
        public virtual ActionResult ConfirmEmailAlreadyConfirmed(string userName)
        {
            ViewBag.UserName = userName;

            return View();
        }

        [RequireHttps]
        [AllowAnonymous]
        [HttpPost]
        public virtual async Task<ActionResult> ConfirmEmailAlreadyConfirmed(ConfirmInvitationViewModel model)
        {

            if (ModelState.IsValid)
            {
                var club = _clubBusiness.Get(this.GetCurrentClubDomain());

                var staff = club
                     .ClubStaffs.Where(s => !s.IsDeleted)
                     .SingleOrDefault(
                           s => s.JbUserRole.User.UserName.Equals(model.UserName, StringComparison.OrdinalIgnoreCase));

                if (staff == null || !_applicationUserManager.IsUserExist(model.UserName))
                {
                    ModelState.AddModelError("UserName", Constants.M_UsernameOrPasswordIsIncorrect);
                    return View(model);
                }

                if (_applicationUserManager.IsUserExist(model.UserName))
                {
                    staff = club.ClubStaffs.Where(s => !s.IsDeleted).FirstOrDefault(s => s.JbUserRole.User.UserName.Equals(model.UserName, StringComparison.OrdinalIgnoreCase));
                    if (staff.Status == StaffStatus.Pending && !staff.IsFrozen)
                    {
                        if (staff.Contact == null)
                        {
                            staff.Contact = new ContactPerson();
                        }
                        staff.Status = StaffStatus.Active;
                        _clubBusiness.Update(club);
                    }

                    var user = Ioc.UserProfileBusiness.Get(model.UserName);

                    _applicationUserManager.UserTokenProvider =
                        new DataProtectorTokenProvider<JbUser, int>(
                            new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Sample").Create(
                                "EmailConfirmation"));

                    var clubBaseInfo = ClubService.Get().GetClubBaseInfo(club);
                    CurrentUser userBaseInfo = GetCurrentUser(user.UserName, club);

                    var result = await _applicationSignInManager.PasswordSignInAsync(model.UserName, model.Password, clubBaseInfo, userBaseInfo, model.Rememberme, false);

                    switch (result)
                    {
                        case SignInStatus.Success:
                            {
                                if (_clubBusiness.IsNotPartnerRoleInPartnerClub(staff.Id))
                                {
                                    return RedirectToActionPermanent("ConfirmStaffEmailSuccess", new { userName = model.UserName });
                                }

                                if (userBaseInfo.Role == RoleCategory.Parent)
                                {
                                    return Redirect("/");
                                }

                                return Redirect(Constants.Jumbula_Dashbaord_Url);

                            }
                        case SignInStatus.Failure:
                            {

                                ModelState.AddModelError("Password", Constants.M_UsernameOrPasswordIsIncorrect);

                                return View(model);
                            }

                    }

                    return Redirect(Constants.Jumbula_Dashbaord_Url);
                }
            }

            return View(model);
        }


        #region Helpers

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        #endregion

    }
}