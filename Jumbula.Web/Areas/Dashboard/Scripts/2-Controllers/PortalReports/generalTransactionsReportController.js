﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('generalTransactionsReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', 'reportService', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, reportService) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();
        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;


        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        $scope.Model = customReportService.getModelFromStrorage();

        $http.get(baseUrl + '/FillFinanceFilters')
            .success(function (data, status, headers, config) {
                if (is.undefined($scope.Model.Filters) || $scope.Model.ReportName != $scope.MC.stateParams.reportName) {
                    $scope.Model.Filters = {};
                    $scope.Model.Filters.TransactionFilters = {};
                }
                $scope.AllMembers = data.AllMembers;
                $scope.memberId = "";
                $scope.Model.Filters.TransactionFilters.TransactionCategories = data.TransactionsCategories;
                $scope.Model.Filters.TransactionFilters.PaymentMethods = data.PaymentMethods;
                $scope.Model.Filters.TransactionFilters.GroupBy = data.GroupBy;
                $scope.Model.Filters.TransactionFilters.PaymentMethod = "";
                $scope.Model.Filters.TransactionFilters.TransactionCategory = "";
                $scope.Model.Filters.TransactionFilters.IsGroupBy = false;

                prepareGrid();
            }).error(function (data, status, headers, config) {
                //alert(data);
            });

        var prepareGrid = function() {
            $scope.transactionReportsDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport:
                {
                    read: {
                        url: baseUrl + '/GeneralReportRun',
                        type: "POST",
                        data: function() {
                            return {
                                seasonDomain: $scope.MC.stateParams.seasonDomain,
                                filters: $scope.Model.Filters,
                                memberId: $scope.memberId,
                                isGroupBy: $scope.Model.Filters.TransactionFilters.IsGroupBy,
                                isExcelExport: false
                            };
                        },
                    },

                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                batch: true,
                resizable: true,
            });

            $scope.TransactionReportGridOptions = {
                excel: {
                    allPages: true,
                    fileName: "General transactions.xlsx",
                    proxyURL: "/Dashboard/CustomReport/Save"
                },
                excelExport: function(e) {
                    cfpLoadingBar.complete();
                },
                dataSource: $scope.transactionReportsDataSource,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100
                    ],
                    buttonCount: 5
                },
                dataBound: function(e) {
                    customReportService.initializeDoubleScroll();
                },
                columnMenu: true,
                columns: [
                    {
                        field: "ClubName",
                        title: "Organiziation",
                        width: "180px",
                        lockable: true,
                        locked: true,
                    },
                    {
                        field: "FullName",
                        title: "Name",
                        width: "180px",
                        lockable: true,
                        locked: true,
                    },
                    {
                        field: "SeasonName",
                        title: "Season",
                        width: "180px",
                        lockable: true,
                        locked: true,
                    },
                    {
                        field: "ProgramName",
                        title: "Program",
                        width: "160px",
                        lockable: true,
                        locked: true,
                    },
                    {
                        field: "UserEmail",
                        title: "Account email",
                        width: "160px",
                        lockable: true,
                        locked: true,
                    },
                    {
                        field: "Date",
                        title: "Transaction date",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "PayerEmail",
                        title: "Payer email",
                        width: "180px",
                        lockable: true,
                    },
                    {
                        field: "Confirmation",
                        title: "Confirmation",
                        width: "140px",
                        lockable: true,
                        locked: true,
                    },
                    {
                        field: "TransactionType",
                        title: "Transaction type",
                        width: "140px",
                        lockable: true,
                    },
                    {
                        field: "TransactionCategory",
                        title: "Transaction category",
                        width: "185px",
                        lockable: true,
                    },
                    // I put it here.
                    {
                        field: "CheckId",
                        title: "Transaction ID",
                        width: "180px",
                        lockable: true,
                    },
                    {
                        field: "PaymentMethod",
                        title: "Payment method",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "PaypalHandler",
                        title: "Payment handler",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "Note",
                        title: "Memo",
                    },
                    {
                        field: "Payment",
                        title: "Payment",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "Charge",
                        title: "Deposit",
                        width: "160px",
                        lockable: true,
                    },
                ],
            }
        }

        $scope.saveToExcel = function () {

            cfpLoadingBar.start();

            $http.post(baseUrl + '/GeneralReportRun', {
                filters: $scope.Model.Filters,
                memberId: $scope.memberId,
                isGroupBy: $scope.Model.Filters.TransactionFilters.IsGroupBy,
                isExcelExport: true
            }, {
                headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
            }).success(function (data) {

                reportService.exportExcelData(data, "Transaction");
                cfpLoadingBar.complete();

            }).error(function (data, status, headers, config) {
                cfpLoadingBar.complete();

            });

        }


        $scope.showGrid = false;
        $scope.runReport = function () {

            $scope.showGrid = true;
            $scope.TransactionReportGridOptions.dataSource.read();
        }

        $scope.filterData = function () {
            customReportService.setModelInStorage($scope.Model);
            $scope.TransactionReportGridOptions.dataSource.read();
        }

        $scope.clearFilters = function () {
            $scope.Model.Filters.RegistrationStart = "";
            $scope.Model.Filters.RegistrationEnd = "";
            $scope.Model.Filters.TransactionFilters.TransactionCategory = {},
            $scope.Model.Filters.TransactionFilters.PaymentMethod = {};
            $scope.searchTerm = "";
            $scope.memberId = "";
            $scope.TransactionReportGridOptions.dataSource.read();
        }
    }]);
})();