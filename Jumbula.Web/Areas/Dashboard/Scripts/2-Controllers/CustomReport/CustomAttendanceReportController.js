﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('CustomAttendanceReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonDomain = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        var model = {};
        var dataInfo = {};

        $scope.Model = customReportService.getModelFromStrorage();

        var programId = $scope.Model._SelectedPrograms;

        if (programId == 'undefined' || programId == null) {
            programId = 0;
        }

        $http.get(baseUrl + '/GetCustomAttendanceReportDetail', { params: { seasonDomain: $scope.seasonDomain, programId: programId } }).success(function (data, status, headers, config) {

            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
            $scope.SeasonName = data.SeasonName;
            $scope.ClassName = data.ClassName;
            $scope.Days = data.Days;
            $scope.Room = data.Room;
            dataInfo = data;
            prepareGrid();

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });


        var prepareGrid = function () {
            $scope.CustomAttendanceDataSource = new kendo.data.DataSource({

                transport: {
                    read: {
                        url: baseUrl + "/GetCustomAttendanceReport",
                        dataType: "json",
                        type: "GET",
                        data: function () {
                            return {
                                programId: programId,
                            };
                        },
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                resizable: true,
                batch: true,
            });

            $scope.CustomAttendanceGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.ClubName + " (Attendance sheet).xlsx",
                    proxyURL: "/Dashboard/CustomReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.unshift({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Room: " + dataInfo.Room
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.ClassName + " - " + dataInfo.Days
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.SeasonName
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Attendance sheet - " + dataInfo.ClubName
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.CustomAttendanceDataSource,
                resizable: true,
                dataBound: function () {
                    var grid = $("#CustomAttendanceGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5,
                },
                sortable: true,
                columns: [

                    {
                        field: "ParticipantFullName",
                        title: "Full name",
                        width: "230px"
                    }, {
                        field: "StandardDismissal",
                        title: "Standard dismissal",
                        width: "230px"
                    }, {
                        field: "SchoolGrade",
                        title: "Grade",
                        width: "230px"
                    }, {
                        field: "SchoolTeacherName",
                        title: "Teacher name",
                        width: "230px"
                    }, {
                        field: "Medical_AllergiesMedicalInfo_SpecialNeeds",
                        title: "Special needs",
                        width: "230px",
                        template: '<span style="white-space: pre-line" > #=Medical_AllergiesMedicalInfo_SpecialNeeds# </span>'
                    }, {
                        field: "Medical_AllergiesAllergies",
                        title: "Allergies",
                        width: "230px",
                        template: '<span style="white-space: pre-line" > #=Medical_AllergiesAllergies# </span>'
                    }

                ]
            }
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#CustomAttendanceGrid").getKendoGrid().saveAsExcel();
        }
    }
    ]);

})();