
/****** Object:  Table [dbo].[ProgramAttributeValues]    Script Date: 10/31/2017 3:49:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ProgramAttributeValues](
	[ProgramId] [bigint] NOT NULL,
	[AttributeId] [int] NOT NULL,
	[Value] [nvarchar](max) NULL,
	[AttributeOptionId] [int] NULL,
 CONSTRAINT [PK_dbo.ProgramAttributeValues] PRIMARY KEY CLUSTERED 
(
	[ProgramId] ASC,
	[AttributeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProgramAttributeValues]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProgramAttributeValues_dbo.JbAttributeOptions_AttributeOptionId] FOREIGN KEY([AttributeOptionId])
REFERENCES [dbo].[JbAttributeOptions] ([Id])
GO

ALTER TABLE [dbo].[ProgramAttributeValues] CHECK CONSTRAINT [FK_dbo.ProgramAttributeValues_dbo.JbAttributeOptions_AttributeOptionId]
GO

ALTER TABLE [dbo].[ProgramAttributeValues]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProgramAttributeValues_dbo.JbAttributes_AttributeId] FOREIGN KEY([AttributeId])
REFERENCES [dbo].[JbAttributes] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ProgramAttributeValues] CHECK CONSTRAINT [FK_dbo.ProgramAttributeValues_dbo.JbAttributes_AttributeId]
GO

ALTER TABLE [dbo].[ProgramAttributeValues]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProgramAttributeValues_dbo.Programs_ProgramId] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Programs] ([Id])
GO

ALTER TABLE [dbo].[ProgramAttributeValues] CHECK CONSTRAINT [FK_dbo.ProgramAttributeValues_dbo.Programs_ProgramId]
GO


