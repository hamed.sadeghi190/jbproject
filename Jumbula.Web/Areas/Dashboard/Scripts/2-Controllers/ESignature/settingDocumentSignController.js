﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('settingDocumentSignController', ['$scope', '$http', '$stateParams', '$state', 'jbToast', function ($scope, $http, $stateParams, $state, jbToast) {
        window.app.globalObjects.pageClass = 'invoice_View';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + '/Dashboard/ESignature';


        $scope.Model = {};

        $http.get($scope.baseUrl+'/GetSettings').success(function (data, status, headers, config) {
            $scope.Model = data;
        });

        $scope.saveSetting = function () {
            $http.post($scope.baseUrl + '/SaveSetting', { model: $scope.Model})
         .success(function (data, status, headers, config) {
             if (data.Status == true) {
                 $scope.errors = null;

                 jbToast.success(data.Message, "");
                 $state.go('ESignature')
             }
             else {
                 $scope.MC.handleErrors($scope, data);
             }
         });
        }
       
        $scope.cancelAction = function () {
            if ("comesFromOrderItem" in window) {
                if (window.comesFromOrderItem) {
                    window.comesFromOrderItem = false;
                    $state.go("ESignature", null);
                }
            }
            else {
                $state.go("ESignature", null);
            }
        }
    }])

})();