﻿using Autofac;
using Jumbula.Core.Infrastructure;
using Jumbula.Core.Infrastructure.Email;
using Jumbula.Infrastructure;
using Jumbula.Infrastructure.Email;
using Jumbula.Web.Infrastructure.DataMigration;
using Jumbula.Web.Mvc.WebConfig;

namespace Jumbula.Bootstrapper.Modules
{
    public class InfrastructureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType(typeof(DataManipulationService)).As(typeof(IDataManipulationService)).InstancePerRequest();
            builder.RegisterType(typeof(JbDateTime)).As(typeof(IJbDateTime)).InstancePerRequest();

            builder.RegisterType(typeof(JbEmailService)).As(typeof(IJbEmailService)).InstancePerRequest().WithParameter("deploymentEnvironment", WebConfigHelper.DeploymentEnvironment);
            builder.RegisterType<SendGridEmailProvider>().As<IEmailProvider>().Keyed<IEmailProvider>(nameof(SendGridEmailProvider)).WithParameter("apiKey", WebConfigHelper.Sendgrid_ApiKey);
            builder.RegisterType<CommonEmailProvider>().As<IEmailProvider>().Keyed<IEmailProvider>(nameof(CommonEmailProvider)).WithParameter("config", WebConfigHelper.SmtpMailSettings);
            builder.RegisterType(typeof(JbLocker)).As(typeof(IJbLocker)).SingleInstance();
        }
    }
}
