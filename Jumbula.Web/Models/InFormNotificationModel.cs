﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public enum InFormNotificationType
    {
        Succeed,
        Information,
        Warning,
        Error
    }

    public static class InFormNotificationHelper
    {
        static InFormNotificationModel _Notification;
        public static InFormNotificationModel Notification
        {
            get
            {
                InFormNotificationModel temp = _Notification;

                _Notification = null;

                return temp;
            }
            private set
            {
                _Notification = value;
            }
        }

        public static void Add(string title, string text, InFormNotificationType type)
        {
            _Notification = new InFormNotificationModel()
            {
                Title = title,
                Text = text,
                Type = type
            };
        }
    }

    public class InFormNotificationModel
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public InFormNotificationType Type { get; set; }

        public InFormNotificationModel() { }

        public InFormNotificationModel(string title, string text, InFormNotificationType type)
            : this()
        {
            this.Title = title;
            this.Text = text;
            this.Type = type;
        }
    }
}