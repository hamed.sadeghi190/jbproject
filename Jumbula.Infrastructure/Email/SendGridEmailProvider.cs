﻿using Jumbula.Common.Delegates;
using Jumbula.Common.Helper;
using Jumbula.Core.Infrastructure.Email;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;

namespace Jumbula.Infrastructure.Email
{
    public class SendGridEmailProvider : IEmailProvider
    {
        #region Fields
        private readonly string _apiKey;
        #endregion

        #region Constructors
        public SendGridEmailProvider(string apiKey)
        {
            _apiKey = apiKey;
        }
        #endregion

        #region Public Methods
        public void Send(MailMessage message, int emailId, EmailCompletedEventHandler emailCompletedEventHandler, bool isCampaignMode = false)
        {
            try
            {
                var result = Send(message, emailId, isCampaignMode);

                var serializedResponse = JsonHelper.JsonSerializer(result);

                emailCompletedEventHandler(message, new EmailCompletedEventArgs(emailId, result.StatusCode == System.Net.HttpStatusCode.Accepted, serializedResponse));
            }
            catch (Exception ex)
            {
                emailCompletedEventHandler(message, new EmailCompletedEventArgs(emailId, ex));
            }
        }
        #endregion

        #region Private Methods

        private Response Send(MailMessage message, int emailId, bool isCampaignMode)
        {
            var client = new SendGridClient(_apiKey);

            var from = new EmailAddress(message.From.Address, message.From.DisplayName);
            var tos = message.To.DistinctBy(t => t.Address).Select(MapEmailAddress).ToList();
            var ccs = message.CC.Where(c => !tos.Select(t => t.Email).Contains(c.Address)).DistinctBy(t => t.Address).Select(MapEmailAddress).ToList();
            var bCCs = message.Bcc.Where(c => !tos.Select(t => t.Email).Contains(c.Address) && !ccs.Select(t => t.Email).Contains(c.Address)).DistinctBy(t => t.Address).Select(MapEmailAddress).ToList();

            var personalizations = new List<Personalization>();


            if (isCampaignMode)
            {
                foreach (var item in tos)
                {
                    personalizations.Add(MapPersonalization(item, ccs, bCCs));
                }
            }
            else
            {
                personalizations.Add(MapPersonalization(tos, ccs, bCCs));
            }

            var sendGridMessage = new SendGridMessage()
            {
                From = from,
                Subject = message.Subject,
                HtmlContent = message.Body,
                Personalizations = personalizations
            };

            if (message.ReplyToList.Any())
                sendGridMessage.SetReplyTo(new EmailAddress(message.ReplyToList.First().Address));

            if (message.Attachments.Any())
                sendGridMessage.AddAttachments(message.Attachments.Select(a => MapAttachment(a)).ToList());

            sendGridMessage.AddGlobalCustomArgs(GenerateCustomArgs(emailId));


            return client.SendEmailAsync(sendGridMessage).Result;
        }

        private Personalization MapPersonalization(EmailAddress to, List<EmailAddress> ccs, List<EmailAddress> bCCs)
        {
            var personalization = new Personalization
            {
                Tos = new List<EmailAddress> { to },
            };

            if (ccs.Any())
                personalization.Ccs = ccs;

            if (bCCs.Any())
                personalization.Bccs = bCCs;

            return personalization;
        }

        private Personalization MapPersonalization(List<EmailAddress> tos, List<EmailAddress> ccs, List<EmailAddress> bCCs)
        {
            var personalization = new Personalization
            {
                Tos = tos,
            };

            if (ccs.Any())
                personalization.Ccs = ccs;

            if (bCCs.Any())
                personalization.Bccs = bCCs;

            return personalization;
        }

        private EmailAddress MapEmailAddress(MailAddress mailAddress)
        {
            return new EmailAddress(mailAddress.Address, mailAddress.DisplayName);
        }

        private Dictionary<string, string> GenerateCustomArgs(int emailId)
        {
            return new Dictionary<string, string>
            {
                {
                    "EmailId", emailId.ToString()
                },
            };
        }

        private SendGrid.Helpers.Mail.Attachment MapAttachment(System.Net.Mail.Attachment attachment)
        {
            using (var stream = new MemoryStream())
            {
                try
                {
                    attachment.ContentStream.CopyTo(stream);
                    return new SendGrid.Helpers.Mail.Attachment()
                    {
                        Disposition = "attachment",
                        Type = attachment.ContentType.MediaType,
                        Filename = attachment.Name,
                        ContentId = attachment.ContentId,
                        Content = Convert.ToBase64String(stream.ToArray())
                    };
                }
                finally
                {
                    stream.Close();
                }
            }
        }

        #endregion
    }
}
