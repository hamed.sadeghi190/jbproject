﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Tests;
using NUnit.Framework;

namespace Jumbula.AutomationTests.Scenarios
{
    
    public class ClassSchedulingEmailTestScenario : BaseTestScenario
    {
        [Test]
        public void SendConfirmationEmail()

        {
            BaseUrl = DataDriver.SportClubURL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            ExtraAPI.Wait(5000);
            WebDriver.Manage().Window.Maximize();

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginAdmin();

            // Go to Notification Setting
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToNotificationSetting();

            // Save confirmation email setting
            var notificationSettingTest = new NotificationSettingTest(WebDriver);
            notificationSettingTest.SaveConfirmationEmail();

            // Go to programs
            bladeTest.GoToProgramAndOrders();

            // Send class confirm email
            var classSchedulingEmailTest = new ClassSchedulingEmailTest(WebDriver);

            classSchedulingEmailTest.SendConfirmationEmail();

            WebDriver.Close();

        }

        [Test]
        public void SendCancellationEmail()

        {
            BaseUrl = DataDriver.SportClubURL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            ExtraAPI.Wait(5000);
            WebDriver.Manage().Window.Maximize();

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginAdmin();

            // Go to Notification Setting
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToNotificationSetting();

            // Save cancellation email setting
            var notificationSettingTest = new NotificationSettingTest(WebDriver);
            notificationSettingTest.SaveCancellationEmail();

            // Go to programs
            bladeTest.GoToProgramAndOrders();

            // Send class confirm email
            var classSchedulingEmailTest = new ClassSchedulingEmailTest(WebDriver);

            classSchedulingEmailTest.SendCancellationEmail();

            WebDriver.Close();

        }
    }
}
