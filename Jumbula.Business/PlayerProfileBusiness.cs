﻿using Jb.Framework.Common.Forms;
using Jb.Framework.Common.Forms.Validation;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Jumbula.Common.Constants;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using System.Web.Mvc;

namespace Jumbula.Business
{
    public partial class PlayerProfileBusiness : IPlayerProfileBusiness
    {
        private IFormBusiness _formBusiness;
        private IJbFormBusiness _jbFormBusiness;
        private readonly IFamilyBusiness _familyBusiness;
        private IOrderBusiness _orderBusiness;
        private IOrderItemBusiness _orderItemBusiness;
        private readonly IFamilyContactBusiness _familyContactBusiness;
        private readonly IRepository<PlayerProfile> _playerProfileRepository;
        private readonly IEntitiesContext _dataContext;
        private ICustomReportBusiness _customReportBusiness;
        private readonly ISeasonBusiness _seasonBusiness;

        public PlayerProfileBusiness(IFamilyBusiness familyBusiness,
        IFamilyContactBusiness familyContactBusinsess,
        IRepository<PlayerProfile> playerProfileRepository,
        IEntitiesContext dataContext,
        ISeasonBusiness seasonBusiness, IJbFormBusiness jbFormBusiness)
        {
            _familyBusiness = familyBusiness;
            _familyContactBusiness = familyContactBusinsess;
            _playerProfileRepository = playerProfileRepository;
            _dataContext = dataContext;
            _seasonBusiness = seasonBusiness;
            _jbFormBusiness = jbFormBusiness;
        }

        public IOrderBusiness OrderBusienss
        {
            get => _orderBusiness;
            set => _orderBusiness = value;
        }

        public IFormBusiness FormBusiness
        {
            get => _formBusiness;
            set => _formBusiness = value;
        }

        public IOrderItemBusiness OrderItemBusienss
        {
            get => _orderItemBusiness;
            set => _orderItemBusiness = value;
        }

        public ICustomReportBusiness CustomReportBusiness
        {
            get => _customReportBusiness;
            set => _customReportBusiness = value;
        }

        public PlayerProfile Get(int playerId)
        {
            return _playerProfileRepository.GetList(p => p.Id == playerId).Single();
        }

        public IQueryable<PlayerProfile> GetList()
        {
            return _playerProfileRepository.GetList();
        }

        public IQueryable<PlayerProfile> GetList(string email)
        {
            return _playerProfileRepository.GetList(p => p.Contact.Email == email);
        }

        public IQueryable<PlayerProfile> GetList(int userId)
        {
            return _playerProfileRepository.GetList(p => p.UserId == userId && p.Status != PlayerProfileStatusType.Deleted);
        }

        public IQueryable<PlayerProfile> GetList(string firsName, string LastName)
        {
            firsName = firsName.ToLower();
            LastName = LastName.ToLower();
            var query = Enumerable.Empty<PlayerProfile>().AsQueryable();
            if (!string.IsNullOrEmpty(firsName))
                query = _playerProfileRepository.GetList(p => p.Contact.FirstName.ToLower().StartsWith(firsName) ||
                     p.Contact.FirstName.ToLower().EndsWith(firsName) ||
                     p.Contact.LastName.ToLower().StartsWith(firsName) ||
                     p.Contact.LastName.ToLower().EndsWith(firsName));
            if (!string.IsNullOrEmpty(LastName))
                query = query.Where(p => p.Contact.FirstName.ToLower().StartsWith(LastName) ||
                    p.Contact.FirstName.ToLower().EndsWith(LastName) ||
                    p.Contact.LastName.ToLower().StartsWith(LastName) ||
                    p.Contact.LastName.ToLower().EndsWith(LastName));
            return query;
        }
        public OperationStatus Create(PlayerProfile playerProfile)
        {
            return _playerProfileRepository.Create(playerProfile);
        }
        public OperationStatus Update(PlayerProfile playerProfile)
        {
            return _playerProfileRepository.Update(playerProfile);
        }
        public string GetPreapprovalkey(int playerId)
        {
            var player = _playerProfileRepository.GetList().SingleOrDefault(u => u.Id == playerId);
            string result = string.Empty;
            if (player != null)
            {

                if (!string.IsNullOrEmpty(player.PaypalPreapprovalID) && player.PaypalPreapprovalStartDate.HasValue && player.PaypalPreapprovalEndDate.HasValue)
                {
                    if (DateTime.UtcNow.Date.CompareTo(player.PaypalPreapprovalStartDate.Value.Date) > 0 && DateTime.UtcNow.Date.CompareTo(player.PaypalPreapprovalEndDate.Value.Date) < 0)
                    {
                        result = player.PaypalPreapprovalID;
                    }
                }

            }

            return result;
        }

        public bool HasPreapprovalkey(int playerId)
        {
            var player = _playerProfileRepository.GetList().SingleOrDefault(u => u.Id == playerId);
            if (player != null)
            {
                if (!string.IsNullOrEmpty(player.PaypalPreapprovalID))
                {
                    return true;
                }
            }

            return false;
        }

        public OperationStatus Delete(PlayerProfile profile)
        {
            profile = Get(profile.Id);
            profile.Status = PlayerProfileStatusType.Deleted;
            return Update(profile);

        }

        public PlayerProfile GetFromForm(JbForm jbForm, SectionsName section)
        {
            if (jbForm == null)
                return null;
            var profile = new PlayerProfile();
            var infoSection = jbForm.Elements.SingleOrDefault(e => e is JbSection && e.Name == section.ToString()) as JbSection;
            if (infoSection == null)
                return null;
            profile.Contact = new ContactPerson();

            profile.Contact.FirstName = infoSection.Elements.Any(e => e.Name == ElementsName.FirstName.ToString()) ? infoSection.Elements.Single(e => e.Name == ElementsName.FirstName.ToString()).GetValue().ToString() : string.Empty;
            profile.Contact.LastName = infoSection.Elements.Any(e => e.Name == ElementsName.LastName.ToString()) ? infoSection.Elements.Single(e => e.Name == ElementsName.LastName.ToString()).GetValue().ToString() : string.Empty;
            profile.Contact.Email = infoSection.Elements.Any(e => e.Name == ElementsName.Email.ToString()) ? infoSection.Elements.Single(e => e.Name == ElementsName.Email.ToString()).GetValue().ToString() : string.Empty;
            profile.Contact.Phone = infoSection.Elements.Any(e => e.Name == ElementsName.Phone.ToString()) ? infoSection.Elements.Single(e => e.Name == ElementsName.Phone.ToString()).GetValue().ToString() : string.Empty;
            var gender = infoSection.Elements.Any(e => e.Name == ElementsName.Gender.ToString()) ? ((JbDropDown)infoSection.Elements.Single(e => e.Name == ElementsName.Gender.ToString())).Value : string.Empty;
            if (!string.IsNullOrEmpty(gender))
            {
                profile.Gender = (GenderCategories)Enum.Parse(typeof(GenderCategories), gender, true);
            }
            else
            {
                profile.Gender = GenderCategories.None;
            }

            if (infoSection.Elements.Any(e => e.Name == ElementsName.DoB.ToString()))
            {
                try
                {
                    var dob = infoSection.Elements.First(e => e.Name == ElementsName.DoB.ToString()).GetValue();
                    if (dob != null && !string.IsNullOrEmpty(dob.ToString()))
                        profile.Contact.DoB = DateTime.Parse(dob.ToString());
                }
                catch { }
            }

            if (infoSection.Elements.Any(e => e.Name == ElementsName.Occupation.ToString()))
            {
                try
                {
                    profile.Contact.Occupation = infoSection.Elements.Any(e => e.Name == ElementsName.Occupation.ToString()) ?
                        ((JbTextBox)infoSection.Elements.Single(e => e.Name == ElementsName.Occupation.ToString())).Value :
                        string.Empty;
                }
                catch { }
            }

            if (infoSection.Elements.Any(e => e.Name == ElementsName.Employer.ToString()))
            {
                try
                {
                    profile.Contact.Employer = infoSection.Elements.Any(e => e.Name == ElementsName.Employer.ToString()) ?
                        ((JbTextBox)infoSection.Elements.Single(e => e.Name == ElementsName.Employer.ToString())).Value :
                        string.Empty;
                }
                catch { }
            }

            if (section == SectionsName.ParticipantSection)
            {
                profile.Relationship = RelationshipType.Registrant;
            }

            return profile;
        }

        public OperationStatus UpdateGraduatedProfiles(List<PlayerProfile> profiles)
        {
            profiles.ForEach(p => p.EducationalStatus = EducationalStatus.graduated);

            var result = _playerProfileRepository.Save();

            return result;
        }

        public JbSection GetParticipantSection(JbForm jbForm = null)
        {
            if (jbForm != null && jbForm.Elements.Any() &&
                jbForm.Elements.Any(e => e.Name.ToLower() == SectionsName.ParticipantSection.ToString().ToLower()))
                return jbForm.Elements.Single(e => e.Name == SectionsName.ParticipantSection.ToString()) as JbSection;

            var sectionParticipant = new JbSection() { Name = SectionsName.ParticipantSection.ToString(), Title = "Participant Information", CurrentMode = AccessMode.Edit };

            sectionParticipant.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.FirstName.ToString(),
                Title = "First name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "First name is required"
                        }
                    }
                }
            });

            sectionParticipant.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.LastName.ToString(),
                Title = "Last name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Last name is required"
                        }
                    }
                }
            });

            var itemsGender = Enum.GetValues(typeof(GenderCategories)).Cast<GenderCategories>().Where(g => g != GenderCategories.All && g != GenderCategories.None).Select(v => new JbElementItem
            {
                Text = ((GenderCategories)v).ToDescription(),
                Value = v.ToString(),
            }).ToList();

            sectionParticipant.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.Gender.ToString(),
                Title = "Gender",
                CurrentMode = AccessMode.Hidden,
                Items = itemsGender
            });
            sectionParticipant.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.DoB.ToString(),
                Title = "Date of birth",
                PlaceHolder = "MM/DD/YYYY",
                CurrentMode = AccessMode.Hidden
            });
            sectionParticipant.Elements.Add(new JbCheckBox()
            {
                Name = ElementsName.IsAdult.ToString(),
                Title = "This participant is an adult.",
                CurrentMode = AccessMode.Hidden
            });
            sectionParticipant.Elements.Add(new JbEmail()
            {
                Name = ElementsName.Email.ToString(),
                Title = "Email address",
                CurrentMode = AccessMode.Hidden
            });

            var items = typeof(SchoolGradeType).GetWithOrder().Select(g =>
                new JbElementItem
                {
                    Text = g.Text,
                    Value = ((int)EnumHelper.ParseEnum<SchoolGradeType>(g.Value)).ToString(),

                }).ToList();

            sectionParticipant.Elements.Add(new JbDropDown()
            {
                Name = "Grade",
                Title = "Grade",
                Items = items,
                CurrentMode = AccessMode.Hidden,
            });



            if (jbForm != null && jbForm.Elements.All(e => e.Name != SectionsName.ParticipantSection.ToString()))
                jbForm.Elements.Insert(0, sectionParticipant);

            return sectionParticipant;
        }

        public List<SelectListItem> GetSchoolGrades()
        {
            var result = new List<SelectListItem>();
            result.AddItemToFirst(DefaultDropdownItem.Empty);
            result.AddRange(typeof(SchoolGradeType).GetWithOrder().Select(g => new SelectListItem() { Text = g.Text, Value = ((int)EnumHelper.ParseEnum<SchoolGradeType>(g.Value)).ToString() }).ToList());

            return result;
        }

        public List<SelectListItem> GetGenderCategories()
        {
            var result = new List<SelectListItem>();
            result.AddRange(typeof(GenderCategories).GetWithOrder().Select(g => new SelectListItem() { Text = g.Text, Value = ((int)EnumHelper.ParseEnum<GenderCategories>(g.Value)).ToString() }).ToList().Where(e => e.Value != "3"));

            return result;
        }

        public object GetParticipantField(JbForm form, ElementsName name)
        {
            var section = form.GetElement<JbSection>(SectionsName.ParticipantSection.ToString());

            var result = section.Elements.Single(e => e.Name == name.ToString()).GetValue();

            return result;
        }

        public void ProfileSectionsGenerator(JbForm jbForm)
        {
            #region sectionPaticipant

            var sectionParticipant = new JbSection() { Name = SectionsName.ParticipantSection.ToString(), Title = "Participant Information", CurrentMode = AccessMode.Edit };

            sectionParticipant.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.FirstName.ToString(),
                Title = "First name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "First name is required"
                        }
                    }
                }
            });

            sectionParticipant.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.LastName.ToString(),
                Title = "Last name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Last name is required"
                        }
                    }
                }
            });

            var itemsGender = Enum.GetValues(typeof(GenderCategories)).Cast<GenderCategories>().Where(g => g != GenderCategories.All && g != GenderCategories.None).Select(v => new JbElementItem
            {
                Text = ((GenderCategories)v).ToDescription(),
                Value = v.ToString(),
            })
          .ToList();

            sectionParticipant.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.Gender.ToString(),
                Title = "Gender",
                Items = itemsGender
            });
            sectionParticipant.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.DoB.ToString(),
                Title = "Date of birth",
                PlaceHolder = "MM/DD/YYYY",
            });
            sectionParticipant.Elements.Add(new JbCheckBox()
            {
                Name = ElementsName.IsAdult.ToString(),
                Title = "This participant is an adult.",
                CurrentMode = AccessMode.Hidden
            });
            sectionParticipant.Elements.Add(new JbEmail()
            {
                Name = ElementsName.Email.ToString(),
                Title = "Email address",
            });

            var items = typeof(SchoolGradeType).GetWithOrder().Select(g =>
                new JbElementItem
                {
                    Text = g.Text,
                    Value = ((int)EnumHelper.ParseEnum<SchoolGradeType>(g.Value)).ToString(),

                }).ToList();

            sectionParticipant.Elements.Add(new JbDropDown()
            {
                Name = "Grade",
                Title = "Grade",
                Items = items,
                CurrentMode = AccessMode.Hidden,
            });


            jbForm.Elements.Add(sectionParticipant);

            #endregion

            #region sectionContact
            JbSection sectionContact = new JbSection() { Name = SectionsName.ContactSection.ToString(), Title = "Contact Information" };
            sectionContact.Elements.Add(new JbAddress() { Name = "Address", IsRequired = true, Title = "Address", AddressType = AddressType.US });
            sectionContact.Elements.Add(new JbPhone()
            {
                Name = ElementsName.Phone.ToString(),
                Title = "Phone number",
                IsRequired = true,
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }
                    }
                }
            });
            jbForm.Elements.Add(sectionContact);
            #endregion

            #region sectionParent
            var sectionParent = new JbSection() { Name = SectionsName.ParentGuardianSection.ToString(), Title = "Parent/Guardian Information" };
            sectionParent.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.FirstName.ToString(),
                Title = "First name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "First name is required"
                        }
                    }
                }
            });
            sectionParent.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.LastName.ToString(),
                Title = "Last name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Last name is required"
                        }
                    }
                }
            });
            sectionParent.Elements.Add(new JbEmail()
            {
                Name = ElementsName.Email.ToString(),
                Title = "Email address",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Email, IsChecked = true, Message = "invalid email address"
                        } ,
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Email is required"
                        }
                }
            });


            sectionParent.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.Gender.ToString(),
                Title = "Gender",
                Items = itemsGender
            });


            var itemsRelationship = Enum.GetValues(typeof(ParentRelationship)).Cast<ParentRelationship>().Select(v => new JbElementItem
            {
                Text = ((ParentRelationship)v).ToDescription(),
                Value = ((int)v).ToString()
            })
            .ToList();


            sectionParent.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.ParentRelationship.ToString(),
                Title = "Relationship",
                Items = itemsRelationship,
                CurrentMode = AccessMode.Hidden,
            });

            sectionParent.Elements.Add(new JbTextBox() { Name = "DateOfBirth", Title = "Date of birth" });
            sectionParent.Elements.Add(new JbPhone()
            {
                Name = ElementsName.Phone.ToString(),
                Title = "Cell phone",
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }
                    }
                }
            });
            sectionParent.Elements.Add(new JbTextBox() { Name = ElementsName.Occupation.ToString(), Title = "Occupation" });
            sectionParent.Elements.Add(new JbTextBox() { Name = ElementsName.Employer.ToString(), Title = "Employer" });
            sectionParent.Elements.Add(new JbAddress() { Name = "Address", Title = "Address", AddressType = AddressType.US, CurrentMode = AccessMode.Hidden });
            jbForm.Elements.Add(sectionParent);
            #endregion

            #region sectionParent2
            var sectionParent2 = new JbSection() { Name = SectionsName.Parent2GuardianSection.ToString(), Title = "Parent/Guardian 2 Information" };
            sectionParent2.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.FirstName.ToString(),
                Title = "First name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "First name is required"
                        }
                    }
                }
            });
            sectionParent2.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.LastName.ToString(),
                Title = "Last name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Last name is required"
                        }
                    }
                }
            });
            sectionParent2.Elements.Add(new JbEmail()
            {
                Name = ElementsName.Email.ToString(),
                Title = "Email address",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Email, IsChecked = true, Message = "Invalid email address"
                        } ,
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Email is required"
                        }
                }
            });

            sectionParent2.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.Gender.ToString(),
                Title = "Gender",
                Items = itemsGender
            });

            sectionParent2.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.ParentRelationship.ToString(),
                Title = "Relationship",
                Items = itemsRelationship,
                CurrentMode = AccessMode.Hidden,
            });

            sectionParent2.Elements.Add(new JbTextBox() { Name = "DateOfBirth", Title = "Date of birth" });
            sectionParent2.Elements.Add(new JbPhone()
            {
                Name = ElementsName.Phone.ToString(),
                Title = "Cell phone",
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }
                    }
                }
            });
            sectionParent2.Elements.Add(new JbTextBox() { Name = ElementsName.Occupation.ToString(), Title = "Occupation" });
            sectionParent2.Elements.Add(new JbTextBox() { Name = ElementsName.Employer.ToString(), Title = "Employer" });
            sectionParent2.Elements.Add(new JbAddress() { Name = "Address", Title = "Address", AddressType = AddressType.US, CurrentMode = AccessMode.Hidden });
            jbForm.Elements.Add(sectionParent2);
            #endregion

            #region sectionEmergency
            JbSection sectionEmergency = new JbSection() { Name = "EmergencyContactSection", Title = "Emergency Contact Information" };
            sectionEmergency.Elements.Add(new JbTextBox() { Name = "FirstName", IsRequired = true, Title = "First name" });
            sectionEmergency.Elements.Add(new JbTextBox() { Name = "LastName", IsRequired = true, Title = "Last name" });
            sectionEmergency.Elements.Add(new JbEmail()
            {
                Name = "EmailAddress",
                Title = "Email address",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Email, IsChecked = true, Message = "Invalid email address"
                        } ,
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Email is required"
                        }
                }
            });
            sectionEmergency.Elements.Add(new JbPhone()
            {
                Name = "HomePhone",
                Title = "Home phone",
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }
                    }
                }
            });
            sectionEmergency.Elements.Add(new JbPhone()
            {
                Name = "WorkPhone",
                Title = "Work phone",
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }
                    }
                }
            });

            var relationship = Enum.GetValues(typeof(Relationship)).Cast<Relationship>().Select(v => new JbElementItem
            {
                Text = ((Relationship)v).ToDescription(),
                Value = ((int)v).ToString()
            }).ToList();

            sectionEmergency.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.Relationship.ToString(),
                Title = "Relationship",
                Items = relationship,
                CurrentMode = AccessMode.Hidden,
            });
            jbForm.Elements.Add(sectionEmergency);
            #endregion

            #region sectionSchool
            var sectionSchool = new JbSection() { Name = "SchoolSection", Title = "School Information" };

            //var items = Enum.GetValues(typeof(SchoolGradeType)).Cast<SchoolGradeType>().Select(v => new JbElementItem
            //{
            //    Text = ((SchoolGradeType)v).ToDescription(),
            //    Value = ((int)v).ToString()
            //}).ToList();

            sectionSchool.Elements.Add(new JbDropDown()
            {
                Name = "Grade",
                Title = "Grade",
                Items = items,
            });
            sectionSchool.Elements.Add(new JbTextBox() { Name = "Name", Title = "Name" });
            sectionSchool.Elements.Add(new JbTextBox() { Name = "TeacherName", Title = "Teacher name" });
            sectionSchool.Elements.Add(new JbTextBox() { Name = ElementsName.Homeroom.ToString(), Title = "Homeroom", CurrentMode = AccessMode.Hidden });

            jbForm.Elements.Add(sectionSchool);
            #endregion

            #region sectionInsurance
            var sectionInsurance = new JbSection() { Name = SectionsName.InsuranceSection.ToString(), Title = "Insurance Information" };
            sectionInsurance.Elements.Add(new JbTextBox() { Name = ElementsName.CompanyName.ToString(), Title = "Company name" });
            sectionInsurance.Elements.Add(new JbTextBox() { Name = ElementsName.PolicyNumber.ToString(), Title = "Policy number" });
            sectionInsurance.Elements.Add(new JbPhone()
            {
                Name = ElementsName.CompanyPhone.ToString(),
                Title = "Phone number",
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }
                    }
                },
                CurrentMode = AccessMode.Hidden
            });

            jbForm.Elements.Add(sectionInsurance);
            #endregion

            #region sectionHealth
            var sectionHealth = new JbSection() { Name = SectionsName.HealthSection.ToString(), Title = "Medical/Allergy Information" };

            var itemsMedicalConditions = Enum.GetValues(typeof(YesNoDropDownOptions)).Cast<YesNoDropDownOptions>().Select(v => new JbElementItem
            {
                Text = ((YesNoDropDownOptions)v).ToDescription(),
                Value = ((int)v).ToString()
            }).ToList();

            sectionHealth.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.HasMedicalConditions.ToString(),
                Title = ElementsName.HasMedicalConditions.ToDescription(),
                Items = itemsMedicalConditions
            });

            sectionHealth.Elements.Add(new JbTextArea()
            {
                Name = ElementsName.MedicalConditions.ToString(),
                Title = ElementsName.MedicalConditions.ToDescription(),
                IsConditional = true,
                RelatedElementId = ElementsName.HasMedicalConditions.ToString(),
                RelatedElementValue = 1.ToString()
            });

            var itemsAllergiesInfo = Enum.GetValues(typeof(YesNoDropDownOptions)).Cast<YesNoDropDownOptions>().Select(v => new JbElementItem
            {
                Text = ((YesNoDropDownOptions)v).ToDescription(),
                Value = ((int)v).ToString()
            }).ToList();

            sectionHealth.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.HasAllergiesInfo.ToString(),
                Title = ElementsName.HasAllergiesInfo.ToDescription(),
                Items = itemsAllergiesInfo
            });

            sectionHealth.Elements.Add(new JbTextArea()
            {
                Name = ElementsName.AllergiesInfo.ToString(),
                Title = ElementsName.AllergiesInfo.ToDescription(),
                IsConditional = true,
                RelatedElementId = ElementsName.HasAllergiesInfo.ToString(),
                RelatedElementValue = 1.ToString()
            });

            var itemsSelfAdministeredMedication = Enum.GetValues(typeof(SelfAdministeredMedication)).Cast<SelfAdministeredMedication>().Select(v => new JbElementItem
            {
                Text = ((SelfAdministeredMedication)v).ToDescription(),
                Value = ((int)v).ToString()
            }).ToList();

            var itemsNutAllergy = Enum.GetValues(typeof(NutAllergy)).Cast<NutAllergy>().Select(v => new JbElementItem
            {
                Text = ((NutAllergy)v).ToDescription(),
                Value = ((int)v).ToString()
            }).ToList();

            sectionHealth.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.SelfAdministerMedication.ToString(),
                Title = "Self-administered medication",
                CurrentMode = AccessMode.Hidden,
                Items = itemsSelfAdministeredMedication
            });

            sectionHealth.Elements.Add(new JbTextArea()
            {
                Name = ElementsName.SelfAdministerMedicationDescription.ToString(),
                Title = "Self administer medication description",
                CurrentMode = AccessMode.Hidden,
                IsConditional = true,
                RelatedElementId = ElementsName.SelfAdministerMedication.ToString(),
                RelatedElementValue = 1.ToString()
            });

            sectionHealth.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.NutAllergy.ToString(),
                Title = "Nut allergy",
                CurrentMode = AccessMode.Hidden,
                Items = itemsNutAllergy
            });

            sectionHealth.Elements.Add(new JbTextArea()
            {
                Name = ElementsName.NutAllergyDescription.ToString(),
                Title = ElementsName.NutAllergyDescription.ToDescription(),
                CurrentMode = AccessMode.Hidden,
                IsConditional = true,
                RelatedElementId = ElementsName.NutAllergy.ToString(),
                RelatedElementValue = 1.ToString()
            });

            sectionHealth.Elements.Add(new JbTextBox() { Name = ElementsName.DoctorName.ToString(), Title = "Doctor name", CurrentMode = AccessMode.Hidden });
            sectionHealth.Elements.Add(new JbTextBox() { Name = ElementsName.DoctorPhone.ToString(), Title = "Doctor phone", CurrentMode = AccessMode.Hidden });
            sectionHealth.Elements.Add(new JbTextBox() { Name = ElementsName.Address.ToString(), Title = "Doctor location", CurrentMode = AccessMode.Hidden });

            jbForm.Elements.Add(sectionHealth);
            #endregion

            #region Photography/VideoRelease
            var sectionPhotography = new JbSection() { Name = SectionsName.PhotographySection.ToString(), Title = "Photography/Video Release Permission", CurrentMode = AccessMode.Hidden };

            var itemsPermissionPhotography = Enum.GetValues(typeof(PermissionPhotography)).Cast<PermissionPhotography>().Select(v => new JbElementItem
            {
                Text = ((PermissionPhotography)v).ToDescription(),
                Value = ((int)v).ToString()
            }).ToList();

            sectionPhotography.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.PermissionPhotography.ToString(),
                Title = "I give permission for my child to be photographed and videotaped during program activities.",
                CurrentMode = AccessMode.Hidden,
                IsRequired = true,
                Width = ElementWidth.Full,
                Items = itemsPermissionPhotography,
                Validations = new List<BaseElementValidator>
                    {
                        {
                            new BaseElementValidator()
                            {
                                Type = ElementValidationType.Required, IsChecked = true, Message = "Photography release permission is required."
                            }
                        }
                    }
            });

            sectionPhotography.Elements.Add(new JbSignature() { Name = ElementsName.Signature.ToString(), Title = "Signature", CurrentMode = AccessMode.Hidden });
            jbForm.Elements.Add(sectionPhotography);
            #endregion

            #region Miscellaneous
            var sectionMiscellaneous = new JbSection() { Name = SectionsName.Miscellaneous.ToString(), Title = "Miscellaneous", CurrentMode = AccessMode.Hidden };

            var itemsHearAboutUs = Enum.GetValues(typeof(HearAboutUs)).Cast<HearAboutUs>().Select(v => new JbElementItem
            {
                Text = ((HearAboutUs)v).ToDescription(),
                Value = ((int)v).ToString()
            })
          .ToList();

            sectionMiscellaneous.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.HearAboutUs.ToString(),
                Title = "How did you hear about us?",
                Items = itemsHearAboutUs,
                CurrentMode = AccessMode.Hidden,
            });

            jbForm.Elements.Add(sectionMiscellaneous);
            #endregion
        }

        public void ProfileSectionsGenerator2(JbForm jbForm)
        {
            jbForm.Elements.Add(GetParticipantSection());

            #region sectionSchool
            var sectionSchool = new JbSection() { Name = "SchoolSection", Title = "School Information" };

            var items = typeof(SchoolGradeType).GetWithOrder().Select(g =>
                new JbElementItem
                {
                    Text = g.Text,
                    Value = ((int)EnumHelper.ParseEnum<SchoolGradeType>(g.Value)).ToString(),

                }).ToList();

            sectionSchool.Elements.Add(new JbDropDown()
            {
                Name = "Grade",
                Title = "Grade",
                Items = items,
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Grade is required"
                        }
                    }
                }
            });

            sectionSchool.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.TeacherName.ToString(),
                Title = "Teacher name",
                IsRequired = true,
                Items = new List<JbElementItem>()
                    {
                       new JbElementItem()
                        {
                            Text= "Teacher not listed",
                            Value = "Teacher not listed",
                            ReadOnly = true
                        },
                        new JbElementItem("John Doe", "John Doe"),
                        new JbElementItem("Jane Doe", "Jane Doe"),
                    },
                Validations = new List<BaseElementValidator>
                    {
                        {
                            new BaseElementValidator()
                            {
                                Type = ElementValidationType.Required, IsChecked = true, Message = "Teacher name is required"
                            }
                        }
                    }
            });

            sectionSchool.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.TeacherNotListed.ToString(),
                Title = "Teacher not listed - write name here",
                IsConditional = true,
                RelatedElementId = ElementsName.TeacherName.ToString(),
                RelatedElementValue = "Teacher not listed"
            });

            sectionSchool.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.StandardDismissal.ToString(),
                Title = "Standard dismissal",
                IsRequired = true,
                Items = new List<JbElementItem>()
                    {
                        new JbElementItem()
                        {
                            Text = "Bus rider",
                            Value = "Bus rider",
                            ReadOnly = true
                        },
                        new JbElementItem("Car pick up", "Car pick up"),
                        new JbElementItem("Extended day", "Extended day"),
                        new JbElementItem("Walker", "Walker"),
                    },
                Validations = new List<BaseElementValidator>
                    {
                        {
                            new BaseElementValidator()
                            {
                                Type = ElementValidationType.Required, IsChecked = true, Message = "Standard dismissal is required"
                            }
                        }
                    }
            });

            sectionSchool.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.ChildBusNumber.ToString(),
                Title = ElementsName.ChildBusNumber.ToDescription(),
                Items = new List<JbElementItem>()
                    {
                        new JbElementItem()
                        {
                            Text = "Bus not listed",
                            Value = "Bus not listed",
                            ReadOnly = true
                        },
                        new JbElementItem("012 Yellow", "012 Yellow"),
                        new JbElementItem("023 White ", "023 White"),
                    },
                IsConditional = true,
                RelatedElementId = ElementsName.StandardDismissal.ToString(),
                RelatedElementValue = "Bus rider",

            });


            sectionSchool.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.ChildBusNumberNotListed.ToString(),
                Title = ElementsName.ChildBusNumberNotListed.ToDescription(),
                IsConditional = true,
                RelatedElementId = ElementsName.ChildBusNumber.ToString(),
                RelatedElementValue = "Bus not listed"
            });

            sectionSchool.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.DismissalFromEnrichment.ToString(),
                Title = "Dismissal from enrichment",
                IsRequired = true,
                Items = new List<JbElementItem>()
                    {
                        new JbElementItem("Extended day", "Extended day"),
                        new JbElementItem("Pick up", "Pick up"),
                    },
                Validations = new List<BaseElementValidator>
                    {
                        {
                            new BaseElementValidator()
                            {
                                Type = ElementValidationType.Required, IsChecked = true, Message = "Dismissal from enrichment is required"
                            }
                        }
                    }
            });
            sectionSchool.Elements.Add(new JbTextBox() { Name = ElementsName.SchoolName.ToString(), Title = "School name", CurrentMode = AccessMode.Hidden });
            sectionSchool.Elements.Add(new JbTextBox() { Name = ElementsName.Homeroom.ToString(), Title = "Homeroom", CurrentMode = AccessMode.Hidden });

            jbForm.Elements.Add(sectionSchool);
            #endregion

            #region sectionParent
            var sectionParent = new JbSection() { Name = SectionsName.ParentGuardianSection.ToString(), Title = "Parent/Guardian Information" };
            sectionParent.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.FirstName.ToString(),
                Title = "First name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "First name is required"
                        }
                    }
                }
            });
            sectionParent.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.LastName.ToString(),
                Title = "Last name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Last name is required"
                        }
                    }
                }
            });
            sectionParent.Elements.Add(new JbEmail()
            {
                Name = ElementsName.Email.ToString(),
                Title = "Email address",

            });

            sectionParent.Elements.Add(new JbPhone()
            {
                Name = ElementsName.Phone.ToString(),
                Title = "Primary phone",
                IsRequired = true,
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }

                    },
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Primary phone is required"
                        }
                    }
                }
            });
            sectionParent.Elements.Add(new JbPhone()
            {
                Name = ElementsName.AlternatePhone.ToString(),
                Title = "Alternate phone",
                IsRequired = true,
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }

                    },
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Alternate phone is required"
                        }
                    }
                }
            });
            var itemsGender = Enum.GetValues(typeof(GenderCategories)).Cast<GenderCategories>().Where(g => g != GenderCategories.All && g != GenderCategories.None).Select(v => new JbElementItem
            {
                Text = ((GenderCategories)v).ToDescription(),
                Value = v.ToString(),
            })
          .ToList();
            sectionParent.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.Gender.ToString(),
                Title = "Gender",
                CurrentMode = AccessMode.Hidden,
                Items = itemsGender
            });

            var itemsRelationship = Enum.GetValues(typeof(ParentRelationship)).Cast<ParentRelationship>().Select(v => new JbElementItem
            {
                Text = ((ParentRelationship)v).ToDescription(),
                Value = ((int)v).ToString()
            })
            .ToList();

            sectionParent.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.ParentRelationship.ToString(),
                Title = "Relationship",
                Items = itemsRelationship,
                CurrentMode = AccessMode.Hidden,
            });

            sectionParent.Elements.Add(new JbTextBox() { Name = "DateOfBirth", Title = "Date of birth", CurrentMode = AccessMode.Hidden });
            sectionParent.Elements.Add(new JbTextBox() { Name = ElementsName.Occupation.ToString(), Title = "Occupation", CurrentMode = AccessMode.Hidden });
            sectionParent.Elements.Add(new JbTextBox() { Name = ElementsName.Employer.ToString(), Title = "Employer", CurrentMode = AccessMode.Hidden });
            sectionParent.Elements.Add(new JbAddress() { Name = "Address", Title = "Address", AddressType = AddressType.US, CurrentMode = AccessMode.Hidden });
            jbForm.Elements.Add(sectionParent);
            #endregion

            #region sectionAuthorizedPickup1
            var sectionAuthorizedPickup1 = new JbSection() { Name = SectionsName.AuthorizedPickup1.ToString(), Title = "Additional Authorized Pickup 1 / Permission to Release Information" };
            sectionAuthorizedPickup1.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.FirstName.ToString(),
                Title = "First name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "First name is required"
                        }
                    }
                }
            });
            sectionAuthorizedPickup1.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.LastName.ToString(),
                Title = "Last name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Last name is required"
                        }
                    }
                }
            });
            sectionAuthorizedPickup1.Elements.Add(new JbPhone()
            {
                Name = ElementsName.Phone.ToString(),
                Title = "Primary phone",
                IsRequired = true,
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }
                    },
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Primary phone is required"
                        }
                    }
                }
            });
            sectionAuthorizedPickup1.Elements.Add(new JbPhone()
            {
                Name = ElementsName.AlternatePhone.ToString(),
                Title = "Alternate phone",
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }
                    }
                }
            });

            var relationship = Enum.GetValues(typeof(Relationship)).Cast<Relationship>().Select(v => new JbElementItem
            {
                Text = ((Relationship)v).ToDescription(),
                Value = ((int)v).ToString()
            })
               .ToList();

            sectionAuthorizedPickup1.Elements.Add(new JbEmail()
            {
                Name = ElementsName.Email.ToString(),
                Title = "Email address",
                CurrentMode = AccessMode.Hidden
            });

            sectionAuthorizedPickup1.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.Relationship.ToString(),
                Title = "Relationship",
                Items = relationship,
                CurrentMode = AccessMode.Hidden,
            });

            sectionAuthorizedPickup1.Elements.Add(new JbCheckBox()
            {
                Name = ElementsName.IsEmergencyContact.ToString(),
                Title = "This person is also an emergency contact.",
                CurrentMode = AccessMode.Hidden
            });
            jbForm.Elements.Add(sectionAuthorizedPickup1);
            #endregion

            #region sectionAuthorizedPickup2
            var sectionAuthorizedPickup2 = new JbSection() { Name = SectionsName.AuthorizedPickup2.ToString(), Title = "Additional Authorized Pickup 2 / Permission to Release Information" };
            sectionAuthorizedPickup2.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.FirstName.ToString(),
                Title = "First name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "First name is required"
                        }
                    }
                }
            });
            sectionAuthorizedPickup2.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.LastName.ToString(),
                Title = "Last name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Last name is required"
                        }
                    }
                }
            });
            sectionAuthorizedPickup2.Elements.Add(new JbPhone()
            {
                Name = ElementsName.Phone.ToString(),
                Title = "Primary phone",
                IsRequired = true,
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }
                    },
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Primary phone is required"
                        }
                    }
                }
            });
            sectionAuthorizedPickup2.Elements.Add(new JbPhone()
            {
                Name = ElementsName.AlternatePhone.ToString(),
                Title = "Alternate phone",
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }
                    }
                }
            });

            sectionAuthorizedPickup2.Elements.Add(new JbEmail()
            {
                Name = ElementsName.Email.ToString(),
                Title = "Email address",
                CurrentMode = AccessMode.Hidden
            });

            sectionAuthorizedPickup2.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.Relationship.ToString(),
                Title = "Relationship",
                Items = relationship,
                CurrentMode = AccessMode.Hidden,
            });

            sectionAuthorizedPickup2.Elements.Add(new JbCheckBox()
            {
                Name = ElementsName.IsEmergencyContact.ToString(),
                Title = "This person is also an emergency contact.",
                CurrentMode = AccessMode.Hidden
            });
            jbForm.Elements.Add(sectionAuthorizedPickup2);
            #endregion

            #region sectionEmergency
            JbSection sectionEmergency = new JbSection() { Name = "EmergencyContactSection", Title = "Emergency Contact Information" };
            sectionEmergency.Elements.Add(new JbTextBox() { Name = "FirstName", IsRequired = true, Title = "First name" });
            sectionEmergency.Elements.Add(new JbTextBox() { Name = "LastName", IsRequired = true, Title = "Last name" });
            sectionEmergency.Elements.Add(new JbPhone()
            {
                Name = ElementsName.Phone.ToString(),
                Title = "Primary phone",
                PhoneType = PhoneType.US,
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                     {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }

                    },
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Primary phone is required"
                        }
                    }
                }
            });
            sectionEmergency.Elements.Add(new JbPhone()
            {
                Name = ElementsName.AlternatePhone.ToString(),
                Title = "Alternate phone",
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }
                    }
                }
            });

            sectionEmergency.Elements.Add(new JbEmail()
            {
                Name = ElementsName.Email.ToString(),
                Title = "Email address",
                CurrentMode = AccessMode.Hidden
            });

            sectionEmergency.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.Relationship.ToString(),
                Title = "Relationship",
                Items = relationship,
                CurrentMode = AccessMode.Hidden,
            });

            //sectionEmergency.Elements.Add(new JbTextBox() { Name = "Relationship", Title = "Relationship", CurrentMode = AccessMode.Hidden });
            jbForm.Elements.Add(sectionEmergency);
            #endregion

            #region sectionHealth


            var sectionHealth = new JbSection() { Name = SectionsName.HealthSection.ToString(), Title = "Medical Information" };

            var itemsMedicalConditions = Enum.GetValues(typeof(YesNoDropDownOptions)).Cast<YesNoDropDownOptions>().Select(v => new JbElementItem
            {
                Text = ((YesNoDropDownOptions)v).ToDescription(),
                Value = ((int)v).ToString()
            }).ToList();

            sectionHealth.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.HasMedicalConditions.ToString(),
                Title = ElementsName.HasMedicalConditions.ToDescription(),
                Items = itemsMedicalConditions
            });

            sectionHealth.Elements.Add(new JbTextArea()
            {
                Name = ElementsName.MedicalConditions.ToString(),
                Title = ElementsName.MedicalConditions.ToDescription(),
                IsConditional = true,
                RelatedElementId = ElementsName.HasMedicalConditions.ToString(),
                RelatedElementValue = 1.ToString()
            });

            var itemsAllergiesInfo = Enum.GetValues(typeof(YesNoDropDownOptions)).Cast<YesNoDropDownOptions>().Select(v => new JbElementItem
            {
                Text = ((YesNoDropDownOptions)v).ToDescription(),
                Value = ((int)v).ToString()
            }).ToList();

            sectionHealth.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.HasAllergiesInfo.ToString(),
                Title = ElementsName.HasAllergiesInfo.ToDescription(),
                Items = itemsAllergiesInfo
            });

            sectionHealth.Elements.Add(new JbTextArea()
            {
                Name = ElementsName.AllergiesInfo.ToString(),
                Title = ElementsName.AllergiesInfo.ToDescription(),
                IsConditional = true,
                RelatedElementId = ElementsName.HasAllergiesInfo.ToString(),
                RelatedElementValue = 1.ToString()
            });

            var itemsSelfAdministeredMedication = Enum.GetValues(typeof(SelfAdministeredMedication)).Cast<SelfAdministeredMedication>().Select(v => new JbElementItem
            {
                Text = ((SelfAdministeredMedication)v).ToDescription(),
                Value = ((int)v).ToString()
            }).ToList();

            var itemsNutAllergy = Enum.GetValues(typeof(NutAllergy)).Cast<NutAllergy>().Select(v => new JbElementItem
            {
                Text = ((NutAllergy)v).ToDescription(),
                Value = ((int)v).ToString()
            }).ToList();

            sectionHealth.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.SelfAdministerMedication.ToString(),
                Title = "Self-administered medication",
                CurrentMode = AccessMode.Hidden,
                Items = itemsSelfAdministeredMedication
            });

            sectionHealth.Elements.Add(new JbTextArea()
            {
                Name = ElementsName.SelfAdministerMedicationDescription.ToString(),
                Title = "Self-administered medication description",
                CurrentMode = AccessMode.Hidden,
                IsConditional = true,
                RelatedElementId = ElementsName.SelfAdministerMedication.ToString(),
                RelatedElementValue = 1.ToString()
            });

            sectionHealth.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.NutAllergy.ToString(),
                Title = "Nut allergy",
                CurrentMode = AccessMode.Hidden,
                Items = itemsNutAllergy
            });

            sectionHealth.Elements.Add(new JbTextArea()
            {
                Name = ElementsName.NutAllergyDescription.ToString(),
                Title = ElementsName.NutAllergyDescription.ToDescription(),
                CurrentMode = AccessMode.Hidden,
                IsConditional = true,
                RelatedElementId = ElementsName.NutAllergy.ToString(),
                RelatedElementValue = 1.ToString()
            });

            sectionHealth.Elements.Add(new JbTextBox() { Name = ElementsName.DoctorName.ToString(), Title = "Doctor name", CurrentMode = AccessMode.Hidden });
            sectionHealth.Elements.Add(new JbTextBox() { Name = ElementsName.DoctorPhone.ToString(), Title = "Doctor phone", CurrentMode = AccessMode.Hidden });
            sectionHealth.Elements.Add(new JbTextBox() { Name = ElementsName.Address.ToString(), Title = "Doctor location", CurrentMode = AccessMode.Hidden });

            jbForm.Elements.Add(sectionHealth);
            #endregion

            #region sectionContact
            JbSection sectionContact = new JbSection() { Name = SectionsName.ContactSection.ToString(), Title = "Contact Information", CurrentMode = AccessMode.Hidden };
            sectionContact.Elements.Add(new JbAddress() { Name = "Address", IsRequired = true, Title = "Address", AddressType = AddressType.US });
            sectionContact.Elements.Add(new JbPhone()
            {
                Name = ElementsName.Phone.ToString(),
                Title = "Primary phone",
                IsRequired = true,
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }

                    },
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Primary phone is required"
                        }
                    }
                }
            });
            sectionContact.Elements.Add(new JbPhone()
            {
                Name = ElementsName.AlternatePhone.ToString(),
                Title = "Alternate phone",
                PhoneType = PhoneType.US,
            });
            jbForm.Elements.Add(sectionContact);
            #endregion

            #region sectionParent2
            var sectionParent2 = new JbSection() { Name = SectionsName.Parent2GuardianSection.ToString(), Title = "Parent/Guardian 2 Information", CurrentMode = AccessMode.Hidden };
            sectionParent2.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.FirstName.ToString(),
                Title = "First name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "First name is required"
                        }
                    }
                }
            });
            sectionParent2.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.LastName.ToString(),
                Title = "Last name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Last name is required"
                        }
                    }
                }
            });
            sectionParent2.Elements.Add(new JbEmail()
            {
                Name = ElementsName.Email.ToString(),
                Title = "Email address",
            });

            sectionParent2.Elements.Add(new JbPhone()
            {
                Name = ElementsName.Phone.ToString(),
                Title = "Primary phone",
                IsRequired = true,
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }

                    },
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Primary phone is required"
                        }
                    }
                }
            });
            sectionParent2.Elements.Add(new JbPhone()
            {
                Name = ElementsName.AlternatePhone.ToString(),
                Title = "Alternate phone",
                IsRequired = true,
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }

                    },
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Alternate phone is required"
                        }
                    }
                }
            });
            sectionParent2.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.Gender.ToString(),
                Title = "Gender",
                CurrentMode = AccessMode.Hidden,
                Items = itemsGender
            });

            //     var itemsRelationship = Enum.GetValues(typeof(Relationship)).Cast<Relationship>().Select(v => new JbElementItem
            //     {
            //         Text = ((Relationship)v).ToDescription(),
            //         Value = ((int)v).ToString()
            //     })
            //.ToList();

            sectionParent2.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.ParentRelationship.ToString(),
                Title = "Relationship",
                Items = itemsRelationship,
                CurrentMode = AccessMode.Hidden,
            });

            sectionParent2.Elements.Add(new JbTextBox() { Name = "DateOfBirth", Title = "Date of birth", CurrentMode = AccessMode.Hidden });
            sectionParent2.Elements.Add(new JbTextBox() { Name = ElementsName.Occupation.ToString(), Title = "Occupation", CurrentMode = AccessMode.Hidden });
            sectionParent2.Elements.Add(new JbTextBox() { Name = ElementsName.Employer.ToString(), Title = "Employer", CurrentMode = AccessMode.Hidden });
            sectionParent2.Elements.Add(new JbAddress() { Name = "Address", Title = "Address", AddressType = AddressType.US, CurrentMode = AccessMode.Hidden });
            jbForm.Elements.Add(sectionParent2);
            #endregion

            #region sectionAuthorizedPickup3
            var sectionAuthorizedPickup3 = new JbSection() { Name = SectionsName.AuthorizedPickup3.ToString(), Title = "Additional Authorized Pickup 3 / Permission to Release Information", CurrentMode = AccessMode.Hidden };
            sectionAuthorizedPickup3.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.FirstName.ToString(),
                Title = "First name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "First name is required"
                        }
                    }
                }
            });
            sectionAuthorizedPickup3.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.LastName.ToString(),
                Title = "Last name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Last name is required"
                        }
                    }
                }
            });
            sectionAuthorizedPickup3.Elements.Add(new JbPhone()
            {
                Name = ElementsName.Phone.ToString(),
                Title = "Primary phone",
                IsRequired = true,
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }
                    },
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Primary phone is required"
                        }
                    }
                }
            });
            sectionAuthorizedPickup3.Elements.Add(new JbPhone()
            {
                Name = ElementsName.AlternatePhone.ToString(),
                Title = "Alternate phone",
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }
                    }
                }
            });

            sectionAuthorizedPickup3.Elements.Add(new JbEmail()
            {
                Name = ElementsName.Email.ToString(),
                Title = "Email address",
                CurrentMode = AccessMode.Hidden
            });

            sectionAuthorizedPickup3.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.Relationship.ToString(),
                Title = "Relationship",
                Items = relationship,
                CurrentMode = AccessMode.Hidden,
            });

            sectionAuthorizedPickup3.Elements.Add(new JbCheckBox()
            {
                Name = ElementsName.IsEmergencyContact.ToString(),
                Title = "This person is also an emergency contact.",
                CurrentMode = AccessMode.Hidden
            });
            jbForm.Elements.Add(sectionAuthorizedPickup3);
            #endregion

            #region sectionAuthorizedPickup4
            var sectionAuthorizedPickup4 = new JbSection() { Name = SectionsName.AuthorizedPickup4.ToString(), Title = "Additional Authorized Pickup 4 / Permission to Release Information", CurrentMode = AccessMode.Hidden };
            sectionAuthorizedPickup4.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.FirstName.ToString(),
                Title = "First name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "First name is required"
                        }
                    }
                }
            });
            sectionAuthorizedPickup4.Elements.Add(new JbTextBox()
            {
                Name = ElementsName.LastName.ToString(),
                Title = "Last name",
                IsRequired = true,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Last name is required"
                        }
                    }
                }
            });
            sectionAuthorizedPickup4.Elements.Add(new JbPhone()
            {
                Name = ElementsName.Phone.ToString(),
                Title = "Primary phone",
                IsRequired = true,
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }
                    },
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Required, IsChecked = true, Message = "Primary phone is required"
                        }
                    }
                }
            });
            sectionAuthorizedPickup4.Elements.Add(new JbPhone()
            {
                Name = ElementsName.AlternatePhone.ToString(),
                Title = "Alternate phone",
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }
                    }
                }
            });

            sectionAuthorizedPickup4.Elements.Add(new JbEmail()
            {
                Name = ElementsName.Email.ToString(),
                Title = "Email address",
                CurrentMode = AccessMode.Hidden
            });

            sectionAuthorizedPickup4.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.Relationship.ToString(),
                Title = "Relationship",
                Items = relationship,
                CurrentMode = AccessMode.Hidden,
            });



            sectionAuthorizedPickup4.Elements.Add(new JbCheckBox()
            {
                Name = ElementsName.IsEmergencyContact.ToString(),
                Title = "This person is also an emergency contact.",
                CurrentMode = AccessMode.Hidden
            });
            jbForm.Elements.Add(sectionAuthorizedPickup4);
            #endregion

            #region sectionInsurance
            var sectionInsurance = new JbSection() { Name = SectionsName.InsuranceSection.ToString(), Title = "Insurance Information", CurrentMode = AccessMode.Hidden };
            sectionInsurance.Elements.Add(new JbTextBox() { Name = ElementsName.CompanyName.ToString(), Title = "Company name" });
            sectionInsurance.Elements.Add(new JbTextBox() { Name = ElementsName.PolicyNumber.ToString(), Title = "Policy number" });
            sectionInsurance.Elements.Add(new JbPhone()
            {
                Name = ElementsName.CompanyPhone.ToString(),
                Title = "Phone number",
                PhoneType = PhoneType.US,
                Validations = new List<BaseElementValidator>
                {
                    {
                        new BaseElementValidator()
                        {
                            Type = ElementValidationType.Numeric, IsChecked = true, Message = "Value must be integer"
                        }
                    }
                },
                CurrentMode = AccessMode.Hidden
            });
            jbForm.Elements.Add(sectionInsurance);
            #endregion

            #region Photography/VideoRelease
            var sectionPhotography = new JbSection() { Name = SectionsName.PhotographySection.ToString(), Title = "Photography/Video Release Permission", CurrentMode = AccessMode.Hidden };

            var itemsPermissionPhotography = Enum.GetValues(typeof(PermissionPhotography)).Cast<PermissionPhotography>().Select(v => new JbElementItem
            {
                Text = ((PermissionPhotography)v).ToDescription(),
                Value = ((int)v).ToString()
            }).ToList();

            sectionPhotography.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.PermissionPhotography.ToString(),
                Title = "I give permission for my child to be photographed and videotaped during program activities.",
                CurrentMode = AccessMode.Hidden,
                IsRequired = true,
                Width = ElementWidth.Full,
                Items = itemsPermissionPhotography,
                Validations = new List<BaseElementValidator>
                    {
                        {
                            new BaseElementValidator()
                            {
                                Type = ElementValidationType.Required, IsChecked = true, Message = "Photography release permission is required."
                            }
                        }
                    }
            });


            sectionPhotography.Elements.Add(new JbSignature() { Name = ElementsName.Signature.ToString(), Title = "Signature", CurrentMode = AccessMode.Hidden });
            jbForm.Elements.Add(sectionPhotography);
            #endregion

            #region Miscellaneous
            var sectionMiscellaneous = new JbSection() { Name = SectionsName.Miscellaneous.ToString(), Title = "Miscellaneous", CurrentMode = AccessMode.Hidden };

            var itemsHearAboutUs = Enum.GetValues(typeof(HearAboutUs)).Cast<HearAboutUs>().Select(v => new JbElementItem
            {
                Text = ((HearAboutUs)v).ToDescription(),
                Value = ((int)v).ToString()
            })
          .ToList();

            sectionMiscellaneous.Elements.Add(new JbDropDown()
            {
                Name = ElementsName.HearAboutUs.ToString(),
                Title = "How did you hear about us?",
                Items = itemsHearAboutUs,
                CurrentMode = AccessMode.Hidden,
            });

            jbForm.Elements.Add(sectionMiscellaneous);
            #endregion
        }

        public void FillParticipantSection(int playerId, JbForm jbForm)
        {
            var profile = Get(playerId);
            if (!jbForm.Elements.Any(e => e.Name == SectionsName.ParticipantSection.ToString()))
                jbForm.Elements.Insert(0, GetParticipantSection());
            var infoSection = jbForm.Elements.First(e => e.Name == SectionsName.ParticipantSection.ToString()) as JbSection;


            ((JbTextBox)infoSection.Elements.Single(e => e.Name == ElementsName.FirstName.ToString())).Value =
                profile.Contact.FirstName;
            ((JbTextBox)infoSection.Elements.Single(e => e.Name == ElementsName.LastName.ToString())).Value =
                profile.Contact.LastName;
            if (infoSection.Elements.Any(e => e.Name == ElementsName.Email.ToString()))
            {
                ((JbEmail)infoSection.Elements.Single(e => e.Name == ElementsName.Email.ToString())).Value =
                    profile.Contact.Email;
            }
            if (infoSection.Elements.Any(e => e.Name == ElementsName.Gender.ToString()))
            {
                ((JbDropDown)infoSection.Elements.Single(e => e.Name == ElementsName.Gender.ToString())).Value =
               profile.Gender.ToString();
            }
            if (profile.Contact.DoB.HasValue && infoSection.Elements.Any(e => e.Name == ElementsName.DoB.ToString()))
            {
                try
                {
                    infoSection.Elements.First(e => e.Name == ElementsName.DoB.ToString()).SetValue(profile.Contact.DoB.Value.ToShortDateString());
                    //((JbNumber)infoSection.Elements.Single(e => e.Name == ElementName.Age.ToString())).Value =
                    //    profile.Contact.Age.ToString();
                }
                catch { }
            }

            //if (jbForm.Elements.Any(e => e.Name == SectionsName.ParticipantSection.ToString()))
            //{
            //    infoSection.Title = jbForm.Elements.First(e => e.Name == SectionsName.ParticipantSection.ToString()).Title;
            //    if (jbForm.Elements.Count == 1)
            //    {
            //        jbForm.Elements[0] = infoSection;
            //    }
            //    else
            //    {
            //        jbForm.Elements.RemoveAll(e => e.Name == SectionsName.ParticipantSection.ToString());
            //        jbForm.Elements.Insert(0, infoSection);
            //    }

            //}
            //else
            //    jbForm.Elements.Insert(0, infoSection);
        }

        public void GenerateParticipantSection(JbForm jbForm)
        {
            JbSection infoSection = GetParticipantSection();
            jbForm.Elements.Insert(0, infoSection);
        }

        public IQueryable<PlayerProfile> GetAllWithPreapproval()
        {
            return _playerProfileRepository.GetList().Where(c => !string.IsNullOrEmpty(c.PaypalPreapprovalID));
        }

        public OperationStatus SetEducationalStatus(int id)
        {
            var player = this.Get(id);
            player.EducationalStatus = EducationalStatus.graduated;
            return _playerProfileRepository.Update(player);
        }
        public SchoolGradeType? GetGradeValueFromDescription(object gradeObj)
        {
            SchoolGradeType? grade = null;
            foreach (SchoolGradeType schoolGrade in Enum.GetValues(typeof(SchoolGradeType)))
            {
                if (gradeObj.ToString().ToLower().Equals(schoolGrade.ToDescription().ToLower()) || gradeObj.ToString().ToLower().Equals(schoolGrade.ToString().ToLower()))
                {
                    grade = schoolGrade;
                    break;
                }
            }

            return grade;
        }

        public OperationStatus UpdateInfoParent1(JbForm formData, List<PlayerProfile> parents)
        {
            var parentEmail = _formBusiness.GetParentEmail(formData);
            var parentFirstName = _formBusiness.GetParentFirstName(formData).Trim();
            var parentDoB = _formBusiness.GetParentDoB(formData);
            var parentLastName = _formBusiness.GetParentLastName(formData).Trim();
            var parentCellPhone = _formBusiness.GetParentCellPhone(formData);
            var parentEmployer = _formBusiness.GetParentEmployer(formData);
            var parentOccupation = _formBusiness.GetParentOccupation(formData);
            var parentGender = _formBusiness.GetParentGender(formData);

            foreach (var parent in parents)
            {
                parent.Contact.FirstName = parentFirstName;
                parent.Contact.LastName = parentLastName;
                parent.Contact.Email = parentEmail;
                parent.Contact.DoB = parentDoB;
                parent.Contact.Phone = parentCellPhone;
                parent.Contact.Occupation = parentOccupation;
                parent.Contact.Employer = parentEmployer;
                parent.Gender = parentGender != null ? parentGender.Value == Genders.Male
             ? GenderCategories.Male : GenderCategories.Female : GenderCategories.None;
            }

            return _playerProfileRepository.Save();
        }

        public OperationStatus CreatePlayerParent1(JbForm formData, OrderItem orderItem)
        {
            var parentGender = _formBusiness.GetParentGender(formData);
            var parentEmail = _formBusiness.GetParentEmail(formData);
            var parentFirstName = _formBusiness.GetParentFirstName(formData).Trim();
            var parentDoB = _formBusiness.GetParentDoB(formData);
            var parentLastName = _formBusiness.GetParentLastName(formData).Trim();
            var parentCellPhone = _formBusiness.GetParentCellPhone(formData);
            var parentEmployer = _formBusiness.GetParentEmployer(formData);
            var parentOccupation = _formBusiness.GetParentOccupation(formData);

            var profile = new PlayerProfile() { UserId = orderItem.Order.UserId };
            profile.Contact = new ContactPerson();
            profile.Status = PlayerProfileStatusType.Active;
            profile.Gender = parentGender != null ? parentGender.Value == Genders.Male
             ? GenderCategories.Male : GenderCategories.Female : GenderCategories.None; ;
            profile.Relationship = RelationshipType.Parent;
            profile.Contact.FirstName = parentFirstName;
            profile.Contact.LastName = parentLastName;
            profile.Contact.Email = parentEmail;
            profile.Contact.DoB = parentDoB;
            profile.Contact.Phone = parentCellPhone;
            profile.Contact.Occupation = parentOccupation;
            profile.Contact.Employer = parentEmployer;

            return Create(profile);
        }

        public OperationStatus UpdateInfoParent2(JbForm formData, List<PlayerProfile> parents)
        {
            var parent2Email = _formBusiness.GetParent2Email(formData);
            var parent2Gender = _formBusiness.GetParent2Gender(formData);
            var parent2CellPhone = _formBusiness.GetParent2CellPhone(formData);
            var parent2Occupation = _formBusiness.GetParent2Occupation(formData);
            var parent2Employer = _formBusiness.GetParent2Employer(formData);
            var parent2DoB = _formBusiness.GetParent2DoB(formData);
            var parent2FirstName = _formBusiness.GetParent2FirstName(formData).Trim();
            var parent2LastName = _formBusiness.GetParent2LastName(formData).Trim();

            foreach (var parent in parents)
            {
                parent.Contact.FirstName = parent2FirstName;
                parent.Contact.LastName = parent2LastName;
                parent.Contact.Email = parent2Email;
                parent.Contact.DoB = parent2DoB;
                parent.Contact.Phone = parent2CellPhone;
                parent.Contact.Occupation = parent2Occupation;
                parent.Contact.Employer = parent2Employer;
                parent.Gender = parent2Gender != null ? parent2Gender.Value == Genders.Male
             ? GenderCategories.Male : GenderCategories.Female : GenderCategories.None;
            }

            return _playerProfileRepository.Save();
        }

        public OperationStatus CreatePlayerParent2(JbForm formData, OrderItem orderItem)
        {
            var parent2Email = _formBusiness.GetParent2Email(formData);
            var parent2Gender = _formBusiness.GetParent2Gender(formData);
            var parent2CellPhone = _formBusiness.GetParent2CellPhone(formData);
            var parent2Occupation = _formBusiness.GetParent2Occupation(formData);
            var parent2Employer = _formBusiness.GetParent2Employer(formData);
            var parent2DoB = _formBusiness.GetParent2DoB(formData);
            var parent2FirstName = _formBusiness.GetParent2FirstName(formData).Trim();
            var parent2LastName = _formBusiness.GetParent2LastName(formData).Trim();

            var profile = new PlayerProfile() { UserId = orderItem.Order.UserId };
            profile.Contact = new ContactPerson();
            profile.Status = PlayerProfileStatusType.Active;
            profile.Gender = parent2Gender != null ? parent2Gender.Value == Genders.Male
             ? GenderCategories.Male : GenderCategories.Female : GenderCategories.None; ;
            profile.Relationship = RelationshipType.Parent;
            profile.Contact.FirstName = parent2FirstName;
            profile.Contact.LastName = parent2LastName;
            profile.Contact.Email = parent2Email;
            profile.Contact.DoB = parent2DoB;
            profile.Contact.Phone = parent2CellPhone;
            profile.Contact.Occupation = parent2Occupation;
            profile.Contact.Employer = parent2Employer;

            return Create(profile);
        }

        public bool IsExistPlayer(List<ContactPerson> listContactFamily, string playerFirstName, string playerLastName)
        {
            if (listContactFamily.Any(c => c.FirstName.EqualsTrim(playerFirstName) && c.LastName.EqualsTrim(playerLastName)))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public dynamic GetFamilyProfiles(int userId)
        {
            dynamic result = new System.Dynamic.ExpandoObject();

            var parents = GetList(userId).Where(u => u.Relationship == RelationshipType.Parent);
            var participants = GetList(userId).Where(u => u.Relationship == RelationshipType.Registrant);
            var family = _familyBusiness.GetFamily(userId);

            if (family == null)
            {
                family = _familyBusiness.AddUserFamily(userId);
            }
            var emergencyContacts = family != null && family.Contacts != null ? family.Contacts.Where(c => c.Type == FamilyContactType.Emergency && c.Status == FamilyContactStatusType.Active) : null;
            var authorizedPickups = family != null && family.Contacts != null ? family.Contacts.Where(c => c.Type == FamilyContactType.AuthorizedPickup && c.Status == FamilyContactStatusType.Active) : null;

            result.Parents = parents;
            result.Participants = participants;
            result.EmergencyContacts = emergencyContacts;
            result.AuthorizedPickups = authorizedPickups;

            return result;
        }

        public IQueryable<PlayerProfile> GetParents(int userId)
        {
            return GetList(userId).Where(u => u.Relationship == RelationshipType.Parent);
        }

        public IQueryable<PlayerProfile> GetParticipants(int userId)
        {
            return GetList(userId).Where(u => u.Relationship == RelationshipType.Registrant);
        }

        public dynamic GetAllFamiliesProfiles(List<int> userIds)
        {
            dynamic result = new System.Dynamic.ExpandoObject();
            var parents = GetAllParentsProfile(userIds);
            var participants = GetAllParticipantsProfile(userIds);
            var families = _familyBusiness.GetList().Where(d => userIds.Contains(d.UserId));

            var emergencyContacts = families != null && families.Select(f => f.Contacts) != null ? families.SelectMany(f => f.Contacts.Where(c => c.Type == FamilyContactType.Emergency && c.Status == FamilyContactStatusType.Active)) : null;
            var authorizedPickups = families != null && families.Select(f => f.Contacts) != null ? families.SelectMany(f => f.Contacts.Where(c => c.Type == FamilyContactType.AuthorizedPickup && c.Status == FamilyContactStatusType.Active)) : null;

            result.Parents = parents;
            result.Participants = participants;
            result.EmergencyContacts = emergencyContacts;
            result.AuthorizedPickups = authorizedPickups;

            return result;
        }

        public List<PlayerProfile> GetAllParticipantsProfile(List<int> userIds, bool includeContact = false, bool includeInfo = false)
        {
            var result = GetList().Where(p => userIds.Contains(p.UserId) && p.Relationship == RelationshipType.Registrant && p.Status != PlayerProfileStatusType.Deleted);

            if (includeContact)
                result = result.Include("Contact");
            else if (includeInfo)
                result = result.Include("Info");

            return result.ToList();
        }

        public List<PlayerProfile> GetAllParentsProfile(List<int> userIds, bool includeContact = false)
        {
            var result = GetList().Where(p => userIds.Contains(p.UserId) && p.Relationship == RelationshipType.Parent && p.Status != PlayerProfileStatusType.Deleted);

            if (includeContact)
                result = result.Include("Contact");

            return result.ToList();
        }

        public List<FamilyContact> GetAllEmergencyFamily(List<int> userIds, bool includeContact = false)
        {
            var families = _familyBusiness.GetList().Where(d => userIds.Contains(d.UserId));
            var emergencyContacts = families != null && families.Select(f => f.Contacts) != null ? families.SelectMany(f => f.Contacts.Where(c => c.Type == FamilyContactType.Emergency && c.Status == FamilyContactStatusType.Active)) : null;

            if (includeContact)
                emergencyContacts = emergencyContacts.Include("Contact");

            return emergencyContacts.ToList();
        }

        public List<FamilyContact> GetAllAuthorizePickupFamily(List<int> userIds, bool includeContact = false)
        {
            var families = _familyBusiness.GetList().Where(d => userIds.Contains(d.UserId));
            var authorizedPickups = families != null && families.Select(f => f.Contacts) != null ? families.SelectMany(f => f.Contacts.Where(c => c.Type == FamilyContactType.AuthorizedPickup && c.Status == FamilyContactStatusType.Active)) : null;

            if (includeContact)
                authorizedPickups = authorizedPickups.Include("Contact");

            return authorizedPickups.ToList();
        }

        public List<Family> GetAllInsuranceFamily(List<int> userIds)
        {
            var families = _familyBusiness.GetList().Where(d => userIds.Contains(d.UserId)).ToList();
            return families;
        }

        public OperationStatus DeletePlayerProfile(int id)
        {
            var result = new OperationStatus();
            var player = Get(id);
            var orderItemBusiness = _orderItemBusiness;

            if (player.Relationship == RelationshipType.Registrant)
            {
                var playerOrderitems = orderItemBusiness.GetList().Where(o => o.PlayerId == player.Id).ToList();

                if (!playerOrderitems.Any() && playerOrderitems.Count() == 0)
                {
                    result = Delete(player);
                }
                else
                {
                    result.Status = false;
                    result.Message = "This participant has order and you cannot delete.";
                }
            }
            else
            {
                result = Delete(player);
            }

            return result;
        }

        #region Parent report

        public JbForm ParentReportSectionGenerator(JbForm jbForm, long? seasonId, int clubId)
        {
            jbForm.Elements.Add(GenerateParentReportGeneralSection());
            jbForm.Elements.Add(GenerateParentReportParticipantSection());
            jbForm.Elements.Add(GenerateParentReportParentSection());
            jbForm.Elements.Add(GenerateParentReportAuthorizePickupSection());
            jbForm.Elements.Add(GenerateParentReportEmergencyContactSection());
            jbForm.Elements.Add(GenerateParentReportInsuranceSection());
            jbForm.Elements.Add(GenerateParentReportChargesSection(clubId));
            jbForm.Elements.Add(GenerateParentReportDiscountCouponSection(seasonId, clubId));
            jbForm.Elements.Add(GenerateParentReportChessTournamentSection());
            jbForm.Elements.Add(GenerateParentReportMoreInfoSection(seasonId, clubId));

            return jbForm;
        }


        private JbSection GenerateParentReportParticipantSection()
        {
            var moreSection = new JbSection
            {
                Name = SectionsName.ParticipantSection.ToString(),
                Title = "Child/Participant",
                Elements = new List<IJbBaseElement>
                {
                    new JbCheckBox { Name = "%Personal", Title = "Personal" },
                    new JbCheckBox { Name = ElementsName.ParticipantId.ToString(), Title = ElementsName.ParticipantId.ToDescription() },
                    new JbCheckBox { Name = ElementsName.FirstName.ToString(), Title = ElementsName.FirstName.ToDescription() },
                    new JbCheckBox { Name = ElementsName.LastName.ToString(), Title = ElementsName.LastName.ToDescription() },
                    new JbCheckBox { Name = ElementsName.Grade.ToString(), Title = ElementsName.Grade.ToDescription() },
                    new JbCheckBox { Name = ElementsName.DoB.ToString(), Title = ElementsName.DoB.ToDescription() },
                    new JbCheckBox { Name = ElementsName.Email.ToString(), Title = ElementsName.Email.ToDescription() },
                    new JbCheckBox { Name = ElementsName.Gender.ToString(), Title = ElementsName.Gender.ToDescription() },
                    new JbCheckBox { Name = ElementsName.PermissionPhotography.ToString(), Title = ElementsName.PermissionPhotography.ToDescription() },
                    new JbCheckBox { Name = "%School", Title = "School" },
                    new JbCheckBox { Name = ElementsName.SchoolName.ToString(), Title = ElementsName.SchoolName.ToDescription() },
                    new JbCheckBox { Name = ElementsName.Homeroom.ToString(), Title = ElementsName.Homeroom.ToDescription() },
                    new JbCheckBox { Name = "%Medical", Title = "Medical" },
                    new JbCheckBox { Name = ElementsName.HasMedicalConditions.ToString(), Title = ElementsName.HasMedicalConditions.ToDescription() },
                    new JbCheckBox { Name = ElementsName.MedicalConditions.ToString(), Title = ElementsName.MedicalConditions.ToDescription() },
                    new JbCheckBox { Name = ElementsName.HasAllergiesInfo.ToString(), Title = ElementsName.HasAllergiesInfo.ToDescription() },
                    new JbCheckBox { Name = ElementsName.AllergiesInfo.ToString(), Title = ElementsName.AllergiesInfo.ToDescription() },
                    new JbCheckBox { Name = ElementsName.SelfAdministerMedication.ToString(), Title = ElementsName.SelfAdministerMedication.ToDescription() },
                    new JbCheckBox { Name = ElementsName.SelfAdministerMedicationDescription.ToString(), Title = ElementsName.SelfAdministerMedicationDescription.ToDescription() },
                    new JbCheckBox { Name = ElementsName.NutAllergy.ToString(), Title = ElementsName.NutAllergy.ToDescription() },
                    new JbCheckBox { Name = ElementsName.NutAllergyDescription.ToString(), Title = ElementsName.NutAllergyDescription.ToDescription() },
                    new JbCheckBox { Name = ElementsName.DoctorName.ToString(), Title = ElementsName.DoctorName.ToDescription() },
                    new JbCheckBox { Name = ElementsName.DoctorPhone.ToString(), Title = ElementsName.DoctorPhone.ToDescription() },
                    new JbCheckBox { Name = ElementsName.DoctorLocation.ToString(), Title = ElementsName.DoctorLocation.ToDescription() },

                }
            };

            return moreSection;
        }

        private JbSection GenerateParentReportParentSection()
        {
            var moreSection = new JbSection
            {
                Name = SectionsName.ParentGuardianSection.ToString(),
                Title = "Parent/Guardian",
                Elements = new List<IJbBaseElement>
                {
                    new JbCheckBox { Name = "%Personal", Title = "Personal" },
                    new JbCheckBox { Name = ElementsName.ParentId.ToString(), Title = ElementsName.ParentId.ToDescription() },
                    new JbCheckBox { Name = ElementsName.FirstName.ToString(), Title = ElementsName.FirstName.ToDescription() },
                    new JbCheckBox { Name = ElementsName.LastName.ToString(), Title = ElementsName.LastName.ToDescription() },
                    new JbCheckBox { Name = ElementsName.Phone.ToString(), Title = ElementsName.Phone.ToDescription() },
                    new JbCheckBox { Name = ElementsName.Cell.ToString(), Title = ElementsName.Cell.ToDescription() },
                    new JbCheckBox { Name = ElementsName.Gender.ToString(), Title = ElementsName.Gender.ToDescription() },
                    new JbCheckBox { Name = ElementsName.Relationship.ToString(), Title = ElementsName.Relationship.ToDescription() },
                    new JbCheckBox { Name = ElementsName.Email.ToString(), Title = ElementsName.Email.ToDescription() },
                    new JbCheckBox { Name = ElementsName.DoB.ToString(), Title = ElementsName.DoB.ToDescription() },
                    new JbCheckBox { Name = "%Address", Title = "Address" },
                    new JbCheckBox { Name = ElementsName.AddressLine1.ToString(), Title = ElementsName.AddressLine1.ToDescription() },
                    new JbCheckBox { Name = ElementsName.AddressLine2.ToString(), Title = ElementsName.AddressLine2.ToDescription() },
                    new JbCheckBox { Name = ElementsName.Country.ToString(), Title = ElementsName.Country.ToDescription() },
                    new JbCheckBox { Name = ElementsName.City.ToString(), Title = ElementsName.City.ToDescription() },
                    new JbCheckBox { Name = ElementsName.State.ToString(), Title = ElementsName.State.ToDescription() },
                    new JbCheckBox { Name = ElementsName.Zip.ToString(), Title = ElementsName.Zip.ToDescription() },
                    new JbCheckBox { Name = "%Occupation", Title = "Occupation" },
                    new JbCheckBox { Name = ElementsName.Employer.ToString(), Title = ElementsName.Employer.ToDescription() },
                    new JbCheckBox { Name = ElementsName.Occupation.ToString(), Title = ElementsName.Occupation.ToDescription() },
                }
            };
            return moreSection;
        }

        private JbSection GenerateParentReportAuthorizePickupSection()
        {
            var moreSection = new JbSection
            {
                Name = SectionsName.AuthorizedPickup.ToString(),
                Title = "Authorized Pickup",
                Elements = new List<IJbBaseElement>
                {
                    new JbCheckBox { Name = ElementsName.FirstName.ToString(), Title = ElementsName.FirstName.ToDescription() },
                    new JbCheckBox { Name = ElementsName.LastName.ToString(), Title = ElementsName.LastName.ToDescription() },
                    new JbCheckBox { Name = ElementsName.Phone.ToString(), Title = ElementsName.Phone.ToDescription() },
                    new JbCheckBox { Name = ElementsName.Cell.ToString(), Title = ElementsName.Cell.ToDescription() },
                    new JbCheckBox { Name = ElementsName.Relationship.ToString(), Title = ElementsName.Relationship.ToDescription() },
                    new JbCheckBox { Name = ElementsName.IsEmergencyContact.ToString(), Title = ElementsName.IsEmergencyContact.ToDescription() },
                }
            };

            return moreSection;
        }

        private JbSection GenerateParentReportEmergencyContactSection()
        {
            var moreSection = new JbSection
            {
                Name = SectionsName.EmergencyContactSection.ToString(),
                Title = "Emergency Contact",
                Elements = new List<IJbBaseElement>
                {
                    new JbCheckBox { Name = ElementsName.FirstName.ToString(), Title = ElementsName.FirstName.ToDescription() },
                    new JbCheckBox { Name = ElementsName.LastName.ToString(), Title = ElementsName.LastName.ToDescription() },
                    new JbCheckBox { Name = ElementsName.Phone.ToString(), Title = ElementsName.Phone.ToDescription() },
                    new JbCheckBox { Name = ElementsName.Cell.ToString(), Title = ElementsName.Cell.ToDescription() },
                    new JbCheckBox { Name = ElementsName.Email.ToString(), Title = ElementsName.Email.ToDescription() },
                    new JbCheckBox { Name = ElementsName.Relationship.ToString(), Title = ElementsName.Relationship.ToDescription() },
                }
            };

            return moreSection;
        }

        private JbSection GenerateParentReportInsuranceSection()
        {
            var moreSection = new JbSection
            {
                Name = SectionsName.InsuranceSection.ToString(),
                Title = "Insurance",
                Elements = new List<IJbBaseElement>
                {
                    new JbCheckBox { Name = ElementsName.CompanyName.ToString(), Title = ElementsName.CompanyName.ToDescription() },
                    new JbCheckBox { Name = ElementsName.PolicyNumber.ToString(), Title = ElementsName.PolicyNumber.ToDescription() },
                    new JbCheckBox { Name = ElementsName.CompanyPhone.ToString(), Title = ElementsName.CompanyPhone.ToDescription() },
                }
            };

            return moreSection;
        }

        private JbSection GenerateParentReportGeneralSection()
        {
            var moreSection = new JbSection
            {
                Name = SectionsName.General.ToString(),
                Title = "General",
                Elements = new List<IJbBaseElement>
                {
                    new JbCheckBox { Name = ElementsName.ProgramId.ToString(), Title = ElementsName.ProgramId.ToDescription(), Value = true},
                    new JbCheckBox { Name = ElementsName.Program.ToString(), Title = ElementsName.Program.ToDescription(), Value = true}
                }
            };

            return moreSection;
        }

        private JbSection GenerateParentReportMoreInfoSection(long? seasonId, int clubId)
        {
            var moreSection = _customReportBusiness.CreateMoreInfoSection();

            _customReportBusiness.SetAccessControllJbSection(moreSection, clubId);

            if (!seasonId.HasValue)
            {
                return moreSection;
            }

            return moreSection;
        }

        private JbSection GenerateParentReportChessTournamentSection()
        {
            var chessTournamentSection = new JbSection
            {
                Name = "ChessTournamentSection",
                Title = "Chess Tournament Info",
                Elements = new List<IJbBaseElement>
                {
                    new JbCheckBox { Name="SectionName",Title="Section name" },
                    new JbCheckBox { Name="ScheduleName",Title="Schedule" },
                }
            };

            return chessTournamentSection;
        }

        private JbSection GenerateParentReportDiscountCouponSection(long? seasonId, int clubId)
        {
            var discountCouponSection = _customReportBusiness.CreateDiscountCouponSection(seasonId, clubId);

            return discountCouponSection;
        }

        private JbSection GenerateParentReportChargesSection(int clubId)
        {
            var chargesSection = _customReportBusiness.CreateChargeSection(clubId);

            return chargesSection;
        }

        private IEnumerable<OrderChargeDiscount> GetAllSeasonCharges(long seasonId, int clubId)
        {
            var season = _seasonBusiness.Get(seasonId);

            var allOrdersCharges =
                _orderItemBusiness.GetOrderItems(clubId: clubId, seasonId: season.Id, isTestMode: season.Status == SeasonStatus.Test)
                    .SelectMany(oi => oi.OrderChargeDiscounts)
                    .Where(c => !c.IsDeleted && c.Category > 0 && c.Category != ChargeDiscountCategory.EntryFee).AsEnumerable()
                    .DistinctBy(c => new { c.Name, c.Category });

            return allOrdersCharges;
        }

        #endregion

        public PlayerProfile GetPlayerParticipantjbForm(JbForm jbForm, PlayerProfile player, JbUser user)
        {
            var formBusiness = _formBusiness;
            var contactPerson = player.Contact;
            if (contactPerson == null)
            {
                contactPerson = new ContactPerson();
            }
            if (player.Info == null)
            {
                player.Info = new PlayerInfo();
            }
            contactPerson.FirstName = formBusiness.GetPlayerFirstName(jbForm);

            contactPerson.LastName = formBusiness.GetPlayerLastName(jbForm);

            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.Email.ToString(), SectionsName.ParticipantSection.ToString()) != null)
                contactPerson.Email = formBusiness.GetPlayerEmail(jbForm);


            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.DoB.ToString(), SectionsName.ParticipantSection.ToString()) != null)
                contactPerson.DoB = formBusiness.GetParticipantDoB(jbForm);


            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.Gender.ToString(), SectionsName.ParticipantSection.ToString()) != null)
                player.Gender = formBusiness.GetParticipantGender(jbForm);

            player.UserId = user.Id;
            player.User = user;
            player.Contact = contactPerson;
            player.Relationship = RelationshipType.Registrant;

            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.Grade.ToString()) != null)
                player.Info.Grade = formBusiness.GetPlayerGrade(jbForm) != null ? formBusiness.GetPlayerGrade(jbForm).Value : 0;

            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.SchoolName.ToString(), SectionsName.SchoolSection.ToString()) != null || _jbFormBusiness.GetFormElement(jbForm, "Name") != null)
                player.Info.SchoolName = formBusiness.GetPlayerSchoolName(jbForm);

            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.Homeroom.ToString(), SectionsName.SchoolSection.ToString()) != null)
                player.Info.HomeRoom = formBusiness.GetPlayerHomeRoom(jbForm);

            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.AllergiesInfo.ToString(), SectionsName.HealthSection.ToString()) != null)
            {
                player.Info.Allergies = formBusiness.GetPlayerHasAllergeis(jbForm) != null && formBusiness.GetPlayerHasAllergeis(jbForm).Value ?
                      formBusiness.GetPlayerAllergies(jbForm) : string.Empty;
            }

            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.DoctorPhone.ToString(), SectionsName.HealthSection.ToString()) != null)
                player.Info.DoctorPhone = formBusiness.GetPlayerDoctorPhone(jbForm);

            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.DoctorName.ToString()) != null)
                player.Info.DoctorName = formBusiness.GetPlayerDoctorName(jbForm);

            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.Address.ToString(), SectionsName.HealthSection.ToString()) != null)
                player.Info.DoctorLocation = formBusiness.GetPlayerDoctorLocation(jbForm);

            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.MedicalConditions.ToString(), SectionsName.HealthSection.ToString()) != null)
            {
                player.Info.SpecialNeeds = formBusiness.GetPlayerHasSpecialNeeds(jbForm) != null && formBusiness.GetPlayerHasSpecialNeeds(jbForm).Value ?
                      formBusiness.GetPlayerMedicalConditions(jbForm) : string.Empty;
            }

            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.SelfAdministerMedication.ToString(), SectionsName.HealthSection.ToString()) != null)
                player.Info.SelfAdministerMedication = formBusiness.GetPlayerSelfAdministerMedication(jbForm);

            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.SelfAdministerMedicationDescription.ToString(), SectionsName.HealthSection.ToString()) != null)
            {
                player.Info.SelfAdministerMedicationDescription = formBusiness.GetPlayerSelfAdministerMedication(jbForm) != null && formBusiness.GetPlayerSelfAdministerMedication(jbForm).Value ?
                      formBusiness.GetPlayerSelfAdministerMedicationDescription(jbForm) : string.Empty;
            }

            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.NutAllergy.ToString(), SectionsName.HealthSection.ToString()) != null)
                player.Info.NutAllergy = formBusiness.GetPlayerNutAllergy(jbForm);

            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.NutAllergyDescription.ToString(), SectionsName.HealthSection.ToString()) != null)
            {
                player.Info.NutAllergyDescription = formBusiness.GetPlayerNutAllergy(jbForm) != null && formBusiness.GetPlayerNutAllergy(jbForm).Value ?
                      formBusiness.GetPlayerNutAllergyDescription(jbForm) : string.Empty;
            }

            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.HasAllergiesInfo.ToString(), SectionsName.HealthSection.ToString()) != null)
                player.Info.HasAllergies = formBusiness.GetPlayerHasAllergeis(jbForm);

            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.HasMedicalConditions.ToString(), SectionsName.HealthSection.ToString()) != null)
                player.Info.HasSpecialNeeds = formBusiness.GetPlayerHasSpecialNeeds(jbForm);

            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.PermissionPhotography.ToString(), SectionsName.PhotographySection.ToString()) != null)
                player.Info.HasPermissionPhotography = formBusiness.GetPermissionPhotography(jbForm);


            return player;
        }

        public PlayerProfile GetPlayerParentjbForm(JbForm jbForm, PlayerProfile player, JbUser user)
        {
            var formBusiness = _formBusiness;

            var contactPerson = player.Contact;
            if (contactPerson == null)
            {
                contactPerson = new ContactPerson();
            }

            contactPerson.FirstName = formBusiness.GetParentFirstName(jbForm);
            contactPerson.LastName = formBusiness.GetParentLastName(jbForm);
            player.Relationship = RelationshipType.Parent;
            player.UserId = user.Id;
            player.User = user;
            player.Contact.Email = formBusiness.GetParent1Email(jbForm);
            player.Contact.DoB = formBusiness.GetParentDoB(jbForm);
            player.Gender = formBusiness.GetParent1Gender(jbForm);
            player.Contact.Relationship = formBusiness.GetParent1RelationshipEnum(jbForm);
            player.Contact.Phone = formBusiness.GetParentPhone(jbForm) != null ? formBusiness.GetParentPhone(jbForm).Replace("-", "") : string.Empty;
            player.Contact.Cell = formBusiness.GetParent1AlternatePhone(jbForm) != null ? formBusiness.GetParent1AlternatePhone(jbForm).Replace("-", "") : string.Empty;
            player.Contact.Occupation = formBusiness.GetParentOccupation(jbForm);
            player.Contact.Employer = formBusiness.GetParentEmployer(jbForm);

            var addressLine1 = string.Empty;
            var addressLine2 = string.Empty;
            var city = string.Empty;
            var state = string.Empty;
            var Zip = string.Empty;
            var country = string.Empty;
            var addressType = AddressType.INTERNATIONAL;

            var parent1Address = formBusiness.GetParent1Address(jbForm);
            if (parent1Address != null)
            {
                var address = parent1Address;
                addressLine1 = address.AddressLine1;
                addressLine2 = address.AddressLine2;
                city = address.City;
                state = address.State;
                Zip = address.Zip;
                country = address.Country;
                addressType = address.AddressType;
            }

            if (country == null && addressType == AddressType.US)
            {
                country = "UnitedStates";
            }
            if (country == null && addressType == AddressType.Canada)
            {
                country = "Canada";
            }
            if (player.Contact.Address == null)
            {
                player.Contact.Address = new PostalAddress()
                {
                    City = city,
                    State = state,
                    Zip = Zip,
                    Street = addressLine1,
                    Street2 = addressLine2,
                    Country = country,
                    Address = addressLine1 + " " + addressLine2 + ", " + city + ", " + state + " " + Zip + ", " + country,
                };
            }
            else
            {
                player.Contact.Address.City = city;
                player.Contact.Address.State = state;
                player.Contact.Address.Zip = Zip;
                player.Contact.Address.Street = addressLine1;
                player.Contact.Address.Street2 = addressLine2;
                player.Contact.Address.Country = country;
                player.Contact.Address.Address = addressLine1 + " " + addressLine2 + ", " + city + ", " + state + " " + Zip + ", " + country;
            }

            return player;
        }
        public PlayerProfile GetPlayerParent2jbForm(JbForm jbForm, PlayerProfile player, JbUser user)
        {
            var formBusiness = _formBusiness;

            var contactPerson = player.Contact;
            if (contactPerson == null)
            {
                contactPerson = new ContactPerson();
            }

            contactPerson.FirstName = formBusiness.GetParent2FirstName(jbForm);
            contactPerson.LastName = formBusiness.GetParent2LastName(jbForm);
            player.Relationship = RelationshipType.Parent;
            player.UserId = user.Id;
            player.User = user;
            player.Contact.Email = formBusiness.GetParent2Email(jbForm);
            player.Contact.DoB = formBusiness.GetParent2DoB(jbForm);
            player.Gender = formBusiness.GetParent2GenderNew(jbForm);
            player.Contact.Relationship = formBusiness.GetParent2RelationshipEnum(jbForm);
            player.Contact.Phone = formBusiness.GetParent2Phone(jbForm) != null ? formBusiness.GetParent2Phone(jbForm).Replace("-", "") : string.Empty;
            player.Contact.Cell = formBusiness.GetParent2AlternatePhone(jbForm) != null ? formBusiness.GetParent2AlternatePhone(jbForm).Replace("-", "") : string.Empty;
            player.Contact.Occupation = formBusiness.GetParent2Occupation(jbForm);
            player.Contact.Employer = formBusiness.GetParent2Employer(jbForm);
            var addressLine1 = string.Empty;
            var addressLine2 = string.Empty;
            var city = string.Empty;
            var state = string.Empty;
            var zip = string.Empty;
            var country = string.Empty;
            var addressType = AddressType.INTERNATIONAL;

            var parent2Address = formBusiness.GetParent2Address(jbForm);
            if (parent2Address != null)
            {
                var address = formBusiness.GetParent2Address(jbForm);
                addressLine1 = address.AddressLine1;
                addressLine2 = address.AddressLine2;
                city = address.City;
                state = address.State;
                zip = address.Zip;
                country = address.Country;
                addressType = address.AddressType;
            }

            if (country == null && addressType == AddressType.US)
            {
                country = "UnitedStates";
            }
            if (country == null && addressType == AddressType.Canada)
            {
                country = "Canada";
            }
            if (player.Contact.Address == null)
            {
                player.Contact.Address = new PostalAddress()
                {
                    City = city,
                    State = state,
                    Zip = zip,
                    Street = addressLine1,
                    Street2 = addressLine2,
                    Country = country,
                    Address = addressLine1 + " " + addressLine2 + ", " + city + ", " + state + " " + zip + ", " + country,
                };
            }
            else
            {
                player.Contact.Address.City = city;
                player.Contact.Address.State = state;
                player.Contact.Address.Zip = zip;
                player.Contact.Address.Street = addressLine1;
                player.Contact.Address.Street2 = addressLine2;
                player.Contact.Address.Country = country;
                player.Contact.Address.Address = addressLine1 + " " + addressLine2 + ", " + city + ", " + state + " " + zip + ", " + country;
            }

            return player;
        }

        public FamilyContact GetAuthorizePickup1jbForm(JbForm jbForm, FamilyContact authorizedPickup, Family family)
        {

            var formBusiness = _formBusiness;
            authorizedPickup.Family = family;
            authorizedPickup.FamilyId = family.Id;
            authorizedPickup.Type = FamilyContactType.AuthorizedPickup;
            authorizedPickup.Contact.FirstName = formBusiness.GetAuthorizePickup1FirstName(jbForm);
            authorizedPickup.Contact.LastName = formBusiness.GetAuthorizePickup1LastName(jbForm);
            authorizedPickup.Contact.Phone = formBusiness.GetAuthorizePickup1PrimaryPhone(jbForm) != null ? formBusiness.GetAuthorizePickup1PrimaryPhone(jbForm).Replace("-", "") : string.Empty;
            authorizedPickup.Contact.Cell = formBusiness.GetAuthorizePickup1AlternatePhone(jbForm) != null ? formBusiness.GetAuthorizePickup1AlternatePhone(jbForm).Replace("-", "") : string.Empty;
            authorizedPickup.Relationship = formBusiness.GetAuthorizePickup1Relationship(jbForm);
            authorizedPickup.IsEmergencyContact = formBusiness.GetAuthorizePickup1IsEmergencyContact(jbForm);
            authorizedPickup.Contact.Email = formBusiness.GetFormValue(jbForm, SectionsName.AuthorizedPickup1, ElementsName.Email);

            return authorizedPickup;
        }

        public FamilyContact GetAuthorizePickup2jbForm(JbForm jbForm, FamilyContact authorizedPickup2, Family family)
        {

            var formBusiness = _formBusiness;
            authorizedPickup2.Family = family;
            authorizedPickup2.FamilyId = family.Id;
            authorizedPickup2.Type = FamilyContactType.AuthorizedPickup;
            authorizedPickup2.Contact.FirstName = formBusiness.GetAuthorizePickup2FirstName(jbForm);
            authorizedPickup2.Contact.LastName = formBusiness.GetAuthorizePickup2LastName(jbForm);
            authorizedPickup2.Contact.Phone = formBusiness.GetAuthorizePickup2PrimaryPhone(jbForm) != null ? formBusiness.GetAuthorizePickup2PrimaryPhone(jbForm).Replace("-", "") : string.Empty;
            authorizedPickup2.Contact.Cell = formBusiness.GetAuthorizePickup2AlternatePhone(jbForm) != null ? formBusiness.GetAuthorizePickup2AlternatePhone(jbForm).Replace("-", "") : string.Empty;
            authorizedPickup2.Relationship = formBusiness.GetAuthorizePickup2Relationship(jbForm);
            authorizedPickup2.IsEmergencyContact = formBusiness.GetAuthorizePickup2IsEmergencyContact(jbForm);
            authorizedPickup2.Contact.Email = formBusiness.GetFormValue(jbForm, SectionsName.AuthorizedPickup2, ElementsName.Email);

            return authorizedPickup2;
        }

        public FamilyContact GetAuthorizePickup3jbForm(JbForm jbForm, FamilyContact authorizedPickup3, Family family)
        {
            var formBusiness = _formBusiness;
            authorizedPickup3.Family = family;
            authorizedPickup3.FamilyId = family.Id;
            authorizedPickup3.Type = FamilyContactType.AuthorizedPickup;
            authorizedPickup3.Contact.FirstName = formBusiness.GetAuthorizePickup3FirstName(jbForm);
            authorizedPickup3.Contact.LastName = formBusiness.GetAuthorizePickup3LastName(jbForm);
            authorizedPickup3.Contact.Phone = formBusiness.GetAuthorizePickup3PrimaryPhone(jbForm) != null ? formBusiness.GetAuthorizePickup3PrimaryPhone(jbForm).Replace("-", "") : string.Empty;
            authorizedPickup3.Contact.Cell = formBusiness.GetAuthorizePickup3AlternatePhone(jbForm) != null ? formBusiness.GetAuthorizePickup3AlternatePhone(jbForm).Replace("-", "") : string.Empty;
            authorizedPickup3.Relationship = formBusiness.GetAuthorizePickup3Relationship(jbForm);
            authorizedPickup3.IsEmergencyContact = formBusiness.GetAuthorizePickup3IsEmergencyContact(jbForm);
            authorizedPickup3.Contact.Email = formBusiness.GetFormValue(jbForm, SectionsName.AuthorizedPickup3, ElementsName.Email);

            return authorizedPickup3;
        }

        public FamilyContact GetAuthorizePickup4jbForm(JbForm jbForm, FamilyContact authorizedPickup4, Family family)
        {
            var formBusiness = _formBusiness;

            authorizedPickup4.Family = family;
            authorizedPickup4.FamilyId = family.Id;
            authorizedPickup4.Type = FamilyContactType.AuthorizedPickup;
            authorizedPickup4.Contact.FirstName = formBusiness.GetAuthorizePickup4FirstName(jbForm);
            authorizedPickup4.Contact.LastName = formBusiness.GetAuthorizePickup4LastName(jbForm);
            authorizedPickup4.Contact.Phone = formBusiness.GetAuthorizePickup4PrimaryPhone(jbForm) != null ? formBusiness.GetAuthorizePickup4PrimaryPhone(jbForm).Replace("-", "") : string.Empty;
            authorizedPickup4.Contact.Cell = formBusiness.GetAuthorizePickup4AlternatePhone(jbForm) != null ? formBusiness.GetAuthorizePickup4AlternatePhone(jbForm).Replace("-", "") : string.Empty;
            authorizedPickup4.Relationship = formBusiness.GetAuthorizePickup4Relationship(jbForm);
            authorizedPickup4.IsEmergencyContact = formBusiness.GetAuthorizePickup4IsEmergencyContact(jbForm);
            authorizedPickup4.Contact.Email = formBusiness.GetFormValue(jbForm, SectionsName.AuthorizedPickup4, ElementsName.Email);

            return authorizedPickup4;
        }
        public FamilyContact GetEmergencyContactjbForm(JbForm jbForm, FamilyContact emergencyContact, Family family)
        {
            var formBusiness = _formBusiness;
            emergencyContact.Family = family;
            emergencyContact.FamilyId = family.Id;
            emergencyContact.Type = FamilyContactType.Emergency;
            emergencyContact.Contact.FirstName = formBusiness.GetEmergencyContactFirstName(jbForm);
            emergencyContact.Contact.Email = formBusiness.GetEmergencyContactEmail(jbForm);
            emergencyContact.Contact.LastName = formBusiness.GetEmergencyContactLastName(jbForm);
            emergencyContact.Contact.Phone = formBusiness.GetEmergencyContactPrimeryPhone(jbForm) != null ? formBusiness.GetEmergencyContactPrimeryPhone(jbForm).Replace("-", "") : string.Empty;
            emergencyContact.Contact.Cell = formBusiness.GetEmergencyContactAlternatePhone(jbForm) != null ? formBusiness.GetEmergencyContactAlternatePhone(jbForm).Replace("-", "") : string.Empty;
            emergencyContact.Contact.Work = formBusiness.GetEmergencyContactWorkPhone(jbForm) != null ? formBusiness.GetEmergencyContactWorkPhone(jbForm).Replace("-", "") : string.Empty;
            emergencyContact.Relationship = formBusiness.GetEmergencyContactRelationShip(jbForm);

            return emergencyContact;
        }
        public Family GetInsurancejbForm(JbForm jbForm, Family family)
        {
            var formBusiness = _formBusiness;

            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.CompanyName.ToString()) != null)
                family.InsuranceCompanyName = formBusiness.GetInsuranceCompanyName(jbForm);

            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.CompanyPhone.ToString()) != null)
                family.InsurancePhone = formBusiness.GetInsuranceCompanyPhone(jbForm) != null ? formBusiness.GetInsuranceCompanyPhone(jbForm).Replace("-", "") : string.Empty;

            if (_jbFormBusiness.GetFormElement(jbForm, ElementsName.PolicyNumber.ToString()) != null)
                family.InsurancePolicyNumber = formBusiness.GetInsurancePolicyNumber(jbForm);

            return family;
        }

        public OperationStatus SaveProfiles(List<PlayerProfile> playerList, List<FamilyContact> familyContactList, Family family)
        {
            _familyBusiness.Update(family);



            #region Save playerprofiles in db
            var playersList = new List<PlayerProfile>();
            foreach (var playerProfile in playerList)
            {
                var newplayerProfile = _dataContext.Set<PlayerProfile>().SingleOrDefault(i => i.Id == playerProfile.Id);

                if (newplayerProfile == null)
                {
                    playersList.Add(playerProfile);
                }
                else
                {
                    _dataContext.Entry(newplayerProfile).CurrentValues.SetValues(playerProfile);
                }
            }

            _dataContext.Set<PlayerProfile>().AddRange(playersList);
            #endregion


            #region Save FamilyContacts in db

            var newfamilyContacts = new List<FamilyContact>();
            foreach (var familyContact in familyContactList)
            {
                var contact = _dataContext.Set<FamilyContact>().SingleOrDefault(i => i.Id == familyContact.Id);

                if (contact == null)
                {
                    newfamilyContacts.Add(familyContact);
                }
                else
                {
                    _dataContext.Entry<FamilyContact>(contact).CurrentValues.SetValues(familyContact);
                }
            }

            _dataContext.Set<FamilyContact>().AddRange(newfamilyContacts);
            #endregion

            return _playerProfileRepository.Save();
        }

        public List<PlayerProfile> CompareParent1(PlayerProfile player, JbForm newRegForm, JbForm oldRegForm)
        {
            var formBusiness = _formBusiness;
            var parent1FirstName = string.Empty;
            var parent1LastName = string.Empty;
            var parent1FullName = string.Empty;

            if (oldRegForm == null)
            {
                parent1FirstName = formBusiness.GetParentFirstName(newRegForm);
                parent1LastName = formBusiness.GetParentLastName(newRegForm);
            }
            else
            {
                parent1FirstName = formBusiness.GetParentFirstName(oldRegForm);
                parent1LastName = formBusiness.GetParentLastName(oldRegForm);
            }

            parent1FullName = string.Format("{0} {1}", parent1FirstName, parent1LastName);

            var userParents = GetParents(player.UserId).ToList();
            var playerList = new List<PlayerProfile>();

            if (!string.IsNullOrEmpty(parent1FullName.Trim()))
            {
                var parent = userParents.FirstOrDefault();

                if (parent != null)
                {
                    if (!userParents.Any(c => c.Contact.FirstName.EqualsTrim(parent1FirstName) && c.Contact.LastName.EqualsTrim(parent1LastName) && c.Id != parent.Id))
                    {
                        //update parent1
                        playerList.Add(GetPlayerParentjbForm(newRegForm, parent, player.User));
                    }
                    else
                    {
                        var updateParent = userParents.FirstOrDefault(c => c.Contact.FirstName.EqualsTrim(parent1FirstName) && c.Contact.LastName.EqualsTrim(parent1LastName) && c.Id != parent.Id);
                        //update another parent
                        playerList.Add(GetPlayerParentjbForm(newRegForm, updateParent, player.User));
                    }
                }
                else //new parent1
                {
                    var newParent = new PlayerProfile();
                    newParent.Contact = new ContactPerson();
                    playerList.Add(GetPlayerParentjbForm(newRegForm, newParent, player.User));
                }
            }
            return playerList;
        }

        public List<PlayerProfile> CompareParent2(PlayerProfile player, JbForm newRegForm, JbForm oldRegForm)
        {
            var formBusiness = _formBusiness;
            var parent2FirstName = string.Empty;
            var parent2LastName = string.Empty;
            var parent2FullName = string.Empty;

            if (oldRegForm == null)
            {
                parent2FirstName = formBusiness.GetParent2FirstName(newRegForm);
                parent2LastName = formBusiness.GetParent2LastName(newRegForm);
            }
            else
            {
                parent2FirstName = formBusiness.GetParent2FirstName(oldRegForm);
                parent2LastName = formBusiness.GetParent2LastName(oldRegForm);
            }

            parent2FullName = string.Format("{0} {1}", parent2FirstName, parent2LastName);

            var userParents = GetParents(player.UserId).ToList();
            var playerList = new List<PlayerProfile>();

            if (!string.IsNullOrEmpty(parent2FullName.Trim()))
            {
                var parent2 = userParents.Count() > 1 ? userParents[1] : null;
                var parent1 = userParents.Count() == 1 ? userParents[0] : null;

                if (parent2 != null)
                {
                    if (!userParents.Any(c => c.Contact.FirstName.EqualsTrim(parent2FirstName) && c.Contact.LastName.EqualsTrim(parent2LastName) && c.Id != parent2.Id))
                    {
                        //update parent2
                        playerList.Add(GetPlayerParent2jbForm(newRegForm, parent2, player.User));
                    }
                    else
                    {
                        var updateParent2 = userParents.FirstOrDefault(c => c.Contact.FirstName.EqualsTrim(parent2FirstName) && c.Contact.LastName.EqualsTrim(parent2LastName) && c.Id != parent2.Id);
                        //update another parent
                        playerList.Add(GetPlayerParent2jbForm(newRegForm, updateParent2, player.User));

                    }

                }
                else //new parent2
                {
                    if (userParents.Any(c => c.Contact.FirstName.EqualsTrim(parent2FirstName) && c.Contact.LastName.EqualsTrim(parent2LastName)))
                    {
                        //update parent1
                        playerList.Add(GetPlayerParent2jbForm(newRegForm, parent1, player.User));
                    }
                    else
                    {
                        var newParent2 = new PlayerProfile();
                        newParent2.Contact = new ContactPerson();
                        playerList.Add(GetPlayerParent2jbForm(newRegForm, newParent2, player.User));
                    }

                }
            }
            return playerList;
        }

        public List<FamilyContact> CompareAuthorizePickup1(PlayerProfile player, JbForm newRegForm, JbForm oldRegForm)
        {
            var formBusiness = _formBusiness;
            var familyContactBusiness = _familyContactBusiness;
            var familyBusiness = _familyBusiness;

            var familyContactList = new List<FamilyContact>();

            var family = familyBusiness.GetFamily(player.UserId);

            if (family == null)
            {
                family = familyBusiness.AddUserFamily(player.UserId);
            }

            var authorizePickup1FirstName = string.Empty;
            var authorizePickup1LastName = string.Empty;
            var authorizePickup1FullName = string.Empty;
            var authorizedPickups = familyContactBusiness.GetAuthorizedPickups(player.UserId);

            if (oldRegForm == null)
            {
                authorizePickup1FirstName = formBusiness.GetAuthorizePickup1FirstName(newRegForm);
                authorizePickup1LastName = formBusiness.GetAuthorizePickup1LastName(newRegForm);
            }
            else
            {
                authorizePickup1FirstName = formBusiness.GetAuthorizePickup1FirstName(oldRegForm);
                authorizePickup1LastName = formBusiness.GetAuthorizePickup1LastName(oldRegForm);
            }

            authorizePickup1FullName = string.Format("{0} {1}", authorizePickup1FirstName, authorizePickup1LastName);

            if (!string.IsNullOrEmpty(authorizePickup1FullName.Trim()))
            {
                var authorizedPickup = authorizedPickups.FirstOrDefault();
                if (authorizedPickup != null)
                {
                    if (!authorizedPickups.Any(c => c.Contact.FirstName.EqualsTrim(authorizePickup1FirstName) && c.Contact.LastName.EqualsTrim(authorizePickup1LastName) && c.Id != authorizedPickup.Id))
                    {
                        familyContactList.Add(GetAuthorizePickup1jbForm(newRegForm, authorizedPickup, family));
                    }
                    else
                    {
                        var updateAuthorizePickup = authorizedPickups.FirstOrDefault(c => c.Contact.FirstName.EqualsTrim(authorizePickup1FirstName) && c.Contact.LastName.EqualsTrim(authorizePickup1LastName) && c.Id != authorizedPickup.Id);
                        //update another AuthorizePickup
                        familyContactList.Add(GetAuthorizePickup1jbForm(newRegForm, updateAuthorizePickup, family));

                    }
                }
                else
                {
                    var newAuthorizePickup = new FamilyContact();
                    newAuthorizePickup.Contact = new ContactPerson();
                    familyContactList.Add(GetAuthorizePickup1jbForm(newRegForm, newAuthorizePickup, family));
                }
            }
            return familyContactList;
        }

        public List<FamilyContact> CompareAuthorizePickup2(PlayerProfile player, JbForm newRegForm, JbForm oldRegForm)
        {
            var formBusiness = _formBusiness;
            var familyContactBusiness = _familyContactBusiness;
            var familyBusiness = _familyBusiness;

            var familyContactList = new List<FamilyContact>();

            var family = familyBusiness.GetFamily(player.UserId);

            if (family == null)
            {
                family = familyBusiness.AddUserFamily(player.UserId);
            }

            var authorizePickup2FirstName = string.Empty;
            var authorizePickup2LastName = string.Empty;
            var authorizePickup2FullName = string.Empty;
            var authorizedPickups = familyContactBusiness.GetAuthorizedPickups(player.UserId);

            if (oldRegForm == null)
            {
                authorizePickup2FirstName = formBusiness.GetAuthorizePickup2FirstName(newRegForm);
                authorizePickup2LastName = formBusiness.GetAuthorizePickup2LastName(newRegForm);
            }
            else
            {
                authorizePickup2FirstName = formBusiness.GetAuthorizePickup2FirstName(oldRegForm);
                authorizePickup2LastName = formBusiness.GetAuthorizePickup2LastName(oldRegForm);
            }

            authorizePickup2FullName = string.Format("{0} {1}", authorizePickup2FirstName, authorizePickup2LastName);


            if (!string.IsNullOrEmpty(authorizePickup2FullName.Trim()))
            {
                //var authorizedPickup = authorizedPickups.FirstOrDefault();
                var authorizedPickup2 = authorizedPickups.Count() > 1 ? authorizedPickups[1] : null;
                var authorizedPickup1 = authorizedPickups.Count() == 1 ? authorizedPickups[0] : null;

                if (authorizedPickup2 != null)
                {
                    if (!authorizedPickups.Any(c => c.Contact.FirstName.EqualsTrim(authorizePickup2FirstName) && c.Contact.LastName.EqualsTrim(authorizePickup2LastName) && c.Id != authorizedPickup2.Id))
                    {
                        familyContactList.Add(GetAuthorizePickup2jbForm(newRegForm, authorizedPickup2, family));
                    }
                    else
                    {
                        var updateAuthorizePickup = authorizedPickups.FirstOrDefault(c => c.Contact.FirstName.EqualsTrim(authorizePickup2FirstName) && c.Contact.LastName.EqualsTrim(authorizePickup2LastName) && c.Id != authorizedPickup2.Id);
                        //update another AuthorizePickup
                        familyContactList.Add(GetAuthorizePickup2jbForm(newRegForm, updateAuthorizePickup, family));

                    }
                }
                else
                {
                    if (authorizedPickups.Any(c => c.Contact.FirstName.EqualsTrim(authorizePickup2FirstName) && c.Contact.LastName.EqualsTrim(authorizePickup2LastName)))
                    {
                        var updateAuthorizePickup = authorizedPickups.FirstOrDefault(c => c.Contact.FirstName.EqualsTrim(authorizePickup2FirstName) && c.Contact.LastName.EqualsTrim(authorizePickup2LastName));

                        //update another AuthorizePickup
                        familyContactList.Add(GetAuthorizePickup2jbForm(newRegForm, updateAuthorizePickup, family));
                    }
                    else
                    {
                        var newAuthorizePickup = new FamilyContact();
                        newAuthorizePickup.Contact = new ContactPerson();
                        familyContactList.Add(GetAuthorizePickup2jbForm(newRegForm, newAuthorizePickup, family));
                    }
                }
            }
            return familyContactList;
        }

        public List<FamilyContact> CompareAuthorizePickup3(PlayerProfile player, JbForm newRegForm, JbForm oldRegForm)
        {
            var formBusiness = _formBusiness;
            var familyContactBusiness = _familyContactBusiness;
            var familyBusiness = _familyBusiness;

            var familyContactList = new List<FamilyContact>();

            var family = familyBusiness.GetFamily(player.UserId);

            if (family == null)
            {
                family = familyBusiness.AddUserFamily(player.UserId);
            }

            var authorizePickup3FirstName = string.Empty;
            var authorizePickup3LastName = string.Empty;
            var authorizePickup3FullName = string.Empty;
            var authorizedPickups = familyContactBusiness.GetAuthorizedPickups(player.UserId);

            if (oldRegForm == null)
            {
                authorizePickup3FirstName = formBusiness.GetAuthorizePickup3FirstName(newRegForm);
                authorizePickup3LastName = formBusiness.GetAuthorizePickup3LastName(newRegForm);
            }
            else
            {
                authorizePickup3FirstName = formBusiness.GetAuthorizePickup3FirstName(oldRegForm);
                authorizePickup3LastName = formBusiness.GetAuthorizePickup3LastName(oldRegForm);
            }

            authorizePickup3FullName = string.Format("{0} {1}", authorizePickup3FirstName, authorizePickup3LastName);


            if (!string.IsNullOrEmpty(authorizePickup3FullName.Trim()))
            {

                var authorizedPickup3 = authorizedPickups.Count() > 2 ? authorizedPickups[2] : null;

                if (authorizedPickup3 != null)
                {
                    if (!authorizedPickups.Any(c => c.Contact.FirstName.EqualsTrim(authorizePickup3FirstName) && c.Contact.LastName.EqualsTrim(authorizePickup3LastName) && c.Id != authorizedPickup3.Id))
                    {
                        familyContactList.Add(GetAuthorizePickup3jbForm(newRegForm, authorizedPickup3, family));
                    }
                    else
                    {
                        var updateAuthorizePickup = authorizedPickups.FirstOrDefault(c => c.Contact.FirstName.EqualsTrim(authorizePickup3FirstName) && c.Contact.LastName.EqualsTrim(authorizePickup3LastName) && c.Id != authorizedPickup3.Id);
                        //update another AuthorizePickup
                        familyContactList.Add(GetAuthorizePickup3jbForm(newRegForm, updateAuthorizePickup, family));
                    }
                }
                else
                {
                    if (authorizedPickups.Any(c => c.Contact.FirstName.EqualsTrim(authorizePickup3FirstName) && c.Contact.LastName.EqualsTrim(authorizePickup3LastName)))
                    {
                        var updateAuthorizePickup = authorizedPickups.FirstOrDefault(c => c.Contact.FirstName.EqualsTrim(authorizePickup3FirstName) && c.Contact.LastName.EqualsTrim(authorizePickup3LastName));

                        //update another AuthorizePickup
                        familyContactList.Add(GetAuthorizePickup3jbForm(newRegForm, updateAuthorizePickup, family));
                    }
                    else
                    {
                        var newAuthorizePickup = new FamilyContact();
                        newAuthorizePickup.Contact = new ContactPerson();
                        familyContactList.Add(GetAuthorizePickup3jbForm(newRegForm, newAuthorizePickup, family));
                    }
                }
            }

            return familyContactList;
        }

        public List<FamilyContact> CompareAuthorizePickup4(PlayerProfile player, JbForm newRegForm, JbForm oldRegForm)
        {
            var formBusiness = _formBusiness;
            var familyContactBusiness = _familyContactBusiness;
            var familyBusiness = _familyBusiness;

            var familyContactList = new List<FamilyContact>();

            var family = familyBusiness.GetFamily(player.UserId);

            if (family == null)
            {
                family = familyBusiness.AddUserFamily(player.UserId);
            }

            var authorizePickup4FirstName = string.Empty;
            var authorizePickup4LastName = string.Empty;
            var authorizePickup4FullName = string.Empty;
            var authorizedPickups = familyContactBusiness.GetAuthorizedPickups(player.UserId);

            if (oldRegForm == null)
            {
                authorizePickup4FirstName = formBusiness.GetAuthorizePickup4FirstName(newRegForm);
                authorizePickup4LastName = formBusiness.GetAuthorizePickup4LastName(newRegForm);
            }
            else
            {
                authorizePickup4FirstName = formBusiness.GetAuthorizePickup4FirstName(oldRegForm);
                authorizePickup4LastName = formBusiness.GetAuthorizePickup4LastName(oldRegForm);
            }

            authorizePickup4FullName = string.Format("{0} {1}", authorizePickup4FirstName, authorizePickup4LastName);


            if (!string.IsNullOrEmpty(authorizePickup4FullName.Trim()))
            {

                var authorizedPickup4 = authorizedPickups.Count() > 3 ? authorizedPickups[3] : null;

                if (authorizedPickup4 != null)
                {
                    if (!authorizedPickups.Any(c => c.Contact.FirstName.EqualsTrim(authorizePickup4FirstName) && c.Contact.LastName.EqualsTrim(authorizePickup4LastName) && c.Id != authorizedPickup4.Id))
                    {
                        familyContactList.Add(GetAuthorizePickup4jbForm(newRegForm, authorizedPickup4, family));
                    }
                    else
                    {
                        var updateAuthorizePickup = authorizedPickups.FirstOrDefault(c => c.Contact.FirstName.EqualsTrim(authorizePickup4FirstName) && c.Contact.LastName.EqualsTrim(authorizePickup4LastName) && c.Id != authorizedPickup4.Id);
                        //update another AuthorizePickup
                        familyContactList.Add(GetAuthorizePickup4jbForm(newRegForm, updateAuthorizePickup, family));

                    }
                }
                else
                {
                    if (authorizedPickups.Any(c => c.Contact.FirstName.EqualsTrim(authorizePickup4FirstName) && c.Contact.LastName.EqualsTrim(authorizePickup4LastName)))
                    {
                        var updateAuthorizePickup = authorizedPickups.FirstOrDefault(c => c.Contact.FirstName.EqualsTrim(authorizePickup4FirstName) && c.Contact.LastName.EqualsTrim(authorizePickup4LastName));
                        //update another AuthorizePickup
                        familyContactList.Add(GetAuthorizePickup4jbForm(newRegForm, updateAuthorizePickup, family));
                    }
                    else
                    {
                        var newAuthorizePickup = new FamilyContact();
                        newAuthorizePickup.Contact = new ContactPerson();
                        familyContactList.Add(GetAuthorizePickup4jbForm(newRegForm, newAuthorizePickup, family));
                    }
                }
            }
            return familyContactList;
        }

        public List<FamilyContact> CompareEmergencyContact(PlayerProfile player, JbForm newRegForm, JbForm oldRegForm)
        {
            var formBusiness = _formBusiness;
            var familyContactBusiness = _familyContactBusiness;
            var familyBusiness = _familyBusiness;

            var EmergencyContacts = familyContactBusiness.GetEmergencyContacts(player.UserId);

            var familyContactList = new List<FamilyContact>();

            var family = familyBusiness.GetFamily(player.UserId);

            if (family == null)
            {
                family = familyBusiness.AddUserFamily(player.UserId);
            }

            var emergencyContactFirstName = string.Empty;
            var emergencyContactLastName = string.Empty;
            var authorizePickup4FullName = string.Empty;
            var authorizedPickups = familyContactBusiness.GetAuthorizedPickups(player.UserId);

            if (oldRegForm == null)
            {
                emergencyContactFirstName = formBusiness.GetEmergencyContactFirstName(newRegForm);
                emergencyContactLastName = formBusiness.GetEmergencyContactLastName(newRegForm);

            }
            else
            {
                emergencyContactFirstName = formBusiness.GetEmergencyContactFirstName(oldRegForm);
                emergencyContactLastName = formBusiness.GetEmergencyContactLastName(oldRegForm);
            }

            var emergencyContactFullName = string.Format("{0} {1}", emergencyContactFirstName, emergencyContactLastName);


            if (!string.IsNullOrEmpty(emergencyContactFullName.Trim()))
            {
                var emergencyContact = EmergencyContacts.FirstOrDefault();

                if (emergencyContact != null)
                {
                    if (!EmergencyContacts.Any(c => c.Contact.FirstName.EqualsTrim(emergencyContactFirstName) && c.Contact.LastName.EqualsTrim(emergencyContactLastName) && c.Id != emergencyContact.Id))
                    {
                        familyContactList.Add(GetEmergencyContactjbForm(newRegForm, emergencyContact, family));
                    }
                    else
                    {
                        var updateEmergencyContact = EmergencyContacts.FirstOrDefault(c => c.Contact.FirstName.EqualsTrim(emergencyContactFirstName) && c.Contact.LastName.EqualsTrim(emergencyContactLastName) && c.Id != emergencyContact.Id);
                        //update another EmergencyContact
                        familyContactList.Add(GetEmergencyContactjbForm(newRegForm, updateEmergencyContact, family));

                    }

                }
                else
                {
                    var newEmergencyContact = new FamilyContact();
                    newEmergencyContact.Contact = new ContactPerson();
                    familyContactList.Add(GetEmergencyContactjbForm(newRegForm, newEmergencyContact, family));
                }
            }
            return familyContactList;
        }

    }
}
