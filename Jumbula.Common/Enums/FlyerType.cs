﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum FlyerType
    {
        [Description("No, I don't want a flyer ")]
        None,

        [Description("Yes, I have my own flyer")]
        Generate,

        [Description("automatically generate a flyer")]
        Upload
    }

}
