﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;

namespace Jumbula.Core.Domain
{
    public class EmailTemplate : BaseEntity, IProtectedResource
    {
        [StringLength(128)]
        public string TemplateName { get; set; }

        public string Template { get; set; }

        public TemplateType Type { get; set; }

        [ForeignKey("Club_Id")]
        public virtual Club Club { get; set; }

        public int Club_Id { get; set; }

        public bool DoesTheClubOwnIt(int clubId)
        {
            if (Club_Id == clubId)
            {
                return true;
            }

            return false;
        }

        public bool DoesTheParentOwnIt(int userId)
        {
            return false;
        }
    }
}
