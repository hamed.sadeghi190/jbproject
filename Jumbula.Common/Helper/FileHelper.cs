﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;


namespace Jumbula.Common.Helper
{
    public static class FileHelper
    {
        public static byte[] ConvertStreamToByte(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public static Stream ConvertStringToStream(string file)
        {
            var fileData = FixUploadedStringFile(file);

            return new MemoryStream(Convert.FromBase64String(fileData));

        }

        public static List<string[]> ConveretCsvToList(Stream file)
        {

            string sFileContents;

            using (var oStreamReader = new StreamReader(file))
            {
                sFileContents = oStreamReader.ReadToEnd();
            }

            var oCsvList = new List<string[]>();

            string[] sFileLines = sFileContents.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (var sFileLine in sFileLines)
            {
                oCsvList.Add(sFileLine.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries));
            }

            return oCsvList;
        }

        public static long GetSize(byte file)
        {
            throw new NotImplementedException();
        }

        public static byte[] Compress(byte[] data)
        {
            var output = new MemoryStream();
            using (var dstream = new DeflateStream(output, CompressionLevel.Optimal))
            {
                dstream.Write(data, 0, data.Length);
            }
            return output.ToArray();
        }

        public static byte[] Decompress(byte[] data)
        {
            MemoryStream input = new MemoryStream(data);
            MemoryStream output = new MemoryStream();
            using (DeflateStream dstream = new DeflateStream(input, CompressionMode.Decompress))
            {
                dstream.CopyTo(output);
            }
            return output.ToArray();
        }

        public static string GetMimeTypeFromExtension(string ext)
        {
            string mime;

            // Remove the dot from begining

            if (!string.IsNullOrEmpty(ext) && ext[0] == Constants.Constants.C_Dot)
            {
                ext = ext.Substring(1);
            }

            switch (ext)
            {
                case Constants.Constants.W_Png:
                    mime = string.Format(Constants.Constants.F_Image_MimeType, Constants.Constants.W_Png);
                    break;
                case Constants.Constants.W_Jpeg:
                    mime = string.Format(Constants.Constants.F_Image_MimeType, Constants.Constants.W_Jpeg);
                    break;
                case Constants.Constants.W_Jpg:
                    mime = string.Format(Constants.Constants.F_Image_MimeType, Constants.Constants.W_Jpg);
                    break;
                case Constants.Constants.W_Gif:
                    mime = string.Format(Constants.Constants.F_Image_MimeType, Constants.Constants.W_Gif);
                    break;
                default:
                    mime = Constants.Constants.E_Binary_MimeType;
                    break;
            }

            return mime;
        }

        public static string FixUploadedStringFile(string file)
        {
            var extensions = new List<string> { "jpeg", "png" };

            foreach (var item in extensions)
            {
                file = file.Replace($"data:{GetMimeTypeFromExtension(item)};base64,", string.Empty);
            }

            return file;
        }

    }
}
