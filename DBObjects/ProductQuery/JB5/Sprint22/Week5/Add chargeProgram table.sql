

CREATE TABLE [dbo].[ChargePrograms](
	[Charge_Id] [bigint] NOT NULL,
	[Program_Id] [bigint] NOT NULL,
 CONSTRAINT [PK_dbo.ChargePrograms] PRIMARY KEY CLUSTERED 
(
	[Charge_Id] ASC,
	[Program_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ChargePrograms]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ChargePrograms_dbo.Charges_Charge_Id] FOREIGN KEY([Charge_Id])
REFERENCES [dbo].[Charges] ([Id])
GO

ALTER TABLE [dbo].[ChargePrograms] CHECK CONSTRAINT [FK_dbo.ChargePrograms_dbo.Charges_Charge_Id]
GO

ALTER TABLE [dbo].[ChargePrograms]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ChargePrograms_dbo.Programs_Program_Id] FOREIGN KEY([Program_Id])
REFERENCES [dbo].[Programs] ([Id])
GO

ALTER TABLE [dbo].[ChargePrograms] CHECK CONSTRAINT [FK_dbo.ChargePrograms_dbo.Programs_Program_Id]
GO


