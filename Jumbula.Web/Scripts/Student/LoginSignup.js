﻿var $jbIconInput = $('.jb-icon-input');

function addFocused($this) {
    $this.parent().addClass('focused');
    $this.removeAttr('placeholder');
}
function removeFocused($this) {
    var placeholder = $this.attr('data-placeholder');
    $this.parent().removeClass('focused');
    $this.attr('placeholder', placeholder);
}

$jbIconInput.focus(function () {
    addFocused($(this));
}).focusout(function () {
    if ($(this).val() == '')
        removeFocused($(this));
});;


var signupBox = $('#signupBox'),
    loginBox = $('#loginBox'),
    forgetPassBox = $('#forgetPassBox');
$('#jumpToSignup').click(function () {
    signupBox.removeClass('deactive').addClass('active');
    loginBox.removeClass('active').addClass('deactive');
});
$('#jumpToLogin').click(function () {
    loginBox.removeClass('deactive').addClass('active');
    signupBox.removeClass('active').addClass('deactive');
});
$('#forgetPass').click(function () {
    forgetPassBox.removeClass('deactive').addClass('active');
    loginBox.removeClass('active').addClass('deactive');
});
$('#goBack').click(function () {
    forgetPassBox.removeClass('active').addClass('deactive');
    loginBox.removeClass('deactive').addClass('active');
});