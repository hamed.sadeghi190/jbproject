﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('parentMailingListReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        $scope.DonationsReportDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetParentMailingList',
                        type: "POST",
                    },

                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },

            serverSorting: false,
            serverPaging: true,
            pageSize: 10,
            resizable: true,
        });



        $scope.ParentMailingListGridOptions = {
            excel: {
                allPages: true,
                fileName: "ParentMailingList_Report.xlsx",
                proxyURL: "/Dashboard/PortalReport/Save"
            },
            excelExport: function (e) {
                cfpLoadingBar.complete();
            },
            dataSource: $scope.DonationsReportDataSource,
            resizable: true,
            pageable: {
                refresh: true,
                pageSizes:
                [
                    5, 10, 20, 30, 50, 100, 200
                ],
                buttonCount: 5,
            },
            dataBound: function () {
                var grid = $("#parentMailingListReportGrid").data("kendoGrid");
                customReportService.initializeDoubleScroll(grid);
            },
            scrollable: true,
            sortable: { mode: 'single' },
            columns: [
                 {
                     field: "UserName",
                     title: "User",
                     width: "200px"
                 },
                {
                    field: "ParentFirstName",
                    title: "Parent first name",
                    width: "200px"
                },
                 {
                     field: "ParentLastName",
                     title: "Last name",
                     width: "200px"
                 },
                {
                    field: "AddressLine1",
                    title: "Address line 1",
                    width: "200px",
                },
                {
                    field: "AddressLine2",
                    title: "Address line 2",
                },
                {
                    field: "City",
                    title: "City",
                },
                {
                    field: "State",
                    title: "State",
                },
                {
                    field: "Zip",
                    title: "Zip code",
                }
            ]
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#parentMailingListReportGrid").getKendoGrid().saveAsExcel();
        }
    }
    ]);

})();