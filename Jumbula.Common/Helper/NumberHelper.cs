﻿namespace Jumbula.Common.Helper
{
    public static class NumberHelper
    {
        public static string ToOrdinal(int num)
        {
            if (num <= 0) return num.ToString();

            switch (num % 100)
            {
                case 11:
                case 12:
                case 13:
                    return num + "th";
            }

            switch (num % 10)
            {
                case 1:
                    return num + "st";
                case 2:
                    return num + "nd";
                case 3:
                    return num + "rd";
                default:
                    return num + "th";
            }
        }

        public static string NumberToWords(int number)
        {
            if (number == 0) return "zero";

            if (number < 0) return $"minus {NumberToWords(-number)}";

            var words = string.Empty;

            if (number / 1000000 > 0)
            {
                words += $"{NumberToWords(number / 1000000)} million ";
                number %= 1000000;
            }

            if (number / 1000 > 0)
            {
                words += $"{NumberToWords(number / 1000)} thousand ";
                number %= 1000;
            }

            if (number / 100 > 0)
            {
                words += $"{NumberToWords(number / 100)} hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != string.Empty) words += " and ";

                var unitsMap = new[] { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                var tensMap = new[] { "", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                if (number < 20) words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if (number % 10 > 0) words += $"- {unitsMap[number % 10]}";
                }
            }

            return words;
        }

        public static string NumberToAdjective(int number)
        {
            if (number == 0) return "zero";

            if (number < 0) return $"minus {NumberToAdjective(-number)}";

            var words = string.Empty;

            if (number / 1000000 > 0)
            {
                words += $"{NumberToWords(number / 1000000)} million";
                number %= 1000000;
            }

            if (number / 1000 > 0)
            {
                words += $"{NumberToWords(number / 1000)} thousand";
                number %= 1000;
            }

            if (number / 100 > 0)
            {
                words += $"{NumberToWords(number / 100)} hundred";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != string.Empty) words += " and ";

                var unitsMap = new[] { "", "first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh", "twelfth", "thirteenth", "fourteenth", "fifteenth", "sixteenth", "seventeenth", "eighteenth", "nineteenth" };
                var tensMap = new[] { "", "tenth", "twentieth", "thirtieth", "fortieth", "fiftieth", "sixtieth", "seventieth", "eightieth", "ninetieth" };

                if (number < 20) words += unitsMap[number];
                else if (number / 10 != 0)
                {
                    words += tensMap[number / 10];
                    if (number % 10 > 0) words += $"- {unitsMap[number % 10]}";
                }
                else words += "th";
            }

            return words;
        }
    }
}
