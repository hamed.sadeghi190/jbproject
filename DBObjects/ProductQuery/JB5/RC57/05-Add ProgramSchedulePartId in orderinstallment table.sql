Alter table OrderInstallments Add [ProgramSchedulePartId] [int] NULL
Go
ALTER TABLE [dbo].[OrderInstallments]  WITH CHECK ADD  CONSTRAINT [FK_dbo.OrderInstallments_dbo.ProgramScheduleParts_ProgramSchedulePartId] FOREIGN KEY([ProgramSchedulePartId])
REFERENCES [dbo].[ProgramScheduleParts] ([Id])