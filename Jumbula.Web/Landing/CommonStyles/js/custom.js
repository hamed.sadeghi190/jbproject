$(function () {
    $(".menu-btn").on("click", function () {
        $("body").toggleClass("open");

        $(".menu").css("display", "block");
    });
    $('#nav-icon1').click(function () {
        $(this).toggleClass('open');
    });
});

// ----- Send Code Verification Message

function changeMask($this) {
    var flag = $('.flag-container .selected-flag .iti-flag');
    if (flag.hasClass('us')) {
        $('#phone').mask('(000) 000-0000');
    } else {
        $('#phone').mask('0#');
    }
    ;
}

changeMask();
$("body").delegate(".intl-tel-input .country-list .country ", "click", function () {
    changeMask($(this));
});
$("body").delegate("#phone", "click", function () {
    changeMask($(this));
});


