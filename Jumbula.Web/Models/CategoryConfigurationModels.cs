﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using Jumbula.Common.Enums;

namespace SportsClub.Models
{
    public class CategoryConfigInitializer
    {
        // All categoryConfiguration Set this Class

        public static CategoryConfigurationModels DefaultConfiguration(string campSearchKeyword, string classSearchKeyword, string clubSearchKeyword)
        {
            // get default configuration for (club) page

            // Get all section
            var pageconfig = GetAllSection();

            // config each component... 

            pageconfig.CampSearchKeyword = campSearchKeyword;
            pageconfig.ClassSearchKeyword = classSearchKeyword;
            pageconfig.ClubSearchKeyword = clubSearchKeyword;

            var contact = (ContactSection)pageconfig.Components.Single(x => x.GetType() == typeof(ContactSection));
            contact.WebsiteLabel = "Club Website";


            var jbevents = (JBEventsSection)pageconfig.Components.Single(x => x.GetType() == typeof(JBEventsSection));
            jbevents.Title = "Events";
            jbevents.Events.Add(new Event() { Name = "Camp", Type = EventType.Camp });
            jbevents.Events.Add(new Event() { Name = "Course", Type = EventType.Course });
            //jbevents.Events.Add(new Event() { Name = "Tournoment", Type = EventType.Tourney });

            var instructor = (InstructorSection)pageconfig.Components.Single(x => x.GetType() == typeof(InstructorSection));
            instructor.Title = "Coaches";
            instructor.EditTitle = "Manage Coaches";

            return pageconfig;

        }

        //public static CategoryConfigurationModels SchoolGetnerate()
        //{

        //    var pageconfig = GetAllSection();

        //    var contact = (ContactSection)pageconfig.Components.Single(x => x.GetType() == typeof(ContactSection));
        //    contact.WebsiteLabel = "School Website";


        //    var jbevents = (JBEventsSection)pageconfig.Components.Single(x => x.GetType() == typeof(JBEventsSection));
        //    jbevents.Title = "Events";
        //    jbevents.Events.Add(new Event() { Name = "Camp", Type = EventType.Camp });
        //    jbevents.Events.Add(new Event() { Name = "Course", Type = EventType.Course });
        //    //jbevents.Events.Add(new Event() { Name = "Tournoment", Type = EventType.Tourney });

        //    var instructor = (InstructorSection)pageconfig.Components.Single(x => x.GetType() == typeof(InstructorSection));
        //    instructor.Title = "Teachers";
        //    instructor.EditTitle = "Manage Teachers";

        //    return pageconfig;
        //}

        //public static CategoryConfigurationModels SportWithoutTourneyGetnerate()
        //{

        //    var pageconfig = GetAllSection();

        //    var contact = (ContactSection)pageconfig.Components.Single(x => x.GetType() == typeof(ContactSection));
        //    contact.WebsiteLabel = "Club Website";


        //    var jbevents = (JBEventsSection)pageconfig.Components.Single(x => x.GetType() == typeof(JBEventsSection));
        //    jbevents.Title = "Events";
        //    jbevents.Events.Add(new Event() { Name = "Camp", Type = EventType.Camp });
        //    jbevents.Events.Add(new Event() { Name = "Course", Type = EventType.Course });
        //    //jbevents.Events.Add(new Event() { Name = "Tournoment", Type = EventType.Tourney });

        //    var instructor = (InstructorSection)pageconfig.Components.Single(x => x.GetType() == typeof(InstructorSection));
        //    instructor.Title = "Coaches";
        //    instructor.EditTitle = "Manage Coaches";

        //    return pageconfig;
        //}

        //public static CategoryConfigurationModels SportByTourneyGetnerate()
        //{

        //    var pageconfig = GetAllSection();

        //    var contact = (ContactSection)pageconfig.Components.Single(x => x.GetType() == typeof(ContactSection));
        //    contact.WebsiteLabel = "Club Website";


        //    var jbevents = (JBEventsSection)pageconfig.Components.Single(x => x.GetType() == typeof(JBEventsSection));
        //    jbevents.Title = "Events";
        //    jbevents.Events.Add(new Event() { Name = "Camp", Type = EventType.Camp });
        //    jbevents.Events.Add(new Event() { Name = "Course", Type = EventType.Course });
        //    jbevents.Events.Add(new Event() { Name = "Tournoment", Type = EventType.Tourney });

        //    var instructor = (InstructorSection)pageconfig.Components.Single(x => x.GetType() == typeof(InstructorSection));
        //    instructor.Title = "Coach";
        //    instructor.EditTitle = "Manage Coach";

        //    return pageconfig;
        //}


        private static CategoryConfigurationModels GetAllSection()
        {
            var containerConfig = new CategoryConfigurationModels();

            containerConfig.Components.Add(new CoverSection() { Order = 1, Title = "Cover" });
            containerConfig.Components.Add(new JBEventsSection() { Order = 2, Title = "Events" });
            containerConfig.Components.Add(new GallerySection() { Order = 3, Title = "Gallery" });
            containerConfig.Components.Add(new InstructorSection() { Order = 4, Title = "Instructor" });
            containerConfig.Components.Add(new NewsSection() { Order = 5, Title = "News" });
            containerConfig.Components.Add(new AffiliationSection() { Order = 6, Title = "Affiliation" });
            containerConfig.Components.Add(new AboutSection() { Order = 7, Title = "About" });
            containerConfig.Components.Add(new MapSection() { Order = 8, Title = "Map" });
            containerConfig.Components.Add(new ContactSection() { Order = 9, Title = "Contact" });
            containerConfig.Components.Add(new WorkingHoursSection() { Order = 10, Title = "Working Hours" });

            return containerConfig;
        }
    }

    [XmlInclude(typeof(MapSection))]
    [XmlInclude(typeof(WorkingHoursSection))]
    [XmlInclude(typeof(CoverSection))]
    [XmlInclude(typeof(JBEventsSection))]
    [XmlInclude(typeof(GallerySection))]
    [XmlInclude(typeof(InstructorSection))]
    [XmlInclude(typeof(NewsSection))]
    [XmlInclude(typeof(AffiliationSection))]
    [XmlInclude(typeof(AboutSection))]
    [XmlInclude(typeof(ContactSection))]
    [Serializable]
    public class CategoryConfigurationModels
    {
        public CategoryConfigurationModels()
        {
            Components = new List<Component>();
        }

        public bool ShowSection(ContainerSection section)
        {
            // if we dont have this section in component list 
            // section should not be shown and return false
            // if section exist in our component list read show property value
            //to indicate show section or not

            switch (section)
            {
                case ContainerSection.CoverSection:
                    {
                        var coverSection = Components.SingleOrDefault(x => x.GetType() == typeof (CoverSection));
                        return coverSection != null && coverSection.Show;
                    }
                case ContainerSection.JBEventsSection:
                    {
                        var jbEventsSection = Components.SingleOrDefault(x => x.GetType() == typeof (JBEventsSection));
                        return jbEventsSection != null && jbEventsSection.Show;
                    }
                case ContainerSection.GallerySection:
                    {
                        var gallerySection = Components.SingleOrDefault(x => x.GetType() == typeof(GallerySection));
                        return gallerySection != null && gallerySection.Show;
                    }
                case ContainerSection.InstructorSection:
                    {
                        var instructorSection =  Components.SingleOrDefault(x => x.GetType() == typeof(InstructorSection));
                        return instructorSection != null && instructorSection.Show;

                    }
                case ContainerSection.NewsSection:
                    {
                        var newsSection =  Components.SingleOrDefault(x => x.GetType() == typeof(NewsSection));
                        return newsSection != null && newsSection.Show;
                    }
                case ContainerSection.AffiliationSection:
                    {
                        var affiliationSection  =  Components.SingleOrDefault(x => x.GetType() == typeof (AffiliationSection));
                        return affiliationSection != null && affiliationSection.Show;
                    }
                case ContainerSection.AboutSection:
                    {
                        var aboutSection = Components.SingleOrDefault(x => x.GetType() == typeof (AboutSection));
                        return aboutSection != null && aboutSection.Show;
                    }
                case ContainerSection.MapSection:
                    {
                        var mapSection = Components.SingleOrDefault(x => x.GetType() == typeof(MapSection));
                        return mapSection != null && mapSection.Show;
                    }
                case ContainerSection.ContactSection:
                    {
                        var contactSection = Components.SingleOrDefault(x => x.GetType() == typeof (ContactSection));
                        return contactSection != null && contactSection.Show;
                    }
                case ContainerSection.WorkingHoursSection:
                    {
                        var workingHourSection = Components.SingleOrDefault(x => x.GetType() == typeof(WorkingHoursSection));
                        return workingHourSection != null && workingHourSection.Show;
                    }
            }

            return true;
        }

        public List<Component> Components { get; set; }

        

        public string ClubSearchKeyword { get; set; }

        public string CampSearchKeyword { get; set; }

        public string ClassSearchKeyword { get; set; }

        public string Layout { get; set; }

    }

  

    public enum ContainerSection
    {
            CoverSection,
            JBEventsSection,
            GallerySection,
            InstructorSection,
            NewsSection,
            AffiliationSection,
            AboutSection,
            MapSection,
            ContactSection,
            WorkingHoursSection
    }

    [Serializable]
    public class Component
    {
        public Component()
        {
            // by default all added component will be show unless you change it
            Show = true;
        }

        public bool Show { get; set; }

        public string Title { get; set; }

        public int Order { get; set; }

    }

    [Serializable]
    public class CoverSection : Component
    {

    }
    [Serializable]
    public class JBEventsSection : Component
    {
        public JBEventsSection()
        {
            Events = new List<Event>();
        }

        public List<Event> Events { get; set; }
    }

    [Serializable]
    public class Event
    {
        public EventType Type { get; set; }

        public string Name { get; set; }
    }

    //imageGalery
    [Serializable]
    public class GallerySection : Component
    {

    }

    
    [Serializable]
    public class InstructorSection : Component
    {
        public string EditTitle { get; set; }
    }

    [Serializable]
    public class NewsSection : Component
    {
        public string BackgroundImage { get; set; }
    }

    [Serializable]
    public class AffiliationSection:Component
    {

    }

    [Serializable]
    public class AboutSection:Component
    {

    }
    [Serializable]
    public class MapSection : Component
    {

    }

    [Serializable]
    public class ContactSection : Component
    {
        public string WebsiteLabel { get; set; }
    }

    [Serializable]
    public class WorkingHoursSection : Component
    {

    }



}