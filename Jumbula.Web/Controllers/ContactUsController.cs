﻿using System.Web.Mvc;
using Jumbula.Web.Mvc.Controllers;
using SportsClub.Models;
using Constants = Jumbula.Common.Constants.Constants;
namespace Jumbula.Web.Controllers
{
    [AllowAnonymous]
    public class ContactUsController : JbBaseController
    {
        public virtual ActionResult ShowForm()
        {
            ContactUsFormViewModel model = new ContactUsFormViewModel();

            return PartialView("_Form", model);
        }

        [RequireHttps]
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public virtual ActionResult SubmitForm(ContactUsFormViewModel model)
        {
            bool status = false;
            string message = string.Empty;

            if (ModelState.IsValid)
            {
                try
                {
                    // Send email to support account
                    //Email.SendContactUsToSupport(model.Name, model.Email, model.Subject, model.Description);

                    // add notification to notification model
                    //NotificationHelper.Add(Constants.ContactUs_Form_Submit_Success_Title, Constants.ContactUs_Form_Submit_Success_Message);

                    status = true;
                }
                catch 
                {
                    status = false;

                    message = Constants.ContactUs_Form_Submit_Failed;
                }
            }

            return new JsonResult()
            {
                Data = new
                {
                    Status = status,
                    Message = message
                }
            };
        }
    }
}
