﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('viewCatalogsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', '$location', '$anchorScroll', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService, $location, $anchorScroll) {
        window.app.globalObjects.pageClass = 'catalog-settings-view';
        window.app.globalObjects.initializeView();
        $scope.stateParams = $stateParams;

        var baseUrl = window.location.origin + "/Dashboard/Catalog";
        $scope.MC.stateParams = $stateParams;

        $http.get(baseUrl + '/GetAllCatalogs/?clubId=' + $stateParams.providerId ).success(function (data, status, headers, config) {
            $scope.Model = data.Result;
            $scope.IsPartner = data.IsPartner;
            $location.hash($stateParams.catalogId);
            $anchorScroll();
        });


    }]);
})();
