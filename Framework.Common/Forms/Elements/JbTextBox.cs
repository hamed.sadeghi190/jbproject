using Jb.Framework.Common.Forms.JsonFormatter;
using Jb.Framework.Common.Forms.Validation;
using Newtonsoft.Json;

namespace Jb.Framework.Common.Forms
{
    [FluentValidation.Attributes.Validator(typeof(TextBoxValidator))]
    [JsonConverter(typeof(JbElementConverter))]
    public class JbTextBox : JbBaseElement, IInputElement
    {

        public virtual string Value
        {

            get;
            set;
        }

        public override object GetValue()
        {
            return Value == null ? string.Empty : Value;
        }


        public bool IsRequired { get; set; }


        public string PlaceHolder { get; set; }
    }
}