﻿using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Model.Email;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.WebConfig;
using MailChimp.Errors;
using reCAPTCHA.MVC;
using SportsClub.Models.Home;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Web.Mvc;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Controllers
{
    public class HomeController : JbBaseController
    {
        #region Fields
        private readonly IEmailBusiness _emailBusiness;
        #endregion

        #region Costructors
        public HomeController(IEmailBusiness emailBusiness)
        {
            _emailBusiness = emailBusiness;
        }
        #endregion


        [AllowAnonymous]
        public virtual ActionResult Index()
        {
            return View();
        }
        [AllowAnonymous]
        public virtual ActionResult FreeTrial()
        {
            return View();
        }
        /// <summary>
        /// The Channel File is a page with very long cache expiration time
        /// that does nothing but reference the Facebook Javascript file. 
        /// This is used to “greatly improves the performance of the JS SDK by addressing issues 
        /// with cross-domain communication in certain browsers”. 
        /// </summary>
        /// <returns>Channel View</returns>
        /// 
        [AllowAnonymous]
        public virtual ActionResult Channel()
        {
            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult Error(int? code)
        {
            //if (!code.HasValue) { return new RedirectResult(Url.Action("Index", "Home")); }

            if (code.HasValue == false)
            {
                return View("Error");
            }

            switch (code.Value)
            {
                case 404:
                    {
                        return View("Error_404");
                    }
                default:
                    return View("Error");
            }
        }

        [AllowAnonymous]
        public virtual ActionResult Agreement(string type)
        {
            if (string.IsNullOrEmpty(type) || !System.IO.File.Exists(Server.MapPath(string.Format("~/Views/Home/{0}.cshtml", type))))
            {
                return new RedirectResult(Url.Action("Index", "Home"));
            }

            return View(type);
        }

        [AllowAnonymous]
        public virtual ActionResult Solution()
        {
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public virtual ActionResult Pricing()
        {
            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult Portfolio()
        {
            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult Contact()
        {
            return View();
        }

        

        [AllowAnonymous]
        public virtual ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [CaptchaValidator]
        public virtual ActionResult Contact(ContactFormViewModel model)
        {
          
            if (ModelState.IsValid)
            {
                try
                {

                    // mail to support
                    //"~/Views/Shared/Email/ContactEmail.cshtml"
                    var body = this.RenderViewToString("~/Views/Shared/Email/ContactEmail.cshtml", model);
                    var from = new MailAddress(Constants.W_NoReplyEmailAddress);
                    var to = new MailAddress(Constants.W_JumbulaSupportEmail);
                    var cc = new List<MailAddress>() { new MailAddress("nacho@jumbula.com") };
                    Ioc.OldEmailBusiness.SendEmail(
                        from, to,
                        string.Format("Contact, from: {0} {1}", model.FName, model.LName),
                        body, true, null, cc, null, null
                        );


                    //go to mailchimp and Add a subscriber
                    MailChimpHelper.AddSubescriber(model.Email, model,
                        WebConfigHelper.MailChimpContactListID, WebConfigHelper.MailChimpAPIKey);

                }
                catch { }

                if (model.Newsletter)
                {
                    try
                    {
                        //go to mailchimp and Add a subscriber
                        MailChimpHelper.AddSubescriber(model.Email, new { Email = model.Email },
                            WebConfigHelper.MailChimpNewsletterListID, WebConfigHelper.MailChimpAPIKey);
                    }
                    catch { }
                }

                return Json(new { status = true }, JsonRequestBehavior.AllowGet);
            }

            return PartialView("_ContactPartial", model);
        }

        [HttpPost]
        [AllowAnonymous]
        public virtual ActionResult NewsletterSubscribe(string email)
        {
            if (!String.IsNullOrEmpty(email))
            {
                try
                {
                    //go to mailchimp and Add a subscriber
                    MailChimpHelper.AddSubescriber(email, new { Email = email },
                        WebConfigHelper.MailChimpNewsletterListID, WebConfigHelper.MailChimpAPIKey);
                }
                catch (MailChimpAPIException ex)
                {
                    return Json(new { status = false, code = ex.MailChimpAPIError.Code, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { status = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = false, message = "Email is required" }, JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        public virtual ActionResult ComparisonToActivityHero()
        {
            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult MoreConvincing()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public virtual JsonResult FreeTrial(FreeTrialViewModel model)
        {
            if (ModelState.IsValid)
            {

                // mail to support
                var body = this.RenderViewToString("~/Views/Shared/Email/FreeTrialEmaill.cshtml", model);
                var from = new MailAddress(Constants.W_NoReplyEmailAddress);
                var to = new MailAddress(Constants.W_JumbulaSupportEmail);
                var cc = new List<MailAddress>() { new MailAddress("nacho@jumbula.com") };
                Ioc.OldEmailBusiness.SendEmail(
                    from, to,
                    string.Format("Free trial request from: {0}", model.FullName),
                    body, true, null, cc, null, null
                    );

                try
                {
                    //go to mailchimp and Add a subscriber
                    MailChimpHelper.AddSubescriber(model.Email, model,
                        WebConfigHelper.MailChimpTrialListID, WebConfigHelper.MailChimpAPIKey);

                    return Json(new { status = true }, JsonRequestBehavior.AllowGet);
                }
                catch (MailChimpAPIException ex)
                {
                    return Json(new { status = false, code = ex.MailChimpAPIError.Code, message = ex.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { status = false, message = "Validation error." }, JsonRequestBehavior.AllowGet);
            }
        }

        public virtual ActionResult Blog()
        {
            return RedirectPermanent("http://blog.jumbula.com/");
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult SendRequestDemo(RequestDemoEmailParameterModel model)
        {
            _emailBusiness.Send(Common.Enums.EmailCategory.RequestDemo, model);

            return JsonNet(true) ;
        }

       
    }
}
