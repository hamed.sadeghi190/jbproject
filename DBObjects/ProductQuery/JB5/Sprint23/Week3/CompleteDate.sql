Alter Table Orders Add [CompleteDate] [datetime] NULL

Go

Update orders set CompleteDate = (select top(1) actiondate from OrderHistories where orderId = Orders.id and ([Action] = 5 Or [Action] = 4))

Go

Update orders set CompleteDate = OrderDate where CompleteDate is null

Go


DROP INDEX nci_wi_Orders_BA4E6FA82C6722BF218DF4D4B01FC566   
    ON Orders
Go

Alter Table Orders Drop Column [OrderDate] 

Go