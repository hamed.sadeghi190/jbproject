﻿
namespace Jumbula.Common.Constants
{
    public static partial class Constants
    {

        public static class Donation
        {
            public static string Section_Info_Name = "PersoanlInformation";
            public static string Section_Amount_Name = "AmountSection";
            public static string Element_Amount_Name = "Amount";
        }
        public const int FreeTrialDays = 14;
        public const string FreeTrialExpiredMessage = "Your trial has expired";
        public const string FreeTrialStartedMessage = "Your trial just started";
        public const string FreeTrialMessage = "You have {0} days left of your trial";
        public const string DefaultPartnerName = "Enrichment Matters";
        public const string DefaultPartnerEmail = "info@enrichmentmatters.com";
        public const string EmailRegex = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
        public const string ConfirmationIdRegex = "[A-Z a-z 0-9]{4}-[A-Z a-z 0-9]{2}-[A-Z a-z 0-9]{4}";

        public const decimal Default_Paypal_MaxPreapprovalAmount = 4000;
        public const int Default_Paypal_MaxMemoChar = 995;
        // Contact us
        public const string ContactUs_Form_Submit_Failed = "Failed to submit contact us form. please try again.";
        public const string contactUs_Form_New_ContactForm_Received = "New contact us received from {0}";
        public const string ContactUs_Form_Submit_Success_Title = "Thanks for your message!";
        public const string ContactUs_Form_Submit_Success_Message = "Your message has been forwarded to the Jumbula team. We will respond you within next 24 hours.";

        // Net Gross
        public static decimal Gross_Promotional_Rate = ((decimal)0.03);
        public static decimal Gross_NonPromotional_Rate = ((decimal)0.05);


        // Login
        public const string Login_Home_Url = "/";

        // Coach
        public const int Coach_Avatar_Medium_W = 75;
        public const int Coach_Avatar_Medium_H = 75;

        public const int Coach_Avatar_Small_W = 32;
        public const int Coach_Avatar_Small_H = 32;

        public const int Coach_Logo_Crop_W = 470;
        public const int Coach_Logo_Crop_H = 300;
        public const string DefaultDateFormatWith3CharMonth = "MMM/dd/yyyy";
        public const string PriceFormat = "$0.00";
        public const string DateTime_Comma = "MMM dd, yyyy";
        public const string DateWithoutYear = "MMM dd";
        public const string TimeWithAMPM = "HH:mm tt";
        public const string F_DateTime_WithComma = "dddd MMMM dd, yyyy";
        public const string DefaultDateFormat = "MM/dd/yyyy";
        public const string DateFormatWithDash = "yyyy-MM-dd";
        public const string ExpiryDateFormat = "{0}/{1}";
        public const string DefaultTimeFormat = "hh:mm tt";
        public const string DefaultDateTimeFormatWithAMPM = "MM/dd/yyyy, HH:mm tt";
        public const string DefaultDateTimeFormatWithAMPMWithoutComma = "MM/dd/yyyy HH:mm tt";
        public const string DefaultDateTimeFormatAbrMonthWithAMPM = "MMM dd, yyyy HH:mm:ss tt";
        public const string DefaultShortDate = "M/d";
        public const string F_DateTimeWithAMPM_Camma = "MMMM dd, yyyy HH:mm tt";
        public const string F_DateTimeWithAMPM_AT = "MMMM d AT h:mm tt"; 
        public const string DefaultDateTimeFormat = "MM/dd/yyyy, HH:mm";

        public const string DateFormatWithMonth = "dd MMMM yyyy";
        public const string DateTimeFormatForStartOrEndTime = "dd MMM hh:mm";
        public const int Refund_Period = 180;
        // Account
        public const string Account_Why_Need_Your_Birthday = "We use your birthday to determine whether you an adult over the age of 18 and provide the right workflow experience for you.";
        public const string Account_Why_Need_Add_Parent = "You are a minor of less than 18 years old. You need to add your parent or legal guardian to the family profile before you can register for any events.";
        public const string Account_Invalid_DoB = "Invalid date of birth";


        // facebook
        public const string Facebook_Permissions = "me?fields=id,first_name,last_name,email,birthday,gender";
        public const string Facebook_Message_Title = "I am using Jumbula!";
        public const string Facebook_Message_Desc = "Interested in playing in a tournament or joining a camp? Use Jumbula to find the sports activity you are looking for and register online!";
        public const string Facebook_Message_Name = "Jumbula";
        public const string Facebook_Message_Picture_URL = "https://www.jumbula.com/Images/JBLogo.png";
        public const string Facebook_Page_URL = "https://www.facebook.com/jumbulame";
        public const string Insightl_Url = "https://api.insight.ly/v2.1";


        // flyer
        public const string Flyer_Default_Chess_Club_Logo = "~/images/chess-club-avatar.png";
        public const string Flyer_Default_Club_Logo = "~/images/club-nosport.png";

        // Message box
        public const string MessageBox_OK_Button = "OK";
        public const string MessageBox_Title_Expired_Event = "Expired";
        public const string MessageBox_Title_Frozen_Event = "Frozen event";
        public const string MessageBox_Message_Frozen_Event = "The {0} is frozen";
        public const string MessageBox_Title_OfflineReg = "Online registration";
        public const string MessageBox_Title_NotAuthorized = "Sorry, you are not a player!";
        public const string MessageBox_Title_TableTennis_OfflineRegistration = "Online registration for this event is not supported by Jumbula.";
        public const string MessageBox_Title_TableTennis_OfflineRegistration_Link_Href = "Please click the following link to register.<br/><a href='{0}'>Register</a>";
        public const string MessageBox_NotAuthorized_Message = "You are a club administrator and cannot register for events as a player. Please join Jumbula with a different email address and try to register for this event again.";
        public const string MessageBox_Title_SignupRequired = "Signup required";
        public const string MessageBox_SignupRequired_Message = "You need to sign into Jumbula in order to register for events.";
        public const string MessageBox_Title_PlayerProfile_Limitation_Reached = "Add member";
        public const string MessageBox_PlayerProfile_Limitation_Reached = "you can add only 10 profiles in 1 account.";

        public const string MessageBox_Title_NotCurrentDomain = "Event registration forbidden";
        public const string MessageBox_NotCurrentDomain_Message = "You have another item in your cart from a different vendor. Please checkout your cart before making a new registration.";

        // Delete Dialog
        public const string DeleteDialog_Title_ImageGallery = "Delete photo";
        public const string DeleteDialog_Question_ImageGallery = "Are you sure you want to delete this photo?";

        public const string DeleteDialog_Title_Coach = "Delete coach";
        public const string DeleteDialog_Question_Coach = "Are you sure you want to delete this coach?";

        public const string DeleteDialog_Title_Player = "Delete profile";
        public const string DeleteDialog_Question_Player = "Are you sure you want to delete this profile?";

        public const string DeleteDialog_Title_News = "Delete news";
        public const string DeleteDialog_Question_News = "Are you sure you want to delete this news?";

        public const string DeleteDialog_Title_Form = "Delete form";
        public const string DeleteDialog_Question_Form = "Are you sure you want to delete this form?";


        public const string DeleteDialog_Title_General = "Delete event";
        public const string DeleteDialog_Question_General = "Are you sure you want to delete this event?";
        public const string DeleteDialog_Note_General = "Note that this operation is permanent and is not reversible.Once you delete this event, you cannot undo the delete operation.";
        // TableTennis page 
        public const string F_TTTourney_EntryFee = "Entry price prize for the tournament is required.";
        public const string F_TTTourney_FirstStartMustBeAfterCheckInDeadLine = "The Check in deadLine start date cannot be equal or in the past Online Registration DeadLine.";
        public const string F_TTTourney_FirstStartMustBeAfterOnlineRegisterDeadLine = "The start date cannot be equal or in the past Online Registration DeadLine.";
        public const string F_TTTourney_DaysToDeadline = "The tournament online registration deadline should be at least {0} days from today.";
        public const string F_TTTourney_DaysToCheckInTime = "The tournament check in time should be at least {0} days from today.";
        public const int Tourney_NoDaysToCheckInTime = 1;
        public const string TableTennis_Events_EndDate_Required = "The End date field is required.";
        public const string TableTennis_Events_StartDate_Invalid = "The Start date field is Invalid.";
        public const string TableTennis_Events_EndDate_Invalid = "The End date field is Invalid.";
        public const string TableTennis_Events_EndDate_least = "The end date should be at least one day after today.";
        public const string TableTennis_Events_EndCannotBeBeforeStart = "The end date field cannot be earlier than the start date.";
        public const string TableTennis_Events_Required = "At least one event for the tournament should be defined.";
        public const string TableTennis_Priz_Required = "At least one prize for the tournament should be defined.";
        public const string TableTennis_RewardNote_Reqiured = "reward note for the tournament is required.";
        public const string TableTennis_Rating_Required = "Rating field is required";
        public const string TableTennis_RewardType_Required = "An appopriate reward type for the tournament should be selected. ";
        public const string TableTennis_Seniors_Age = "Age should be a number between 40 and 100";
        public const string TableTennis_Cadets_Juniors_Age_Range = "Age should be a number between 5 and 39";
        public const string TableTennis_Policy_RefundDayMissing = "Refund policy days field is required.";
        public const string TableTennis_USATTMembership_Required = "At least one membership should be defined.";
        // myjumbula
        public const int Grid_Page_Size = 10;
        public const string MyJumbula_Grid_ViewEvent = "View";
        public const string MyJumbula_Grid_ManageEvent = "Manage";
        public const string MyJumbula_Grid_DeleteEvent = "Delete";
        public const string Myjumbula_Change_Notification_Settings_Failed = "Failed to update notification settings. try again.";
        public const string Myjumbula_Change_Notification_Settings_Done = "Your configuration has been updated.";

        // Player Profile
        public const int PlayerProfile_Max_FirstName_Length = 16;
        public const int PlayerProfile_Max_LastName_Length = 16;
        public const int PlayerProfile_Min_Age = 18;
        public const string PlayerProfile_Invalid_DoB = "{0} should be adult but the birthday you select is under 18";
        public const string PlayerProfile_Invalid_Relationship = "as you are not adult, your can't add {0}";
        public const string PlayerProfile_Gender_Required = "Gender not selected";
        public const string PlayerProfile_Notification_Title = "Warning!";
        public const string PlayerProfile_Notification_ChildNoParent_Message = "As you are not adult, you would need to add your parent in order to register for events.";
        public const string PlayerProfile_UserIsNotOwnerForProfile = "{0} is not the owner for the profile {1}";
        public const string PlayerProfile_UserIsNotOwnerForProfile_AndNotOwnerOfClub = "{0} is nither the owner for the profile {1} nor the owner for the club";
        public const string PlayerProfile_ProfileComplition_Link = "We don't have your complete information<br />to be able to register for this event, <a href='{0}'>please complete your profile</a> first";
        public const string PlayerProfile_FirstTime_Minor = "Due to our policy, you need to add your parent to be able to register";
        public const string PlayerProfile_Failed_To_UpdateInfo = "failed to update {0} information. try again later";
        public const string PlayerProfile_Failed_To_UpdateJobInfo = "failed to update job information. try again later";
        public const string PlayerProfile_Update_JobInfo_Successful = "update job information was successful";
        public const string PlayerProfile_Update_Info_Successful = "update {0} information was successful";
        public const string PlayerProfile_Doctor_Info = "doctor contact";
        public const string PlayerProfile_Contact_Info = "contact";
        public const string PlayerProfile_Health_Info = "health";
        public const string PlayerProfile_School_Info = "school";
        public const string PlayerProfile_Insurance_Info = "insurance";
        public const string PlayerProfile_Emergency_Info = "emergency contact";
        public const string PlayerProfile_School_Name_Empty = "School name is required";
        public const string PlayerProfile_School_Grade_Empty = "Select school grade";
        public const string PlayerProfile_Default_Avatar = "/images/profile-avatar_{0}.png";
        public const string PlayerProfile_NavBar_Default_Icon = "/images/player.png";
        public const string PlayerProfile_Minor = "Minor";
        public const string PlayerProfile_Adult = "Adult";
        public const string PlayerProfile_ChangePassword_Succeed = "Your password successfully changed.";
        public const string PlayerProfile_ChangePassword_Failed = "Failed to change password. Please try again";
        public const string PlayerProfile_ChangePassword_Failed_InvalidData = "Invalid data received. try sending valid data";
        public const string PlayerProfile_Ajax_Request_Failed = "Error in establish connection with server. try again later";
        public const string PlayerProfile_Avatar_Name = "profile-avatar.png";
        public const string PlayerProfile_Avatar_Format = "[size]_{0}_{1}{2}";
        public const string PlayerProfile_Avatar_Large = "Large_{0}_{1}{2}";
        public const string PlayerProfile_Avatar_Medium = "Medium_{0}_{1}{2}";
        public const string PlayerProfile_Avatar_Small = "Small_{0}_{1}{2}";
        public const string PlayerProfile_Avatar_Extension = ".png";
        public const string PlayerProfile_Avatar_Temp = "{0}_{1}_Temp";
        public const string PlayerProfile_Avatar_Main = "primary";
        public const int PlayerProfile_Avatar_Large_W = 150;
        public const int PlayerProfile_Avatar_Large_H = 150;
        public const int PlayerProfile_Avatar_Medium_W = 75;
        public const int PlayerProfile_Avatar_Medium_H = 75;
        public const int PlayerProfile_Avatar_Small_W = 32;
        public const int PlayerProfile_Avatar_Small_H = 32;
        public const int PlayerProfile_Avatar_Crop_W = 470;
        public const int PlayerProfile_Avatar_Crop_H = 300;

        // ImageGalleryItem Page
        public const int ImageGalleryItem_Club_W = 650;
        public const int ImageGalleryItem_Club_H = 310;
        public const int ImageGalleryItem_Club_Thumbnail_W = 150;
        public const int ImageGalleryItem_Club_Thumbnail_H = 150;
        //-------------------------------------------------------
        public const int ImageGalleryItem_Event_W = 800;
        public const int ImageGalleryItem_Event_H = 600;
        public const int ImageGalleryItem_Event_Thumbnail_W = 150;
        public const int ImageGalleryItem_Event_Thumbnail_H = 150;
        // In-form notification
        public const string Inform_Notification_Color_Error = "red";
        public const string Inform_Notification_Color_Warning = "orange";
        public const string Inform_Notification_Color_Succeed = "green";
        public const string Inform_Notification_Color_Information = "blue";

        // Jumbula
        public const int Jum_SysAdminUserId = 1;
        public const int Jum_SupportAdminUserId = 2;
        public const string Jum_JumbulaCore = "Jumbula.Core";

        // Utilities.PreventCacheRandom
        public const int Prevent_Cache_Min = 1000;
        public const int Prevent_Cache_Max = 9999;

        // Notification Titles
        public const string Notification_Warning = "Warning!";
        public const string Notification_Error = "Error!";
        public const string Notification_Success = "Succeed!";
        public const string Notification_Expired = "Expired!";
        public const string Notification_NotStarted = "Not started!";
        public const string Notification_Restricted = "Restricted Event";

        // course page
        public const string Course_Session_Left_Format = "({0} left)";
        public const string Course_Session_Left_Session = "session";

        // tourney reg page
        public const string Tourney_Reg_Error_Title = "Error!";
        public const string Tourney_Reg_Error_Desc = "There was an error processing your form. please check the form";

        // camp page
        public const int Camp_Name_Max_Length = 50;
        public const int Camp_Name1_Max_Length = 50;
        public const int Camp_Address_Max_Length = 90;
        public const int Camp_Club_Name_Max_Length = 50;

        // all event pages
        public const string Event_Online_Registration_Expired_On = "The online registration for this event expired on:";
        public const string Event_Online_Registration_Expired = "The online registration for this event expired.";
        public const string Event_Online_Registration_NotStarted = "The online registration for this event has not been started yet.";
        public const string Event_Online_Registration_Restricted = "Unfortunately, you are not eligible to register for this event. For more information, please call {0}";

        // club page
        public const string Coach_Avatar_Default_Small = "/images/coach-avatar-small.png";
        public const string Club_Flyer_FileName = "flyer_{0}{1}";
        public const string Club_Tournament_Coverage_FileName = "Coverage_{0}_{1}{2}";
        public const string Club_Flyer_BlobFileName = "{0}/{1}";
        public const string Club_Cover_Chess_Default = "/images/club-cover-chess.jpg";

        public const string Club_Empty_Announce_Text = "Write a subtitle for your club";
        public const string ImageGallery_Success_Saved = "Image details have been saved";
        public const int Club_Name_Max_Length = 50;

        public const int Club_Person_Name_Max_Length = 40;


        public const int Club_Announce_Max_Length = 160;

        public const int Club_News_Short_Summary_Max_Length = 140;
        public const int Club_News_Title_Max_Length = 60;

        public const int Club_Coach_Name_Max_Length = 40;
        public const int Club_Coach_Bio_Max_Length = 200;

        public const int Club_News_Page_Size = 4;
        public const string Default_ConvFee_Message = "You can avoid paying this extra charge if you pay with cash or check.";
        public const string Club_Cover_Temp = "Cover_Temp";
        public const string Club_Cover = "Cover";
        public const int Cover_Image_Size_W = 1600;
        public const int Cover_Image_Size_H = 700;

        public const string Cover_Upload_Defautl_Path = "~/Images/club-cover-default.png";
        public const string Cover_Default_Path = "~/Images/Covers/all-sport-cover.jpg";


        public const string Cover_Local_Directory = "~/Images/Covers/";
        public const string Cover_Local_Thumbnail_Directory = "~/Images/Covers/Thumbnail/";
        public const string ImageGallery_Local_Directory = "~/Images/ImageGallery/";

        public const string Club_Logo_Temp = "Logo_Temp";
        public const string Club_Logo_Large_Name = "Logo_Large";
        public const string Club_Logo_Med_Name = "Logo_Medium";
        public const string Club_Logo_Small_Name = "Logo_Small";

        public const int Club_Logo_Crop_W = 470;
        public const int Club_Logo_Crop_H = 300;

        public const int Club_Logo_Large_W = 150;
        public const int Club_Logo_Large_H = 150;
        public const int Club_Cover_W = 800;
        public const int Club_Cover_H = 215;
        public const int Club_Logo_Med_W = 75;
        public const int Club_Logo_Med_H = 75;
        public const int Club_Logo_Small_W = 32;
        public const int Club_Logo_Small_H = 32;

        public const string Event_Logo_Temp = "{0}_Temp";
        public const string Event_Logo_Large_Name = "{0}_Large";
        public const string Event_Logo_Small_Name = "{0}_Small";
        public const string CoachAvatar_Name = "coach_small.png";
        // Order History Desc
        public const string Order_History_Cancel_Desc = "Order {0} - {1} ({2}) is canceled.";
        public const string Order_History_Cancel_OrderItem_Desc = "The {0} ({1}) has been canceled by {2}";
        public const string Order_History_Delete_Charge_Desc = "{0} for {1} ({2}) has been canceled by {3}";
        public const string Order_History_Cancel_Add_To_Credit = "Order {0} - {1} ({2}) is canceled and {3} is refunded to the family's credit.";
        public const string Order_History_Refund_Add_To_Credit = "Order {0} - {1} ({2}) is refunded and {3} is added to the family's credit.";
        // Category logo
        public const string Club_NoSport_Default_Image_Large = "/images/club-nosport.png";
        public const string Club_NoSport_Default_Image_Small = "/images/club.png";

        public const string Club_Chess_Default_Image_Large = "/images/chess-club-avatar.png";

        public const string Club_TableTennis_Default_Image_Large = "/images/tt-club-avatar.png";

        public const string Club_Wrestling_Default_Image_Large = "/images/wrestling-club-avatar.png";


        public const int Coach_Logo_Med_W = 75;
        public const int Coach_Logo_Med_H = 75;
        public const int Coach_Short_Bio_Len = 200;
        public const int Gallery_Title_Max_Legth = 40;
        public const int Gallery_Image_W = 188;
        public const int Gallery_Image_H = 90;
        public const int Gallery_Large_Image_W = 650;
        public const int Gallery_Large_Image_H = 310;
        public const int Profile_Avatar_W = 150;
        public const int Profile_Avatar_H = 150;
        public const int HotelFee_Max = 1000;
        public const int HotelFee_Min = 9;
        public const int Tourney_EntryFee_Max = 1000;
        public const int Tourney_EntryFee_Min = 1;
        public const int Tourney_PrizeFund_Min = 100;
        public const int Tourney_PrizeFund_Max = 250000;
        public const int Tourney_PrizeFundBased_Min = 2;
        public const int Tourney_PrizeFundBased_Max = 1000;
        public const int Tourney_NoDaysToDeadline = 1;
        public const int UscfMembership_Max = 150;
        public const int Camp_EntryFee_Max = 2500;
        public const int USATTMembership_Max = 500;
        public const int Camp_EarlyBirdDiscount_Max = 100;
        public const int Camp_MultipleWeekDiscount_Max = 100;
        public const int Camp_SiblingDiscount_Max = 50;
        public const int Camp_ClubMembershipDiscount_Max = 50;
        public const int Camp_LunchService_Max = 100;
        public const int Camp_Childcare_Max = 100;
        public const int Camp_RecommendedMinAge = 5;
        public const int Camp_RefundProcesingFee_Max = 100;
        public const string Camp_Earlybird_EndsOn = "ends on";


        public const int Search_Pageer_Record_Count = 10;
        public const int Search_AutoComplate_Max_Length = 50; // max character
        public const int Search_ExpaireDate = 7; // save search text

        public const int E_NoOfTrialsToGenerateRandomSubDomainName = 5;

        public const int Db_TakeMaxClubRecords = 50;
        public const int Db_TakeMaxTourneyRecords = 50;
        public const int Db_TakeMaxCampRecords = 50;

        public const int PasswordResetTokenExpiry = 2880;  // in minutes, 2 days

        public const double Camp_CostToTotalDiscountMultiplier = 3;

        public const int Order_Fixed_Quantity = 1;


        // Const chars  

        public const char C_Space = ' ';
        public const char C_Tab = '\t';
        public const char C_Dot = '.';
        public const char C_AmperSign = '&';
        public const char C_Equal = '=';
        // Paths

        public const string Path_Images = "Images";
        public const string Path_ClubFilesContainer = "clubs"; // must be all lowercase for azure storage
        public const string Path_PlayerFilesContainer = "players"; // must be all lowercase for azure storage
        public const string Path_SysContainer = "sys"; // must be all lowercase for azure storage
        public const string Path_ClubFilesImagesDir = "Images";
        public const string Path_ClubFilesThumnailsDir = "Thumnails";
        public const string Path_DefaultClubLogo = "logo.png";
        public const string Path_Templates = "Templates";
        public const string Path_ChessTemplate = "Chess";
        public const string Path_CoachAvatarChildDir = "CoachesAvatar";
        public const string Path_EmailAttachments = "emailattachments";
        public const string Path_SupplementalType = "Upload";
        public const string Path_FollowUpForms = "/FollowUpForms/{0}_{1}/";
        public const string Path_InsuranceForm = "/InsuranceForm/";
        public const string Path_InstructorBackgroundCheck = "/InstructorBackgroundCheck/";
        public const string Path_TableTennisTemplate = "TableTennis";
        public const string Path_ConfirmFbLoginSuccess = "Content/html/ConfirmFacebookLoginSuccess.html";
        public const string Path_ConfirmReservation = "~/Views/Shared/Email/ReservationNotification.cshtml";
        public const string Path_ConfirmReservationBody = "~/Views/Shared/Email/ReservationNotificationBody.cshtml";
        public const string Path_ConfirmPreapprovalBody = "~/Views/Shared/Email/PreapprovalPaymentConfirmationBody.cshtml";
        public const string Path_RequestPreapprovalBody = "~/Views/Shared/Email/PreapprovalRequestEmail.cshtml";
        public const string Path_EmailTemplate = "~/Views/EmailCampaignTemplate/EmailTemplate.cshtml";
        public const string Path_ConfirmEventReg = "Content/html/ConfirmEventReg.html";
        public const string Path_CancelOrderReg = "Content/html/CancelOrderReg.html";
        public const string Path_ConfirmNotifyImportedClub = "Content/html/ConfirmNotifyImportedClub.html";
        public const string Path_ConfirmNotifyImportedClubSuccess = "Content/html/ConfirmNotifyImportedClubSuccess.html";
        public const string Path_NotifyClubOwnerEventReg = "Content/html/NotifyClubOwnerEventReg.html";
        public const string Path_NotifyClubOwnerCancelOrder = "Content/html/NotifyClubOwnerCancelOrder.html";
        public const string Path_ClaimClub = "Content/html/ClaimClubEmail.html";
        public const string Path_Event_Question = "Content/html/EventQuestionEmail.html";
        public const string Path_Cash_Confirm = "Content/html/ConfirmCashPaid.html";
        public const string Path_Settlement_Report = "Content/html/SettlementReportEmail.html";
        public const string Path_CatalogItems_Folder = "CatalogItems";
        public const string PriceOption_Lable = "Fee";
        public const string Notification_Confirmation_Content = "<body autocorrect='off' contenteditable='true' class=''><p><span style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'>Dear Parent/Guardian:</span><br style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'><br style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'><span style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'>Your child, %CHILDNAME%, has been enrolled in the %CLASSNAME% for the %SEASONNAME% session.</span></p><p><span style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'>We look forward to providing a fun and exciting program for your child.</span>&#65279;</p></body>";
        public const string Notification_Cancellation_Content = "<body autocorrect='off' contenteditable='true' class=''>&#65279;<p><span style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'>Dear Parent/Guardian:</span><br style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'><br style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'><span style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'>Your child, %CHILDNAME%, has not been enrolled in the %CLASSNAME% for the %SEASONNAME% session due to insufficient number of enrollments.</span></p><p><span style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'>We are sorry about any inconvenience and look forward to providing a fun and exciting program for your child in another class&#65279;.</span><span style='font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255); color: rgb(34, 34, 34);'>&#65279;</span></p><div style='color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; background-color: rgb(255, 255, 255);'><span style='font-family: Helvetica; font-size: 15px;'></span></div></body>";
        public const string Notification_NewFamilyNotification = "Welcome to our new registration page! All families must create a new account and password under the “New Family?” button below. Once you set up a new account, you will be able to sign in as 'Already a member' in the future.";
        public const string Notification_DependentCareDescription = "The class fees listed above were paid to the Provider of the enrichment class(es) attended by the student(s) named above. In accordance with the rules as set forth by the Internal Revenue Service (IRS), any additional fees paid at the time of registration were not paid to the Provider of the class and, therefore, are not eligible dependent care expenses to be used in the calculation of the child care credit on your Federal individual income tax return or reimbursement from a dependent care flexible spending account. Additionally, please be aware that in the event of a random audit by the IRS, the IRS may determine that the class fees paid are not a reimbursable expense.";

        // these constants are probably useless and must be delete later
        public const string Path_Image_CampFlyer_TT_Banner = "Images/CampFlyer_TT_Banner.jpg";
        public const string Path_Image_CampFlyer_Wrestling_Banner = "Images/ClassFlyer_Wrestling_Banner.jpg";
        public const string Path_Image_CampFlyer_TT_LeftPanel = "Images/CampFlyer_TT_LeftPanel.jpg";
        public const string Path_Image_CampFlyer_Wrestling_LeftPanel = "Images/ClassFlyer_Wrestling_LeftPanel.jpg";
        public const string Path_Image_CampFlyer_Chess_Banner = "Images/CampFlyer_Chess_Banner.jpg";
        public const string Path_Image_ClassFlyer_Chess_LeftPanel = "Images/ClassFlyer_Chess_LeftPanel.jpg";
        public const string Path_Image_ClassFlyer_TT_LeftPanel = "Images/ClassFlyer_TT_LeftPanel.jpg";
        public const string Path_Image_Trophy_Chess_Tournament_Prize = "Images/ChessTrophy.jpg";
        public const string Path_Image_CampFlyer_Chess_LeftPanel = "Images/CampFlyer_Chess_LeftPanel.jpg";

        public const string Path_Image_JBPowerLogo = "Images/JBpower.png";
        public const string Path_Image_CampFlyer_LineSeparator = "Images/CamFlyer_SeparatorLine.jpg";
        public const string Path_Image_JBLogo = "Images/logo-90.png";
        public const string Path_Image_PreviewLogo = "/images/image-preview.png";
        public const string Path_Image_ProfileAvatar = "/images/profile-avatar.png";
        public const string Path_HTTPS_JbLogo = "{0}/Images/JBpower.png"; //"https://jblobprod.blob.core.windows.net/sys/JBpower.png";

        // Flyer image paths
        public const string Path_Image_CampFlyer_Banner = "Images/CampFlyer_Banner.jpg";
        public const string Path_Image_ClassFlyer_Banner = "Images/ClassFlyer_Banner.jpg";

        // Const chars but represented as strings 

        public const string S_AmperSign = "&";
        public const string S_EqualSign = "=";
        public const string S_QuestionMark = "?";
        public const string S_Comma = ",";
        public const string S_Space = " ";
        public const string S_Period = ".";
        public const string S_Slash = "/";
        public const string S_Tab = "\t";
        public const string S_UnderScore = "_";
        public const string S_Dash = "-";
        public const string S_Treeple_Dash = "---";
        public const string S_OpenPran = "(";
        public const string S_ClosePran = ")";
        public const string S_Colon = ":";
        public const string S_ColonSpace = ": ";
        public const string S_Semicolon = ";";
        public const string S_DoubleQuote = "\"";
        public const string S_BackSlash = "\\";
        public const string S_SpaceDashSpace = " - ";
        public const string S_CommaSpace = ", ";
        public const string S_TrancatedNames = "...";

        // Word strings
        public const string W_OK = "OK";
        public const string W_Fail = "Fail";
        public const string W_Large = "Large";
        public const string W_Medium = "Medium";
        public const string W_Small = "Small";
        public const string W_Default = "Default";
        public const string W_Anonymous = "Anonymous";

        public const string JumbulaDotCom_Url = "https://www.jumbula.com/";
        public const string W_Jumbula = "Jumbula";
        public const string W_JumbulaDotCom = "www.jumbula.com";
        public const string W_NoReplyEmailAddress = "noreply@jumbula.com";
        public const string W_BCC_EmailNotification = "support@jumbula.com";
        public const string W_JumbulaSupportEmail = "support@jumbula.com";
        public const string W_JumbulaInfoEmail = "info@jumbula.com";
        public const string W_JumbulaEmailFormat = "{0}@jumbula.com";
        public const string W_InClubs = " in clubs";
        public const string W_InTournaments = " in tournaments";
        public const string W_InCourses = " in classes";
        public const string W_InCamps = " in camps";
        public const string Jumbula_Dashbaord_Url = "/Dashboard";

        public const string Jumbula_Address = "425 Broadway Redwood City, CA 94063 United States";
        public const string Jumbula_PhoneNumber = "6502570076";

        // search key words
        // search key words
        // ChargeDiscount Donation Description
        public const string W_Donation = "Donation {0}";
        public const string W_DonationDescription = "Add a donation to your order";
        public const string W_CampsLabel = "Camps";
        public const string W_CoursesLabel = "Classes";
        public const string W_TournamentsLabel = "Tournaments";
        public const string W_ClubsLabel = "Clubs";
        public const string W_EventsLabel = "Events";
        public const string W_Convenience = "Convenience fee ({0}%) :  {1}";
        public const string W_EventRouteName = "Events";
        public const string W_AllChessClubsRoute = "Clubs/Chess";

        public const string W_AllWrestlingClubsRoute = "Clubs/Wrestling";
        public const string W_AllTournaments = "Event/Tournaments";
        public const string W_AllCamps = "Event/Camps";
        public const string W_AllClasses = "Event/Classes";

        public const string W_AllTableTennisClubsRoute = "Clubs/TableTennis";
        public const string W_Tourney_Ef = "Tournament entry fee: {0}";
        public const string W_Tourney_Ef_After = "Tournament entry price for registering after {0}: {1}";
        public const string W_Camp_Program_Ef = "Event {0} ({1}): {2}";
        //public const string W_Camp_Program_Ef = "Camp registration fee, {0} program";
        public const string W_Camp_Services_EF = "Camp Services, {0}: {1}";
        public const string W_Camp_Sibling = "Sibling discount, sibling {0}: -{1}";
        public const string W_Camp_EB_Discount = "Early bird discount, {0} program: -{1}";
        public const string W_Camp_MW_Discount = "Multiple discounts, {0} program: -{1}";
        public const string W_Camp_Membership = "Membership discount, expiration date {0}: -{1}";
        //public const string W_TTEvent_Program_Ef = "Table tennis event registration fee, {0} program";
        public const string W_TTEvent_Program_Ef = "Tournament {0} ({1}): {2}";
        public const string W_Event_Program_Ef = "Event {0}: {1}";
        public const string W_Course_FullProgram_Ef = "Class: {0}";
        public const string W_Course_ProRating_Ef = "{0}: {1}";
        public const string W_Course_DropInProgram_Ef = "Drop in course, {0}: {1}";
        public const string W_Course_EB_Discount = "Early bird discount, {0} program: -{1}";
        public const string W_Course_Sibling = "Sibling discount, sibling {0}: -{1}";
        public const string W_Course_Membership = "Membership discount, expiration date {0}: -{1}";
        public const string W_RecordCreatedOn = "Created on: ";
        public const string W_Tournament = "Tournament";
        public const string W_Camp = "Camp";
        public const string W_Course = "Course";
        public const string W_Pack = "Pack";
        public const string W_ClubDonation = "Donation";
        public const string W_League = "League";
        public const string W_Coach = "Coach";
        public const string W_Calendar = "Calendar";
        public const string W_GmIm = "GM";
        public const string W_Econ = "Econ";
        public const string W_Senior = "Senior";
        public const string W_Playup = "Playup";
        public const string W_Amount = "amount";
        public const string W_Description = "description";
        public const string W_ClubDiscounts = "Club Discounts";
        public const string W_ClubPromotionalDiscounts = "Promotional Discounts";
        public const string W_Png = "png";
        public const string W_Jpg = "jpg";
        public const string W_Jpeg = "jpeg";
        public const string W_Gif = "gif";
        public const string W_Camp_CreateButton = "Create camp";
        public const string W_Route_ShowSpecificChessTournament = "ShowSpecificChessTournament";
        public const string W_Route_ShowSpecificTableTennisTournament = "ShowSpecificTableTennisTournament";
        public const string W_Route_ShowSpecificEvent = "ShowSpecificEvent";
        public const string W_Route_ShowSpecificProgram = "ShowSpecificProgram";
        public const string W_Route_ShowSpecificProgramInViewMode = "ShowSpecificProgramInViewMode";
        public const string W_Route_ShowSpecificSeasonPrograms = "ShowSpecificSeasonPrograms";
        public const string W_Route_ShowSpecificTournament = "ShowSpecificTournament";
        public const string W_Route_ShowSpecificCamp = "ShowSpecificCamp";
        public const string W_Route_ShowSpecificCourse = "ShowSpecificCourse";
        public const string W_Route_ShowSpecificClub = "ShowSpecificClub";
        public const string W_Route_Url_Format = "{clubDomain}.{host}.{tld}";
        public const string M_Camp_Policy_RefundDayMissing = "Refund policy days field is required.";
        public const string M_Camp_Policy_RefundProccedingFeeMissing = "Refund policy processing field is required.";
        public const string M_Camp_ExternalUrlMissing = "Enter a value for the external web site url.";
        public const string M_Short_MorningExtendedCareService = "MEC";
        public const string M_Short_AfternoonExtendedCareService = "AEC";
        public const string M_Short_LunchService = "Lunch";
        public const string M_Short_EarlyBirdDiscount = "EB";
        public const string M_Short_MultipleWeekDiscount = "MW";
        public const string M_Short_SiblingDiscount = "Sibling";
        public const string M_Short_ClubMembershipDiscount = "Club";
        public const string W_Coach_CreateButton = "Add Coach";
        public const string W_TableTennis_Under15_Discount_Desc = "Cadet 15 and Under discount: -{0}";
        public const string NotValidPercent = "{0} is not valid. The value should be between 1 - 100.";
        //Facebook
        public const string Path_FB_ChannelURI = "Home/Channel/";
        public const string Path_FB_Picture = "http://graph.facebook.com/{0}/picture?width={1}&height={2}";

        // Format strings
        public const string F_StreetFormt = "{0}, {1}";
        public const string F_AddressFormt = "{0}, {1}, {2}, {3}, {4}, {5}";
        public const string F_CategoryFormat = "{0} --> {1}";
        public const string F_GoogleTimeZoneWebService = "https://maps.googleapis.com/maps/api/timezone/json?location={0},{1}&timestamp={2}&sensor={3}";
        public const string F_FullName = "{0} {1}";
        public const string F_UserName = "{0}";
        public const string F_ReceivedFeedbackNotificationTitle = "Thanks for your feedback!";
        public const string F_ReceivedFeedbackNotificationDesc = "Your feedback has been forwarded to the Jumbula team. While we can't respond individually to feedback, each submission is reviewed and considered.";
        public const string F_ReceivedFeedbackToSupportTitle = "New feedback received from {0}";
        public const string F_ResetPasswordEmailSentNotificationTitle = "We've sent password reset instructions to your email";
        public const string F_ResetPasswordEmailSentNotificationDesc = "Please check your email ({0}) account and click on the provided link.";
        public const string F_JoinUsNotificationTitle = "Thanks for registering with Jumbula!";
        public const string F_JoinUsNotificationDesc = "Check your email {0} at {1} to activate your account. No email? Check your spam/junk folder";
        public const string F_ConfirmEmailSubject = "Confirm your Jumbula account, {0}!";
        public const string F_ConfirmNotifyImportedClubSubject = "Take the ownership of your club, {0}!";
        public const string F_ConfirmEmailSuccessSubject = "Welcome to Jumbula, {0}!";
        public const string F_ResendConfirmEmailSuccess = "We just sent an email to {0}";
        public const string F_ResendConfirmEmailInProgress = "We are sending an email to {0}...";
        public const string F_preapprovedpaymentsplanSubject = "Preapproved payments plan";
        public const string F_EcheckClearedNotification = "Notification of a Cleared eCheck Payment from {0}";
        public const string F_ClientMonthlyBilling = "Jumbula Monthly Billing {0}";
        public const string F_ClientMonthlyBillingEmailDescription = "Jumbula Online for Business{0} ";
        //add subject for jb/Dashboard
        public const string F_ClientBillingSubject = "Jumbula Billing {0}";
        public const string F_ClientMonthlyBillingDescription = "Automatic payment: {0} ***{1}";
        public const string F_ClientBillingByCreditSubject = "Your account has been credited {0}";
        public const string F_ClientBillingRefundSubject = "Your account has been refunded {0}";
        public const string F_ConfirmReservationSubject = "Confirmation Reservation";
        public const string F_ConfirmTransferSubject = "Transfer Reservation";
        public const string F_CancelReservationSubject = "Cancel Reservation";
        public const string F_PartialRefundReservationSubject = "Refund Confirmation";
        public const string F_EditReservationSubject = "Edit reservation";
        public const string F_CashConfirmEmailSubject = "Payment Confirmation";
        public const string F_CashEditedEmailSubject = "Edit Payment Transaction";
        public const string F_AdminInvoiceEmail = "{0} just paid for your invoice # {1}";
        public const string F_ParentInvoiceEmail = "You just sent a payment to {0} for invoice # {1}";
        public const string F_ManualInstallmentReminderSubject = "Manual installment reminder";
        public const string F_AutomaticInstallmentReminderSubject = "Automatic installment reminder";
        public const string F_InvoiceSubject = "Invoice";
        public const string F_AdminTakePaymentSubject = "You have taken a payment for {0}";
        public const string F_EditDropInSubject = "Edit reservation for {0}. ";
        public const string F_CancelDropInSubject = "Cancel reservation for {0}. ";
        public const string F_ParentTakePaymentSubject = "A payment has been taken by {0}";
        public const string F_AdminInvoiceSubject = "You have sent an invoice to {0}";
        public const string F_ParentInvoiceSubject = "You have received an invoice from {0}";
        public const string F_ProviderRespondToInvitationSubject = "Invitation Response ({0} - {1})";
        public const string F_PreapprovalReminderSubject = "Preapproval Reminder";
        public const string ConfirmReservationBodyTitle = "Congratulations, the following reservation was made";
        public const string ConfirmTransferBodyTitle = "Congratulations, the following order was transferred";
        public const string ConfirmCashConfirmTitle = "Congratulations, the following reservation was paid";
        public const string EditReservationBodyTitle = "The following reservation was Edited";
        public const string CancelReservationBodyTitle = "The following Item(s) was canceled";
        public const string PartialRefunReservationBodyTitle = "The following Item was refunded.";
        public const string InstallmentReminderTitle = "Your installment due date is approaching.";
        public const string InvoiceTitle = "Please make the following payment";
        public const string InstallmentConfirmationBodyTitle = "Congratulations, the installment payment was received";
        public const string ConfirmPreapprovalPaymentTitle = "Congratulations, the following payment has been received.";
        public const string ConfirmFamilyTakePaymentTitle = "Congratulations, the following payment has been taken.";
        public const string ConfirmEditPaymentTitle = "The following transaction was changed";

        public const string F_ConfirmEventRegSubject = "Order confirmation";
        public const string F_CancelOrderRegSubject = "Order cancellation";
        public const string F_CancelOrder_AlreadyCanceled = "This event has been canceled already";
        public const string F_CancelOrder_NotSupported = "This event does not support cancellation";
        public const string F_EditOrder_NotSupported = "This event does not support edit";
        public const string F_CancelOrder_NotSupported_Expired = "Expired event does not allowed to be canceled";
        public const string F_RefundOrder_NotSupported_AfterRefundPeriod = "Refund order is not supported after {0} days from when the payment is received";
        public const string F_RefundNotSupported = "Refund is not supported for this order";
        public const string F_AutomaticRefundNotSupported = "You cannot automatically refund a manual (cash, check, family credit, etc.) transaction.";
        public const string F_InvalidAmountForRefund = "The maximum amount you can refund is {0}";
        public const string F_EditOrder_NotSupported_Expired = "Expired event does not allowed to edit";
        public const string F_EditOrder_NotSupported_ByClub = "Edit order is not supported for this club";
        public const string F_NotifyClubOwnerEventRegSubject = "New Registration";
        public const string F_NotifyClubOwnerCancelOrderSubject = "Cancel Registration";
        public const string F_ResetPasswordSubject = "Reset your Jumbula password";
        public const string F_ResetPasswordSuccessSubject = "Jumbula password reset notification";
        public const string F_ClubPageNameNotAvailable = "The club page name {0} is not available";
        public const string F_SportCategoryNotSupported = "The SportCategory {0} is not supported";
        public const string F_SubDomainCategoryNotSupported = "The SubDomainCategory {0} is not supported";
        public const string F_CardTypeCategoryNotSupported = "The CardTypeCategory {0} is not supported";
        public const string F_PaymentGetwayNotSupported = "The PaymentGetway {0} is not supported";
        public const string F_ActionCallerNotSupported = "The ActionCaller {0} is not supported";
        public const string F_ChargeDiscountCategoryNotSupported = "The ChargeDiscountCategory {0} is not supported";
        public const string F_CampSectionCategoryNotSupported = "The CampSectionCategory {0} is not supported";
        public const string F_PolicyCategoryNotSupported = "The PolicyCategory {0} is not supported";
        public const string F_OptionalRegDataCategoryNotSupported = "The OptionalRegDataCategory {0} is not supported";
        public const string F_ClubFeeRateNotSupported = "The ClubFeeRateCategory {0} is not supported";
        public const string F_Image_MimeType = "image/{0}";
        public const string F_AttachmentMemeType = "attachment/{0}";
        public const string F_Input_EnterValue = "Please provide a value for {0}";
        public const string F_Tourney_DaysToDeadline = "The tournament online registration deadline should be at least {0} days from today";
        public const string F_Cart_OnlineRegFor = "Online registration for {0}";
        public const string F_Cart_OnlineRegFor_Camp = "Camp {0} ({1} - {2} , {3})";
        public const string F_Cart_OnlineRegFor_Event = "Event {0} {1}";
        public const string F_Cart_OnlineRegFor_Course = "{0} ({1} - {2})";
        public const string F_DbGeographyPointFormat = "POINT ({0} {1})";
        public const string F_UserIsNotAdminForClub = "{0} is not the club admin for the club {1}";
        public const string F_Tourney_Reg_NoOfByeRequestsExceedsMax = "You can select a maximum of {0} byes for this tournament (you currently have selected {1} byes)";
        public const string F_Tourney_CO_NegAmount = "Cannot check out for a negative amount: {0}";
        public const string F_NoCartItems = "Cannot check out a negative or 0 items in cart: {0}";
        public const string F_Amount0WrongCartType = "Cannot check out cart amount 0 with card type: {0}";
        public const string F_HttpsSchemeAuthority = "https://{0}";
        public const string F_Payment_MismatchedAmount = "Payment mismatched amount: order id: {0}, order amount: {1}, paid amount {2}";
        public const string F_Payment_MismatchedAmount_Edit = "Payment mismatched amount in edit order, order id: {0}, order amount: {1}, paid amount {2}";
        public const string F_Payment_Incomplete = "Payment incomplete: order id: {0}";
        public const string F_Payment_Incomplete_Edit = "Payment incomplete in edit order: order id: {0}";
        public const string F_Tourney_Ef_UscfMemChargeExceedsMax = "The USCF membership amount cannot exceed {0}";
        public const string F_Tourney_Ef_UsaTTMemChargeExceedsMax = "The USATT membership amount cannot exceed {0}";
        public const string F_Club_Delete = "the Club cannot be deleted because players have already registered for it.";
        public const string F_Event_CannotBeDeleted = "Event {0} for club {1} cannot be deleted because players have already registered for it";
        public const string F_Form_CannotBeDeleted = "The form is being used for some events and can not be deleted";
        public const string F_Camp_Discount_ExceedsMax = "The discount amount cannot exceed {0}";
        public const string F_Camp_Service_ExceedsMax = "The service amount cannot exceed {0}";
        public const string F_Camp_Policy_RefundChargeTooMuch = "The refund policy processing charge cannot exceed {0}";
        public const string F_Camp_Policy_NoCampScheduleSelected = "You should select at least one camp schedule to proceed with the registration";
        public const string F_Camp_Policy_No_SiblingName = "You should enter sibling name to proceed with the registration";
        public const string F_Camp_Policy_No_MembershipExpireDate = "You should enter your membership expire date to proceed with the registration";
        public const string F_Camp_Policy_No_GenderSelected = "You should select your gender to proceed with the registration";
        public const string F_Camp_Policy_OfflineRegCheckedNotPermitToReg = "Registration for this camp is not allowed by the Owner of the club";
        public const string F_Camp_Policy_FrozenCamp = "The camp is frozen";
        public const string F_Camp_Policy_ExpiredCamp = "The camp is expired on {0}";
        public const string F_Course_Policy_StartCourse = "The course starts on {0}";
        public const string F_Course_Policy_ExpiredCourse = "The course is expired on {0}";
        public const string F_Camp_Policy_DeletedCamp = "The camp is deleted";
        public const string F_Course_Policy_DeletedCourse = "The course is deleted";
        public const string F_Event_Policy_DeletedEvent = "The event is deleted";
        public const string F_Course_Policy_No_SiblingName = "You should enter sibling name to proceed with the registration";
        public const string F_Course_DropINSessionDate_Not_Entered = "The Session date field is required.";
        public const string F_Course_SchoolLevel_Not_Entered = "The skill level field is required";
        public const string F_Tournaments_TT_Policy_NoEventsSelected = "You should select at least one event to proceed with the registration";
        public const string F_Tournaments_TT_No_Capacity = "Sorry! There is no capacity left for {0}";
        public const string F_Tournaments_TT_Policy_NoPartnerName = "You should enter your partner's name for the doubles event";
        public const string F_Tournaments_TT_Policy_NotMoreThaEvents_PerDay = "You cannot register for more than 4 events in a day";
        public const string F_Tournaments_TT_Policy_NotMoreThanEvents_AtTheSameTime = "You cannot register for more than 2 events at the same time";
        public const int F_Tournaments_TT_Policy_UnderDiscount_Year = 15;
        public const int F_Tournaments_TT_Policy_UnderDiscount_Value = 5;
        public const string F_OrderHistory_PaypalRefund_Response = "Order {0} has been refunded by {1}, refund transaction id: {2}";
        public const string F_OrderHistory_PaypalRefundItem_Response = "The {0} ({1}) has been refunded by {2}{3}. (Refunded amount: {4}) \n\r";
        public const string F_OrderHistory_PaypalRefundCharge_Response = "{0} for {1} ({2}) has been refunded by {3}{4}. (Refunded amount: {5}) \n\r";

        public const string F_OrderHistory_Installment_Paypal_Desc = "Installment {0}, refund transaction id: {1}\n\r";
        public const string F_OrderHistory_Installment_Paypal_Fail_Desc = "Installment {0}\n\r";
        public const string F_OrderHistory_Installment_Desc = "Installment {0} did not refund due to no transaction id.\n\r";

        public const string F_E_ImportClub_NotOwnedBySys = "The club {0} is not owned by the system admin and cannot be transferred.";
        public const string F_E_ImportClub_NotOwnedBySup = "The club {0} is not owned by the support admin and cannot be transferred.";
        public const string F_E_ImportClub_ClubNotExists = "The club {0} does not exist in the club table.";
        public const string F_E_ImportClub_ImportClubNotExists = "The import club {0} does not exist in the import club table.";
        public const string F_E_ImportClub_EmailNotMatched = "The email {0} does not match the original email in import club table.";
        public const string F_E_ImportClub_AccountExists = "The email {0} already has an account.";
        public const string F_E_ImportClub_DomainExists = "The new domain {0} already exists.";
        public const string F_E_DuplicateUserName = "Sorry, it looks like {0} belongs to an existing user. Would you like to ";


        public const string F_Paypal_Pay_Memo = "Payment for confirmation id: {0}";
        public const string F_Paypal_Pay_Memo_Detail = "\n\r{0}, {1} ({2}).";
        public const string F_Paypal_Pay_Memo_TakePayment = "Take a payment for confirmation: {0}.";
        public const string F_Paypal_Pay_Memo_Checkout = "Received payment for confirmation: {0}.";

        public const string F_Paypal_Pay_Memo_Installment = "Received installment payment for confirmation: {0}.";
        public const string F_Paypal_Pay_Memo_Invoice = "Received invoice payment for confirmation: {0}.";
        public const string F_Paypal_Pay_Memo_FamilyInvoice = "Received invoice payment for username: {0}";
        public const string F_Paypal_Pay_Memo_Preapproval = "Automated payment for confirmation: {0}, paid amount:{1}.";
        public const string F_Paypal_Pay_Memo_Preapproval_Detail = "\n\r{0}, {1}.";
        public const string F_Client_Pay_Memo = "Received monthly payment ({0}) for client: {1}.";
        public const string F_Connect_To_Stripe = "You have connected to stipe successfully.";
        // RegEx Pattern

        public const string Html_Tag_Pattern = "<.*?>";

        // Message strings
        public const string M_Event_RegistrationDates = "Registration end date must be equal or greater than registeration start date.";
        public const string M_Event_RegistrationStartDate = "Registration start date must be before of start date of event.";
        public const string M_Event_RegistrationStartDateRequired = "Register start date is required.";
        public const string M_Event_RegistrationEndDateRequired = "Register end date is required.";
        public const string M_Event_RegistrationEndDate = "Registration end date must be before of end date of event.";
        public const string M_Event_EventScheduleEmpty = "There is no any schedule defined.";
        public const string M_FORM_MustBeAtLeastOne = "At least one question for form should be defined.";
        public const string M_Event_Donation_TitleForDonationRequired = "Title for donation is required";
        public const string M_Ilegal_Uploaded_Pdf_Message = "You upload ilegal file! Please upload a PDF file";
        public const string M_Ilegal_Uploaded_Pdf_Image_Doc_Message = "Please upload an image or a PDF file";
        public const string M_Ilegal_Uploaded_Image_Message = "You upload ilegal file! Please upload an image";
        public const string M_Ilegal_Uploaded_Cover_MinSize_Message = "Your photo must be at least {0} * {1} px.";
        public const string M_Ilegal_Uploaded_Logo_MinSize_Message = "Your photo must be at least {0} * {1} px.";
        public const string M_ResetPasswordCannotFindAccount = "We couldn't find you using the information you entered. Please try again.";
        public const string M_ResetPasswordAccountNotYetConfirmed = "You need to confirm your email before you can reset your password.";
        public const string M_AccountNotYetConfirmed = "You need to confirm your email before you can login.";
        public const string M_UsernameOrPasswordIsIncorrect = "The email or password is wrong.";
        public const string M_AddressNotComplete = "Please enter a complete address that includes the city and state.";
        public const string M_SelectSportActivity = "Select sport activity";
        public const string M_SelectSubDomain = "Make a selection";
        public const string M_Tourney_NoTourneysSetUpYet = "You have not set up any tournaments yet";
        public const string M_Tourney_Bye_CommitByExceedsByeRounds = "Bye commit round number cannot exceed the number of rounds in the tournament.";
        public const string M_Tourney_Bye_ByeMaxExceedsByeRounds = "Maximum number of byes cannot exceed the number of rounds in the tournament.";
        public const string M_Tourney_Ef_FirstLateChargeMustExceedEf = "The first late charge must be more than the regular entry price.";
        public const string M_Tourney_Ef_SecondLateChargeMustExceedFirst = "The second late charge must be more than the first late charge.";
        public const string M_Tourney_Ef_SecondLateChargeNoFirst = "The second late charge cannot be applied without applying the first late charge.";
        public const string M_Tourney_Ef_ChargeDiscountExceedsEf = "The charge or discount amount cannot exceed the entry fee.";
        public const string M_Tourney_Ef_OnlineExceedsOnsite = "The onsite entry fee cannot be less than the online entry fee.";
        public const string M_Tourney_Date_SecondLateDateExceedsDeadline = "Second late charge date must be before the deadline.";
        public const string M_Tourney_Date_SecondLateDateLessThanFirstLateDate = "Second late charge date should be after the first late charge date.";
        public const string M_Tourney_Date_FirstLateDateExceedsDeadline = "First late charge date must be before the deadline.";
        public const string M_Tourney_Date_LateDateIsBeforeNow = "The late charge date cannot be in the past.";
        public const string M_Tourney_Section_MustBeAtLeastOne = "At least one section should be definded.";
        public const string M_Tourney_Schedule_MustBeAtLeastOne = "At least one schedule should be defined.";
        public const string M_LiveAndSandBoxItemInCart = "You cannot place your order when you have live and sandbox items in the cart. Please remove either sandbox or live items to proceed.";
        public const string M_Tourney_Schedule_StartMustBeAfterDeadline = "The schedule start date should be at least one day after registration deadline.";
        public const string M_Tourney_Schedule_EndCannotBeBeforeStart = "The schedule end date cannot be earlier than the start date.";
        public const string M_Tourney_HotelName = "Hotel name field is required.";
        public const string M_Tourney_LocationHotelMustBeChecked = "You should check the box indicating that the location is a hotel if you want to provide a hotel rate or code.";
        public const string M_Tourney_HotelCodeButNoRate = "You cannot provide hotel rate code without provding a hotel rate.";
        public const string M_Tourney_PrizeFund = "Prize Fund should be a number between {0} and {1}.";
        public const string M_Installment_Token_Invalid = "Your installment token is invalid";
        public const string M_Extra_Payment_Invalid = "Club domain or amount is invalid";
        public const string M_Paypal_Payment_Failed = "We have recieved the following errors from Paypal when processing your payment.";
        public const string M_Payment_Failed = "We have recieved the following errors when processing your payment.";
        public const string M_Paypal_PreApprovalPayment_Failed = "We have recieved errors from paypal when processing your payment, please try later.";
        public const string M_Tourney_PrizeFundBased = "Number of full entries (registrations) the prize fund is based on should be a number between {0} and {1}";
        public const string M_Event_IUnderstandNotChecked = "You must check the box indicating that you will double check the registration page.";
        public const string M_Course_NoCourseSetUpYet = "You have not set up any courses yet.";
        public const string M_Camp_NoCampsSetUpYet = "You have not set up any camps yet.";
        public const string M_Camp_Section_MustBeAtLeastOne = "At least one program for camp should be defined.";
        public const string M_Camp_Section_NameMissing = "The program name field is required.";
        public const string M_Camp_Section_HourMissing = "The program hour field is required.";
        public const string M_Camp_Section_CostMissing = "The program price field is required.";
        public const string M_Camp_Section_CapacityMissing = "The program capacity field is required.";
        public const string M_Camp_Schedule_MustBeAtLeastOne = "At least one schedule for camp should be defined.";
        public const string M_Camp_Schedule_FirstStartMustBeAfterNow = "The first camp start date cannot be today or in the past.";
        public const string M_Camp_EarlyBirdMustBeAfterNow = "The early bird discount end date cannot be today or in the past.";
        public const string M_Camp_EarlyBirdMustBeBeforeFirstStart = "The early bird discount end date should be before the start of the first camp.";
        public const string M_Camp_Schedule_EndCannotBeBeforeStart = "Camp end date cannot be before the start date.";
        public const string M_Tourney_Schedule_StartMustBeAfterPreviousEnd = "Camp start date should be after the previous camp end date.";
        public const string M_Camp_Section_EarlyBirdDiscountMissing = "The early bird discount amount field is required.";
        public const string M_Camp_Section_MultipleWeekDiscountMissing = "The multiple weeks discount field is required.";
        public const string M_Camp_Section_TotalDiscountsTooMuch = "The total available discounts of {0} for the program exceeds {1} of the program cost of {2}. Note that a camper can select all the available discounts.";
        public const string M_Camp_Section_AmName = "Morning session";
        public const string M_Camp_Section_MdName = "Midday session";
        public const string M_Camp_Section_PmName = "Afternoon session";
        public const string M_Camp_Section_FullName = "Full day session";
        public const string M_Camp_CD_MultipleWeek = "multiple weeks";
        public const string M_Camp_CD_EarlyBird = "early bird";
        public const string M_Camp_CD_Sibling = "Sibling";
        public const string M_Camp_CD_ClubMembership = "club membership";
        public const string M_Camp_CD_Lunch = "lunch";
        public const string M_Camp_CD_MorningChildCare = "morning extended care";
        public const string M_Camp_CD_AfternoonChildCare = "afternoon extended care";
        public const string M_Course_EarlyBirdEndDate_IsRequired = "The early bird end date field is required.";
        public const string M_Course_EarlyBirdMustBeAfterNow = "The early bird discount end date cannot be today or in the past.";
        public const string M_Course_EarlyBirdMustBeBeforeFirstStart = "The early bird discount end date should be before the start of the class.";
        public const string M_Course_EarlyBird_Required = "The early bird discount field is required.";
        public const string M_Course_StartDate_Required = "The Start date field is required.";
        public const string M_Course_FinishDate_Required = "The Finish date field is required.";
        public const string M_Course_FullCapacity = "The class is full.";
        public const string M_Course_SpotsLeft = "{0} spots left.";
        public const string M_Course_EndMustBeAfterNow = "The class end date cannot be today or in the past.";
        public const string M_Course_WeeksCount_Required = "The number of weeks field is required.";
        public const string M_Course_SessionPrice_Required = "The session price field is required.";
        public const string M_Course_WeeksCount_Must_Positive = "The number of weeks field should be greater than 0.";
        public const string M_Course_Schedule_EndCannotBeBeforeStart = "The end date field cannot be earlier than the start date.";
        public const string M_Course_Time_EndCannotBeBeforeStart = "The finish time field cannot be earlier than the start time.";
        public const string M_Course_Schedule_TimeRange = "Time should be in a valid range.";
        public const string M_Course_Discount_MustBeLessThanPrice = "The discounts amount cannot exceed the program price.";
        public const string M_Course_FullPrice = "The program price field is required.";
        public const string M_Course_DropIn_MustBeLessThanPrice = "The drop in session price cannot exceed the program price.";
        public const string M_Course_DropIn_MustBeGreaterThanZero = "The drop in session price must be a positive number";
        public const string M_Course_Must_Check_Accept = "You must check the box indicating that you will review the system generated class and registration page.";
        public const string M_Event_Must_Check_Accept = "You must check the box indicating that you will review the system generated event and registration page.";
        public const string M_Course_Must_Check_AtLeast_OneDay = "At least one day in week should be selected to proceed with the submit.";
        public const string M_Change_Password_Error = "The current password is incorrect or the new password is invalid.";
        public const string M_Class_IUnderstandNotChecked = "You must check the box indicating that you will review the registration page.";
        public const string M_Camp_Location = "The club location should be selected";
        public const string M_CamSchedule = "The 'Has any reduced price' is checked but you did not provide a reduced price for the sessions you have defined in your camp.";
        public const string M_Course_SessionCount = "No class session can be generated in the selected date span.";
        public const string M_Tourney_Coverage_File = "No file is selected.";
        public const string OutsourcerIsRequired = "The outsourcer should be selected.";
    
        // Exceptions
        public const string M_E_Db = "Jumbula database exception";
        public const string M_E_NotFoundResource = "Jumbula resource not found";
        public const string M_E_Payment = "Jumbula payment exception";
        public const string M_E_Account = "Jumbula account management exception";
        public const string M_E_Import = "Jumbula club import exception";
        public const string M_E_Misc = "Jumbula miscellaneous exception";
        public const string M_E_CreateEdit = "Error in establish connection with server, please try again.";
        public const string M_E_SequenceContainsNoElements = "sequence contains no elements";

        // Etc
        public const string E_ClubLogoAlt = "logo";
        public const string E_ImageGalleryThumnailAlt = "thumnail";
        public const string E_PdfFileExt = "pdf";
        public const string E_ExcelFileExt = "xls";
        public const string E_Pdf_MimeType = "application/pdf";
        public const string E_Excel_MimeType = "application/vnd.ms-excel";
        public const string E_Binary_MimeType = "application/octet-stream";

        //  Email
        public const string SendMail_NoEmail_Error = "No email is selected!";
        public const string SendMail_Status_Error = "Sorry, there was an error in sending emails. Please try again.";
        public const string SendMail_Status_Success = "Your message has been sent successfully.";

        public const string SendMail_Body_Required = "Body field is required.";

        // Session Keys
        public const string Membership_Session_Key = "JBMembership";

        // Profile
        public const string Profile_ChangePassword_Failed_Invalid_UserName = "Failed to change password .Invalid UserNam";
        public const string Profile_ChangePassword_Failed_Invalid_Password = "Failed to change password. Invalid Password";

        // Primary Categories
        public const string Category_Chess = "Chess";
        public const string Category_TableTennis = "Table Tennis";

        // Schema.org 
        public const string Itemtype_LocalBusiness_Url = "http://www.schema.org/LocalBusiness";
        public const string Itemtype_PostalAddress_Url = "http://www.schema.org/PostalAddress";
        public const string Itemtype_GeoCoordinate_Url = "http://www.schema.org/GeoCoordinates";
        public const string Itemtype_SportEvent_Url = "http://www.schema.org/SportsEvent";
        public const string Itemtype_Location_Url = "http://www.schema.org/location";
        public const string Itemtype_Subevent_Url = "http://www.schema.org/subEvent";

        public const string M_UploadFlyer = "Event flyer shoulde be uploaded";
        public const int M_MaxAge = 120;

        public const string DropInSessionDate = "Drop in session date should be a valid date.";
        public const string StartRegisterDate = "Start register date should be before {0} and after today.";
        // Ages and grades
        public const string Grades_Abbreviation = "{0} - {1} grades";
        public const string Years_Abbreviation = "{0} - {1} years";
        public const string Min_Grade_Abbreviation = "Min {0} grade";
        public const string Max_Grade_Abbreviation = "Max {0} grade";
        public const string Min_Years_Abbreviation = "Min {0} years";
        public const string Max_Years_Abbreviation = "Max {0} years";

        public const string Age_Restriction_Message = "All participants must be from {0} to {1} years old.";
        public const string Min_Age_Restriction_Message = "All participants must be at least {0} years old.";
        public const string Max_Age_Restriction_Message = "All participants must be at most {0} years old.";

        public const string Grade_Restriction_Message = "All participants must currently be in grade {0} - {1}.";
        public const string Min_Grade_Restriction_Message = "All participants must be at least in {0} grade.";
        public const string Max_Grade_Restriction_Message = "All participants must be at most in {0} grade.";

        public const string Player_Age_Message = "According to your profile information you are {0} years old. If your age is not correct please update your profile information first. Otherwise you cannot enrol for this event.";
        public const string Player_Grade_Message = "According to your profile information your grade is {0}. If your grade is not correct please update your profile information. Otherwise you cannot enrol for this event.";

        public const string Trophy = "Trophy";
        public const string Chess_Tourney_Prize_Fund = "Prize fund: {0}";

        public const string RequiedFieldErrorMessage = "The {0} field is required.";
        public const string EventType = "Tournament type should be selected.";
        public const string RegisterationDeadLine = "Registeration deadline field is required.";
        public const string M_Event_SelectCategory = "The category should be selected";
        public const string TemplateNameISRequired = "The template name field is required.";
        public static readonly string[] LegalFlyerype = new[] { ".gif", ".png", ".jpeg", ".jpg", ".doc", ".pdf", ".docx" };
        public static readonly string[] LegalAttachmentType = new[] { ".gif", ".png", ".jpeg", ".jpg", ".doc", ".pdf", ".docx", ".zip", ".rar", ".pptx", "xls" };
        public static readonly string LegalCoverageType = ".txt,.htm,.html";
        // Paypal
        public const string Paypal_Unsuccessful_Payment_Message = "Sorry, but your payment transaction cannot be proceed. Please review the following messages and try again, or select another payment method.";
        public const string Paypal_Unsuccessful_Refund_Message = "The refund transaction cannot be proceed. Please contact paypal for further information.";
        public const string Card_Number_Validation_Message = "Card type and card number do not match.";
        public const string Card_Digits_Validation_Message = "The card security code must be {0} digits";
        public const string Paypal_Result_Status = "ack";
        public const string Paypal_Result_Succeed = "success";
        public const string Paypal_Refund_TransactionID = "REFUNDTRANSACTIONID";
        //----------------------------------------
        // Coupon Constants
        public const string ShouldBeSelected = "The {0} Should be selected.";
        public const string EndDateCanNotEarlier = "The end date cannot be earlire from start date.";
        public const string ExistCode = "The code is exist. Please choose another code.";
        public const string NotExistCode = "The code is not valid.";
        public const string NotBelong = "The code cannot be applied to your cart.";
        public const string IsExpired = "The code is expired.";
        public const string CouponIsInUse = "Note: the coupon is already in use.";
        public const string MailUserCount = "The email count cannot be less than one";

        //---------------------------------------------------------
        // installment Constants
        public const string EarlierFromTodayOrPast = "The {0} connot be earlier from today or in past.";
        public const string Installment_Fixed_Desc = "Installment Payment";
        //-------------------------Report---------------------------
        public const string DeleteDialog_Question_Report = "Are you sure you want to delete this report?";
        public const string DeleteDialog_Title_Report = "Delete Report";

        //--------JbForm @mn---------
        //public const string JbFormParticipantSectionName = "ParticipantSection";

        public const string Default_Head_Instructor_Label = "Head instructor";
        public const string Default_Assistant_Instructor_Label = "Assistant instructor";
        public const string Default_Team_ChessTournament_Form = "Dafault team registration form";

        public const string Default_School_Form = "Default school";

        public const string Index_Task_InProgress = "An index task is in progress.";

        public const string M_Program_Not_Exist = "The program is not exist.";

        // Report Name
        public const string ReportName = "{0} ({1}).xls";

        // Email Campaign
        public const string MialList_UploadedListName = "{0} UploadedList.csv";

        public const string W_Transfer_Fee = "Transfer fee";

        public const string W_Application_Fee = "Application fee";

        public const string W_First_Subscription = "First subscription";

        public const string W_Edit_Fee = "Edit fee";
        public const string W_Cancel_Fee = "Cancellation fee";
        public const string W_Prorate_Fee = "Prorate fee";
        public const string W_Sandbox_Paypal_Email = "paypayseller@jumbula.com";
        public const string F_Cancel_ErrorSendgrid = "{0} get the error from sendgrid for cancel campaign";

        public const int MinimumEnrollment_Default = 1;
        public const int CapacityLeftNumber_Default = 3;

        public const string DefaultRestrictionPinMessage = "Please enter your access password to register for this season. If the password does not work or you do not have one, contact your organization directly.";
        public const string DefaultEarlyRestrictionPinMessage = "​​Please enter the early registration authorization code you received from your coordinator.";
        public const string DefaultWaitlistPolicy = "We will contact you if a spot becomes available. Waitlists are free of charge, the payment will be due upon actual registration in the program.";

        public const string AddedToWaitlistEmailSubject = "Program added to waitlist";

        public const string ChangeEmailLogin = "Change email login";
        public const string ChangePassword = "Change password";

        #region Validation Messages
        public const string E_SelectedUser = "You should select at least one user.";
        public const string E_SelectedProgram = "You should select at least one program.";
        public const string E_SelectedInstallment = "You should select at least one installment.";
        public const string E_PercentageAmount = "Percentage amount must be between 1 and 100.";
        public const string E_Amount = "Amount is required.";
        public const string E_Amount_Between = "Amount must be between {0} to {1}.";
        public const string E_Balance_Zero = "The Balance is zero and you can not make any other payment.";
        #endregion

        #region History Message
        public const string Order_Refund = "The amount {0} is refunded. Memo: {1}";

        public const string Order_Initiated = "Order {0} is placed.";
        public const string Order_Paid = "Order {0} is placed and paid.";
        public const string Order_Preapprovad_Paid = "The amount ${0} is paid for order {1}.";

        public const string OrderInstallment_Paid = "The amount ${0} is paid for the installment due date on {1}.";
        public const string Order_Prepaid = "Order {0} is placed and paid.";
        public const string Order_MakePayment = "The amount ${0} is recieved. Memo: {1}";
        public const string Order_FamilyMakePayment = "The amount {0} is recieved. Memo: {1}";
        public const string Order_InvoiceHistory = "The amount ${0} is recieved.";
        public const string Order_Edit_MakePayment = "The amount ${0} is updated on {1}. Memo: {2}";
        public const string Order_Delete_MakePayment = "The payment {0} is deleted. Memo: {1}";

        public const string Weekly_Email_Msg = "This provider has committed to sending regular class updates.";

        public const string StripeCheckoutLabel = "Credit card";
        public const string AchPaymentCheckoutLabel = "ECheck";
        public const string PaypalCheckoutLabel = "Paypal";
        public const string CashOrCheckCheckoutLabel = "Cash or check";
        public const string BankDepositCheckoutLabel = "Bank deposit";


        public const string Invoice_Created = "You created a {0} invoice.";
        public const string Invoice_Sent = "You sent a {0} invoice to {1}.";
        public const string Invoice_Paided = "{0} made a {1} payment.";
        public const string Invoice_TakePayment = "{0} recorded a {1} payment.";
        public const string Invoice_TakePaymentWithCreditcard = "{0} charged {1} to credit card.";
        public const string Invoice_TakePaymentWithPayPal = "{0} charged {1} to PayPal.";
        public const string Invoice_TakePaymentWithCredit = "{0} charged {1} to credit.";
        public const string Invoice_Reminder = "{0} sent an invoice reminder.";
        public const string Invoice_ReminderWithCcEmail = "{0} sent an invoice reminder (Cc to {1}).";
        public const string Invoice_AdminCancelationSubject = "Invoice cancellation to {0}";
        public const string Invoice_ParentCancelationSubject = "Invoice cancellation from {0}";
        public const string Invoice_AdminReminderSubject = "Invoice reminder to {0}";
        public const string Invoice_ParentReminderSubject = "Invoice reminder from {0}";
        #endregion

        public const int DefaultTokenLifeTime = 20;
        public const int MinimumCapacityInSearch = 8;
        public const int MaximumCapacityInSearch = 20;
        public const int MaximumCapacity = 250;

        // Apearance
        public const string DefaultERButtonBackColor = "#F78B0F";
        public const string DefaultERButtonTextColor = "#FFFFFF";
        public const string DefaultERButtonTextLabel = "Express Registration";

        public const string DefaultContactBoxBackColor = "#249ab5";
        public const string DefaultTestimonialsBoxBackColor = "#249ab5";
        public const string DefaultTestimonialsBoxColor = "#FFF";
        public const string DefaultTestimonialsTitle = "Testimonials";

        public const string DefualtPriceTextColor = "#FFFFFF";
        public const string DefaultTuitionLabeltextColor = "#2d87cd";
        public const string DefaultPriceBackColor = "#2d87cd";

        public const string DefaultERButtonAddToWaitlistLabel = "Add To Waitlist";
        public const string DefaultFamilyClubDomain = "family";

        public const string LastDateForGetCartOrders = "2017-08-16 13:00:00.723";

        public const string RightAtSchoolDomain = "rightatschool";



        public const string DefaultSuccessMessage = "Operation done successfully";
        public const string DefaultFailMessage = "Something went wrong";
        public const string DefaultRegistrationButtonTitle = "Express Register";

        public const string YoungRembrandstDomain = "young-rembrandst";
        public const string ProceedWithPassword = "Proceed with password";

        public const string UnlimitedCapacity = "Unlimited";
    }
}
