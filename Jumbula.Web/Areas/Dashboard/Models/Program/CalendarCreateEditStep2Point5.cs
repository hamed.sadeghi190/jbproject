﻿using SportsClub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CalendarCreateEditStep2Point5
	{
        public string ClubDomain { get; set; }

        public string SeasonDomain { get; set; }

        public string Domain { get; set; }

        public PageMode PageMode
        {
            get
            {

                if (this.PageStep <= this.LastCreatedPage)
                {
                    return PageMode.Edit;
                }

                return PageMode.Create;
            }
        }

        public ProgramPageStep PageStep { get; set; }

        public ProgramPageStep LastCreatedPage { get; set; }

        public bool FromTemplate { get; set; }

        public DateTime Date { get; set; }

        public DateTime EndDate { get; set; }

        public List<CalendarScheduleViewModel> Schedules { get; set; }
	}

    public class CalendarScheduleViewModel
	{
        public long Value { get; set; }

        public string Text { get; set; }

        public string Color { get; set; }
	}
}