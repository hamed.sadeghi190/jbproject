﻿using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jumbula.Core.Domain
{
    public class Email : BaseEntity
    {
        [StringLength(255)]
        public string FromEmail { get; set; }

        [StringLength(255)]
        public string FromDisplayName { get; set; }

        [StringLength(255)]
        public string ReplyTo { get; set; }

        public DateTime DateCreated { get; set; }

        [StringLength(255)]
        public string Subject { get; set; }

        public EmailCategory Category { get; set; }

        public string SubCategory { get; set; }

        [NotMapped]
        public EmailScheduleType ScheduleType => ScheduleDate.HasValue ? EmailScheduleType.Scheduled : EmailScheduleType.ImmediateSend;

        [DefaultValue(EmailSendPriority.Normal)]
        public EmailSendPriority Priority { get; set; }

        public EmailSendStatus Status { get; set; }
        public DateTime? SentDate { get; set; }

        public EmailCallerType CallerType { get; set; }

        public DateTime? ScheduleDate { get; set; }

        public int? TemplateId { get; set; }

        public string Parameters { get; set; }

        public string Response { get; set; }

        public int AttemptNumber { get; set; }

        public bool IsCampaignMode { get; set; }

        public int? UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual JbUser User { get; set; }

        [ForeignKey("TemplateId")]
        public virtual EmailTemplate EmailTemplate { get; set; }

        public DateTime? LastAttemptTime { get; set; }

        public virtual ICollection<EmailRecipient> Recipients { get; set; }

        public virtual ICollection<CommunicationHistory> Histories { get; set; }

        public virtual ICollection<EmailAttachment> Attachments { get; set; }
    }

}
