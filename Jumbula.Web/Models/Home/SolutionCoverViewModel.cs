﻿namespace SportsClub.Models.Home
{
    public class SolutionCoverViewModel
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string CoverBackground { get; set; }
    }
}