﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('clubFlyersController', ['$scope', '$http', 'jbToast', '$log', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, jbToast, $log, $stateParams, $state, jbConfirmation) {
        window.app.globalObjects.pageClass = 'campaign-templates';
        window.app.globalObjects.initializeView();


        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/Flyer';

        $scope.flyerDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetAllFlyers',
                        type: "POST",
                        data: function () {
                            return {
                                term: $scope.searchTerm,
                            };
                        },
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: '',
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: null
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverPaging: true,
            pageSize: 50,
        });

        $scope.FillTemplateGrid = {
            dataSource: $scope.flyerDataSource,
            sortable: true,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            columns: [
                {
                    field: "Name",
                    title: "Name",
                    template: "",
                    width: "200px",
                },
                {
                    field: "SchoolName",
                    title: "School",
                    template: "",
                    width: "200px",
                },
                {
                    field: "SeasonName",
                    title: "Season",
                    template: "",
                    width: "200px",
                },
                {
                    field: "",
                    title: "Actions",
                    width: "70px",
                    headerTemplate: "<span class='actions-title'>Actions</span>",
                    template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                        <li> ... \
                            <ul>\
                                <li  class='k-item k-state-default k-first' role='menuitem'>\
                                           <a class='link-get-hover-color jbi-download'  href='/dashboard/Flyer/Generate?Id=#=Id#' download>Download PDF</a>\
                                      </li>\
                                    <li  ui-sref='FlyerEdit({flyerId: \"#=Id#\"})'>\
                                          <span  class='k-link jbi-pencil'>Rename</span>\
                                    </li>\
                                      <li ng-click='delete(#= Id #)'>\
                                          <span  class='k-link jbi-remove'>Delete</span>\
                                    </li>\
                            </ul>\
                        </li>\
                    </ul>",
                }
            ],
        };

    
        $scope.viewFlyer = function (id) {

            $http.post(baseUrl + '/Generate', {
                Id: id,
            }, {
                headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
            }).success(function (data) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                var blob = new Blob([data], { type: "application/pdf" });

                var fileURL = URL.createObjectURL(blob);
                a.href = fileURL;
                a.target = '_self';
                
                a.download = "FlyerTemplate1.pdf";
                a.click();
            });


        }

        $scope.delete = function (id) {
            jbConfirmation
                .yes(function () {
                    $http.post(baseUrl + '/DeleteFlyer', { id: id })
                        .success(function (data) {
                            console.log(data.Status);
                            if (data.Status == true) {

                                jbToast.success(data.Message);
                                $scope.FillTemplateGrid.dataSource.read();
                                $('#FillTemplateGrid').data('kendoGrid').refresh();
                            } else {
                                $scope.MC.handleErrors($scope, data);
                            }
                        });
                })
                .confirm();
        }
    }]);
})();