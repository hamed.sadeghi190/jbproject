

CREATE TABLE [dbo].[UserCheckins](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProgramId] [bigint] NOT NULL,
	[SessionId] [int] NOT NULL,
	[LocalDateTime] [datetime] NOT NULL,
	[ClubDateTime] [datetime] NOT NULL,
	[Lng] [float] NULL,
	[Lat] [float] NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.UserCheckins] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[UserCheckins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserCheckins_dbo.JbUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[JbUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[UserCheckins] CHECK CONSTRAINT [FK_dbo.UserCheckins_dbo.JbUsers_UserId]
GO

ALTER TABLE [dbo].[UserCheckins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserCheckins_dbo.Programs_ProgramId] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Programs] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[UserCheckins] CHECK CONSTRAINT [FK_dbo.UserCheckins_dbo.Programs_ProgramId]
GO

ALTER TABLE dbo.ScheduleAttendances
  ADD CONSTRAINT iniqueKeys UNIQUE (ScheduleId, ProfileId, SessionId)