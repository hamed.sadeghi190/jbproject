﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('instructorAssignForProgramsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbToast) {

        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;
        $scope.seasonDomain = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        $scope.init = function () {
            app.globalObjects.initializeView();
        }

        $scope.Model = {};

        $http.get(window.location.origin + '/Dashboard/Season/GetAllProgramsInstructors', { params: { seasonDomain: $scope.seasonDomain } }).success(function (data, status, headers, config) {

            $scope.Model = data.Data;
        });

        $scope.save = function () {
            $http.post(window.location.origin + '/Dashboard/Season/SaveAssignInstructorsPrograms', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                   
                    jbToast.success("Instructors assigned successfully.");

                })
        }
    }]);
})();