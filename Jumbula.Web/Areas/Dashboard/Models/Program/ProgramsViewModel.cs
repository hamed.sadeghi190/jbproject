﻿
using Jumbula.Common.Enums;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ProgramsViewModel
    {
        public long Id { get; set; }
        public string ClubDomain { get; set; }
        public string SeasonDomain { get; set; }
        public string Name { get; set; }
        public ProgramTypeCategory TypeCategory { get; set; }

    }
}