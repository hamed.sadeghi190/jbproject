﻿namespace Jumbula.Common.Enums
{
    public enum ReportFilterAction
    {
        PrepareData = 0,
        LoadGrid = 1,
        NoAction = 2
    }
}
