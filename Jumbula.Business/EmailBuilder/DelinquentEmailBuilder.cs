﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Email;
using Jumbula.Core.Model.Generic;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using Email = Jumbula.Common.Constants.Email;

namespace Jumbula.Business.EmailBuilder
{
    public class DelinquentEmailBuilder : CustomizableEmailBuilder<DelinquentEmailParameterModel>
    {
        #region Contstructors

        public DelinquentEmailBuilder(IClubBusiness clubBusiness, IEmailTemplateBusiness emailTemplateBusiness, IRepository<EmailTemplate> emailTemplateRepository, IOrderItemBusiness orderItemBusiness, IOrderInstallmentBusiness orderInstallmentBusiness) : base(clubBusiness, emailTemplateBusiness, emailTemplateRepository)
        {
            _clubBusiness = clubBusiness;
            _orderItemBusiness = orderItemBusiness;
            _orderInstallmentBusiness = orderInstallmentBusiness;
        }

        #endregion

        #region Fields
        private DelinquentEmailParameterModel _parameters;
        private Club _club;

        private readonly IClubBusiness _clubBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IOrderInstallmentBusiness _orderInstallmentBusiness;
        #endregion

        #region Constants

        private const string Subject = "Delinquent account";
        #endregion

        #region Properties
        public override EmailCategory Category => EmailCategory.Delinquent;

        public override string TemplateName => string.Empty;

        public override EmailCallerType CallerType => EmailCallerType.User;

        public override EmailSendPriority EmailSendPriority => EmailSendPriority.Normal;
        #endregion

        #region Public Methods

        public override void Initiate(DelinquentEmailParameterModel parameters)
        {
            _parameters = parameters;
            _club = _clubBusiness.Get(_parameters.ClubId);
        }

        public override void InitiatePreview(DelinquentEmailParameterModel parameters)
        {
            
        }

        public override string GenerateBody(Core.Domain.Email email, EmailBodyGenerationMode previewMode = EmailBodyGenerationMode.Normal)
        {
            var parameterModel = base.GetParameter(email.Parameters);

            var template = GetEmailTemplate(email.TemplateId);
            var body = parameterModel.Body;

            template.Body = body;
            template.PreviewMode = previewMode;

            return ViewHelper.RenderViewToString(Email.GeneralEmailTemplatePath, template);
        }

        public override object BuildBodyModel(string subCategory, EmailBodyGenerationMode previewMode = EmailBodyGenerationMode.Normal)
        {
            return new DelinquentEmailBodyModel();
        }

        public override MailAddress GetFrom(string subCategory = null)
        {
            return new MailAddress(string.Format(Constants.W_JumbulaEmailFormat, _club.Domain));
        }

        public override string GetFromDisplayName(string subCategory = null)
        {
            return _club.Name;
        }

        public override List<CommunicationHistory> GetHistories()
        {
            return _parameters.RelatedEntity.Select(x => new CommunicationHistory { RelatedEntityId = x.RelatedEntityId, RelatedEntity = x.RelatedEntityName }).ToList();
        }

        public override string GetReplyTo(string subCategory = null)
        {
            return string.Empty;
        }

        public override string GetSubject(string subCategory = null)
        {
            return Subject;
        }

        public override IEnumerable<MailAddress> GetTos(string subCategory = null)
        {
            var tos = _orderItemBusiness.GetOrderItems(_parameters.RelatedEntity.Where(x => x.RelatedEntityName == nameof(OrderItem)).Select(x => x.RelatedEntityId).ToList()).ToList().Select(x => new MailAddress(x.Order.User.UserName)).DistinctBy(x => x.Address).AsEnumerable();

            tos = tos.Union(
                _orderInstallmentBusiness.GetOrderInstallments(_parameters.RelatedEntity.Where(x => x.RelatedEntityName == nameof(OrderInstallment)).Select(x => x.RelatedEntityId).ToList()).ToList().Select(x => new MailAddress(x.OrderItem.Order.User.UserName)).DistinctBy(x => x.Address).AsEnumerable());

            return tos;
        }

        public override List<SelectKeyValue<int>> GetFromList(Club club)
        {
            var fromList = _clubBusiness.GetAllSchoolsAndProviders().Where(x => x.PartnerId == club.Id).ToList();

            if (club.PartnerClub != null) fromList.Add(club.PartnerClub);
            fromList.Add(club);

            return fromList.Select(y => new SelectKeyValue<int> { Value = y.Id, Text = string.Format(Constants.W_JumbulaEmailFormat, y.Domain) }).ToList();
        }

        public override List<SelectKeyValue<int>> GetReplyToList(Club club)
        {
            return GetClubContacts(club);
        }

        public override List<SelectKeyValue<int>> GetCcList(Club club)
        {
            return GetClubContacts(club);
        }

        public override List<SelectKeyValue<int>> GetBccList(Club club)
        {
            return GetClubContacts(club);
        }
        #endregion
    }
}
