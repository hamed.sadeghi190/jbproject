﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsClub.Models.Sistem
{
    public class DbConnetionTestItem
    {
        public int Id { get; set; }

        public string Query { get; set; }

        [Display(Name = "EF elapsed time")]
        public long EfElapsedTime { get; set; }

        [Display(Name = "ADO elapsed time")]
        public long ADOElapsedTime { get; set; }
    }
}