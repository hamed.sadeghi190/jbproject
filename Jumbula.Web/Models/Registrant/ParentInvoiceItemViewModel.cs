﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MailChimp.Ecomm;


namespace SportsClub.Models
{
    public class ParentInvoiceItemViewModel
    {
        public List<ParentInvoiceOrderViewModel> Orders { get; set; }
        public List<ParentInvoiceOrderItemViewModel> OrderItems { get; set; }
        public List<ParentInvoiceInstallmentViewModel> Instalments { get; set; }
    }
}