﻿using Jb.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;
using Jumbula.Core.Domain.Generic;

namespace Jumbula.Core.Business
{
    public interface IProgramSessionBusiness
    {
        ProgramSession Get(int id);

        OperationStatus Create(Program program, int dueDate = 0);

        List<ProgramSchedulePart> Create(ProgramSchedule programSchedule, int dueDate);

        IQueryable<ProgramSession> GetList();
        IQueryable<ProgramSession> GetList(Expression<Func<ProgramSession, bool>> predicate);

        List<ProgramSession> GetList(Program program);

        List<ProgramSession> GetList(ProgramSchedule programSchedule, DateTime desiredStartDate, List<DayOfWeek> days);

        List<ProgramSession> GetList(DateTime date);
        List<ProgramSession> GetList(ProgramSchedule programSchedule);

        List<ProgramSession> GetList(List<ProgramSchedule> programSchedules);

        ProgramSession GetSameDayProgramSession(int programSessionId);
        ProgramSession GetSameDayProgramSession(ProgramSession programSession);

        int GetNumberOfTakenPlaces(ProgramSession programSession);

        int GetLeftSpots(ProgramSession programSession, int capacity);

        bool IsFull(ProgramSession programSession, int capacity);
        bool IsFull(ProgramSession programSession);

        OperationStatus EditAgenda(int sessionId, string text);

        OperationStatus StopSendAgenda(int sessionId);

        OperationStatus ScheduleSendAgenda(int sessionId);

        OperationStatus StopSendAgenda(DateTime date, int clubId);

        OperationStatus ScheduleSendAgenda(DateTime date, int clubId);

        DateTime GetAgendaDeliverDate(ProgramSession programSession);

        bool IsStatusChangeAllowed(ProgramSession programSession);

        bool IsProccessed(ProgramSession programSession);

        int MigrateFlexProgramSessions(int offset, int start);

        int MigrateFlexOrderSessions(int offset, int start);
        OperationStatus CreateScheduleSessions(List<ProgramSchedule> schedules);

        OperationStatus DeleteLogicaly(ProgramSchedule schedule);
        OperationStatus Delete(ProgramSchedule schedule);
        List<ProgramSession> GetProgramSessions(ProgramSchedule programSchedule);

        OperationStatus CreateTutionSessions(List<ProgramSchedule> schedules, List<Charge> charges, List<ProgramSession> updatedSession = null);

        List<ProgramSession> GetProgramSessions(List<Charge> charges);

        List<ProgramSession> GetList(Charge charge);

        void UpdateProgramSessionsTime(Program program);

        OperationStatus UpdateSessionWithStartEndDateProgram(Program program, List<ProgramSchedulePart> deletedSchedulePartsByEndDate, List<ProgramSchedulePart> deletedSchedulePartsByStartDate, bool isProgramStartDateChanged, bool isProgramEndDateChanged);

        void ApplyHolidays(Program program, List<ProgramSession> programSessions);

        ClassSessionsDateTime GetFirstSession(OrderItem orderItemm, bool generateSession = true);

        ClassSessionsDateTime GetLastSession(OrderItem orderItemm, bool generateSession = true);
    }
}
