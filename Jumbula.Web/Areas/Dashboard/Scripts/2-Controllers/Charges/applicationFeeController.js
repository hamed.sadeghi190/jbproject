﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('applicationFeeController', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'ApplicationFee_Add';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;

        $scope.baseUrl = window.location.origin + "/Dashboard/Charge";

        $http.get($scope.baseUrl + '/GetSeasonApllicationFee?seasonDomain=' + $scope.MC.stateParams.seasonDomain).success(function (data, status, headers, config) {
            $scope.Model = data;
        });
        $scope.PageMode = "";

        console.log($stateParams.type);

        if ($stateParams.type == "Add") {

            $scope.PageMode = "Add";

        } else {

            $scope.PageMode = "Edit";
        }

        $scope.saveApplicationFee = function () {

            $http.post($scope.baseUrl + '/SaveApplicationFee', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $state.go('Charges', { seasonDomain: $scope.MC.stateParams.seasonDomain })
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        };

        $scope.getProgramsList = function () {
            if ($scope.Model && $scope.Model.ProgramsList) {
                return $scope.Model.ProgramsList;
            } 
        }

        $scope.changeEB = function (hasEarlyBird) {

            if (!hasEarlyBird) {
                $scope.Model.ExpireDate = null;
                $scope.Model.EarlyBirdAmount = null;
            }
        }

    }]);

})();