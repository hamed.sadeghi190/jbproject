﻿using Jb.Framework.Common;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Core.Model.Club;
using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using OperationStatus = Jumbula.Core.Data.OperationStatus;
using System.Web.Mvc;

namespace Jumbula.Core.Business
{
    public interface IClubBusiness
    {
        IQueryable<Club> GetList();
        IQueryable<Club> GetSchools();
        Club Get(string domain);
        Club Get(int clubId);
        Club GetBaseInfo(string domain);
        dynamic GetInvoiceSetting(Club club);
        dynamic GetCatalogSetting(int partnerId);
        bool IsClubSchool(ClubType clubtype);
        bool IsClubSchool(int typeId);
        bool IsClubProvider(ClubType clubtype);
        OperationStatus Delete(int clubId);
        OperationStatus Create(Club club);
        IQueryable<JbUser> GetAllUserWithSpecificRoleForClubs(int clubId, RoleCategory role);

        IQueryable<JbUser> GetAllUserWithSpecificRolesForClubs(int clubId, List<RoleCategory> roles);
        IQueryable<JbUser> GetActiveUsersWithSpecificRoles(int clubId, List<RoleCategory> roles);

        DateTime GetClubDateTime(Club club, DateTime utcDate);

        DateTime GetClubDateTime(int clubId, DateTime utcDate);

        DateTime GetClubDateTime(string clubDomain, DateTime utcDate);

        string GetClubLogoURL(string Domain, string logo);
        string GetClubLogo(Club club);
        List<ClubWaiver> GetWaivers(int id);
        Club GetPartner(string clubDomain);

        PartnerSetting GetPartnerSetting(int partnerId);

        OperationStatus Update(Club club, List<JbUserRole> deletedUserRoles = null);

        OperationStatus DeleteWaiver(int id);

        OperationStatus UpdateWaiver(ClubWaiver clubWaiver);

        OperationStatus CreateWaiver(ClubWaiver clubWaiver);

        List<ClubWaiver> GetWaivers(IEnumerable<int> ids);

        IEnumerable<EmailTemplate> GetTemplate(int clubId, TemplateType temType);

        OperationStatus UpdateClubMembers(string clubDomain, int userId);

        int UpdateClubMembers(List<AllClubMembers> membersUserIdList);


        OperationStatus DeleteFormTemplate(int id);

        List<ClubFormTemplate> GetFormTemplates(int clubId, FormType? formType);

        List<ClubFormTemplate> GetFormTemplates(int clubId, IEnumerable<int> ids, FormType? formType);

        OperationStatus AddDefaultTemplates(int clubId);

        OperationStatus AddDefaultChessTeamTemplate(int clubId);
        bool AddUserInClubUsers(int userId, int clubId);
        bool AddUserInClubUsers(Order order);
        Club GetClubByClientId(long clientId);
        OperationStatus AddFormTemplate(int clubId, JbForm jbForm, string templateName, FormType formType, DefaultFormType? defaultFormType = null);
        OperationStatus AddFormTemplate(long clubId, int jbFormId, string templateName, FormType formType, DefaultFormType? defaultFormType = null);
        int GenerateDonationForm();

        List<ClubStaff> GetClubsByMember(string userName);
        //IQueryable<Club> GetClubsByMember(string userName);
        IQueryable<Club> GetClubsByMember(int userId);
        OperationStatus CreateStaff(ClubStaff clubStaff);
        List<ClubStaff> GetStaffsByUserName(string userName, RoleCategory? currentUserRole = null);

        ClubStaff GetStaff(int staffId);

        ClubStaff GetStaff(string userName, int clubId);

        ClubStaff GetStaff(int userRoleId, int clubId);

        IQueryable<ClubStaff> GetStaffs(int clubId);

        bool IsClubDomainExist(string domain, int clubId = 0);

        IQueryable<Club> GetRelatedClubs(int partnerId, bool includeDestricts = true);

        IQueryable<Club> GetRelatedClubs(string partnerDomain);

        IQueryable<ClubType> GetClubTypeList();
        IQueryable<Club> GetAllPartnerSchools(int partnerId);
        IQueryable<Club> GetAllPartnerProviders(int partnerId);
        IQueryable<ClubStaff> GetClubInstructors(int clubId);
        bool HasDefaultBillingCard(int id);

        bool IsInstructorIsInUse(int staffId);

        bool IsStaffCanChange(ClubStaff clubStaff);

        bool IsStaffCanChange(int staffId);
        OperationStatus DeleteStaff(int staffId);

        int MigrateDefaultSchoolClubFormTemaplates();

        List<ClubStaff> GetClubsForUser(string userName, RoleCategory? currentUserRole = null);

        Club GetNotBrandedUserClub(string userName);
        List<string> ClubOwnerEmailList(Club club);

        dynamic GetHelpInformationSetting(Club club);

        List<Charge> Calculatesurcharge(Club clubInfo);

        AgendaSettings GetAgendaSettings(Club club);

        AgendaSettings GetAgendaSettings(int clubId);
        OperationStatus SaveAgendaSettings(int clubId, AgendaSettings agendaSettings);

        ClubAttributeValue GetAttribute(Club club, AttributeName attributeName);

        List<ClubAttributeValue> GetAttributes(Club club);

        List<ClubAttributeValue> GetAttributes(int clubId);

        void SetAttributes(Club club, Dictionary<AttributeName, string> KeyValues, int? partnerId = null);
        IQueryable<ContactPerson> GetDistrictContact(int districtId);
        IQueryable<ClubSubsidy> GetAllSubsidiesDistrict(int districtId);
        OperationStatus DeleteDistrictSubSidies(List<ClubSubsidy> clubSubsidies);

        bool IsStaffIsInUseInAttendance(int staffId);
        void UpdateClubs(List<Club> clubs);
        IQueryable<ClubType> GetClubTypeListForSchools();
        IQueryable<ClubType> GetClubTypeListForProvider();

        List<ClubStaff> GetAddablePartnerStaffsForMember(int clubId);

        OperationStatus AddPartnerStaffToMember(int clubId, int staffId);

        bool AdminIsFromPartner(string userName, int clubId);
        string GetClubNotification(Club club, TypeClubNotification typeNotification);

        bool IsNotPartnerRoleInPartnerClub(int staffId);

        bool IsNotPartnerRoleInPartnerClub(string userName, int clubId);
        Club SetStaffsForNewClub(Club district, IEnumerable<ClubStaff> districtStaffs, List<int> clubStaffIds);

        OperationStatus SetStaffActive(ClubStaff clubStaff);
        OperationStatus CreateUserRole(JbUserRole userRole);
        Charge GetApplicationFee(Club club);
        decimal GetUserCredit(Club club, int userId, bool isTestMode);
        OperationStatus Save();
        List<dynamic> GetMemeberClubs(string clubDomain, string userName, RoleCategory? roleCategory = null);
        bool IsRasOrMembers(string domain);
        IQueryable<Club> GetAllSchoolsAndProviders();
        List<SelectListItem> GetKeyValueSchoolsAndProvider();
        List<SelectListItem> GetAllPartners();
        Charge GetSeasonApplicationFee(Season season);

        List<SelectKeyValue<int?>> GetPartnerMembers(int partnerId, ClubBaseInfoModel currentClub, string notSelectedOption = "All");

        List<SelectKeyValue<string>> GetStaffTitles(List<SelectKeyValue<string>> staffTitles);

        List<SelectKeyValue<int>> GetListOfClubStaffs(string clubDomain);

        bool IsUserBelongToTheClub(int clubId, int userId);

        CurrencyCodes GetClubCurrency(string clubDomain);

        List<SelectKeyValue<int>> GetClubTypes(string parentName = null);

        List<SelectKeyValue<int>> GetClubTypesForProvider();

        List<SelectKeyValue<int>> GetClubTypesForSchool();

        string GetFreeTrialMessage(DateTime createDate);

        string GetAccountOverviewMessage(PricePlan priceplan, DateTime createDate, CurrencyCodes currency);

        bool IsExpressPaymentEnabledInOfflineMode(Club club);

        bool SendMessageToProviderOnScheduleSession(int partnerId);

        bool EnableViewParentDashboard(Club club);
    }
}

