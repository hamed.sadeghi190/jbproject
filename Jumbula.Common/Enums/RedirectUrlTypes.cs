﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum RedirectUrlTypes : byte
    {
        [Description("Jumbula home page")]
        JumbulaPage = 0,
        [Description("Organization website")]
        OrganizationWebsite = 1,
        [Description("Other")]
        Other = 2

    }
}
