﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using System;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Constants = Jumbula.Common.Constants.Constants;
namespace SportsClub.Areas.Dashboard.Models    //.system
{
    public class BillingHistoryClubViewModel : BaseViewModel<ClientBillingHistory,long>
    {

        public string BillingId { get; set; }
        public string Description { get; set; }
        public string ClubName { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Balance { get; set; }
        public bool IsSelected { get; set; }
        public CurrencyCodes Currency { get; set; }
        public bool Editable { get; set; }
        public DateTime BillingDate { get; set; }
        public string Date
        {
            get
            {
                return BillingDate.ToString(Constants.DefaultDateFormat);
            }
        }
        public string StrDebit
        {
            get
            {
                return CurrencyHelper.FormatCurrencyWithPenny(Debit, CurrencyCodes.USD);
            }
        }
        public string StrCredit
        {
            get
            {
                return CurrencyHelper.FormatCurrencyWithPenny(Credit, CurrencyCodes.USD);
            }
        }

    }
}