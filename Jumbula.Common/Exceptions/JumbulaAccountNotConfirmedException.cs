﻿using System;

namespace Jumbula.Common.Exceptions
{
    public class JumbulaAccountNotConfirmedException : JumbulaAccountException
    {
        public JumbulaAccountNotConfirmedException(Exception e)
            : base(e)
        {
        }
    }
}
