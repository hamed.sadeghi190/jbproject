﻿
using Jb.Framework.Common;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Common.Constants;

namespace Jumbula.Business
{
    public class OrderSessionBusiness : IOrderSessionBusiness
    {
        private readonly IClubBusiness _clubBusiness;
        private readonly IOrderItemBusiness _orderItemBuinsess;
        private readonly ISubscriptionBusiness _subscriptionBusiness;
        private IProgramSessionBusiness _programSessionBusiness;
        private readonly IRepository<OrderSession, long> _orderSessionRepository;
        private readonly IEntitiesContext _dataContext;

        public OrderSessionBusiness(IClubBusiness clubBusiness, IOrderItemBusiness orderItemBusiness, ISubscriptionBusiness subscriptionBusiness, IRepository<OrderSession, long> orderSessionRepository, IEntitiesContext dataContext)
        {
            _clubBusiness = clubBusiness;
            _orderItemBuinsess = orderItemBusiness;
            _subscriptionBusiness = subscriptionBusiness;
            _orderSessionRepository = orderSessionRepository;
            //_programSessionBusiness = programSessionBusiness;
            _dataContext = dataContext;
        }

        public IProgramSessionBusiness ProgramSessionBusiness
        {
            get { return _programSessionBusiness; }
            set { _programSessionBusiness = value; }
        }

        public IQueryable<OrderSession> GetList()
        {
            return _orderSessionRepository.GetList();
        }

        public IQueryable<OrderSession> GetList(Expression<Func<OrderSession, bool>> predicate)
        {
            return _orderSessionRepository.GetList(predicate);
        }

        public IQueryable<OrderSession> GetList(long seasonId)
        {
            return _orderSessionRepository.GetList().Where(r => r.OrderItem.SeasonId == seasonId);
        }

        public IQueryable<OrderSession> GetOrderItemSessions(long orderItemId)
        {
            return _orderSessionRepository.GetList().Where(r => r.OrderItem.Id == orderItemId);
        }

        public OperationStatus Update(OrderSession session)
        {
            return _orderSessionRepository.Update(session);
        }

        public OperationStatus Update(List<OrderSession> sessions)
        {
            var result = new OperationStatus();

            return _orderSessionRepository.Save();
        }

        public OperationStatus Create(OrderItem orderItem)
        {
            var result = new OperationStatus();

            var orderSessions = new List<OrderSession>();

            var programSessions = _programSessionBusiness.GetList(orderItem.ProgramSchedule.Program).Where(p => !p.IsDeleted);

            orderSessions = programSessions.Select(p =>
                new OrderSession
                {
                    ProgramSessionId = p.Id,
                    OrderItemId = orderItem.Id,
                    MetaData = new MetaData()
                    {
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow,
                    }
                })
                .ToList();

            _dataContext.Set<OrderSession>().AddRange(orderSessions);

            result = _orderSessionRepository.Save();

            return result;
        }

        public OperationStatus Create(OrderItem orderItem, List<long> selectedSessionIds, bool IsCombo = false)
        {
            var result = new OperationStatus();

            var orderSessions = new List<OrderSession>();

            var programSessions = new List<ProgramSession>();

            var beforeAfterAttribuite = _subscriptionBusiness.GetBeforAfterCareAtribute(orderItem.ProgramSchedule);

            var sessionAmount = beforeAfterAttribuite.DropInSessionPrice;
            var sessionsSameAmount = beforeAfterAttribuite.DropInSameSessionPrice;
            var enableSamePrice = beforeAfterAttribuite.EnableSameDropInPrice;

            var todayDate = _clubBusiness.GetClubDateTime(orderItem.Order.Club, DateTime.UtcNow);

            #region Subscription

            programSessions = _subscriptionBusiness.GetDropInProgramSessions(orderItem.ProgramSchedule.Program, selectedSessionIds);

            foreach (var programSession in programSessions)
            {
                decimal amount = enableSamePrice && programSession.StartDateTime.Date == todayDate.Date ? sessionsSameAmount : sessionAmount;

                var orderSession = new OrderSession()
                {
                    ProgramSessionId = programSession.Id,
                    Amount = amount,
                    IsSameDayDropIn = enableSamePrice && programSession.StartDateTime.Date == todayDate.Date ? true : false,
                    MetaData = new MetaData()
                    {
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow,
                    }
                };

                orderItem.OrderSessions.Add(orderSession);
            }
            #endregion

            return _orderSessionRepository.Save();
        }

        public OperationStatus Create(OrderItem orderItem, List<DayOfWeek> days, DateTime? desiredStartDate = null, DateTime? cancelEffectiveDate = null, bool IsCombo = false)
        {
            var result = new OperationStatus();

            var orderSessions = new List<OrderSession>();

            var today = DateTime.UtcNow;
            var effctiveDate = cancelEffectiveDate.HasValue ? cancelEffectiveDate.Value : today;

            #region Subscription
            if (desiredStartDate.HasValue || cancelEffectiveDate.HasValue)
            {
                var deletedSession = orderItem.OrderSessions.Where(o => o.ProgramSession.StartDateTime >= effctiveDate);
                if (deletedSession != null)
                {
                    _dataContext.Set<OrderSession>().RemoveRange(deletedSession);
                }
            }

            desiredStartDate = desiredStartDate.HasValue ? desiredStartDate.Value : orderItem.DesiredStartDate.Value;

            if (days != null)
            {
                var programSessions = new List<ProgramSession>();

                if (IsCombo)
                {
                    var beforeAfterSchedules = orderItem.ProgramSchedule.Program.ProgramSchedules.Where(s => s.ScheduleMode != TimeOfClassFormation.Both);

                    foreach (var schedule in beforeAfterSchedules)
                    {
                        programSessions.AddRange(_programSessionBusiness.GetList(schedule));
                    }

                }
                else
                {
                    programSessions = _programSessionBusiness.GetList(orderItem.ProgramSchedule);
                }

                foreach (var programSession in programSessions.Where(p => p.StartDateTime >= desiredStartDate))
                {
                    if (days.Contains(programSession.StartDateTime.DayOfWeek))
                    {
                        var orderSession = new OrderSession()
                        {
                            ProgramSessionId = programSession.Id,
                            MetaData = new MetaData()
                            {
                                DateCreated = DateTime.UtcNow,
                                DateUpdated = DateTime.UtcNow,
                            }
                        };

                        orderItem.OrderSessions.Add(orderSession);
                    }
                }
            }
            #endregion

            return _orderSessionRepository.Save();
        }


        public OperationStatus CreateSessionByChangeDesiredStartDate(OrderItem orderItem, List<DayOfWeek> days, DateTime desiredStartDate, bool IsCombo = false)
        {
            var result = new OperationStatus();

            var orderSessions = new List<OrderSession>();

            var today = DateTime.UtcNow;
            var effctiveDate = desiredStartDate.Date;

            #region Subscription

            var sessionsToDelete = orderItem.OrderSessions.ToList();

            sessionsToDelete.ForEach(s => _dataContext.Entry(s).State = System.Data.Entity.EntityState.Deleted);

            if (days != null)
            {
                var programSessions = new List<ProgramSession>();

                if (IsCombo)
                {
                    var beforeAfterSchedules = orderItem.ProgramSchedule.Program.ProgramSchedules.Where(s => s.ScheduleMode != TimeOfClassFormation.Both);

                    foreach (var schedule in beforeAfterSchedules)
                    {
                        programSessions.AddRange(_programSessionBusiness.GetList(schedule));
                    }

                }
                else
                {
                    programSessions = _programSessionBusiness.GetList(orderItem.ProgramSchedule);
                }

                foreach (var programSession in programSessions.Where(p => p.StartDateTime >= desiredStartDate))
                {
                    if (days.Contains(programSession.StartDateTime.DayOfWeek))
                    {
                        var orderSession = new OrderSession()
                        {
                            OrderItemId = orderItem.Id,
                            ProgramSessionId = programSession.Id,
                            MetaData = new MetaData()
                            {
                                DateCreated = DateTime.UtcNow,
                                DateUpdated = DateTime.UtcNow,
                            }
                        };

                        _dataContext.Entry(orderSession).State = System.Data.Entity.EntityState.Added;// orderSessions.Add(orderSession);
                    }
                }
            }
            #endregion

            return _orderSessionRepository.Save();
        }

        public OperationStatus CreateSession(OrderItem orderItem, List<DayOfWeek> days, DateTime? effctiveDate = null, bool IsCombo = false)
        {
            var result = new OperationStatus();

            var orderSessions = new List<OrderSession>();

            var today = DateTime.UtcNow;
            var correctEffctiveDate = effctiveDate.HasValue ? effctiveDate.Value : today;

            #region Subscription
            if (orderItem.DesiredStartDate.HasValue)
            {
                var deletedSession = orderItem.OrderSessions.Where(o => o.ProgramSession.StartDateTime >= correctEffctiveDate);
                if (deletedSession != null)
                {
                    _dataContext.Set<OrderSession>().RemoveRange(deletedSession);
                }
            }

            var dateStartAddSession = correctEffctiveDate;

            if (days != null)
            {
                var programSessions = new List<ProgramSession>();

                if (IsCombo)
                {
                    var beforeAfterSchedules = orderItem.ProgramSchedule.Program.ProgramSchedules.Where(s => s.ScheduleMode != TimeOfClassFormation.Both);

                    foreach (var schedule in beforeAfterSchedules)
                    {
                        programSessions.AddRange(_programSessionBusiness.GetList(schedule));
                    }

                }
                else
                {
                    programSessions = _programSessionBusiness.GetList(orderItem.ProgramSchedule);
                }

                foreach (var programSession in programSessions.Where(p => p.StartDateTime >= dateStartAddSession))
                {
                    if (days.Contains(programSession.StartDateTime.DayOfWeek))
                    {
                        var orderSession = new OrderSession()
                        {
                            ProgramSessionId = programSession.Id,
                            MetaData = new MetaData()
                            {
                                DateCreated = DateTime.UtcNow,
                                DateUpdated = DateTime.UtcNow,
                            }
                        };

                        orderItem.OrderSessions.Add(orderSession);
                    }
                }
            }
            #endregion

            return _orderSessionRepository.Save();

        }
        public OperationStatus CreateForTransfer(OrderItem orderItem, List<DayOfWeek> days, OrderItem oldOrderItemIntransfer, DateTime? desiredStartDate = null)
        {
            var result = new OperationStatus();

            var orderSessions = new List<OrderSession>();

            var today = DateTime.UtcNow;

            var effectiveDate = desiredStartDate;

            var programschedules = orderItem.ProgramSchedule.Program.ProgramSchedules.Where(s => !s.IsDeleted);

            #region Subscription
            if (desiredStartDate.HasValue)
            {

                if (oldOrderItemIntransfer != null)
                {
                    var deletedSession = oldOrderItemIntransfer.OrderSessions.Where(o => o.ProgramSession.StartDateTime >= effectiveDate);
                    if (deletedSession != null)
                    {
                        _dataContext.Set<OrderSession>().RemoveRange(deletedSession);
                    }
                }

            }

            var programSessions = new List<ProgramSession>();

            if (orderItem.ProgramSchedule.ScheduleMode == TimeOfClassFormation.Both)
            {
                foreach (var schedule in programschedules.Where(s => s.ScheduleMode != TimeOfClassFormation.Both))
                {
                    programSessions.AddRange(_programSessionBusiness.GetList(schedule));
                }
            }
            else
            {
                programSessions = _programSessionBusiness.GetList(orderItem.ProgramSchedule);
            }

            foreach (var programSession in programSessions.Where(p => p.StartDateTime >= effectiveDate))
            {
                if (days.Contains(programSession.StartDateTime.DayOfWeek))
                {
                    var orderSession = new OrderSession()
                    {
                        ProgramSessionId = programSession.Id,
                        MetaData = new MetaData()
                        {
                            DateCreated = DateTime.UtcNow,
                            DateUpdated = DateTime.UtcNow,
                        }
                    };

                    orderItem.OrderSessions.Add(orderSession);
                }
            }
            #endregion

            return _orderSessionRepository.Save();
        }

        public void CreateSession(OrderItem orderItem, long chargeId)
        {
            var charge = orderItem.ProgramSchedule.Charges.FirstOrDefault(c => c.Id == chargeId);
            var deletedSession = orderItem.OrderSessions;
            if (deletedSession != null)
            {
                _dataContext.Set<OrderSession>().RemoveRange(deletedSession);
            }

            var programSessions = _programSessionBusiness.GetList(charge);

            foreach (var programSession in programSessions)
            {
                var orderSession = new OrderSession()
                {
                    ProgramSessionId = programSession.Id,
                    MetaData = new MetaData()
                    {
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow,
                    }
                };

                orderItem.OrderSessions.Add(orderSession);
            }
        }
        public bool HasOrder(Program program)
        {
            return program.ProgramSchedules.Any(s => !s.IsDeleted && HasOrder(s));
        }

        public bool HasOrder(ProgramSchedule programSchedule)
        {
            return programSchedule.OrderItems.Any(o => o.ItemStatus != OrderItemStatusCategories.deleted);
        }

        public bool HasActiveOrder(Program program)
        {
            return program.ProgramSchedules.Any(s => !s.IsDeleted && HasActiveOrder(s));
        }

        public bool HasActiveOrder(ProgramSchedule programSchedule)
        {
            if (programSchedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare || programSchedule.Program.TypeCategory == ProgramTypeCategory.Subscription)
            {
                return HasActiveOrderSession(programSchedule);
            }
            else
            {
                return programSchedule.OrderItems.Any(o => o.ItemStatus == OrderItemStatusCategories.completed && o.Order.IsLive);
            }

        }

        public bool HasOrderSession(Program program)
        {
            return program.ProgramSchedules.Any(s => !s.IsDeleted && HasOrderSession(s));
        }

        public bool HasOrderSession(ProgramSchedule programSchedule)
        {
            return programSchedule.OrderItems.Any(o => o.ItemStatus != OrderItemStatusCategories.deleted && o.OrderSessions.Any());
        }

        public bool HasActiveOrderSession(Program program)
        {
            return program.ProgramSchedules.Any(s => !s.IsDeleted && HasActiveOrderSession(s));
        }

        public bool HasActiveOrderSession(ProgramSchedule programSchedule)
        {
            return programSchedule.OrderItems.Any(o => o.ItemStatus == OrderItemStatusCategories.completed || (o.ItemStatus == OrderItemStatusCategories.changed && (o.ItemStatusReason == OrderItemStatusReasons.canceled || o.ItemStatusReason == OrderItemStatusReasons.transferOut) && (o.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare || o.ProgramTypeCategory == ProgramTypeCategory.Subscription) && o.OrderSessions.Any()) && o.Order.IsLive);
        }

        #region Punchcard

        public IEnumerable<ProgramSession> CheckSessionsAreInFreezedSchedule(Program program, List<long> programSessionIds)
        {
            var programScessionFreezedSchedules = new List<ProgramSession>();

            var programSessions = _programSessionBusiness.GetList(program).Where(p => programSessionIds.Contains(p.Id));

            foreach (var session in programSessions)
            {
                if (session.ProgramSchedule.IsFreezed)
                {
                    programScessionFreezedSchedules.Add(session);
                }
            }

            return programScessionFreezedSchedules;
        }
        public OperationStatus AssignPunchCardSession(long orderItemId, List<int> programSessionIds, int userId)
        {
            var orderItem = _orderItemBuinsess.GetItem(orderItemId);

            if (orderItem.Order.OrderHistories == null)
            {
                orderItem.Order.OrderHistories = new List<OrderHistory>();
            }

            var session = _programSessionBusiness.GetList(p => programSessionIds.Contains(p.Id)).FirstOrDefault();
            var historyDescription = $"{session.StartDateTime.ToString(Constants.DateTime_Comma)} is assigned to a day.";

            orderItem.Order.OrderHistories.Add(
               new OrderHistory
               {
                   Action = OrderAction.PunchcardAssigned,
                   ActionDate = DateTime.UtcNow,
                   Description = historyDescription,
                   UserId = userId,
                   OrderItemId = orderItem.Id
               });

            AddPunchCardSession(orderItemId, programSessionIds);

            return _orderSessionRepository.Save();
        }

        public void AddPunchCardSession(long orderItemId, List<int> programSessionIds)
        {
            var clubBusiness = _clubBusiness;

            var programSessions = _programSessionBusiness.GetList(p => programSessionIds.Contains(p.Id));
            var club = programSessions.First().ProgramSchedule.Program.Club;

            foreach (var programSessionId in programSessionIds)
            {
                var orderSession = new OrderSession()
                {
                    OrderItemId = orderItemId,
                    ProgramSessionId = programSessionId,
                    MetaData = new MetaData
                    {
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow,
                    },
                };

                _dataContext.Set<OrderSession>().Add(orderSession);

                var sessionEndTime = programSessions.Single(p => p.Id == programSessionId).EndDateTime;
                var now = clubBusiness.GetClubDateTime(club, DateTime.UtcNow);

                if (sessionEndTime < now)
                {
                    var studentAttendance = new StudentAttendance()
                    {
                        AttendanceStatus = AttendanceStatus.Present,
                        OrderSession = orderSession
                    };

                    _dataContext.Set<StudentAttendance>().Add(studentAttendance);
                }
            }
        }

        public OperationStatus ReassignPunchCardSession(long orderItemId, int orderSessionId, List<int> programSessionIds, int userId)
        {
            var orderItem = _orderItemBuinsess.GetItem(orderItemId);

            if (orderItem.Order.OrderHistories == null)
            {
                orderItem.Order.OrderHistories = new List<OrderHistory>();
            }

            var assignedOrderSession = orderItem.OrderSessions.Single(o => o.Id == orderSessionId);
            var sameDayAssignedOrderSession = GetSameDayOrderSession(assignedOrderSession);

            var newSession = _programSessionBusiness.GetList(p => programSessionIds.Contains(p.Id)).FirstOrDefault();
            var historyDescription = $"{assignedOrderSession.ProgramSession.StartDateTime.ToString(Constants.DateTime_Comma)} is reassigned to {newSession.StartDateTime.ToString(Constants.DateTime_Comma)}.";

            _dataContext.Set<OrderSession>().Remove(assignedOrderSession);
            if (sameDayAssignedOrderSession != null)
            {
                _dataContext.Set<OrderSession>().Remove(sameDayAssignedOrderSession);
            }

            orderItem.Order.OrderHistories.Add(
               new OrderHistory
               {
                   Action = OrderAction.PunchcardReassigned,
                   ActionDate = DateTime.UtcNow,
                   Description = historyDescription,
                   UserId = userId,
                   OrderItemId = orderItem.Id
               });

            AddPunchCardSession(orderItemId, programSessionIds);

            return _orderSessionRepository.Save();
        }

        public OperationStatus UnassignPunchCardSession(int orderSessionId, int userId)
        {
            var orderSession = GetList().Single(o => o.Id == orderSessionId);
            var sameDayOrderSession = GetSameDayOrderSession(orderSession);

            var orderItem = orderSession.OrderItem;

            if (orderItem.Order.OrderHistories == null)
            {
                orderItem.Order.OrderHistories = new List<OrderHistory>();
            }

            var deletedSession = _programSessionBusiness.GetList(p => p.Id == orderSession.ProgramSessionId).FirstOrDefault();
            var historyDescription = $"{deletedSession.StartDateTime.ToString(Constants.DateTime_Comma)} is unassigned.";

            orderItem.Order.OrderHistories.Add(
               new OrderHistory
               {
                   Action = OrderAction.PunchcardUnassigned,
                   ActionDate = DateTime.UtcNow,
                   Description = historyDescription,
                   UserId = userId,
                   OrderItemId = orderItem.Id
               });

            _dataContext.Set<OrderSession>().Remove(orderSession);

            if (sameDayOrderSession != null)
            {
                _dataContext.Set<OrderSession>().Remove(sameDayOrderSession);
            }

            return _orderSessionRepository.Save();
        }

        private OrderSession GetSameDayOrderSession(OrderSession orderSession)
        {
            OrderSession result = null;

            var sameDayProgramSession = _programSessionBusiness.GetSameDayProgramSession(orderSession.ProgramSession);

            var orderItem = orderSession.OrderItem;

            if (sameDayProgramSession != null)
            {
                result = orderItem.OrderSessions.SingleOrDefault(s => s.ProgramSessionId == sameDayProgramSession.Id);
            }

            return result;
        }
        #endregion

        public IQueryable<OrderSession> GetListByDate(long programScheduleId, DateTime dateTime)
        {
            var tommorow = dateTime.AddDays(1);

            var orderSessions = GetList().Where(o =>
                o.ProgramSession.ProgramScheduleId.Value == programScheduleId &&
                (o.ProgramSession.ProgramSchedule.Program.Season.Status == SeasonStatus.Live) == o.OrderItem.Order.IsLive &&
                !o.ProgramSession.IsDeleted &&
                o.ProgramSession.StartDateTime >= dateTime && o.ProgramSession.StartDateTime < tommorow);

            return orderSessions;
        }

        public IQueryable<OrderSession> GetListBySchedule(long programScheduleId)
        {
            var orderSessions = GetList().Where(o =>
                o.ProgramSession.ProgramScheduleId.Value == programScheduleId &&
                (o.ProgramSession.ProgramSchedule.Program.Season.Status == SeasonStatus.Live) == o.OrderItem.Order.IsLive &&
                !o.ProgramSession.IsDeleted);

            return orderSessions;
        }

        public List<OrderSession> GetReportOrderSessionsWithEffectiveDate(long programScheduleId, DateTime dateTime)
        {
            var orderSessions = GetListByDate(programScheduleId, dateTime);

            var result = orderSessions.ToList()
                .Where(o => o.OrderItem.ItemStatus == OrderItemStatusCategories.completed || (o.OrderItem.ItemStatus == OrderItemStatusCategories.changed && o.OrderItem.Attributes.CancelEffectiveDate > dateTime) || (o.OrderItem.ItemStatus == OrderItemStatusCategories.changed && o.OrderItem.Attributes.TransferEffectiveDate > dateTime))
                .ToList();

            return result;
        }
    }
}
