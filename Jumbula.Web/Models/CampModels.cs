﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Kendo.Mvc.Extensions;
using SportsClub.Domain;
using SportsClub.Infrastructure.JBMembership;
using SportsClub.Models.FormModels;
using SportsClub.Validations;
using SportsClub.Db;
using Microsoft.Practices.Unity;
using System.ComponentModel;
using System.Web.Mvc;
using System.Collections.ObjectModel;
using System.Web.Helpers;
using SportsClub.Infrastructure;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using SportsClub.Infrastructure.Helper;
using Jb.Framework.Common.Model;

namespace SportsClub.Models
{

    public class CreateEditCampViewModel
    {

        [Required]
        [DisplayName("I understand that I should double check the system generated flier and the camp registration page.")]
        public bool IUnderstandChecked { get; set; }

        //[Required]
        [DisplayName("Disable Jumbula online registration")]
        public bool OfflineRegChecked { get; set; }

        public int? CoachId { get; set; }

        public int? AssistantCoachId { get; set; }

        public EventSearchTags EventSearchTags { get; set; }

        public EventSearchField EventSearch { get; set; }

        [Required]
        [DisplayName("Camp name")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        [MinLength(3, ErrorMessage = "Enter a complete {0}")]
        public string Name { get; set; }

        [DisplayName("Camp additional name")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string Name1 { get; set; }

        [Required]
        [DisplayName("Head instructor")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        [MinLength(5, ErrorMessage = "Enter a complete {0}")]
        public string ChiefCoach { get; set; }

        [DisplayName("Assistant instructors")]
        [StringLength(256, ErrorMessage = "{0} is too long")]
        [MinLength(5, ErrorMessage = "Enter a complete {0}")]
        public string AssistantCoaches { get; set; }

        [AllowHtml]
        [DisplayName("Camp description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [AllowHtml]
        [DisplayName("Notes")]
        [DataType(DataType.MultilineText)]
        [StringLength(1024, ErrorMessage = "{0} is too long")]
        public string Notes { get; set; }


        [StringLength(128, ErrorMessage = "{0} is too long")]
        [DisplayName("External web site Url")]
        public string ExternalRegUrl { get; set; }

        [DisplayName("Early bird discount ends")]
        [DataType(DataType.Date)]
        public DateTime? EarlyBirdEndDate { get; set; }

        [DisplayName("Time zone")]
        public Domain.TimeZone TimeZone { get; set; }

        [DisplayName("Time zone")]
        public string TimeZoneName { get; set; }

        public AssignFormViewModel AssignForm { get; set; }

        [DisplayName(" Morning session")]
        public CampSectionViewModel AmSection { get; set; }

        [DisplayName("Midday session")]
        public CampSectionViewModel MdSection { get; set; }

        [DisplayName("Afternoon session")]
        public CampSectionViewModel PmSection { get; set; }

        [DisplayName("Full day session")]
        public CampSectionViewModel FullSection { get; set; }

        [DisplayName("Early bird discount")]
        public ChargeDiscountViewModel EarlyBird { get; set; }

        [DisplayName("Multiple weeks discount")]
        public ChargeDiscountViewModel MultipleWeek { get; set; }

        [DisplayName("Sibling discount")]
        public ChargeDiscountViewModel Sibling { get; set; }

        [DisplayName("Club membership discount")]
        public ChargeDiscountViewModel Membership { get; set; }
        
        [DisplayName("Lunch")]
        public ChargeDiscountViewModel Lunch { get; set; }

        [DisplayName("Morning extended care")]
        public ChargeDiscountViewModel MorningChildCare { get; set; }

        [DisplayName("Afternoon extended care")]
        public ChargeDiscountViewModel AfternoonChildCare { get; set; }

        public PolicyViewModel Refund { get; set; }

        public PolicyViewModel Waiver { get; set; }

        public PolicyViewModel MedicalRelease { get; set; }

        [DisplayName("School information")]
        private OptionalRegDataViewModel School { get; set; }

        [DisplayName("Parent/gaurdian information")]
        private OptionalRegDataViewModel ParentGuardian { get; set; }

        [DisplayName("Insurance information")]
        private OptionalRegDataViewModel Insurance { get; set; }

        [DisplayName("Skill level information")]
        private OptionalRegDataViewModel SkillLevel { get; set; }

        [DisplayName("Emergency contact information")]
        private OptionalRegDataViewModel EmergencyContact { get; set; }

        [DisplayName("Doctor contact information")]
        private OptionalRegDataViewModel Doctor { get; set; }

        [Required]
        public ICollection<CampScheduleViewModel> Schedules { get; set; }

        [DefaultValue(EventPageStep.BasicInformation)]
        public EventPageStep CurrentStep { get; set; }

        // Hidden inputs, not user provided, already set by the controller

        public string ClubDomain { get; set; }

        public string CreateEditText { get; set; } // sets the name of the submit button

        public string Domain { get; set; } // set after post back

        [DisplayName("Upload an existing flyer")]
        public FlyerType FlyerType { get; set; }

        public Club Club { get; set; }  // Needed for flyer generation, etc

        public int PostalAdderssId { get; set; }

        public PostalAddress PostalAddress { get; set; }

        public ICollection<byte[]> Images { get; set; }

        public PageMode PageMode { get; set; }


        // club admin custom fields, used in create and edit   
        // public IList<CustomFieldViewModel> TemplateFields { get; set; }

        public DonationViewModel Donation { get; set; }

        [DisplayName("Camp category")]
        public DisplayCategoryViewModel Category { get; set; }

        //[DisplayName("Camp category")]
        //public ICollection<Category> Categories { get; set; }

        public int ClubLocationId { get; set; }

        public int OutSourcerClubLocationId { get; set; }

        [DisplayName("Check if you want to save a copy of this event as template.")]
        public bool AsTemplate { get; set; }

        public bool FromTemplate { get; set; }

        public string FileName { get; set; }

        [DisplayName("Room")]
        public string Room { get; set; }

        [DisplayName("Template name")]
        public string TemplateName { get; set; }

        public List<CampOrderItem> CompleteEntries { get; set; }

        public SelectList Ages { get; set; }

        public DisplayClubLocationViewModel DisplayClubLocationViewModel { get; set; }

        [DisplayName("Outsourcer")]
        public string OutSourcerClubDomain { get; set; }

        public IEnumerable<OutSourcerList> OutSourcers { get; set; }

        public SelectList OutSourcerSelectList { get; set; }

        public int ClubId { get; set; }

        public bool IsVendor { get; set; }

        [DisplayName("Is this an outsourcer event for a school or club?")]
        public EventSponserType IsVendorEvent { get; set; }

        [DisplayName("Capture restriction")]
        public bool CaptureRestriction { get; set; }

        public RegisterDeadLine RegisterDeadLine { get; set; }

        [UIHint("PaymentPlanViewModel")]
        public PaymentPlanViewModel PaymentPlan { get; set; }

        // base constructor
        public CreateEditCampViewModel()
        {
            AssignForm = new AssignFormViewModel();
            Donation = new DonationViewModel();
            // initilize collections
            EventSearch = new EventSearchField();
            EventSearchTags = new EventSearchTags();
            //  TemplateFields = new List<CustomFieldViewModel>();
            Schedules = new HashSet<CampScheduleViewModel>();
            Images = new HashSet<byte[]>();
            OutSourcers = new List<OutSourcerList>();
            SeedAges();
            OutSourcers = new List<OutSourcerList>();
            Category = new DisplayCategoryViewModel();
            RegisterDeadLine = new RegisterDeadLine();
            PaymentPlan = new PaymentPlanViewModel();

        }

        // create constructor
        public CreateEditCampViewModel(string clubDomain, int clubCategoryId, int clubId, bool isVendor) :
            this()
        {
            ClubDomain = clubDomain;
            //Category = new DisplayCategoryViewModel();
            EventSearch = new EventSearchField { };
            DisplayClubLocationViewModel = new DisplayClubLocationViewModel(clubDomain);
            Seed();
            PageMode = PageMode.Create;
            CreateEditText = Constants.W_Camp_CreateButton; // name of submit button
            CurrentStep = EventPageStep.BasicInformation;
            ClubId = clubId;
            IsVendor = isVendor;
            SeedOUtSourcers();
            SeedCategories(clubCategoryId: clubCategoryId);
            PaymentPlan = new PaymentPlanViewModel(clubDomain);

            // initial AssignForm
            var forms = Ioc.IFormsDbService.GetForms(clubDomain, EventStatusCategories.open);
            AssignForm = new AssignFormViewModel() { AllForms = new SelectList(forms, "Id", "Name") };

            AssignForm.Forms = forms.Select(x => new FormListViewModel
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();


        }
        // edit constructor
        public CreateEditCampViewModel(Camp camp, bool fromTemplate = false, int clubId = 0, bool isVendor = false)
            : this()
        {
            // set event for a special outsourcer
            ClubId = clubId;
            IsVendor = isVendor;
            IsVendorEvent = string.IsNullOrEmpty(camp.OutSourcerClubDomain) ? EventSponserType.Vendor : EventSponserType.OutSourcer;
            OutSourcerClubDomain = camp.OutSourcerClubDomain;
            RegisterDeadLine = camp.RegisterDeadLine;

            // initial AssignForm
            var forms = Ioc.IFormsDbService.GetForms(camp.ClubDomain, EventStatusCategories.open);
            AssignForm = new AssignFormViewModel() { AllForms = new SelectList(forms, "Id", "Name") };
            foreach (var item in camp.EventForms)
            {
                AssignForm.SelectedForms.Add(item.FormId);
            }

            AssignForm.Forms = forms.Select(x => new FormListViewModel
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            // Custom Field 
            //if (!string.IsNullOrEmpty(camp.CustomTemplateMetaData))
            //{
            //    var customTemplateMetadata = SerializeHelper.Deserialize<CustomModelMetaData>(camp.CustomTemplateMetaData);

            //    //  customTemplateMetadata
            //    var templateFields = customTemplateMetadata.Fields.Select(item => new CustomFieldViewModel()
            //    {
            //        Title = item.Title,
            //        FieldType = item.Type,
            //        ValidationType = item.Validations.First().Type
            //    }).ToList();

            //    TemplateFields = templateFields;
            //}

            CoachId = camp.CoachId;
            AssistantCoachId = camp.AssistantCoachId;

            Domain = camp.Domain;
            ClubDomain = camp.ClubDomain;
            PostalAddress = camp.ClubLocation.PostalAddress;

            EventSearch.MinAge = camp.EventSearchField.MinAge;
            EventSearch.MaxAge = camp.EventSearchField.MaxAge;
            EventSearch.MinGrade = (camp.EventSearchField.MinGrade == SchoolGradeType.College || camp.EventSearchField.MinGrade == SchoolGradeType.NotInSchool) ? SchoolGradeType.None : camp.EventSearchField.MinGrade; ;
            EventSearch.MaxGrade = (camp.EventSearchField.MaxGrade == SchoolGradeType.College || camp.EventSearchField.MaxGrade == SchoolGradeType.NotInSchool) ? SchoolGradeType.None : camp.EventSearchField.MaxGrade;
            EventSearch.Level = camp.EventSearchField.Level;
            EventSearch.Gender = camp.EventSearchField.Gender;
            Name = camp.Name;
            Name1 = camp.Name1;
            ChiefCoach = camp.ChiefCoach;
            AssistantCoaches = camp.AssistantCoaches;
            Description = camp.Description;
            Notes = camp.Notes;
            EarlyBirdEndDate = camp.EarlyBirdEndDate;
            OfflineRegChecked = camp.OfflineRegChecked;
            ExternalRegUrl = camp.ExternalRegUrl;
            DisplayClubLocationViewModel = new DisplayClubLocationViewModel(camp.ClubDomain)
                {
                    SelectedClubLocationId = camp.ClubLocation.Id
                };

            FileName = camp.FileName;
            FlyerType = camp.FlyerType;
            TemplateName = camp.TemplateName;
            AsTemplate = !fromTemplate && camp.AsTemplate;
            FromTemplate = fromTemplate;
            Category.CategoryId = camp.CategoryId;
            Donation.Checked = camp.Donation.Checked;
            Donation.Title = camp.Donation.Title;
            Donation.Description = camp.Donation.Description;
            Room = camp.Room;
            EventSearchTags = camp.EventSearchTags;
            CaptureRestriction = camp.CaptureRestriction;
            PaymentPlan = new PaymentPlanViewModel(camp.PaymentPlans.FirstOrDefault(), camp.ClubDomain);

            // Programs
            // Call the seed method first to set up default values
            // The foreach statement overrides the default values if they exist in db
            SeedSections();

            foreach (CampSection item in camp.Sections)
            {
                switch (item.Category)
                {
                    case CampSectionCategories.Am:
                        AmSection.Set(item);
                        break;

                    case CampSectionCategories.Md:
                        MdSection.Set(item);
                        break;

                    case CampSectionCategories.Pm:
                        PmSection.Set(item);
                        break;

                    case CampSectionCategories.Full:
                        FullSection.Set(item);
                        break;

                    default:
                        throw new NotSupportedException(string.Format(Constants.F_CampSectionCategoryNotSupported, item.Category));
                }
            }

            // Schedules
            // No seeding required
            var campSchedule = camp.Schedules.OrderBy(c => c.Start).ToList();
            foreach (CampSchedule item in campSchedule)
            {
                Schedules.Add(new CampScheduleViewModel(item));
            }

            // Charge and discounts
            // Call the seed method first to set up default values
            // The foreach statement overrides the default values if they exist in db
            // NOTE: This strategy works only if the seed method sets all the options to false. 
            //       If we need to have some options enabled in seed, then another method must reset all the options to false

            SeedChargeDiscounts();

            foreach (ChargeDiscount item in camp.ChargeDiscounts)
            {
                switch (item.Category)
                {
                    case ChargeDiscountCategories.EarlyBird:
                        EarlyBird = new ChargeDiscountViewModel(item);
                        break;

                    case ChargeDiscountCategories.MultipleWeek:
                        {
                            MultipleWeek = new ChargeDiscountViewModel(item);
                            // deserialize and initial additional parameter of this discount
                            var multipleWeekMetaData = camp.ChargeDiscounts.SingleOrDefault(x => x.Category == ChargeDiscountCategories.MultipleWeek).Detail;
                            if (multipleWeekMetaData != null)
                            {
                                MultipleWeek.MultipleWeekDiscountMetaData = new MultipleWeekDiscountMetaData();
                                var multipleWeekDiscount = SerializeHelper.Deserialize<MultipleWeekDiscountMetaData>(multipleWeekMetaData);
                                if (multipleWeekDiscount != null)
                                {
                                    MultipleWeek.MultipleWeekDiscountMetaData.RequiredRegister = multipleWeekDiscount.RequiredRegister;
                                    MultipleWeek.MultipleWeekDiscountMetaData.ApplyBefor = multipleWeekDiscount.ApplyBefor;
                                    MultipleWeek.MultipleWeekDiscountMetaData.IsRepeated = multipleWeekDiscount.IsRepeated;
                                    MultipleWeek.MultipleWeekDiscountMetaData.MaximumDiscount = multipleWeekDiscount.MaximumDiscount;
                                }

                            }
                            break;
                        }
                    case ChargeDiscountCategories.Sibling:
                        Sibling = new ChargeDiscountViewModel(item);
                        break;

                    case ChargeDiscountCategories.ClubMembership:
                        Membership = new ChargeDiscountViewModel(item);
                        break;

                    case ChargeDiscountCategories.Lunch:
                        Lunch = new ChargeDiscountViewModel(item);
                        break;

                    case ChargeDiscountCategories.MorningChildCare:
                        {
                            MorningChildCare = new ChargeDiscountViewModel(item);
                            var extendedCareChargeMetaData = camp.ChargeDiscounts.SingleOrDefault(x => x.Category == ChargeDiscountCategories.MorningChildCare).Detail;
                            if (extendedCareChargeMetaData != null)
                            {
                                MorningChildCare.ExtendedCareChargeMetaData = new ExtendedCareChargeMetaData();

                                var morningChildCareMetaData = SerializeHelper.Deserialize<ExtendedCareChargeMetaData>(extendedCareChargeMetaData);
                                if (morningChildCareMetaData != null)
                                {
                                    MorningChildCare.ExtendedCareChargeMetaData.StartTime = morningChildCareMetaData.StartTime;
                                    MorningChildCare.ExtendedCareChargeMetaData.EndTime = morningChildCareMetaData.EndTime;
                                }

                            }
                            break;
                        }
                    case ChargeDiscountCategories.AfternoonChildCare:
                        {
                            AfternoonChildCare = new ChargeDiscountViewModel(item);

                            var extendedCareChargeMetaData = camp.ChargeDiscounts.SingleOrDefault(x => x.Category == ChargeDiscountCategories.AfternoonChildCare).Detail;
                            if (extendedCareChargeMetaData != null)
                            {
                                AfternoonChildCare.ExtendedCareChargeMetaData = new ExtendedCareChargeMetaData();

                                var afternoonChildCareMetaData = SerializeHelper.Deserialize<ExtendedCareChargeMetaData>(extendedCareChargeMetaData);

                                if (afternoonChildCareMetaData != null)
                                {
                                    AfternoonChildCare.ExtendedCareChargeMetaData.StartTime = afternoonChildCareMetaData.StartTime;
                                    AfternoonChildCare.ExtendedCareChargeMetaData.EndTime = afternoonChildCareMetaData.EndTime;
                                }

                            }
                            break;
                        }

                    default:
                        throw new NotSupportedException(string.Format(Constants.F_ChargeDiscountCategoryNotSupported, item.Category));
                }
            }

            // Policies
            // Call the seed method first to set up default values
            // The foreach statement overrides the default values if they exist in db

            SeedPolicies();

            foreach (Policy item in camp.Policys)
            {
                switch (item.Category)
                {
                    case PolicyCategories.Refund:
                        Refund.Set(item);
                        break;

                    case PolicyCategories.Waiver:
                        Waiver.Set(item);
                        break;

                    case PolicyCategories.MedicalRelease:
                        MedicalRelease.Set(item);
                        break;

                    default:
                        throw new NotSupportedException(string.Format(Constants.F_PolicyCategoryNotSupported, item.Category));
                }
            }

            // Additiona reg data

            SeedRegDataOptions();

            foreach (OptionalRegData item in camp.OptionalRegData)
            {
                switch (item.Category)
                {
                    case OptionalRegDataCategories.EmergencyContact:
                        EmergencyContact.IsChecked = true;
                        break;

                    case OptionalRegDataCategories.Insurance:
                        Insurance.IsChecked = true;
                        break;

                    case OptionalRegDataCategories.ParentGuardian:
                        ParentGuardian.IsChecked = true;
                        break;

                    case OptionalRegDataCategories.School:
                        School.IsChecked = true;
                        break;

                    case OptionalRegDataCategories.SkillLevel:
                        SkillLevel.IsChecked = true;
                        break;

                    case OptionalRegDataCategories.Doctor:
                        Doctor.IsChecked = true;
                        break;

                    default:
                        throw new NotSupportedException(string.Format(Constants.F_OptionalRegDataCategoryNotSupported, item.Category));
                }
            }

            SeedCategories(camp.CategoryId);
            SeedClubLocation();
            SeedEntries();
            SeedRegisterdSections();
            SeedRegisterdSchedule();
            SeedAges();
            SeedOUtSourcers();
        }

        public void Seed()
        {
            SeedSections();
            SeedSchedules();
            SeedChargeDiscounts();
            SeedPolicies();
            SeedNote();
            SeedClubLocation();
            SeedAges();

        }

        public Camp ToCamp(bool setDomain = false)
        {

            var camp = new Camp();

            // add all selected form to eventforms table
            var eventForms = AssignForm.SelectedForms.Select(item => new EventForms() { FormId = item }).ToList();
            camp.EventForms.AddRange(eventForms);

            var clubLoc = Ioc.IClubLocationDb.GetClubLocation(DisplayClubLocationViewModel.SelectedClubLocationId.HasValue ? DisplayClubLocationViewModel.SelectedClubLocationId.Value : ClubLocationId);
            camp.ClubLocation = clubLoc;
            camp.Room = Room;

            // if user select couch from autocomplete put id of coach else put null
            camp.CoachId = CoachId;
            camp.AssistantCoachId = AssistantCoachId;

            camp.ClubDomain = ClubDomain;
            camp.EventSearchField = new EventSearchField
                {
                    MinAge = EventSearch.MinAge,
                    MaxAge = EventSearch.MaxAge,
                    MinGrade = (EventSearch.MinGrade.HasValue && EventSearch.MinGrade.Value != SchoolGradeType.None) ? EventSearch.MinGrade : null,
                    MaxGrade = EventSearch.MaxGrade.HasValue && EventSearch.MaxGrade.Value != SchoolGradeType.None ? EventSearch.MaxGrade : null,
                    Level = EventSearch.Level.HasValue && EventSearch.Level.Value != Level.None ? EventSearch.Level : null,
                    Gender = EventSearch.Gender.HasValue ? EventSearch.Gender : null,

                };
            camp.Name = Name;
            camp.Name1 = Name1;
            camp.ChiefCoach = ChiefCoach;
            camp.AssistantCoaches = AssistantCoaches;
            camp.Description = Utilities.GetDescription(Description);
            camp.Notes = Utilities.GetDescription(Notes);
            camp.EarlyBirdEndDate = EarlyBirdEndDate.HasValue ? EarlyBirdEndDate.Value : DateTime.Today;
            camp.OfflineRegChecked = OfflineRegChecked;
            camp.ExternalRegUrl = ExternalRegUrl;
            camp.CategoryId = Category.CategoryId;
            camp.ClubDomain = ClubDomain;
            camp.Domain = setDomain ? Utilities.GenerateSubDomainName(ClubDomain, Name) : Domain;
            camp.ItemId = Utilities.GenerateItemId();
            camp.EventStatus = EventStatusCategories.open;
            camp.OfflineRegChecked = OfflineRegChecked;
            camp.ExternalRegUrl = ExternalRegUrl;
            camp.MetaData = new MetaData { DateCreated = DateTime.Now, DateUpdated = DateTime.Now };
            camp.TemplateName = TemplateName;
            camp.OutSourcerClubDomain = IsVendorEvent == EventSponserType.OutSourcer ? OutSourcerClubDomain : null;

            camp.EventSearchTags = EventSearchTags;
            camp.Geography = Utilities.NewDbGeography(PostalAddress.Lat, PostalAddress.Lng);
            camp.FlyerType = FlyerType;
            camp.AsTemplate = AsTemplate;
            camp.CaptureRestriction = CaptureRestriction;
            camp.RegisterDeadLine = RegisterDeadLine;

            if (PaymentPlan.IsChecked)
            {
                camp.PaymentPlans.Add(PaymentPlan.ToPaymentPlan());
            }
            // Programs

            AddSection(
               CampSectionCategories.Am,
               AmSection,
               new ChargeDiscountViewModel() { Amount = AmSection.EarlyBirdDiscount, Category = ChargeDiscountCategories.EarlyBird, IsChecked = EarlyBird.IsChecked },
                // new ChargeDiscountViewModel() { Amount = AmSection.MultipleWeekDiscount, Category = ChargeDiscountCategories.MultipleWeek, IsChecked = MultipleWeek.IsChecked },
               camp);

            AddSection(
                   CampSectionCategories.Md,
                   MdSection,
                   new ChargeDiscountViewModel() { Amount = MdSection.EarlyBirdDiscount, Category = ChargeDiscountCategories.EarlyBird, IsChecked = EarlyBird.IsChecked },
                // new ChargeDiscountViewModel() { Amount = MdSection.MultipleWeekDiscount, Category = ChargeDiscountCategories.MultipleWeek, IsChecked = MultipleWeek.IsChecked },
                   camp);

            AddSection(
                     CampSectionCategories.Pm,
                     PmSection,
                     new ChargeDiscountViewModel() { Amount = PmSection.EarlyBirdDiscount, Category = ChargeDiscountCategories.EarlyBird, IsChecked = EarlyBird.IsChecked },
                //    new ChargeDiscountViewModel() { Amount = PmSection.MultipleWeekDiscount, Category = ChargeDiscountCategories.MultipleWeek, IsChecked = MultipleWeek.IsChecked },
                     camp);

            AddSection(
                CampSectionCategories.Full,
                FullSection,
                new ChargeDiscountViewModel() { Amount = FullSection.EarlyBirdDiscount, Category = ChargeDiscountCategories.EarlyBird, IsChecked = EarlyBird.IsChecked },
                // new ChargeDiscountViewModel() { Amount = FullSection.MultipleWeekDiscount, Category = ChargeDiscountCategories.MultipleWeek, IsChecked = MultipleWeek.IsChecked },
                camp);

            // Schedules

            for (int i = 0; i < Schedules.Count; ++i)
            {
                var vm = Schedules.ElementAt(i);
                CampSchedule s = new CampSchedule
                    {
                        Id = vm.Id,
                        Start = vm.Start.Value,
                        End = vm.End.Value,
                        AnySpecialPrice = vm.AnySpecialPrice,
                        Caption = vm.Caption,
                    };

                // just if any Special price is true save Price
                if (vm.AnySpecialPrice)
                {
                    s.AmPrice = vm.AmPrice;
                    s.MdPrice = vm.MdPrice;
                    s.PmPrice = vm.PmPrice;
                    s.FullPrice = vm.FullPrice;
                }
                camp.Schedules.Add(s);
            }


            // Charge & Discounts
            // Note: EarlyBird and MultipleWeek do not have amount values b/c the amounts are tied to camp sections
            // Set the amount here
            // Note: _CreateEdit does not post back the amount, -1 does not create any problems with range

            EarlyBird.Amount = -1;

            MultipleWeek.Amount = MultipleWeek.Amount;

            // do seed here, because reg data options remove from view
            SeedRegDataOptions();

            AddChargeDiscount(ChargeDiscountCategories.EarlyBird, EarlyBird, camp);
            AddChargeDiscount(ChargeDiscountCategories.MultipleWeek, MultipleWeek, camp);
            AddChargeDiscount(ChargeDiscountCategories.Sibling, Sibling, camp);
            AddChargeDiscount(ChargeDiscountCategories.ClubMembership, Membership, camp);
            AddChargeDiscount(ChargeDiscountCategories.Lunch, Lunch, camp);
            AddChargeDiscount(ChargeDiscountCategories.MorningChildCare, MorningChildCare, camp);
            AddChargeDiscount(ChargeDiscountCategories.AfternoonChildCare, AfternoonChildCare, camp);

            // Policies

            AddPolicy(PolicyCategories.Refund, Refund, camp);
            AddPolicy(PolicyCategories.Waiver, Waiver, camp);
            AddPolicy(PolicyCategories.MedicalRelease, MedicalRelease, camp);

            // Additional data

            AddOptionalRegData(OptionalRegDataCategories.SkillLevel, SkillLevel, camp);
            AddOptionalRegData(OptionalRegDataCategories.School, School, camp);
            AddOptionalRegData(OptionalRegDataCategories.ParentGuardian, ParentGuardian, camp);
            AddOptionalRegData(OptionalRegDataCategories.EmergencyContact, EmergencyContact, camp);
            AddOptionalRegData(OptionalRegDataCategories.Insurance, Insurance, camp);
            AddOptionalRegData(OptionalRegDataCategories.Doctor, Doctor, camp);

            //Donation
            camp.Donation.Checked = Donation.Checked;
            if (!Donation.Checked)
            {
                // if donation is unchecked set description and title null
                camp.Donation.Description = null;
                camp.Donation.Title = null;
            }
            else
            {
                // if donation is checked set description and title to model value
                camp.Donation.Description = Donation.Description;
                camp.Donation.Title = Donation.Title;
            }


            // add Customtemplate
            //var customodelMetaData = new CustomModelMetaData()
            //{
            //    Sections = new List<ModelSection>() { new ModelSection() { SectionId = 1, SectionName = "Additional Informaion" } }
            //};
            ////AA To Do Add form to course 
            //var customFileds = new List<Question>();

            ////foreach (var item in TemplateFields)
            ////{
            ////    customFileds.Add(new Question()
            ////    {
            ////        SectionId = 1,
            ////        Type = item.FieldType,
            ////        Title = item.Title,
            ////        Validations = new List<QuestionValidator>() { new QuestionValidator() { Type = item.ValidationType } }
            ////    });
            ////}
            //customodelMetaData.Fields = customFileds;
            //var customTempalte = SerializeHelper.Serialize(customodelMetaData).ToString();
            //camp.CustomTemplateMetaData = customTempalte;

            camp.TimeZone = TimeZoneHelper.GetTimeZone(clubLoc.PostalAddress.Lat, clubLoc.PostalAddress.Lng);

            // Assign hidden fields
            // Note: No need for ClubLogo

            // System fields
            // Note that Domain is null within create tourney, but is valid during edit

            return camp;
        }


        private void SeedSchedules()
        {
            CampScheduleViewModel sch1 = new CampScheduleViewModel();
            Schedules.Add(sch1);

        }

        private void SeedSections()
        {
            AmSection = new CampSectionViewModel() { Category = CampSectionCategories.Am, Name = string.Empty };
            MdSection = new CampSectionViewModel() { Category = CampSectionCategories.Md, Name = string.Empty };
            PmSection = new CampSectionViewModel() { Category = CampSectionCategories.Pm, Name = string.Empty };
            FullSection = new CampSectionViewModel() { Category = CampSectionCategories.Full, Name = string.Empty };
        }

        private void SeedChargeDiscounts()
        {
            EarlyBird = new ChargeDiscountViewModel(false, ChargeDiscountCategories.EarlyBird, null, Constants.M_Camp_CD_EarlyBird, ChargeDiscountType.Fixed);
            MultipleWeek = new ChargeDiscountViewModel(false, ChargeDiscountCategories.MultipleWeek, null, Constants.M_Camp_CD_MultipleWeek, ChargeDiscountType.Fixed);
            Sibling = new ChargeDiscountViewModel(false, ChargeDiscountCategories.Sibling, null, Constants.M_Camp_CD_Sibling, ChargeDiscountType.Fixed);
            Membership = new ChargeDiscountViewModel(false, ChargeDiscountCategories.ClubMembership, null, Constants.M_Camp_CD_ClubMembership, ChargeDiscountType.Fixed);
            Lunch = new ChargeDiscountViewModel(false, ChargeDiscountCategories.Lunch, null, string.Empty, ChargeDiscountType.Fixed);
            MorningChildCare = new ChargeDiscountViewModel(false, ChargeDiscountCategories.MorningChildCare, null, string.Empty, ChargeDiscountType.Fixed);
            AfternoonChildCare = new ChargeDiscountViewModel(false, ChargeDiscountCategories.AfternoonChildCare, null, string.Empty, ChargeDiscountType.Fixed);
        }

        private void SeedPolicies()
        {
            IStandardPolicyDb policyDb = Ioc.IStandardPolicyDbService;
            StandardPolicy[] policies = policyDb.Get();

            // Note: PolicyCategories start @ index 1, must reduce by one

            Refund = new PolicyViewModel() { Category = PolicyCategories.Refund, Name = policies[(int)PolicyCategories.Refund - 1].Name, Desc = policies[(int)PolicyCategories.Refund - 1].Description, IsChecked = false, Field1 = 14, Field2 = 0 };
            Waiver = new PolicyViewModel() { Category = PolicyCategories.Waiver, Name = policies[(int)PolicyCategories.Waiver - 1].Name, Desc = policies[(int)PolicyCategories.Waiver - 1].Description, IsChecked = false };
            MedicalRelease = new PolicyViewModel() { Category = PolicyCategories.MedicalRelease, Name = policies[(int)PolicyCategories.MedicalRelease - 1].Name, Desc = policies[(int)PolicyCategories.MedicalRelease - 1].Description, IsChecked = false };
        }

        private void SeedRegDataOptions()
        {
            School = new OptionalRegDataViewModel { Category = OptionalRegDataCategories.School, IsChecked = true };
            EmergencyContact = new OptionalRegDataViewModel { Category = OptionalRegDataCategories.EmergencyContact, IsChecked = true };
            Insurance = new OptionalRegDataViewModel { Category = OptionalRegDataCategories.Insurance, IsChecked = true };
            ParentGuardian = new OptionalRegDataViewModel { Category = OptionalRegDataCategories.ParentGuardian, IsChecked = true };
            SkillLevel = new OptionalRegDataViewModel { Category = OptionalRegDataCategories.SkillLevel, IsChecked = true };
            Doctor = new OptionalRegDataViewModel { Category = OptionalRegDataCategories.Doctor, IsChecked = true };
        }

        private void SeedNote()
        {
            Notes = string.Empty;
        }

        public void SeedClubLocation()
        {

            var clubLocations = Ioc.IClubLocationDb.GetClubLocations(ClubDomain);
            DisplayClubLocationViewModel.ClubLocations = clubLocations;
        }

        private void SeedCategories(int categoryId = 0, int clubCategoryId = 0)
        {
            Category = new DisplayCategoryViewModel();
            // Load categories from db with a condition that check template existence in course category
            Category = new DisplayCategoryViewModel()
            {
                Categories = Ioc.ICategoryDbService.GetCategories(SubDomainCategories.Camp),
                CategoryId = categoryId,
                ClubCategoryId = clubCategoryId
            };
        }


        private void SeedEntries()
        {
            var entries = Ioc.IOrderDbService.GetEntries(Domain);
            CompleteEntries = entries.Where(c => c.PaymentStatus == PaymentStatusCategories.completed || c.PaymentStatus == PaymentStatusCategories.PartiallyCompleted || c.PaymentStatus == PaymentStatusCategories.pending).ToList();

        }

        private void SeedRegisterdSections()
        {
            // am section
            if (CompleteEntries.Any(c => c.CampSectionId == AmSection.Id) && !FromTemplate)
            {
                AmSection.HasCompleteEntry = true;
            }

            // mdSection
            if (CompleteEntries.Any(c => c.CampSectionId == MdSection.Id) && !FromTemplate)
            {
                MdSection.HasCompleteEntry = true;
            }

            //PmSection
            if (CompleteEntries.Any(c => c.CampSectionId == PmSection.Id) && !FromTemplate)
            {
                PmSection.HasCompleteEntry = true;
            }

            //FullSection
            if (CompleteEntries.Any(c => c.CampSectionId == FullSection.Id) && !FromTemplate)
            {
                FullSection.HasCompleteEntry = true;
            }

        }

        private void SeedRegisterdSchedule()
        {
            // if we are in template mode admin can remove registerd schedule beacuse this schedule are new 
            if (FromTemplate)
            {
                return;
            }

            foreach (var item in Schedules)
            {
                if (CompleteEntries.Any(c => c.CampScheduleId == item.Id))
                {
                    item.HasCompeteEntry = true;
                }
            }

        }


        private void SeedAges()
        {
            var ageList = new List<SelectListItem>();
            for (int i = 1; i <= 120; i++)
            {
                ageList.Add(new SelectListItem
                    {
                        Text = i.ToString(),
                        Value = i.ToString()
                    });
            }

            Ages = new SelectList(ageList, "Value", "Text");
        }

        private void SeedOUtSourcers()
        {
            var outSourcers = Ioc.IClubVendorDb.GetSchoolsForClub(ClubId);
            OutSourcers = outSourcers.Select(m => new OutSourcerList
                {
                    OutSourcerClubDomain = m.OutSourcerClubDomain,
                    OutSourcerClubName = m.OutSourcerLocation.PostalAddress.Address + "(" + m.OutSourcerClubName + ")",
                    OutSourcerLocation = m.OutSourcerLocation,

                }).ToList();
            OutSourcerSelectList = new SelectList(OutSourcers, "OutSourcerClubDomain", "OutSourcerClubName");

        }

        private void AddSection(CampSectionCategories catg, CampSectionViewModel campVm, ChargeDiscountViewModel earlyBirdVm, Camp camp)
        {
            if (campVm.IsChecked)
            {
                CampSection s = new CampSection { Category = catg, Name = campVm.Name, Cost = campVm.Cost.Value, Capacity = campVm.Capacity, Description = campVm.Description, Id = campVm.Id, From = campVm.From.Value, To = campVm.To.Value, };

                if (earlyBirdVm.IsChecked)
                {
                    s.EarlyBirdDiscount = campVm.EarlyBirdDiscount.Value;
                }
                else
                {
                    s.EarlyBirdDiscount = -1;
                }

                //if (multipleWeekVm.IsChecked)
                //{
                //    s.MultipleWeekDiscount = campVm.MultipleWeekDiscount.Value;
                //}
                //else
                //{
                //    s.MultipleWeekDiscount = -1;
                //}

                camp.Sections.Add(s);
            }
        }



        private void AddChargeDiscount(ChargeDiscountCategories catg, ChargeDiscountViewModel cdVm, Camp camp)
        {
            string detail = null;
            switch (catg)
            {
                case ChargeDiscountCategories.MorningChildCare:
                case ChargeDiscountCategories.AfternoonChildCare:
                    {
                        if (cdVm.IsChecked)
                        {
                            detail = SerializeHelper.Serialize(cdVm.ExtendedCareChargeMetaData).ToString();
                        }
                        break;
                    }
                case ChargeDiscountCategories.MultipleWeek:
                    {
                        if (cdVm.IsChecked)
                        {
                            detail = SerializeHelper.Serialize(cdVm.MultipleWeekDiscountMetaData).ToString();
                        }
                        break;
                    }

            }
            if (cdVm.IsChecked == true)
            {

                ChargeDiscount cd = new ChargeDiscount
                {
                    Category = catg,
                    Amount = cdVm.Amount.Value,
                    Desc = cdVm.Desc,
                    ChargeDiscountType = cdVm.ChargeDiscountType,
                    Detail = detail
                };

                camp.ChargeDiscounts.Add(cd);
            }
        }

        private void AddPolicy(PolicyCategories catg, PolicyViewModel policyVm, Camp camp)
        {
            if (policyVm.IsChecked == true)
            {
                int f1 = -1;
                int f2 = -1;

                if (policyVm.Field1.HasValue)
                {
                    f1 = policyVm.Field1.Value;
                }

                if (policyVm.Field2.HasValue)
                {
                    f2 = policyVm.Field2.Value;
                }

                Policy p = new Policy { Category = catg, Field1 = f1, Field2 = f2, Description = policyVm.Desc };
                camp.Policys.Add(p);
            }
        }

        private void AddOptionalRegData(OptionalRegDataCategories category, OptionalRegDataViewModel vm, Camp camp)
        {
            if (vm.IsChecked)
            {
                camp.OptionalRegData.Add(new OptionalRegData { Category = category });
            }
        }

    }

    public static class JsonExtensions
    {
        public static string ToJson(this Object obj)
        {
            return new JavaScriptSerializer().Serialize(obj);
        }
    }

    public class CampSectionViewModel
    {
        public int Id { get; set; }

        [Required]
        public bool IsChecked { get; set; }

        [DisplayName("Price")]
        [Range(1, Constants.Camp_EntryFee_Max, ErrorMessage = "{0} must be a number between {1} and {2}")]
        public int? Cost { get; set; }

        [DisplayName("Capacity")]
        public int? Capacity { get; set; }

        public int? EarlyBirdDiscount { get; set; }

        [DisplayName("EarlyBird")]
        public bool IsEarlyBirdDiscountChecked { get; set; }

        public decimal? MultipleWeekDiscount { get; set; }

        [DisplayName("MultipleWeek")]
        public bool IsMultipleWeekDiscountChecked { get; set; }

        public CampSectionCategories Category { get; set; }

        [DisplayName("Session name")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string Name { get; set; }

        [AllowHtml]
        [DisplayName("Description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public bool HasCompleteEntry { get; set; }

        public SelectList CapacityList { get; set; }

        [DisplayName("Start time")]
        public TimeSpan? From { get; set; }

        [DisplayName("End time")]
        public TimeSpan? To { get; set; }


        public CampSectionViewModel()
        {
            SeedCapacity();
        }

        public CampSectionViewModel(CampSection s)
        {
            IsChecked = true;
            Cost = (int)s.Cost;
            Capacity = s.Capacity;
            Category = s.Category;
            Name = s.Name;
            Description = s.Description;
            Id = s.Id;
            From = s.From;
            To = s.To;
            if (s.EarlyBirdDiscount != -1)
            {
                EarlyBirdDiscount = (int)s.EarlyBirdDiscount;
            }

            //if (s.MultipleWeekDiscount != -1)
            //{
            //    MultipleWeekDiscount = (int)s.MultipleWeekDiscount;
            //}

            SeedCapacity();
        }


        public CampSectionViewModel(CampSectionViewModel s, bool isChecked, bool expireEarlyBird)
        {
            IsChecked = isChecked;
            Cost = (int)s.Cost;
            Capacity = s.Capacity;
            Category = s.Category;
            Name = s.Name;
            Description = s.Description;
            Id = s.Id;
            From = s.From;
            To = s.To;

            if (s.EarlyBirdDiscount != null && s.EarlyBirdDiscount != -1 && !expireEarlyBird)
            {
                EarlyBirdDiscount = (int)s.EarlyBirdDiscount;
            }

            //if (s.MultipleWeekDiscount != null && s.MultipleWeekDiscount != -1)
            //{
            //    MultipleWeekDiscount = (int)s.MultipleWeekDiscount;
            //}

            SeedCapacity();
        }

        public void Set(CampSection s)
        {
            IsChecked = true;
            Cost = (int)s.Cost;
            Capacity = s.Capacity;
            Name = s.Name;
            Description = s.Description;
            Id = s.Id;
            From = s.From;
            To = s.To;

            if (s.EarlyBirdDiscount != -1)
            {
                EarlyBirdDiscount = (int)s.EarlyBirdDiscount;
            }

            //if (s.MultipleWeekDiscount != -1)
            //{
            //    MultipleWeekDiscount = (int)s.MultipleWeekDiscount;
            //}
        }

        private void SeedCapacity()
        {
            var capacityList = new List<SelectListItem>();
            for (int i = 1; i <= 1000; i++)
            {
                capacityList.Add(new SelectListItem
                    {
                        Text = i.ToString(),
                        Value = i.ToString()
                    });
            }

            CapacityList = new SelectList(capacityList, "Value", "Text");
        }
    }

    public class CampScheduleViewModel
    {
        public int Id { get; set; }

        [Required]
        [DisplayName("Start date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime? Start { get; set; }

        [Required]
        [DisplayName("End date")]
        [DataType(DataType.Date)]
        public DateTime? End { get; set; }

        [DisplayName("Has reduced price?")]
        public bool AnySpecialPrice { get; set; }

        [DisplayName("Morning")]
        public decimal? AmPrice { get; set; }

        [DisplayName("Midday")]
        public decimal? MdPrice { get; set; }

        [DisplayName("Afternoon")]
        public decimal? PmPrice { get; set; }

        [DisplayName("Full day")]
        public decimal? FullPrice { get; set; }

        public string Caption { get; set; }

        public bool HasCompeteEntry { get; set; }

        public decimal TotalRegistrations(string clubDomain, string domain, int scheduleNo)
        {
            decimal total = 0;

            total = Ioc.IOrderDbService.CampPayments(clubDomain, domain, scheduleNo).Sum(c => c.TotalPrice);

            return total;
        }

        public CampScheduleViewModel()
        {

        }

        public CampScheduleViewModel(CampSchedule sch)
        {
            Id = sch.Id;
            Start = sch.Start;
            End = sch.End;
            AmPrice = sch.AmPrice;
            MdPrice = sch.MdPrice;
            PmPrice = sch.PmPrice;
            FullPrice = sch.FullPrice;
            AnySpecialPrice = sch.AnySpecialPrice;
            Caption = sch.Caption;
        }
    }

    public class RegisterCampViewModel
    {
        public PlayerProfile Player { get; set; }

        public int MinimumDOBYear { get; set; }

        public bool HasMultipleWeek { get; set; }

        // System assigned fields set after postback
        public string ItemId { get; set; }
        public string Name { get; set; }
        public string Name1 { get; set; }
        public string Domain { get; set; }
        public string ClubDomain { get; set; }
        public decimal Total { get; set; }
        public string PrimaryPlayerEmail { get; set; }
        public int CategoryId { get; set; }
        public string OutSourcerClubDomain { get; set; }

        public string PlayerImageUrl { get; set; }

        // indicate user that logged in is adult or not
        public bool IsAdultLoggedInUser { get; set; }
        // player information

        //public PlayerProfileViewModel Player { get; set; }

        public PlayerProfileComplitionFormContactViewModel Contact { get; set; }

        // view model for insert parent
        public AddPlayerProfileViewModel Parent { get; set; }
        public PlayerProfileComplitionFormSkillViewModel Skill { get; set; }
        public PlayerProfileComplitionFormDoctorContactViewModel Doctor { get; set; }
        public PlayerProfileSchoolViewModel School { get; set; }
        public PlayerProfileComplitionFormEmergencyContactViewModel EmergencyContact { get; set; }
        public PlayerProfileComplitionFormPlayerProfileViewModel HealthInfo { get; set; }
        public InsuranceViewModel Insurance { get; set; }
        public DonationViewModel Donation { get; set; }

        // if player was not adult and doesent have parent it set to true
        public bool ParentRequired { get; set; }

        // this Property for dropdownlist
        [Display(Name = "Registering for")]
        public int SelectedProfileId { get; set; }

        // this Property for dropdownlist get it value from playerprofile view model
        public IEnumerable<SelectListItem> PlayerLists { get; set; }

        [Required]
        public ICollection<CampsSheduleRegisterViewModel> SchedulesPrograms { get; set; }

        [DisplayName("Sibling discount")]
        public SiblingViewModel Sibling { get; set; }

        [DisplayName("Club membership discount")]
        public MembershipViewModel Membership { get; set; }

        [DisplayName("Club multipleweek discount")]
        public MultipleWeekViewModel MultipleWeek { get; set; }

        [Required]
        [DisplayName("I have read and accept the camp policy(ies)")]
        public bool IsPolicyChecked { get; set; }

        public PolicyViewModel Refund { get; set; }

        public PolicyViewModel Waiver { get; set; }

        public PolicyViewModel MedicalRelease { get; set; }

        public CampPageViewModel CampInfo { get; set; }

        //Club Jumbula Admin defined a template
        public Models.FormModels.FormViewModel JumbulaAdminForm { get; set; }

        // Club Owner defined a template
        public Models.FormModels.FormViewModel ClubOwnerForm { get; set; }

        public bool IsNotCurrentCartDomain
        {
            get
            {
                // Get cart
                var cartDb = Ioc.ICartDbService;

                // Get domain of orders in the cart(if there is no order in cart return empty)
                string currentDomain = cartDb.CurrentDomain(HttpContext.Current);

                if (!string.IsNullOrEmpty(currentDomain))
                {
                    return !(currentDomain == this.ClubDomain);
                }

                return false;
            }
        }

        public bool IsRestrictedAge
        {
            get
            {
                if (CampInfo.CaptureRestriction)
                {
                    // Calculate age of player
                    int age = Utilities.CalculateAge(Player.Contact.DoB.Value, CampInfo.Schedules.First().Start);

                    // Check age (values between min age and max age of event is acceptable)
                    if ((CampInfo.MinAge.HasValue && age < CampInfo.MinAge) || (CampInfo.MaxAge.HasValue && age > CampInfo.MaxAge))
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        public bool IsRestrictedGrade { get; set; }

        // Returns message for not acceptable age range
        public string AgeRestrictionMessage
        {
            get
            {
                if (CampInfo.MinAge.HasValue)
                {
                    if (CampInfo.MaxAge.HasValue)
                    {
                        return string.Format(Constants.Age_Restriction_Message, CampInfo.MinAge, CampInfo.MaxAge);
                    }

                    return string.Format(Constants.Min_Age_Restriction_Message, CampInfo.MinAge);
                }

                if (CampInfo.MaxAge.HasValue)
                {
                    return string.Format(Constants.Max_Age_Restriction_Message, CampInfo.MaxAge);
                }

                return string.Empty;
            }
        }

        // Returns message for no acceptable grade range
        public string GradeRestrictionMessage
        {
            get
            {
                if (CampInfo.MinGrade.HasValue)
                {
                    if (CampInfo.MaxGrade.HasValue)
                    {
                        return string.Format(Constants.Grade_Restriction_Message, CampInfo.MinGrade.Value.ToDescription(), CampInfo.MaxGrade.Value.ToDescription());
                    }
                    return string.Format(Constants.Min_Grade_Restriction_Message, CampInfo.MinGrade.Value.ToDescription());
                }
                if (CampInfo.MaxGrade.HasValue)
                {
                    return string.Format(Constants.Max_Grade_Restriction_Message, CampInfo.MaxGrade.Value.ToDescription());
                }

                return string.Empty;
            }
        }

        // If player grade is not between min and max grade of event, return true
        public bool CheckGradeRestriction(SchoolGradeType? eventMinGrade, SchoolGradeType? eventMaxGrade, SchoolGradeType? playerGrade, bool isAdult)
        {
            if (CampInfo.CaptureRestriction && playerGrade.HasValue && playerGrade != SchoolGradeType.None && !isAdult)
            {
                if ((eventMinGrade.HasValue && eventMinGrade != SchoolGradeType.None && playerGrade < eventMinGrade) || (eventMinGrade.HasValue && eventMaxGrade != SchoolGradeType.None && playerGrade > eventMaxGrade))
                {
                    return true;
                }
            }

            return false;
        }

        public PaymentPlanViewModel PaymentPlan { get; set; }

        public bool IsRestrictedUser { get; set; }

        public RegisterCampViewModel()
        {
            SchedulesPrograms = new HashSet<CampsSheduleRegisterViewModel>();

            Sibling = new SiblingViewModel();
            Membership = new MembershipViewModel();

            Refund = new PolicyViewModel();
            Waiver = new PolicyViewModel();
            MedicalRelease = new PolicyViewModel();
            JumbulaAdminForm = new Models.FormModels.FormViewModel();
            ClubOwnerForm = new Models.FormModels.FormViewModel();
            Donation = new DonationViewModel();
            PaymentPlan = new PaymentPlanViewModel();

        }

        public RegisterCampViewModel(Camp camp)
            : this()
        {
            CampInfo = new CampPageViewModel(camp.Domain, camp);
            Donation = new DonationViewModel(camp.Donation);
        }

        public RegisterCampViewModel(CreateEditCampViewModel camp)
            : this()
        {
            InitRegisterCampViewModel(camp);


            // indecate logged in user is adult or not
            var loggedUserContact = Ioc.IPlayerProfileDbService.GetByUserId(TheMembership.GetCurrentUserId())
                                             .Single(x => x.Type == PlayerProfileType.Primary)
                                             .Contact;
            IsAdultLoggedInUser = Utilities.CalculateAge(loggedUserContact.DoB.Value) >= Constants.PlayerProfile_Min_Age;

            // custom question
            // from category template
            var xmlCustomCategoryMetaData = Ioc.ICategoryDbService.GetTemplate(SubDomainCategories.Camp, camp.Category.CategoryId).CustomFields;
            if (!string.IsNullOrEmpty(xmlCustomCategoryMetaData))
            {
                JumbulaAdminForm = SerializeHelper.Deserialize<Models.FormModels.FormViewModel>(xmlCustomCategoryMetaData);
            }

            // from event : need to enhance, add template metada to vm 
            var xmlCustomEventMetaData = Ioc.ICampDbService.GetEventQuestionMetadata(camp.Domain, camp.ClubDomain);
            if (!string.IsNullOrEmpty(xmlCustomEventMetaData))
            {
                this.ClubOwnerForm = SerializeHelper.Deserialize<Models.FormModels.FormViewModel>(xmlCustomEventMetaData);
            }

            //
            var newCamp = camp.ToCamp();
            CampInfo = new CampPageViewModel(camp.Domain, newCamp);
            Donation = new DonationViewModel(newCamp.Donation);
        }

        public void InitRegisterCampViewModel(Camp camp)
        {
            CampInfo = new CampPageViewModel(camp.Domain, camp);

            // ????????????????????????????
            CreateEditCampViewModel a = new CreateEditCampViewModel(camp);
            Refund = a.Refund;
            Waiver = a.Waiver;
            MedicalRelease = a.MedicalRelease;

            Domain = camp.Domain;
            ClubDomain = camp.ClubDomain;
        }

        public void InitRegisterCampViewModel(CreateEditCampViewModel camp)
        {
            Domain = camp.Domain;
            ClubDomain = camp.ClubDomain;

            var currentUser = TheMembership.GetCurrentUserName();
            PaymentPlan = camp.PaymentPlan;
            IsRestrictedUser = PaymentPlan.PaymentPlanType == PaymentPlanType.Installment && (PaymentPlan.RestrictedType == RestrictedType.AllUser || PaymentPlan.SelectedUsers.Contains(currentUser));

            var chargdiscounts = Ioc.ICampDbService.Get(camp.Domain, camp.ClubDomain).ChargeDiscounts;
            HasMultipleWeek = chargdiscounts.Any(x => x.Category == ChargeDiscountCategories.MultipleWeek);

            // Schedules 
            int counter = 1;
            bool earlyDateExpired = Utilities.IsEventExpired(camp.EarlyBirdEndDate.Value, camp.TimeZone);
            foreach (CampScheduleViewModel item in camp.Schedules)
            {
                CampsSheduleRegisterViewModel campSchedulRegister = new CampsSheduleRegisterViewModel();
                campSchedulRegister.Schedule = item;
                campSchedulRegister.Name = string.Format("Camp {0}", counter.ToString());
                campSchedulRegister.Caption = item.Caption;

                if (camp.AmSection.IsChecked)
                {
                    campSchedulRegister.AmSection = new CampSectionViewModel(camp.AmSection, false, earlyDateExpired);
                }

                if (camp.PmSection.IsChecked)
                {
                    campSchedulRegister.PmSection = new CampSectionViewModel(camp.PmSection, false, earlyDateExpired);
                }

                if (camp.MdSection.IsChecked)
                {
                    campSchedulRegister.MdSection = new CampSectionViewModel(camp.MdSection, false, earlyDateExpired);
                }

                if (camp.FullSection.IsChecked)
                {
                    campSchedulRegister.FullSection = new CampSectionViewModel(camp.FullSection, false, earlyDateExpired);
                }

                // Services
                if (camp.Lunch.IsChecked)
                {
                    campSchedulRegister.Lunch = new ChargeDiscountViewModel(camp.Lunch, false);
                }
                if (camp.MorningChildCare.IsChecked)
                {
                    campSchedulRegister.MorningChildCare = new ChargeDiscountViewModel(camp.MorningChildCare, false);
                }
                if (camp.AfternoonChildCare.IsChecked)
                {
                    campSchedulRegister.AfternoonChildCare = new ChargeDiscountViewModel(camp.AfternoonChildCare, false);
                }

                counter++;
                SchedulesPrograms.Add(campSchedulRegister);
            }

            // Sibling and Membership
            if (camp.Sibling.IsChecked)
            {
                Sibling = new SiblingViewModel(camp.Sibling, false);
            }

            if (camp.Membership.IsChecked)
            {
                Membership = new MembershipViewModel(camp.Membership, false);
                Membership.ExpireDate = DateTime.Today;
            }

            if (camp.MultipleWeek.IsChecked)
            {
                MultipleWeek = new MultipleWeekViewModel(camp.MultipleWeek, false);
            }

            // Policies
            Refund = camp.Refund;
            Waiver = camp.Waiver;
            MedicalRelease = camp.MedicalRelease;
        }
    }


    public class CampsSheduleRegisterViewModel
    {
        public CampsSheduleRegisterViewModel()
        {
        }

        public string Name { get; set; }

        public bool IsChecked { get; set; }

        public CampScheduleViewModel Schedule { get; set; }

        [DisplayName("Morning program")]
        public CampSectionViewModel AmSection { get; set; }

        [DisplayName("Midday program")]
        public CampSectionViewModel MdSection { get; set; }

        [DisplayName("Afternoon program")]
        public CampSectionViewModel PmSection { get; set; }

        [DisplayName("Full day program")]
        public CampSectionViewModel FullSection { get; set; }

        [DisplayName("Lunch")]
        public ChargeDiscountViewModel Lunch { get; set; }

        [DisplayName("Morning extended care")]
        public ChargeDiscountViewModel MorningChildCare { get; set; }

        [DisplayName("Afternoon extended care")]
        public ChargeDiscountViewModel AfternoonChildCare { get; set; }

        public string Caption { get; set; }
    }

    public class RegisterCampSectionModel
    {
        public decimal SiblingDiscountAmount { get; set; }

        public decimal MembershipDiscountAmount { get; set; }

        public decimal EarlybirdDiscountAmount { get; set; }

        public decimal MultipleweekDiscountAmount { get; set; }

        public ICollection<ChargeDiscount> selectedCharges { get; set; }

        public ICollection<ChargeDiscount> selectedDiscounts { get; set; }

        public decimal Total { get; set; }

        public string Description { get; set; }

        public int ScheduleId { get; set; }
        public int ScheduleNo { get; set; }

        public int SectionNo { get; set; }

        public int SectionId { get; set; }

        public RegisterCampSectionModel()
        {
            selectedCharges = new HashSet<ChargeDiscount>();
            selectedDiscounts = new HashSet<ChargeDiscount>();
        }
    }

    public class CampCheckoutBoxViewModel
    {
        public List<CampCheckoutBoxItemViewModel> Items { get; set; }
        public decimal Total { get; set; }

        public ChargeDiscountViewModel Sibling { get; set; }
        public ChargeDiscountViewModel Membership { get; set; }
        public ChargeDiscountViewModel MultipleWeek { get; set; }
    }

    public class CampCheckoutBoxItemViewModel
    {
        public decimal EntryFee { get; set; }
        public decimal ServiceFee { get; set; }
        public List<ChargeDiscountViewModel> Discounts { get; set; }
    }
}