﻿using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jumbula.Core.Domain
{
    public class Campaign : BaseEntity, IProtectedResource
    {
        public Campaign()
        {
            Recipients = new HashSet<CampaignRecipients>();
        }

        [StringLength(128)]
        public string CampaignName { get; set; }

        [StringLength(128)]
        public string Sender { get; set; }

        [StringLength(128)]
        public string ReplyTo { get; set; }

        [StringLength(128)]
        public string Subject { get; set; }
        [StringLength(128)]
        public string BatchId { get; set; }

        public string Body { get; set; }

        public CampaignScheduleType Status { get; set; }

        public DateTime? ScheduleDate { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime? SendDate { get; set; }

        public int EmailTemplate_Id { get; set; }

        public virtual ICollection<CampaignRecipients> Recipients { get; set; }

        public string Attachments { get; set; }

        public EmailCampaignStep LastCreatedPage { get; set; }

        [ForeignKey("Club_Id")]
        public virtual Club Club { get; set; }

        public int Club_Id { get; set; }

        public CampaignReciepientsMode CampaignType { get; set; }
        public string Response { get; set; }

        public bool DoesTheClubOwnIt(int clubId)
        {
            if (Club_Id == clubId)
            {
                return true;
            }

            return false;
        }

        public bool DoesTheParentOwnIt(int userId)
        {
            return false;
        }
    }

    public enum EmailCampaignStep
    {
        Step1 = 1,
        Step2 = 2,
        Step3 = 3,
        Step4 = 4
    }

    public enum DateConditionStep1
    {
        [Description("All")]
        All = 0,

        [Description("Last 30 days")]
        Last30Days = 1,

        [Description("Last 3 months")]
        Last3Months = 2,

        [Description("This year")]
        ThisYear = 3,

        [Description("Last year")]
        LastYear = 4,

        [Description("Custom")]
        Custom = 5
    }


}
