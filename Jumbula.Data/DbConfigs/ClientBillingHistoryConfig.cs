﻿using Jumbula.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Data.DbConfigs
{
    public class ClientBillingHistoryConfig : EntityTypeConfiguration<ClientBillingHistory>
    {
        public ClientBillingHistoryConfig()
        {
            HasRequired(c => c.Client).WithMany(p => p.BillingHistories).HasForeignKey(c => c.ClientId);

        }
    }
}
