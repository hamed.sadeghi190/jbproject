
ALTER TABLE Orders DROP CONSTRAINT [FK_dbo.Orders_dbo.Coupons_CouponId];

ALTER TABLE Orders 
DROP COLUMN [CouponId];
GO

ALTER TABLE Orders 
DROP COLUMN CreditCartType;
GO

ALTER TABLE Orders 
DROP COLUMN PaymentPlanMetaData;
GO

ALTER TABLE OrderItems
ADD CouponId int

ALTER TABLE [dbo].[OrderItems]  WITH CHECK ADD  CONSTRAINT [FK_dbo.OrderItems_dbo.Coupons_CouponId] FOREIGN KEY([CouponId])
REFERENCES [dbo].[Coupons] ([Id])

ALTER TABLE OrderItems
ADD ISTS bigint
