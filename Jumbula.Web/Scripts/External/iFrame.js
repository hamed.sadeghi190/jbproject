﻿
// Add script dynamicly to the page
function loadScript(url, callback) {
    // Adding the script tag to the head as suggested before
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;

    // Then bind the event to the callback function.
    // There are several events for cross browser compatibility.
    script.onreadystatechange = callback;
    script.onload = callback;

    // Fire the loading
    head.appendChild(script);
}

// check if jquery is loaded aleardy 
if (typeof (jQuery) == 'undefined') {
    loadScript('http://code.jquery.com/jquery-1.11.0.min.js', function () {

        // add loading script
        loadScript('https://www.jumbula.com/scripts/showLoading.js', function () {
            $("#JBLoadEvents").showLoading();
        });

        $(document).ready(function () {
            // get home URL
            var clubWebsiteUrl = encodeURIComponent(document.URL);
            var externalEventUrl = jQuery("#JBLoadEvents").attr("data-iframesrc");

            //alert(externalEventUrl + "&returnUrl=" + returnUrl);
            externalEventUrl = externalEventUrl + "&clubWebsiteUrl=" + clubWebsiteUrl;
            var iframe = "<iframe id='jbEventsIframe' style='width: 100%; height: 500px;'  src='" + externalEventUrl + "'></iframe>";

            document.getElementById("JBLoadEvents").innerHTML = iframe;
            jQuery('#jbEventsIframe').load(function () {
                jQuery("#JBLoadEvents").hideLoading();
            });

            // add style loading
            jQuery('head').append('<link rel="stylesheet" href="https://www.jumbula.com/content/showloading.css" type="text/css" />');

        });
    });
}// end of if
else {

    // add loading script
    loadScript('https://www.jumbula.com/scripts/showLoading.js', function () {
        jQuery("#JBLoadEvents").showLoading();
    });

    jQuery(document).ready(function () {

        var clubWebsiteUrl = encodeURIComponent(document.URL);
        //alert(clubWebsiteUrl);
        var externalEventUrl = jQuery("#JBLoadEvents").attr("data-iframesrc");

        //alert(externalEventUrl + "&returnUrl=" + returnUrl);
        externalEventUrl = externalEventUrl + "&clubWebsiteUrl=" + clubWebsiteUrl;
        var iframe = "<iframe id='jbEventsIframe' style='width: 100%; height: 500px;'  src='" + externalEventUrl + "'></iframe>";

        document.getElementById("JBLoadEvents").innerHTML = iframe;
        jQuery('#jbEventsIframe').load(function () {
            jQuery("#JBLoadEvents").hideLoading();
        });


        // add style loading
        jQuery('head').append('<link rel="stylesheet" href="https://www.jumbula.com/content/showloading.css" type="text/css" />');
    });
}


