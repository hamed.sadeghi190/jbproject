﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('campaignAddBladeController', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'campaign-add-blade';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/EmailCampaignAddChooseType", order: 1, menu: "CAMPAIGN" });
    }]);
})();