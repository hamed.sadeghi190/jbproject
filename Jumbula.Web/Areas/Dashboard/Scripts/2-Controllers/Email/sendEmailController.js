﻿(function () {
    'use strict';
    var app = angular.module('dashboardApp');

    app.controller('sendEmailController', ['$scope', '$http', 'jbToast', '$stateParams', '$state', 'templateEditorService', 'emailNotificationService', 'EmailService', 'imageUploadService', function ($scope, $http, jbToast, $stateParams, $state, templateEditorService, emailNotificationService, emailService, imageUploadService) {
        window.app.globalObjects.pageClass = 'form-view';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;
        $scope.Model = emailService.get();

        var baseUrl = window.location.origin + "/Dashboard/Email";

        $http.get(baseUrl + "/GetLimitedExtensions")
            .success(function (data) {
                $scope.LimitedExtensions = data;

                //$scope.ChangeTemplate(0);
            });

        $scope.validateImage = function (elem, pos) {
            if (pos === "header") {
                $scope.positionBanner = "header";
            }
            else if (pos === "footer") {
                $scope.positionBanner = "footer";
            }
            imageUploadService.validateImage(this, elem);
        };
        $scope.typeBannerHeader = function (typeBanner) {
            $scope.typeBannerHeaderValue = typeBanner;
        };
        $scope.typeBannerFooter = function (typeBanner) {
            $scope.typeBannerFooterValue = typeBanner;
        };
        $scope.showCropper1 = true;
        $scope.showCropper2 = false;
        $scope.hideCropper1 = function () {
            $scope.showCropper1 = false;
            $scope.showCropper2 = true;
            $scope.cropper = imageUploadService.clearCropper(this);
        };

        $scope.hideCropper2 = function () {
            $scope.showCropper1 = true;
            $scope.showCropper2 = false;
            $scope.cropper = imageUploadService.clearCropper(this);
        };
        $scope.clearCropper = function () {
            $scope.cropper = imageUploadService.clearCropper(this);
        };

        // init model.
        if (is.undefined($scope.Model)) {
            $scope.Model = {};
            $scope.Model.MailTemplate = {};
        }

        // use service.
        $scope.mailDesignerInit = function () {
            return templateEditorService.mailDesignerInit(this);
        };

        $scope.existingImages = templateEditorService.existingImages;

        $scope.setHeaderBackground = function (image) {
            return templateEditorService.setHeaderBackground(this, image);
        };

        $scope.getHeaderBackground = function () {
            return templateEditorService.getHeaderBackground(this);
        };

        $scope.setFooterBackground = function (image) {
            return templateEditorService.setFooterBackground(this, image);
        };

        $scope.getFooterBackground = function () {
            return templateEditorService.getFooterBackground(this);
        };

        $scope.getClubLogo = function () {
            return templateEditorService.getClubLogo(this);
        };

        $scope.enterEditing = function () {
            return templateEditorService.enterEditing(this);
        };

        $scope.saveEditing = function (posBanner, typeBannerHv, typeBannerFv) {
            $scope.positionBanner = null;
            $scope.ChangedTemplate = true;
            return templateEditorService.saveEditing(this, posBanner, typeBannerHv, typeBannerFv);
        };

        $scope.cancelEditing = function () {
            $scope.positionBanner = null;
            return templateEditorService.cancelEditing(this);
        };

        var returnedByBack = $state.returnByBack();

        if (returnedByBack) {
            $scope.Model = emailNotificationService.get();
        }

        $scope.ChangeTemplate = function (idx) {
            $http.get(baseUrl + "/GetEmailTemplate", { params: { templateId: idx } }).success(function (data) {

                $scope.Model.MailTemplate = data;
                
                if ($scope.Model.MailTemplate.Body !== "" && $scope.Model.MailTemplate.Body !== null)
                    $scope.Model.Body = $scope.Model.MailTemplate.Body;

                if ($scope.Model.TemplateId === null) {
                    $scope.ChangedTemplate = true;
                } else {
                    $scope.ChangedTemplate = false;
                }
            });
        };


        if (returnedByBack) {
            $scope.MC.backQueueItems.pop();
        }

        var strRecipients = JSON.stringify($scope.Model.Recipients);
        var recipients = JSON.parse('{"recipients":' + strRecipients + '}');

        $scope.gridOptionsDataSource = new kendo.data.DataSource({
            dataType: "json",
            data: recipients,
            schema:
                {
                    total: "recipients.length",
                    data: "recipients"
                },
            sort:
                [{
                    field: "Id",
                    dir: "asc"
                }],
            pageSize: 5,
            batch: true,
            serverFiltering: false,
            serverPaging: false,
            serverSorting: false,
            requestStart: function () {
                kendo.ui.progress($('[kendo-grid]'), true);
            },
            requestEnd: function () {
                kendo.ui.progress($('[kendo-grid]'), false);
            }
        });
        $scope.recipientGridOptions = {
            dataSource: $scope.gridOptionsDataSource,
            sortable: true,
            pageable: true,
            columns: [
                {
                    field: "EmailAddress",
                    sortable: true,
                    width: "120px",
                    headerAttributes: {
                        style: "display: none"
                    }
                }
            ]
        };

        $scope.reflective = function () {
            $scope.gridOptionsDataSource.dataSource.read();
        };

        $scope.SendEmail = function () {

            var upload = $("#files").data("kendoUpload");
            var files = upload.getFiles();

            for (var i = 0; i < files.length; i++)
                $scope.Model.Attachments.push({ FileName: files[i].name, FileSize: files[i].size, FileUrl: "" });

            $http.post(baseUrl + "/ValidateFiles", { model: $scope.Model })
                .success(function (res) {
                    if (res.Status === true) {

                        if (files.length > 0) {
                            upload.upload();
                        } else {
                            $scope.onComplete();
                        }
                    }
                    else {
                        $scope.MC.handleErrors($scope, res);
                    }

                });
        };

        var attachments = [];
        $scope.onSuccess = function (e) {

            var result = e.response;

            if (result.Status === true) {
                result.Data.forEach(function (data) {
                    attachments.push(data);
                });
            }
        };

        $scope.onComplete = function () {
            $scope.Model.Attachments = attachments;

            if ($scope.Model.HasTemplate && $scope.Model.TemplateId === null)
                $scope.Model.MailTemplate.Body = $scope.Model.Body;

            $http.post(baseUrl + "/SendEmail", { email: $scope.Model })
                .success(function (result) {

                    if (result.Status === true) {
                        jbToast.success("Email is sent successfully.");
                        window.history.back();
                    }
                    else {
                        $scope.MC.handleErrors($scope, result);
                    }
                });
        };

        $scope.onSelect = function (e) {
            var isLimitedFormat = ($.inArray(e.files[0].extension, $scope.LimitedExtensions)) === -1;

            if (!isLimitedFormat) {
                e.preventDefault();
                $scope.MC.handleErrors($scope, { Message: "These file formats are limited" });
            };
        };

        $scope.PreviewEmail = function () {
            $http.post(baseUrl + "/EmailPreview", { model: $scope.Model })
                .success(function (response) {
                    $scope.previewText2 = response;
                    $scope.isShowPreviewPopup = true;
                });

        };

        $scope.dismissPreviewPopup = function () {
            $scope.isShowPreviewPopup = false;
        };
    }]);
})();