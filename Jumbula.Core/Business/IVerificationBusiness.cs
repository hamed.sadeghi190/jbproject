﻿using Jb.Framework.Common;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IVerificationBusiness
    {
        OperationStatus Create(UserVerification userVerification);
        UserVerification Get(string phone, string code);
        OperationStatus Update(UserVerification userVerification);
        bool IsTokenValid(string token);
        UserVerification GetByToken(string token);
    }
}
