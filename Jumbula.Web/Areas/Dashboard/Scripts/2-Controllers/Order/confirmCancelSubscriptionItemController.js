﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('confirmCancelSubscriptionItemController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'cancelSubscriptionOrderService', function ($scope, $http, $log, $stateParams, $state, jbToast, cancelSubscriptionOrderService) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();

        if (!$scope.MC.account.isPartner) {
            $scope.MC.openBlade({ url: "Dashboard/Blade/Season", order: 1, menu: "SEASONS" });
        }
        $scope.baseUrl = window.location.origin + "/Dashboard/Order";
        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $stateParams.seasonDomain;
        $scope.orderItemId = $stateParams.orderId;

        $scope.Model = cancelSubscriptionOrderService.get();

        $scope.cancelAction = function () {
            cancelSubscriptionOrderService.set($scope.Model);
            $state.back();
        }


        $http.get($scope.baseUrl + '/ConfirmCancelSubscriptionOrderInfo/?orderItemId=' + $scope.Model.OrderItemId + '&effectivedate=' + $scope.Model.EffectiveDate + '&cancelationFee=' + $scope.Model.CancellationFee + '&memo=' + $scope.Model.CancelMemo).success(function (data, status, headers, config) {
            $scope.Model = data;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

     
        $scope.Submit = function () {
            $('#CancelSubmit').prop("disabled", true);
            $http.post($scope.baseUrl + '/SubmitCancelSubscriptionOrder', { model: $scope.Model })
                .success(function (data, status, headers, config) {

                    if (data.Status == true) {
                        $scope.MC.backQueueItems.pop();
                        jbToast.success('Cancel orderItem successfully.');
                        $state.go("OrderItem", { seasonDomain: $stateParams.seasonDomain, orderItemId: $stateParams.orderId })
                    }
                    else {
                        $('#CancelSubmit').prop("disabled", true);
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        };


        $scope.formatDate = function (date) {

            if (date != '' && date != null && date != 'undefined') {
                return new Date(date).format('mm/dd/yyyy');
            }
        }
      
    }]);
})();