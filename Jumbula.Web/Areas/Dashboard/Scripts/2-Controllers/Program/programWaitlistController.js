﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('programWaitlistController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbConfirmation, jbToast) {

        $scope.baseUrl = window.location.origin + "/Program";

        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        $scope.init = function () {
            app.globalObjects.initializeView();
        }

        var programId = $scope.MC.stateParams.programId;

        $scope.Model = {};
        $scope.Model.checkedIds = {};

        $scope.Filters = {};

        $http.get('/Dashboard/Program/GetCampSchedules?programId=' + $stateParams.programId).success(function (data, status, headers, config) {

            $scope.Filters = data;

            console.log($scope.Filters.ProgramType)
        });


        $scope.searchTerm = "";
        $scope.filter = {};
        $scope.filter.scheduleId = 0;
        $scope.waitListDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport: {
                read:
                {
                    url: window.location.origin + '/Dashboard/Program/GetWaitList/',
                    contentType: "application/json; charset=utf-8", // tells the web method to serialize JSON
                    type: "POST" //use HTTP POST or get
                },
                parameterMap: function (data, operation) {
                    if (operation == "read") {
                        data = $.extend(
                        {
                            sort: null,
                            filter: null,
                            programId: programId,
                            scheduleId: $scope.filter.scheduleId,
                        }, data);

                        return JSON.stringify(data);
                    }
                }
            },
            schema:
                {
                    total: "TotalCount",
                    data: "DataSource",
                },
            sort:
                [{
                    field: "Id",
                    dir: "Asc"
                }],
            pageSize: 5,
            batch: true,
            serverFiltering: true,
            serverPaging: true,
            serverSorting: false,
            requestStart: function (e) {
                kendo.ui.progress($('[kendo-grid]'), true);
            },
            requestEnd: function (e) {
                kendo.ui.progress($('[kendo-grid]'), false);
            }
        });

        $scope.waitlistGridOptions = {
            dataSource: $scope.waitListDataSource,
            sortable: true,
            dataBound: function () {
                var grid = $('#waitListGrid').data('kendoGrid');
                if (grid.dataSource.data().length > 0) {

                    if (grid.dataSource.data()[0].ProgramType != 'Camp') {
                        grid.hideColumn(2);
                    }
                }
            },
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            columns: [{
                field: "",
                title: "",
                template: "<input value='#= Id #' type='checkbox' class='checkbox'/>",
                width: "30px",
                headerTemplate: '<input type="checkbox" class="checkAll" />'
            },
            {
                field: "Email",
                title: "Email",
                width:"250px",
            },
             {
                 field: "Schedule",
                 title: "Schedule",
                 width: "200px",
             },
             {
                 field: "Date",
                 title: "Date",
             },
            {
                field: "ParticipantName",
                title: "Participant"
            },
             {
                 field: "PhoneNumber",
                 title: "Phone number"
             },
            {
                field: "Grade",
                title: "Grade"
            }
            ],
        };

        $(document.body).on('click', '#waitListGrid table .checkbox', selectRow);
        $(document.body).on('click', '#waitListGrid table .checkAll', checkAll);

        //on click the checkbox:
        function selectRow() {

            var checked = this.checked,
            row = $(this).closest("tr"),
            grid = $("#waitListGrid").data("kendoGrid"),
            dataItem = grid.dataItem(row);
            $scope.Model.checkedIds[dataItem.Id] = { checked: checked, userId: dataItem.UserId };

            if (checked) {
                //-select the row
                row.addClass("k-state-selected");
            } else {
                //-remove selection
                row.removeClass("k-state-selected");
            }

        }

        function checkAll() {
            var grid = $("#waitListGrid").data("kendoGrid");
            var state = $("#waitListGrid table > thead > tr > th:nth-child(1) > input").prop("checked");
            var ckbox;
            grid.tbody.find("tr").each(function () {
                var $this = $(this);
                ckbox = $this.find("td .checkbox");
                ckbox.prop("checked", state);
                var dataItem = grid.dataItem($this);
                $scope.Model.checkedIds[dataItem.Id] = { checked: state, userId: dataItem.UserId };
                if (state) {
                    //-select the row
                    $this.addClass("k-state-selected");
                } else {
                    //-remove selection
                    $this.removeClass("k-state-selected");
                }
            });

        }

        $scope.deleteItems = function () {

            var ids = [];

            var checkedIds = $scope.Model.checkedIds;

            for (var id in checkedIds) {

                if (checkedIds[id].checked == true) {
                    ids.push(id);
                }
            }

            if (ids.length > 0) {
                jbConfirmation
                    .yes(function () {
                        $http.post('Dashboard/Program/DeleteWaitlistItems', { ids: ids })
                            .success(function (data, status, headers, config) {

                                if (data.Status == true) {

                                    jbToast.success('Items deleted successfully.');

                                    $scope.Model.checkedIds = {};

                                    $scope.waitlistGridOptions.dataSource.read();
                                }
                                else {
                                    $scope.MC.handleErrors($scope, data);
                                }
                            });
                    })
                    .confirm();
            }
            else {
                jbToast.error('Please select at least one item to delete.');
            }
        }

        $scope.MakeCampaign = function () {
            var userIds = [];

            var checkedIds = $scope.Model.checkedIds;
            for (var id in checkedIds) {
                if (checkedIds[id].checked == true) {
                    userIds.push(checkedIds[id].userId);
                }
            }

            if (userIds.length > 0) {
                $http.post("Dashboard/Program/SendEmailToWaitlistUser", { userIds: userIds, programId: programId })
                    .success(function (data, status, headers, config) {
                        if (data.Status) {
                            $state.go('EmailCampaignAddStep2', { campaignId: data.Data, campaignType: 'UserList' });
                        } else {
                            jbToast.error(data.Message);
                        }
                    }).error(function (data, status, headers, config) {

                    });
            } else {
                jbToast.error("Please select at lest a participant to send mail.");
            }
        };


        $scope.AddWaitlist = function () {
            var userIds = [];

            $state.go("Register", { seasonDomain: $stateParams.seasonDomain, programId: programId,Type:"Waitlist" });

          
        };

        $scope.search = function () {
            $("#waitListGrid").data("kendoGrid").dataSource.read();
        };
        $scope.clearFilters = function () {
            $scope.searchTerm = "";
            $scope.filter.scheduleId = 0;
            $("#waitListGrid").data("kendoGrid").dataSource.read();
        }
    }]);
})();