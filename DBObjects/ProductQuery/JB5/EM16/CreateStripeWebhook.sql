
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StripeWebhooks](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[EventId] [nvarchar](max) NULL,
	[livemode] [bit] NOT NULL,
	[Created] [nvarchar](max) NULL,
	[Type] [nvarchar](max) NULL,
	[Data] [nvarchar](max) NULL,
	[Webhook] [nvarchar](max) NULL,
	[IsProceed] [bit] NOT NULL,
	[IsActionTaken] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_dbo.StripeWebhooks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


