﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using Jumbula.Common.Enums;

using Constants = Jumbula.Common.Constants.Constants;

namespace SportsClub.Models
{
    public enum ClubGridSearchMode
    {
        Past,
        PresentAndFuture,
    }

    public enum UserType
    {
        JB,
        FB
        // GP, // Google Plus
        // TW //Twitter
    }

    public enum OrderConfirmationMode
    {
        New = 0,
        Edit = 1,
        Cancel = 2,
    }

    public enum PageStep
    {
        [Description("Base participant information")]
        Step1 = 1,
        [Description("School information")]
        Step2 = 2,
        [Description("medical information")]
        Step3 = 3
    }

    public class DonationViewModel
    {
        [DisplayName("Ask users for a donation?")]
        public bool Checked { get; set; }

        [StringLength(256)]
        public string Title { get; set; }

        public string Description { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Amount should be a positive number.")]
        [ScaffoldColumn(false)]
        public decimal? Amount { get; set; }

        [ScaffoldColumn(false)]
        public bool HasCompleteEntry { get; set; }

        public DonationViewModel()
        {
        }

        //public DonationViewModel(Donation donation)
        //{
        //    Checked = donation.Checked;
        //    Title = donation.Title;
        //    Description = donation.Description;
        //}

        //public Donation ToDonation(DonationViewModel donationViewModel)
        //{
        //    var donation = new Donation()
        //    {
        //        Checked = donationViewModel.Checked,
        //        Title = donationViewModel.Title,
        //        Description = donationViewModel.Description
        //    };
        //    return donation;
        //}

    }

    public class ContactUsViewModel
    {
        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Email is required.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Subject Summary is required.")]
        public string Subject { get; set; }
        [Required(ErrorMessage = "Message is required.")]
        public string Message { get; set; }
    }

    public class PaymentErrorViewModel
    {
        public bool IsMoneyTaken { get; set; }
        public string Subject { get; set; }
        public string ErrorMessage { get; set; }
        public string OrderConfirmationID { get; set; }
        public PaymentErrorViewModel() { }
        public PaymentErrorViewModel(string subject, string msg, string cid, bool moneyTaken)
        {
            IsMoneyTaken = moneyTaken;
            Subject = subject;
            ErrorMessage = msg;
            OrderConfirmationID = cid;
        }
    }

    public class GeneralPaymentViewModel
    {
        public GeneralPaymentViewModel()
        {
            EnableCreditCardPayment = false;
            EnablePaypalPayment = false;
        }
        public JumbulaSubSystem ActionCaller { get; set; }
        public string Token { get; set; }
        public decimal Amount { get; set; }
        public string ClubDomain { get; set; }
        public bool EnablePaypalPayment { get; set; }
        public bool EnableCreditCardPayment { get; set; }
        public CurrencyCodes Currency { get; set; }
        public bool IsPaid { get; set;}
        public bool BackToDetail { get; set; }
    }

    public class MailConfirmForOrderViewModel
    {
        public enum SendType
        {
            Create = 1,
            Edit = 2,
            Cancel = 3
        }
        public MailConfirmForOrderViewModel()
        {
            CardNumber = "";
            Items = "";
            Charges = "";
            Discounts = "";
            var url = System.Web.HttpContext.Current.Request.Url;
            RootSiteUrl = string.Format(Constants.F_HttpsSchemeAuthority, url.Authority);
        }

        public string BodyMailTitle { get; set; }

        public string ClubUrl { get; set; }

        public string RootSiteUrl { get; set; }

        public string ClubImageUrl { get; set; }

        public string PlayerImageUrl { get; set; }

        public string ClubName { get; set; }

        public string EventName { get; set; }

        public string EventAddress { get; set; }

        public string PlayerFullName { get; set; }

        public string OrderPaidDate { get; set; }

        public string PaidAmount { get; set; }

        public string ClubEmail { get; set; }

        public string ClubPhone { get; set; }

        public string OrderConfirmationId { get; set; }

        public string CardNumber { get; set; }

        public string CardType { get; set; }

        public string AdditionalParameters1 { get; set; }

        /////////////////////////////////////
        public string ItemsTitle { get; set; }
        public string ChargesTitle { get; set; }
        public string DiscountTitle { get; set; }


        public string Items { get; set; }

        public string Charges { get; set; }

        public string Discounts { get; set; }
        /////////////////////////////////////

        public string AppliedText { get; set; }

        public string PaypalInfo { get; set; }

        public string Installment { get; set; }

    }

    public class UploadImageViewModel
    {
        public string ChildDirectory { get; set; }
        public string Directory { get; set; }
        public string ImageUrl { get; set; }
        public string UploaderString { get; set; } // store cute uploader generated string
        public bool HasImage { get; set; }
        public string Controller { get; set; }
    }

    public class CategoryFilterViewModel
    {
        public int Id { get; set; }

        public int? ParentId { get; set; }

        public string Name { get; set; }

        public string Text { get; set; }

        public bool Checked { get; set; }

        public int? Order { get; set; }
    }


  

    public class TransferImportedClubToSupportViewModel
    {
        [Required]
        [Display(Name = "Original domain")]
        [StringLength(128, ErrorMessage = "{0} is too long.")]
        [MinLength(3, ErrorMessage = "Enter a complete {0}.")]
        public string Domain { get; set; }

        [Display(Name = "New domain (optional, last change to provide a shorter domain name)")]
        [StringLength(128, ErrorMessage = "{0} is too long.")]
        [MinLength(3, ErrorMessage = "Enter a complete {0}.")]
        public string NewDomain { get; set; }

        public Jumbula.Common.Enums.TimeZone  timeZone { get; set; }
    }

    public class TransferImportedClubFromSupportToOwnerViewModel
    {
        [Required]
        [Display(Name = "Original domain")]
        [StringLength(128, ErrorMessage = "{0} is too long.")]
        [MinLength(3, ErrorMessage = "Enter a complete {0}.")]
        public string OriginalDomain { get; set; }

        [EmailAddress]
        [Display(Name = "Original email address")]
        [StringLength(128, ErrorMessage = "{0} is too long.")]
        public string Email { get; set; }

        [EmailAddress]
        [Display(Name = "New email address (optional, last change to change original email)")]
        [StringLength(128, ErrorMessage = "{0} is too long.")]
        public string NewEmail { get; set; }

        [Required]
        [Display(Name = "Token is valid for")]
        [Range(1, 10)]
        public int TokenIsValidFor { get; set; }
    }

    
    public class GoogleTimeZone
    {
        //// the offset for daylight-savings time in seconds. 
        //public double dstOffset { get; set; }

        //// the offset from UTC (in seconds) for the given location
        //public double rawOffset { get; set; }

        //// string containing the ID of the time zone, such as "America/Los_Angeles" or "Australia/Sydney". 
        //public string timeZoneId { get; set; }

        // a string containing the long form name of the time zone.
        public string TimeZoneName { get; set; }

        // string indicating the status of the response
        public string status { get; set; }

    }

    public class DateOfBirthViewModel
    {
        [Required]
        public int Year { get; set; }

        [Required]
        public int Month { get; set; }

        [Required]
        public int Day { get; set; }

        public DateOfBirthViewModel()
        {

        }

        public DateOfBirthViewModel(DateTime dt)
        {
            Year = dt.Year;
            Month = dt.Month;
            Day = dt.Day;
        }

        public DateTime ToDate()
        {
            try
            {
                return new DateTime(Year, Month, Day);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool Validate
        {
            get
            {
                try
                {
                    DateTime dt = new DateTime(Year, Month, Day);

                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }
    }

    public class OptionalDateOfBirthViewModel
    {

        public int? Year { get; set; }

        public int? Month { get; set; }

        public int? Day { get; set; }

        public OptionalDateOfBirthViewModel(DateTime dt)
        {
            Year = dt.Year;
            Month = dt.Month;
            Day = dt.Day;
        }

        public OptionalDateOfBirthViewModel()
        {
        }
    }

    public class EventInfo
    {
        public string AutoCompleteText { get; set; }

        public string Domain { get; set; }

        public string Name { get; set; }
    }

    public class ExportPdfSchema
    {
        public dynamic Header { get; set; }

        public List<ExportPdfColumn> Columns { get; set; }

        public List<dynamic> Data { get; set; }

        public string FileName { get; set; }
    }

    public class ExportPdfColumn
    {
        public string DisplayName { get; set; }
        public string DataName { get; set; }
        public int Width { get; set; }
    }

   
}