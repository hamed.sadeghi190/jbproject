﻿using System.ComponentModel.DataAnnotations;


namespace SportsClub.Areas.Dashboard.Models.Settings
{
    public class PartnerSettingViewModel
    {
        public bool ReadStripeSettingsFromMember { get; set; }
        public bool ShowWelcomeToJumbula { get; set; }
        public bool ShowSchoolsInHome { get; set; }
        public bool ReadInvoiceSettingsfromPartner { get; set; }
        public bool SendEmailProvider { get; set; }

        [Required(ErrorMessage = "Email content is required.")]
        public string EmailContent { get; set; }
        public bool ShowGenderForCatalog { get; set; }
        public string PriceOptionFeeInputBoxLabel { get; set; }
        public string PriceOptionNote { get; set; }

        public bool HelpInformationFromPartner { get; set; }
        public bool NotificationEmailFromPartner { get; set; }

        public bool ReadRegistrationFeeFromMember { get; set; }
        public bool ReadLoginTabFromPartner { get; set; }
        public bool ReadMessageNewFamilyFromPartner { get; set; }
        public bool ReadAutoChargePolicyFromMember { get; set; }
        public bool ReadDelinquentPolicyFromMember { get; set; }

        public bool ReadProgramsPolicyFromMember { get; set; }

        public bool ReadDependentCareReportInfoFromMember { get; set; }

        public bool ReadGASettingFromPartner { get; set; }

        public bool ReadDuplicateEnrollmentsFromMember { get; set; }

    }
}