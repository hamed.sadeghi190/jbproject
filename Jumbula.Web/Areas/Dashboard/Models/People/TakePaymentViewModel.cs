﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class TakePaymentViewModel
    {
        public TakePaymentViewModel()
        {
            
            PaymentMethods = SeedMakePaymentMethods();
            SelectedPaymentMethod = "0";
            SelectedChargeMethod = "";
            IsEdit = false;
        }
        public long InvoiceNumber { get; set; }
        public int InvoiceId { get; set; }
        public decimal AmountDue { get; set; }
        public string UserName { get; set; }
        public string UserId { get; set; }
        public bool IsStripeEnabled { get; set; }
        public bool IsPaypalEnabled { get; set; }
        public decimal AvailableCredit { get; set; }
        public DateTime? PaymentDate { get; set; }
        [MaxLength(128, ErrorMessage = "{0} cannot be longer than 128 characters.")]
        [Display(Name = "Transaction/Check ID")]
        public string CheckId { get; set; }
        [MaxLength(1000, ErrorMessage = "{0} cannot be longer than 1000 characters.")]
        public string Note { get; set; }
        public bool SendAdminEmail { set; get; }
        public bool SendParentEmail { set; get; }
        public bool SendEmail { set; get; }
        public List<SelectKeyValue<string>> PaymentMethods { get; set; }
        public string SelectedPaymentMethod { get; set; }
        public string SelectedChargeMethod { get; set; }
        public List<SelectKeyValue<string>> ChargeMethodList
        {
            get
            {
                return SeedMakeChargeMethods(IsStripeEnabled, IsPaypalEnabled);
            }

        }
        public bool IsEdit { get; set; }
        List<SelectKeyValue<string>> SeedMakePaymentMethods()
        {
            var result = new List<SelectKeyValue<string>>();
            result.Add(new SelectKeyValue<string>() { Text = "Select a payment method", Value = "0" });
            foreach (PaymentMethod payMethod in Enum.GetValues(typeof(PaymentMethod)))
            {
                if (payMethod != PaymentMethod.None && payMethod != PaymentMethod.Credit && payMethod != PaymentMethod.Express)
                {
                    result.Add(new SelectKeyValue<string>() { Text = payMethod.ToDescription(), Value = ((int)payMethod).ToString() });
                }
            }

            return result;
        }
        List<SelectKeyValue<string>> SeedMakeChargeMethods(bool isStripeEnabled, bool isPaypalEnabled)
        {
            var result = new List<SelectKeyValue<string>>();

            foreach (ChargeMethodForInvoice chargeMethod in Enum.GetValues(typeof(ChargeMethodForInvoice)))
            {
               
                if (chargeMethod == ChargeMethodForInvoice.PayNowWithCreditCard && (isStripeEnabled || isPaypalEnabled))
                {
                    if (!IsPaypalEnabled && isStripeEnabled)
                    {
                        result.Add(new SelectKeyValue<string>()
                        {

                            Value = ((int)chargeMethod).ToString(),
                            Text = "I have the payer credit card and want to charge now"
                        });
                    }
                    else
                    {
                        result.Add(new SelectKeyValue<string>()
                        {

                            Value = ((int)chargeMethod).ToString(),
                            Text = chargeMethod.ToDescription()
                        });
                    }
                }
                if (chargeMethod != ChargeMethodForInvoice.PayNowWithCreditCard)
                {
                    result.Add(new SelectKeyValue<string>()
                    {

                        Value = ((int)chargeMethod).ToString(),
                        Text = chargeMethod.ToDescription()
                    });
                }

            }

            return result;
        }
    }
    public enum ChargeMethodForTakepayment
    {
        [Description("I have already received the payment")]
        ReceivedPayment = 0,
        [Description("I want to pay with the family's available credit")]
        PayWithCredit = 1,
        [Description("I have the payer PayPal or credit card and want to charge now")]
        PayNowWithCreditCard = 2,

    }
}