﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('addContactController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', 'jbRandom', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService, jbRandom) {
        window.app.globalObjects.pageClass = 'catalog-create-view';
        $scope.MC.openBlade({ url: "Dashboard/Blade/Contact", order: 0, menu: "CONTACT" });
        window.app.globalObjects.initializeView();
        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        $http.get('Dashboard/Contact/GetCreate').success(function (data, status, headers, config) {
            $scope.Model = data;
        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.submit = function () {

            $http.post('Dashboard/Contact/Create', { model : $scope.Model})
                      .success(function (response, status, headers, config) {

                          console.log(response);

                          if (response.Status) {
                              $state.go('EditContact', { contactId: response.Data.Id});
                          }
                          else {
                              $scope.MC.handleErrors($scope, response);
                          }
                      });
        }

    }]);
})();

