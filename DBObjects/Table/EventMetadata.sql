IF OBJECT_ID('[dbo].[EventMetaDatas]') IS NULL
BEGIN
	CREATE TABLE [dbo].[EventMetaDatas](
	[Id] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateUpdated] [datetime] NULL,
	[RegistrationPeriod_RegisterStartDate] [datetime] NULL,
	[RegistrationPeriod_RegisterEndDate] [datetime] NULL,
	[EventSearchTags_SpecialNeeds] [bit] NOT NULL,
	[EventSearchTags_Discounts] [bit] NOT NULL,
	[EventSearchTags_OverNight] [bit] NOT NULL,
	[EventSearchTags_AfterSchool] [bit] NOT NULL,
	[EventSearchField_MinAge] [int] NULL,
	[EventSearchField_MaxAge] [int] NULL,
	[EventSearchField_MinGrade] [int] NULL,
	[EventSearchField_MaxGrade] [int] NULL,
	[EventSearchField_Level] [int] NULL,
	[EventSearchField_Gender] [int] NULL,
	[FileName] [nvarchar](max) NULL,
	[FlyerType] [int] NOT NULL,
	[CaptureRestriction] [bit] NOT NULL Default ((0))
	)
END
GO

---<< ADD PRIMARY KEY >>--
IF NOT EXISTS(SELECT 1 FROM sys.objects WHERE name='PK_dbo.EventMetaDatas')
BEGIN
	ALTER TABLE [dbo].[EventMetaDatas] ADD CONSTRAINT [PK_dbo.EventMetaDatas] PRIMARY KEY CLUSTERED
	(
		Id ASC
	)
END
GO

---<< ADD FOREIGN KEYS >>--
IF NOT EXISTS(SELECT 1 FROM sys.objects WHERE name='FK_dbo.EventMetaDatas_dbo.Events_Id')
BEGIN
	ALTER TABLE [dbo].[EventMetaDatas]  WITH CHECK ADD  CONSTRAINT [FK_dbo.EventMetaDatas_dbo.Events_Id] FOREIGN KEY([Id])
	REFERENCES [dbo].[Events] ([Id])
END
GO


