﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Jumbula.Data.Repositories
{
    public class AuditRepository : BaseRepository<JbAudit, Guid>, IAuditRepository
    {
        public AuditRepository(IEntitiesContext context) 
            : base(context)
        {

        }

        public PaginatedList<JbAudit> GetAdminAudits(string clubDomain, Expression<Func<JbAudit, bool>> predicate,
            List<Sort> sorts, int pageIndex, int pageSize)
        {
            var zone = JbZone.AdminDashboard.ToDescription();
            var audits = Entity.Where(e => e.OwnerKey.Equals(clubDomain, StringComparison.OrdinalIgnoreCase) && e.Zone == zone) ;

            audits = audits.Where(predicate);

            audits = audits.OrderBy(sorts).AsQueryable();
            var total = audits.Count();

            audits = audits.Paginate(pageIndex, pageSize);
            return audits.ToPaginatedList(pageIndex, pageSize, total);
        }
    }
}
