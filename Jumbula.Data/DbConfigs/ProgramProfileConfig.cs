﻿using Jumbula.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Data.DbConfigs
{
    public class ProgramProfileConfig : EntityTypeConfiguration<Program>
    {
        public ProgramProfileConfig()
        {
            HasOptional(s => s.OutSourceSeason).WithMany(c => c.OutSourcePrograms).HasForeignKey(s => s.OutSourceSeasonId);
            HasRequired(s => s.Season).WithMany(c => c.Programs).HasForeignKey(s => s.SeasonId);
        }
    }
}
