﻿using System;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
     public class JbFlyer : BaseEntity
     {
        [StringLength(128)]
        public string Name { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string FlyerContent { get; set; }
        public byte FlyerType { get; set; }
      
    }
}
