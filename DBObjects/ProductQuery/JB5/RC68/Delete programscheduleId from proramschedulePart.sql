alter table ProgramSessions drop DF__ProgramSe__Progr__7325FEFD  
Go
alter table ProgramSessions drop CONSTRAINT  [FK_dbo.ProgramSessions_dbo.ProgramScheduleParts_ProgramSchedulePartId]
Go
ALTER TABLE programsessions DROP COLUMN ProgramSchedulePartId

-----------------------------------------------------------------------------------------------------------------------------

alter table ProgramScheduleParts drop  [FK_dbo.ProgramScheduleParts_dbo.ProgramSchedules_programScheduleId]
Go
ALTER TABLE ProgramScheduleParts DROP COLUMN programScheduleId