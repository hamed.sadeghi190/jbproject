Alter Table ProgramSessions ADD [ChargeId] [bigint] NULL

GO

ALTER TABLE [dbo].[ProgramSessions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProgramSessions_dbo.Charges_ChargeId] FOREIGN KEY([ChargeId])
REFERENCES [dbo].[Charges] ([Id])
