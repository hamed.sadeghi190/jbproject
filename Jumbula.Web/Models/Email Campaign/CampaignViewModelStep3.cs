﻿using Jumbula.Common.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class CampaignViewModelStep3 : BaseViewModel<Campaign> 
    {
        public CampaignScheduleType ScheduleType { get; set; }

        public EmailCampaignStep LastCreatedPage { get; set; }

        [Display(Name = "Send Date")]
        public DateTime? SendDate { get; set; }

        [Display(Name = "Send Time")]
        public DateTime? SendTime { get; set; }
    }
}