﻿using Jb.Framework.Common.Forms.JsonFormatter;
using Newtonsoft.Json;

namespace Jb.Framework.Common.Forms
{
    [JsonConverter(typeof(JbElementConverter))]
    public interface ICustomElement : IJbBaseElement
    {
    }
}
