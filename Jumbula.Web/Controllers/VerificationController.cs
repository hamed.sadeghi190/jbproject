﻿using System;
using System.Web.Mvc;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.Controllers;
using Twilio;
using Constants = Jumbula.Common.Constants.Constants;


namespace Jumbula.Web.Controllers
{
    [AllowAnonymous]
    public class VerificationController : JbBaseController
    {


        public virtual ActionResult UserVerification()
        {
            return View();
        }

        [HttpPost]
        public virtual ActionResult SendVerificationCode(string phone)
        {

            var rnd = new Random(Environment.TickCount);
            var vCode = rnd.Next(1111, 9999).ToString();

            return SmsVerification(phone, vCode);


        }

        //private ActionResult CallVerification(string phone, string vCode)
        //{
        //    var twilioNumber = "+16464614317";
        //    var handlerUri = Url.Action("Connect", "Verification", new { verificationCode = vCode }, Request.Url.Scheme);

        //    TwilioRestClient TwilioClient = new TwilioRestClient("AC1e2b3cea68a7bf36ce3096fc5f86c5f8", "cadc3e6ac054f84e2be91cdcac181023");
        //    TwilioClient.InitiateOutboundCall(twilioNumber, "+"+phone, handlerUri);
        //    return Json(new { status = true, message = "" });
        //}
        //[HttpPost]
        //public virtual JsonResult Connect(string verificationCode)
        //{
        //    var twilioAuthToken = "cadc3e6ac054f84e2be91cdcac181023";
        //    Twilio.TwiML.RequestValidator TwilioRequestValidator = new Twilio.TwiML.RequestValidator();
        //    if (!TwilioRequestValidator.IsValidRequest(System.Web.HttpContext.Current, twilioAuthToken))
        //        return Json(new { status = false, message = "" });

        //    var response = new TwilioResponse();
        //    response.Say("Hello this is a call from Jumbula, your verification number is " + verificationCode).Say(verificationCode);
        //    response.Hangup();

        //    return Json(new { status = true, message = "" });
        //}
        private ActionResult SmsVerification(string phone, string vCode)
        {
            var client = new TwilioRestClient("AC6bc267480c185a225dce5ea671d00567", "e4d3aa312c9c853accb042aefb42c10e");
            var message = "Greetings from Jumbula! Please enter " + vCode + " on the free trial page to verify your phone number. This code will expire in 48 hours.";
            var result = client.SendMessage("+16506677430", "+" + phone, message);
            if (result == null || result.RestException != null)
            {
                if (result.RestException.Code.Equals("21211"))
                {
                    return Json(new { status = false, message = "Phone number is invalid" });
                }
                return Json(new { status = false, message = result != null ? result.RestException.Message : Constants.PlayerProfile_Ajax_Request_Failed });
            }

            var res = Ioc.VerificationBusiness.Create(new UserVerification() { Code = vCode, PhoneNumber = phone });

            return Json(new { status = res.Status, message = res.Message });
        }

        [HttpPost]
        public virtual JsonResult VerifyCode(string phone, string code)
        {
            var model = Ioc.VerificationBusiness.Get(phone, code);
            if (model == null)
            {
                return Json(new { status = false, message = "Code is invalid." });
            }
            if (model.IsVerified)
            {
                return Json(new { status = false, message = "Code is already used." });
            }
            model.Token = RandomHelper.GenerateGuid();
            model.IsVerified = true;
            var res = Ioc.VerificationBusiness.Update(model);
            return Json(new { status = res.Status, message = res.Status ? model.Token : res.Message });

        }
        [HttpPost]
        public virtual JsonResult GetToken()
        {
            var token = RandomHelper.GenerateGuid();
            var res = Ioc.VerificationBusiness.Create(new UserVerification() { Token= token , CreatedDate = DateTime.UtcNow });
            return Json(new { status = res.Status, message = token });

        }
    }
}