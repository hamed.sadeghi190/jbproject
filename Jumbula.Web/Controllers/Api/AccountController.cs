﻿using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Identity;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.WebLogic.UserService;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Jumbula.Web.Controllers.Api
{
    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "X-Custom-Header")]
    public class AccountController : ApiController
    {
        private readonly IClubBusiness _clubBusiness;
        private readonly IApplicationUserManager<JbUser, int> _applicationUserManager;
        private readonly IApplicationSignInManager _applicationSignInManager;


        public AccountController()
        {
            _clubBusiness = Ioc.ClubBusiness;
            _applicationUserManager = Ioc.ApplicationUserManager;
            _applicationSignInManager = Ioc.ApplicationSignInManager;
        } 


        [Route("api/Account/Login")]
        public HttpResponseMessage Login([FromBody]LoginModel model)
        {
            HttpResponseMessage response = null;

            try
            {
                if (ModelState.IsValid)
                {
                    var userIdentity = _applicationUserManager.FindAsync(model.UserName, model.Password).Result;

                    if (userIdentity != null)
                    {
                        var accessToken =
                            Ioc.AuthenticationAdditionalHelpers.GetAccessToken(model.UserName, userIdentity.Id);

                        var staffs = GetClubStaffs(userIdentity.UserName);

                        if (!staffs.Any())
                        {
                            response = Request.CreateResponse(HttpStatusCode.OK, new { Status = false, Message = "The app current version does not support the Parent role. You can log in with Instructor, Onsite Coordinator, or Onsite Support Manager roles only." });
                            return response;
                        }

                        #region
                        var instructors = staffs.Where(s => s.JbUserRole.Role.Name == RoleCategory.Instructor.ToString() && s.Status == StaffStatus.Active && !s.IsFrozen);

                        if (instructors.Any())
                        {
                            response = Request.CreateResponse(HttpStatusCode.OK, new { Status = true, UserId = userIdentity.Id, access_token = accessToken });
                            return response;
                        }
                        #endregion

                        #region
                        var onsiteCoordinators = staffs.Where(s => s.JbUserRole.Role.Name == RoleCategory.OnsiteCoordinator.ToString() && s.Status == StaffStatus.Active && !s.IsFrozen);

                        if (onsiteCoordinators.Any())
                        {
                            response = Request.CreateResponse(HttpStatusCode.OK, new { Status = true, UserId = userIdentity.Id, access_token = accessToken });
                            return response;
                        }
                        #endregion

                        #region
                        var onsiteSupportManager = staffs.Where(s => s.JbUserRole.Role.Name == RoleCategory.OnsiteSupportManager.ToString() && s.Status == StaffStatus.Active && !s.IsFrozen);

                        if (onsiteSupportManager.Any())
                        {
                            response = Request.CreateResponse(HttpStatusCode.OK, new { Status = true, UserId = userIdentity.Id, access_token = accessToken });
                            return response;
                        }
                        #endregion

                        #region
                        var admins = staffs.Where(s => (s.JbUserRole.Role.Name == RoleCategory.Admin.ToString() ||
                                                        s.JbUserRole.Role.Name == RoleCategory.Partner.ToString() ||
                                                        s.JbUserRole.Role.Name == RoleCategory.Manager.ToString() ||
                                                        s.JbUserRole.Role.Name == RoleCategory.SchoolContributor.ToString())
                                                       && s.Status == StaffStatus.Active && !s.IsFrozen).ToList();

                        if (admins.Any())
                        {
                            response = Request.CreateResponse(HttpStatusCode.OK, new { Status = true, UserId = userIdentity.Id, access_token = accessToken });
                            return response;
                        }
                        #endregion

                        var roles = staffs.Select(s => s.JbUserRole.Role.Name).ToList();
                        string formatRoles = FormatRolesForMessage(roles);

                        response = Request.CreateResponse(HttpStatusCode.OK, new { Status = false, Message = "The app current version does not support the " + formatRoles + " role. You can log in with Instructor, Onsite Coordinator, or Onsite Support Manager roles only." });
                        return response;
                    }
                }

                response = Request.CreateResponse(HttpStatusCode.OK, new { Status = false, Message = "Your email or password is incorrect, please try again." });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }

            throw new NotImplementedException("TODO");
        }

        private List<ClubStaff> GetClubStaffs(string userName)
        {
            var result = Ioc.ClubBusiness.GetStaffsByUserName(userName);
            return result;
        }

        private string GetLastLoggedIn(string userName)
        {
            var result = Ioc.UserProfileBusiness.GetLastLoggedinClubDomain(userName);
            return result;
        }

        private string FormatRolesForMessage(List<string> roles)
        {
            string formatRoles = string.Empty;
            if (roles.Count == 1)
            {
                formatRoles = roles.SingleOrDefault();
            }
            else if (roles.Count == 2)
            {
                formatRoles = string.Format("{0} or {1}", roles[0], roles[1]);
            }
            else if (roles.Count == 3)
            {
                formatRoles = string.Format("{0}, {1} or {2}", roles[0], roles[1], roles[2]);
            }
            else if (roles.Count > 3)
            {
                formatRoles = string.Join(", ", roles);
            }
            return formatRoles;
        }

        [Authorize]//(Roles = "Instructor, Admin, Manager")]
        [Route("api/Account/Checkin")]
        public HttpResponseMessage Checkin([FromBody]CheckinModel model)
        {
            HttpResponseMessage response = null;

            try
            {
                var userId = User.Identity.GetCurrentUserId();

                var program = Ioc.ProgramBusiness.Get(model.ProgramId.Value);
                var club = new Club();  
                if (program.OutSourceSeason != null)
                {
                    club = program.OutSourceSeason.Club;
                }
                var clubStaff = club.ClubStaffs.SingleOrDefault(s => s.JbUserRole.UserId == userId && !s.IsDeleted);
                if (clubStaff == null)
                {
                    club = program.Club;
                }
                var clubId = club.Id;

                var clubDateTime = Ioc.ClubBusiness.GetClubDateTime(clubId, DateTime.UtcNow);

                var type = !string.IsNullOrEmpty(model.Type) ? model.Type : UserEventType.Checkin.ToString();

                var userEvent = new UserEvent()
                {
                    ClubDateTime = clubDateTime,
                    LocalDateTime = model.LocalDateTime,
                    ProgramId = model.ProgramId.HasValue ? model.ProgramId.Value : model.ProgramId,
                    SessionId = model.SessionId.HasValue ? model.SessionId.Value : model.SessionId,
                    UserId = userId,
                    ClubId = clubId,
                    Type = Jumbula.Common.Helper.EnumHelper.ParseEnum<UserEventType>(type),
                };

                var postalAddress = new PostalAddress();

                GoogleMap.GetGeo(model.Lat, model.Lng, ref postalAddress);

                userEvent.Address = postalAddress;

                var result = Ioc.UserEventBusiness.CreateEdit(userEvent);

                response = Request.CreateResponse(HttpStatusCode.OK, new { Status = result.Status, Exception = result.ExceptionMessage });

                if (!result.Status)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(result.Exception);
                }

                return response;
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return response;
            }
        }

        [Authorize]//(Roles = "Instructor, Admin, Manager")]
        [HttpGet]
        [Route("api/Account/CanOnsitePersonCheckin_V2_2_1")]
        public HttpResponseMessage CanOnsitePersonCheckin_V2_2_1(int clubId, int userId)
        {
            HttpResponseMessage response = null;

            try
            {
                var message = "";

                var clubDateTime = Ioc.ClubBusiness.GetClubDateTime(clubId, DateTime.UtcNow);
                var today = clubDateTime.Date;
                var tomorrow = clubDateTime.AddDays(1).Date;

                var events = Ioc.UserEventBusiness.GetList(u => u.ClubId == clubId && u.UserId == userId && (u.ClubDateTime >= today && u.ClubDateTime < tomorrow) && !u.ProgramId.HasValue && !u.SessionId.HasValue).ToList();

                var status = events != null && events.Any() ? (events.LastOrDefault().Type == UserEventType.Checkin ? false : true) : true;
                               
                if (!status)
                {
                    message = events != null ? "You already have checked in for today." : "";
                }

                response = Request.CreateResponse(HttpStatusCode.OK, new { Status = status, Message = message });

                return response;
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return response;
            }
        }

        [Authorize]//(Roles = "Instructor, Admin, Manager")]
        [HttpGet]
        [Route("api/Account/CanOnsitePersonCheckout_V2_2_1")]
        public HttpResponseMessage CanOnsitePersonCheckout_V2_2_1(int clubId, int userId)
        {
            HttpResponseMessage response = null;

            try
            {
                var message = "";

                var clubDateTime = Ioc.ClubBusiness.GetClubDateTime(clubId, DateTime.UtcNow);
                var today = clubDateTime.Date;
                var tomorrow = clubDateTime.AddDays(1).Date;

                var events = Ioc.UserEventBusiness.GetList(u => u.ClubId == clubId && u.UserId == userId && (u.ClubDateTime >= today && u.ClubDateTime < tomorrow) && !u.ProgramId.HasValue && !u.SessionId.HasValue).ToList();

                var status = events != null && events.Any() ? (events.LastOrDefault().Type == UserEventType.Checkout ? false : true) : false;

                if (!status)
                {
                    message =  !events.Any() ? "You must check in before you can check out."  : "You already have checked out for today.";
                }

                response = Request.CreateResponse(HttpStatusCode.OK, new { Status = status, Message = message });

                return response;
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return response;
            }
        }

        [Authorize]//(Roles = "Instructor, Admin, Manager")]
        [Route("api/Account/Checkin_V2_2_1")]
        public HttpResponseMessage Checkin_V2_2_1([FromBody]CheckinModel model)
        {
            HttpResponseMessage response = null;

            try
            {
                var clubDateTime = Ioc.ClubBusiness.GetClubDateTime(model.ClubId.Value, DateTime.UtcNow);

                var userEvent = new UserEvent()
                {
                    ClubDateTime = clubDateTime,
                    LocalDateTime = model.LocalDateTime,
                    ProgramId = model.ProgramId.HasValue ? model.ProgramId.Value : model.ProgramId,
                    SessionId = model.SessionId.HasValue ? model.SessionId.Value : model.SessionId,
                    UserId = model.UserId.Value,
                    ClubId = model.ClubId.HasValue ? model.ClubId.Value : model.ClubId,
                    Type = Jumbula.Common.Helper.EnumHelper.ParseEnum<UserEventType>(model.Type),
                };

                var postalAddress = new PostalAddress();

                GoogleMap.GetGeo(model.Lat, model.Lng, ref postalAddress);

                userEvent.Address = postalAddress;

                var result = new OperationStatus();

                if (model.ProgramId.HasValue && model.SessionId.HasValue)
                { 
                    result = Ioc.UserEventBusiness.CreateEdit(userEvent);
                }
                else
                {
                    result = Ioc.UserEventBusiness.Create(userEvent); 
                }

                response = Request.CreateResponse(HttpStatusCode.OK, new { Status = result.Status, Exception = result.ExceptionMessage });

                if (!result.Status)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(result.Exception);
                }

                return response;
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return response;
            }
        }

        [Authorize]//(Roles = "Instructor, Admin, Manager")]
        [Route("api/Account/GetStudentInfo")]
        public HttpResponseMessage GetStudentInfo(int studentId, int programId)
        {
            HttpResponseMessage response = null;

            try
            {
                var formBusiness = Ioc.FormBusiness;

                var student = Ioc.PlayerProfileBusiness.Get(studentId);
                var orderItems = Ioc.OrderItemBusiness.GetProgramOrderItems(programId).Where(o => o.PlayerId == student.Id);

                var authorizedAdultsforPickUp = new List<string>();
                var authorizedAdultsforPickUpPhoneNumber = new List<string>();

                SchoolGradeType? studentGrade = null;

                var firstName = string.Empty;
                var lastName = string.Empty;
                var parentFirstName = string.Empty;
                var parentLastName = string.Empty;
                var parentEmail = string.Empty;
                var parentPhone = string.Empty;
                var parent2FirstName = string.Empty;
                var parent2LastName = string.Empty;
                var parent2Email = string.Empty;
                var parent2Phone = string.Empty;
                var emergencyHomePhoneNumber = string.Empty;
                var emergencyWorkPhoneNumber = string.Empty;
                var emergencyName = string.Empty;
                var teacher = string.Empty;
                var standardDismissal = string.Empty;
                var dismissalFromEnrichment = string.Empty;
                var studentImageUrl = string.Empty;

                var studentOrderItems = orderItems != null ? orderItems.ToList() : new List<OrderItem>();


                if (studentOrderItems.Any())
                {
                    var lastOrderJbForm = studentOrderItems.Last().JbForm;

                    if (lastOrderJbForm != null)
                    {
                        firstName = student.Contact.FirstName;
                        lastName = student.Contact.LastName;
                        studentGrade = formBusiness.GetPlayerGrade(lastOrderJbForm);

                        parentFirstName = formBusiness.GetFormValue(lastOrderJbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName);
                        parentLastName = formBusiness.GetFormValue(lastOrderJbForm, SectionsName.ParentGuardianSection, ElementsName.LastName);
                        parentEmail = formBusiness.GetFormValue(lastOrderJbForm, SectionsName.ParentGuardianSection, ElementsName.Email);
                        parentPhone = formBusiness.GetFormValue(lastOrderJbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" });

                        parent2FirstName = formBusiness.GetFormValue(lastOrderJbForm, SectionsName.Parent2GuardianSection, ElementsName.FirstName);
                        parent2LastName = formBusiness.GetFormValue(lastOrderJbForm, SectionsName.Parent2GuardianSection, ElementsName.LastName);
                        parent2Email = formBusiness.GetFormValue(lastOrderJbForm, SectionsName.Parent2GuardianSection, ElementsName.Email);
                        parent2Phone = formBusiness.GetFormValue(lastOrderJbForm, SectionsName.Parent2GuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" });

                        authorizedAdultsforPickUp = formBusiness.GetAuthorizedAdultNames(lastOrderJbForm);
                        authorizedAdultsforPickUpPhoneNumber = formBusiness.GetAuthorizedAdultPhones(lastOrderJbForm);

                        emergencyHomePhoneNumber = formBusiness.GetFormValue(lastOrderJbForm, SectionsName.EmergencyContactSection, ElementsName.HomePhone, new List<string> { "Emergency contact phone number", "Emergency contact phone", "Phone" });

                        if (lastOrderJbForm.Elements.Any(s => s.Name == SectionsName.EmergencyContactSection.ToString()) && ((JbSection)lastOrderJbForm.Elements.Single(s => s.Name == SectionsName.EmergencyContactSection.ToString())).Elements.Any(e => e.Name == ElementsName.WorkPhone.ToString()))
                        {
                            try
                            {
                                emergencyWorkPhoneNumber = ((JbSection)lastOrderJbForm.Elements.Single(s => s.Name == SectionsName.EmergencyContactSection.ToString())).Elements.Single(e => e.Name == ElementsName.WorkPhone.ToString()).GetValue().ToString();
                            }
                            catch
                            {
                                emergencyWorkPhoneNumber = string.Empty;
                            }
                        }
                        emergencyName = string.Format("{0} {1}", formBusiness.GetFormValue(lastOrderJbForm, SectionsName.EmergencyContactSection, ElementsName.FirstName), formBusiness.GetFormValue(lastOrderJbForm, SectionsName.EmergencyContactSection, ElementsName.LastName));

                        teacher = formBusiness.GetTeacher(lastOrderJbForm);
                        standardDismissal = formBusiness.GetFormValue(lastOrderJbForm, SectionsName.SchoolSection, ElementsName.StandardDismissal);
                        dismissalFromEnrichment = formBusiness.GetFormValue(lastOrderJbForm, SectionsName.SchoolSection, ElementsName.DismissalFromEnrichment);

                        //var genderStudent = getStudentGender(lastOrderJbForm);
                        var studentImageFolder = Ioc.UserProfileBusiness.GetStudentImageFolder(student.Gender);
                        studentImageUrl = string.Concat(string.Format("{0}://{1}", Url.Request.RequestUri.Scheme, Url.Request.RequestUri.Host), studentImageFolder);
                    }
                }

                var info = new
                {
                    FirstName = StringHelper.ReturnEmptyIfNull(firstName),
                    LastName = StringHelper.ReturnEmptyIfNull(lastName),
                    Grade = studentGrade.HasValue ? studentGrade.Value.ToDescription() : string.Empty,
                    ParentFirstName = parentFirstName != null ? StringHelper.ReturnEmptyIfNull(parentFirstName) : string.Empty,
                    ParentLastName = parentLastName != null ? StringHelper.ReturnEmptyIfNull(parentLastName) : string.Empty,
                    Parent2FirstName = parent2FirstName != null ? StringHelper.ReturnEmptyIfNull(parent2FirstName) : string.Empty,
                    Parent2LastName = parent2LastName != null ? StringHelper.ReturnEmptyIfNull(parent2LastName) : string.Empty,
                    CellNumberForParent = parentPhone != null ? StringHelper.ReturnEmptyIfNull(parentPhone) : string.Empty,
                    CellNumberForParent2 = parent2Phone != null ? StringHelper.ReturnEmptyIfNull(parent2Phone) : string.Empty,
                    ParentEmail = parentEmail != null ? StringHelper.ReturnEmptyIfNull(parentEmail) : string.Empty,
                    Parent2Email = parent2Email != null ? StringHelper.ReturnEmptyIfNull(parent2Email) : string.Empty,
                    AuthorizedAdultsForPickup = authorizedAdultsforPickUp,
                    AuthorizedAdultsPickupPhone = authorizedAdultsforPickUpPhoneNumber,
                    EmergencyName = StringHelper.ReturnEmptyIfNull(emergencyName),
                    EmergencyHomePhone = StringHelper.ReturnEmptyIfNull(emergencyHomePhoneNumber),
                    EmergencyWorkPhone = StringHelper.ReturnEmptyIfNull(emergencyWorkPhoneNumber),
                    TeacherName = StringHelper.ReturnEmptyIfNull(teacher),
                    StandardDismissal = StringHelper.ReturnEmptyIfNull(standardDismissal),
                    DismissalFromEnrichment = StringHelper.ReturnEmptyIfNull(dismissalFromEnrichment),
                    StudentImageUrl = studentImageUrl
                };

                response = Request.CreateResponse(HttpStatusCode.OK, info);
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.InnerException != null ? ex.InnerException.Message : ex.Message });
                return response;
            }
        }

        [HttpGet]
        [Route("api/Account/GetSeasonInstructorList")]
        public HttpResponseMessage GetSeasonInstructorList(int clubId, long seasonId)
        {
            HttpResponseMessage response = null;

            try
            {
                var result = new List<object>();
                var programBusiness = Ioc.ProgramBusiness;

                var clubDateTime = Ioc.ClubBusiness.GetClubDateTime(clubId, DateTime.UtcNow);

                var programs = programBusiness.GetList().Where(p => p.SeasonId == seasonId && p.Status != ProgramStatus.Deleted && p.Status != ProgramStatus.Frozen
                         && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Season.Status == SeasonStatus.Live && p.TypeCategory == ProgramTypeCategory.Class)
                         .Include("ProgramSchedules").Include("ProgramSchedules.Charges").Include("ProgramSchedules.Charges.ProgramSessions");

                foreach (var program in programs)
                {
                    var sessions = programBusiness.GetSessions(program);
                    foreach (var session in sessions)
                    {
                        if (session.Start.Date == clubDateTime.Date)
                        {
                            var instructors = program.Instructors.Select(i => new
                            {
                                UserId = i.JbUserRole.UserId,
                                FullName = i.Contact.FullName
                            });
                            foreach (var item in instructors)
                            {
                                result.Add(new
                                {
                                    UserId = item.UserId,
                                    InstructorFullName = item.FullName,
                                    ProgramId = program.Id,
                                    ProgramName = program.Name,
                                    SessionId = session.Id,
                                    ClubId = program.OutSourceSeason != null ? program.OutSourceSeason.ClubId : program.ClubId,
                                    SessionDateTime = string.Concat(session.Start.ToString("ddd, MMM dd, yyyy"), " ", session.Start.ToString("h:mm tt"), " - ", session.End.ToString("h:mm tt"))
                                });
                            }
                            break;
                        }
                    }
                }

                response = Request.CreateResponse(HttpStatusCode.OK, new { Data = result });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        private GenderCategories getStudentGender(JbForm jbForm)
        {
            GenderCategories result = GenderCategories.None;

            var gender = Ioc.FormBusiness.GetPlayerGender(jbForm);

            if (gender.Value == Genders.Male)
            {
                result = GenderCategories.Male;
            }
            else if (gender.Value == Genders.Female)
            {
                result = GenderCategories.Female;
            }
            else if (gender.Value == Genders.NoRestriction)
            {
                result = GenderCategories.All;
            }
            else if (gender == null)
            {
                result = GenderCategories.None;
            }

            return result;
        }

    }


    public class LoginModel
    {
        public string UserName { get; set; }

        public string Password { get; set; }

    }

    public class CheckinModel
    {
        public string ClubDomain { get; set; }

        public int? SessionId { get; set; }

        public int? ProgramId { get; set; }

        public int? UserId { get; set; }

        public DateTime LocalDateTime { get; set; }

        public double Lat { get; set; }

        public double Lng { get; set; }

        public int? ClubId { get; set; }

        public string Type { get; set; }

    }
}
