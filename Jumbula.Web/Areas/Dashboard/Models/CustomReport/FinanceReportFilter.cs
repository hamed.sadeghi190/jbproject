﻿using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using System;
using System.Collections.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class FinanceReportFilter
    {
        public DateTime? RegistrationStart { get; set; }

        public DateTime? RegistrationEnd { get; set; }

        public OrderItemStatusReasons? Status { get; set; }

        public long? Tuition { get; set; }

        public BalanceReportFilters BalanceFilter { get; set; }

        public int CouponFilter { get; set; }

        public TransactionFinanceFilters TransactionFilters { get; set; }

        public List<long> _SelectedPrograms { get; set; }

    }

    public class TransactionFinanceFilters
    {
        public List<TransactionCategory?> TransactionCategory { get; set; }

        public PaymentMethod? PaymentMethod { get; set; }
    }
}