﻿
using Jb.Framework.Common;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Domain.Generic;

namespace Jumbula.Business
{
    public class ProgramSessionBusiness : IProgramSessionBusiness
    {
        private readonly IClubBusiness _clubBusiness;
        private readonly IProgramBusiness _programBusiness;
        private readonly IOrderSessionBusiness _orderSessionBusiness;
        private readonly ISubscriptionBusiness _subscriptionBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IRepository<ProgramSession> _programSessionRepository;
        private readonly IEntitiesContext _dataContext;
        private readonly IOrderHistoryBusiness _orderHistoryBusiness;
        private readonly ITransactionActivityBusiness _transactionActivityBusiness;

        public ProgramSessionBusiness(IClubBusiness clubBusiness, IProgramBusiness programBusiness,
            IOrderSessionBusiness orderSessionBusiness, ISubscriptionBusiness subscriptionBusiness,
            IOrderItemBusiness orderItemBusiness, IRepository<ProgramSession> programSessionRepository,
            IEntitiesContext dataContext, IOrderHistoryBusiness orderHistoryBusiness, ITransactionActivityBusiness transactionActivityBusiness)
        {
            _clubBusiness = clubBusiness;
            _programBusiness = programBusiness;
            _orderSessionBusiness = orderSessionBusiness;
            _subscriptionBusiness = subscriptionBusiness;
            _orderItemBusiness = orderItemBusiness;
            _programSessionRepository = programSessionRepository;
            _dataContext = dataContext;
            _orderHistoryBusiness = orderHistoryBusiness;
            _transactionActivityBusiness = transactionActivityBusiness;
        }

        public ProgramSession Get(int id)
        {
            return _programSessionRepository.GetList().SingleOrDefault(s => s.Id == id);
        }

        public OperationStatus Create(Program program, int dueDate = 0)
        {
            var result = new OperationStatus();

            var deleteResult = new OperationStatus();

            if (!_orderSessionBusiness.HasOrder(program))
            {
                deleteResult = Delete(program);
            }
            else
            {
                deleteResult = DeleteLogicaly(program);
            }

            var programScheduleParts = new List<ProgramSchedulePart>();

            foreach (var programSchedule in program.ProgramSchedules.Where(s => !s.IsDeleted))
            {
                programScheduleParts.AddRange(Create(programSchedule, dueDate));
            }

            _dataContext.Set<ProgramSchedulePart>().AddRange(programScheduleParts);

            result.Status = _dataContext.SaveChanges() > 0;

            return result;
        }

        public List<ProgramSchedulePart> Create(ProgramSchedule programSchedule, int dueDate)
        {
            var result = new List<ProgramSchedulePart>();

            switch (programSchedule.Program.TypeCategory)
            {
                case ProgramTypeCategory.Subscription:
                    {
                        var startDate = programSchedule.StartDate;
                        var endDate = programSchedule.EndDate;

                        var subscriptionAttribute = _subscriptionBusiness.GetSubscriptionAtribute(programSchedule);

                        //var subscriptionItems = subscriptionAttribute.SubscriptionItems;

                        var subscriptionScheduleParts = _programBusiness.GenerateSubscriptionScheduleParts(startDate, endDate, 0, dueDate);

                        var days = subscriptionAttribute.Days;

                        foreach (var item in subscriptionScheduleParts)
                        {
                            var programSchedulePart = new ProgramSchedulePart()
                            {
                                StartDate = item.StartDate,
                                EndDate = item.EndDate,
                                ProgramId = programSchedule.ProgramId,
                                DueDate = item.DueDate
                            };

                            result.Add(programSchedulePart);
                        }

                        programSchedule.ProgramSessions = Create(subscriptionScheduleParts, days);

                        break;
                    }
                case ProgramTypeCategory.Class:
                    {
                        var startDate = programSchedule.StartDate;
                        var endDate = programSchedule.EndDate;

                        var sessions = _programBusiness.GenerateSessions(programSchedule.StartDate, programSchedule.EndDate, ((ScheduleAttribute)programSchedule.Attributes).Days, ((ScheduleAttribute)programSchedule.Attributes).ContinueType, ((ScheduleAttribute)programSchedule.Attributes).Occurances); //ServiceFactory.Get<ProgramBusiness>().GetSessions(programSchedule);

                        var programSchedulePart = new ProgramSchedulePart()
                        {
                            EndDate = endDate,
                            StartDate = startDate,
                            ProgramId = programSchedule.ProgramId,
                        };

                        var catalogComments = new Dictionary<int, string>();

                        if (programSchedule.Program.CatalogId.HasValue)
                        {
                            var catalog = programSchedule.Program.Catalog;

                            var catalogPriceOptions = catalog.PriceOptions;

                            var programSessionCount = sessions.Count() + 1;

                            var condidateCatalogPriceOption = catalogPriceOptions.ToList().FirstOrDefault(c => c.Attribute != null && c.Attribute.AgendaList != null && c.Attribute.AgendaList.Count == programSessionCount);

                            var agendaList = new List<CatalogPriceOptionAgendaItem>();

                            if (condidateCatalogPriceOption != null)
                            {
                                agendaList = condidateCatalogPriceOption.Attribute.AgendaList;
                            }

                            for (int i = 0; i < agendaList.Count; i++)
                            {
                                catalogComments.Add(i + 1, agendaList[i].AgendaWeek);
                            }
                        }

                        int j = 1;

                        foreach (var item in sessions.Where(s => !s.IsDeleted))
                        {
                            var session = new ProgramSession();

                            session.StartDateTime = item.Start;
                            session.EndDateTime = item.End;
                            session.WeeklyAgenda = catalogComments.ContainsKey(j) ? catalogComments[j] : string.Empty;

                            programSchedule.ProgramSessions.Add(session);

                            j++;
                        }

                        result.Add(programSchedulePart);

                    }
                    break;
                case ProgramTypeCategory.BeforeAfterCare:
                    {
                        if (programSchedule.ScheduleMode != TimeOfClassFormation.Both)
                        {
                            var startDate = programSchedule.StartDate;
                            var endDate = programSchedule.EndDate;
                        }
                    }
                    break;
                default:
                    {
                        throw new NotImplementedException();
                    }
            }

            return result;
        }

        public List<ProgramSession> Create(List<ProgramSubscriptionItem> subscriptionItems, List<ProgramScheduleDay> days)
        {
            var result = new List<ProgramSession>();

            foreach (var subscriptionItem in subscriptionItems)
            {
                var startDate = subscriptionItem.StartDate;
                var endDate = subscriptionItem.EndDate;

                for (DateTime dateTime = startDate; dateTime <= endDate; dateTime = dateTime.AddDays(1))
                {
                    if (days.Select(d => d.DayOfWeek).Contains(dateTime.DayOfWeek))
                    {
                        var day = days.Single(d => d.DayOfWeek == dateTime.DayOfWeek);

                        var startTime = day.StartTime;
                        if (!startTime.HasValue)
                        {
                            startTime = new TimeSpan(0, 0, 0);
                        }

                        var endTime = day.EndTime;
                        if (!endTime.HasValue)
                        {
                            endTime = new TimeSpan(23, 59, 59);
                        }

                        var programSession = new ProgramSession()
                        {
                            StartDateTime = dateTime.Date.Add(startTime.Value),
                            EndDateTime = dateTime.Date.Add(endTime.Value),
                        };

                        result.Add(programSession);
                    }
                }
            }


            return result;
        }

        public List<ProgramSession> Create(List<ProgramSchedulePart> subscriptionParts, List<ProgramScheduleDay> days)
        {
            var result = new List<ProgramSession>();

            foreach (var subscriptionPart in subscriptionParts)
            {
                var startDate = subscriptionPart.StartDate;
                var endDate = subscriptionPart.EndDate;

                for (DateTime dateTime = startDate; dateTime <= endDate; dateTime = dateTime.AddDays(1))
                {
                    if (days.Select(d => d.DayOfWeek).Contains(dateTime.DayOfWeek))
                    {
                        var day = days.Single(d => d.DayOfWeek == dateTime.DayOfWeek);

                        var startTime = day.StartTime;
                        if (!startTime.HasValue)
                        {
                            startTime = new TimeSpan(0, 0, 0);
                        }

                        var endTime = day.EndTime;
                        if (!endTime.HasValue)
                        {
                            endTime = new TimeSpan(23, 59, 59);
                        }

                        var programSession = new ProgramSession()
                        {
                            StartDateTime = dateTime.Date.Add(startTime.Value),
                            EndDateTime = dateTime.Date.Add(endTime.Value),
                        };

                        result.Add(programSession);
                    }
                }
            }

            return result;
        }

        public OperationStatus CreateScheduleSessions(List<ProgramSchedule> schedules)
        {
            var result = new OperationStatus();
            var programSessionList = new List<ProgramSession>();
            var deleteResult = new OperationStatus();
            var hasOrder = false;
            var program = _programBusiness.Get(schedules.FirstOrDefault().ProgramId);

            if (_orderSessionBusiness.HasOrder(program))
            {
                hasOrder = true;
            }

            foreach (var schedule in schedules)
            {
                deleteResult = DeleteLogicaly(schedule);

                var beforeAfterCareAttribute = _subscriptionBusiness.GetBeforAfterCareAtribute(schedule);
                var days = beforeAfterCareAttribute.Days;

                var startDate = schedule.StartDate;
                var endDate = schedule.EndDate;

                if (days != null)
                {
                    for (DateTime dateTime = startDate; dateTime <= endDate; dateTime = dateTime.AddDays(1))
                    {
                        if (days.Select(d => d.DayOfWeek).Contains(dateTime.DayOfWeek))
                        {
                            var day = days.Single(d => d.DayOfWeek == dateTime.DayOfWeek);

                            var startTime = day.StartTime;
                            var endTime = day.EndTime;

                            var programSession = new ProgramSession()
                            {
                                StartDateTime = dateTime.Date.Add(startTime.Value),
                                EndDateTime = dateTime.Date.Add(endTime.Value),
                                ProgramScheduleId = schedule.Id,
                            };

                            programSessionList.Add(programSession);
                        }
                    }
                }
            }

            _dataContext.Set<ProgramSession>().AddRange(programSessionList);

            return _programSessionRepository.Save();
        }

        public List<ProgramSession> SetTutionSessions(List<Charge> charges)
        {
            var programSessionList = new List<ProgramSession>();

            foreach (var charge in charges)
            {
                var schedule = charge.ProgramSchedule;
                var attribute = ((ScheduleAttribute)schedule.Attributes);
                var days = attribute.Days;

                var startDate = schedule.StartDate;
                var endDate = schedule.EndDate;

                if (charge.ProgramSessions != null && charge.ProgramSessions.Any())
                {
                    _dataContext.Set<ProgramSession>().RemoveRange(charge.ProgramSessions);
                }

                if (days != null)
                {
                    for (DateTime dateTime = startDate; dateTime <= endDate; dateTime = dateTime.AddDays(1))
                    {
                        if (days.Select(d => d.DayOfWeek).Contains(dateTime.DayOfWeek))
                        {
                            var day = days.Single(d => d.DayOfWeek == dateTime.DayOfWeek);

                            var startTime = day.StartTime;
                            var endTime = day.EndTime;

                            var programSession = new ProgramSession()
                            {
                                StartDateTime = dateTime.Date.Add(startTime.Value),
                                EndDateTime = dateTime.Date.Add(endTime.Value),
                                ChargeId = charge.Id,
                            };

                            programSessionList.Add(programSession);
                        }
                    }
                }
            }
            return programSessionList;
        }

        public OperationStatus CreateTutionSessions(List<ProgramSchedule> schedules, List<Charge> charges, List<ProgramSession> updatedSession = null)
        {
            var result = new OperationStatus();
            var programSessionList = new List<ProgramSession>();
            var deleteResult = new OperationStatus();
            var hasOrder = false;

            if (updatedSession.Any())
            {
                foreach (var item in updatedSession)
                {
                    var programsession = updatedSession.Where(o => o.Id == item.Id);

                    if (programsession != null)
                    {
                        _dataContext.Entry(item).CurrentValues.SetValues(programsession);
                    }
                }
            }

            if (charges.Any())
            {
                programSessionList.AddRange(SetTutionSessions(charges));
            }

            if (schedules.Any())
            {
                var program = _programBusiness.Get(schedules.FirstOrDefault().ProgramId);

                if (_orderSessionBusiness.HasOrder(program))
                {
                    hasOrder = true;
                }
            }

            foreach (var schedule in schedules)
            {
                if (!hasOrder)
                {
                    deleteResult = Delete(schedule);
                }
                else
                {
                    deleteResult = DeleteLogicaly(schedule);
                    schedule.OrderItems.Where(o => o.ItemStatus == OrderItemStatusCategories.initiated).ToList().ForEach(i => i.ItemStatus = OrderItemStatusCategories.deleted);
                }

                var attribute = ((ScheduleAttribute)schedule.Attributes);
                var days = attribute.Days;

                var startDate = schedule.StartDate;
                var endDate = schedule.EndDate;

                foreach (var charge in schedule.Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && !c.IsDeleted))
                {
                    if (charge.ProgramSessions != null && charge.ProgramSessions.Any())
                    {
                        charge.ProgramSessions.ToList().ForEach(p => p.IsDeleted = true);
                    }

                    if (days != null)
                    {
                        for (DateTime dateTime = startDate; dateTime <= endDate; dateTime = dateTime.AddDays(1))
                        {
                            if (days.Select(d => d.DayOfWeek).Contains(dateTime.DayOfWeek))
                            {
                                var day = days.Single(d => d.DayOfWeek == dateTime.DayOfWeek);

                                var startTime = day.StartTime;
                                var endTime = day.EndTime;

                                var programSession = new ProgramSession()
                                {
                                    StartDateTime = dateTime.Date.Add(startTime.Value),
                                    EndDateTime = dateTime.Date.Add(endTime.Value),
                                    ChargeId = charge.Id,
                                };

                                programSessionList.Add(programSession);
                            }
                        }
                    }
                }
            }

            _dataContext.Set<ProgramSession>().AddRange(programSessionList);

            result.Status = _dataContext.SaveChanges() > 0;

            return result;
        }

        public IQueryable<ProgramSession> GetList()
        {
            return _programSessionRepository.GetList(p => !p.IsDeleted);
        }

        public List<ProgramSession> GetList(Program program)
        {
            var result = new List<ProgramSession>();

            program.ProgramSchedules.Where(s => !s.IsDeleted).OrderBy(s => s.ScheduleMode).ToList().ForEach(s => result.AddRange(GetList(s)));

            return result;
        }

        public List<ProgramSession> GetProgramSessions(ProgramSchedule programSchedule)
        {
            var result = new List<ProgramSession>();
            result = _dataContext.Set<ProgramSession>().Where(i => i.ProgramScheduleId == programSchedule.Id).ToList();

            return result;
        }

        public List<ProgramSession> GetProgramSessions(List<Charge> charges)
        {
            var result = new List<ProgramSession>();

            result.AddRange(charges.SelectMany(c => c.ProgramSessions.Where(p => !p.IsDeleted)));

            return result;
        }

        public List<ProgramSession> GetList(ProgramSchedule programSchedule, DateTime desiredStartDate, List<DayOfWeek> days)
        {
            var result = new List<ProgramSession>();

            result = GetList(programSchedule).Where(p => !p.IsDeleted && p.StartDateTime >= desiredStartDate && days.Contains(p.StartDateTime.DayOfWeek)).ToList();

            return result;
        }

        public List<ProgramSession> GetList(ProgramSchedule programSchedule)
        {
            var result = programSchedule.ProgramSessions.Where(p => !p.IsDeleted).ToList();

            return result;
        }

        public List<ProgramSession> GetList(List<ProgramSchedule> programSchedules)
        {
            var programScheduleIds = programSchedules.Where(p => !p.IsDeleted).Select(p => p.Id).ToList();

            var result = _programSessionRepository.GetList(p => !p.IsDeleted && programScheduleIds.Contains(p.ProgramScheduleId.Value)).ToList();

            return result;
        }

        public List<ProgramSession> GetList(DateTime date)
        {
            var result = new List<ProgramSession>();

            var beginOfDate = date.Date;
            var endOfDate = date.Date.AddDays(1).AddMinutes(-1);

            result = GetList().Where(p => p.ProgramSchedule.Program.LastCreatedPage >= ProgramPageStep.Step4 && p.StartDateTime >= beginOfDate && p.StartDateTime <= endOfDate).ToList();

            return result;
        }

        public List<ProgramSession> GetList(Charge charge)
        {
            var result = new List<ProgramSession>();

            result = GetList().Where(p => p.ChargeId == charge.Id).ToList();

            return result;
        }

        public OperationStatus Delete(Program program)
        {
            var result = new OperationStatus();

            var programSessionParts = _dataContext.Set<ProgramSchedulePart>().Where(p => p.ProgramId == program.Id);

            _dataContext.Set<ProgramSchedulePart>().RemoveRange(programSessionParts);

            return _programSessionRepository.Save();
        }

        public OperationStatus Delete(ProgramSchedule schedule)
        {
            var result = new OperationStatus();

            var programSessions = _dataContext.Set<ProgramSession>().Where(p => p.ProgramScheduleId == schedule.Id);

            _dataContext.Set<ProgramSession>().RemoveRange(programSessions);

            return _programSessionRepository.Save();
        }

        public OperationStatus DeleteLogicaly(ProgramSchedule schedule)
        {
            var result = new OperationStatus();

            var programSessions = _dataContext.Set<ProgramSession>().Where(p => p.ProgramScheduleId == schedule.Id);

            programSessions.ToList().ForEach(s => s.IsDeleted = true);

            return _programSessionRepository.Save();
        }

        public OperationStatus DeleteLogicaly(Program program)
        {
            var result = new OperationStatus();

            var schedules = program.ProgramSchedules.Where(s => !s.IsDeleted);

            var scheduleIds = schedules.Where(s => !s.IsDeleted).Select(s => s.Id).ToList();

            var programSessionParts = _dataContext.Set<ProgramSchedulePart>().Where(p => p.ProgramId == program.Id);

            programSessionParts.ToList().ForEach(p => p.IsDeleted = true);

            schedules.SelectMany(p => p.ProgramSessions).ToList().ForEach(f => f.IsDeleted = true);

            return _programSessionRepository.Save();
        }

        public ProgramSession GetSameDayProgramSession(int programSessionId)
        {
            var programSession = Get(programSessionId);

            return GetSameDayProgramSession(programSession);
        }

        public ProgramSession GetSameDayProgramSession(ProgramSession programSession)
        {
            var program = programSession.ProgramSchedule.Program;

            return program.ProgramSchedules.Where(s => !s.IsDeleted).SelectMany(s => s.ProgramSessions).ToList().FirstOrDefault(s => !s.IsDeleted && s.StartDateTime.Date == programSession.StartDateTime.Date && s.Id != programSession.Id);
        }

        public void UpdateProgramSessionsTime(Program program)
        {
            foreach (var schedule in program.ProgramSchedules.Where(s => !s.IsDeleted && s.ScheduleMode != TimeOfClassFormation.Both))
            {
                var programSessions = schedule.ProgramSessions;
                var beforeAfterCareAttribute = _subscriptionBusiness.GetBeforAfterCareAtribute(schedule);
                var days = beforeAfterCareAttribute.Days;

                if (programSessions != null)
                {
                    foreach (var day in days)
                    {
                        var startTime = day.StartTime;
                        var endTime = day.EndTime;

                        programSessions.Where(p => p.StartDateTime.DayOfWeek == day.DayOfWeek).ToList().ForEach(s =>
                        {
                            if (startTime != null)
                            {
                                s.StartDateTime = s.StartDateTime.Date + startTime.Value;
                            }
                        });

                        programSessions.Where(p => p.StartDateTime.DayOfWeek == day.DayOfWeek).ToList().ForEach(s =>
                        {
                            if (endTime != null)
                            {
                                s.EndDateTime = s.EndDateTime.Date + endTime.Value;
                            }
                        });
                    }
                }
            }

            _programSessionRepository.Save();
        }

        public OperationStatus UpdateSessionWithStartEndDateProgram(Program program, List<ProgramSchedulePart> deletedSchedulePartsByEndDate, List<ProgramSchedulePart> deletedSchedulePartsByStartDate, bool isProgramStartDateChanged, bool isProgramEndDateChanged)
        {
            var newProgramSessions = new List<ProgramSession>();
            var deletedProgramSessions = new List<ProgramSession>();

            var startDate = program.ProgramSchedules.First(s => !s.IsDeleted).StartDate;
            var endDate = program.ProgramSchedules.First(s => !s.IsDeleted).EndDate;

            var programSchedules = program.ProgramSchedules.Where(s => !s.IsDeleted);
            var programScheduleids = programSchedules.Select(s => s.Id).ToList();
            var orderitems = _orderItemBusiness.GetList().Where(i => i.ItemStatus != OrderItemStatusCategories.deleted && programScheduleids.Contains(i.ProgramScheduleId.Value));

            var programSessionForAddToOrder = new List<ProgramSession>();

            var programParts = _dataContext.Set<ProgramSchedulePart>().Where(p => !p.IsDeleted && p.ProgramId == program.Id);

            var minStartDate = programParts.Min(p => p.StartDate);
            var maxEndDate = programParts.Max(p => p.EndDate);

            var firstProgramPart = programParts.First(p => p.StartDate == minStartDate);
            var lastProgramPart = programParts.First(p => p.EndDate == maxEndDate);

            var lastDeletedPartByStartDate = deletedSchedulePartsByStartDate.Any() ? deletedSchedulePartsByStartDate.Last() : firstProgramPart;
            var firstDeletedPartByEndDate = deletedSchedulePartsByEndDate.Any() ? deletedSchedulePartsByEndDate.First() : lastProgramPart;

            long? comboScheduleId = program.ProgramSchedules.Any(s => !s.IsDeleted && s.ScheduleMode == TimeOfClassFormation.Both) ? program.ProgramSchedules.First(s => !s.IsDeleted && s.ScheduleMode == TimeOfClassFormation.Both).Id : (long?)null;

            foreach (var schedule in program.ProgramSchedules.Where(s => !s.IsDeleted && s.ScheduleMode != TimeOfClassFormation.Both))
            {
                var beforeAfterCareAttribute = _subscriptionBusiness.GetBeforAfterCareAtribute(schedule);
                var days = beforeAfterCareAttribute.Days;

                if (isProgramStartDateChanged)
                {
                    var programSessions = schedule.ProgramSessions.Where(s => !s.IsDeleted && s.StartDateTime.Date >= firstProgramPart.StartDate.Date && s.EndDateTime.Date <= lastDeletedPartByStartDate.EndDate.Date).ToList();

                    if (programSessions.Any() && startDate <= programSessions.OrderBy(s => s.StartDateTime).First().StartDateTime)
                    {
                        newProgramSessions.AddRange(Create(days, programSessions, schedule.Id, startDate, firstProgramPart.EndDate));
                    }
                    else
                    {
                        var deletedProgramSessins = programSessions.Where(p => p.StartDateTime.Date < startDate.Date);
                        schedule.ProgramSessions.Where(p => deletedProgramSessins.Select(s => s.Id).Contains(p.Id)).ToList().ForEach(ps => ps.IsDeleted = true);
                    }
                }

                if (isProgramEndDateChanged)
                {
                    var updateOrderItems = orderitems.Where(i => i.ProgramScheduleId.Value == schedule.Id).ToList();

                    if (comboScheduleId.HasValue)
                    {
                        updateOrderItems.AddRange(orderitems.Where(i => i.ProgramScheduleId.Value == comboScheduleId.Value).ToList());
                    }
                    var programSessions = schedule.ProgramSessions.Where(s => !s.IsDeleted && s.StartDateTime.Date >= firstDeletedPartByEndDate.StartDate.Date && s.EndDateTime.Date <= lastProgramPart.EndDate.Date).ToList();

                    if (programSessions.Any() && endDate > programSessions.OrderBy(s => s.EndDateTime).Last().EndDateTime)
                    {
                        var sessions = Create(days, programSessions, schedule.Id, lastProgramPart.StartDate, endDate);
                        newProgramSessions.AddRange(sessions);
                        programSessionForAddToOrder.AddRange(sessions);
                    }
                    else
                    {
                        var deletedProgramSessionsSchedule = programSessions.Where(p => p.StartDateTime.Date > endDate.Date).ToList();

                        UpdateOrderItemSessions(updateOrderItems, deletedProgramSessionsSchedule, true);

                        schedule.ProgramSessions.Where(p => deletedProgramSessionsSchedule.Select(s => s.Id).Contains(p.Id)).ToList().ForEach(ps => ps.IsDeleted = true);
                        deletedProgramSessions.AddRange(deletedProgramSessionsSchedule);
                    }
                }
            }

            if (startDate.Date > firstProgramPart.EndDate.Date)
            {
                var deletedProgramParts = programParts.ToList().Where(p => p.EndDate.Date < startDate.Date);
                var remainingProgramParts = programParts.ToList().Where(p => !deletedProgramParts.Select(s => s.Id).ToList().Contains(p.Id));

                deletedProgramParts.ToList().ForEach(p => p.IsDeleted = true);
                firstProgramPart = remainingProgramParts.Any() ? remainingProgramParts.First() : firstProgramPart;
            }

            if (endDate.Date < lastProgramPart.StartDate.Date)
            {
                var deletedProgramParts = programParts.ToList().Where(p => p.StartDate.Date > endDate.Date);
                var remainingProgramParts = programParts.ToList().Where(p => !deletedProgramParts.Select(s => s.Id).ToList().Contains(p.Id));

                deletedProgramParts.ToList().ForEach(p => p.IsDeleted = true);
                lastProgramPart = remainingProgramParts.Any() ? remainingProgramParts.Last() : lastProgramPart;
            }

            firstProgramPart.StartDate = startDate;
            lastProgramPart.EndDate = endDate;

            _dataContext.Set<ProgramSession>().AddRange(newProgramSessions);
            var result = _programSessionRepository.Save();

            if (result.Status && newProgramSessions.Any())
            {
                UpdateOrderItemSessions(orderitems.ToList(), programSessionForAddToOrder, false);

                result = _programSessionRepository.Save();
            }

            return result;
        }

        private void UpdateOrderItemSessions(List<OrderItem> orderitems, List<ProgramSession> programSessions, bool isDeleteSessions)
        {
            var orderSessions = new List<OrderSession>();

            if (orderitems.Any())
            {
                foreach (var item in orderitems)
                {
                    var orderItemDays = item.Attributes.WeekDays;
                    if (isDeleteSessions)
                    {
                        var deletedOrderSessions = item.OrderSessions.Where(s => s.ProgramSessionId != null && programSessions.Select(p => p.Id).ToList().Contains(s.ProgramSessionId.Value));
                        _dataContext.Set<OrderSession>().RemoveRange(deletedOrderSessions);
                    }
                    else
                    {
                        var programSessionDate = programSessions.Select(s => s.StartDateTime).ToList();
                        List<ProgramSession> newProgramSessionsForOrders;

                        if (item.ProgramSchedule.ScheduleMode != TimeOfClassFormation.Both)
                        {
                            newProgramSessionsForOrders = GetList().Where(p => !p.IsDeleted && programSessionDate.Contains(p.StartDateTime) && p.ProgramScheduleId == item.ProgramScheduleId.Value).ToList();
                        }
                        else
                        {
                            var programScheduleIds = item.ProgramSchedule.Program.ProgramSchedules.Where(p => !p.IsDeleted && p.ScheduleMode != TimeOfClassFormation.Both).Select(s => s.Id);
                            newProgramSessionsForOrders = GetList().Where(p => !p.IsDeleted && programSessionDate.Contains(p.StartDateTime) && programScheduleIds.ToList().Contains(p.ProgramScheduleId.Value)).ToList();
                        }

                        if (item.ItemStatus != OrderItemStatusCategories.changed && item.ItemStatusReason != OrderItemStatusReasons.canceled)
                        {
                            foreach (var programSession in newProgramSessionsForOrders)
                            {
                                if (orderItemDays.Contains(programSession.StartDateTime.DayOfWeek))
                                {
                                    var orderSession = new OrderSession()
                                    {
                                        ProgramSessionId = programSession.Id,
                                        OrderItemId = item.Id,
                                        MetaData = new MetaData()
                                        {
                                            DateCreated = DateTime.UtcNow,
                                            DateUpdated = DateTime.UtcNow,
                                        }
                                    };

                                    orderSessions.Add(orderSession);
                                }
                            }
                        }

                        _dataContext.Set<OrderSession>().AddRange(orderSessions);
                    }
                }
            }
        }

        private List<ProgramSession> Create(List<ProgramScheduleDay> days, List<ProgramSession> programSessions, long scheduleId, DateTime startDateTime, DateTime ConditionaDateTime)
        {
            var newProgramSessionList = new List<ProgramSession>();

            if (days != null)
            {
                for (DateTime dateTime = startDateTime; dateTime <= ConditionaDateTime; dateTime = dateTime.AddDays(1))
                {
                    if (days.Select(d => d.DayOfWeek).Contains(dateTime.DayOfWeek))
                    {
                        var day = days.Single(d => d.DayOfWeek == dateTime.DayOfWeek);

                        var startTime = day.StartTime;
                        var endTime = day.EndTime;

                        var programSession = new ProgramSession()
                        {
                            StartDateTime = dateTime.Date.Add(startTime.Value),
                            EndDateTime = dateTime.Date.Add(endTime.Value),
                            ProgramScheduleId = scheduleId,
                        };

                        if (programSessions.Any(p => p.StartDateTime == programSession.StartDateTime && p.EndDateTime == programSession.EndDateTime))
                        {
                        }
                        else
                        {
                            newProgramSessionList.Add(programSession);
                        }
                    }
                }
            }

            return newProgramSessionList;
        }

        #region Capacity

        public int GetNumberOfTakenPlaces(ProgramSession programSession)
        {
            var season = programSession.ProgramSchedule.Program.Season;
            var result = 0;
            var programType = programSession.ProgramSchedule.Program.TypeCategory;

            if (programType == ProgramTypeCategory.BeforeAfterCare || programType == ProgramTypeCategory.Subscription)
            {
                result = programSession.OrderSessions.Count(s => s.OrderItem.ItemStatus != OrderItemStatusCategories.deleted &&
                          s.OrderItem.ItemStatus != OrderItemStatusCategories.notInitiated && s.OrderItem.ItemStatus != OrderItemStatusCategories.initiated
                          && (s.OrderItem.Order.IsLive && season.Status == SeasonStatus.Live) || (!s.OrderItem.Order.IsLive && season.Status != SeasonStatus.Live));
            }
            else
            {
                result = programSession.OrderSessions.Count(s => s.OrderItem.ItemStatus == OrderItemStatusCategories.completed
                          && (s.OrderItem.Order.IsLive && season.Status == SeasonStatus.Live) || (!s.OrderItem.Order.IsLive && season.Status != SeasonStatus.Live));
            }

            return result;
        }

        public int GetLeftSpots(ProgramSession programSession, int capacity)
        {
            capacity = capacity == 0 ? int.MaxValue : capacity;

            var result = capacity - GetNumberOfTakenPlaces(programSession);

            return result;
        }

        public bool IsFull(ProgramSession programSession, int capacity)
        {
            var result = GetLeftSpots(programSession, capacity) < 1;
            return result;
        }

        public bool IsFull(ProgramSession programSession)
        {
            var capacity = 0;
            var result = false;

            var programSchedule = programSession.ProgramSchedule;
            capacity = _programBusiness.GetScheduleCapacity(programSchedule);

            if (capacity != 0)
            {
                result = IsFull(programSession, capacity);
            }

            return result;
        }

        public void ApplyHolidays(Program program, List<ProgramSession> programSessions)
        {
            List<string> holidayDates = program.Club.Setting.ListOfHolidayDates;
            List<string> holidayAMDates = program.Club.Setting.ListOfHolidayAMDates;
            List<string> holidayPMDates = program.Club.Setting.ListOfHolidayPMDates;


            if (holidayDates != null && holidayDates.Any())
            {
                programSessions.RemoveAll(c => holidayDates.Where(d => !string.IsNullOrWhiteSpace(d)).Select(d => DateTime.Parse(d).Date).ToList().Contains(c.StartDateTime.Date));
            }

            if (holidayAMDates != null && holidayAMDates.Any())
            {
                programSessions.RemoveAll(c => holidayAMDates.Where(d => !string.IsNullOrWhiteSpace(d)).Select(d => DateTime.Parse(d)).ToList().Contains(c.StartDateTime.Date) && DateTimeHelper.IsTimeAm(c.StartDateTime.TimeOfDay));
            }

            if (holidayPMDates != null && holidayPMDates.Any())
            {
                programSessions.RemoveAll(c => holidayPMDates.Where(d => !string.IsNullOrWhiteSpace(d)).Select(d => DateTime.Parse(d)).ToList().Contains(c.StartDateTime.Date) && !DateTimeHelper.IsTimeAm(c.StartDateTime.TimeOfDay));
            }
        }

        #endregion

        #region WeeklyAgenda
        public OperationStatus EditAgenda(int sessionId, string text)
        {
            var result = new OperationStatus();

            var session = Get(sessionId);

            session.WeeklyAgenda = text;

            return _programSessionRepository.Save();

        }

        public OperationStatus StopSendAgenda(int sessionId)
        {
            var result = new OperationStatus();

            var session = Get(sessionId);

            if (IsStatusChangeAllowed(session))
            {
                session.Status = ProgramSessionAgendaSendStatus.Stopped;

                result = _programSessionRepository.Save();
            }

            return result;

        }

        public OperationStatus ScheduleSendAgenda(int sessionId)
        {
            var result = new OperationStatus();

            var session = Get(sessionId);

            if (IsStatusChangeAllowed(session))
            {
                session.Status = null;

                result = _programSessionRepository.Save();
            }

            return result;

        }

        public OperationStatus StopSendAgenda(DateTime date, int clubId)
        {
            var result = new OperationStatus();

            var sessions = GetList(date).Where(c => c.ProgramSchedule.Program.ClubId == clubId);

            foreach (var session in sessions)
            {
                if (IsStatusChangeAllowed(session))
                {
                    session.Status = ProgramSessionAgendaSendStatus.Stopped;
                }
            }

            result = _programSessionRepository.Save();

            return result;
        }

        public OperationStatus ScheduleSendAgenda(DateTime date, int clubId)
        {
            var result = new OperationStatus();

            var sessions = GetList(date).Where(c => c.ProgramSchedule.Program.ClubId == clubId);

            foreach (var session in sessions)
            {
                if (IsStatusChangeAllowed(session))
                {
                    session.Status = null;
                }
            }

            result = _programSessionRepository.Save();

            return result;
        }

        public DateTime GetAgendaDeliverDate(ProgramSession programSession)
        {
            DateTime? result = null;

            var club = programSession.ProgramSchedule.Program.Club;

            var agendaSettings = _clubBusiness.GetAgendaSettings(club);

            var classDate = programSession.StartDateTime.Date;

            result = agendaSettings.IsExactDay ? classDate.Add(agendaSettings.ExactTime.Value.TimeOfDay) : programSession.EndDateTime.AddHours(agendaSettings.HourAfterClassEnds);

            return result.Value;
        }

        public bool IsStatusChangeAllowed(ProgramSession programSession)
        {
            var result = true;

            if (programSession.Status.HasValue && programSession.Status == ProgramSessionAgendaSendStatus.Procecced)
            {
                return false;
            }
            else
            {
                if (IsProccessed(programSession))
                {
                    return false;
                }
            }

            return result;
        }

        public bool IsProccessed(ProgramSession programSession)
        {
            var club = programSession.ProgramSchedule.Program.Club;

            var clubDateTime = _clubBusiness.GetClubDateTime(club, DateTime.UtcNow);

            if (clubDateTime >= GetAgendaDeliverDate(programSession))
            {
                return true;
            }

            return false;
        }

        #endregion

        #region Migration
        public int MigrateFlexProgramSessions(int offset, int start)
        {
            var result = 0;

            var flexClub = _clubBusiness.Get("flexacademies");

            var flexPrograms = _programBusiness.GetList().Where(p => p.Status != ProgramStatus.Deleted && p.Club.PartnerId.HasValue && p.Club.PartnerId == flexClub.Id && p.TypeCategory == ProgramTypeCategory.Class).OrderBy(p => p.Id).Skip(start - 1).Take(offset).ToList();


            foreach (var program in flexPrograms)
            {
                createProgamSessions(program);
            }

            result = _dataContext.SaveChanges();

            return result;
        }

        private void createProgamSessions(Program program)
        {

            deleteProgramSessions(program);

            var programScheduleParts = new List<ProgramSchedulePart>();

            foreach (var programSchedule in program.ProgramSchedules.Where(s => !s.IsDeleted))
            {
                programScheduleParts.AddRange(createPart(programSchedule));

                var agendaList = getWeeklyAgendas(programSchedule);

                var catalogComments = new Dictionary<int, string>();

                if (programSchedule.Program.CatalogId.HasValue)
                {
                    var catalog = programSchedule.Program.Catalog;
                }

                for (int i = 0; i < agendaList.Count; i++)
                {
                    catalogComments.Add(i + 1, agendaList[i].AgendaWeek);
                }

                int j = 1;
                foreach (var item in programSchedule.Sessions.Where(s => !s.IsDeleted))
                {
                    var session = new ProgramSession();

                    session.StartDateTime = item.Start;
                    session.EndDateTime = item.End;
                    session.WeeklyAgenda = catalogComments.ContainsKey(j) ? catalogComments[j] : string.Empty;

                    programSchedule.ProgramSessions.Add(session);

                    j++;
                }
            }

            _dataContext.Set<ProgramSchedulePart>().AddRange(programScheduleParts);
        }

        private void deleteProgramSessions(Program program)
        {

            var programSessionParts = _dataContext.Set<ProgramSchedulePart>().Where(p => p.ProgramId == program.Id);

            _dataContext.Set<ProgramSchedulePart>().RemoveRange(programSessionParts);
        }

        public List<ProgramSchedulePart> createPart(ProgramSchedule programSchedule)
        {
            var result = new List<ProgramSchedulePart>();

            var startDate = programSchedule.StartDate;
            var endDate = programSchedule.EndDate;

            var sessions = _programBusiness.GetSessions(programSchedule);

            var programSchedulePart = new ProgramSchedulePart()
            {
                EndDate = endDate,
                StartDate = startDate,
                ProgramId = programSchedule.ProgramId,
                //Sessions = new List<ProgramSession>(),
            };

            result.Add(programSchedulePart);

            return result;

        }

        private List<CatalogPriceOptionAgendaItem> getWeeklyAgendas(ProgramSchedule programSchedule)
        {
            var program = programSchedule.Program;
            var catalog = program.Catalog;
            var classSessions = _programBusiness.GetSessions(programSchedule);

            var priceOptions = catalog.PriceOptions;

            var firstProgramSession = classSessions.First();
            var duration = firstProgramSession.End - firstProgramSession.Start;
            var classDurationsMinutes = duration.Hours * 60 + duration.Minutes;
            var numberOfClassSessions = classSessions.Count();

            var correctPriceOption = getCorrectPriceOption(priceOptions.ToList(), classDurationsMinutes, numberOfClassSessions);

            if (correctPriceOption == null || (correctPriceOption != null && correctPriceOption.Attribute == null))
            {
                return new List<CatalogPriceOptionAgendaItem>();
            }

            return correctPriceOption.Attribute.AgendaList.ToList();

        }

        private CatalogItemPriceOption getCorrectPriceOption(List<CatalogItemPriceOption> priceOptions, int classSessionsMinutes, int numberOfClassSessioins)
        {
            var allDurations = new List<string> { "30", "45", "50", "60", "75", "90", "120" };
            var allWeeks = new List<int> { 5, 6, 7, 8, 9, 10 };

            var priceOptionDurations = priceOptions.Where(p => !string.IsNullOrWhiteSpace(p.DurationTitle)).Select(p => p.DurationTitle.Substring(0, 2)).Where(p => allDurations.Contains(p)).ToList();

            List<string> itemsForRemove = new List<string>();

            foreach (var item in priceOptionDurations)
            {
                try
                {
                    int.Parse(item);
                }
                catch (Exception)
                {
                    itemsForRemove.Add(item);
                }
            }

            priceOptionDurations.RemoveAll(i => itemsForRemove.Contains(i));

            var correctDuration = getClosestNumber(priceOptionDurations.Select(a => int.Parse(a)).ToList(), classSessionsMinutes);

            var priceOptionsWithDuration = priceOptions.Where(a => a.Attribute != null && a.Attribute.AgendaList != null).Where(p => !string.IsNullOrWhiteSpace(p.DurationTitle) && p.DurationTitle.StartsWith(correctDuration.ToString()));

            List<int> weekNumbers = priceOptionsWithDuration.Where(d => !string.IsNullOrWhiteSpace(d.DurationTitle)).Select(p => p.Attribute.AgendaList.Count).Where(p => allWeeks.Contains(p)).ToList();

            var correctWeekNumbers = getClosestNumber(weekNumbers, numberOfClassSessioins);

            var result = priceOptionsWithDuration.Where(a => a.Attribute != null && a.Attribute.AgendaList != null).SingleOrDefault(p => p.Attribute.AgendaList.Count() == correctWeekNumbers);

            return result;
        }


        private int getClosestNumber(List<int> numbers, int number)
        {
            var result = 0;
            int minDistance = 0;

            if (!numbers.Any())
            {
                return 0;
            }

            if (numbers.Any(n => n >= number))
            {
                minDistance = numbers.Where(n => n >= number).Min(n => Math.Abs(number - n));

                result = numbers.Where(n => n >= number).FirstOrDefault(n => Math.Abs(number - n) == minDistance);
            }

            if (result == 0)
            {
                minDistance = numbers.Min(n => Math.Abs(number - n));

                result = numbers.FirstOrDefault(n => Math.Abs(number - n) == minDistance);
            }

            return result;
        }

        public int MigrateFlexOrderSessions(int offset, int start)
        {
            var result = 0;

            var flexClub = _clubBusiness.Get("flexacademies");

            var flexOrders = _orderItemBusiness.GetList().Where(o => o.ItemStatus == OrderItemStatusCategories.completed && o.SeasonId.HasValue && o.Season.Club.PartnerId == flexClub.Id).OrderBy(p => p.Id).Skip(start - 1).Take(offset).ToList();

            foreach (var order in flexOrders)
            {
                createOrderSessions(order);
            }

            result = _dataContext.SaveChanges();

            return result;
        }

        private void createOrderSessions(OrderItem orderItem)
        {
            var orderSessions = new List<OrderSession>();

            var programSessions = GetList(orderItem.ProgramSchedule.Program).Where(p => !p.IsDeleted);

            orderSessions = programSessions.Select(p =>
                new OrderSession
                {
                    ProgramSessionId = p.Id,
                    OrderItemId = orderItem.Id
                })
                .ToList();

            _dataContext.Set<OrderSession>().AddRange(orderSessions);
        }

        public IQueryable<ProgramSession> GetList(Expression<Func<ProgramSession, bool>> predicate)
        {
            return _programSessionRepository.GetList(predicate);
        }

        #endregion

        public ClassSessionsDateTime GetFirstSession(OrderItem orderItem, bool generateSession = true)
        {
            var club = orderItem.Order.Club;

            var sessions = GetOrderItemSessions(orderItem);

            if (!sessions.Any())
            {
                if (generateSession)
                {
                    //when order is full cancel and sessions is empty
                    sessions = GetSessionsWhenNoAnySessions(orderItem);
                }
                else
                {
                    return null;
                }
            }

            //Map schedule session to class session
            var programSessionsDateTime = sessions.Select(MapClassSessionsDateTime).ToList();

            //Remove holiday dates from sessions
            var sessionsWithoutHoliday = _programBusiness.ApplyHolidaysToClassDates(club, programSessionsDateTime, TimeOfClassFormation.None);

            if (!sessionsWithoutHoliday.Any())
            {
                return programSessionsDateTime.OrderBy(p => p.StartTime).First();
            }

            return sessionsWithoutHoliday.OrderBy(p => p.StartTime).First();
        }

        public ClassSessionsDateTime GetLastSession(OrderItem orderItem, bool generateSession = true)
        {
            var club = orderItem.Order.Club;

            var sessions = GetOrderItemSessions(orderItem);

            if (!sessions.Any())
            {
                if (generateSession)
                {
                    //when order is full cancel and sessions is empty
                    sessions = GetSessionsWhenNoAnySessions(orderItem);
                }
                else
                {
                    return null;
                }
            }

            //Map schedule session to class session
            var programSessionsDateTime = sessions.Select(MapClassSessionsDateTime).ToList();

            //Remove holiday dates from sessions
            var sessionsWithoutHoliday = _programBusiness.ApplyHolidaysToClassDates(club, programSessionsDateTime, TimeOfClassFormation.None);

            if (!sessionsWithoutHoliday.Any())
            {
                return programSessionsDateTime.OrderBy(p => p.StartTime).Last();
            }

            return sessionsWithoutHoliday.OrderBy(p => p.StartTime).Last();
        }

        private List<ScheduleSession> GetOrderItemSessions(OrderItem orderItem)
        {
            var result = new List<ScheduleSession>();

            if (orderItem.ProgramSchedule != null)
            {
                var program = orderItem.ProgramSchedule.Program;
                var club = program?.Club;

                switch (program?.TypeCategory)
                {
                    case ProgramTypeCategory.Class:

                        result = GetClassSessions(orderItem);
                        break;
                    case ProgramTypeCategory.SeminarTour:
                    case ProgramTypeCategory.ChessTournament:

                        result = GetSeminarTourOrChessTournamentSessions(orderItem);
                        break;
                    case ProgramTypeCategory.Calendar:
                        break;
                    case ProgramTypeCategory.Subscription:

                        result = GetSubscriptionSessions(orderItem);
                        break;
                    case ProgramTypeCategory.Camp:

                        result = GetCampSessions(orderItem);
                        break;
                    case ProgramTypeCategory.BeforeAfterCare:

                        result = GetBeforeAfterCareSessions(orderItem);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return result;
        }

        private List<ScheduleSession> GetClassSessions(OrderItem orderItem)
        {
            var result = new List<ScheduleSession>();
            var program = orderItem.ProgramSchedule.Program;
            var programSchedules = program.ProgramSchedules.Where(s => !s.IsDeleted);
            var programScheduleMode = _programBusiness.GetClassScheduleTime(program);

            if (_programBusiness.GetClassDates(programSchedules.First(), programScheduleMode) != null)
            {
                result.AddRange(_programBusiness.GetClassDates(programSchedules.First(), programScheduleMode)
                       .Where(p => p.StartTime >= orderItem.Order.CompleteDate)
                                    .Select(p => new ScheduleSession()
                                    {
                                        Id = p.Id,
                                        Start = p.StartTime,
                                        End = p.EndTime,
                                    }));
            }

            return result;
        }

        private List<ScheduleSession> GetSeminarTourOrChessTournamentSessions(OrderItem orderItem)
        {
            var result = new List<ScheduleSession>();
            var program = orderItem.ProgramSchedule.Program;
            var programSchedules = program.ProgramSchedules.Where(s => !s.IsDeleted);

            result = programSchedules.Select(p => new ScheduleSession()
            {
                Start = p.StartDate,
                End = p.EndDate,
            }).ToList();

            if (result.Any())
            {
                if (result.Any(s => orderItem.Order.CompleteDate >= s.Start))
                {
                    var resultSession = new ScheduleSession
                    {
                        Start = orderItem.Order.CompleteDate,
                        End = orderItem.Order.CompleteDate,
                    };

                    result = new List<ScheduleSession> { resultSession };
                }

            }

            return result;
        }

        private List<ScheduleSession> GetSubscriptionSessions(OrderItem orderItem)
        {
            return _orderSessionBusiness.GetOrderItemSessions(orderItem.Id).Select(o => o.ProgramSession)
                       .Select(p => new ScheduleSession()
                       {
                           Id = p.Id,
                           Start = p.StartDateTime,
                           End = p.EndDateTime,
                       }).ToList();
        }

        private List<ScheduleSession> GetCampSessions(OrderItem orderItem)
        {
            return _orderSessionBusiness.GetOrderItemSessions(orderItem.Id)
                .Select(o => o.ProgramSession).Where(p => p.StartDateTime >= orderItem.Order.CompleteDate)
                        .Select(p => new ScheduleSession()
                        {
                            Id = p.Id,
                            Start = p.StartDateTime,
                            End = p.EndDateTime,
                        }).ToList();
        }

        private List<ScheduleSession> GetBeforeAfterCareSessions(OrderItem orderItem)
        {
            var result = new List<ScheduleSession>();
            var program = orderItem.ProgramSchedule.Program;
            var programSchedules = program.ProgramSchedules.Where(s => !s.IsDeleted);

            if (orderItem.Mode == OrderItemMode.PunchCard)
            {
                foreach (var schedule in programSchedules)
                {
                    result.AddRange(GetList(schedule)
                                   .OrderBy(p => p.StartDateTime).Select(p => new ScheduleSession()
                                   {
                                       Id = p.Id,
                                       Start = p.StartDateTime,
                                       End = p.EndDateTime,
                                   }).ToList());
                }
            }
            else // for dropIn and Fully Pay
            {
                result = _orderSessionBusiness.GetOrderItemSessions(orderItem.Id).Select(o => o.ProgramSession).Select(p => new ScheduleSession()
                {
                    Id = p.Id,
                    Start = p.StartDateTime,
                    End = p.EndDateTime,
                }).ToList();
            }

            return result;
        }

        private List<ScheduleSession> GetSessionsWhenNoAnySessions(OrderItem orderItem)
        {
            var sessions = new List<ScheduleSession>();

            if (orderItem.PaidAmount > 0)
            {
                //select last transaction date
                sessions = GenerateSessionsFromLastSuccessTransaction(orderItem);
            }
            else
            {
                //select last cancel history date
                sessions = GenerateSessionsFromLastCancelHistory(orderItem);
            }

            //If wasnt't no any session
            if (!sessions.Any()) //first program session
            {
                sessions = GenerateSessionsFromProgramSession(orderItem);
            }

            if (!sessions.Any()) //For transfer Out we not should show in calendar
            {
                sessions = GenerateSessionsFromDateNow(orderItem);
            }

            return sessions;
        }

        private List<ScheduleSession> GenerateSessionsFromLastSuccessTransaction(OrderItem orderItem)
        {
            var succeedTransaction = _transactionActivityBusiness.GetAllsucceedTransactionByOrderItemId(orderItem.Id).ToList();
            var lastSucceedTransaction = succeedTransaction.Any() ? succeedTransaction.Last() : null;
            var result = new List<ScheduleSession>();

            if (lastSucceedTransaction != null)
            {
                var item = new ScheduleSession()
                {
                    Start = DateTimeHelper.ConvertUtcDateTimeToLocal(lastSucceedTransaction.TransactionDate, orderItem.Order.Club.TimeZone),
                    End = DateTimeHelper.ConvertUtcDateTimeToLocal(lastSucceedTransaction.TransactionDate, orderItem.Order.Club.TimeZone)
                };

                result.Add(item);
            }

            return result;
        }

        private List<ScheduleSession> GenerateSessionsFromLastCancelHistory(OrderItem orderItem)
        {
            var cancelHistory = _orderHistoryBusiness.GetCancelHistory(orderItem.Order_Id, orderItem.Id).ToList();
            var lastCancelHistory = cancelHistory.Any() ? cancelHistory.ToList().Last() : null;
            var result = new List<ScheduleSession>();

            if (lastCancelHistory != null)
            {
                var item = new ScheduleSession()
                {
                    Start = DateTimeHelper.ConvertUtcDateTimeToLocal(lastCancelHistory.ActionDate, orderItem.Order.Club.TimeZone),
                    End = DateTimeHelper.ConvertUtcDateTimeToLocal(lastCancelHistory.ActionDate, orderItem.Order.Club.TimeZone)
                };

                result.Add(item);
            }

            return result;
        }

        private List<ScheduleSession> GenerateSessionsFromProgramSession(OrderItem orderItem)
        {
            var program = orderItem.ProgramSchedule.Program;
            var programSchedules = program.ProgramSchedules.Where(s => !s.IsDeleted);
            var result = new List<ScheduleSession>();

            foreach (var schedule in programSchedules)
            {
                result.AddRange(GetList(schedule)
                               .OrderBy(p => p.StartDateTime).Select(p => new ScheduleSession()
                               {
                                   Id = p.Id,
                                   Start = p.StartDateTime,
                                   End = p.EndDateTime,
                               }).ToList());
            }

            return result;
        }

        private List<ScheduleSession> GenerateSessionsFromDateNow(OrderItem orderItem)
        {
            var result = new List<ScheduleSession>();

            var item = new ScheduleSession()
            {
                Start = DateTimeHelper.ConvertUtcDateTimeToLocal(DateTime.UtcNow, orderItem.Order.Club.TimeZone),
                End = DateTimeHelper.ConvertUtcDateTimeToLocal(DateTime.UtcNow, orderItem.Order.Club.TimeZone)
            };

            result.Add(item);

            return result;
        }

        private static ClassSessionsDateTime MapClassSessionsDateTime(ScheduleSession scheduleSession)
        {
            return new ClassSessionsDateTime
            {
                Id = scheduleSession.Id,
                SessionDate = scheduleSession.Start,
                TimeChanges = false,
                StartTime = scheduleSession.Start,
                EndTime = scheduleSession.End
            };
        }
    }
}
