﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CreateEditLocationViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [Required(ErrorMessage="Address is required.")]
        public string Address { get; set; }
    }
}