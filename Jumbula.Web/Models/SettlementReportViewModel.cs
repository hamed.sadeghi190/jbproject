﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class SRManagementViewModel
    {
        public SRManagementViewModel()
        {
            StartDateGenerate = DateTime.Now;
            StartDateGenerateMinValue=DateTime.Now;
            EndDateGenerate =DateTime.Now;
            EndDateGenerateMaxValue =DateTime.Now;
            StartDateReportView =DateTime.Now;
            EndDateReportView =DateTime.Now;
        }

        [Required(ErrorMessage = "You must enter start date to Generate.")]
        public DateTime StartDateGenerate { get; set; }

        public DateTime StartDateGenerateMinValue { get; set;}

        public bool IsEnableStartDateGenerate { get; set; }
        
        [Required(ErrorMessage = "You must enter end date to Generate.")]
        public DateTime EndDateGenerate { get; set; }

        public DateTime EndDateGenerateMaxValue { get; set; }
        
        public DateTime StartDateReportView { get; set; }

        public DateTime EndDateReportView { get; set; }

        public string LastSettlementPeriod { get; set; }

        [Required(ErrorMessage = "You Must Enter Club Domain.")]
        public string ClubDomain { get; set; }

        public IList<SettlementReport> SettlementReportsItems { get; set; }
    }

    public class SettlementReportItemViewModel
    {
        public int Id { get; set; }

        public decimal GrossProceeds { get; set; }

        public decimal ProcessingFee { get; set; }

        public decimal NetProceeds { get; set; }

        public string SettlementPeriod { get; set; }

        public string Status { get; set; }

    }


    public class SRAddSettlementViewModel
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string ClubDomain { get; set; }

        public decimal GrossProceeds { get; set; }

        public decimal ProcessingFee { get; set; }

        public decimal NetProceeds { get; set; }

        public bool IsEnableSaveButton { get; set; }

        public IList<OrdersOfSettlementViewModel> SRAddSettlementItemsViewModel { get; set; }
    }

    public class OrdersOfSettlementViewModel
    {
        public string Date { get; set; }

        public string Event { get; set; }

        public string User { get; set; }

        public string PaidBy { get; set; }

        public string Confirmation { get; set; }

        public decimal GrossProceeds { get; set; }

        public decimal ProcessingFee { get; set; }

        public decimal NetProceeds { get; set; }
    }
}