﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models.Email
{
    public class SendAddedToWaitlistEmailModel
    {
        public string UserEmail { get; set; }

        public string ProgramName { get; set; }

        public string ContactEmail { get; set; }

        public string HomeTitle { get; set; }
        public string ClubDomain { get; set; }
        public string SeasonTitle { get; set; }
        public string HomePageUrl { get; set; }

        public string LogoUrl { get; set; }

        public string ClubName { get; set; }

        public string Participant { get; set; }

        public string Date { get; set; }

        public string Grade { get; set; }
    }
}