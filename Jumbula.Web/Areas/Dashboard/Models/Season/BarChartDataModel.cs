﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models.Season
{
    public class BarChartDataModel
    {
        public string ProgramName { get; set; }
        public int ExtraCapacity { get; set; }
        public string SeasonName { get; set; }
        public string ProgramFullName { get; set; }
        public int WaitList { get; set; }
        public int Capacity { get; set; }
        public int OpenSpot { get; set; }
        public int MinimumCapacity  { get; set; }
        public int FillSpot { get; set; }

        public DateTime Date { get; set; }

        public bool IsFull { get; set; }

        public int PlusMember { get; set; }
    }
}