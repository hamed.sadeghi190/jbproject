﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('clubEditUserRoleController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbToast) {
        window.app.globalObjects.pageClass = 'members-edit-view';
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/Clubs", order: 1, menu: "Clubs" });

        $scope.MC.stateParams = $stateParams;
        $scope.type = $stateParams.type;

        $http.get('Dashboard/Club/EditUserRole', { params: { clubId: $stateParams.clubId } }).success(function (data, status, headers, config) {
            $scope.Model = data;  
        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.changedUser=function()
        {
           
            var setRole={};
            for (var i = 0, len =  $scope.Model.ClubUsers.length; i < len; i++) {
                if( $scope.Model.ClubUsers[i].Id == $scope.Model.SelectedUserId)
                {
                   
                    $scope.Model.SelectedRole = $scope.Model.ClubUsers[i].UserClubRole;
                        break;
                }
            }
            
        }
        $scope.save = function () {

            $http.post('Dashboard/Club/EditUserRole', { model: $scope.Model })
             .success(function (data, status, headers, config) {

                 if (data.Status == true) {
                     jbToast.success(data.Message,"");
                     $state.go('Clubs');
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }

    }])

})();