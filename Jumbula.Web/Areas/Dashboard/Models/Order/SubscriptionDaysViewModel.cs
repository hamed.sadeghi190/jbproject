﻿using Jumbula.Core.Model.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SubscriptionDaysViewModel
    {
        public List<SubscriptionProgramDaysViewModel> AllDays { get; set; }
        public DateTime LastSessionDate { get; set; }
        public DateTime Today { get; set; }
    }
}