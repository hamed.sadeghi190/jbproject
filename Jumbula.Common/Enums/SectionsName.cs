﻿

namespace Jumbula.Common.Enums
{
    public enum SectionsName
    {
        ParticipantSection = 0,
        ContactSection = 1,
        ParentGuardianSection = 2,
        Parent2GuardianSection = 3,
        EmergencyContactSection = 4,
        SchoolSection = 5,
        HealthSection = 6,
        AuthorizedPickup1 = 7,
        AuthorizedPickup2 = 8,
        AuthorizedPickup3 = 9,
        AuthorizedPickup4 = 10,
        InsuranceSection = 11,
        PhotographySection = 12,
        Miscellaneous = 13,
        AuthorizedPickup = 14,
        General = 15,
        ChargeSection = 16,
        DiscountCouponSection = 17,
        ChessTournamentSection = 18,
        MoreInfoSection = 19
    }
}
