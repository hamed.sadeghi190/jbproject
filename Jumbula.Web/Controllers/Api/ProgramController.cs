﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.WebLogic.UserService;
using SportsClub.Models.Api;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Http;
using System.Web.Http.Cors;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Controllers.Api
{
    [Authorize]//(Roles = "Instructor, Admin, Manager")]
    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "X-Custom-Header")]
    public class ProgramController : ApiController
    {
        public ProgramController()
        {
        }


        [HttpGet]
        [Route("api/Program/GetList")]
        public HttpResponseMessage GetList(int clubId)
        {
            HttpResponseMessage response = null;

            try
            {
                var programBusiness = Ioc.ProgramBusiness;
                var programs = new List<Program>();

                var user = Ioc.UserProfileBusiness.Get(User.Identity.GetCurrentUserId());
                var staffs = Ioc.ClubBusiness.GetStaffsByUserName(user.UserName).Where(c => c.Club.ClubType.EnumType != ClubTypesEnum.SchoolDistrict).ToList();

                if (!staffs.Any())
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { Programs = string.Empty, Message = "" });
                    return response;
                }


                var instructors = staffs.Where(s => s.JbUserRole.Role.Name == RoleCategory.Instructor.ToString() && s.Status == StaffStatus.Active && !s.IsFrozen);

                if (instructors.Any())
                {
                    programs = instructors.SelectMany(s => s.Programs).Where(p => p.ClubId == clubId && p.Status != ProgramStatus.Deleted && p.Status != ProgramStatus.Frozen
                      && p.LastCreatedPage >= ProgramPageStep.Step4 && !p.Season.IsArchived && p.Season.Status == SeasonStatus.Live && (p.TypeCategory == ProgramTypeCategory.Class || p.TypeCategory == ProgramTypeCategory.Subscription || p.TypeCategory == ProgramTypeCategory.Camp || p.TypeCategory == ProgramTypeCategory.BeforeAfterCare)).ToList();
                }


                var onsitePersons = staffs.Where(s => (s.JbUserRole.Role.Name == RoleCategory.OnsiteCoordinator.ToString() ||
                                    s.JbUserRole.Role.Name == RoleCategory.OnsiteSupportManager.ToString() ||
                                    s.JbUserRole.Role.Name == RoleCategory.Admin.ToString() ||
                                    s.JbUserRole.Role.Name == RoleCategory.Partner.ToString() ||
                                    s.JbUserRole.Role.Name == RoleCategory.Manager.ToString() ||
                                    s.JbUserRole.Role.Name == RoleCategory.SchoolContributor.ToString()) && s.Status == StaffStatus.Active && !s.IsFrozen);

                if (!instructors.Any() && onsitePersons.Any())
                {
                    programs = Ioc.ProgramBusiness.GetList().Where(p => p.ClubId == clubId && p.Status != ProgramStatus.Deleted && p.Status != ProgramStatus.Frozen
                               && p.LastCreatedPage >= ProgramPageStep.Step4 && !p.Season.IsArchived && p.Season.Status == SeasonStatus.Live && (p.TypeCategory == ProgramTypeCategory.Class || p.TypeCategory == ProgramTypeCategory.Subscription || p.TypeCategory == ProgramTypeCategory.Camp || p.TypeCategory == ProgramTypeCategory.BeforeAfterCare)).ToList();
                }

                if (programs == null || !programs.Any())
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { Programs = string.Empty, Message = "You do not have any classes to teach. Please contact your provider to designate you as the instructor for your classes." });
                    return response;
                }

                var result = new List<object>();
                var programsSortBySeason = new List<Program>();

                var sortedSeasonsHasYear = programs.Where(s => s.Season.Year.HasValue && s.Season.Year > 0).OrderByDescending(s => s.Season.Year).ThenBy(s => s.Season.Name).ThenByDescending(s => s.Season.MetaData.DateCreated).ToList();
                var sortedSeasonsNotHasYear = programs.Where(s => !s.Season.Year.HasValue || s.Season.Year == 0).OrderByDescending(s => s.Season.MetaData.DateCreated).ToList();
                programsSortBySeason.AddRange(sortedSeasonsHasYear);
                programsSortBySeason.AddRange(sortedSeasonsNotHasYear);

                var sortedSeasonsName = programsSortBySeason.Select(s => s.Season.Title).Distinct();

                foreach (var program in programsSortBySeason)
                {
                    var programStatus = programBusiness.GetEmProgramStatus(program, program.Season.Status == SeasonStatus.Live);

                    if (programStatus != "Canceled")
                    {
                        var programSchoolId = program.ClubId;

                        var programSessions = programBusiness.GetSessions(program)
                             .OrderBy(o => o.Start).ToList();

                        var sessionsDate = programSessions.Select(s => new
                        {
                            Id = s.Id,
                            Date = string.Format("{0} ({1}-{2})", s.Start.ToString("ddd, MMM dd, yyyy"),
                                                    s.Start.ToString("h:mm tt"),
                                                    s.End.ToString("h:mm tt"))
                        }).ToList();

                        var classDate = programSessions.Any() && programSessions != null ? string.Format("{0} - {1}", programSessions.FirstOrDefault().Start.ToString("ddd, MMM dd, yyyy"), programSessions.LastOrDefault().Start.ToString("ddd, MMM dd, yyyy")) : string.Empty;
                        var roomNumber = program.Room != null ? program.Room : string.Empty;
                        var isNullProgramSessionsMessage = programSessions.Any() && programSessions != null ? string.Empty : "The sessions for this class have not started yet.";

                        result.Add(new { Id = program.Id, ClassName = program.Name, TypeCategory = program.TypeCategory.ToString(), Sessions = sessionsDate, SeasonId = program.SeasonId, SeasonName = program.Season.Title, ClassDate = classDate, RoomNumber = roomNumber, Message = isNullProgramSessionsMessage });
                    };
                };

                response = Request.CreateResponse(HttpStatusCode.OK, new { Programs = result, SortedSeasonsName = sortedSeasonsName });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }


        [HttpGet]
        [Route("api/Program/GetSchools")]
        public HttpResponseMessage GetSchools(int userId)
        {
            HttpResponseMessage response = null;

            try
            {
                var user = Ioc.UserProfileBusiness.Get(userId);
                var staffs = Ioc.ClubBusiness.GetStaffsByUserName(user.UserName);

                if (!staffs.Any())
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { Status = false, Message = "" });
                    return response;
                }

                var schools = new List<dynamic>();
                var programs = new List<Program>();

                var instructors = staffs.Where(s => s.JbUserRole.Role.Name == RoleCategory.Instructor.ToString() && s.Status == StaffStatus.Active && !s.IsFrozen);

                if (instructors.Any())
                {
                    schools = instructors.SelectMany(s => s.Programs).Where(p => p.Status != ProgramStatus.Deleted && p.Status != ProgramStatus.Frozen
                                  && p.Season.Status == SeasonStatus.Live && (p.TypeCategory == ProgramTypeCategory.Class || p.TypeCategory == ProgramTypeCategory.Subscription || p.TypeCategory == ProgramTypeCategory.Camp || p.TypeCategory == ProgramTypeCategory.BeforeAfterCare)).DistinctBy(c => c.ClubId).Select(p =>
                                  new
                                  {
                                      ClubName = p.Club.Name,
                                      ClubDomain = p.Club.Domain,
                                      ClubId = p.ClubId,
                                      Role = RoleCategory.Instructor.ToDescription(),
                                      LogoUrl = UrlHelpers.GetClubLogoUrl(p.Club.Domain, p.Club.Logo, true),
                                      ProviderId = p.OutSourceSeason != null ? p.OutSourceSeason.Club.Id : 0,
                                      PartnerName = p.Club.PartnerClub != null ? p.Club.PartnerClub.Name : string.Empty,
                                      PartnerLogoUrl = p.Club.PartnerClub != null ? UrlHelpers.GetClubLogoUrl(p.Club.PartnerClub.Domain, p.Club.PartnerClub.Logo, true) : UrlHelpers.GetClubLogoUrl(p.Club.Domain, p.Club.Logo, true),
                                      ClubStaffId = p.OutSourceSeason != null ? p.OutSourceSeason.Club.ClubStaffs.SingleOrDefault(c => c.JbUserRole.UserId == userId && !c.IsDeleted).Id
                                                     : p.Club.ClubStaffs.SingleOrDefault(c => c.JbUserRole.UserId == userId && c.ClubId == p.ClubId && !c.IsDeleted).Id
                                  })
                                  .OrderBy(p => p.ClubName)
                                 .ToList<dynamic>();
                }

                var admins = staffs.Where(s => (s.JbUserRole.Role.Name == RoleCategory.Admin.ToString() ||
                                                s.JbUserRole.Role.Name == RoleCategory.OnsiteCoordinator.ToString() ||
                                                s.JbUserRole.Role.Name == RoleCategory.OnsiteSupportManager.ToString() ||
                                                s.JbUserRole.Role.Name == RoleCategory.Partner.ToString() ||
                                                s.JbUserRole.Role.Name == RoleCategory.Manager.ToString() ||
                                                s.JbUserRole.Role.Name == RoleCategory.SchoolContributor.ToString())
                                               && s.Status == StaffStatus.Active && !s.IsFrozen).ToList();

                if (admins.Any())
                {
                    var clubsStaffs = admins.Where(a => a.Club.ClubType.EnumType != ClubTypesEnum.SchoolDistrict && !a.IsDeleted).OrderBy(c => c.Club.Name).ToList();


                    var clubIds = clubsStaffs.Select(s => s.ClubId).ToList();
                    var clubs = Ioc.ClubBusiness.GetList().Where(c => clubIds.Contains(c.Id) && !c.IsDeleted).ToList();

                    foreach (var staff in clubsStaffs)
                    {
                        var club = clubs.SingleOrDefault(c => (c.Id == staff.ClubId && c.ClubStaffs.Any(s => s.JbUserRole.UserId == staff.JbUserRole.UserId && !s.IsDeleted)) || (c.District != null && c.Id == staff.ClubId && c.District.ClubStaffs.Any(s => s.JbUserRole.UserId == staff.JbUserRole.UserId && !s.IsDeleted)));

                        if (club != null)
                        {
                            schools.Add(new
                            {
                                ClubName = staff.Club.Name,
                                ClubDomain = staff.Club.Domain,
                                ClubId = staff.ClubId,
                                Role = staff.JbUserRole.Role.Name == RoleCategory.OnsiteCoordinator.ToString() ? "Onsite Coordinator" : staff.JbUserRole.Role.Name == RoleCategory.OnsiteSupportManager.ToString() ? "Onsite Support Manager" : staff.JbUserRole.Role.Name == RoleCategory.SchoolContributor.ToString() ? "School Contributor" : staff.JbUserRole.Role.Name,
                                LogoUrl = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo, true),
                                ProviderId = 0,
                                PartnerName = club.PartnerClub != null ? club.PartnerClub.Name : string.Empty,
                                PartnerLogoUrl = club.PartnerClub != null
                                    ? UrlHelpers.GetClubLogoUrl(club.PartnerClub.Domain, club.PartnerClub.Logo, true)
                                    : UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo, true),
                                ClubStaffId =
                                    club.ClubStaffs.SingleOrDefault(c => c.JbUserRole.UserId == staff.JbUserRole.UserId && !c.IsDeleted) != null
                                        ? club.ClubStaffs.Single(c => c.JbUserRole.UserId == staff.JbUserRole.UserId && !c.IsDeleted).Id
                                        : club.District.ClubStaffs.Single(c => c.JbUserRole.UserId == staff.JbUserRole.UserId && !c.IsDeleted).Id
                            });

                        }
                    }

                    schools = schools.Distinct().ToList();

                }

                var fullName = staffs.First().Contact != null ? staffs.First().Contact.FullName : string.Empty;
                if (schools.Any())
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { Status = true, Clubs = schools, FullName = fullName });
                    return response;
                }


                response = Request.CreateResponse(HttpStatusCode.OK, new { Status = false, Clubs = string.Empty, FullName = fullName });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpGet]
        [Route("api/Program/HasSessionToday")]
        public HttpResponseMessage HasSessionToday(long programId)
        {
            HttpResponseMessage response = null;

            try
            {
                var program = Ioc.ProgramBusiness.Get(programId);

                var clubDateTime = Ioc.ClubBusiness.GetClubDateTime(program.Club.Id, DateTime.UtcNow);

                var programSessions = Ioc.ProgramBusiness.GetSessions(program);

                foreach (var item in programSessions)
                {
                    if (item.Start.Date == clubDateTime.Date)
                    {
                        response = Request.CreateResponse(HttpStatusCode.OK, new { HasSessionToday = true, DateTime = string.Concat(item.Start.ToString("ddd, MMM dd, yyyy"), " ", item.Start.ToString("h:mm tt"), " - ", item.End.ToString("h:mm tt")), SessionId = item.Id });
                        return response;
                    }
                }

                response = Request.CreateResponse(HttpStatusCode.OK, new { HasSessionToday = false });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpGet]
        [Route("api/Program/GetStudentList")]
        public HttpResponseMessage GetStudentList(long programId)
        {
            HttpResponseMessage response = null;

            try
            {
                var programBusiness = Ioc.ProgramBusiness;

                var program = programBusiness.Get(programId);
                var programAttendances = new List<ScheduleAttendance>();

                switch (program.TypeCategory)
                {
                    case ProgramTypeCategory.Class:
                        programAttendances = programBusiness.GetProgramAttendance(programId).ToList();
                        break;

                    case ProgramTypeCategory.Camp:
                        break;

                    case ProgramTypeCategory.Subscription:
                        programAttendances = programBusiness.GetProgramSubscriptionAttendance(programId, null);
                        break;

                    default:
                        break;
                }

                var result = programAttendances.ToList()
                .Select(a =>
                    new
                    {
                        Id = a.ProfileId,
                        FullName = string.Format("{0}, {1}", StringHelper.ReturnEmptyIfNull(a.PlayerProfile.Contact.LastName), StringHelper.ReturnEmptyIfNull(a.PlayerProfile.Contact.FirstName)),
                        Gender = a.PlayerProfile.Gender.ToString(),
                        ImageUrl = GetStudentImageUrl(a.PlayerProfile.Gender),
                    }
                 ).OrderBy(a => a.FullName).ToList();

                response = Request.CreateResponse(HttpStatusCode.OK, new { Students = result });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpGet]
        [Route("api/Program/GetStudentList_V2_2_1")]
        public HttpResponseMessage GetStudentList_V2_2_1(long programId, DateTime? dateTime, int page, int pageSize)
        {
            HttpResponseMessage response = null;
            int totalCount = 0;

            try
            {
                var program = Ioc.ProgramBusiness.Get(programId);
                var programAttendances = new List<ScheduleAttendance>();

                switch (program.TypeCategory)
                {
                    case ProgramTypeCategory.Class:
                        programAttendances = Ioc.ProgramBusiness.GetProgramAttendance_V2_2_1(programId, page, pageSize, ref totalCount);
                        break;

                    case ProgramTypeCategory.Camp:
                    case ProgramTypeCategory.Subscription:
                        programAttendances = Ioc.ProgramBusiness.GetProgramSubscriptionAttendance_V2_2_1(programId, dateTime.Value, page, pageSize, ref totalCount);
                        break;

                    case ProgramTypeCategory.BeforeAfterCare:
                        programAttendances = Ioc.ProgramBusiness.GetProgramBeforeAfterAttendance_V2_2_1(programId, dateTime.Value, page, pageSize, ref totalCount);
                        break;
                }


                var result = programAttendances.ToList()
                .Select(a =>
                    new
                    {
                        Id = a.ProfileId,
                        FullName = string.Format("{0}, {1}", StringHelper.ReturnEmptyIfNull(a.PlayerProfile.Contact.LastName), StringHelper.ReturnEmptyIfNull(a.PlayerProfile.Contact.FirstName)),
                        Gender = a.PlayerProfile.Gender.ToString(),
                        ImageUrl = GetStudentImageUrl(a.PlayerProfile.Gender),
                    }
                 ).OrderBy(a => a.FullName).ToList();

                response = Request.CreateResponse(HttpStatusCode.OK, new { Students = result, TotalCount = totalCount });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpGet]
        [Route("api/Program/GetAllSessions")]
        public HttpResponseMessage GetAllSessions(long programId)
        {
            HttpResponseMessage response = null;

            try
            {
                var program = Ioc.ProgramBusiness.Get(programId);

                var clubDateTime = Ioc.ClubBusiness.GetClubDateTime(program.Club.Id, DateTime.UtcNow);

                var programSesssions = Ioc.ProgramBusiness.GetSessions(program)
                    .Where(s => s.End.Date <= clubDateTime.Date)
                    .OrderBy(o => o.Start)
                    .Select(s =>
                        new
                        {
                            Id = s.Id,
                            Date = s.Start.ToString("ddd, MMM dd, yyyy"),
                        })
                        .DistinctBy(o => o.Date)
                        .ToList();

                var currentSessionAttendaces = new object();
                var currentSessionId = 0;

                if (programSesssions.Any() && programSesssions != null)
                {
                    currentSessionId = programSesssions.Last().Id;
                    switch (program.TypeCategory)
                    {
                        case ProgramTypeCategory.Class:
                            currentSessionAttendaces = GetSessionAttendances(programId, currentSessionId);
                            break;
                        case ProgramTypeCategory.Camp:
                            break;
                        case ProgramTypeCategory.Subscription:
                            currentSessionAttendaces = GetSessionSubscriptionAttendances(programId, currentSessionId);
                            break;
                        default:
                            break;
                    }


                    response = Request.CreateResponse(HttpStatusCode.OK, new { AllSessions = programSesssions, CurrentSessionAttendaces = currentSessionAttendaces });
                    return response;
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { AllSessions = (ScheduleSession)null, Message = "The sessions for this class have not started yet." });
                    return response;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpGet]
        [Route("api/Program/GetAllSessions_V2_2_1")]
        public HttpResponseMessage GetAllSessions_V2_2_1(long programId, int page, int pageSize)
        {
            HttpResponseMessage response = null;
            int totalCount = 0;

            try
            {
                var program = Ioc.ProgramBusiness.Get(programId);

                var clubDateTime = Ioc.ClubBusiness.GetClubDateTime(program.Club.Id, DateTime.UtcNow);

                var programSesssions = Ioc.ProgramBusiness.GetSessions(program)
                    .Where(s => s.End.Date <= clubDateTime.Date)
                    .OrderBy(o => o.Start)
                    .Select(s =>
                        new
                        {
                            Id = s.Id,
                            Date = s.Start.ToString("ddd, MMM dd, yyyy"),
                            DateTime = s.Start.Date
                        })
                        .DistinctBy(o => o.Date)
                        .ToList();

                var currentSessionAttendaces = new object();
                var currentSessionId = 0;

                if (programSesssions.Any() && programSesssions != null)
                {
                    currentSessionId = programSesssions.Last().Id;
                    switch (program.TypeCategory)
                    {
                        case ProgramTypeCategory.Class:
                            currentSessionAttendaces = GetSessionAttendances_V2_2_1(programId, currentSessionId, page, pageSize, ref totalCount);
                            break;

                        case ProgramTypeCategory.Camp:
                        case ProgramTypeCategory.Subscription:
                            currentSessionAttendaces = GetSessionSubscriptionAttendances_V2_2_1(programId, currentSessionId, page, pageSize, ref totalCount);
                            break;

                        case ProgramTypeCategory.BeforeAfterCare:
                            GetBeforeAfterAttendances(programId, programSesssions.Last().DateTime, page, pageSize, ref totalCount);
                            break;
                    }


                    response = Request.CreateResponse(HttpStatusCode.OK, new { AllSessions = programSesssions, CurrentSessionAttendaces = currentSessionAttendaces, TotalCount = totalCount });
                    return response;
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { AllSessions = (ScheduleSession)null, Message = "The sessions for this class have not started yet." });
                    return response;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpGet]
        [Route("api/Program/GetSessionAttendaces")]
        public HttpResponseMessage GetSessionAttendaces(long programId, int sessionId)
        {
            HttpResponseMessage response = null;

            try
            {
                var program = Ioc.ProgramBusiness.Get(programId);

                var sessionAttendaces = new List<SessionAttendanceModel>();
                switch (program.TypeCategory)
                {
                    case ProgramTypeCategory.Class:
                        sessionAttendaces = GetSessionAttendances(programId, sessionId);
                        break;
                    case ProgramTypeCategory.Camp:
                        break;
                    case ProgramTypeCategory.Subscription:
                        sessionAttendaces = GetSessionSubscriptionAttendances(programId, sessionId);
                        break;
                    default:
                        break;
                }


                response = Request.CreateResponse(HttpStatusCode.OK, new { SessionAttendaces = sessionAttendaces });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpGet]
        [Route("api/Program/GetSessionAttendaces_V2_2_1")]
        public HttpResponseMessage GetSessionAttendaces_V2_2_1(long programId, int? sessionId, DateTime? dateTime, int page, int pageSize)
        {
            HttpResponseMessage response = null;
            int totalCount = 0;

            try
            {
                var program = Ioc.ProgramBusiness.Get(programId);


                var sessionAttendaces = new List<SessionAttendanceModel>();
                switch (program.TypeCategory)
                {
                    case ProgramTypeCategory.Class:
                        {
                            if (sessionId.HasValue)
                            {
                                sessionAttendaces = GetSessionAttendances_V2_2_1(programId, sessionId.Value, page, pageSize,
                                    ref totalCount);
                            }
                        }
                        break;
                    case ProgramTypeCategory.Camp:
                        {
                            var startDate = dateTime.Value;

                            var programSessionBusiness = Ioc.ProgramSessionBusiness;

                            var programSessions = programSessionBusiness.GetList()
                                .Where(p => p.Charge.ProgramSchedule.ProgramId == programId && !p.Charge.ProgramSchedule.IsDeleted)
                                .ToList();

                            if (programSessions != null && programSessions.Any())
                            {
                                sessionId = programSessions.Any(p => p.StartDateTime.Date == startDate.Date)
                                    ? programSessions.FirstOrDefault(p => p.StartDateTime.Date == startDate.Date).Id
                                    : 0;
                            }
                            else
                            {
                                sessionId = 0;
                            }

                            if (sessionId.Value != 0)
                            {
                                sessionAttendaces = GetSessionSubscriptionAttendances_V2_2_1(programId, sessionId.Value,
                                    page, pageSize, ref totalCount);
                            }
                        }
                        break;
                    case ProgramTypeCategory.Subscription:
                        {
                            var schedule = program.ProgramSchedules.First();

                            var sessions = Ioc.ProgramSessionBusiness.GetList(schedule);

                            sessionId = sessions != null && sessions.Any(p => p.StartDateTime.Date == dateTime.Value.Date)
                                ? sessions.SingleOrDefault(p => p.StartDateTime.Date == dateTime.Value.Date).Id
                                : 0;

                            if (sessionId.Value != 0)
                            {
                                sessionAttendaces = GetSessionSubscriptionAttendances_V2_2_1(programId, sessionId.Value,
                                    page, pageSize, ref totalCount);
                            }
                        }
                        break;
                    case ProgramTypeCategory.BeforeAfterCare:
                        {
                            sessionAttendaces = GetBeforeAfterAttendances(programId, dateTime.Value, page, pageSize, ref totalCount);
                        }
                        break;
                }


                response = Request.CreateResponse(HttpStatusCode.OK, new { SessionAttendaces = sessionAttendaces, TotalCount = totalCount });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpPost]
        [Route("api/Program/UpdateSessionAttendances_V2_2_1")]
        public HttpResponseMessage UpdateSessionAttendances_V2_2_1([FromBody]AttendanceStatusModel model)
        {
            HttpResponseMessage response = null;

            try
            {
                var statusClassUpdates = new OperationStatus() { Status = true };
                var statusSubscriptionUpdates = new OperationStatus() { Status = true };

                var programBusiness = Ioc.ProgramBusiness;

                var modelClasses = model.StudentsStatus.Where(s => s.Id.Split('-').Count() == 4);

                var modelSubscriptions = model.StudentsStatus.Where(s => s.Id.Split('-').Count() == 5);

                if (modelClasses != null && modelClasses.Any())
                {

                    var attendances = modelClasses.Select(s =>
                         new ScheduleAttendance
                         {
                             Id = int.Parse(s.Id.Split('-').ElementAt(0)),
                             ProfileId = int.Parse(s.Id.Split('-').ElementAt(1)),
                             ScheduleId = int.Parse(s.Id.Split('-').ElementAt(2)),
                             SessionId = int.Parse(s.Id.Split('-').ElementAt(3)),
                             Status = s.Status,
                             ClubStaffId = s.ClubStaffId.Value
                         })
                         .ToList();

                    statusClassUpdates = Ioc.ProgramBusiness.UpdateAttendances(attendances.ToList());

                }

                if (modelSubscriptions != null && modelSubscriptions.Any())
                {

                    var attendances = modelSubscriptions.Select(s =>
                         new StudentAttendance
                         {
                             Id = int.Parse(s.Id.Split('-').ElementAt(0)),
                             OrderSessionId = int.Parse(s.Id.Split('-').ElementAt(4)),
                             AttendanceStatus = s.Status,
                             AttendanceStaffId = s.ClubStaffId.Value
                         })
                         .ToList();

                    statusSubscriptionUpdates = Ioc.StudentAttendanceBusiness.UpdateAttendances(attendances.ToList());

                }

                var result = statusClassUpdates.Status && statusSubscriptionUpdates.Status;

                response = Request.CreateResponse(HttpStatusCode.OK, new { Updated = result });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpPost]
        [Route("api/Program/UpdateSessionAttendances")]
        public HttpResponseMessage UpdateSessionAttendances([FromBody]AttendanceStatusModel model)
        {
            HttpResponseMessage response = null;

            try
            {
                var currentUserId = User.Identity.GetCurrentUserId();

                var scheduleId = int.Parse(model.StudentsStatus.First().Id.Split('-').ElementAt(2));

                var program = Ioc.ProgramBusiness.GetByScheduleId(scheduleId);

                var clubStaffId = program.OutSourceSeason != null ?
                program.OutSourceSeason.Club.ClubStaffs.SingleOrDefault(s => s.JbUserRole.UserId == currentUserId && !s.IsDeleted) != null ?
                   program.OutSourceSeason.Club.ClubStaffs.SingleOrDefault(s => s.JbUserRole.UserId == currentUserId && !s.IsDeleted).Id : program.Club.ClubStaffs.SingleOrDefault(s => s.JbUserRole.UserId == currentUserId && !s.IsDeleted).Id :
                program.Club.ClubStaffs.SingleOrDefault(s => s.JbUserRole.UserId == currentUserId && !s.IsDeleted).Id;

                var statusClassUpdates = new OperationStatus() { Status = true };
                var statusSubscriptionUpdates = new OperationStatus() { Status = true };

                var programBusiness = Ioc.ProgramBusiness;

                var modelClasses = model.StudentsStatus.Where(s => s.Id.Split('-').Count() == 4);

                var modelSubscriptions = model.StudentsStatus.Where(s => s.Id.Split('-').Count() == 5);

                if (modelClasses != null && modelClasses.Any())
                {

                    var attendances = model.StudentsStatus.Select(s =>
                         new ScheduleAttendance
                         {
                             Id = int.Parse(s.Id.Split('-').ElementAt(0)),
                             ProfileId = int.Parse(s.Id.Split('-').ElementAt(1)),
                             ScheduleId = int.Parse(s.Id.Split('-').ElementAt(2)),
                             SessionId = int.Parse(s.Id.Split('-').ElementAt(3)),
                             Status = s.Status,
                             ClubStaffId = s.ClubStaffId.HasValue ? s.ClubStaffId : clubStaffId
                         })
                         .ToList();

                    statusClassUpdates = Ioc.ProgramBusiness.UpdateAttendances(attendances.ToList());

                }

                if (modelSubscriptions != null && modelSubscriptions.Any())
                {

                    var attendances = model.StudentsStatus.Select(s =>
                         new StudentAttendance
                         {
                             Id = int.Parse(s.Id.Split('-').ElementAt(0)),
                             OrderSessionId = int.Parse(s.Id.Split('-').ElementAt(4)),
                             AttendanceStatus = s.Status,
                             AttendanceStaffId = s.ClubStaffId.HasValue ? s.ClubStaffId : clubStaffId
                         })
                         .ToList();

                    statusSubscriptionUpdates = Ioc.StudentAttendanceBusiness.UpdateAttendances(attendances.ToList());

                }

                var result = statusClassUpdates.Status && statusSubscriptionUpdates.Status;

                response = Request.CreateResponse(HttpStatusCode.OK, new { Updated = result });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpPost]
        [Route("api/Program/SendEmail")]
        public HttpResponseMessage SendEmail([FromBody]EmailToStudentModel model)
        {
            HttpResponseMessage response = null;

            try
            {
                var programBusiness = Ioc.ProgramBusiness;
                var provider = programBusiness.Get(model.ProgramId).OutSourceSeason != null ? programBusiness.Get(model.ProgramId).OutSourceSeason.Club :
                                programBusiness.Get(model.ProgramId).Club;

                var player = Ioc.PlayerProfileBusiness.Get(model.StudentId);

                var userEmail = player.User.Email;

                var providerEmail = provider.ContactPersons.First().Email;

                var providerDomainFormat = string.Format(Constants.W_JumbulaEmailFormat, provider.Domain);

                var from = new MailAddress(providerDomainFormat, provider.Name);
                var replyTo = new MailAddress(providerEmail, provider.Name);
                var to = new MailAddress(userEmail);

                Ioc.OldEmailBusiness.SendEmail(from, to, replyTo, model.Subject, model.Body, true);

                response = Request.CreateResponse(HttpStatusCode.OK, new { MailSent = true });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }

        }

        [HttpPost]
        [Route("api/Program/SendEmailToAll")]
        public HttpResponseMessage SendEmailToAll([FromBody]EmailToClassMembersModel model)
        {
            HttpResponseMessage response = null;

            try
            {
                var studentEmails = Ioc.ProgramBusiness.GetProgramAttendance(model.ProgramId)
               .Select(a => a.PlayerProfile.User.UserName).ToList();

                var programBusiness = Ioc.ProgramBusiness;
                var provider = programBusiness.Get(model.ProgramId).OutSourceSeason != null ? programBusiness.Get(model.ProgramId).OutSourceSeason.Club :
                                programBusiness.Get(model.ProgramId).Club;

                var providerEmail = provider.ContactPersons.First().Email;

                var providerDomainFormat = string.Format(Constants.W_JumbulaEmailFormat, provider.Domain);

                var from = new MailAddress(providerDomainFormat, provider.Name);
                var replyTo = new MailAddress(providerEmail, provider.Name);

                var emailBusiness = Ioc.OldEmailBusiness;

                foreach (var studentEmail in studentEmails)
                {
                    emailBusiness.SendEmail(from, new MailAddress(studentEmail), replyTo, model.Subject, model.Body, true);
                }

                response = Request.CreateResponse(HttpStatusCode.OK, new { MailsSent = true });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpPost]
        [Route("api/Program/SendEmailToStudentsSchool")]
        public HttpResponseMessage SendEmailToStudentsSchool([FromBody]EmailToSchoolMembersModel model)
        {
            HttpResponseMessage response = null;

            try
            {
                var programIds = model.Students.Select(s => s.ProgramId);
                var profileIds = model.Students.Select(s => s.ProfileId);

                var students = Ioc.OrderItemBusiness.GetList().Where(o => profileIds.Contains(o.PlayerId.Value) &&
                programIds.Contains(o.ProgramSchedule.ProgramId));

                var emailBusiness = Ioc.OldEmailBusiness;
                var clubBusiness = Ioc.ClubBusiness;

                foreach (var student in students)
                {
                    var provider = student.ProgramSchedule.Program.OutSourceSeason != null ? student.ProgramSchedule.Program.OutSourceSeason.Club :
                        student.ProgramSchedule.Program.Club;

                    var providerEmail = provider.ContactPersons.First().Email;

                    var providerDomainFormat = string.Format(Constants.W_JumbulaEmailFormat, provider.Domain);

                    var from = new MailAddress(providerDomainFormat, provider.Name);

                    var replyTo = new MailAddress(providerEmail, provider.Name);

                    var studentEmail = student.Order.User.UserName;

                    emailBusiness.SendEmail(from, new MailAddress(studentEmail), replyTo, model.Subject, model.Body, true);
                }

                response = Request.CreateResponse(HttpStatusCode.OK, new { MailsSent = true });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpGet]
        [Route("api/Program/GetBodyAbsenteeEmail")]
        public HttpResponseMessage GetBodyAbsenteeEmail(int clubId)
        {
            HttpResponseMessage response = null;

            try
            {
                var clubBusiness = Ioc.ClubBusiness;

                var club = clubBusiness.Get(clubId);

                var subject = "";
                var body = clubBusiness.GetClubNotification(club, TypeClubNotification.Absentee);

                response = Request.CreateResponse(HttpStatusCode.OK, new { Subject = subject, Body = body });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpGet]
        [Route("api/Program/GetStudentAttendance")]
        public HttpResponseMessage GetStudentAttendance(long programId, int studentId)
        {
            HttpResponseMessage response = null;
            try
            {
                var programs = Ioc.ProgramBusiness;
                var program = programs.Get(programId);
                var clubDateTime = Ioc.ClubBusiness.GetClubDateTime(program.Club.Id, DateTime.UtcNow);

                var studentAttendaces = new List<ScheduleAttendance>();
                var attendances = new List<object>();
                bool hasAttendance = false;

                studentAttendaces = programs.GetStudentAttendance(programId, studentId).ToList();
                var programSesssions = programs.GetSessions(program).ToList();

                if (programSesssions != null && programSesssions.Any())
                {
                    foreach (var session in programSesssions)
                    {
                        foreach (var attendance in studentAttendaces)
                        {
                            if (session.Start == attendance.Start)
                            {
                                var studentAttendace = studentAttendaces.First(s => s.Id == attendance.Id);
                                attendances.Add(new
                                {
                                    Id = studentAttendace.Id,
                                    Status = (int)studentAttendace.Status,
                                    Date = studentAttendace.Start.ToString("ddd, MMM dd, yyyy")
                                });
                                hasAttendance = true;
                            }
                        }

                        if (!hasAttendance)
                        {
                            if (session.Start <= clubDateTime)
                            {
                                attendances.Add(new
                                {
                                    Id = session.Id,
                                    Status = -1,
                                    Date = session.Start.ToString("ddd, MMM dd, yyyy")
                                });
                            }
                            else if (session.Start > clubDateTime)
                            {
                                attendances.Add(new
                                {
                                    Id = session.Id,
                                    Status = -2,
                                    Date = session.Start.ToString("ddd, MMM dd, yyyy")
                                });
                            }
                        }

                        hasAttendance = false;
                    }

                    response = Request.CreateResponse(HttpStatusCode.OK, new { AllAttendance = attendances, Message = "", TotalSessions = programSesssions.Count });
                    return response;
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { AllAttendance = (ScheduleAttendance)null, Message = "The sessions for this class have not started yet.", TotalSessions = 0 });
                    return response;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }


        [HttpPost]
        [Route("api/Program/UpdateStudentNotes")]
        public HttpResponseMessage UpdateStudentNotes([FromBody]StudentNotesModel model)
        {
            HttpResponseMessage response = null;

            try
            {
                var notes = model.StudentNotes.Select(m => new PlayerProgramNote()
                {
                    Id = m.Id,
                    ClubStaffId = m.ClubStaffId,
                    ProgramId = m.ProgramId,
                    ProfileId = m.ProfileId,
                    Color = m.Color,
                    Note = m.Note,
                    Title = m.Title,
                }).ToList();

                var result = Ioc.PlayerProgramNoteBusiness.Update(notes);

                response = Request.CreateResponse(HttpStatusCode.OK, new { Status = result.Status });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpGet]
        [Route("api/Program/GetStudentNotes")]
        public HttpResponseMessage GetStudentNotes(int programId, int clubStaffId, int profileId)
        {
            HttpResponseMessage response = null;

            try
            {
                var program = Ioc.ProgramBusiness.Get(programId);
                var clubId = program.OutSourceSeason != null ? program.OutSourceSeason.ClubId : program.ClubId;
                var clubBusiness = Ioc.ClubBusiness;

                var playerProgramNoteBusiness = Ioc.PlayerProgramNoteBusiness;
                var notes = playerProgramNoteBusiness.GetList(programId, clubStaffId, profileId).ToList().Where(n => !n.IsDeleted).Select(n => new
                {
                    Id = n.Id,
                    ProgramId = n.ProgramId,
                    ProfileId = n.ProfileId,
                    ClubStaffId = n.ClubStaffId,
                    Title = n.Title,
                    Note = n.Note,
                    BackgroundColor = n.Color,
                    Date = clubBusiness.GetClubDateTime(clubId, n.MetaData.DateUpdated).ToString("MMM dd, yyyy hh:mm tt")
                }).ToList().OrderByDescending(n => n.Id);


                response = Request.CreateResponse(HttpStatusCode.OK, new { Notes = notes });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }


        [HttpGet]
        [Route("api/Program/GetStudentNote")]
        public HttpResponseMessage GetStudentNote(int id)
        {
            HttpResponseMessage response = null;

            try
            {
                var note = Ioc.PlayerProgramNoteBusiness.Get(id);
                var program = Ioc.ProgramBusiness.Get(note.ProgramId);
                var clubId = program.OutSourceSeason != null ? program.OutSourceSeason.ClubId : program.ClubId;

                dynamic data = new ExpandoObject();
                data.Id = note.Id;
                data.ProgramId = note.ProgramId;
                data.ProfileId = note.ProfileId;
                data.ClubStaffId = note.ClubStaffId;
                data.Title = note.Title;
                data.Note = note.Note;
                data.BackgroundColor = note.Color;
                data.Date = Ioc.ClubBusiness.GetClubDateTime(clubId, note.MetaData.DateUpdated).ToString("MMM dd, yyyy hh:mm tt");

                response = Request.CreateResponse(HttpStatusCode.OK, new { Note = data });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }


        [HttpPost]
        [Route("api/Program/DeleteNotes")]
        public HttpResponseMessage DeleteNotes([FromBody]NoteIdModel model)
        {
            HttpResponseMessage response = null;

            try
            {
                var result = Ioc.PlayerProgramNoteBusiness.Delete(model.ids);

                response = Request.CreateResponse(HttpStatusCode.OK, new { Status = result.Status });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }


        private List<SessionAttendanceModel> GetSessionAttendances(long programId, int sessionId)
        {
            var result = Ioc.ProgramBusiness.GetSessionAttendance(programId, sessionId).ToList().Select(s =>
                  new SessionAttendanceModel
                  {
                      Id = string.Concat(s.Id, "-", s.ProfileId, "-", s.ScheduleId, "-", s.SessionId),
                      Status = s.Status,
                      FullName = string.Format("{0}, {1}", StringHelper.ReturnEmptyIfNull(s.PlayerProfile.Contact.LastName), StringHelper.ReturnEmptyIfNull(s.PlayerProfile.Contact.FirstName)),
                      Gender = s.PlayerProfile.Gender,
                      ImageUrl = GetStudentImageUrl(s.PlayerProfile.Gender),
                  }).OrderBy(s => s.FullName).ToList();

            return result;
        }

        private List<SessionAttendanceModel> GetSessionAttendances_V2_2_1(long programId, int sessionId, int page, int pageSize, ref int totalCount)
        {
            var result = Ioc.ProgramBusiness.GetSessionAttendance_V2_2_1(programId, sessionId, page, pageSize, ref totalCount).Select(s =>
             new SessionAttendanceModel
             {
                 Id = string.Concat(s.Id, "-", s.ProfileId, "-", s.ScheduleId, "-", s.SessionId),
                 Status = s.Status,
                 FullName = string.Format("{0}, {1}", StringHelper.ReturnEmptyIfNull(s.PlayerProfile.Contact.LastName), StringHelper.ReturnEmptyIfNull(s.PlayerProfile.Contact.FirstName)),
                 Gender = s.PlayerProfile.Gender,
                 ImageUrl = GetStudentImageUrl(s.PlayerProfile.Gender),
             }).ToList();

            return result;
        }

        private List<SessionAttendanceModel> GetSessionSubscriptionAttendances(long programId, int sessionId)
        {
            var result = Ioc.ProgramBusiness.GetSessionSubscriptionAttendance(programId, sessionId).ToList().Select(s =>
                  new SessionAttendanceModel
                  {
                      Id = string.Concat(s.Id, "-", s.OrderSession.OrderItem.PlayerId, "-", s.OrderSession.OrderItem.ProgramScheduleId, "-", s.OrderSession.ProgramSessionId, "-", s.OrderSession.Id),
                      Status = s.AttendanceStatus,
                      FullName = string.Format("{0}, {1}", StringHelper.ReturnEmptyIfNull(s.OrderSession.OrderItem.Player.Contact.LastName), StringHelper.ReturnEmptyIfNull(s.OrderSession.OrderItem.Player.Contact.FirstName)),
                      Gender = s.OrderSession.OrderItem.Player.Gender,
                      ImageUrl = GetStudentImageUrl(s.OrderSession.OrderItem.Player.Gender),
                  }).OrderBy(s => s.FullName).ToList();

            return result;
        }

        private List<SessionAttendanceModel> GetSessionSubscriptionAttendances_V2_2_1(long programId, int sessionId, int page, int pageSize, ref int totalCount)
        {
            var result = Ioc.ProgramBusiness.GetSessionSubscriptionAttendance_V2_2_1(programId, sessionId, page, pageSize, ref totalCount).Select(s =>
             new SessionAttendanceModel
             {
                 Id = string.Concat(s.Id, "-", s.OrderSession.OrderItem.PlayerId, "-", s.OrderSession.OrderItem.ProgramScheduleId, "-", s.OrderSession.ProgramSessionId, "-", s.OrderSession.Id),
                 Status = s.AttendanceStatus,
                 FullName = string.Format("{0}, {1}", StringHelper.ReturnEmptyIfNull(s.OrderSession.OrderItem.Player.Contact.LastName), StringHelper.ReturnEmptyIfNull(s.OrderSession.OrderItem.Player.Contact.FirstName)),
                 Gender = s.OrderSession.OrderItem.Player.Gender,
                 ImageUrl = GetStudentImageUrl(s.OrderSession.OrderItem.Player.Gender),
             }).ToList();

            return result;
        }

        private List<SessionAttendanceModel> GetBeforeAfterAttendances(long programId, DateTime date, int page, int pageSize, ref int totalCount)
        {
            var result = Ioc.ProgramBusiness.GetBeforeAfterAttendance(programId, date, page, pageSize, ref totalCount).Select(s =>
                new SessionAttendanceModel
                {
                    Id = string.Concat(s.Id, "-", s.OrderSession.OrderItem.PlayerId, "-", s.OrderSession.OrderItem.ProgramScheduleId, "-", s.OrderSession.ProgramSessionId, "-", s.OrderSession.Id),
                    Status = s.AttendanceStatus,
                    FullName = $"{StringHelper.ReturnEmptyIfNull(s.OrderSession.OrderItem.Player.Contact.LastName)}, {StringHelper.ReturnEmptyIfNull(s.OrderSession.OrderItem.Player.Contact.FirstName)}",
                    Gender = s.OrderSession.OrderItem.Player.Gender,
                    ImageUrl = GetStudentImageUrl(s.OrderSession.OrderItem.Player.Gender),
                }).ToList();

            return result;
        }

        private string GetStudentImageUrl(GenderCategories gender = GenderCategories.None)
        {
            var baseUrl = string.Format("{0}://{1}", Url.Request.RequestUri.Scheme, Url.Request.RequestUri.Host);
            var imageFolderUrl = string.Concat(baseUrl, "/Images/Mobile");
            string imageFileName = string.Empty;

            switch (gender)
            {
                case GenderCategories.None:
                    {
                        imageFileName = "allgenders.png";
                    }
                    break;
                case GenderCategories.Male:
                    {
                        imageFileName = "male.png";
                    }
                    break;
                case GenderCategories.Female:
                    {
                        imageFileName = "female.png";
                    }
                    break;
                case GenderCategories.All:
                    {
                        imageFileName = "allgenders.png";
                    }
                    break;
                default:
                    break;
            }

            return string.Concat(imageFolderUrl, "/", imageFileName);
        }

    }

    public enum PresenceStatus : byte
    {
        Present = 0,
        Absent = 1,
        Late = 2
    }

    public class AttendanceStatusModel
    {
        public List<StudentPresentStatus> StudentsStatus { get; set; }
    }

    public class StudentPresentStatus
    {
        public string Id { get; set; }

        public AttendanceStatus? Status { get; set; }

        public int? ClubStaffId { get; set; }
    }

    public class EmailToStudentModel
    {
        public int ProgramId { get; set; }

        public int StudentId { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }
    }

    public class EmailToClassMembersModel
    {
        public int ProgramId { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }
    }

    public class EmailToStudentSchoolModel
    {
        public long ProgramId { get; set; }

        public int ProfileId { get; set; }
    }

    public class EmailToSchoolMembersModel
    {
        public List<EmailToStudentSchoolModel> Students { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }
    }

    public class StudentNote
    {
        public int Id { get; set; }

        public long ProgramId { get; set; }

        public int ClubStaffId { get; set; }

        public int ProfileId { get; set; }

        public string Title { get; set; }

        public string Note { get; set; }

        public string Color { get; set; }
    }

    public class NoteIdModel
    {
        public List<int> ids { get; set; }
    }

    public class StudentNotesModel
    {
        public List<StudentNote> StudentNotes { get; set; }
    }

}
