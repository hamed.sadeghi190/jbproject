﻿using Jumbula.Common.Enums;
using Jumbula.Common.Utilities;
using Newtonsoft.Json;
using SportsClub.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{

    public class BeforeAfterCareCreateEditModelStep2 : BaseViewModel<Program, long>, IProgramWizardModel
    {
        public BeforeAfterCareCreateEditModelStep2()
        {
            RegistrationPeriod = new RegistrationPeriodViewModel();
            Subscriptions = new List<SubscriptionViewModel>();
            PaymentDueDate = SeedDueDate();
            BeforeSchoolTitle = "Before school";
            AfterSchoolTitle = "After school";
            ComboTitle = "Combo";
            IsAfterSchool = true;
            SelectedDueDate = 0;
        }

        public string ClubDomain { get; set; }

        public string Domain { get; set; }

        public ProgramPageStep LastCreatedPage { get; set; }

        public string Name { get; set; }

        public PageMode PageMode
        {
            get
            {

                if (this.PageStep <= this.LastCreatedPage)
                {
                    return PageMode.Edit;
                }

                return PageMode.Create;
            }
        }

        public ProgramPageStep PageStep { get; set; }

        public ProgramRegStatus RegStatus { get; set; }

        public ProgramRunningStatus RunningStatus { get; set; }

        public SaveType SaveType { get; set; }

        public string SeasonDomain { get; set; }

        public ProgramStatus Status { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Description")]
        [StringLength(4096, ErrorMessage = "{0} must be at last {1} characters.")]

        public string BeforeSchoolDescription { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Description")]
        [StringLength(4096, ErrorMessage = "{0} must be at last {1} characters.")]

        public string AfterSchoolDescription { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Description")]
        [StringLength(4096, ErrorMessage = "{0} must be at last {1} characters.")]

        public string ComboDescription { get; set; }

        public RegistrationPeriodViewModel RegistrationPeriod { get; set; }

        [Display(Name = "Start date")]
        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "{0} is required.")]
        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? StartDate { get; set; }

        [Display(Name = "End date")]
        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "{0} is required.")]
        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? EndDate { get; set; }

        public List<SubscriptionViewModel> Subscriptions { get; set; }
        public ProgramTypeCategory TypeCategory { get; set; }
        public List<SelectKeyValue<string>> ContinueTypes { get; set; }
        public ContinueType SelectedContinueType { get; set; }
        public List<SelectKeyValue<string>> GenderItems { get; set; }
        public Genders SelectedGender { get; set; }
        public List<SelectKeyValue<string>> RestrictionTypes { get; set; }
        public RestrictionType SelectedRestrictionType { get; set; }
        public List<SelectKeyValue<string>> Ages { get; set; }
        public List<SelectKeyValue<string>> Grades { get; set; }

        public List<SelectKeyValue<string>> DaysBeforeBillingItems { get; set; }
        public string SelectedMinAge { get; set; }
        public string SelectedMaxAge { get; set; }
        public SchoolGradeType? SelectedMinGrade { get; set; }
        public SchoolGradeType? SelectedMaxGrade { get; set; }
        public List<SelectKeyValue<string>> Sessions { get; set; }
        public string SelectedSession { get; set; }

        public bool IsProgramStartDateChanged { get; set; }

        public bool IsProgramEndDateChanged { get; set; }
        public bool IsProrate { get; set; }

        [Display(Name = "Prorate time")]
        [Required(ErrorMessage = "{0} is required.")]
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime ProrateTime { get; set; }

        public decimal? RegistrationFee { get; set; }

        public long BeforeProgramScheduleId { get; set; }
        public long AfterProgramScheduleId { get; set; }
        public long ComboProgramScheduleId { get; set; }

        public bool IsFreezedBeforeProgramSchedule { get; set; }
        public bool IsFreezedAfterProgramSchedule { get; set; }
        public bool IsFreezedComboProgramSchedule { get; set; }
        public List<EarlyBird> EarlyBirds { get; set; }

        public bool HasEarlyBird { get; set; }

        public bool HasPayInFullOption { get; set; }

        public decimal BeforeFullPayDiscount { get; set; }
        public decimal AfterFullPayDiscount { get; set; }
        public decimal ComboFullPayDiscount { get; set; }
        public decimal BeforeDropInFee { get; set; }
        public decimal AfterDropInFee { get; set; }
        public decimal ComboDropInFee { get; set; }
        public string BeforeDropInTitel { get; set; }
        public string AfterDropInTitel { get; set; }
        public string ComboDropInTitel { get; set; }
        public bool EnableDropIn { get; set; }

        public bool HasOrderBeforeSchool { get; set; }

        public bool HasOrderAfterSchool { get; set; }

        public bool HasOrderComboSchool { get; set; }

        public bool HasProgramActiveOrder { get; set; }

        public ProgramSchedulePartModel FirstProgramPart { get; set; }
        public ProgramSchedulePartModel LastProgramPart { get; set; }

        public List<ProgramSchedulePartModel> ProgramScheduleParts { get; set; }

        public List<ProgramSchedulePartModel> DeletedProgramSchedulePartsByEndDate { get; set; }

        public List<ProgramSchedulePartModel> DeletedProgramSchedulePartsByStartDate { get; set; }

        [Display(Name = "Drop-in session label")]
        [Required(ErrorMessage = "{0} is required.")]
        public string DropInSessionLabel { get; set; }

        [Display(Name = "Drop-in session price")]
        [Required(ErrorMessage = "{0} is required.")]
        public decimal? DropInSessionPrice { get; set; }

        public bool EnableSameDropInPrice { get; set; }

        [Display(Name = "price")]
        [Required(ErrorMessage = "{0} is required.")]
        public decimal? DropInSameSessionPrice { get; set; }

        public bool EnablePunchCard { get; set; }

        [Display(Name = "Label")]
        [Required(ErrorMessage = "{0} is required.")]
        public string PunchcardSessionLabel { get; set; }

        [Display(Name = "Price")]
        [Required(ErrorMessage = "{0} is required.")]
        public decimal? PunchcardSessionPrice { get; set; }
        public List<SelectKeyValue<int>> PunchcardSessions { get; set; }
        public int SelectedPunchcard { get; set; }

        [Display(Name = "Message to users")]
        [MaxLength(50, ErrorMessage = "Maximum of {0} id {1} maximum.")]
        public string FullPayDiscountMessage { get; set; }

        public List<BeforAfterCareScheduleDaysViewModel> Days { get; set; }


        public bool AppliesRestrictionAtTheProgramStart { get; set; }
        public List<SelectKeyValue<string>> Capacities { get; set; }
        public List<SelectKeyValue<int>> PaymentDueDate { get; set; }
        public string Capacity { get; set; }
        public bool GetPaymentAtRefister { get; set; }
        public bool HasProrate { get; set; }
        public bool IsBeforeSchool { get; set; }
        public bool IsAfterSchool { get; set; }
        public bool IsCombo { get; set; }
        [Display(Name = "Title")]
        [Required(ErrorMessage = "{0} is required.")]
        public string BeforeSchoolTitle { get; set; }
        [Display(Name = "Title")]
        [Required(ErrorMessage = "{0} is required.")]
        public string AfterSchoolTitle { get; set; }
        [Display(Name = "Title")]
        [Required(ErrorMessage = "{0} is required.")]
        public string ComboTitle { get; set; }
        public List<SelectKeyValue<string>> PaymentSchaduleItems { get; set; }
        public PaymentSchedule? SelectedPaymentSchaduleMode { get; set; }
        public List<SelectKeyValue<string>> PaymentSchaduleTypes { get; set; }
        public MonthlyPaymentScheduleType? SelectedPaymentSchaduleType { get; set; }
        public int SelectedDueDate { get; set; }

        public List<SelectKeyValue<int>> NumberOfClassDays { get; set; }
        List<SelectKeyValue<int>> SeedDueDate()
        {
            var result = new List<SelectKeyValue<int>>()
            {
                new SelectKeyValue<int> { Value = 0, Text = "Beginning of schedule"},
                new SelectKeyValue<int> { Value = 1, Text = "1 day before"},
                new SelectKeyValue<int> { Value = 2, Text = "2 days before"},
                new SelectKeyValue<int> { Value = 3, Text = "3 days before"},
                new SelectKeyValue<int> { Value = 4, Text = "4 days before"},
                new SelectKeyValue<int> { Value = 5, Text = "5 days before"},
                new SelectKeyValue<int> { Value = 6, Text = "6 days before"},
                new SelectKeyValue<int> { Value = 7, Text = "7 days before"},
                new SelectKeyValue<int> { Value = 8, Text = "8 days before"},
                new SelectKeyValue<int> { Value = 9, Text = "9 days before"},
                new SelectKeyValue<int> { Value = 10,Text = "10 days before"},

        };
            return result;
        }
    }
    public class BeforAfterCareScheduleDaysViewModel
    {
        public DayOfWeek DayOfWeek { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Start time")]
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? MorningStartTime { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [JsonConverter(typeof(CustomTimeConverter))]
        [Display(Name = "End time")]
        public DateTime? MorningEndTime { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Start time")]
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? AfternoonStartTime { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [JsonConverter(typeof(CustomTimeConverter))]
        [Display(Name = "End time")]
        public DateTime? AfternoonEndTime { get; set; }
        public bool IsChecked { get; set; }
    }

    public class BeforeAfterCareAddEditStep2FeesViewModel
    {
        public List<BeforeAfterFeesItemViewModel> BeforeFees { get; set; }
        public List<BeforeAfterFeesItemViewModel> AfterFees { get; set; }
        public List<BeforeAfterFeesItemViewModel> ComboFees { get; set; }

        public decimal? BeforeFullPayDiscount { get; set; }
        public decimal? AfterFullPayDiscount { get; set; }
        public decimal? ComboFullPayDiscount { get; set; }

        public bool isSchedulePartsChanged { get; set; }
        public bool IsDropIn { get; set; }

        public bool IsFullPay { get; set; }
        public bool IsSchedulePartChanged { get; set; }
        public bool IsScheduleChargeChanged { get; set; }
        public List<SelectKeyValue<string>> Capacities { get; set; }

        public string BeforeCapacity { get; set; }

        public string AfterCapacity { get; set; }

        public List<SelectKeyValue<int>> NumberOfClassDays { get; set; }
        public bool HasComboSchedule { get; set; }
        public bool HasBeforeSchedule { get; set; }
        public bool HasAfterSchedule { get; set; }

        public string LastCreatedPage { get; set; }

    }

    public class BeforeAfterFeesItemViewModel
    {
        public long ChargeId { get; set; }
        [Display(Name = "Tuition label")]
        [Required(ErrorMessage = "{0} is required.")]
        public string TuitionLabel { get; set; }

        public string NumberOfClassDay { get; set; }
        [Display(Name = "Price for schedule")]
        [Required(ErrorMessage = "{0} is required.")]
        [Range(minimum: 0.01, maximum: double.MaxValue, ErrorMessage = "{0} is incorrect.")]
        public decimal? Amount { get; set; }

        public long ScheduleId { get; set; }

        public bool IsDeleted { get; set; }

        public TimeOfClassFormation ScheduleMode { get; set; }
    }

    public class BeforeAfterCareStep2ScheduleListViewModel
    {
        public List<BeforeAfterCareSchedulePartViewModel> ScheduleParts { get; set; }

        public ProgramPageStep LastCreatedPage { get; set; }
        public bool HasBeforeSchedule { get; set; }
        public bool HasAfterSchedule{ get; set; }
        public bool HasComboSchedule { get; set; }
    }
    public class BeforeAfterCareSchedulePartViewModel
    {
        public List<SchedulePartChargeModel> schedulePartCharges { get; set; }
        public long Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime? DueDate { get; set; }

        public bool IsFirstPartOfProgram { get; set; }

        public bool IsLastPartOfProgram { get; set; }

    }
    public class SchedulePartChargeModel
    {
        public long Id { get; set; }
        public decimal Amount { get; set; }
        public long ScheduleId { get; set; }
        public long ChargeId { get; set; }
        public TimeOfClassFormation ScheduleMode { get; set; }

    }

    public class ProgramSchedulePartModel
    {
        public long Id { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

    }
    public enum PaymentSchedule
    {
        [Description("Monthly")]
        Monthly,

        [Description("Bi-weekly")]
        BiWeekly,

        [Description("Weekly")]
        Weekly,
    }
    public enum MonthlyPaymentScheduleType
    {
        [Description("Calendar")]
        Calendar,

        [Description("Roll over")]
        RollOver,
    }
}
