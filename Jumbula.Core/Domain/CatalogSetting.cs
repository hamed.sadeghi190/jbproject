﻿using Jumbula.Common.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jumbula.Core.Domain
{
    public class CatalogSetting: BaseEntity
    {
        public CatalogSetting()
        {
            CatalogSettingCounties = new List<CatalogSettingCounty>();

            CatalogPriceOptions = new List<CatalogItemPriceOption>();

            CatalogOptions = new List<CatalogItemOption>();

            CatalogSearchTags = new CatalogSearchTags();
        }

        [Key, ForeignKey("Club")]
        public override int Id { get; set; }

        public CatalogSearchTags CatalogSearchTags { get; set; }

        public bool WeeklyEmail { get; set; }

        public bool FairFaxCounty { get; set; }

        public bool VirtusCertified { get; set; }

        public bool JumbulaMobileApp { get; set; }
        public bool HasTitle1SchoolDiscounts { get; set; }

        [StringLength(200)]
        public string Title1SchoolDiscountType { get; set; }

        public virtual Club Club { get; set; }

        public virtual IList<CatalogSettingCounty> CatalogSettingCounties { get; set; }

        public virtual IList<CatalogItemPriceOption> CatalogPriceOptions { get; set; }

        public virtual IList<CatalogItemOption> CatalogOptions { get; set; }
    }

    public class CatalogSearchTags
    {
        public bool BeforeSchool { get; set; }

        public bool AfterSchool { get; set; }

        public bool PreSchools { get; set; }

        public bool ElementarySchools { get; set; }

        public bool MiddleSchools { get; set; }

        public bool HighSchools { get; set; }

        public bool NoGlobalSearch { get; set; }

    }

   
}
