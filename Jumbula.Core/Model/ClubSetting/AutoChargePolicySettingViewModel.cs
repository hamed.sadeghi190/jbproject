﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Common.Helper;

namespace Jumbula.Core.Model.ClubSetting
{
    public class AutoChargePolicySettingViewModel
    {
        private static Dictionary<string, string> GetRetriesOptions
        {
            get
            {
                var dictionary = new Dictionary<string, string>();

                for (var i = 1; i <= Common.Constants.ClubSetting.CountOfAttempts; i++)
                {
                    var word = NumberHelper.NumberToWords(i - 1);
                    word = word.First().ToString().ToUpper() + word.Substring(1);

                    switch (i - 1)
                    {
                        case 0:
                            dictionary.Add(i.ToString(), "Do not attempt anymore");
                            break;
                        case 1:
                            dictionary.Add(i.ToString(), word + " additional attempt");
                            break;
                        default:
                            dictionary.Add(i.ToString(), word + " additional attempts");
                            break;
                    }
                }

                return dictionary;
            }
        }

        private static readonly Dictionary<string, string> GetTryDurationOptions = new Dictionary<string, string> { { "1", "Retry 1 day after the previous attempt" }, { "2", "Retry 2 days after the previous attempt" }, { "3", "Retry 3 days after the previous attempt" }, { "4", "Retry 4 days after the previous attempt" }, { "5", "Retry 5 days after the previous attempt" }, { "6", "Retry 6 days after the previous attempt" }, { "7", "Retry 7 days after the previous attempt" } };

        public List<SelectListItem> TotalAttempts => GetRetriesOptions.Select(x => new SelectListItem { Text = x.Value.ToString(), Value = x.Key.ToString() }).ToList();

        public List<SelectListItem> TryDurationOptions => GetTryDurationOptions.Select(x => new SelectListItem { Text = x.Value.ToString(), Value = x.Key.ToString() }).ToList();

        public bool SuccessEmailToAdmin { get; set; } = true;

        public string SuccessAdminEmailTemplate { get; set; }

        public string SuccessParentEmailTemplate { get; set; }

        public bool SendSummeryEmail { get; set; }

        public bool CombinePayments { get; set; }

        public bool HasReplyTo { get; set; }

        [Required(ErrorMessage = "{0} is not valid")]
        [Display(Name = "ReplyTo email address")]
        [RegularExpression(Common.Constants.Validation.EmailRegex, ErrorMessage = "{0} is not valid")]
        public string ReplyToEmailAddress { get; set; }

        public int EnableAttemptsCount { get; set; } = 4;

        public List<AutoChargeAttemptViewModel> Attempts { get; set; } = new List<AutoChargeAttemptViewModel>();
    }

    public class AutoChargeAttemptViewModel
    {
        private string LetterTryNumber
        {
            get
            {
                switch (AttemptOrder - 1)
                {
                    case 1: return "first";
                    case 2: return "second";
                    case 3: return "third";
                    case 4: return "fourth";
                    case 5: return "fifth";
                    case 6: return "sixth";
                    case 7: return "seventh";
                    case 8: return "eighth";
                    case 9: return "ninth";
                    default: return "";
                }
            }
        }

        public int AttemptOrder { get; set; }

        public string FailedAdminEmailTemplate { get; set; } = string.Empty;

        public string FailedParentEmailTemplate { get; set; } = string.Empty;

        public string IntervalDays { get; set; } = 1.ToString();

        public bool FailedEmailToParent { get; set; } = false;

        public bool FailedEmailToAdmin { get; set; } = false;

        public string TemplateTitle => string.Format(Common.Constants.ClubSetting.AutoChargePolicyBlockTitle, LetterTryNumber);

        public bool Enabled { get; set; } = true;
    }
}