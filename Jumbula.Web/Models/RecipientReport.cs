﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class RecipientReport
    {
        public string UserName { get; set; }

        public string EmailAddress { get; set; }

        public string IsDeliveryed { get; set; }

        public string IsOpened { get; set; }

        public string IsClicked { get; set; }

    }
}