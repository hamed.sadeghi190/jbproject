﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models.Temp_Model
{
    public class ExcelTempModel
    {
        public string StudentLastName { get; set; }

        public string StudentFirstName { get; set; }

        public string Domain { get; set; }

        public string ClassRequest { get; set; }

        public int Scholarship { get; set; }

        public decimal Payment { get; set; }

        public string Type { get; set; }

        public DateTime Date { get; set; }

        public string Discount1 { get; set; }

        public string Discount2 { get; set; }

        public string Discount3 { get; set; }

        public DateTime DOB { get; set; }

        public string Email { get; set; }

        public Gender Gender { get; set; }

        public string TuitionLable { get; set; }

        public bool Installment { get; set; }

        public decimal FullPrice { get; set; }

        public decimal TotalAmount { get; set; }

        public PlanOptions PlanOption { get; set; }
    }

    public class ChangeUserNameModel
    {
        public string OldUserName { get; set; }

        public string NewUserName { get; set; }
    }

    public enum Gender
    {
        Male = 0,
        Female = 1
    }

    public enum PlanOptions
    {
        Quarterly = 0,
        Semi = 1,
        NoInstallment = 2,
        Single = 3
    }
}