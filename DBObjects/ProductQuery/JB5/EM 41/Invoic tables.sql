CREATE TABLE [dbo].[Invoices](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClubId] [int] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[DueDate] [int] NULL,
	[UserId] [int] NOT NULL,
	[PaidAmount] [decimal](18, 2) NOT NULL,
	[InvoiceNumber] [bigint] NOT NULL,
	[Memo] [nvarchar](2000) NULL,
	[NotToRecipient] [nvarchar](4000) NULL,
	[EmailToCC] [nvarchar](256) NULL,
	[Reference] [nvarchar](max) NULL,
	[InvoicingDate] [datetime] NOT NULL DEFAULT ('1900-01-01T00:00:00.000'),
 CONSTRAINT [PK_dbo.Invoices] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON))
 ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[Invoices]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Invoices_dbo.Clubs_ClubId] FOREIGN KEY([ClubId])
REFERENCES [dbo].[Clubs] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Invoices] CHECK CONSTRAINT [FK_dbo.Invoices_dbo.Clubs_ClubId]
GO

ALTER TABLE [dbo].[Invoices]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Invoices_dbo.JbUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[JbUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Invoices] CHECK CONSTRAINT [FK_dbo.Invoices_dbo.JbUsers_UserId]
GO

CREATE TABLE [dbo].[InvoiceHistories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceId] [int] NULL,
	[ActionDate] [datetime] NOT NULL,
	[Action] [tinyint] NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.InvoiceHistories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON))
 ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[InvoiceHistories]  WITH CHECK ADD  CONSTRAINT [FK_dbo.InvoiceHistories_dbo.Invoices_InvoiceId] FOREIGN KEY([InvoiceId])
REFERENCES [dbo].[Invoices] ([Id])
GO

ALTER TABLE [dbo].[InvoiceHistories] CHECK CONSTRAINT [FK_dbo.InvoiceHistories_dbo.Invoices_InvoiceId]

go

Alter table [dbo].[PaymentDetails] alter column [InvoiceId] [int] NULL

go

update [dbo].[PaymentDetails]  set [InvoiceId] = null

go

ALTER TABLE [dbo].[PaymentDetails]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PaymentDetails_dbo.Invoices_InvoiceId] FOREIGN KEY([InvoiceId])
REFERENCES [dbo].[Invoices] ([Id])