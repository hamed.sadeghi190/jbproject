﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('SaleOverTimeController', ['$scope', '$http', 'jbToast', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, jbToast, $stateParams, $state, jbConfirmation) {
        window.app.globalObjects.pageClass = 'campaigns-view';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/Season';
        $scope.Model = {};
        var colors = ["#c2e4ff", "#8fcdff", "#5cb7ff", "#29a0ff", "#0088f5", "#006cc2", "#00508f", "#00335c"];

        $http.get(baseUrl + "/GetTotalSalesPieChart", { params: { seasonDomain: $scope.MC.stateParams.seasonDomain } })
            .success(function (data, status, headers, config) {
                $scope.Model.Totals = data;
            });

        $scope.dataSourceLinearChart = new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl + "/GetSaleOverTimeChartReport",
                    dataType: "json",
                    cache: false
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        return {
                            seasonDomain: $scope.MC.stateParams.seasonDomain,
                        }
                    }
                }
            },
            sort: {
                field: "SortDate",
                dir: "asc"
            }
        });


        $scope.linearChartOption = {
            dataSource: $scope.dataSourceLinearChart,
            title: {
                //text: "Sale over time. #= dataItem.Total #",
                template: "Sale over time. #= dataItem.Total #"
            },
            legend: {
                visible: true
            },
            chartArea: {
                background: "",
                //width: 800,
                //height: 500
            },
            seriesColors: colors,
            series: [{
                type: "line",
                //type: "area",
                style: "smooth",
                //style: "step",
                field: "Price",
                markers: {
                    visible: true
                },
                overlay: {
                    gradient: "none"
                },
                color: "#29506D"
            },
            {
                type: "area",
                style: "smooth",
                field: "Revenue",
                markers: {
                    visible: true
                },
                overlay: {
                    gradient: "none"
                },
                color: "#73c100"
            }],
            categoryAxis: {
                field: "Date",
                majorGridLines: {
                    visible: false
                },
                majorTicks: {
                    visible: true
                },
                labels: {
                    rotation: -75
                },
                crosshair: {
                    visible: true
                },
                justified: true
            },
            tooltip: {
                visible: true,
                template: "#= kendo.format('{0:C}',value) #"
            },
            valueAxis: {
                majorGridLines: {
                    visible: true
                },
                visible: true,
                labels: {
                    format: "$" + ("{0}")
                },
            }
        };


    }]);
})();