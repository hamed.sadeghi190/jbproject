﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using SportsClub.Models;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ProgramParentsEmailViewModel
    {
        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Subject")]
        [MaxLength(128)]
        public string Subject { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Body")]
        public string Body { get; set; }
     
        [EmailAddress]
        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Email")]
        [MaxLength(128)]
        public string From { get; set; }

        public List<SelectKeyValue<string>> TemplateNames { get; set; }

        [Required(ErrorMessage = "Please select one of email templates.")]
        [Display(Name = "Email Template")]
        public string SelectedTemplateId { get; set; }

        public EmailTemplateModel MailTemplate { get; set; }

        public EmailStyle EmailStyle { get; set; }

        public bool IsConfirmation { get; set; }
        public int ProgramId { get; set; }

        [EmailAddress]
        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Email")]
        [MaxLength(128)]
        public string TestEmail { get; set; }
    }
}