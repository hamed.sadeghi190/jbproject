
-- Remove OrderItem_Id from JBForms table

Alter table [JbForms] drop [FK_dbo.JbForms_dbo.OrderItems_OrderItem_Id]

Drop index [IX_OrderItem_Id] on [JbForms]

Alter table [JbForms] drop column [OrderItem_Id]