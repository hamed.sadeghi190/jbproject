﻿(function () {
    'use strict';

    angular.module('dashboardApp').service("couponUploadService", ['$state', '$http', 'jbToast', 'jbConfirmation', function ($state, $http, jbToast, jbConfirmation) {
        return function AddEditCoupon($scope, $http, $stateParams) {

            $scope.baseUrl = window.location.origin + "/Dashboard/Coupon";
            $scope.baseModel = null;

            $scope.MC.stateParams = $stateParams;
            $scope.couponId = 0;
            if ($stateParams.couponId)
                $scope.couponId = $stateParams.couponId;

            $http.get($scope.baseUrl + '/GetBaseData').success(function (data, status, headers, config) {
                $scope.baseModel = data;
                
            });

            $scope.eventDataSource = new kendo.data.DataSource({
                type: "json",
                serverFiltering: true,
                transport: {
                    read: {
                        url: "Dashboard/Program/GetListSelector",
                        cache: false,
                    }, parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                seasonDomain: $scope.MC.stateParams.seasonDomain,
                                term: document.activeElement.value
                            }
                        }
                    }
                }
            });

            $http.get($scope.baseUrl + '/UploadCouponsJson?id=' + $scope.couponId).success(function (data, status, headers, config) {             
                $scope.Model = data;
                $scope.Model.IsOneTimeUse = true;
            });

         
            $scope.eventsOptions = {
                placeholder: "Select programs...",
                dataTextField: "Title",
                dataValueField: "Value",
                dataSource: $scope.eventDataSource,
            };

            $scope.saveRow = function () {
                $scope.Model.SeasonDomain = $scope.MC.stateParams.seasonDomain;
                $http.post($scope.baseUrl + '/SaveUploadedList', { model: $scope.Model })
                    .success(function (e) {
                        if (e.Status == true) {

                            jbToast.success("Coupon file uploaded, you can see the results in the downloaded file.");
                        
                                var hiddenElement = document.createElement('a');
                                hiddenElement.href = window.location.origin + "/Dashboard/Coupon/DownloadResult/"+ e.fileId;
                                hiddenElement.target = '_blank';
                             
                                document.body.appendChild(hiddenElement);
                                hiddenElement.click();
                                document.body.removeChild(hiddenElement);
                           
                            $state.go('Coupons', { seasonDomain: $scope.MC.stateParams.seasonDomain, Status: true });
                          
                        }
                        else {
                            if (e.FormErrors[0] && e.FormErrors[0].Key == "model.CouponsLists") {
                                jbToast.error($scope.MC.joinStringsWithBreak(e.FormErrors[0].Messages));
                            }
                            $scope.MC.handleErrors($scope, e);
                        }
                    });
            };

        };
    }]);
})();