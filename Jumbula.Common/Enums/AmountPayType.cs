﻿namespace Jumbula.Common.Enums
{
    public enum AmountPayType
    {
        FullPay,
        PartiallyPay
    }
}
