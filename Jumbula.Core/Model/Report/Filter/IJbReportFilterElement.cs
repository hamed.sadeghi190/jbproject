﻿using Jumbula.Common.Enums;

namespace Jumbula.Core.Model.Report.Filter
{
    public interface IJbReportFilterElement
    {
        int Id { get; set; }
        ReportFilterElementName Name { get; set; }
        string SectionName { get; set; }
        string Title { get; set; }
        ReportFilterType Type { get; set; }
        ReportFilterAction? ActionFilter { get; set; }
        ReportFilterElementName? TargetFilter { get; set; }
        int ShowByFilterId { get; set; }
        string ShowByValue { get; set; }
        bool IsRequired { get; set; }
        string ParameterName { get; set; }
        object ParameterValue { get; set; }
        bool IsBr { get; set; }
        bool IsHr { get; set; }
        string Width { get; set; }
        string FontWeight { get; set; }
    }
}
