﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('subscriptionAttendanceReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;


        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        $scope.Model = customReportService.getModelFromStrorage();

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.InfoModel = {};
        var timer = false;
        $scope.IsRunReport = false;

        $scope.Model.Sorts = [{ Value: "Participant", Title: "Participant" }, { Value: "Grade", Title: "Grade" }];
        $scope.Model.SortBy = "Participant";

        $http.get(baseUrl + '/GetAllSeasonPrograms', { params: { seasonDomain: $scope.stateParams.seasonDomain, reportName: "SubscriptionAttendance" } }).success(function (data, status, headers, config) {  ///GetClubSeasonsProvider delete
            $scope.AllPrograms = data.AllPrograms;
            $scope.Model.ProgramId = null;

        }).error(function (data, status, headers, config) {
            jbToast.error(data.Message);
        });

        $scope.updateChosenScope = function () {
            $("select[chosen]").trigger('chosen:updated');
        };

        $scope.$watch('AllPrograms', function () {
            if (timer) {
                $timeout.cancel(timer);
            }
            timer = $timeout(function () {
                $scope.updateChosenScope();
            }, 1500);
        });


        $scope.runReport = function () {

            if ($scope.Model._SelectedPrograms == null || $scope.Model._SelectedPrograms.length === 0) {
                jbToast.info("Please select a program");
                return;
            }
            else if ($scope.Model.DateProgram == null || $scope.Model.DateProgram === "") {
                jbToast.info("Please select start date");
                return;
            }

            $scope.IsRunReport = true;


            $http.get(baseUrl + '/GetSubscriptionAttendanceReportDetail',
                { params: { programIds: $scope.Model._SelectedPrograms, startDate: $scope.Model.DateProgram, sortBy: $scope.Model.SortBy } })
                .success(function (data, status, headers, config) {

                    $scope.InfoModel = data;

                }).error(function (data, status, headers, config) {
                    jbToast.error("error " + data);
                });

        }

        $scope.saveToPDF = function () {

            $http({
                url: baseUrl + '/GenerateAttendanceReportPdf',
                method: "POST",
                data:
                    {
                        programIds: $scope.Model._SelectedPrograms,
                        startDate: $scope.Model.DateProgram,
                        sortBy: $scope.Model.SortBy
                    },
                headers: {
                    'Content-type': 'application/json'
                },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers, config) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                var blob = new Blob([data], { type: "application/pdf" });

                var fileURL = URL.createObjectURL(blob);
                a.href = fileURL;
                a.target = '_self';

                a.download = "Attendance roster.pdf";
                a.click();
            });
        }
    }
    ]);

})();