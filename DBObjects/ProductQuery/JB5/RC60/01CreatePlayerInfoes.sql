
CREATE TABLE [dbo].[PlayerInfoes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Allergies] [nvarchar](max) NULL,
	[SpecialNeeds] [nvarchar](max) NULL,
	[HasPermissionPhotography] [bit] NULL DEFAULT ((0)),
	[SchoolName] [nvarchar](256) NULL,
	[HomeRoom] [nvarchar](64) NULL,
	[Grade] [int] NOT NULL DEFAULT ((0)),
	[SelfAdministerMedication] [bit] NULL,
	[SelfAdministerMedicationDescription] [nvarchar](max) NULL,
	[NutAllergy] [bit] NULL,
	[DoctorName] [nvarchar](256) NULL,
	[DoctorPhone] [nvarchar](64) NULL,
	[DoctorLocation] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.PlayerInfoes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


