﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;
using Jumbula.Core.Business;

namespace Jumbula.Core.Domain
{
    public class CatalogItem : BaseEntity, IProtectedResource
    {
        public CatalogItem()
        {
            Categories = new List<Category>();
        }

        [StringLength(200)]
        public string Name { get; set; }

        public string Description { get; set; }

        public AttendeeRestriction AttendeeRestriction { get; set; }

        public string ImageFileName { get; set; }

        public string MaterialsNeeded { get; set; }

        public int? MinimumEnrollment { get; set; }

        public int? MaximumEnrollment { get; set; }

        public int? ISRatio { get; set; }

        public MetaData MetaData { get; set; }

        public string Detail { get; set; }

        public CatalogStatus Status { get; set; }

        public virtual ICollection<Category> Categories { get; set; }

        [ForeignKey("ClubId")]
        public virtual Club Club { get; set; }

        public int ClubId { get; set; }
        public byte Rating { get; set; }

        public virtual ICollection<CatalogItemOption> CatalogItemOptions { get; set; }

        public virtual ICollection<CatalogItemPriceOption> PriceOptions { get; set; }
        public virtual ICollection<Program> Programs { get; set; }

        public int? ImageGallery_Id { get; set; }
        [ForeignKey("ImageGallery_Id")]
        public virtual ImageGallery ImageGallery { get; set; }

        public bool DoesTheClubOwnIt(int clubId)
        {
            if(ClubId == clubId || (Club.PartnerId.HasValue && Club.PartnerId == clubId))
            {
                return true;
            }

            return false;
        }

        public bool DoesTheParentOwnIt(int userId)
        {
            return false;
        }
    }

    public enum CatalogStatus : byte
    {
        Active = 0,
        Archived = 1,
        Hidden = 2,
        Deleted = 3
    }
}
