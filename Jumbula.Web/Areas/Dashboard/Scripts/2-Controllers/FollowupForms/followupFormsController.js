﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('followupFormsController', ['$scope', '$http', 'cfpLoadingBar', '$stateParams', '$state', 'jbToast', 'jbRandom', 'programService', 'formService', 'jbConfirmation', function ($scope, $http, cfpLoadingBar, $stateParams, $state, jbToast, jbRandom, programService, formService, jbConfirmation) {
        window.app.globalObjects.pageClass = 'form-view';
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();

        $scope.MC.stateParams = $stateParams;
        $scope.MC.IsFormUploadRename = false;

        $scope.programId = $stateParams.programId;
        //console.log($scope.programId)

        $scope.waiverDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: window.location.origin + "/Dashboard/Club/GetFormTemplatesPaging",
                        type: "POST"
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: data.sort
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverFiltering: true
        });

        $scope.followUpGridOptions = {
            dataSource: $scope.waiverDataSource,
            sortable: true,
            columns: [
                {
                    field: "Title",
                    title: "Name",
                    width: "250px",
                    template: "<strong class='grid-row-title'>#= Title #</strong>"
                },
                {
                    field: "Type",
                    title: "Type",
                    width: "100px",
                    template: "#= Type # form"
                },
                {
                    field: "StrFollowUpFormMode",
                    title: "Mode",
                    width: "100px"
                },
                {
                    field: "DateCreated",
                    title: "Created",
                    width: "160px",
                    template: "#= kendo.toString(kendo.parseDate(DateCreated), 'MMM dd, yyyy') #"
                },
                {
                    title: "Actions",
                    width: "70px",
                    headerTemplate: "<span class='actions-title'>Actions</span>",
                    template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                        <li> ... \
                            <ul>\
                                  <li ng-show='#=Type==\'Supplemental\'#' ui-sref='SupplementalFormView({seasonDomain: $scope.MC.stateParams.seasonDomain , formId: #=Id#})'>\
                                        <span class='k-link jbi-show'>View</span>\
                                  </li>\
                                  <li ng-show='#=Type!=\'Supplemental\'#' ng-click=Edit(#:Id#,\"#:Type#\")>\
                                        <span class='k-link jbi-pencil'>Edit</span>\
                                  </li>\
                                  <li ng-show='#=Type!=\'Supplemental\' && FollowUpFormMode==\'Offline\' #' ng-click=ChangeFollowUpFormMode(#:Id#,\"#:FollowUpFormMode#\")>\
                                        <span class='k-link jbi-forget-pass'>Switch mode to during registration</span>\
                                  </li>\
                                  <li ng-show='#=Type!=\'Supplemental\' && FollowUpFormMode==\'Online\' #' ng-click=ChangeFollowUpFormMode(#:Id#,\"#:FollowUpFormMode#\")>\
                                        <span class='k-link jbi-forget-pass'>Switch mode to after registration</span>\
                                  </li>\
                                  <li ng-click='Delete(#:Id#)'>\
                                        <span class='k-link jbi-remove'>Delete</span>\
                                  </li>\
                            </ul>\
                        </li>\
                    </ul>"
                }]
        };

        $scope.Save = function () {
            if ($scope.programId == undefined) {

                $state.go("ClubForms");
            }
            else {
                var formModel = formService.getModel();

                $http.get('Dashboard/club/GetFormTemplates', { params: { formType: 'FollowUp' } }).success(
                    function (data) {

                        formModel.AllFollowUpForms = data;

                        formService.setModel(formModel);

                        $state.back();
                    });
            }
        }

        $scope.Edit = function (id, type) {

            if (type === 'Online') {
                $state.forward('Form', { seasonDomain: $scope.MC.stateParams.seasonDomain, formType: "FollowUp", templateId: id }, 'FollowUpForms', { programId: $scope.MC.stateParams.programId }
                    , function () {

                    });
            } else {
                $scope.MC.IsFormUploadRename = true;
                $state.go("FormUpload", { programId: $scope.MC.stateParams.programId });
            }
        }

        $scope.Upload = function () {

            $state.forward('FormUpload', { templateId: 0 }, 'FollowUpForms', { programId: $scope.MC.stateParams.programId }
                , function () {

                });
        }

        $scope.Add = function () {

            $state.forward('Form', { seasonDomain: $scope.MC.stateParams.seasonDomain, formType: "FollowUp", templateId: 0 }, 'FollowUpForms', { programId: $scope.MC.stateParams.programId }
                , function () {

                });
        }

        $scope.Delete = function (id) {

            $http.post('/Dashboard/Form/AssignedTemplateToPrograms', { id: id })
                .success(function (data) {
                    if (data.Status) {

                        jbToast.warning("The form you want to delete has been assigned to the following programs. Please undo all the assignments first. " + data.Message);
                    } else {

                        jbConfirmation
                            .yes(function () {

                                $http.post('/Dashboard/Club/DeleteFormTemplate', { id: id })
                                    .success(function (data) {
                                        if (data.Status) {
                                            $scope.followUpGridOptions.dataSource.read();
                                        }
                                        else {
                                            $scope.MC.handleErrors($scope, data);
                                        }
                                    });
                            })
                            .confirm();
                    }
                });
        }

        $scope.ChangeFollowUpFormMode = function (id, currentMode) {

            if (currentMode === "Offline") {
                jbConfirmation
                    .yes(function () {

                        $http.post('/Dashboard/Form/ChangeFollowUpFormMode', { id: id })
                            .success(function (data) {
                                if (data.Status) {
                                    $scope.followUpGridOptions.dataSource.read();
                                }
                                else {
                                    $scope.MC.handleErrors($scope, data);
                                }
                            });
                    })
                    .message('Are you sure you want users to fill out this form during the registration flow instead of after the registration is completed? Note: This option only works for multi registration--it does not work for single registration.')
                    .confirm();
            } else {
                jbConfirmation
                    .yes(function () {

                        $http.post("/Dashboard/Form/ChangeFollowUpFormMode", { id: id })
                            .success(function (data) {
                                if (data.Status) {
                                    $scope.followUpGridOptions.dataSource.read();
                                }
                                else {
                                    $scope.MC.handleErrors($scope, data);
                                }
                            });
                    })
                    .message("Are you sure you want users to fill out this form after the registration is completed instead of during the registration flow?")
                    .confirm();
            }
        }

        if ($state.returnByBack()) {
            $scope.MC.backQueueItems.pop();
        }
    }]);
})();