﻿using System;
using System.Security.Claims;
using System.Security.Principal;
using Jumbula.Core.Identity;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace Jumbula.Web.WebLogic.UserService
{
    public static class IdentityAdditionalExtensions
    {
        public static TResult GetAdditionalData<TResult>(this IIdentity identity)
            where TResult : IAdditionalData
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("AdditionalData");

            var result = JsonConvert.DeserializeObject<TResult>(claim.Value);

            return result;
        }

        public static int GetCurrentUserId(this IIdentity identity)
        {
            var currentUserId = identity.GetUserId<int>();

            return currentUserId;
        }

        public static bool IsCurrentUserExist(this IIdentity identity)
        {
            try
            {
                return identity.GetCurrentUserId() > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
