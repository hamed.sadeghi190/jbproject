------ Update User Table
Go
alter TABLE [dbo].[webpages_UsersInRoles] add [ClubId] [int]  NULL
Go
ALTER TABLE [dbo].[webpages_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [fk_ClubId] FOREIGN KEY([ClubId])
REFERENCES [dbo].[Clubs] (Id)
Go
update [dbo].[webpages_UsersInRoles] set ClubId=(select top(1) ClubId from ClubUsers where UserId=[dbo].[webpages_UsersInRoles].UserId) where UserId in (select UserId from ClubUsers)
Go
update [dbo].[webpages_UsersInRoles] set ClubId=(select top(1) id from clubs where UserId=[dbo].[webpages_UsersInRoles].UserId) where ClubId is null 
Go
ALTER TABLE [dbo].[webpages_UsersInRoles] DROP CONSTRAINT [PK__webpages__AF2760ADBBAA93CC]
go

ALTER TABLE [dbo].[webpages_UsersInRoles]  
ADD id int IDENTITY;
GO


ALTER TABLE [dbo].[webpages_UsersInRoles]  
ADD CONSTRAINT PK_dbo_webpages_UsersInRoles PRIMARY KEY CLUSTERED  (id);
GO
------ Add county postal Address
alter TABLE [dbo].[PostalAddresses] add [County] [nvarchar](64) NULL
Go

------- Clean up DB
drop table Coaches
go

drop table VendorClubs
go



ALTER TABLE [dbo].[EventForms] DROP CONSTRAINT [FK_dbo.EventForms_dbo.Tourneys_Tourney_Id]
go
alter table eventforms drop column Tourney_Id
go
DROP INDEX [IX_Tourney_Id] ON [dbo].[Notes]
go
DROP INDEX [IX_AffiliatorID] ON [dbo].[Clubs]
go
ALTER TABLE [dbo].[Clubs] DROP CONSTRAINT [FK_dbo.Clubs_dbo.Clubs_AffiliatorID]
go
alter table dbo.Clubs  drop column [Geography]
go
alter table dbo.Clubs  drop column CoverImage
go
alter table dbo.Clubs  drop column ExternalId
go
alter table dbo.Clubs  drop column MeetUpTimes
go
ALTER TABLE [dbo].[Clubs] DROP CONSTRAINT [DF__Clubs__ExternalT__6FE99F9F]
go
ALTER TABLE [dbo].[Clubs] DROP CONSTRAINT [DF__Clubs__IsAffilia__7CF981FA]
go
alter table dbo.Clubs  drop column ExternalType
go
alter table dbo.Clubs  drop column PaymentAppStatus
go
alter table dbo.Clubs  drop column ClubFeeRateCode
go
alter table dbo.Clubs  drop column IsAffiliator
go
alter table dbo.Clubs  drop column AffiliatorID
go
alter table dbo.Clubs  drop column Announce
go
alter table dbo.clubs drop column OraganizationType
go
ALTER TABLE [dbo].[Notes] DROP CONSTRAINT [FK_dbo.Notes_dbo.Tourneys_Tourney_Id]
go
alter table  [dbo].[Notes] drop column Tourney_Id
go

drop table ClubSubDomains
go
drop table ClubImports
go

DROP TABLE [dbo].[TourneyCoverages]
go
drop table TourneySchedules
go
drop table TourneySections
go
drop table RestrictedTourneys
go
drop table News
go
drop table Tourneys
go


------Add partner Id

alter TABLE [dbo].[Clubs] add [PartnerId] [int] NULL 


go

------ Add is deleted
alter TABLE [dbo].[Clubs] add [IsDeleted] [bit] NOT NULL CONSTRAINT [DF__Clubs__IsDeleted__59511E61] DEFAULT ((0))
go
------- Add type table

CREATE TABLE [dbo].[ClubTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[ParentId] [int] NULL,
 CONSTRAINT [PK_dbo.ClubTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[ClubTypes]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClubTypes_dbo.ClubTypes_ParentId] FOREIGN KEY([ParentId])
REFERENCES [dbo].[ClubTypes] ([Id])
GO

ALTER TABLE [dbo].[ClubTypes] CHECK CONSTRAINT [FK_dbo.ClubTypes_dbo.ClubTypes_ParentId]
GO

----- Insert into clubtype

INSERT INTO [dbo].[ClubTypes]
           ([Name]
           ,[ParentId])
     VALUES
           ('SportClub'
           ,null)
go
INSERT INTO [dbo].[ClubTypes]
           ([Name]
           ,[ParentId])
     VALUES
           ('School'
           ,null)
go
INSERT INTO [dbo].[ClubTypes]
           ([Name]
           ,[ParentId])
     VALUES
           ('Provider'
           ,null)

go


INSERT INTO [dbo].[ClubTypes]
           ([Name]
           ,[ParentId])
     VALUES
           ('Individual'
           ,3)

		   go
INSERT INTO [dbo].[ClubTypes]
           ([Name]
           ,[ParentId])
     VALUES
           ('Corp'
           ,3)
		   go

INSERT INTO [dbo].[ClubTypes]
           ([Name]
           ,[ParentId])
     VALUES
           ('Public'
           ,2)
go
		   INSERT INTO [dbo].[ClubTypes]
           ([Name]
           ,[ParentId])
     VALUES
           ('Private'
           ,2)

go
		   INSERT INTO [dbo].[ClubTypes]
           ([Name]
           ,[ParentId])
     VALUES
           ('Title1'
           ,2)

go

----- Add club type 
alter TABLE [dbo].[Clubs] add [TypeId] [int] NULL
go

update Clubs set TypeId=1

go
alter table [dbo].[Clubs] alter column [TypeId] [int] Not NULL
go

ALTER TABLE [dbo].[Clubs]  WITH CHECK ADD  CONSTRAINT [FK_Clubs_ClubTypes] FOREIGN KEY([TypeId])
REFERENCES [dbo].[ClubTypes] ([Id])
GO

----  Add setting to club

alter TABLE [dbo].[Clubs] add [Setting] [nvarchar](1024) NULL

go
-- ADD roles to role
INSERT INTO [dbo].[webpages_Roles]
           ([RoleName]
           ,[RoleTitle])
     VALUES
           ('Partner'
           ,'Partner')
GO
INSERT INTO [dbo].[webpages_Roles]
           ([RoleName]
           ,[RoleTitle])
     VALUES
           ('Instructor'
           ,'Instructor')
GO



