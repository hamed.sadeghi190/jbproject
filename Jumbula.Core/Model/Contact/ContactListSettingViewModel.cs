﻿

namespace Jumbula.Core.Model.Contact
{
    public class ContactListSettingViewModel
    {
        public bool ShowEmail { get; set; }
        public bool ShowWorkPhone { get; set; }
        public bool ShowCellPhone { get; set; }
        public bool ShowNickname { get; set; }
        public bool ShowPhone { get; set; }
        public bool ShowTitle { get; set; }
        public bool ShowDoB { get; set; }
        public bool ShowOccupation { get; set; }
        public bool ShowEmployer { get; set; }
    }
}
