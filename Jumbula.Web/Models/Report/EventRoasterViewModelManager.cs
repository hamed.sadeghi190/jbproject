﻿using System.Collections.Generic;
using System.Linq;
using Jb.Framework.Common.Forms;
using SportsClub.Domain;
using SportsClub.Infrastructure.Helper;

namespace SportsClub.Models
{
    public class EventRoasterViewModelManager
    {
        public static IEnumerable<EventRoasterViewModel> ToEventRoasterViewModel(IEnumerable<EventRoaster> eventRoasters)
        {
            return eventRoasters.Select(er => new EventRoasterViewModel
                {
                    Id = er.Id,
                    Name = er.Name,
                    Description = er.Description
                });
        }

        public static EventRoasterViewModel ToEventRoasterViewModel(EventRoaster eventRoaster)
        {
            return new EventRoasterViewModel() { Name = eventRoaster.Name, Description = eventRoaster.Description };
        }

        public static EventRoasterReportViewModel ToEventRoasterReportViewModel(EventRoaster eventRoaster, string domain, bool forOldEvent = false)
        {
            var model = new EventRoasterReportViewModel(domain, forOldEvent)
                {
                    Id = eventRoaster.Id,
                    Name = eventRoaster.Name,
                    Description = eventRoaster.Description,
                    JbForm = eventRoaster.JbForm,
                    SelectedEvents = eventRoaster.Events != null ? eventRoaster.Events.Split(',').ToList() : null,
                    SeasonId = eventRoaster.SeasonId,
                    Type = eventRoaster.Type
                };

            return model;
        }

        public static EventRoaster ToEventRoaster(EventRoasterReportViewModel eventRoasterViewModel)
        {
            var evenetRoaster = new EventRoaster()
                {
                    Id = eventRoasterViewModel.Id,
                    Name = eventRoasterViewModel.Name,
                    Description = eventRoasterViewModel.Description,
                    JbForm = eventRoasterViewModel.JbForm,
                    JbForm_Id = eventRoasterViewModel.JbForm.Id,
                    SeasonId = eventRoasterViewModel.SeasonId,
                    Type = eventRoasterViewModel.Type
                };
            evenetRoaster.Events = string.Join(",", eventRoasterViewModel.SelectedEvents.Select(ev => ev.Replace(",", "")));

            //evenetRoaster.JbForm =new JbForm(){JsonElements = } SerializeHelper.Serialize(eventRoasterViewModel.ReportForm).ToString();}
            return evenetRoaster;
        }
    }
}