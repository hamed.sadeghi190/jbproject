﻿using Jumbula.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Data.DbConfigs
{
    public class UserPaymentPlanConfig : EntityTypeConfiguration<UserPaymentPlan>
    {
        public UserPaymentPlanConfig()
        {
            HasRequired(i => i.PaymentPlan).WithMany(c => c.Users).HasForeignKey(i => i.PaymentPlanId);

        }
    }
}
