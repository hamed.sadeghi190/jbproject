﻿using System.Linq;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element
{
    class GovermentSubsidyElements : BaseElements
    {
        public GovermentSubsidyElements(IWebDriver driver)
            : base(driver)
        {

        }
        #region Header
        public IWebElement BackLink
        {
            get
            {
                return Driver.FindElement(By.XPath("//a[@class='icon-arrow-left']"));
            }
        }
        public IWebElement PageTitle
        {
            get
            {
                return Driver.FindElement(By.XPath("//div[@class='title']/h1[1]"));
            }
        }

        public IWebElement AddSubsidyLink
        {
            get
            {
                return Driver.FindElement(By.XPath("//a[@data-original-title='Add subsidy']"));
            }
        }
        #endregion Header

        #region Filtering
        public IWebElement SearchTerm
        {
            get
            {
                return Driver.FindElement(By.Id("searchClubName"));
            }
        }
        public IWebElement SearchButton
        {
            get
            {
                return Driver.FindElement(By.Id("btn_search"));
            }
        }

        public IWebElement ClearFilterLink
        {
            get
            {
                return Driver.FindElement(By.Id("btn_clear"));
            }
        }
        #endregion Filtering

        #region GridRow1
        public bool Row1Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[1]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row1Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[1]/td[1]/span"));
            }
        }
        public IWebElement Row1State
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[1]/td[2]/span"));
            }
        }
        public IWebElement Row1Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[1]/td[3]/span"));
            }
        }
        public IWebElement Row1Phone
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[1]/td[4]/span"));
            }
        }
        public IWebElement Row1Website
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[1]/td[5]/span"));
            }
        }
        public IWebElement Row1Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[1]/td[6]/ul/li/span"));
            }
        }
        public IWebElement Row1ViewEditAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[1]/td[6]/ul/li/div/ul/li/span"));
            }
        }

        #endregion GridRow1

        #region GridRow2
        public bool Row2Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[2]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row2Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[2]/td[1]/span"));
            }
        }
        public IWebElement Row2State
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[2]/td[2]/span"));
            }
        }
        public IWebElement Row2Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[2]/td[3]/span"));
            }
        }
        public IWebElement Row2Phone
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[2]/td[4]/span"));
            }
        }
        public IWebElement Row2Website
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[2]/td[5]/span"));
            }
        }
        public IWebElement Row2Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[2]/td[6]/ul/li/span"));
            }
        }
        public IWebElement Row2ViewEditAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[2]/td[6]/ul/li/div/ul/li/span"));
            }
        }

        #endregion GridRow2

        #region GridRow3
        public bool Row3Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[3]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row3Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[3]/td[1]/span"));
            }
        }
        public IWebElement Row3State
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[3]/td[2]/span"));
            }
        }
        public IWebElement Row3Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[3]/td[3]/span"));
            }
        }
        public IWebElement Row3Phone
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[3]/td[4]/span"));
            }
        }
        public IWebElement Row3Website
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[3]/td[5]/span"));
            }
        }
        public IWebElement Row3Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[3]/td[6]/ul/li/span"));
            }
        }
        public IWebElement Row3ViewEditAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[3]/td[6]/ul/li/div/ul/li/span"));
            }
        }

        #endregion GridRow3

        #region GridRow4
        public bool Row4Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[4]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row4Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[4]/td[1]/span"));
            }
        }
        public IWebElement Row4State
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[4]/td[2]/span"));
            }
        }
        public IWebElement Row4Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[4]/td[3]/span"));
            }
        }
        public IWebElement Row4Phone
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[4]/td[4]/span"));
            }
        }
        public IWebElement Row4Website
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[4]/td[5]/span"));
            }
        }
        public IWebElement Row4Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[4]/td[6]/ul/li/span"));
            }
        }
        public IWebElement Row4ViewEditAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[4]/td[6]/ul/li/div/ul/li/span"));
            }
        }

        #endregion GridRow4

        #region GridRow5
        public bool Row5Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[5]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row5Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[5]/td[1]/span"));
            }
        }
        public IWebElement Row5State
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[5]/td[2]/span"));
            }
        }
        public IWebElement Row5Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[5]/td[3]/span"));
            }
        }
        public IWebElement Row5Phone
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[5]/td[4]/span"));
            }
        }
        public IWebElement Row5Website
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[5]/td[5]/span"));
            }
        }
        public IWebElement Row5Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[5]/td[6]/ul/li/span"));
            }
        }
        public IWebElement Row5ViewEditAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[5]/td[6]/ul/li/div/ul/li/span"));
            }
        }

        #endregion GridRow5

        #region GridRow6
        public bool Row6Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[6]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row6Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[6]/td[1]/span"));
            }
        }
        public IWebElement Row6State
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[6]/td[2]/span"));
            }
        }
        public IWebElement Row6Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[6]/td[3]/span"));
            }
        }
        public IWebElement Row6Phone
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[6]/td[4]/span"));
            }
        }
        public IWebElement Row6Website
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[6]/td[5]/span"));
            }
        }
        public IWebElement Row6Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[6]/td[6]/ul/li/span"));
            }
        }
        public IWebElement Row6ViewEditAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[6]/td[6]/ul/li/div/ul/li/span"));
            }
        }

        #endregion GridRow6

        #region AddSubsidyElement
        public IWebElement Name
        {
            get
            {
                return Driver.FindElement(By.Id("Model.Name"));
            }
        }
        public IWebElement State
        {
            get
            {
                return Driver.FindElement(By.Id("SelectState"));
            }
        }
        public IWebElement NewYorkState
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='New York']"));
            }
        }
        public IWebElement Phone
        {
            get
            {
                return Driver.FindElement(By.Id("Model.Phone"));
            }
        }
        public IWebElement FirstName
        {
            get
            {
                return Driver.FindElement(By.Id("Model.FirstName"));
            }
        }
        public IWebElement LastName
        {
            get
            {
                return Driver.FindElement(By.Id("Model.LastName"));
            }
        }
        public IWebElement Email
        {
            get
            {
                return Driver.FindElement(By.Id("Model.Email"));

            }
        }
        public IWebElement Website
        {
            get
            {
                return Driver.FindElement(By.Id("btn_website"));
            }
        }
        public IWebElement Instructions
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@ng-model='Model.Instructions']"));
            }
        }

        public IWebElement Submit
        {
            get
            {
                return Driver.FindElement(By.Id("btn_save"));
            }
        }

        #endregion AddDistrictElement
    }

}
