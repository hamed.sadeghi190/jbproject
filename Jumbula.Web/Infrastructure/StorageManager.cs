﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Jumbula.Common.Helper;
using Jumbula.Web.Mvc.WebConfig;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using SportsClub.Models.Temp_Model;

namespace Jumbula.Web.Infrastructure
{
    public class StorageManager
    {
        private const string DirectoryDelimeter = "/";

        private readonly CloudStorageAccount _account;
        private readonly CloudBlobClient _blobClient;
        private readonly CloudBlobContainer _blobContainer;

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// Should set Microsoft.WindowsAzure.Storage.RetryPolicies??
        /// </remarks>
        /// <param name="connectionString"></param>
        public StorageManager(string connectionString, string containerName)
        {
            try
            {
                _account = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting(connectionString));
                _blobClient = _account.CreateCloudBlobClient();
                _blobContainer = _blobClient.GetContainerReference(containerName);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get an instance of the CloudStorageAccount
        /// </summary>
        /// <returns></returns>
        public CloudStorageAccount GetAccountInstance()
        {
            return _account;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// Assumes default container creation is private access
        /// </remarks>
        /// <param name="containerName"></param>
        /// <param name="publicAccess"></param>
        public static void CreateContainer(string connectionString, string containerName, bool publicAccess)
        {
            try
            {
                var account = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting(connectionString));
                var blobClient = account.CreateCloudBlobClient();
                var blobContainer = blobClient.GetContainerReference(containerName);

                blobContainer.CreateIfNotExists();

                if (publicAccess == true)
                {
                    blobContainer.SetPermissions(
                        new BlobContainerPermissions
                        {
                            PublicAccess = BlobContainerPublicAccessType.Blob
                        });
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool CreateContainerIfNotExist(string containerName, bool publicAccess = false)
        {
            try
            {
                var blobContainer = _blobClient.GetContainerReference(containerName);

                var result = blobContainer.CreateIfNotExists();

                if (publicAccess == true)
                {
                    blobContainer.SetPermissions(
                        new BlobContainerPermissions
                        {
                            PublicAccess = BlobContainerPublicAccessType.Blob
                        });
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DoesContainerExist(string connectionString, string containerName)
        {
            bool result = false;
            var account = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting(connectionString));
            var blobClient = account.CreateCloudBlobClient();

            try
            {
                IEnumerable<CloudBlobContainer> containers = blobClient.ListContainers();
                result = containers.Any(x => x.Name == containerName);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public static Uri GetUri(string containerName, string directoryName, string blobName)
        {
            string path = containerName + DirectoryDelimeter + directoryName + DirectoryDelimeter + blobName;
            return GetUri(path);
        }
        public static Uri GetUriByPath(string path)
        {
            return GetUri(path);
        }

        /// <summary>
        /// Implented for Hassan for his image gallery :)
        /// </summary>
        /// <param name="containerName"></param>
        /// <param name="parentDirectoryName"></param>
        /// <param name="childDirectoryName"></param>
        /// <param name="blobName"></param>
        /// <returns></returns>
        public static Uri GetUri(string containerName, string parentDirName, string childDirName, string blobName)
        {
            string path = containerName + DirectoryDelimeter + parentDirName + DirectoryDelimeter + childDirName + DirectoryDelimeter + blobName;
            return GetUri(path);
        }

        public static string PathCombine(string directoryName, string blobName)
        {
            return directoryName + DirectoryDelimeter + blobName;
        }

        public static string PathCombine(string parentDirName, string childDirName, string blobName)
        {
            return parentDirName + DirectoryDelimeter + childDirName + DirectoryDelimeter + blobName;
        }

        public void UploadBlob(string blobName, Stream stream, string contentType)
        {
            try
            {
                CloudBlockBlob blob = _blobContainer.GetBlockBlobReference(blobName);
                blob.Properties.ContentType = contentType;
                blob.UploadFromStream(stream);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param n ame="directoryName">
        /// directoryName can be a path relative to the container, eg bayareachess or bayareachess/tourney
        /// Use the PathCombine to build the path
        /// </param>
        /// <param name="blobName"></param>
        /// <param name="stream"></param>
        /// <param name="contentType"></param>
        public void UploadBlob(string directoryName, string blobName, Stream stream, string contentType)
        {

            UploadBlob(PathCombine(directoryName, blobName), stream, contentType);
        }

        public void UploadBlob(string parentDirName, string childDirName, string blobName, Stream stream, string contentType)
        {
            var res = CreateContainerIfNotExist(parentDirName, true);

            UploadBlob(PathCombine(parentDirName, childDirName, blobName), stream, contentType);
        }

        public void DeleteBlob(string parentDirName, string childDirName, string blobName)
        {
            try
            {
                var path = PathCombine(parentDirName, childDirName, blobName);
                DeleteBlob(path);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteBlob(string parentDirName, string blobName)
        {
            try
            {
                var path = PathCombine(parentDirName, blobName);
                DeleteBlob(path);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteBlob(string blobPath)
        {
            try
            {
                CloudBlockBlob blob = _blobContainer.GetBlockBlobReference(blobPath);
                blob.Delete();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Uploads the source directory under folderName within the container
        /// </summary>
        /// <param name="sourceDirectory"></param>
        /// <param name="folerName"></param>
        public void UploadDirectory(string sourceDirectory, string folerName)
        {
            try
            {
                var folder = new DirectoryInfo(sourceDirectory);

                foreach (var file in folder.GetFiles())
                {
                    string blobName = file.Name;

                    if (!string.IsNullOrEmpty(folerName))
                    {
                        blobName = PathCombine(folerName, blobName);
                    }

                    using (Stream stream = file.OpenRead())
                    {
                        UploadBlob(blobName, stream, FileHelper.GetMimeTypeFromExtension(file.Extension));
                    }
                }

                foreach (DirectoryInfo dir in folder.GetDirectories())
                {
                    var prefix = dir.Name;

                    if (!string.IsNullOrEmpty(folerName))
                    {
                        prefix = PathCombine(folerName, prefix);
                    }

                    UploadDirectory(dir.FullName, prefix);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Returns a list of URI's of all the page and blocl blobs within a parent child direcotry
        /// </summary>
        /// <param name="parentDirName">
        /// Parent directory must lie directrly under the container.
        /// </param>
        /// <param name="childDirName"></param>
        /// <returns></returns>
        public Uri[] ListDirectoryBlobFiles(string parentDirName, string childDirName)
        {
            string path = parentDirName + DirectoryDelimeter + childDirName + DirectoryDelimeter;

            return ListDirectoryBlobFiles(path);
        }

        public object[] ListObjectsInDirectory(string parentDirName)
        {
            string path = parentDirName;
            if (path.Last() != '/')
            {
                path += DirectoryDelimeter;
            }
            return ListBlobFilesForKendo(path);
        }

        /// <summary>
        /// Returns a list of URI's of all the page and blocl blobs within a parent child direcotry
        /// </summary>
        /// <param name="parentDirName">
        /// Parent directory must lie directrly under the container.
        /// </param>
        /// <param name="childDirName"></param>
        /// <returns></returns>
        public Uri[] ListDirectoryBlobFiles(string parentDirName, string childDirName, string grandChildDirName)
        {
            string path = parentDirName + DirectoryDelimeter + childDirName + DirectoryDelimeter + grandChildDirName + DirectoryDelimeter;

            return ListDirectoryBlobFiles(path);
        }


        private object[] ListBlobFilesForKendo(string path)
        {
            var files = new HashSet<object>();

            foreach (IListBlobItem item in _blobContainer.ListBlobs(path, false))
            {
                if (item.GetType() == typeof(CloudBlockBlob))
                {
                    CloudBlockBlob blob = (CloudBlockBlob)item;
                    files.Add(new
                    {
                        name = Path.GetFileName(blob.Uri.LocalPath),
                        size = blob.Properties.Length,
                        type = "f"
                    });
                }
                else if (item.GetType() == typeof(CloudPageBlob)) // do nothing
                {

                }
                else if (item.GetType() == typeof(CloudBlobDirectory))
                {
                    CloudBlobDirectory blob = (CloudBlobDirectory)item;

                    var charactersArray = blob.Uri.Segments.Last().Where(p => p != '/').ToArray();

                    string directoryName = "";
                    if (charactersArray != null)
                    {
                        directoryName = new string(charactersArray);
                    }

                    files.Add(new
                    {
                        name = directoryName,
                        type = "d"
                    });
                }
            }

            return files.ToArray();
        }

        private Uri[] ListDirectoryBlobFiles(string path)
        {
            var uri = new HashSet<Uri>();

            // Loop over items within the container to find all the blob files

            foreach (IListBlobItem item in _blobContainer.ListBlobs(path, false))
            {
                if (item.GetType() == typeof(CloudBlockBlob))
                {
                    CloudBlockBlob blob = (CloudBlockBlob)item;
                    uri.Add(blob.Uri);
                }
                else if (item.GetType() == typeof(CloudPageBlob))
                {
                    CloudPageBlob blob = (CloudPageBlob)item;
                    uri.Add(blob.Uri);
                }
                else if (item.GetType() == typeof(CloudBlobDirectory)) // do nothing
                {

                }
            }

            return uri.ToArray();
        }

        public static bool IsExist(string path)
        {
            bool result = false;

            try
            {

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(path);
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    result = true;
                }

            }
            catch
            {
                result = false;
            }

            return result;
        }

        public string GetURL(string path)
        {
            return _blobContainer + "/" + path;
        }

        private static Uri GetUri(string path)
        {
            var baseUri = new Uri(WebConfigHelper.AzureStorageUrl + DirectoryDelimeter);
            var uri = new Uri(baseUri, path);

            return uri;
        }

        public void ReadFile(string sourcePath, string targetPath)
        {
            CloudBlockBlob sourceBlob = _blobContainer.GetBlockBlobReference(sourcePath);
            CloudBlockBlob targetBlob = _blobContainer.GetBlockBlobReference(targetPath);

            targetBlob.StartCopyFromBlob(sourceBlob);

        }
        public Stream DownloadBlobAsStream(string blobUri)
        {
            Stream mem = new MemoryStream();
            CloudBlockBlob blob = _blobContainer.GetBlockBlobReference(blobUri);
            if (blob != null)
                blob.DownloadToStream(mem);
            mem.Position = 0;
            return mem;
        }


        public void CreateTable(string tableName)
        {
            CloudTableClient client = _account.CreateCloudTableClient();

            CloudTable table = client.GetTableReference(tableName);

            table.CreateIfNotExists();
        }

        public void DeleteTable(string tableName)
        {
            CloudTableClient client = _account.CreateCloudTableClient();

            CloudTable table = client.GetTableReference(tableName);

            table.Delete();
        }

        public void InsertUserToTable(string tableName, NewUsers item)
        {
            CloudTableClient client = _account.CreateCloudTableClient();

            CloudTable table = client.GetTableReference(tableName);

            CreateTable(tableName);

            TableOperation insertOperation = TableOperation.Insert(item);

            table.Execute(insertOperation);

        }

        public IEnumerable<NewUsers> GetUsersWithPass(string tableName)
        {

            CloudTableClient client = _account.CreateCloudTableClient();

            CloudTable table = client.GetTableReference(tableName);

            TableQuery<NewUsers> query = new TableQuery<NewUsers>();

            query = new TableQuery<NewUsers>().Where(
            TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "General"));

            var result = table.ExecuteQuery(query);

            return result;
        }
    }
}