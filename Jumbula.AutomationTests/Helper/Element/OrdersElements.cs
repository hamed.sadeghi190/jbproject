﻿using System.Linq;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element
{
    class OrdersElements : BaseElements
    {
        public OrdersElements(IWebDriver driver)
            :base(driver)
        {

        }
        #region Filter
        public IWebElement SearchTerm
        {
            get
            {
                return Driver.FindElement(By.Id("searchTerm"));
            }
        }
        public IWebElement SearchBtn
        {
            get
            {
                var btns = Driver.FindElements(By.TagName("button"));
                return btns.First(btn => btn.Text == "Search");
            }
        }
        #endregion Filter
        #region GridRow1
        public IWebElement Row1Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='orderGrid']//span[contains(text(),'...')]"));
            }
        }
        public IWebElement Row1ViewDetailsAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='orderGrid']//span[text()='View details']"));
            }
        }
        #endregion GridRow1
    }
}
