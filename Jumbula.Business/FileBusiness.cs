﻿using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Model.Email;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Serilog;

namespace Jumbula.Business
{
    public class FileBusiness : IFileBusiness
    {
        #region Constants
        private const double MaxAllowedSize = 10 * 1024 * 1024;
        private const string ContentType = "imagePath/jpeg";

        private const string CheckExtensionMessage = "File extension not supported!";
        private const string CheckSizeMessage = "Attachments cannot be larger than 10 mega bytes.";
        private const string ImagePathIsNotValidMessage = "Image path is not valid.";
        #endregion

        #region Fields
        private readonly List<string> _uploadRestrictedExtensions = new List<string>
        {
            ".exe", ".pif", ".application", ".gadget", ".msi", ".msp", ".com", ".scr", ".hta", ".cpl", ".msc", ".jar", ".bat", ".cmd", ".vb", ".vbs", ".vbe", ".js", ".jse", ".ws", ".wsf", ".wsc", ".wsh", ".ps1", ".ps1xml", ".ps2", ".ps2xml", ".psc1", ".psc2", ".msh", ".msh1", ".msh2", ".mshxml", ".nsh1xml", ".msh2xml", ".scf", ".lnk", ".inf", ".reg", ".eoc", ".docm", ".dotm", ".xlsm", ".xltm", ".xlam", ".pptm", ".potm", ".ppam", ".ppsm", ".sldm"
        };

        private readonly IStorageBusiness _storageBusiness;
        public ILogger Logger = Log.ForContext<FileBusiness>();
        #endregion 

        #region Constructors

        public FileBusiness(IStorageBusiness storageBusiness)
        {
            _storageBusiness = storageBusiness;
        }

        #endregion 

        #region Public methods
        public List<string> GetLimitExtensions()
        {
            return _uploadRestrictedExtensions;
        }

        public bool IsAllowedFile(FileStream fileStream)
        {
            return !_uploadRestrictedExtensions.Contains(Path.GetExtension(fileStream.Name)) && fileStream.Length <= MaxAllowedSize;
        }

        public OperationStatus ValidateAttachment(List<AttachmentModel> attachmentModels)
        {
            if (attachmentModels.Select(x => Path.GetExtension(x.FileName)).Intersect(_uploadRestrictedExtensions).Any())
                return new OperationStatus { Status = false, Message = CheckExtensionMessage };

            return attachmentModels.Sum(x => x.FileSize) > MaxAllowedSize ? new OperationStatus { Status = false, Message = CheckSizeMessage } : new OperationStatus { Status = true };
        }

        public string UploadImage(string imagePath, string clubDomain, string folderName)
        {
            if (!UrlPathHelper.IsRelationalUrl(imagePath))
            {
                Logger.Error("{LogCategory} {FileMessageType} {Message} ", "FileManager","ImagePath", ImagePathIsNotValidMessage);
                throw new Exception(ImagePathIsNotValidMessage);
            }

            var fileName = $"{Guid.NewGuid()}.jpg";

            using (var fileStream = FileHelper.ConvertStringToStream(imagePath))
            {
                _storageBusiness.UploadBlob($@"{clubDomain}\{folderName}", fileName, fileStream, ContentType);
            }

            return fileName;
        }

        #endregion
    }
}
