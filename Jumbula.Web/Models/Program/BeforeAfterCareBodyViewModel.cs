﻿using Jumbula.Common.Enums;
using System;
using System.Collections.Generic;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;

namespace SportsClub.Models
{
    public class BeforeAfterCareBodyViewModel : BaseViewModel<Program, long>, IProgramBodyViewModel
    {
        public PostalAddress Address { get; set; }

        public string ClubLogo { get; set; }
        public bool EnablePriceHidden { get; set; }
        public Jumbula.Common.Enums.TimeZone  ClubTimeZone { get; set; }

        public string Description { get; set; }
        public string PaymentSchedule { get; set; }

        public DateTime? EndDate { get; set; }

        public string LocationName { get; set; }

        public DateTime? StartDate { get; set; }

        public string ClubDomain { get; set; }

        public string SeasonDomain { get; set; }

        public bool DisableCapacityRestriction { get; set; }

        public string Domain { get; set; }
        public string WeeklyEmail { get; set; }
        public string MaterialsNeeded { get; set; }
        public string Name { get; set; }
        public string RoomAssignment { get; set; }


        public bool HideGoogleMapInClassPage { get; set; }
        public List<ProgramBodySchedule> Schedules { get; set; }
        public ProgramBodySchedule DefaultSchedule { get; set; }

        public string AgeRestrictionMessage
        {
            get
            {
                return Ioc.ProgramBusiness.AgeRestrictionMessage(DefaultSchedule.Restrict.MinAge,
                    DefaultSchedule.Restrict.MaxAge, DefaultSchedule.Restrict.ApplyAtProgramStart);
            }
        }

        public string Price { get; set; }

        public string PriceLabel { get; set; }

        public CurrencyCodes Currency { get; set; }

        public List<DropInPunchardModel> DropInPunchcard { get; set; }

        public List<ProgramSchedulePartViewModel> ProgramScheduleParts { get; set; }

        public bool HasApplicationFee
        {
            get
            {
                return ApplicationFeeAmount != 0;
            }
        }

        public decimal ApplicationFeeAmount { get; set; }
    }
    public class ProgramSchedulePartViewModel
    {
        public decimal Amount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime? DueDate { get; set; }
    }

    public class DropInPunchardModel
    {
        public bool Enabel { get; set; }

        public string Name { get; set; }

        public decimal Amount { get; set; }
    }

}