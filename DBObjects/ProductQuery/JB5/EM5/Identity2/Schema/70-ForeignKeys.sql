
ALTER TABLE [dbo].[PlayerProfiles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PlayerProfiles_dbo.JbUsers_Id] FOREIGN KEY([UserId])
REFERENCES [dbo].[JbUsers] ([Id])
GO

ALTER TABLE [dbo].[PlayerProfiles] CHECK CONSTRAINT [FK_dbo.PlayerProfiles_dbo.JbUsers_Id]
GO

Alter table [dbo].[PlayerProfiles] drop constraint  [FK_dbo.PlayerProfiles_dbo.UserProfile_UserId]
Go

ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_JbUsers] FOREIGN KEY([UserId])
REFERENCES [dbo].[JbUsers] ([Id])
GO

ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_JbUsers]
GO

Alter table [dbo].[Orders] drop constraint [FK_Orders_UserProfile]
Go

