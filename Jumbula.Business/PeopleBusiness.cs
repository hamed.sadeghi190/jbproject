﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Email;
using Jumbula.Core.Model.Notification;
using Jumbula.Core.Model.People;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Jumbula.Business
{
    public class PeopleBusiness : IPeopleBusiness
    {
        #region Fields
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IClubSettingBusiness _clubSettingBusiness;
        private readonly IEmailBusiness _emailBusiness;
        private readonly IAccountingBusiness _accountingBusiness;
        private readonly IClubBusiness _clubBusiness;
        private readonly IPlayerProfileBusiness _playerProfileBusiness;
        private readonly IProgramSessionBusiness _programSessionBusiness;
        private readonly IProgramBusiness _programBusiness;
        #endregion

        #region Constants
        private const string NotificationMessage = "Your account is delinquent";
        private const string NotificationTitle = "Delinquency";

        //todo: after add Status of Expelled
        //private const string ExpelledStatus = "Expelled";
        #endregion

        #region Constractors
        public PeopleBusiness(IOrderItemBusiness orderItemBusiness, IClubSettingBusiness clubSettingBusiness, IEmailBusiness emailBusiness, IAccountingBusiness accountingBusiness, IClubBusiness clubBusiness, IPlayerProfileBusiness playerProfileBusiness, IProgramSessionBusiness programSessionBusiness, IProgramBusiness programBusiness)
        {
            _orderItemBusiness = orderItemBusiness;
            _clubSettingBusiness = clubSettingBusiness;
            _emailBusiness = emailBusiness;
            _accountingBusiness = accountingBusiness;
            _clubBusiness = clubBusiness;
            _playerProfileBusiness = playerProfileBusiness;
            _programSessionBusiness = programSessionBusiness;
            _programBusiness = programBusiness;
        }
        #endregion

        #region Public methods
        public PaginatedList<DelinquentListViewModel> GetDelinquentList(int clubId, DelinquentFilterViewModel filters, List<Sort> sorts, int pageIndex, int pageSize)
        {
            var delinquents = GetDelinquentsQueryable(clubId);

            delinquents = delinquents != null ? SortDelinquentsOrderItems(delinquents, sorts) : null;

            var orderItemsList = delinquents != null ? delinquents.Paginate(pageIndex, pageSize).ToList() : new List<DelinquentModel>();

            return orderItemsList.Select(MapDelinquentItemViewModel).ToPaginatedList(pageIndex, pageSize, delinquents?.Count() ?? 0);
        }

        public object GetDelinquentsTotal(int clubId)
        {
            var delinquents = GetDelinquentsQueryable(clubId);

            return new
            {
                TotalAmount = delinquents?.Sum(x => x.TotalAmount) ?? 0,
                TotalPaidAmount = delinquents?.Sum(x => x.PaidAmount) ?? 0,
                TotalBalanceAmount = delinquents?.Sum(delinquentModel =>
                                         delinquentModel.DelinquentPolicy == DelinquentPolicies.AutoChargeFailAttemptNumber
                                             ? delinquentModel.OrderInstallment != null ? delinquentModel.OrderInstallment.Amount - (delinquentModel.OrderInstallment.PaidAmount ?? 0) : 0
                                             : delinquentModel.TotalAmount - delinquentModel.PaidAmount) ?? 0
            };
        }

        public EmailViewModel GetDelinquentEmail(string relatedEntity, long relatedEntityId, Club club, int userId)
        {
            var emailViewModel = _emailBusiness.GetViewModel(EmailCategory.Delinquent, new DelinquentEmailParameterModel
            {
                RelatedEntity = new List<DelinquentEmailParameterRelatedEntityItemModel> { new DelinquentEmailParameterRelatedEntityItemModel { RelatedEntityName = relatedEntity, RelatedEntityId = relatedEntityId } },
                ClubId = club.Id
            });

            emailViewModel.UserId = userId;
            emailViewModel.IsCampaignMode = true;

            return emailViewModel;
        }

        public OperationStatus GetDelinquentEmails(List<DelinquentEmailParameterRelatedEntityItemModel> relatedEntity, Club club, int userId)
        {
            if (relatedEntity == null || !relatedEntity.Any()) return new OperationStatus { Status = false, Message = Messages.NotSelectedGridRowErrorMessage, RecordsAffected = 0 };

            var data = _emailBusiness.GetViewModel(EmailCategory.Delinquent, new DelinquentEmailParameterModel
            {
                RelatedEntity = relatedEntity,
                ClubId = club.Id
            });

            data.UserId = userId;
            data.IsCampaignMode = true;

            return new OperationStatus
            {
                Status = true,
                Data = data
            };
        }

        public bool IsDelinquent(int userId, int clubId)
        {
            var delinquentPolicy = _clubSettingBusiness.ReadDelinquentPolicy(clubId);

            if (delinquentPolicy?.AllowEnrollOnDelinquent ?? true)
                return false;

            if (delinquentPolicy.FamilyBalanceNoLimitDate != null &&
                _orderItemBusiness.GetDelinquentFullPaidItems(clubId, delinquentPolicy).Any(x => x.Order.User.Id == userId))
                return true;

            if (delinquentPolicy.AutoChargeFailAttemptNumber != 0 &&
                _orderItemBusiness.GetDelinquentOrderInstallmentItems(clubId, delinquentPolicy).Any(x => x.OrderItem.Order.User.Id == userId))
                return true;

            return false;
        }

        public NotificationModel GetDelinquentNotificationItem(int userId, ClubBaseInfoModel club)
        {
            var delinquentPolicy = _clubSettingBusiness.ReadDelinquentPolicy(club.Id);

            //Date not null and lower than now// && delinquentPolicy.FamilyBalanceNoLimitDate <= DateTime.UtcNow.Date
            if (delinquentPolicy?.FamilyBalanceNoLimitDate != null)
            {
                if (_orderItemBusiness.GetDelinquentFullPaidItems(club.Id, delinquentPolicy).Any(x => x.Order.User.Id == userId))
                {
                    return new NotificationModel
                    {
                        Category = NotificationCategory.Delinquent,
                        Title = NotificationTitle,
                        Message = NotificationMessage,
                        NotificationTime = DateTimeHelper.DateTimeLable(delinquentPolicy.FamilyBalanceNoLimitDate.Value, club.TimeZone)
                    };
                }
            }

            if (delinquentPolicy != null && delinquentPolicy.AutoChargeFailAttemptNumber != 0)
            {
                var installmentDelinquentInfo = _orderItemBusiness.GetDelinquentOrderInstallmentItems(club.Id, delinquentPolicy).Where(x => x.OrderItem.Order.User.Id == userId).ToList();

                if (installmentDelinquentInfo.Any())
                {
                    return new NotificationModel
                    {
                        Category = NotificationCategory.Delinquent,
                        Title = NotificationTitle,
                        Message = NotificationMessage,
                        NotificationTime = DateTimeHelper.DateTimeLable(installmentDelinquentInfo.Min(x => x.InstallmentDate), club.TimeZone)
                    };
                }
            }

            return null;
        }

        public ParticipantClassScheduleViewModel GetParticipantClassSchedule(int playerId, int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            var currentDate = _clubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow).Date;

            var profile = _playerProfileBusiness.Get(playerId);

            var query = _orderItemBusiness.GetRegisteredProfile(playerId).OrderByDescending(o => o.Id);

            var classesInfo = query.ToList().Select(item => MapParticipantClassScheduleViewModels(item, currentDate)).Where(item => item != null).ToList();

            var schoolLogo = _clubBusiness.GetClubLogoURL(club.Domain, club.Logo);

            var model = new ParticipantClassScheduleViewModel
            {
                FullName = $"{profile.Contact.FirstName} {profile.Contact.LastName}",
                ClubName = club.Name,
                Date = currentDate.ToString(Constants.DateTime_Comma),
                Email = club.ContactPersons != null && !string.IsNullOrEmpty(club.ContactPersons.First().Email) ? club.ContactPersons.First().Email : "",
                Phone = club.ContactPersons != null && !string.IsNullOrEmpty(club.ContactPersons.First().Phone) ? PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.First().Phone) : "",
                Address = club.Address != null && !string.IsNullOrEmpty(club.Address.Address) ? club.Address.Address : "",
                SchoolLogo = schoolLogo.ToLower().Contains("http") ? schoolLogo : $"{UrlPathHelper.HomeUrl()}/{schoolLogo}",
                ClassesInfo = classesInfo
            };

            return model;
        }

        private ParticipantClassScheduleItemViewModel MapParticipantClassScheduleViewModels(OrderItem orderItem, DateTime currentDate)
        {
            var lastOrderItemSession = _programSessionBusiness.GetLastSession(orderItem, false);

            if (lastOrderItemSession == null || lastOrderItemSession.SessionDate.Date < currentDate) return null;

            var firstOrderItemSession = _programSessionBusiness.GetFirstSession(orderItem, false);

            var instructorNames = _programBusiness.GetInstractors(orderItem.ProgramSchedule.Program);
            var program = orderItem.ProgramSchedule.Program;
            var days = _orderItemBusiness.GetClassDays(orderItem);

            return new ParticipantClassScheduleItemViewModel
            {
                ProgramName = program.Name,
                TeacherName = instructorNames,
                Room = program.Room,
                Location = program.ClubLocation.PostalAddress.AutoCompletedAddress,
                StartDate = firstOrderItemSession.StartTime.Date.ToString(Constants.DateTime_Comma),
                EndDate = lastOrderItemSession.StartTime.Date.ToString(Constants.DateTime_Comma),
                StartEndTime = $"{firstOrderItemSession.StartTime.ToString(Constants.TimeWithAMPM)} - {firstOrderItemSession.EndTime.ToString(Constants.TimeWithAMPM)}",
                Days = days
            };
        }

        public byte[] GetExcelParticipantClassSchedule(int playerId, int clubId)
        {
            var model = GetParticipantClassSchedule(playerId, clubId);

            var workbook = new HSSFWorkbook();

            var sheet = workbook.CreateSheet();

            //Define Style
            var alignmentCenterStyle = workbook.CreateCellStyle();
            alignmentCenterStyle.Alignment = HorizontalAlignment.Center;
            alignmentCenterStyle.VerticalAlignment = VerticalAlignment.Center;

            var boldFont = workbook.CreateFont();
            boldFont.Boldweight = (short)FontBoldWeight.Bold;

            var borderTopStyle = workbook.CreateCellStyle();
            borderTopStyle.BorderTop = BorderStyle.Thin;
            borderTopStyle.Alignment = HorizontalAlignment.Center;
            borderTopStyle.VerticalAlignment = VerticalAlignment.Center;

            var greyBoldStyle = workbook.CreateCellStyle();
            greyBoldStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyBoldStyle.FillPattern = FillPattern.SolidForeground;
            greyBoldStyle.SetFont(boldFont);
            greyBoldStyle.Alignment = HorizontalAlignment.Left;
            greyBoldStyle.VerticalAlignment = VerticalAlignment.Center;

            var greyStyleLeftAlignment = workbook.CreateCellStyle();
            greyStyleLeftAlignment.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyStyleLeftAlignment.FillPattern = FillPattern.SolidForeground;
            greyStyleLeftAlignment.Alignment = HorizontalAlignment.Left;
            greyStyleLeftAlignment.VerticalAlignment = VerticalAlignment.Center;

            var greyStyleCenterAlignment = workbook.CreateCellStyle();
            greyStyleCenterAlignment.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyStyleCenterAlignment.FillPattern = FillPattern.SolidForeground;
            greyStyleCenterAlignment.Alignment = HorizontalAlignment.Center;
            greyStyleCenterAlignment.VerticalAlignment = VerticalAlignment.Center;

            sheet.SetDefaultColumnStyle(0, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(1, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(2, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(3, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(4, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(5, alignmentCenterStyle);

            var rowNumber = 0;

            //**** Header ****////
            rowNumber++;
            var headerRow = sheet.CreateRow(rowNumber);
            headerRow.CreateCell(0).SetCellValue("Participant:");
            headerRow.CreateCell(1).SetCellValue(model.FullName);
            headerRow.GetCell(0).CellStyle = greyStyleLeftAlignment;

            rowNumber++;
            headerRow = sheet.CreateRow(rowNumber);
            headerRow.CreateCell(0).SetCellValue("School:");
            headerRow.CreateCell(1).SetCellValue(model.ClubName);
            headerRow.GetCell(0).CellStyle = greyStyleLeftAlignment;

           //line space between header and content
            rowNumber++;

            //*** Content ***//
            // static header
            rowNumber++;
            var row = sheet.CreateRow(rowNumber);
            row.CreateCell(0).SetCellValue("Class name");
            row.CreateCell(1).SetCellValue("Start date");
            row.CreateCell(2).SetCellValue("End date");
            row.CreateCell(3).SetCellValue("Start - End time");
            row.CreateCell(4).SetCellValue("Days");
            row.CreateCell(5).SetCellValue("Teacher");
            row.CreateCell(6).SetCellValue("Room");
            row.CreateCell(7).SetCellValue("Location");
            row.GetCell(0).Sheet.SetColumnWidth(0, 20 * 256);
            row.GetCell(1).Sheet.SetColumnWidth(1, 20 * 256);
            row.GetCell(2).Sheet.SetColumnWidth(2, 20 * 256);
            row.GetCell(3).Sheet.SetColumnWidth(3, 20 * 256);
            row.GetCell(4).Sheet.SetColumnWidth(4, 20 * 256);
            row.GetCell(5).Sheet.SetColumnWidth(5, 20 * 256);
            row.GetCell(6).Sheet.SetColumnWidth(6, 20 * 256);
            row.GetCell(7).Sheet.SetColumnWidth(7, 30 * 256);
            row.GetCell(0).CellStyle = greyStyleCenterAlignment;
            row.GetCell(1).CellStyle = greyStyleCenterAlignment;
            row.GetCell(2).CellStyle = greyStyleCenterAlignment;
            row.GetCell(3).CellStyle = greyStyleCenterAlignment;
            row.GetCell(4).CellStyle = greyStyleCenterAlignment;
            row.GetCell(5).CellStyle = greyStyleCenterAlignment;
            row.GetCell(6).CellStyle = greyStyleCenterAlignment;
            row.GetCell(7).CellStyle = greyStyleCenterAlignment;

            foreach (var item in model.ClassesInfo)
            {
                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                row.CreateCell(0).SetCellValue(item.ProgramName);
                row.CreateCell(1).SetCellValue(item.StartDate);
                row.CreateCell(2).SetCellValue(item.EndDate);
                row.CreateCell(3).SetCellValue(item.StartEndTime);
                row.CreateCell(4).SetCellValue(item.Days);
                row.CreateCell(5).SetCellValue(item.TeacherName);
                row.CreateCell(6).SetCellValue(item.Room);
                row.CreateCell(7).SetCellValue(item.Location);
                row.GetCell(0).CellStyle = borderTopStyle;
                row.GetCell(1).CellStyle = borderTopStyle;
                row.GetCell(2).CellStyle = borderTopStyle;
                row.GetCell(3).CellStyle = borderTopStyle;
                row.GetCell(4).CellStyle = borderTopStyle;
                row.GetCell(5).CellStyle = borderTopStyle;
                row.GetCell(6).CellStyle = borderTopStyle;
                row.GetCell(7).CellStyle = borderTopStyle;
            }


            //Write the Workbook to a memory stream
            var output = new MemoryStream();
            workbook.Write(output);

            return output.ToArray();
        }

        #endregion

        #region Private methods
        private DelinquentListViewModel MapDelinquentItemViewModel(DelinquentModel delinquentModel) => new DelinquentListViewModel
        {
            RelatedEntityId = delinquentModel.OrderInstallment?.Id ?? delinquentModel.OrderItem.Id,
            RelatedEntity = delinquentModel.OrderInstallment != null ? nameof(OrderInstallment) : nameof(OrderItem),
            Email = delinquentModel.Email,
            Attempts = delinquentModel.Attempts.ToString(),
            PaidAmount = delinquentModel.PaidAmount.ToString(),
            Program = delinquentModel.Program,
            TotalAmount = delinquentModel.TotalAmount.ToString(),
            Season = delinquentModel.Season,
            //todo: this field return string.Empty just now because we should be create that into roster and then create in here
            Status = string.Empty,//delinquentModel.OrderItem.IsExpelled ? ExpelledStatus : string.Empty,
            Balance = delinquentModel.DelinquentPolicy == DelinquentPolicies.AutoChargeFailAttemptNumber && delinquentModel.OrderInstallment != null ? delinquentModel.OrderInstallment.Balance.ToString(CultureInfo.InvariantCulture) : _accountingBusiness.OrderItemBalance(delinquentModel.OrderItem).ToString(CultureInfo.InvariantCulture),
            Participant = $"{delinquentModel.FirstName} {delinquentModel.LastName}",
            Confirmation = delinquentModel.Confirmation,
            Policy = $"P{(int)delinquentModel.DelinquentPolicy}"
        };

        private static IQueryable<DelinquentModel> SortDelinquentsOrderItems(IQueryable<DelinquentModel> delinquentOrderItems, List<Sort> sorts)
        {
            if (sorts == null) return delinquentOrderItems;

            foreach (var sort in sorts)
            {
                switch (sort.Field)
                {
                    case "Id":
                        {
                            delinquentOrderItems = sort.Dir == SortDirection.Asc ? delinquentOrderItems.OrderBy(o => o.OrderItem.Id).AsQueryable() : delinquentOrderItems.OrderByDescending(o => o.OrderItem.Id).AsQueryable();
                            break;
                        }
                    case "Email":
                        {
                            delinquentOrderItems = sort.Dir == SortDirection.Asc ? delinquentOrderItems.OrderBy(o => o.Email).AsQueryable() : delinquentOrderItems.OrderByDescending(o => o.Email).AsQueryable();
                            break;
                        }
                    case "Participant":
                        {
                            delinquentOrderItems = sort.Dir == SortDirection.Asc ? delinquentOrderItems.OrderBy(o => $"{o.FirstName} {o.LastName}").AsQueryable() : delinquentOrderItems.OrderByDescending(o => $"{o.FirstName} {o.LastName}").AsQueryable();
                            break;
                        }
                    case "Program":
                        {
                            delinquentOrderItems = sort.Dir == SortDirection.Asc ? delinquentOrderItems.OrderBy(o => o.Program).AsQueryable() : delinquentOrderItems.OrderByDescending(o => o.Program).AsQueryable();
                            break;
                        }
                    case "Season":
                        {
                            delinquentOrderItems = sort.Dir == SortDirection.Asc ? delinquentOrderItems.OrderBy(o => o.Season).AsQueryable() : delinquentOrderItems.OrderByDescending(o => o.Season).AsQueryable();
                            break;
                        }
                    case "Confirmation":
                        {
                            delinquentOrderItems = sort.Dir == SortDirection.Asc ? delinquentOrderItems.OrderBy(o => o.Confirmation).AsQueryable() : delinquentOrderItems.OrderByDescending(o => o.Confirmation).AsQueryable();
                            break;
                        }
                    case "TotalAmount":
                        {
                            delinquentOrderItems = sort.Dir == SortDirection.Asc ? delinquentOrderItems.OrderBy(o => o.TotalAmount).AsQueryable() : delinquentOrderItems.OrderByDescending(o => o.TotalAmount).AsQueryable();
                            break;
                        }
                    case "PaidAmount":
                        {
                            delinquentOrderItems = sort.Dir == SortDirection.Asc ? delinquentOrderItems.OrderBy(o => o.PaidAmount).AsQueryable() : delinquentOrderItems.OrderByDescending(o => o.PaidAmount).AsQueryable();
                            break;
                        }
                    case "Balance":
                        {
                            delinquentOrderItems = sort.Dir == SortDirection.Asc ? delinquentOrderItems.OrderBy(o => o.TotalAmount - o.PaidAmount).AsQueryable() : delinquentOrderItems.OrderByDescending(o => o.TotalAmount - o.PaidAmount).AsQueryable();
                            break;
                        }
                }
            }

            return delinquentOrderItems;
        }

        private IQueryable<DelinquentModel> GetDelinquentsQueryable(int clubId)
        {
            var delinquentPolicy = _clubSettingBusiness.ReadDelinquentPolicy(clubId);

            IQueryable<DelinquentModel> delinquents = null;

            #region  policy 1
            if (delinquentPolicy != null && delinquentPolicy.AutoChargeFailAttemptNumber != 0)
            {
                var familyBalanceDelinquent =
                    _orderItemBusiness.GetDelinquentOrderInstallmentItems(clubId, delinquentPolicy).Select(o => new DelinquentModel
                    {
                        OrderItem = o.OrderItem,
                        OrderInstallment = o,
                        Email = o.OrderItem.Order.User.UserName,
                        Attempts = o.AutoChargeAttemps,
                        TotalAmount = o.Amount,
                        PaidAmount = o.PaidAmount,
                        Program = o.OrderItem.ProgramSchedule.Program.Name,
                        Season = o.OrderItem.Season.Title,
                        FirstName = o.OrderItem.Player.Contact.FirstName,
                        LastName = o.OrderItem.Player.Contact.LastName,
                        Confirmation = o.OrderItem.Order.ConfirmationId,
                        DelinquentPolicy = DelinquentPolicies.AutoChargeFailAttemptNumber
                    });
                delinquents = familyBalanceDelinquent;

            }
            #endregion

            #region  policy 3
            // Should not check delinquentPolicy.FamilyBalanceNoLimitDate <= DateTime.Now.Date because maybe user want to see list before due date
            if (delinquentPolicy?.FamilyBalanceNoLimitDate != null)
            {
                var familyBalanceDelinquent = _orderItemBusiness.GetDelinquentFullPaidItems(clubId, delinquentPolicy).Select(o => new DelinquentModel
                {
                    OrderItem = o,
                    OrderInstallment = null,
                    Email = o.Order.User.UserName,
                    Attempts = null,
                    TotalAmount = o.TotalAmount,
                    PaidAmount = o.PaidAmount,
                    Program = o.ProgramSchedule.Program.Name,
                    Season = o.Season.Title,
                    FirstName = o.Player.Contact.FirstName,
                    LastName = o.Player.Contact.LastName,
                    Confirmation = o.Order.ConfirmationId,
                    DelinquentPolicy = DelinquentPolicies.FamilyBalanceNoLimitDate
                });

                delinquents = delinquents != null
                    ? delinquents.Union(familyBalanceDelinquent)
                    : familyBalanceDelinquent;
            }
            #endregion

            return delinquents;
        }

        #endregion
    }
}
