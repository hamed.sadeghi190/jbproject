﻿using System;
using System.Collections.Generic;
using System.Linq;
using SportsClub.Domain;
using SportsClub.Infrastructure;
using System.IO;
using SportsClub.Db;
using System.Web;
using SportsClub.Infrastructure.Helper;
using Jb.Framework.Common.Model;

namespace SportsClub.Models
{
    public class TourneyPageViewModel
    {
        public bool OfflineRegChecked { get; set; }
        public string Domain { get; set; } // must unique within club domain
        public string ClubDomain { get; set; } // must unique within club domain
        public string ClubName { get; set; }
        public string ItemId { get; set; } // unique globally within DB
        public string Name { get; set; }
        public string SubTitle1 { get; set; }
        public string SubTitle2 { get; set; }
        public string ChiefTD { get; set; }
        public string ChiefOrganizer { get; set; }
        //public string DisplayDate { get; set; }
        public string PdfFileName { get; set; }
        public string ResultFileName { get; set; } // change to class?
        public string AddressName { get; set; }
        public string ExternalRegUrl { get; set; }
        public EventStatusCategories EventStatus { get; set; }
        public int CategoryId { get; set; }
        //public SportCategories SportCategory { get; set; }
        public MetaData MetaData { get; set; }
        public virtual PostalAddress PostalAddress { get; set; }
        public EntryFee EF { get; set; }
        public Hotel Hotel { get; set; }
        public Prize Prize { get; set; }
        public Bye Bye { get; set; }
        public bool Expired { get; set; }
        public string LocatinName { get; set; }

        public bool IsLoggedIn { get; set; }

        public OrderMode OrderMode { get; set; }

        public string LeftDays
        {
            get
            {
                string result = string.Empty;

                DateTime expireDate = Utilities.TourneyExpireDate(this.EF.OnLineDeadline.Value);

                TimeSpan elapsedTime = expireDate - Utilities.GetCurrentLocalDateTime(this.TimeZone);

                // If remainig time is more than 3 days
                if (elapsedTime.TotalHours >= 72)
                {
                    result = string.Empty;
                }
                // If remainig time is between 3 and 1 day
                else if (elapsedTime.TotalHours >= 24 && elapsedTime.TotalHours < 72)
                {
                    result = string.Format("{0} days left", elapsedTime.Days + 1);
                }
                // If remainig time is less than 1 day
                else if (elapsedTime.TotalHours < 24)
                {
                    if (elapsedTime.TotalHours >= 1)
                    {
                        result = string.Format("{0}:{1} hours left", elapsedTime.Hours, elapsedTime.Minutes);
                    }
                    // If remainig time is less than 1 houre
                    else if (elapsedTime.Minutes > 0 && elapsedTime.Minutes < 60)
                    {
                        result = string.Format("{0} min left", elapsedTime.Minutes);
                    }
                }

                return result;
            }
        }

        public bool IsNotCurrentCartDomain
        {
            get
            {
                // Get cart
                //var cartDb = Ioc.ICartDbService;

                //// Get domain of orders in the cart(if there is no order in cart return empty)
                //string currentDomain = cartDb.CurrentDomain(HttpContext.Current);

                //if (!string.IsNullOrEmpty(currentDomain))
                //{
                //    return !(currentDomain == this.ClubDomain);
                //}

                return false;
            }
        }

        public RoleCategories CurrentUserRole { get; set; }

        public PageMetaDataViewModel MetaTags { get; set; }

        public virtual dynamic ChargeDiscounts { get; set; }
        public virtual ICollection<TourneySection> Sections { get; set; }
        public virtual List<TourneyPageScheduleViewModel> Schedules { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
        public List<TourneyPageImageGalleryItemViewModel> Gallery { get; set; }

        public GoogleMapsViewModel Map { get; set; }

        public string LogoUri { get; set; }

        public bool IsAdmin { get; set; }

        public int RegisterCount { get; set; }

        public TourneyRewardType TourneyType { get; set; }

        public string TrophyInfo { get; set; }

        // affiliator 
        public bool HasAffiliator { get; set; }

        public string AffiliatorLogoUri { get; set; }

        public string AffiliatorDomain { get; set; }

        public string AffiliatorName { get; set; }

        public Domain.TimeZone TimeZone { get; set; }

        public bool IsOutsourcerEvent { get; set; }
        public string OutsourcerClubDomain { get; set; }
        public string OutsourcerClubName { get; set; }

        public Nullable<int> MinAge { get; set; }

        public Nullable<int> MaxAge { get; set; }

        public SchoolGradeType? MinGrade { get; set; }

        public SchoolGradeType? MaxGrade { get; set; }

        public TourneyPageViewModel() { }

        public TourneyPageViewModel(Tourney tourney)
            : this()
        {
            OfflineRegChecked = tourney.OfflineRegChecked;
            Domain = tourney.Domain;
            ClubDomain = tourney.ClubDomain;
            ClubName = Ioc.IClubDbService.GetClubName(ClubDomain);
            ItemId = tourney.ItemId;
            Name = tourney.Name;
            SubTitle1 = tourney.SubTitle1;
            SubTitle2 = tourney.SubTitle2;
            ChiefTD = tourney.ChiefTD;
            ChiefOrganizer = tourney.ChiefOrganizer;
            //DisplayDate = tourney.DisplayDate;
            PdfFileName = tourney.FileName;
            ResultFileName = tourney.ResultFileName;
            AddressName = tourney.AddressName;
            ExternalRegUrl = tourney.ExternalRegUrl;
            EventStatus = tourney.EventStatus;
            //SportCategory = tourney.SportCategory;
            CategoryId = tourney.CategoryId;
            MetaData = tourney.MetaData;
            PostalAddress = tourney.ClubLocation.PostalAddress;
            EF = tourney.EF;
            Prize = tourney.Prize;
            Bye = tourney.Bye;
            ChargeDiscounts = tourney.ChargeDiscounts;
            Sections = tourney.Sections;
            Notes = tourney.Notes;
            TimeZone = tourney.TimeZone;
            LocatinName = tourney.ClubLocation.Name;

            MinAge = tourney.EventSearchField.MinAge;
            MaxAge = tourney.EventSearchField.MaxAge;
            MinGrade = tourney.EventSearchField.MinGrade;
            MaxGrade = tourney.EventSearchField.MaxGrade;

            DateTime expireDate = Utilities.TourneyExpireDate(EF.OnLineDeadline.Value);
            Expired = Utilities.IsEventExpired(expireDate, TimeZone);

            Map = new GoogleMapsViewModel()
            {
                Name = AddressName,
                PostalAddress = PostalAddress,
               
            };
            
            // Meta tag 
            MetaTags = new PageMetaDataViewModel()
            {
                PageTitle =
                            this.Name +
                            Constants.C_Space +
                            "by" +
                            Constants.C_Space +
                            this.ClubName +
                            Constants.S_SpaceDashSpace +
                            CategoryModel.GetCategoryName(this.CategoryId) +
                            Constants.C_Space + SubDomainCategories.Tourney,

                MetaKeywords = string.Join(Constants.S_CommaSpace, new string[] 
                            { 
                                this.Name,
                                this.ClubName,
                                CategoryModel.GetCategoryName(this.CategoryId) + Constants.C_Space + SubDomainCategories.Tourney, 
                                Utilities.StringNullHandle(this.PostalAddress .City,string.Empty), 
                                Utilities.GetAbbreviationState(this.PostalAddress .State) 
                            }),

                // There is no description definition for tourney
                MetaDesc = string.Empty,
            };

            // load image gallery
            string galleryPath = string.Format("{1}/{0}", Domain, ClubDomain);

            List<Domain.ImageGalleryItem> images = Ioc.IImageGalleryDbService.GetImageGalleryEvent(tourney.Domain, tourney.ClubDomain);
            Gallery = new List<TourneyPageImageGalleryItemViewModel>();
            foreach (var item in images)
            {
                Gallery.Add(new TourneyPageImageGalleryItemViewModel()
                {
                    OriginUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, tourney.ClubDomain, item.Path).AbsoluteUri,
                    ThumbnailUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, tourney.ClubDomain, tourney.Domain + "/ImageGallery/" + Path.GetFileNameWithoutExtension(item.Path) + "_Thumbnail" + Path.GetExtension(item.Path)).AbsoluteUri
                });
            }

            // convert schedules
            Schedules = new List<TourneyPageScheduleViewModel>();

            if (tourney.Schedules != null)
            {
                var counter = 1;

                foreach (TourneySchedule schedule in tourney.Schedules)
                {
                    Schedules.Add(new TourneyPageScheduleViewModel(schedule) { Position = counter });

                    counter++;
                }
            }

            Club club = Ioc.IClubDbService.GetClub(ClubDomain,false);
            if (club != null)
            {
                LogoUri = Utilities.GetClubLogoURL(club.Domain, club.Logo, CategoryId);
            }
            else
            {
                LogoUri = string.Empty;
            }

            // check if club has affiliator
            bool hasAffilator = false;
            string affiliatorLogoUri = string.Empty;
            string affiliatorDomain = string.Empty;
            string affiliatorName = string.Empty;
            if (club.AffiliatorID.HasValue)
            {
                hasAffilator = true;
                IClubDb clubDb = Infrastructure.Helper.Ioc.IClubDbService;
                Club affiliatorClub = clubDb.GetAffiliatorClubByAffiliatorID(club.AffiliatorID.Value);

                if (affiliatorClub != null)
                {
                    affiliatorLogoUri = Utilities.GetClubLogoURL(affiliatorClub.Domain, affiliatorClub.Logo, affiliatorClub.CategoryId);
                    affiliatorDomain = affiliatorClub.Domain;
                    affiliatorName = affiliatorClub.Name;
                }
            }
            HasAffiliator = hasAffilator;
            AffiliatorLogoUri = affiliatorLogoUri;
            AffiliatorDomain = affiliatorDomain;
            AffiliatorName = affiliatorName;

            TourneyType = tourney.TourneyType;

            TrophyInfo = tourney.TrophyInfo;

            IsAdmin = WebMatrix.WebData.WebSecurity.CurrentUserId == Ioc.IClubDbService.GetClub(ClubDomain,false).UserId;

            int total = 0;
            RegisterCount = Ioc.IOrderDbService.TourneyPayments(ClubDomain, Domain, null, null, null, null, null, int.MaxValue, 0, ref total).Count();

            // Realize event is outsourcer or not
            IsOutsourcerEvent = !string.IsNullOrEmpty(tourney.OutSourcerClubDomain);

            if (IsOutsourcerEvent)
            {
                OutsourcerClubDomain = tourney.OutSourcerClubDomain;

                //Get outsourcer club domain from db
                OutsourcerClubName = Ioc.IClubDbService.GetClubName(OutsourcerClubDomain);
            }
        }
    }

    public class TourneyPageImageGalleryItemViewModel
    {
        public string OriginUrl { get; set; }
        public string ThumbnailUrl { get; set; }
    }

    public class TourneyPageScheduleViewModel
    {
        public int Position { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public EventStatusType Status { get; set; }
        public string Games { get; set; }
        public string TimeControl { get; set; }
        public string Name { get; set; }

        public TourneyPageScheduleViewModel() { }

        public TourneyPageScheduleViewModel(TourneySchedule schedule)
        {
            StartDate = schedule.Start;
            FinishDate = schedule.End;

            Games = schedule.Games;
            TimeControl = schedule.TimeControl;

            Name = schedule.Name;

            if (DateTime.Now <= StartDate)
            {
                Status = EventStatusType.Open;
            }
            else if (DateTime.Now >= StartDate && DateTime.Now <= FinishDate)
            {
                Status = EventStatusType.OpenInProgress;
            }
            else
            {
                Status = EventStatusType.Closed;
            }
        }
    }
}