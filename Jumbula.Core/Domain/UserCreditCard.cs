﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{

    public class UserCreditCard : BaseEntity
    {
        public UserCreditCard()
        {
            IsDefault = false;
            IsDeleted = false;
        }
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual JbUser User { get; set; }
        [StringLength(128)]
        public string CustomerId { get; set; }
        [StringLength(4)]
        public string LastDigits { get; set; }
        [StringLength(128)]
        public string Name { get; set; }

        [StringLength(4)]
        public string ExpiryYear { get; set; }

        public int ExpiryYearInt
        {
            get
            {
                if (string.IsNullOrEmpty(ExpiryYear))
                {
                    return 0;
                }
                return Convert.ToInt32(ExpiryYear);
            }
        }

        [StringLength(2)]
        public string ExpiryMonth { get; set; }

        public int ExpiryMonthInt
        {
            get
            {
                if (string.IsNullOrEmpty(ExpiryMonth))
                {
                    return 0;
                }
                return Convert.ToInt32(ExpiryMonth);
            }
        }
        public bool IsDeleted { get; set; }

        [StringLength(20)]
        public string Brand { get; set; }
        public string CustomerResponse { get; set; }
        public bool IsDefault { get; set; }
        public virtual PostalAddress Address { get; set; }
        public int? ClubId { get; set; }
        [ForeignKey("ClubId")]
        public virtual Club Club { get; set; }
        public UserCreditCardType? TypeCreditCard { get; set; }
        public PaymentGateway? TypePaymentGateway { get; set; }
        [StringLength(128)]
        public string CardId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public DateTime? DefaultDate { get; set; }
    }

    #region Enums
    public enum UserCreditCardType
    {
        [Description("OnlineHistory")]  //Add from Credit card not save in Online stripe or authorize 
        OnlineHistory = 0,
        [Description("Installment Online Registration")] //Add from installment Online registration 
        InstallmentOnlineRegistration = 1,
        [Description("Manually Added")]   //Add from parent dashboard
        ManuallyAdded = 2,
        [Description("Installment Offline Registration")]   //Add from installment Offline registration 
        InstallmentOfflineRegistration = 3,
        [Description("OfflineHistory")]  //Add from Credit card not save in Offline stripe or authorize 
        OfflineHistory = 4
    }

    #endregion
}
