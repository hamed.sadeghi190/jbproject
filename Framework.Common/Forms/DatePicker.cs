﻿using System;

namespace Jb.Framework.Common.Forms
{
    #region DatePicker   

    [Serializable]
    public class DatePicker : JbBaseElement
    {
        public DateTime? Value { get; set; }

        public string HelpText { get; set; }
    }

    #endregion
}
