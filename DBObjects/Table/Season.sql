

IF OBJECT_ID('[dbo].[Seasons]') IS NULL
BEGIN
CREATE TABLE [dbo].[Seasons](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](128) NOT NULL,
	[Domain] [nvarchar](128) NOT NULL,
	[ClubDomain] [nvarchar](128) NULL,
	[Description] [nvarchar](1000) NULL,
	[Note] [nvarchar](4000) NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL)
END

---<<ADD PRIMARY KEY>>--
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'PK_Seasons')
BEGIN
	ALTER TABLE [dbo].[season] ADD CONSTRAINT [PK_Seasons] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	) 
END

--<< ADD CONSTRAINT >>--
IF NOT EXISTS(SELECT 1 FROM sys.objects WHERE name='UK_Seasons_Domains')
BEGIN
	ALTER TABLE [dbo].[Seasons] ADD CONSTRAINT [UK_Seasons_Domains] UNIQUE NONCLUSTERED 
	(
		[ClubDomain] ASC,		
		[Domain] ASC
	)
END