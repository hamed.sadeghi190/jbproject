﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class CapacityReportViewModel
    {
        public string ProgramName { get; set; }

        public string Open { get; set; }

        public string Status { get; set; }

        public int Capacity { get; set; }

        public int Filled { get; set; }

        public string Schedule { get; set; }
    }
}