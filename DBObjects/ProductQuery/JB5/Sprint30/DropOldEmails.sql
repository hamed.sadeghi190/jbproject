
Go
ALTER table OrderSessions drop CONSTRAINT  [FK_dbo.OrderSessions_dbo.Emails_EmailId]

Go
DROP INDEX IX_EmailId ON [OrderSessions] 

Go
alter table OrderSessions drop column EmailId

Go
Drop Table Emails