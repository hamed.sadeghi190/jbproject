alter TABLE [dbo].[ClubTypes] Add [EnumType] [int]  NULL
GO
ALTER TABLE [dbo].[ClubTypes] ADD  DEFAULT ((1)) FOR [EnumType]
GO
update dbo.ClubTypes set enumtype=1 where name='SportClub'
GO
update dbo.ClubTypes set enumtype=2 where name='School'
GO
update dbo.ClubTypes set enumtype=3 where name='Provider'
GO
update dbo.ClubTypes set enumtype=4 where name='Individual'
GO
update dbo.ClubTypes set enumtype=5 where name='Corp'
GO
update dbo.ClubTypes set enumtype=6 where name='Public'
GO
update dbo.ClubTypes set enumtype=7 where name='Private'
GO
update dbo.ClubTypes set enumtype=8 where name='Title 1'
GO
update dbo.ClubTypes set enumtype=9 where name='Charter'
GO
alter TABLE [dbo].[ClubTypes] alter column [EnumType] [int]  Not NULL