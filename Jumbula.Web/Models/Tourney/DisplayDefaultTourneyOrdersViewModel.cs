﻿using SportsClub.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SportsClub.Infrastructure;
using SportsClub.Infrastructure.Helper;

namespace SportsClub.Models
{
    public class DisplayDefaultTourneyOrdersViewModel : DisplayOrdersViewModel
    {
        public new List<DefaultTourneyOrderItemViewModel> OrderItems { get; set; }

        public new DefaultTourneySearchViewModel Search { get; set; }

        private void SeedItems(string clubDomain, string domain, string confirmationId, string playerName, DateTime? startDate, DateTime? endDate, OrderMode? orderMode, int pageSize, int page)
        {
            int total = 0;
            IEnumerable<TourneyPaymentItem> items = Ioc.IOrderDbService.TourneyPayments(clubDomain, domain, confirmationId, playerName, startDate, endDate, orderMode, pageSize, page, ref total);
            //Get total records for paging 
            TotalRecord = total;

            OrderItems = items.Select(i => new DefaultTourneyOrderItemViewModel
            {
                OrderId = i.OrderId,
                ConfirmationNumber = i.ConfirmationId,
                ProfileId = i.ProfileId,
                RegisteredUser = string.Format("{0} {1}", i.FirstName, i.LastName),
                Date = Utilities.ShortFormatDateYearMonthDayWithLocalTime(i.Date, TimeZone),
                Section = i.Section,
                Schedule = i.Schedule,
                Byes = i.Byes,
                Options = i.Options,
                Amount = i.TotalPrice,//Utilities.FormatCurrencyWithoutPenny(i.TotalPrice),
                PaymentStatus = i.PaymentStatus,
                PaymentMethod = i.PaymentMethod,
                PaymentStatusDescription = string.Format("{0} ({1})", i.PaymentStatus.ToDescription(), i.PaymentMethod.ToDescription()),
                CategoryId = i.CategoryId,
            })
            .ToList();
        }

        public DisplayDefaultTourneyOrdersViewModel()
            : base()
        {
            Status = ViewOrderMode.ChessTourney;

            Search = new DefaultTourneySearchViewModel();
        }

        public DisplayDefaultTourneyOrdersViewModel(DefaultTourneySearchViewModel search, bool loadItems, int pageSize, int page)
            : base(search)
        {
            Status = ViewOrderMode.ChessTourney;

            Search = search;

            if (loadItems)
            {
                SeedItems(ClubDomain, Domain, search.ConfirmationNumber, search.PlayerName, search.StartDate, search.EndDate, search.OrderMode, pageSize, page);
            }
        }
    }
}