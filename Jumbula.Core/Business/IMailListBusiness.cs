﻿using Jb.Framework.Common;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IMailListBusiness
    {
        MailList Get(int Id);

        OperationStatus CreateList(MailList newMailList);

        IQueryable<MailList> GetAllLists(int clubId);

        OperationStatus DeleteMailList(int listId);

        IQueryable<MailListContact> GetListRecipients(int listId);

        OperationStatus DeleteMailListRecipient(int listId, int recipientId);

        OperationStatus UpdateMailList(MailList newList);
    }
}
