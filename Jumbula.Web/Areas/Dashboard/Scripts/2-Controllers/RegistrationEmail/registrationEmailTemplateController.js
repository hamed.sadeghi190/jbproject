﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('registrationEmailTemplateController', ['$scope', '$http', 'jbToast', '$stateParams', '$state', 'templateEditorService', 'imageUploadService', function ($scope, $http, jbToast, $stateParams, $state, templateEditorService, imageUploadService) {
        window.app.globalObjects.pageClass = 'form-view';
        window.app.globalObjects.initializeView();

        $scope.validateImage = function (elem,pos) {
            if (pos == 'header') {
                $scope.positionBanner = 'header';
            }
            else if (pos = 'footer') {
                $scope.positionBanner = 'footer';
            }
            imageUploadService.validateImage(this, elem)
        }
        $scope.typeBannerHeader = function (typeb) {
            $scope.typeBannerHeaderValue = typeb;
        }
        $scope.typeBannerFooter = function (typeb) {
            $scope.typeBannerFooterValue = typeb;
        }
        $scope.showCropper1 = true;
        $scope.showCropper2 = false;
        $scope.hideCropper1 = function () {
            $scope.showCropper1 = false;
            $scope.showCropper2 = true;
            $scope.cropper = imageUploadService.clearCropper(this);
        }

        $scope.hideCropper2 = function () {
            $scope.showCropper1 = true;
            $scope.showCropper2 = false;
            $scope.cropper = imageUploadService.clearCropper(this);
        }
        $scope.clearCropper = function () {
            $scope.cropper = imageUploadService.clearCropper(this);
         
        }

        $scope.MC.stateParams = $stateParams;

        // init model.
        $scope.Model = {};
        $scope.Model.MailTemplate = {};

        // use service.
        $scope.mailDesignerInit = function () {
            return templateEditorService.mailDesignerInit(this);
        }

        $scope.existingImages = templateEditorService.existingImages;

        $scope.setHeaderBackgroundImageToNone = function () {
            if ($scope.Model.MailTemplate)
                $scope.Model.MailTemplate.HeaderBackground = '';
        }
        $scope.setFooterBackgroundImageToNone = function () {
            if ($scope.Model.MailTemplate)
                $scope.Model.MailTemplate.FooterBackground = '';
        }

        $scope.setHeaderBackground = function (image) {
            return templateEditorService.setHeaderBackground(this, image);
        }

        $scope.getHeaderBackground = function () {
            return templateEditorService.getHeaderBackground(this);
        }

        $scope.setFooterBackground = function (image) {
            return templateEditorService.setFooterBackground(this, image);
        }

        $scope.getFooterBackground = function () {
            return templateEditorService.getFooterBackground(this);
        }

        $scope.getClubLogo = function () {
            return templateEditorService.getClubLogo(this);
        }

        $scope.enterEditing = function () {
            return templateEditorService.enterEditing(this);
        }

        $scope.saveEditing = function (posbanner, typeBannerHV,typeBannerFV) {
            $scope.positionBanner = null;
            return templateEditorService.saveEditing(this, posbanner, typeBannerHV, typeBannerFV);
        }

        $scope.cancelEditing = function () {
            $scope.positionBanner = null;
            return templateEditorService.cancelEditing(this);
        }
        console.log("state", $scope.MC.stateParams.seasonDomain)
        $http.get('Email/CreateEditTemplate?seasonDomain=' + $scope.MC.stateParams.seasonDomain).success(function (data, status, headers, config) {
            $scope.Model.MailTemplate = data.Data;
            $scope.MC.stateParams.templateId = data.templateId;
            $scope.Model.MailTemplate.UseProgramLocation = true;
        });

        $scope.useHtmlBreaks = function (str) {
            if (str)
                return str.replace(new RegExp('\n', 'g'), "<br>").replace(new RegExp('\r', 'g'), "<br>");
        }

        $scope.SaveTemplate = function () {
            $http.post('Email/SaveUpdateRegistrationEmail', { seasonDomain: $scope.MC.stateParams.seasonDomain, model: $scope.Model.MailTemplate, templateId: $scope.MC.stateParams.templateId }).success(function (data, status, headers, config) {
                if (data.Status == true) {
                    jbToast.success("Registration email template saved");
                    $scope.MC.stateParams.templateId = data.Data;
                } else {
                    jbToast.error("An error ocured while saving");
                }
            }).error(function () {
                jbToast.error("An error ocured while saving");
            });
        }

        $scope.openColorPicker = function (e) {
            $(e.currentTarget).find("input").data("kendoColorPicker").open();
        }
    }])
  
})();