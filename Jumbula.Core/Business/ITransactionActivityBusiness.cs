﻿using Jb.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface ITransactionActivityBusiness
    {
        OperationStatus Update(TransactionActivity transactionActivity);
        OperationStatus UpdateTransactions(List<TransactionActivity> transactions, PaymentDetail paymentDetail);
        IQueryable<TransactionActivity> GetList();
        IQueryable<TransactionActivity> GetAllClubTransactionByClubId(int clubId, bool isLive = true);
        IQueryable<TransactionActivity> GetAllTransactionByOrderId(long orderId);
        IQueryable<TransactionActivity> GetAllTransactionByOrderItemId(long orderItemId);
        IQueryable<TransactionActivity> GetAllsucceedTransactionByOrderId(long orderId);
        IQueryable<TransactionActivity> GetAllsucceedTransactionByOrderItemId(long orderItemId);
        IQueryable<TransactionActivity> GetAllClubTransactionByUserId(int userId, int clubId);
        decimal GetPaidAmountForOrder(long orderId);
        decimal GetPaidAmountForOrderItem(long orderItemId);
        decimal GetPaidAmountForInstallment(long InstallmentId);
        void SetOrderTransaction(Order order, TransactionCategory transactionCategory, TransactionType transactionType, PaymentDetail paymentDetail, TransactionStatus transactionStatus, DateTime? date = null);
        void SetOrderItemTransaction(OrderItem orderItem, TransactionCategory transactionCategory, TransactionType transactionType, PaymentDetail paymentDetail, TransactionStatus transactionStatus, decimal transactionAmount, DateTime? date = null);
        void SetInstallmentTransaction(OrderInstallment orderInstallment, TransactionCategory transactionCategory, TransactionType transactionType, PaymentDetail paymentDetail, TransactionStatus transactionStatus, decimal transactionAmount, DateTime? date = null);
        IQueryable<TransactionActivity> GetInvoicedByToken(string token);
        IQueryable<TransactionActivity> GetDraftByToken(string token);
        IQueryable<TransactionActivity> GetAllTransactionBySeasonId(long seasonId);

        bool IsPayKeyExist(string payKey);

        bool DeleteTransaction(long transactionId);

        TransactionActivity GetTransactions(long paymentDetailId);

        PaymentDetail GetPaymentDetail(string transactionId);
        IQueryable<TransactionActivity> GetTransactionsByToken(string token);
        TransactionActivity GetTransactionsForInstallmentReport(OrderInstallment installment);

        decimal CalculateSubscriptionChargeDiscount(List<TransactionActivity> transactions, bool isDiscount, bool isEntireOrder);

        decimal GetTransactionPrice(List<TransactionActivity> transactions);

        string GetOrderChargeDiscountName(List<TransactionActivity> transactions, bool isDiscount, bool isEntireOrder);

        string GetProgramScheduleTitle(List<TransactionActivity> transactions);

        string GetProgramType(List<TransactionActivity> transactions);

        string GetStartDateOrEndDateProgram(List<TransactionActivity> transactions, bool isStartDate);
        IEnumerable<TransactionActivity> GetPaymentTransactions(OrderInstallment installment);
        IEnumerable<TransactionActivity> GetRefundAutomaticallyTransactions(OrderInstallment installment);
        IEnumerable<TransactionActivity> GetPaymentTransactions(OrderItem orderItem);
        IEnumerable<TransactionActivity> GetRefundAutomaticallyTransactions(OrderItem orderItem);
        bool EnableUndoRefundToOrder(ClubUser clubUser, TransactionActivity transactionActivity);

        string GeneratGuidTransactionId();

    }
}
