﻿using System.Linq;
using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Helper.Element;
using Jumbula.AutomationTests.Helper.Element.Parent;
using NUnit.Framework;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Tests
{
    class AccessControlTests : BaseTest<AdminDashboardGeneralElements>
    {
        ParentDashboardGeneralElements parentDashboardGeneralElementsProvider;
        LoginElements loginElementProvider;
        public AccessControlTests(IWebDriver webDriver)
            : base(webDriver)
        {
            parentDashboardGeneralElementsProvider = new ElementProvider<ParentDashboardGeneralElements>().Get(WebDriver);
            loginElementProvider = new ElementProvider<LoginElements>().Get(WebDriver);
        }
        enum DashbaordType
        {
            School,
            Provider,
            District,
            Parent,
            NoRole,
            SchoolOfDistrict,
            EligibleDistrict
        }
        enum Role
        {
            Partner,
            Admin,
            Manager,
            Family,
            OnsiteSupportManager,
            Contributor,
            OnsiteCoordinator,
            RestrictedManager
        }
        enum StaffStatus
        {
            Active,
            Pending,
            Frozen,
            Deleted
        }
              
        public void PartnerStaffs_LoginInMembers()
        {
            PartnerStaff("activePartner@jumbula.com", "12345678", DashbaordType.School, Role.Partner, StaffStatus.Active);
            //PartnerStaff("activePartner@jumbula.com", "12345678", DashbaordType.Provider, Role.Partner, StaffStatus.Active);
            // PartnerStaff("activePartner@jumbula.com", "12345678", DashbaordType.District, Role.Partner, StaffStatus.Active);
            // PartnerStaff("activePartner@jumbula.com", "12345678", DashbaordType.Parent, Role.Partner, StaffStatus.Active);
            // PartnerStaff("activePartner@jumbula.com", "12345678", DashbaordType.NoRole, Role.Partner, StaffStatus.Active);
           //   PartnerStaff("activePartner@jumbula.com", "12345678", DashbaordType.Parent, Role.Partner, StaffStatus.Active, false);
            PartnerStaff("pendingPartner@jumbula.com", "12345678", DashbaordType.School, Role.Partner, StaffStatus.Pending);
            //PartnerStaff("pendingPartner@jumbula.com", "12345678", DashbaordType.Provider, Role.Partner, StaffStatus.Pending);
            //PartnerStaff("pendingPartner@jumbula.com", "12345678", DashbaordType.District, Role.Partner, StaffStatus.Pending);
            //PartnerStaff("pendingPartner@jumbula.com", "12345678", DashbaordType.Parent, Role.Partner, StaffStatus.Pending);
            //PartnerStaff("pendingPartner@jumbula.com", "12345678", DashbaordType.NoRole, Role.Partner, StaffStatus.Pending);
            //PartnerStaff("pendingPartner@jumbula.com", "12345678", DashbaordType.Parent, Role.Partner, StaffStatus.Pending, false);
           PartnerStaff("FrozenPartner@jumbula.com", "12345678", DashbaordType.School, Role.Partner, StaffStatus.Frozen);
            //PartnerStaff("FrozenPartner@jumbula.com", "12345678", DashbaordType.Provider, Role.Partner, StaffStatus.Frozen);
            //PartnerStaff("FrozenPartner@jumbula.com", "12345678", DashbaordType.District, Role.Partner, StaffStatus.Frozen);
            //PartnerStaff("FrozenPartner@jumbula.com", "12345678", DashbaordType.Parent, Role.Partner, StaffStatus.Frozen);
            //PartnerStaff("FrozenPartner@jumbula.com", "12345678", DashbaordType.NoRole, Role.Partner, StaffStatus.Frozen);
            //PartnerStaff("FrozenPartner@jumbula.com", "12345678", DashbaordType.Parent, Role.Partner, StaffStatus.Frozen, false);
            PartnerStaff("deletedPartner@jumbula.com", "12345678", DashbaordType.School, Role.Partner, StaffStatus.Deleted);
            //PartnerStaff("deletedPartner@jumbula.com", "12345678", DashbaordType.Provider, Role.Partner, StaffStatus.Deleted);
            //PartnerStaff("deletedPartner@jumbula.com", "12345678", DashbaordType.District, Role.Partner, StaffStatus.Deleted);
            //PartnerStaff("deletedPartner@jumbula.com", "12345678", DashbaordType.Parent, Role.Partner, StaffStatus.Deleted);
            //PartnerStaff("deletedPartner@jumbula.com", "12345678", DashbaordType.NoRole, Role.Partner, StaffStatus.Deleted);
            //PartnerStaff("deletedPartner@jumbula.com", "12345678", DashbaordType.Parent, Role.Partner, StaffStatus.Deleted, false);

            ///////////////////////////////

            //PartnerStaff("activeAdmin@jumbula.com", "12345678", DashbaordType.School, Role.Admin, StaffStatus.Active);
            //PartnerStaff("activeAdmin@jumbula.com", "12345678", DashbaordType.Provider, Role.Admin, StaffStatus.Active);
            //PartnerStaff("activeAdmin@jumbula.com", "12345678", DashbaordType.District, Role.Admin, StaffStatus.Active);
            //PartnerStaff("activeAdmin@jumbula.com", "12345678", DashbaordType.Parent, Role.Admin, StaffStatus.Active);
            //PartnerStaff("activeAdmin@jumbula.com", "12345678", DashbaordType.NoRole, Role.Admin, StaffStatus.Active);
            //PartnerStaff("activeAdmin@jumbula.com", "12345678", DashbaordType.Parent, Role.Admin, StaffStatus.Active, false);
            //PartnerStaff("pendingAdmin@jumbula.com", "12345678", DashbaordType.School, Role.Admin, StaffStatus.Pending);
            //PartnerStaff("pendingAdmin@jumbula.com", "12345678", DashbaordType.Provider, Role.Admin, StaffStatus.Pending);
            //PartnerStaff("pendingAdmin@jumbula.com", "12345678", DashbaordType.District, Role.Admin, StaffStatus.Pending);
            //PartnerStaff("pendingAdmin@jumbula.com", "12345678", DashbaordType.Parent, Role.Admin, StaffStatus.Pending);
            //PartnerStaff("pendingAdmin@jumbula.com", "12345678", DashbaordType.NoRole, Role.Admin, StaffStatus.Pending);
            //PartnerStaff("pendingAdmin@jumbula.com", "12345678", DashbaordType.Parent, Role.Admin, StaffStatus.Pending, false);
            //PartnerStaff("frozenAdmin@jumbula.com", "12345678", DashbaordType.School, Role.Admin, StaffStatus.Frozen);
            //PartnerStaff("frozenAdmin@jumbula.com", "12345678", DashbaordType.Provider, Role.Admin, StaffStatus.Frozen);
            //PartnerStaff("frozenAdmin@jumbula.com", "12345678", DashbaordType.District, Role.Admin, StaffStatus.Frozen);
            //PartnerStaff("frozenAdmin@jumbula.com", "12345678", DashbaordType.Parent, Role.Admin, StaffStatus.Frozen);
            //PartnerStaff("frozenAdmin@jumbula.com", "12345678", DashbaordType.NoRole, Role.Admin, StaffStatus.Frozen);
            //PartnerStaff("frozenAdmin@jumbula.com", "12345678", DashbaordType.Parent, Role.Admin, StaffStatus.Frozen, false);
            //PartnerStaff("deletedAdmin@jumbula.com", "12345678", DashbaordType.School, Role.Admin, StaffStatus.Deleted);
            //PartnerStaff("deletedAdmin@jumbula.com", "12345678", DashbaordType.Provider, Role.Admin, StaffStatus.Deleted);
            //PartnerStaff("deletedAdmin@jumbula.com", "12345678", DashbaordType.District, Role.Admin, StaffStatus.Deleted);
            //PartnerStaff("deletedAdmin@jumbula.com", "12345678", DashbaordType.Parent, Role.Admin, StaffStatus.Deleted);
            //PartnerStaff("deletedAdmin@jumbula.com", "12345678", DashbaordType.NoRole, Role.Admin, StaffStatus.Deleted);
            //PartnerStaff("deletedAdmin@jumbula.com", "12345678", DashbaordType.Parent, Role.Admin, StaffStatus.Deleted, false);
            ///////////////////////////////

            //PartnerStaff("activeManager@jumbula.com", "12345678", DashbaordType.School, Role.Manager, StaffStatus.Active);
            //PartnerStaff("activeManager@jumbula.com", "12345678", DashbaordType.Provider, Role.Manager, StaffStatus.Active);
            //PartnerStaff("activeManager@jumbula.com", "12345678", DashbaordType.District, Role.Manager, StaffStatus.Active);
            //PartnerStaff("activeManager@jumbula.com", "12345678", DashbaordType.Parent, Role.Manager, StaffStatus.Active);
            //PartnerStaff("activeManager@jumbula.com", "12345678", DashbaordType.NoRole, Role.Manager, StaffStatus.Active);
            //PartnerStaff("activeManager@jumbula.com", "12345678", DashbaordType.Parent, Role.Manager, StaffStatus.Active, false);
            //PartnerStaff("pendingManager@jumbula.com", "12345678", DashbaordType.School, Role.Manager, StaffStatus.Pending);
            //PartnerStaff("pendingManager@jumbula.com", "12345678", DashbaordType.Provider, Role.Manager, StaffStatus.Pending);
            //PartnerStaff("pendingManager@jumbula.com", "12345678", DashbaordType.District, Role.Manager, StaffStatus.Pending);
            //PartnerStaff("pendingManager@jumbula.com", "12345678", DashbaordType.Parent, Role.Manager, StaffStatus.Pending);
            //PartnerStaff("pendingManager@jumbula.com", "12345678", DashbaordType.NoRole, Role.Manager, StaffStatus.Pending);
            //PartnerStaff("pendingManager@jumbula.com", "12345678", DashbaordType.Parent, Role.Manager, StaffStatus.Pending, false);
            //PartnerStaff("frozenManager@jumbula.com", "12345678", DashbaordType.School, Role.Manager, StaffStatus.Frozen);
            //PartnerStaff("frozenManager@jumbula.com", "12345678", DashbaordType.Provider, Role.Manager, StaffStatus.Frozen);
            //PartnerStaff("frozenManager@jumbula.com", "12345678", DashbaordType.District, Role.Manager, StaffStatus.Frozen);
            //PartnerStaff("frozenManager@jumbula.com", "12345678", DashbaordType.Parent, Role.Manager, StaffStatus.Frozen);
            //PartnerStaff("frozenManager@jumbula.com", "12345678", DashbaordType.NoRole, Role.Manager, StaffStatus.Frozen);
            //PartnerStaff("frozenManager@jumbula.com", "12345678", DashbaordType.Parent, Role.Manager, StaffStatus.Frozen, false);
            //PartnerStaff("deletedManager@jumbula.com", "12345678", DashbaordType.School, Role.Manager, StaffStatus.Deleted);
            //PartnerStaff("deletedManager@jumbula.com", "12345678", DashbaordType.Provider, Role.Manager, StaffStatus.Deleted);
            //PartnerStaff("deletedManager@jumbula.com", "12345678", DashbaordType.District, Role.Manager, StaffStatus.Deleted);
            //PartnerStaff("deletedManager@jumbula.com", "12345678", DashbaordType.Parent, Role.Manager, StaffStatus.Deleted);
            //PartnerStaff("deletedManager@jumbula.com", "12345678", DashbaordType.NoRole, Role.Manager, StaffStatus.Deleted);
            //PartnerStaff("deletedManager@jumbula.com", "12345678", DashbaordType.Parent, Role.Manager, StaffStatus.Deleted, false);


        }
        public void DistrictStaffs_LoginInMembers()
        {
            //DistrictStaff("districtActiveAdmin@jumbula.com", "12345678", DashbaordType.SchoolOfDistrict, Role.Admin, StaffStatus.Active);
            //DistrictStaff("districtActiveAdmin@jumbula.com", "12345678", DashbaordType.School, Role.Admin, StaffStatus.Active);
            //DistrictStaff("districtActiveAdmin@jumbula.com", "12345678", DashbaordType.Provider, Role.Admin, StaffStatus.Active);
            //DistrictStaff("districtActiveAdmin@jumbula.com", "12345678", DashbaordType.District, Role.Admin, StaffStatus.Active);
            //DistrictStaff("districtActiveAdmin@jumbula.com", "12345678", DashbaordType.EligibleDistrict, Role.Admin, StaffStatus.Active);
            //DistrictStaff("districtActiveAdmin@jumbula.com", "12345678", DashbaordType.Parent, Role.Admin, StaffStatus.Active);
            //DistrictStaff("districtActiveAdmin@jumbula.com", "12345678", DashbaordType.NoRole, Role.Admin, StaffStatus.Active);
            //DistrictStaff("districtActiveAdmin@jumbula.com", "12345678", DashbaordType.Parent, Role.Admin, StaffStatus.Active, false);
            //DistrictStaff("DistrictPendingAdmin@jumbula.com", "12345678", DashbaordType.SchoolOfDistrict, Role.Admin, StaffStatus.Pending);
            //DistrictStaff("DistrictPendingAdmin@jumbula.com", "12345678", DashbaordType.School, Role.Admin, StaffStatus.Pending);
            //DistrictStaff("DistrictPendingAdmin@jumbula.com", "12345678", DashbaordType.Provider, Role.Admin, StaffStatus.Pending);
            //DistrictStaff("DistrictPendingAdmin@jumbula.com", "12345678", DashbaordType.District, Role.Admin, StaffStatus.Frozen);
            //DistrictStaff("DistrictPendingAdmin@jumbula.com", "12345678", DashbaordType.EligibleDistrict, Role.Admin, StaffStatus.Frozen);
            //DistrictStaff("DistrictPendingAdmin@jumbula.com", "12345678", DashbaordType.Parent, Role.Admin, StaffStatus.Frozen);
            //DistrictStaff("DistrictPendingAdmin@jumbula.com", "12345678", DashbaordType.NoRole, Role.Admin, StaffStatus.Frozen);
            //DistrictStaff("DistrictPendingAdmin@jumbula.com", "12345678", DashbaordType.Parent, Role.Admin, StaffStatus.Frozen, false);
            //DistrictStaff("DistrictFrozenAdmin@jumbula.com", "12345678", DashbaordType.SchoolOfDistrict, Role.Admin, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenAdmin@jumbula.com", "12345678", DashbaordType.School, Role.Admin, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenAdmin@jumbula.com", "12345678", DashbaordType.Provider, Role.Admin, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenAdmin@jumbula.com", "12345678", DashbaordType.District, Role.Admin, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenAdmin@jumbula.com", "12345678", DashbaordType.EligibleDistrict, Role.Admin, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenAdmin@jumbula.com", "12345678", DashbaordType.Parent, Role.Admin, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenAdmin@jumbula.com", "12345678", DashbaordType.NoRole, Role.Admin, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenAdmin@jumbula.com", "12345678", DashbaordType.Parent, Role.Admin, StaffStatus.Frozen, false);
            //DistrictStaff("DistrictDeletedAdmin@jumbula.com", "12345678", DashbaordType.SchoolOfDistrict, Role.Admin, StaffStatus.Pending);
            //DistrictStaff("DistrictDeletedAdmin@jumbula.com", "12345678", DashbaordType.School, Role.Admin, StaffStatus.Pending);
            //DistrictStaff("DistrictDeletedAdmin@jumbula.com", "12345678", DashbaordType.Provider, Role.Admin, StaffStatus.Pending);
            //DistrictStaff("DistrictDeletedAdmin@jumbula.com", "12345678", DashbaordType.District, Role.Admin, StaffStatus.Frozen);
            //DistrictStaff("DistrictDeletedAdmin@jumbula.com", "12345678", DashbaordType.EligibleDistrict, Role.Admin, StaffStatus.Frozen);
            //DistrictStaff("DistrictDeletedAdmin@jumbula.com", "12345678", DashbaordType.Parent, Role.Admin, StaffStatus.Frozen);
            //DistrictStaff("DistrictDeletedAdmin@jumbula.com", "12345678", DashbaordType.NoRole, Role.Admin, StaffStatus.Frozen);
            //DistrictStaff("DistrictDeletedAdmin@jumbula.com", "12345678", DashbaordType.Parent, Role.Admin, StaffStatus.Frozen, false);
            //////////////////
            //DistrictStaff("districtActiveManager@jumbula.com", "12345678", DashbaordType.SchoolOfDistrict, Role.Manager, StaffStatus.Active);
            //DistrictStaff("districtActiveManager@jumbula.com", "12345678", DashbaordType.School, Role.Manager, StaffStatus.Active);
            //DistrictStaff("districtActiveManager@jumbula.com", "12345678", DashbaordType.Provider, Role.Manager, StaffStatus.Active);
            //DistrictStaff("districtActiveManager@jumbula.com", "12345678", DashbaordType.District, Role.Manager, StaffStatus.Active);
            //DistrictStaff("districtActiveManager@jumbula.com", "12345678", DashbaordType.EligibleDistrict, Role.Manager, StaffStatus.Active);
            //DistrictStaff("districtActiveManager@jumbula.com", "12345678", DashbaordType.Parent, Role.Manager, StaffStatus.Active);
            //DistrictStaff("districtActiveManager@jumbula.com", "12345678", DashbaordType.NoRole, Role.Manager, StaffStatus.Active);
            //DistrictStaff("districtActiveManager@jumbula.com", "12345678", DashbaordType.Parent, Role.Manager, StaffStatus.Active, false);
            //DistrictStaff("DistrictPendingManager@jumbula.com", "12345678", DashbaordType.SchoolOfDistrict, Role.Manager, StaffStatus.Pending);
            //DistrictStaff("DistrictPendingManager@jumbula.com", "12345678", DashbaordType.School, Role.Manager, StaffStatus.Pending);
            //DistrictStaff("DistrictPendingManager@jumbula.com", "12345678", DashbaordType.Provider, Role.Manager, StaffStatus.Pending);
            //DistrictStaff("DistrictPendingManager@jumbula.com", "12345678", DashbaordType.District, Role.Manager, StaffStatus.Frozen);
            //DistrictStaff("DistrictPendingManager@jumbula.com", "12345678", DashbaordType.EligibleDistrict, Role.Manager, StaffStatus.Frozen);
            //DistrictStaff("DistrictPendingManager@jumbula.com", "12345678", DashbaordType.Parent, Role.Manager, StaffStatus.Frozen);
            //DistrictStaff("DistrictPendingManager@jumbula.com", "12345678", DashbaordType.NoRole, Role.Manager, StaffStatus.Frozen);
            //DistrictStaff("DistrictPendingManager@jumbula.com", "12345678", DashbaordType.Parent, Role.Manager, StaffStatus.Frozen, false);
            //DistrictStaff("DistrictFrozenManager@jumbula.com", "12345678", DashbaordType.SchoolOfDistrict, Role.Manager, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenManager@jumbula.com", "12345678", DashbaordType.School, Role.Manager, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenManager@jumbula.com", "12345678", DashbaordType.Provider, Role.Manager, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenManager@jumbula.com", "12345678", DashbaordType.District, Role.Manager, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenManager@jumbula.com", "12345678", DashbaordType.EligibleDistrict, Role.Manager, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenManager@jumbula.com", "12345678", DashbaordType.Parent, Role.Manager, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenManager@jumbula.com", "12345678", DashbaordType.NoRole, Role.Manager, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenManager@jumbula.com", "12345678", DashbaordType.Parent, Role.Manager, StaffStatus.Frozen, false);
            //DistrictStaff("DistrictDeletedManager@jumbula.com", "12345678", DashbaordType.SchoolOfDistrict, Role.Manager, StaffStatus.Pending);
            //DistrictStaff("DistrictDeletedManager@jumbula.com", "12345678", DashbaordType.School, Role.Manager, StaffStatus.Pending);
            //DistrictStaff("DistrictDeletedManager@jumbula.com", "12345678", DashbaordType.Provider, Role.Manager, StaffStatus.Pending);
            //DistrictStaff("DistrictDeletedManager@jumbula.com", "12345678", DashbaordType.District, Role.Manager, StaffStatus.Frozen);
            //DistrictStaff("DistrictDeletedManager@jumbula.com", "12345678", DashbaordType.EligibleDistrict, Role.Manager, StaffStatus.Frozen);
            //DistrictStaff("DistrictDeletedManager@jumbula.com", "12345678", DashbaordType.Parent, Role.Manager, StaffStatus.Frozen);
            //DistrictStaff("DistrictDeletedManager@jumbula.com", "12345678", DashbaordType.NoRole, Role.Manager, StaffStatus.Frozen);
            //DistrictStaff("DistrictDeletedManager@jumbula.com", "12345678", DashbaordType.Parent, Role.Manager, StaffStatus.Frozen, false);

            //////////////////
            //DistrictStaff("districtActiveOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.SchoolOfDistrict, Role.OnsiteSupportManager, StaffStatus.Active);
            //DistrictStaff("districtActiveOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.School, Role.OnsiteSupportManager, StaffStatus.Active);
            //DistrictStaff("districtActiveOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.Provider, Role.OnsiteSupportManager, StaffStatus.Active);
            //DistrictStaff("districtActiveOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.District, Role.OnsiteSupportManager, StaffStatus.Active);
            //DistrictStaff("districtActiveOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.EligibleDistrict, Role.OnsiteSupportManager, StaffStatus.Active);
            //DistrictStaff("districtActiveOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.Parent, Role.OnsiteSupportManager, StaffStatus.Active);
            //DistrictStaff("districtActiveOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.NoRole, Role.OnsiteSupportManager, StaffStatus.Active);
            //DistrictStaff("districtActiveOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.Parent, Role.OnsiteSupportManager, StaffStatus.Active, false);
            //DistrictStaff("DistrictPendingOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.SchoolOfDistrict, Role.OnsiteSupportManager, StaffStatus.Pending);
            //DistrictStaff("DistrictPendingOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.School, Role.OnsiteSupportManager, StaffStatus.Pending);
            //DistrictStaff("DistrictPendingOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.Provider, Role.OnsiteSupportManager, StaffStatus.Pending);
            //DistrictStaff("DistrictPendingOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.District, Role.OnsiteSupportManager, StaffStatus.Frozen);
            //DistrictStaff("DistrictPendingOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.EligibleDistrict, Role.OnsiteSupportManager, StaffStatus.Frozen);
            //DistrictStaff("DistrictPendingOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.Parent, Role.OnsiteSupportManager, StaffStatus.Frozen);
            //DistrictStaff("DistrictPendingOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.NoRole, Role.OnsiteSupportManager, StaffStatus.Frozen);
            //DistrictStaff("DistrictPendingOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.Parent, Role.OnsiteSupportManager, StaffStatus.Frozen, false);
            //DistrictStaff("DistrictFrozenOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.SchoolOfDistrict, Role.OnsiteSupportManager, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.School, Role.OnsiteSupportManager, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.Provider, Role.OnsiteSupportManager, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.District, Role.OnsiteSupportManager, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.EligibleDistrict, Role.OnsiteSupportManager, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.Parent, Role.OnsiteSupportManager, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.NoRole, Role.OnsiteSupportManager, StaffStatus.Frozen);
            //DistrictStaff("DistrictFrozenOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.Parent, Role.OnsiteSupportManager, StaffStatus.Frozen, false);
            //DistrictStaff("DistrictDeletedOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.SchoolOfDistrict, Role.OnsiteSupportManager, StaffStatus.Pending);
            //DistrictStaff("DistrictDeletedOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.School, Role.OnsiteSupportManager, StaffStatus.Pending);
            //DistrictStaff("DistrictDeletedOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.Provider, Role.OnsiteSupportManager, StaffStatus.Pending);
            //DistrictStaff("DistrictDeletedOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.District, Role.OnsiteSupportManager, StaffStatus.Frozen);
            //DistrictStaff("DistrictDeletedOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.EligibleDistrict, Role.OnsiteSupportManager, StaffStatus.Frozen);
            //DistrictStaff("DistrictDeletedOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.Parent, Role.OnsiteSupportManager, StaffStatus.Frozen);
            //DistrictStaff("DistrictDeletedOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.NoRole, Role.OnsiteSupportManager, StaffStatus.Frozen);
            //DistrictStaff("DistrictDeletedOnsiteSupportManager@jumbula.com", "12345678", DashbaordType.Parent, Role.OnsiteSupportManager, StaffStatus.Frozen, false);
        }
        
        public void PartnerStaffs_LoginInPartner()
        {
            WebDriver.Navigate().GoToUrl(DataDriver.CreateLoginURL("partnerautomationtest"));
            WebDriver.Manage().Window.Maximize();
                       
            LoginInPartner("activeAdmin@jumbula.com", "12345678", Role.Admin, false);
            LoginInPartner("activeContributor@jumbula.com", "12345678", Role.Contributor , false);
            LoginInPartner("activeManager@jumbula.com", "12345678", Role.Manager, false);
            LoginInPartner("activeOnsiteCoordinator@jumbula.com	", "12345678", Role.OnsiteCoordinator, false);
            LoginInPartner("activeOnsiteSupportManager@jumbula.com", "12345678", Role.OnsiteSupportManager, false);
            LoginInPartner("activeRestrictedManager@jumbula.com", "12345678", Role.RestrictedManager, false);
            LoginInPartner("activePartner@jumbula.com", "12345678", Role.Partner, true);

            WebDriver.Close();
        }

        private void LoginInPartner(string username, string password, Role role, bool wait=true)
        {
            var logingTest = new LoginTest(WebDriver);
            logingTest.Login(username, password, wait);

            if (role==Role.Partner)
            {
                Elements.ClubName.Click();
                ExtraAPI.Wait(2000);
                Elements.DomainLink.Click();
                ExtraAPI.Wait(2000);
                                
                // Check username and role
                Assert.AreEqual(username, Elements.Username.Text);
                Assert.AreEqual(role.ToString(), Elements.Role.Text);
            }
            else
            {
                Assert.AreEqual("Invalid login attempt.", loginElementProvider.UsernameError.Text);
            }
        }
        private void Login(string username, string password, string url)
        {
            //Navigate
            WebDriver.Navigate().GoToUrl(url);
            try
            {
                WebDriver.Manage().Window.Maximize();
                ExtraAPI.Wait(5000);
            }
            catch
            
            {

            }
            
            // login
            var logingTest = new LoginTest(WebDriver);
            logingTest.Login(username, password);
        }
        private void PartnerStaff(string username, string password, DashbaordType dashboardType, Role role, StaffStatus status, bool IsHomePage = true)
        {
            var clubs = DataDriver.Parents;
            if (status == StaffStatus.Active)
            {
                clubs = clubs.Concat(DataDriver.PartnerSchools).Concat(DataDriver.PartnerProviders).Concat(DataDriver.PartnerDistricts).ToList();
                if (role == Role.Partner)
                {
                    clubs = clubs.Concat(DataDriver.Partners).ToList();
                }
            }
            var EligibleClubs = clubs;

            var secondRole = role;
            if (role == Role.Partner)
            {
                secondRole = Role.Admin;
            }
            switch (dashboardType)
            {
                case DashbaordType.School:
                    {

                        Login(username, password, DataDriver.CreateLoginURL(DataDriver.PartnerSchools[0].Domain));
                        EligibleClubs = clubs.Where(d => d.Name != DataDriver.PartnerSchools[0].Name).OrderBy(d => d.Name).ToList();
                        break;
                    }
                case DashbaordType.Provider:
                    {

                        Login(username, password, DataDriver.CreateLoginURL(DataDriver.PartnerProviders[0].Domain));
                        EligibleClubs = clubs.Where(d => d.Name != DataDriver.PartnerProviders[0].Name).OrderBy(d => d.Name).ToList();
                        break;
                    }
                case DashbaordType.District:
                    {

                        Login(username, password, DataDriver.CreateLoginURL(DataDriver.PartnerDistricts[0].Domain));
                        EligibleClubs = clubs.Where(d => d.Name != DataDriver.PartnerDistricts[0].Name).OrderBy(d => d.Name).ToList();
                        break;
                    }

                case DashbaordType.Parent:
                    {

                        Login(username, password, DataDriver.CreateLoginURL(DataDriver.Parents[0].Domain));
                        if (!IsHomePage)
                        {
                            parentDashboardGeneralElementsProvider.InvoiceLink.Click();
                            ExtraAPI.Wait(5000);
                        }
                        EligibleClubs = clubs.Where(d => d.Name != DataDriver.Parents[0].Name).OrderBy(d => d.Name).ToList();
                        break;
                    }
                case DashbaordType.NoRole:
                    {

                        Login(username, password, DataDriver.CreateLoginURL("automationtest3"));
                        EligibleClubs = clubs.OrderBy(d => d.Name).ToList();
                        break;
                    }
            }

            if (dashboardType != DashbaordType.Parent && dashboardType != DashbaordType.NoRole && status == StaffStatus.Active)
            {
                Elements.ClubName.Click();
                ExtraAPI.Wait(2000);
                Elements.DomainLink.Click();
                ExtraAPI.Wait(2000);

                // Check domains

                for (int i = 0; i < EligibleClubs.Count(); i++)
                {
                    Assert.AreEqual(EligibleClubs[i].Name, Elements.Domains[i].Text);
                }

                // Check username and role
                Assert.AreEqual(username, Elements.Username.Text);
                Assert.AreEqual(secondRole.ToString(), Elements.Role.Text);
            }
            else
            {
                parentDashboardGeneralElementsProvider.UsernameLink.Click();
                ExtraAPI.Wait(2000);

                // Check domains
                for (int i = 0; i < EligibleClubs.Count(); i++)
                {
                    if (DataDriver.Parents.Contains(EligibleClubs[i]))
                    {
                        Assert.AreEqual(string.Format("{0} ({1})", EligibleClubs[i].Name, Role.Family.ToString()), parentDashboardGeneralElementsProvider.Domains[i].Text);
                    }
                    else
                    {
                        Assert.AreEqual(string.Format("{0} ({1})", EligibleClubs[i].Name, role), parentDashboardGeneralElementsProvider.Domains[i].Text);
                    }
                }

                // Check username
                Assert.AreEqual(username, parentDashboardGeneralElementsProvider.Username.Text);
            }


            if (status == StaffStatus.Active)
            {
                // Go to school
                if (dashboardType == DashbaordType.School)
                {
                    Elements.GetDomain(DataDriver.PartnerSchools[1].Name).Click();
                }
                else if (dashboardType == DashbaordType.Parent || dashboardType == DashbaordType.NoRole || status != StaffStatus.Active)
                {
                    parentDashboardGeneralElementsProvider.GetDomain(DataDriver.PartnerSchools[0].Name).Click();
                }
                else
                {
                    Elements.GetDomain(DataDriver.PartnerSchools[0].Name).Click();
                }
                ExtraAPI.Wait(30000);
                WebDriver.SwitchTo().Window(WebDriver.WindowHandles[1]);
                Elements.ClubName.Click();
                ExtraAPI.Wait(2000);
                Elements.DomainLink.Click();
                ExtraAPI.Wait(2000);
                // Check domains
                if (dashboardType == DashbaordType.School)
                {
                    EligibleClubs = clubs.Where(d => d.Name != DataDriver.PartnerSchools[1].Name).OrderBy(d => d.Name).ToList();

                }
                else
                {
                    EligibleClubs = clubs.Where(d => d.Name != DataDriver.PartnerSchools[0].Name).OrderBy(d => d.Name).ToList();

                }

                for (int i = 0; i < EligibleClubs.Count(); i++)
                {
                    Assert.AreEqual(EligibleClubs[i].Name, Elements.Domains[i].Text);
                }
                // Check username and role
                Assert.AreEqual(username, Elements.Username.Text);
                Assert.AreEqual(secondRole.ToString(), Elements.Role.Text);
                WebDriver.Close();
                // Switch back to first tab
                WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
                if (dashboardType != DashbaordType.Parent && dashboardType != DashbaordType.NoRole && status == StaffStatus.Active)
                {
                    Elements.ClubName.Click();
                    ExtraAPI.Wait(2000);
                    Elements.DomainLink.Click();
                    ExtraAPI.Wait(2000);
                }
                else
                {
                    parentDashboardGeneralElementsProvider.UsernameLink.Click();
                    ExtraAPI.Wait(2000);
                }
                // Go to provider
                if (dashboardType == DashbaordType.Provider)
                {
                    Elements.GetDomain(DataDriver.PartnerProviders[1].Name).Click();
                }
                else if (dashboardType == DashbaordType.Parent || dashboardType == DashbaordType.NoRole || status != StaffStatus.Active)
                {
                    parentDashboardGeneralElementsProvider.GetDomain(DataDriver.PartnerProviders[0].Name).Click();
                }
                else
                {
                    Elements.GetDomain(DataDriver.PartnerProviders[0].Name).Click();
                }
                ExtraAPI.Wait(30000);
                WebDriver.SwitchTo().Window(WebDriver.WindowHandles[1]);
                Elements.ClubName.Click();
                ExtraAPI.Wait(2000);
                Elements.DomainLink.Click();
                ExtraAPI.Wait(2000);
                // Check domains
                if (dashboardType == DashbaordType.Provider)
                {
                    EligibleClubs = clubs.Where(d => d.Name != DataDriver.PartnerProviders[1].Name).OrderBy(d => d.Name).ToList();

                }
                else
                {
                    EligibleClubs = clubs.Where(d => d.Name != DataDriver.PartnerProviders[0].Name).OrderBy(d => d.Name).ToList();

                }
                for (int i = 0; i < EligibleClubs.Count(); i++)
                {
                    Assert.AreEqual(EligibleClubs[i].Name, Elements.Domains[i].Text);
                }
                // Check username and role
                Assert.AreEqual(username, Elements.Username.Text);
                Assert.AreEqual(secondRole.ToString(), Elements.Role.Text);
                WebDriver.Close();
                // Switch back to first tab
                WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
                if (dashboardType != DashbaordType.Parent && dashboardType != DashbaordType.NoRole && status == StaffStatus.Active)
                {
                    Elements.ClubName.Click();
                    ExtraAPI.Wait(2000);
                    Elements.DomainLink.Click();
                    ExtraAPI.Wait(2000);
                }
                else
                {
                    parentDashboardGeneralElementsProvider.UsernameLink.Click();
                    ExtraAPI.Wait(2000);
                }
                // Go to distrcit
                if (dashboardType == DashbaordType.District)
                {
                    Elements.GetDomain(DataDriver.PartnerDistricts[1].Name).Click();
                }
                else if (dashboardType == DashbaordType.Parent || dashboardType == DashbaordType.NoRole || status != StaffStatus.Active)
                {
                    parentDashboardGeneralElementsProvider.GetDomain(DataDriver.PartnerDistricts[0].Name).Click();
                }
                else
                {
                    Elements.GetDomain(DataDriver.PartnerDistricts[0].Name).Click();
                }
                ExtraAPI.Wait(30000);

                WebDriver.SwitchTo().Window(WebDriver.WindowHandles[1]);
                Elements.ClubName.Click();
                ExtraAPI.Wait(2000);
                Elements.DomainLink.Click();
                ExtraAPI.Wait(2000);
                // Check domains
                if (dashboardType == DashbaordType.District)
                {
                    EligibleClubs = clubs.Where(d => d.Name != DataDriver.PartnerDistricts[1].Name).OrderBy(d => d.Name).ToList();

                }
                else
                {
                    EligibleClubs = clubs.Where(d => d.Name != DataDriver.PartnerDistricts[0].Name).OrderBy(d => d.Name).ToList();

                }
                for (int i = 0; i < EligibleClubs.Count(); i++)
                {
                    Assert.AreEqual(EligibleClubs[i].Name, Elements.Domains[i].Text);
                }
                // Check username and role
                Assert.AreEqual(username, Elements.Username.Text);
                Assert.AreEqual(secondRole.ToString(), Elements.Role.Text);

                WebDriver.Close();

                // Switch back to first tab
                WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
                if (dashboardType != DashbaordType.Parent && dashboardType != DashbaordType.NoRole && status == StaffStatus.Active)
                {
                    Elements.ClubName.Click();
                    ExtraAPI.Wait(2000);
                    Elements.DomainLink.Click();
                    ExtraAPI.Wait(2000);
                }
                else
                {
                    parentDashboardGeneralElementsProvider.UsernameLink.Click();
                    ExtraAPI.Wait(2000);
                }
            }

            // Go to parent

            if (dashboardType == DashbaordType.Parent)
            {
                parentDashboardGeneralElementsProvider.GetDomain(DataDriver.Parents[1].Name).Click();
            }
            else if (dashboardType == DashbaordType.NoRole || status != StaffStatus.Active)
            {
                parentDashboardGeneralElementsProvider.GetDomain(DataDriver.Parents[0].Name).Click();
            }
            else
            {
                Elements.GetDomain(DataDriver.Parents[0].Name).Click();
            }
            ExtraAPI.Wait(30000);
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[1]);
            parentDashboardGeneralElementsProvider.UsernameLink.Click();
            ExtraAPI.Wait(2000);

            // Check domains
            if (dashboardType == DashbaordType.Parent)
            {
                EligibleClubs = clubs.Where(d => d.Name != DataDriver.Parents[1].Name).OrderBy(d => d.Name).ToList();

            }
            else
            {
                EligibleClubs = clubs.Where(d => d.Name != DataDriver.Parents[0].Name).OrderBy(d => d.Name).ToList();

            }

            for (int i = 0; i < EligibleClubs.Count(); i++)
            {
                if (DataDriver.Parents.Contains(EligibleClubs[i]))
                {
                    Assert.AreEqual(string.Format("{0} ({1})", EligibleClubs[i].Name, Role.Family.ToString()), parentDashboardGeneralElementsProvider.Domains[i].Text);
                }
                else
                {
                    Assert.AreEqual(string.Format("{0} ({1})", EligibleClubs[i].Name, role), parentDashboardGeneralElementsProvider.Domains[i].Text);
                }

            }
            // Check username
            Assert.AreEqual(username, parentDashboardGeneralElementsProvider.Username.Text);

            WebDriver.Close();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
        }
        private void DistrictStaff(string username, string password, DashbaordType dashboardType, Role role, StaffStatus status, bool IsHomePage = true)
        {
            var clubs = DataDriver.Parents;

            if (status == StaffStatus.Active)
            {
                clubs = clubs.Concat(DataDriver.SchoolsOfDistrict).ToList();
                clubs.Add(new DataDriver.club { Name = "School district2", Domain = "schooldistrict2" });
            }
            var EligibleClubs = clubs;

            switch (dashboardType)
            {
                case DashbaordType.SchoolOfDistrict:
                    {

                        Login(username, password, DataDriver.CreateLoginURL(DataDriver.SchoolsOfDistrict[0].Domain));
                        EligibleClubs = clubs.Where(d => d.Name != DataDriver.SchoolsOfDistrict[0].Name).OrderBy(d => d.Name).ToList();
                        break;
                    }

                case DashbaordType.Parent:
                    {

                        Login(username, password, DataDriver.CreateLoginURL(DataDriver.Parents[0].Domain));
                        if (!IsHomePage)
                        {
                            parentDashboardGeneralElementsProvider.InvoiceLink.Click();
                            ExtraAPI.Wait(5000);
                        }
                        EligibleClubs = clubs.Where(d => d.Name != DataDriver.Parents[0].Name).OrderBy(d => d.Name).ToList();
                        break;
                    }
                case DashbaordType.NoRole:
                    {

                        Login(username, password, DataDriver.CreateLoginURL("automationtest3"));
                        EligibleClubs = clubs.OrderBy(d => d.Name).ToList();
                        break;
                    }
                case DashbaordType.Provider:
                    {

                        Login(username, password, DataDriver.CreateLoginURL(DataDriver.PartnerProviders[0].Domain));
                        EligibleClubs = clubs.OrderBy(d => d.Name).ToList();
                        break;
                    }
                case DashbaordType.EligibleDistrict:
                    {
                        var district = DataDriver.PartnerDistricts.Where(d => d.Domain == "schooldistrict2").First();
                        Login(username, password, DataDriver.CreateLoginURL(district.Domain));
                        EligibleClubs = clubs.Where(d => d.Name != "School district2").OrderBy(d => d.Name).ToList();
                        break;
                    }
                case DashbaordType.District:
                    {
                        var districts = DataDriver.PartnerDistricts.Where(d => d.Domain != "schooldistrict2").ToList();
                        Login(username, password, DataDriver.CreateLoginURL(districts[0].Domain));
                        EligibleClubs = clubs.OrderBy(d => d.Name).ToList();
                        break;
                    }
                case DashbaordType.School:
                    {
                        var schools = DataDriver.PartnerSchools.Except(DataDriver.SchoolsOfDistrict).ToList();
                        Login(username, password, DataDriver.CreateLoginURL(schools[0].Domain));
                        break;
                    }
            }

            if ((dashboardType == DashbaordType.SchoolOfDistrict || dashboardType == DashbaordType.EligibleDistrict) && status == StaffStatus.Active)
            {
                Elements.ClubName.Click();
                ExtraAPI.Wait(2000);
                Elements.DomainLink.Click();
                ExtraAPI.Wait(2000);

                // Check domains

                for (int i = 0; i < EligibleClubs.Count(); i++)
                {
                    Assert.AreEqual(EligibleClubs[i].Name, Elements.Domains[i].Text);
                }

                // Check username and role
                Assert.AreEqual(username, Elements.Username.Text);
                Assert.AreEqual(role.ToString(), Elements.Role.Text);
            }
            else
            {
                parentDashboardGeneralElementsProvider.UsernameLink.Click();
                ExtraAPI.Wait(2000);

                // Check domains
                for (int i = 0; i < EligibleClubs.Count(); i++)
                {
                    if (DataDriver.Parents.Contains(EligibleClubs[i]))
                    {
                        Assert.AreEqual(string.Format("{0} ({1})", EligibleClubs[i].Name, Role.Family.ToString()), parentDashboardGeneralElementsProvider.Domains[i].Text);
                    }
                    else
                    {
                        Assert.AreEqual(string.Format("{0} ({1})", EligibleClubs[i].Name, role), parentDashboardGeneralElementsProvider.Domains[i].Text);
                    }
                }

                // Check username
                Assert.AreEqual(username, parentDashboardGeneralElementsProvider.Username.Text);
            }


            if (status == StaffStatus.Active)
            {
                // Go to school
                if (dashboardType == DashbaordType.SchoolOfDistrict)
                {
                    Elements.GetDomain(DataDriver.SchoolsOfDistrict[1].Name).Click();
                }
                else if (dashboardType == DashbaordType.EligibleDistrict)
                {
                    Elements.GetDomain(DataDriver.SchoolsOfDistrict[0].Name).Click();
                }
                else
                {
                    parentDashboardGeneralElementsProvider.GetDomain(DataDriver.SchoolsOfDistrict[0].Name).Click();
                }

                ExtraAPI.Wait(30000);
                WebDriver.SwitchTo().Window(WebDriver.WindowHandles[1]);
                Elements.ClubName.Click();
                ExtraAPI.Wait(2000);
                Elements.DomainLink.Click();
                ExtraAPI.Wait(2000);
                // Check domains
                if (dashboardType == DashbaordType.SchoolOfDistrict)
                {
                    EligibleClubs = clubs.Where(d => d.Name != DataDriver.SchoolsOfDistrict[1].Name).OrderBy(d => d.Name).ToList();

                }
                else
                {
                    EligibleClubs = clubs.Where(d => d.Name != DataDriver.SchoolsOfDistrict[0].Name).OrderBy(d => d.Name).ToList();

                }

                for (int i = 0; i < EligibleClubs.Count(); i++)
                {
                    Assert.AreEqual(EligibleClubs[i].Name, Elements.Domains[i].Text);
                }
                // Check username and role
                Assert.AreEqual(username, Elements.Username.Text);
                Assert.AreEqual(role.ToString(), Elements.Role.Text);
                WebDriver.Close();
                // Switch back to first tab
                WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
                // Go to distrcit
                if (dashboardType != DashbaordType.EligibleDistrict)
                {
                    if (dashboardType == DashbaordType.SchoolOfDistrict)
                    {
                        Elements.ClubName.Click();
                        ExtraAPI.Wait(2000);
                        Elements.DomainLink.Click();
                        ExtraAPI.Wait(2000);
                        Elements.GetDomain("School district2").Click();
                    }
                    else
                    {
                        parentDashboardGeneralElementsProvider.UsernameLink.Click();
                        ExtraAPI.Wait(2000);
                        parentDashboardGeneralElementsProvider.GetDomain("School district2").Click();
                    }

                    ExtraAPI.Wait(30000);

                    WebDriver.SwitchTo().Window(WebDriver.WindowHandles[1]);
                    Elements.ClubName.Click();
                    ExtraAPI.Wait(2000);
                    Elements.DomainLink.Click();
                    ExtraAPI.Wait(2000);
                    // Check domains
                    EligibleClubs = clubs.Where(d => d.Name != "School district2").OrderBy(d => d.Name).ToList();


                    for (int i = 0; i < EligibleClubs.Count(); i++)
                    {
                        Assert.AreEqual(EligibleClubs[i].Name, Elements.Domains[i].Text);
                    }
                    // Check username and role
                    Assert.AreEqual(username, Elements.Username.Text);
                    Assert.AreEqual(role.ToString(), Elements.Role.Text);

                    WebDriver.Close();
                }
                // Switch back to first tab
                WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
                if (dashboardType == DashbaordType.SchoolOfDistrict || dashboardType == DashbaordType.EligibleDistrict)
                {
                    Elements.ClubName.Click();
                    ExtraAPI.Wait(2000);
                    Elements.DomainLink.Click();
                    ExtraAPI.Wait(2000);
                }
                else
                {
                    parentDashboardGeneralElementsProvider.UsernameLink.Click();
                    ExtraAPI.Wait(2000);
                }
            }

            // Go to parent

            if (dashboardType == DashbaordType.Parent)
            {
                parentDashboardGeneralElementsProvider.GetDomain(DataDriver.Parents[1].Name).Click();
            }
            else if ((dashboardType == DashbaordType.SchoolOfDistrict || dashboardType == DashbaordType.EligibleDistrict) && status == StaffStatus.Active)
            {
                Elements.GetDomain(DataDriver.Parents[0].Name).Click();

            }
            else
            {
                parentDashboardGeneralElementsProvider.GetDomain(DataDriver.Parents[0].Name).Click();
            }
            ExtraAPI.Wait(30000);
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[1]);
            parentDashboardGeneralElementsProvider.UsernameLink.Click();
            ExtraAPI.Wait(2000);

            // Check domains
            if (dashboardType == DashbaordType.Parent)
            {
                EligibleClubs = clubs.Where(d => d.Name != DataDriver.Parents[1].Name).OrderBy(d => d.Name).ToList();

            }
            else
            {
                EligibleClubs = clubs.Where(d => d.Name != DataDriver.Parents[0].Name).OrderBy(d => d.Name).ToList();

            }

            for (int i = 0; i < EligibleClubs.Count(); i++)
            {
                if (DataDriver.Parents.Contains(EligibleClubs[i]))
                {
                    Assert.AreEqual(string.Format("{0} ({1})", EligibleClubs[i].Name, Role.Family.ToString()), parentDashboardGeneralElementsProvider.Domains[i].Text);
                }
                else
                {
                    Assert.AreEqual(string.Format("{0} ({1})", EligibleClubs[i].Name, role), parentDashboardGeneralElementsProvider.Domains[i].Text);
                }

            }
            // Check username
            Assert.AreEqual(username, parentDashboardGeneralElementsProvider.Username.Text);

            WebDriver.Close();
            WebDriver.SwitchTo().Window(WebDriver.WindowHandles[0]);
        }
    }
}
