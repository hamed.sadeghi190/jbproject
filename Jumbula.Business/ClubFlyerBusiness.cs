﻿using Jb.Framework.Common;
using Jumbula.Core.Business;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class ClubFlyerBusiness : IClubFlyerBusiness
    {
        private readonly IRepository<ClubFlyer> _clubFlyerRepository;
        public ClubFlyerBusiness(IRepository<ClubFlyer> clubFlyerRepository)
        {
            _clubFlyerRepository = clubFlyerRepository;
        }

        public ClubFlyerBusiness()
        {

        }

        public OperationStatus Create(ClubFlyer clubflyer)
        {
            return _clubFlyerRepository.Create(clubflyer);
        }

        public List<ClubFlyer> GetClubFlyerList(long clubId)
        {
            return _clubFlyerRepository.GetList(c => c.ClubId == clubId).ToList();
        }

        public List<ClubFlyer> GetPartnerFlyerList(long partnerId)
        {
            return _clubFlyerRepository.GetList(c => c.Club.PartnerId.HasValue && c.Club.PartnerId == partnerId).ToList();
        }

        public List<int> GetListIds(List<int> schoolId)
        {
            var flyerIds = new List<int>();

            foreach (var id in schoolId)
            {
                var clubFlyerId = _clubFlyerRepository.GetList().Where(c => c.ClubId == id).Select(f => f.FlyerId);
                if (clubFlyerId != null)
                {
                    flyerIds.AddRange(clubFlyerId);
                }
            }
            return flyerIds;
        }

    }  
}
