﻿using System.Collections.Generic;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Business
{
    public interface ICartBusiness
    {
        Cart Get(int userId, int clubId);
        bool CheckExistCoupon(Cart cart, int couponId);
        List<Coupon> GetAllCoupons(Cart cart);
        void ManipulatePreregisterationStutus(List<OrderItem> orderItems);
        void ManipulatePreregisterationStutus(Order order);
        void ManipulatePreregisterationStutus(Cart cart);

        void ManipulateLotteryStutus(Cart cart);
        bool CheckCartItemsValidation(Cart cart, ref Dictionary<long, string> errors);
        void CalculateRegistrationFee(Order order);
        bool IsCartBelongsToUser(int currentUserId, int userId, bool isUserAdminForClub);

        decimal CalculateOrderItemTuitionPrice(OrderItem orderItem, decimal amount);
    }
}
