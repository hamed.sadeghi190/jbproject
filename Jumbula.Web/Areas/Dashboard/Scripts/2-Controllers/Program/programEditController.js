﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('programEditController', ['$scope', '$http', '$log','$stateParams','$state', function ($scope, $http, $log,$stateParams,$state) {
        window.app.globalObjects.pageClass = 'seasons-view edit-program';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/SeasonSetup", order: 2, menu: "SEASONS" });

        $scope.MC.stateParams = $stateParams;

        $scope.programName = $stateParams.programDomain;
    }]);
})();