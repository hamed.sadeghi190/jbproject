﻿(function () {
    'use strict';

    angular.module('dashboardApp').service("imageUploadService", ['jbToast', function (jbToast) {


        //Cropper
        this.cropperInit = function ($scope) {
            //$scope.cropper = {};
            //$scope.cropper.sourceImage = null;
            //$scope.cropper.croppedImage = null;
            //$scope.bounds = {};
            //$scope.bounds.left = 0;
            //$scope.bounds.right = 0;
            //$scope.bounds.top = 0;
            //$scope.bounds.bottom = 0;
        }
        this.cropperGetSize = function ($scope, element) {
            //var files = element.files;
            //var file = files[0];

            //var reader = new FileReader();
            //reader.onload = loadFinished;
            //reader.readAsDataURL(file);

            //function loadFinished(event) {

            //    var data = event.target.result;
            //    var image = new Image();
            //    image.src = data;
            //    image.onload = function () {
            //        $scope.bounds = {};
            //        $scope.bounds.left = 0;
            //        if (image.naturalWidth >= 636)
            //            $scope.bounds.right = 636;
            //        else if (image.naturalWidth < 636)
            //            $scope.bounds.right = image.naturalWidth;
            //        if (image.naturalHeight >= 300) {
            //            $scope.bounds.top = 300
            //        }
            //        else if (image.naturalHeight < 300) {
            //            $scope.bounds.top = image.naturalHeight;
            //        }

            //        $scope.bounds.bottom = 0;
            //    }
            //}

        }
        this.clearCropper = function ($scope) {
            $scope.cropper = {};
            $scope.cropper.sourceImage = null;
            $scope.cropper.croppedImage = null;
            return $scope.cropper;
        }
        this.validateImage = function ($scope, element) {
            var files = element.files;
            var file = files[0];
            var filePath = element.value;
            var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;

            if (!allowedExtensions.exec(filePath)) {
                jbToast.error("Please select a valid image, supported types are JPEG, GIF, PNG and JPG.");
                element.value = '';
                return false;
            } else {
                if (files[0].size > 10000000) {
                    jbToast.error("You cannot upload images larger than 10 mega bytes.");
                    element.value = "";
                    return false;
                } else {
                    return true;
                }
            }
        }



    }]);
})();