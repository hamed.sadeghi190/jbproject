﻿using Jumbula.Core.Model.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Email = Jumbula.Core.Domain.Email;

namespace Jumbula.Business.EmailBuilder
{
    public abstract class CustomizableEmailBuilder<TEmailParameter> : BaseEmailBuilder<TEmailParameter> where TEmailParameter : CustomizableEmailParameterModel
    {
        #region Contstructors

        protected CustomizableEmailBuilder(IClubBusiness clubBusiness, IEmailTemplateBusiness emailTemplateBusiness, IRepository<EmailTemplate> emailTemplateRepository)
        {
            _clubBusiness = clubBusiness;
            _emailTemplateBusiness = emailTemplateBusiness;
            _emailTemplateRepository = emailTemplateRepository;
        }

        #endregion

        #region Fields
        private Club _club;

        private readonly IClubBusiness _clubBusiness;
        private readonly IEmailTemplateBusiness _emailTemplateBusiness;
        private readonly IRepository<EmailTemplate> _emailTemplateRepository;
        #endregion

        #region Constants
        private const string DefaultTemplateName = "Default";
        private const string NewTemplateName = "New Template ...";
        #endregion

        #region Properties 
        #endregion

        #region Public Methods

        public override Email GetEmail(EmailViewModel emailViewModel)
        {
            Initiate(GetParameter(emailViewModel.Parameters));

            return MapEmail(emailViewModel);
        }

        public override EmailViewModel GetEmailViewModel(TEmailParameter parameter)
        {
            _club = _clubBusiness.Get(parameter.ClubId);

            return MapEmailViewModel(GetEmail(parameter));
        }

        public abstract List<SelectKeyValue<int>> GetFromList(Club club);
        public abstract List<SelectKeyValue<int>> GetReplyToList(Club club);
        public abstract List<SelectKeyValue<int>> GetCcList(Club club);
        public abstract List<SelectKeyValue<int>> GetBccList(Club club);

        public override string GeneratePreviewBody(EmailViewModel emailViewModel)
        {
            var parameters = GetParameter(emailViewModel.Parameters);

            emailViewModel.MailTemplate.PreviewMode = EmailBodyGenerationMode.Preview;

            return GenerateBody(emailViewModel.MailTemplate, emailViewModel.Body, BuildPreviewBodyModel(parameters, Common.Constants.Email.PrimarySubCategory));
        }

        public override string GenerateBody(EmailTemplateModel template, string emailBody, object bodyModel)
        {
            template.Body = emailBody;

            return HttpUtility.HtmlDecode(ViewHelper.RenderViewToString(Common.Constants.Email.GeneralEmailTemplatePath, template));
        }

        protected List<SelectKeyValue<int>> GetClubContacts(Club club)
        {
            var activeStaff = _clubBusiness.GetActiveUsersWithSpecificRoles(club.Id, new List<RoleCategory> { RoleCategory.Admin, RoleCategory.Manager }).Select(s => new SelectKeyValue<int> { Value = s.Id, Text = s.UserName }).ToList();

            var clubContact = club.ContactPersons.FirstOrDefault();
            if (clubContact != null) activeStaff.Add(new SelectKeyValue<int> { Value = clubContact.Id, Text = clubContact.Email });

            return activeStaff;
        }

        protected virtual EmailTemplateModel GetEmailTemplate(int? templateId)
        {
            return _emailTemplateBusiness.GetEmailTemplate(templateId);
        }
        #endregion

        #region Private Methods
        private EmailViewModel MapEmailViewModel(Email email) => new EmailViewModel
        {
            Parameters = email.Parameters,
            FromList = GetFromList(_club),
            Subject = email.Subject,
            From = email.FromEmail,
            Category = email.Category,
            TemplateId = email.TemplateId,
            Recipients = email.Recipients.ToList(),
            ReplyTo = email.ReplyTo,
            ReplyToList = GetReplyToList(_club),
            CcList = GetCcList(_club),
            BccList = GetBccList(_club),
            TemplateNames = GetEmailTemplateList(_club),
            UserId = email.UserId,
            IsCampaignMode = email.IsCampaignMode
        };

        private Email MapEmail(EmailViewModel emailViewModel)
        {
            var recipients = emailViewModel.Recipients.Select(x => new EmailRecipient { EmailAddress = x.EmailAddress, Type = EmailRecipientType.To }).ToList();

            var ccList = !emailViewModel.IsCcListEmailSet ? emailViewModel.Ccs.FirstOrDefault()?.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList() : emailViewModel.Ccs;
            if (ccList != null)
                recipients.AddRange(ccList.Select(x => new EmailRecipient
                    {EmailAddress = x.Trim(), Type = EmailRecipientType.Cc}));

            var bccList = !emailViewModel.IsCcListEmailSet ? emailViewModel.Bccs.FirstOrDefault()?.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList() : emailViewModel.Bccs;
            if (bccList != null)
                recipients.AddRange(bccList.Select(x => new EmailRecipient
                    {EmailAddress = x.Trim(), Type = EmailRecipientType.Bcc}));

            var parameters = GetParameter(emailViewModel.Parameters);
            parameters.Body = emailViewModel.Body;

            return new Email
            {
                Subject = emailViewModel.Subject,
                DateCreated = DateTime.UtcNow,
                ReplyTo = emailViewModel.ReplyTo?.Replace(",", "").Trim(),
                FromEmail = emailViewModel.From,
                FromDisplayName = GetFromDisplayName(),
                Category = emailViewModel.Category,
                Parameters = SerializeParameter(parameters),
                Recipients = recipients,
                TemplateId = emailViewModel.TemplateId,
                Histories = GetHistories(),
                Attachments = GetAttachments(emailViewModel.Attachments),
                UserId = emailViewModel.UserId,
                IsCampaignMode = emailViewModel.IsCampaignMode
            };
        }

        private List<SelectKeyValue<string>> GetEmailTemplateList(Club club)
        {
            var templates = _emailTemplateRepository.GetList().Where(t => t.Club_Id == club.Id && t.Type == TemplateType.Email).OrderBy(o => o.TemplateName).ToList();

            templates.Add(_emailTemplateRepository.GetList().SingleOrDefault(n => n.TemplateName == DefaultTemplateName));

            var templateNames = templates.Where(temp => temp != null).Select(temp => new SelectKeyValue<string> { Value = temp.Id.ToString(), Text = temp.TemplateName }).ToList();

            templateNames.Add(new SelectKeyValue<string> { Value = null, Text = NewTemplateName });

            return templateNames;
        }

        private static ICollection<EmailAttachment> GetAttachments(IEnumerable<AttachmentModel> attachments)
        {
            return attachments.Select(x => new EmailAttachment { FileName = x.FileName, FileSize = x.FileSize, FileUrl = x.FileUrl }).ToList();
        }

        #endregion

    }
}
