﻿using Jumbula.Common.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Models
{
    public class CampaignViewModelStep2 : BaseViewModel<Campaign>
    {
        public CampaignViewModelStep2()
        {
            TemplateNames = new List<SelectKeyValue<string>>();
            Attachments = new List<AttachmentModel>();
            MailTemplate = new EmailTemplateModel();
        }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Campaign Name")]
        public string CampaignName { get; set; }

        public List<AttachmentModel> Attachments { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Email Subject")]
        [MaxLength(128)]
        public string Subject { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Email Body")]
        public string Body { get; set; }

        public string From { get; set; }

        [EmailAddress]
        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Reply to")]
        [MaxLength(128)]
        public string ReplyTo { get; set; }
        public List<SelectKeyValue<string>> ListUsersReplyTo { get; set; }

        public List<SelectKeyValue<string>> TemplateNames { get; set; }

        [Required(ErrorMessage = "Please select one of email templates.")]
        [Display(Name = "Email Template")]
        public string SelectedTemplateId { get; set; }

        public EmailTemplateModel MailTemplate { get; set; }

        public bool EditMode { get; set; }

        public EmailCampaignStep LastCreatedPage { get; set; }

        public EmailStyle EmailStyle { get; set; }

        public bool IsReview { get; set; }

        public int ListId { get; set; }

        public bool IsUserRestrictedManager { get; set; }

        public List<string> SelectedUser { get; set; }
        public List<SelectKeyValue<string>> ClubAdmins { get; set; }
        public bool IsReplyToEmailSet { get; set; }
        public bool IsCcListEmailSet { get; set; }

        public string Ccs { get; set; }

        public string ClubLogo { get; set; }

    }

    public enum EmailStyle
    {
        Text = 0,
        Template = 1
    }
}