﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Core.Model.Generic;
using SportsClub.Models;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CreateEditStaffViewModel
    {
        public CreateEditStaffViewModel()
        {
            AllStaffTitles = new List<SelectKeyValue<string>>();
        }
        public int Id { get; set; }

        [Display(Name = "First name")]
        [Required(ErrorMessage = "{0} is required.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone number")]
        [RegularExpression("[0-9]+$", ErrorMessage = "Please enter the phone number including area code (e.g. 5551239876).")]
        [StringLength(15, ErrorMessage = "{0} is too long.")]
        [MinLength(8, ErrorMessage = "Enter a complete phone number including area code.")]
        public string PhoneNumber { get; set; }
        public string ExtensionPhone { get; set; }

        [Display(Name = "Email address")]
        [Required(ErrorMessage = "{0} is required.")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Email format is invalid.")]
        public string EmailAddress { get; set; }

        [Display(Name = "Confirm email address")]
        [Required(ErrorMessage = "{0} is required.")]
        [DataType(DataType.EmailAddress)]
        [System.ComponentModel.DataAnnotations.Compare("EmailAddress", ErrorMessage = "The email and confirmation email do not match.")]
        public string ConfirmEmailAddress { get; set; }

        [Display(Name = "Role category")]
        [Required(ErrorMessage = "{0} is required.")]
        public RoleCategory RoleCategory { get; set; }
        [Display(Name = "Title")]
        public StaffTitle? Title { get; set; }

        public DateTime? DeteOfBackgrandCheck { get; set; }
        public PageMode PageMode { get; set; }

        public List<SelectKeyValue<string>> AllRoles { get; set; }
        //public List<SelectKeyValue<string>> AllRolesAndDiscription { get; set; }
        public List<SelectKeyValue<string>> AllStaffTitles { get; set; }

    }

}