﻿(function () {
    "use strict";

    angular.module("dashboardApp").controller("duplicateEnrollmentPolicyController",
        [
            "$scope", "$http", "jbToast", "uiComponentsService", function ($scope, $http, jbToast) {
                window.app.globalObjects.pageClass = "school-siteinfo-settings-view";
                window.app.globalObjects.initializeView();


                $scope.Model = {};

                $http.get("Dashboard/Setting/GetDuplicateEnrollmentPolicy").success(
                    function (data) {

                        $scope.Model = data;

                    }).error(function (data) {
                        jbToast.error("error " + data);
                    });


                $scope.save = function () {
                    $http.post("Dashboard/Setting/SaveDuplicateEnrollmentPolicy", { model: $scope.Model })
                        .success(function (data) {

                            if (data.Status) {
                                $scope.errors = null;
                                jbToast.success("Duplicate enrollment policy updated successfully.", "");
                            } else {
                                $scope.MC.handleErrors($scope, data);
                            }
                        });
                };
            }
        ]);
})();