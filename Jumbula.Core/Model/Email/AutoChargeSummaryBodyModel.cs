﻿using System;
using System.Collections.Generic;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Model.Email
{
    public class AutoChargeSummaryBodyModel
    {
        public string ClubName { get; set; }
        public string Subject { get; set; }
        public DateTime Date { get; set; }

        public bool IsPreviewMode = false;

        public string ClubLogoUrl;

        public string Title;
        public int FailCount;
        public int SuccessCount;

        public int TotalCount => SuccessCount + FailCount;
        public decimal TotalAmount => SuccessTotalAmount + FailTotalAmount;

        public decimal SuccessTotalAmount;
        public decimal FailTotalAmount;


        public IEnumerable<AutoChargeSummaryBodyItemModel> SuccessItems { get; set; }
        public IEnumerable<AutoChargeSummaryBodyItemModel> FailedItems { get; set; }

    }
}
