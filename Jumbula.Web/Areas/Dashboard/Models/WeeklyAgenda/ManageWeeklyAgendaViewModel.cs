﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ManageWeeklyAgendaViewModel
    {
        public DateTime Date { get; set; }
    }
}