﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('addSubsidyController', ['$scope', '$http', '$stateParams', '$state', 'jbToast', function ($scope, $http, $stateParams, $state, jbToast) {
        window.app.globalObjects.pageClass = 'Subsidies_View';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + '/Dashboard/Club';

        $scope.subsidyId = $scope.MC.stateParams.subsidyId;

        $scope.Model = {};

            $http.get($scope.baseUrl + '/GetSubsidyInformation').success(function (data, status, headers, config) {

                $scope.Model = data;

            });
       
        $scope.save = function () {
            $http.post('Dashboard/Club/SaveSubsidy', { model: $scope.Model })
             .success(function (data, status, headers, config) {

                 if (data.Status == true) {

                     $state.go('AddSubsidyStep2', { subsidyId: data.Data });
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }

    }])

})();