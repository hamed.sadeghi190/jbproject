﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('registerController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'addNewUserService', function ($scope, $http, $log, $stateParams, $state, jbToast, addNewUserService) {
        window.app.globalObjects.pageClass = 'seasons-view program';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        $scope.programId = $stateParams.programId;
        $scope.type = $stateParams.Type;
        $scope.isType = $stateParams.Type == 'register' ? true : false;
        $scope.searchTerm = "";
        $scope.SearchTypes = [{ Text: 'Name', Value: 'Name' }, { Text: 'Email', Value: 'Email' }, { Text: 'Confirmation id', Value: 'ConfirmationId' }];
        $scope.SearchType = 'Name';

        $scope.userDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: window.location.origin + '/Dashboard/Order/GetRegisterredUsers/',
                    cache: false
                }, parameterMap: function (data, type) {
                    if (type === "read") {
                        return {
                            term: $scope.searchTerm,
                            searchType: $scope.SearchType,
                            firstName: $scope.firstName,
                            lastName: $scope.lastName,
                            seasonDomain: $scope.seasonName,
                            skip: data.skip,
                            take: data.take,
                            page: data.page,
                            pageSize: data.pageSize,
                            sort: data.sort
                        }
                    }
                }
            },
            pageSize: 10,
            batch: true,
            serverPaging: true,
            serverSorting: true,
            schema: {
                data: "Data",
                total: "Total"
            },
            requestStart: function (e) {
                kendo.ui.progress($('[kendo-grid]'), true);
            },
            requestEnd: function (e) {
                kendo.ui.progress($('[kendo-grid]'), false);
            }
        });
        $scope.userGridOptions = {
            dataSource: $scope.userDataSource,
            sortable: true,
            serverPaging: true,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            columns: [{
                field: "UserName",
                title: "Username",
                width: 250,
                template: "<span class='block'> #= UserName # </span>"
            }, {
                field: "PlayersName",
                title: "Participants",
                template: "<span> #= PlayersName # </span>"
            },
            {
                field: "TotalOrders",
                title: "Total orders"
            },
          
           {
               field: "Balance",
               title: 'Balance',
               template: "<span>#if(Balance >0) {# <p class='fg-red'>#=(Balance).formatCurrencyWithPenny(2)#</p> #} else if(Balance <0) {# <p class='fg-green'>#=(Balance).formatCurrencyWithPenny(2)#</p> #} else if(Balance ==0) {# <p>#=Balance#</p> #}#"
           }, {
                title: "Action",
                width: "140px",
                template: "#if(" + $scope.isType + "){#<a target='_blank' ng-click='offlineRegister(#=Id#)' class='block'> Register </a>#}else{#<a target='_blank' ng-click='addWaitlist(#=Id#)' class='block'> Add </a>#}#"
            }],
        };
       
        $scope.offlineRegister=function(userId)
        {
            window.open(window.location.origin + "/dashboard/program/register/?programId=" + $stateParams.programId+"&userId="+userId);
            $state.go("ProgramsReview", { seasonDomain: $stateParams.seasonDomain });
        }
        $scope.addWaitlist = function (userId) {
         
            $state.go('AddToWaitlists', { seasonDomain: $stateParams.seasonDomain, programId: $stateParams.programId, userId:userId });
        }
        $scope.addUser = function () 
        {
            var addModel={
                page:"Register",
                stateParams:$scope.MC.stateParams
            };
            addNewUserService.set(addModel);
            if ($scope.isType) {
                $state.go('AddClubUsers', { type: 'register' });
            }
            else { $state.go('AddClubUsers', { type: 'Waitlist' }); }
        }
        $scope.search = function () {
            $("#userGrid").data("kendoGrid").dataSource.read();
        };

        $scope.clearFilters = function () {
            $scope.searchTerm = "";
            $scope.firstName = "";
            $scope.lastName = "";

            $("#userGrid").data("kendoGrid").dataSource.read();
        }
        $scope.seasonName = $stateParams.seasonDomain;

    }]);
})();