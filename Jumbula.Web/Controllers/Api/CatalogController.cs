﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.UIHelpers;

namespace Jumbula.Web.Controllers.Api
{
    public class CatalogController : ApiController
    {

        public object GetAdvancedSearch(string partnerDomain)
        {
            object result = null;

            try
            {
                List<string> emCounties = new List<string>()
                {
                    "Alexandria City","Fairfax County", "Arlington County", "Loudoun County", "Prince William County", "Montgomery County", "District of Columbia County"
                };

                List<string> emStates = new List<string>()
                {
                    "Virginia", "Maryland", "District of Columbia"
                };

                var allProviders = Ioc.ClubBusiness.GetRelatedClubs(partnerDomain).Where(r => (r.ClubType.ParentType != null && r.ClubType.ParentType.EnumType == ClubTypesEnum.Provider) && !(r.CatalogSetting != null && r.CatalogSetting.CatalogSearchTags.NoGlobalSearch)).Select(s =>
                           new KeyValuePair<string, string>(s.Domain, s.Name)).OrderBy(o => o.Value).ToList();

                var allCounties = Ioc.CountryBusiness.GetCounties()
                     .Where(w => emCounties.Contains(w.Name) && emStates.Contains(w.State.Name))
                    .ToList().Select(x => new SelectKeyValue<int>() { Group = x.State.Name, Text = x.Name, Value = x.Id }).OrderBy(c => c.Text).ToList();

                allCounties.Remove(allCounties.Single(s => s.Text.Equals("Montgomery County", StringComparison.OrdinalIgnoreCase) && s.Group.Equals("Virginia", StringComparison.OrdinalIgnoreCase)));

                var usedCategoryIds = Ioc.CatalogBusiness.GetList().SelectMany(c => c.Categories).Select(c => c.Id).Distinct();

                var allCategories = Ioc.CategoryBuisness.GetList()
                      .Where(e => e.ParentId != null && usedCategoryIds.Contains(e.Id))
                      .ToList().OrderBy(o => o.Order).Select(x => new SelectKeyValue<int>() { Group = x.ParentCategory.Name, Text = x.Name, Value = x.Id }).ToList();

                result = new
                {
                    Providers = allProviders,
                    Counties = allCounties,
                    Categories = allCategories,
                    Grades = DropdownHelpers.ToOrderedSelectList<SchoolGradeType>("Select grade", null),

                    MinPrices = GetMinPrices(),
                    MaxPrices = GetMaxPrices(),

                    MinEnrollments = GetMinEnrollments(),
                    MaxEnrollments = GetMaxEnrollments(),
                };
            }

            catch (Exception ex)
            {
                return new { Status = "Error", Message = "An exception has occurred." };
            }

            return result;
        }

        public HttpResponseMessage Post([FromBody]CatalogSearchModel search)
        {
            IQueryable<CatalogItem> catalogs = Enumerable.Empty<CatalogItem>().AsQueryable();

            int page = search.Page != 0 ? search.Page : 1;
            int pageSize = search.Offset != 0 ? search.Offset : 20;

            catalogs = Ioc.CatalogBusiness.GetList().Where(c => c.Status == CatalogStatus.Active && !(c.Club.CatalogSetting != null && c.Club.CatalogSetting.CatalogSearchTags.NoGlobalSearch));

            catalogs = catalogs.Where(c => c.Club.PartnerClub.Domain.Equals("em", StringComparison.OrdinalIgnoreCase) && c.Club.ClubType.ParentType.Name == "Provider");

            if (search.Category.HasValue && search.Category != 0)
            {
                catalogs = catalogs.Where(c => c.Categories.Select(f => f.Id).Contains(search.Category.Value));
            }

            if (search.County.HasValue && search.County != 0)
            {
                catalogs = catalogs.Where(c => c.Club.CatalogSetting != null && c.Club.CatalogSetting.CatalogSettingCounties.Select(co => co.CountyId).Contains(search.County.Value));
            }

            if (search.MinGrade.HasValue)
            {
                catalogs = catalogs.Where(c => c.AttendeeRestriction.MinGrade.HasValue && c.AttendeeRestriction.MinGrade >= search.MinGrade);
            }

            if (search.MaxGrade.HasValue)
            {
                catalogs = catalogs.Where(c => c.AttendeeRestriction.MaxGrade.HasValue && c.AttendeeRestriction.MaxGrade <= search.MaxGrade);
            }

            if (search.MinPrice.HasValue)
            {
                if (search.MinPrice != -1)
                {
                    catalogs = catalogs.Where(c => c.PriceOptions.Any() && c.PriceOptions.Any(p => p.Amount >= search.MinPrice.Value));
                }
                else
                {
                    catalogs = catalogs.Where(c => c.PriceOptions.Any() && c.PriceOptions.Any(p => p.Amount < 100));
                }
            }

            if (search.MaxPrice.HasValue)
            {
                if (search.MaxPrice != -1)
                {
                    catalogs = catalogs.Where(c => c.PriceOptions.Any() && c.PriceOptions.Any(p => p.Amount <= search.MaxPrice.Value));
                }
                else
                {
                    catalogs = catalogs.Where(c => c.PriceOptions.Any() && c.PriceOptions.Any(p => p.Amount > 300));
                }
            }

            if (search.MinEnrollment.HasValue)
            {
                catalogs = catalogs.Where(c => c.MinimumEnrollment >= search.MinEnrollment.Value);
            }

            if (search.MaxEnrollment.HasValue)
            {
                catalogs = catalogs.Where(c => c.MaximumEnrollment <= search.MaxEnrollment.Value);
            }

            if (search.BeforeSchool)
            {
                catalogs = catalogs.Where(c => c.Club.CatalogSetting == null || (c.Club.CatalogSetting != null && c.Club.CatalogSetting.CatalogSearchTags.BeforeSchool));
            }

            if (search.Virtus)
            {
                catalogs = catalogs.Where(c => c.Club.CatalogSetting == null || (c.Club.CatalogSetting != null && c.Club.CatalogSetting.VirtusCertified));
            }

            if (search.WeeklyEmails)
            {
                catalogs = catalogs.Where(c => c.Club.CatalogSetting == null || (c.Club.CatalogSetting != null && c.Club.CatalogSetting.WeeklyEmail));
            }

            var totalItems = catalogs.Count();

            var items = catalogs.OrderBy(p => p.Name).ThenBy(p => p.Club.Name).Skip(pageSize * (page - 1)).Take(pageSize).ToList();

            var finalResult = new
            {
                Search = search,
                TotalItems = totalItems,
                Items = items.Select(c =>
                new
                {
                    Id = c.Id,
                    Address = "School address",
                    Description = c.Description,
                    MinGrade = c.AttendeeRestriction.MinGrade.HasValue ? c.AttendeeRestriction.MinGrade.ToDescription() : string.Empty,
                    MaxGrade = c.AttendeeRestriction.MaxGrade.HasValue ? c.AttendeeRestriction.MaxGrade.ToDescription() : string.Empty,
                    MinPrice = c.PriceOptions != null && c.PriceOptions.Any() ? c.PriceOptions.Min(p => p.Amount) : 0,
                    MaxPrice = c.PriceOptions != null && c.PriceOptions.Any() ? c.PriceOptions.Max(p => p.Amount) : 0,
                    Name = c.Name,
                    ProviderName = c.Club.Name,
                    Category = c.Categories != null && c.Categories.Any() ? c.Categories.First().Name : string.Empty,
                    WeeklyEmails = c.Club.CatalogSetting == null || (c.Club.CatalogSetting != null && c.Club.CatalogSetting.WeeklyEmail),
                    ProviderDomain = c.Club.Domain
                })
                .ToList(),
            };

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, finalResult);
            return response;
        }

        private List<KeyValuePair<int, string>> GetMinPrices()
        {
            return new List<KeyValuePair<int, string>>
                    {
                        new KeyValuePair<int, string>(-1, "Less than $100"),
                        new KeyValuePair<int, string>(100, "$100"),
                        new KeyValuePair<int, string>(150, "$150"),
                        new KeyValuePair<int, string>(200, "$200"),
                        new KeyValuePair<int, string>(250, "$250"),
                        new KeyValuePair<int, string>(300, "$300"),
                    }
                    .ToList();
        }

        private List<KeyValuePair<int, string>> GetMaxPrices()
        {
            return new List<KeyValuePair<int, string>>
                   {
                        new KeyValuePair<int, string>(100, "$100"),
                        new KeyValuePair<int, string>(150, "$150"),
                        new KeyValuePair<int, string>(200, "$200"),
                        new KeyValuePair<int, string>(250, "$250"),
                        new KeyValuePair<int, string>(300, "$300"),
                        new KeyValuePair<int, string>(-1, "More than $300")
                    };
        }

        private List<KeyValuePair<int, string>> GetMinEnrollments()
        {
            var result = new List<KeyValuePair<int, string>>();

            for (int i = 1; i < 9; i++)
            {
                var item = new KeyValuePair<int, string>(i, i.ToString());

                result.Add(item);
            }

            return result;
        }

        private List<KeyValuePair<int, string>> GetMaxEnrollments()
        {
            var result = new List<KeyValuePair<int, string>>();

            for (int i = 9; i < 21; i++)
            {
                var item = new KeyValuePair<int, string>(i, i.ToString());

                result.Add(item);
            }

            result.Add(new KeyValuePair<int, string>(500, "20+"));

            return result;
        }
    }


}
