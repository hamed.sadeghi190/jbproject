﻿using System;

namespace Jumbula.Common.Exceptions
{
    public class JumbulaResourceNotFoundException : JumbulaException
    {
        public JumbulaResourceNotFoundException(Exception e)
            : base(Constants.Constants.M_E_NotFoundResource, e)
        {

        }
    }
}
