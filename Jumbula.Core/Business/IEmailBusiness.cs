﻿using Jumbula.Common.Enums;
using Jumbula.Core.Model.Email;
using System;

namespace Jumbula.Core.Business
{
    public interface IEmailBusiness
    {
        void Send<TEmailParameter>(EmailCategory emailCategory, TEmailParameter parameters, DateTime? scheduleDateTimeUtc = null) where TEmailParameter : class;
        void SendAllQueued(EmailCategory? emailCategory = null);
        void Send(EmailViewModel emailViewModel);

        string GetPreview(int id);
        string GetPreview(EmailViewModel emailViewModel);
        EmailViewModel GetViewModel<TEmailParameter>(EmailCategory emailCategory, TEmailParameter parameters) where TEmailParameter : class;

        string GetPreview<TEmailParameter>(EmailCategory emailCategory, TEmailParameter parameters, string subCategory = null) where TEmailParameter : class;

    }
}
