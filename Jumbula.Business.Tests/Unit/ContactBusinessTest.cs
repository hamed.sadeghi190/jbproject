﻿using FluentAssertions;
using Jumbula.Common.Enums;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;

namespace Jumbula.Business.Tests.Unit
{
    [TestClass]
    public class ContactBusinessTest
    {
        #region Fields
        private Mock<IRepository<Contact>> _mockContactRepositoryBusiness;
        #endregion

        #region Constants
        private const int ContactId = 50;
        #endregion

        #region Test methods
        [TestMethod]
        public void Delete_WithoutHistory_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            var contactWithoutHistory = ContactWithoutHistory();
            _mockContactRepositoryBusiness.Setup(x => x.Delete(contactWithoutHistory)).Returns(new OperationStatus { Status = true });

            // Act
            var result = service.Delete(contactWithoutHistory);

            // Assert
            result.Status.Should().BeTrue();
        }

        [TestMethod]
        public void Delete_WithHistory_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            var contactWithHistory = ContactWithHistory();
            _mockContactRepositoryBusiness.Setup(x => x.Delete(contactWithHistory)).Returns(new OperationStatus { Status = false });

            // Act
            var result = service.Delete(contactWithHistory);

            // Assert
            result.Status.Should().BeFalse();
        }
        #endregion

        #region Private methods
        private ContactBusiness CreateService()
        {
            _mockContactRepositoryBusiness = new Mock<IRepository<Contact>>();

            return new ContactBusiness(_mockContactRepositoryBusiness.Object);
        }

        private static Contact ContactWithoutHistory()
        {
            return new Contact { Id = ContactId, Category = ContactCategory.Note, Note = string.Empty };
        }

        private static Contact ContactWithHistory()
        {
            return new Contact { Id = ContactId, Category = ContactCategory.Note, Note = string.Empty, Histories = new List<CommunicationHistory> { new CommunicationHistory { RelatedEntity = "OrderItems", RelatedEntityId = 100, ContactId = ContactId } } };
        }

        #endregion 
    }
}
