
If Object_ID('[dbo].[Orders]') Is Null
BEGIN
	CREATE TABLE [dbo].[Orders](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[CardSecurityCode] [int] NOT NULL,
	[OrderAmount] [decimal](18, 2) NOT NULL,
	[PaidAmount] [decimal](18, 2) NOT NULL,
	[UserAnanymous] [bit] NOT NULL,
	[OrderId] [nvarchar](128) NOT NULL,
	[ConfirmationId] [nvarchar](64) NOT NULL,
	[TransactionId] [nvarchar](128) NULL,
	[TransactionMessage] [nvarchar](128) NULL,
	[TimeStamp] [nvarchar](128) NULL,
	[Currency] [nvarchar](32) NULL,
	[CardFirstName] [nvarchar](128) NULL,
	[CardLastName] [nvarchar](128) NULL,
	[CardEmail] [nvarchar](128) NULL,
	[CardNumber] [nvarchar](128) NULL,
	[OrderDate] [datetime] NOT NULL,
	[OrderPaidDate] [datetime] NOT NULL,
	[OrderMode] [tinyint] NULL,
	[CardExpireDate] [datetime] NOT NULL,
	[PaymentMethod] [int] NOT NULL,
	[PaymentStatus] [int] NOT NULL,
	[BillingAddress_Id] [int] NULL,
	[Domain] [nvarchar](128) NOT NULL,
	[SubDomain] [nvarchar](128) NOT NULL,
	[SeasonDomain] [nvarchar](128) NULL,
	[SubDomainCategory] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[PlayerId] [int] NULL,
	[Name] [nvarchar](max) NULL,
	[FirstName] [nvarchar](128) NOT NULL,
	[LastName] [nvarchar](128) NOT NULL,
	[Custom1] [nvarchar](128) NULL,
	[Custom2] [nvarchar](128) NULL,
	[Custom3] [nvarchar](128) NULL,
	[Custom4] [nvarchar](128) NULL,
	[Custom5] [nvarchar](128) NULL,
	[PrimaryOrderEmail] [nvarchar](128) NULL,
	[SecondaryOrderEmail] [nvarchar](128) NULL,
	[CustomFieldsMetaData] [nvarchar](max) NULL,
	[EventBaseInfoMetaData] [nvarchar](max) NULL,
	[ClubOwnerCustomFieldMetaData] [nvarchar](max) NULL,
	[CouponId] [int] NULL,
	[PaymentPlanMetaData] [nvarchar](max) NULL,
	[PaymentPlanType] [int] NULL,
	[CardType] [tinyint] NULL,
	[PaypalIPN] [nvarchar](max) NULL)
END
Go
--<< PRIMARYKEY DEFINITION >>--
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'PK_dbo.Orders')
BEGIN
	ALTER TABLE [dbo].[Orders] ADD CONSTRAINT [PK_dbo.Orders] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	) 
END
Go

--<< ADD CLOLUMNS >>--
IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=Object_ID('[dbo].[Orders]') AND NAME='PaypalIPN')
BEGIN
	ALTER TABLE [dbo].[Orders] 
	ADD [PaypalIPN] nvarchar(max) null
END
Go
IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=Object_ID('[dbo].[Orders]') AND NAME='OrderMode')
BEGIN
	ALTER TABLE [dbo].[Orders] 
	ADD [OrderMode] [tinyint] NULL
END
Go
IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=Object_ID('[dbo].[Orders]') AND NAME='SeasonDomain')
BEGIN
	ALTER TABLE [dbo].[Orders] 
	ADD [SeasonDomain] nvarchar(128) NULL
END
Go

--<< ALTER COLUMN >>--
IF EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=Object_ID('[dbo].[Orders]') AND NAME='PlayerId')
BEGIN
	ALTER TABLE [dbo].[Orders] 
	ALTER COLUMN [PlayerId] [int] NULL
END
Go

--<< ALTER COLUMN >>--
IF EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=Object_ID('[dbo].[OrderItems]') AND NAME='PlayerId')
BEGIN
	ALTER TABLE [dbo].[OrderItems] 
	ALTER COLUMN [PlayerId] [int] NULL
END
Go


--<< FOREIGNKEYS DEFINITION >>--
declare @TableName nvarchar(50)='Orders' --without dbo.

IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'FK_'+@TableName+'_JbForms')
BEGIN
declare @command nvarchar(500)='ALTER TABLE [dbo].['+@TableName+'] Add JbForm_Id int null';
exec(@command);
print 'JbForm_Id column created successfully ';

set @command='ALTER TABLE [dbo].['+@TableName+']  WITH CHECK ADD  CONSTRAINT [FK_'+@TableName+'_JbForms] FOREIGN KEY([JbForm_Id])
REFERENCES [dbo].[JbForms] ([Id])
ALTER TABLE [dbo].['+@TableName+'] CHECK CONSTRAINT [FK_'+@TableName+'_JbForms]';
exec(@command);
print 'FK created successfully ';
END
ELSE
BEGIN
	print 'Fk has already existed';
END

