﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ChangeDesiredStartDateViewModel
    {
        public List<DayOfWeek> Days { get; set; }
        public List<DayOfWeek> NewDays { get; set; }
        public string DaysName
        {
            get
            {
                if (Days != null && Days.Any())
                {
                    return string.Join(", ", Days);
                }
                return string.Empty;
            }
        }
        public string OldDesiredStartDate { get; set; }
        public long OrderItemId { get; set; }
        public DateTime? newDesiredStartDate { get; set; }
        public DateTime LastSessiondate { get; set; }

        public decimal refundedAmount { get; set; }

        public DateTime LastProgramSessiondate { get; set; }
    }
}