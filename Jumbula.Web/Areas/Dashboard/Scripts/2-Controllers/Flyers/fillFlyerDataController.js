﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('fillFlyerDataController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', '$timeout', function ($scope, $http, $log, $stateParams, $state, jbConfirmation, $timeout) {
        window.app.globalObjects.pageClass = 'campaign-templates';
        window.app.globalObjects.initializeView();


        var baseUrl = window.location.origin + '/Dashboard/Flyer';

        var timer = false;

        $scope.Model = {};


        $scope.Model.schoolId = 0;
        $scope.Model.SeasonId = 0;


        $http.get(baseUrl + '/GetAllSchool') 
        .success(function (data, status, headers, config) {

            $scope.AllSchools = data;
        });

        $scope.updateChosenScope = function () {
            $("select[chosen]").trigger('chosen:updated');
        };


        $scope.$watch('AllSeasons', function () {
            if (timer) {
                $timeout.cancel(timer);
            }
            timer = $timeout(function () {
                $scope.updateChosenScope();
            }, 1500);
        });

        $scope.fillSeasonData = function (schoolId) {
            $http.get(baseUrl + '/GetClubSeasons', { params: { clubId: schoolId } }).success(function (data, status, headers, config) {  ///GetClubSeasonsProvider delete
                $scope.AllSeasons = data;

            }).error(function (data, status, headers, config) {
                jbToast.error(data.Message);
            });

            $scope.Model.schoolId = schoolId;

            $scope.Model.seasonId = seasonId;
        }

        var checkValidation = function (schoolId, seasonId) {

            if (is.undefined(schoolId) || schoolId <= 0) {
                jbToast.error("Please select a school.");
                return false;
            }
            else if (is.undefined(seasonId) || seasonId <= 0) {
                jbToast.error("Please select a season.");
                return false;
            }
            else {
                return true;
            }
        }

    }]);
})();