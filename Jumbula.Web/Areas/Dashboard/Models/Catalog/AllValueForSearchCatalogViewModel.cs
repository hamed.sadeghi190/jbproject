﻿using Jumbula.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class AllValueForSearchCatalogViewModel
    {
        public int Id { get; set; }
        public bool BeforeSchool { get; set; }
        public bool Virtus { get; set; }
        public bool SendsSessionUpdates { get; set; }
        public string StringId
        {
            get
            {
                return Id == 0 ? Guid.NewGuid().ToString() : Id.ToString();
            }
        }
        public List<int> SelectedProvider { get; set; }
        [Display(Name = "Categories")]
        public int? Categories { get; set; }
        public List<SelectKeyValue<int>> PartnerProviders { get; set; }
        public List<SelectKeyValue<int>> AllCategories { get; set; }
        public List<SelectKeyValue<string>> Grades { get; set; }
        public List<SelectKeyValue<int>> AllCapacitiesForMax { get; set; }
        public List<SelectKeyValue<int>> AllCapacitiesForMin { get; set; }
        public int? MinimumEnrollment { get; set; }
        public int? MaximumEnrollment { get; set; }
        public int? County { get; set; }
        public List<SelectKeyValue<int>> AllCounties { get; set; }
        public SchoolGradeType? MinGrade { get; set; }
        public SchoolGradeType? MaxGrade { get; set; }
        public int? MinPrice { get; set; }

        public int? MaxPrice { get; set; }
        public int Page { get; set; }
        public int EndPage { get; set; }


    }
}