﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('editOrderController', ['$scope', '$http', 'jbToast', '$state', '$stateParams', 'jbConfirmation', 'paymentHistoryService', function ($scope, $http, jbToast, $state, $stateParams, jbConfirmation, paymentHistoryService) {
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var counter = function () {
            setTimeout(function () {
                var options = {
                    useEasing: true,
                    useGrouping: true,
                    separator: ',',
                    decimal: '.',
                    prefix: symbol,
                    suffix: ''
                };
                var remainingStart = Number($("#remaining").text().replace(new RegExp("[^0-9.-]", 'g'), '')) || 0,
                    newAmountStart = Number($("#newAmount").text().replace(new RegExp("[^0-9.-]", 'g'), '')) || 0,
                    differenceStart = Number($("#difference").text().replace(new RegExp("[^0-9.-]", 'g'), '')) || 0,
                    paidStart = Number($("#paidAmount").text().replace(new RegExp("[^0-9.-]", 'g'), '')) || 0,
                    orderAmountStart = Number($("#orderAmount").text().replace(new RegExp("[^0-9.-]", 'g'), '')) || 0;


                var remainingEnd = Number($scope.calculateBalance()) || 0,
                    newAmountEnd = Number($scope.calculateTotalAmount()) || 0,
                    differenceEnd = Number($scope.calculateDifference()) || 0,
                    paidEnd = Number($scope.Model.PaidAmount) || 0,
                    orderAmountEnd = Number($scope.Model.OrderAmount) || 0;

                new countUp("remaining", remainingStart, remainingEnd, 2, .5, options).start();
                new countUp("newAmount", newAmountStart, newAmountEnd, 2, .5, options).start();
                new countUp("difference", differenceStart, differenceEnd, 2, .5, options).start();
                new countUp("paidAmount", paidStart, paidEnd, 2, .5, options).start();
                new countUp("orderAmount", orderAmountStart, orderAmountEnd, 2, .5, options).start();
            }, 200);
        };

        $scope.Number = Number;
        var returnedByBack = $state.returnByBack();
        $scope.Model = {};
        if (returnedByBack) {
            $scope.Model = paymentHistoryService.get();
            counter();
            paymentHistoryService.set(null);
        }
        if (!returnedByBack || $scope.Model == null) {
            if ($stateParams.orderId) {

                $http.get('Dashboard/Order/GetEdit', { params: { orderItemId: $stateParams.orderId } })
                    .success(function (data, status, headers, config) {

                        $scope.Model = data;

                        $scope.Model.RefundOrPay = 'true';

                        $scope.Model.PageStep = 'OrderDetail';
                    })
            }
        }

        $scope.$watch("Model",
            function (newValue, oldValue) {
                if (newValue != oldValue) {
                    counter();
                }
            }, true
        );

        $scope.showInstallmentDetail = function (installmentId) {
            paymentHistoryService.set($scope.Model);
            $state.forward('PaymentHistory', { seasonDomain: $stateParams.seasonDomain, orderItemId: $stateParams.orderId, installmentId: installmentId }, 'EditOrder', { seasonDomain: $stateParams.seasonDomain, orderId: $stateParams.orderId }, null);

        }

        $scope.edit = function () {

            $scope.submit();

        }

        $scope.submit = function () {

            $http.post('Dashboard/Order/Edit', { model: $scope.Model })
                .success(function (data, status, headers, config) {

                    if (data.Status == true) {

                        $scope.errors = null;

                        var message = '';
                        if (data.Message == 'Donation/Cancel' || data.Message == 'Donation/Edit') {
                            if ($scope.cancelMode(data.Message)) {

                                message = 'The program is edited successfully.';
                            }
                            else {

                                message = 'The program is cancelled successfully.'
                            }
                        } else {
                            if ($scope.cancelMode('Program')) {

                                message = 'The program is cancelled successfully.'
                            } else {
                                message = 'The program is edited successfully.';
                            }

                        }

                        jbToast.success(message);
                        if ("goToFamilyOrder" in window) {
                            if (window.goToFamilyOrder > 0) {

                                var userId = window.goToFamilyOrder;
                                var clubId = window.clubId;
                                window.goToFamilyOrder = -1;
                                $state.go('FamilyOrders', { userId: userId, clubId: clubId });
                            }
                        }
                        else {
                            $state.go('OrderItem', { seasonDomain: $stateParams.seasonDomain, orderItemId: $scope.Model.Id });
                        }
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                })
        }

        $scope.editInstallmentItem = function (installmentItem) {

            installmentItem.EditMode = true;

            installmentItem._amount = installmentItem.Amount;
            installmentItem._dueDate = installmentItem.DueDate;
        }

        $scope.cancelInstallmentItem = function (installmentItem) {

            installmentItem.EditMode = false;

            installmentItem.DueDate = installmentItem._dueDate;
            installmentItem.Amount = installmentItem._amount;
        }


        $scope.addNewInstallment = function () {

            var today = new Date();
            var dueDate = new Date($scope.Model._dueDate);

            if ($scope.Model._dueDate == 0 || $scope.Model._dueDate == '' || $scope.Model._dueDate == null || $scope.Model._dueDate == 'undefined') {
                jbToast.error('You should enter Due date.');
            }
            else if (!$scope.validateDate(dueDate)) {
                jbToast.error('The format of date is invalid.');
            }
            else if (dueDate < today) {
                jbToast.error('Due date can not be before today.');
            }
            else if ($scope.Model._amount == '' || $scope.Model._amount == null || $scope.Model._amount == 'undefined') {
                jbToast.error('You should enter amount.');
            }
            else if (isNaN($scope.Model._amount)) {
                jbToast.error('The format of amount is invalid.');
            }
            else {

                var isManually = true;

                for (var i = 0; i < $scope.Model.OrderInstallments.length; i++) {

                    if (!$scope.Model.OrderInstallments[i].EnableReminder) {
                        isManually = false;
                    }
                }

                var installmentItem = { Amount: $scope.Model._amount, DueDate: $scope.Model._dueDate, IsDeposit: false, Status: '-', EnableReminder: isManually, NoticeSent: false, IsDeleted: false, ItemStatus: 'initiated' };

                $scope.Model.OrderInstallments.push(installmentItem);

                $scope.Model._amount = '';
                $scope.Model._dueDate = '';
            }
        }

        $scope.validateDate = function (date) {

            if (new Date(date) == 'Invalid Date') {
                return false;
            }

            return true;
        }

        $scope.addChargeDiscount = function (chargeDiscountId) {
            if (is.existy($scope.Model.SelectedChargeDiscount) || is.not.empty($scope.Model.SelectedChargeDiscount)) {

                var selectedCDTemp = $scope.getChargeDiscount(chargeDiscountId);

                if (!$scope.hasTuition() && selectedCDTemp.Category != 'EntryFee') {
                    jbToast.error('You should select tuition option first.');
                }
                else {

                    $scope.IsCanceled = false;

                    for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {

                        if ($scope.Model.OrderChargeDiscounts[i].Id == chargeDiscountId) {
                            $scope.Model.OrderChargeDiscounts[i].Checked = true;
                        }
                    }
                }
            }
        }

        $scope.removeChargeDiscount = function (chargeDiscountId) {

            var message = 'Are you sure you want to proceed with this action?';

            if ($scope.getChargeDiscount(chargeDiscountId).Category == 'EntryFee') {
                message = 'Are you sure you want to cancel order?';
            }
            else {
                message = 'Are you sure you want to delete this item?';
            }

            jbConfirmation.message(message).yes(function () {

                if ($scope.Model.OrderChargeDiscounts) {

                    for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {
                        if ($scope.Model.OrderChargeDiscounts[i].Id == chargeDiscountId) {

                            if ($scope.Model.OrderChargeDiscounts[i].Category == 'EntryFee') {

                                for (var l = 0; l < $scope.Model.OrderChargeDiscounts.length; l++) {
                                    $scope.Model.OrderChargeDiscounts[l].Checked = false;
                                }
                                $scope.IsCanceled = true;
                                $scope.Model.EditFee = null;

                            }
                            else {
                                $scope.Model.OrderChargeDiscounts[i].Checked = false;
                            }
                        }
                    }
                }

            })
                .confirm();
        }

        $scope.IsCanceled = false;

        $scope.hasInstallment = function () {
            if ($scope.Model.OrderInstallments != null && $scope.Model.OrderInstallments != 'undefined' && $scope.Model.OrderInstallments.length) {
                return true;
            }

            return false;
        }

        $scope.editItem = function (chargeDiscount) {
            chargeDiscount._Amount = chargeDiscount.Amount;
            chargeDiscount._Name = chargeDiscount.Name;

            chargeDiscount.EditMode = true;
        }

        $scope.cancelEditItem = function (chargeDiscount) {
            chargeDiscount.Amount = chargeDiscount._Amount;
            chargeDiscount.Name = chargeDiscount._Name;

            chargeDiscount.EditMode = false;
        }

        $scope.hasTuition = function () {

            if ($scope.Model.OrderChargeDiscounts) {
                for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {

                    if ($scope.Model.OrderChargeDiscounts[i].Category == 'EntryFee' && $scope.Model.OrderChargeDiscounts[i].Checked) {
                        return true;
                    }
                }
            }

            return false;
        }

        $scope.cancelMode = function (type) {
            if (type == 'Donation/Cancel') {

                return false;

            } else if (type == 'Donation/Edit') {

                return true;

            }
            else {
                return !$scope.hasTuition();
            }

        }

        $scope.getEntryFee = function () {

            for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {

                if ($scope.Model.OrderChargeDiscounts[i].Category == 'EntryFee') {
                    return $scope.Model.OrderChargeDiscounts[i];
                }
            }
        }

        $scope.getChargeDiscount = function (chargeDiscountId) {

            for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {

                if ($scope.Model.OrderChargeDiscounts[i].Id == chargeDiscountId) {
                    return $scope.Model.OrderChargeDiscounts[i];
                }
            }

            return null;
        }

        $scope.saveInstallment = function (installmentItem) {
            $http.post('Dashboard/Order/ValidateInstallments', { model: $scope.Model, installment: installmentItem })
                .success(function (data, status, headers, config) {

                    if (data.Status) {

                        var amount = installmentItem.Amount;
                        var dueDate = installmentItem.DueDate;

                        var today = new Date();
                        var dueDate = new Date(dueDate);

                        if (dueDate == 0 || dueDate == '' || dueDate == null || dueDate == 'undefined') {
                            jbToast.error('You should enter Due date.');
                        }
                        else if (!$scope.validateDate(dueDate)) {
                            jbToast.error('The format of date is invalid.');
                        }
                        else if (amount == '' || amount == null || amount == 'undefined') {
                            jbToast.error('You should enter amount.');
                        }
                        else if (isNaN(amount)) {
                            jbToast.error('The format of amount is invalid.');
                        }
                        else {
                            installmentItem.EditMode = false;
                        }
                    } else {

                        $scope.MC.handleErrors($scope, data);
                    }
                });
        }

        $scope.removeInstallmentItem = function (installmentItem) {

            var message = 'Are you sure you want to delete this installment?';
            jbConfirmation.message(message).yes(function () {

                installmentItem.IsDeleted = true;
            })
                .confirm();
        }

        $scope.removeInstallmentSubscriptionItem = function (installmentItem, isDeletedSessions) {

            if (isDeletedSessions) {
                var message = 'Are you sure you want to delete this installment and all its sessions? After the deletion, the participant name will no longer appear on the roster for the schedule period. Note: you can use the Edit installment action and set the amount to zero to keep the participant on the roster.';
                jbConfirmation.message(message).yes(function () {

                    installmentItem.IsDeletedNormalItem = false;
                    installmentItem.IsDeletedSessions = true;


                    installmentItem.IsActiveInstallmentAndSessions = false;

                })
                    .confirm();
            } else {
                var message = 'Are you sure you want to delete this installment?';
                jbConfirmation.message(message).yes(function () {

                    installmentItem.IsDeletedNormalItem = true;
                    installmentItem.IsDeletedSessions = false;

                    installmentItem.IsActiveInstallment = false;

                })
                    .confirm();
            }

        }

        $scope.deletePaidInFullSchedulePart = function (schedulePart) {

            var message = 'Are you sure you want to delete this schedule and all its sessions? Note: You may want to update the price for this order by going back to the previous page.';
            jbConfirmation.message(message).yes(function () {
                schedulePart.IsDeleted = true;
            })
                .confirm();
        }

        $scope.activePaidInFullSchedulePart = function (schedulePart) {
            var message = 'Are you sure you want to undo this schedule and all its sessions? Note: You may want to update the price for this order by going back to the previous page.';
            jbConfirmation.message(message).yes(function () {

                schedulePart.IsDeleted = false;
                schedulePart.IsRestored = true;

            }).confirm();
        }

        $scope.activeInstallment = function (installmentItem) {

            var message = 'Are you sure you want to undo this installment?';
            jbConfirmation.message(message).yes(function () {

                installmentItem.WasDeleted = false;
                installmentItem.IsDeletedNormalItem = false;
                installmentItem.IsDeleted = false;

                installmentItem.IsActiveInstallment = true;

            }).confirm();

        }

        $scope.activeInstallmentAndSessions = function (installmentItem) {
            var message = 'Are you sure you want to restore the installment and all its sessions? After the restoration, the participant name will appear on the roster for the schedule period.';
            jbConfirmation.message(message).yes(function () {

                installmentItem.WasDeleted = false;
                installmentItem.IsDeletedSessions = false;
                installmentItem.IsDeleted = false;

                installmentItem.IsActiveInstallmentAndSessions = true;

            }).confirm();
        }

        $scope.calculateDifference = function () {
            return $scope.calculateTotalAmount() - $scope.Model.OrderAmount;
        }

        $scope.calculateTotalInstallments = function () {

            var result = 0;

            if ($scope.hasInstallment()) {

                if ($scope.Model.ProgramType != 'Subscription' && $scope.Model.ProgramType != 'BeforeAfterCare') {
                    for (var i = 0; i < $scope.Model.OrderInstallments.length; i++) {
                        if (!$scope.Model.OrderInstallments[i].IsDeleted) {
                            result += parseFloat($scope.Model.OrderInstallments[i].Amount);
                        }
                    }
                } else {
                    for (var i = 0; i < $scope.Model.OrderInstallments.length; i++) {
                        if (!$scope.Model.OrderInstallments[i].WasDeleted && !$scope.Model.OrderInstallments[i].IsDeletedSessions && !$scope.Model.OrderInstallments[i].IsDeletedNormalItem) {
                            result += parseFloat($scope.Model.OrderInstallments[i].Amount);
                        }
                    }
                }
            }

            return result;
        }

        $scope.calculateTotalAmount = function () {

            var charges = 0;

            var discounts = 0;

            var cancellationFee = 0;
            var editFee = 0;
            var prorateFee = 0;

            if ($scope.Model.EditFee) {

                for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {

                    if ($scope.Model.OrderChargeDiscounts[i].SubCategory == 'Charge' && $scope.Model.OrderChargeDiscounts[i].Checked) {
                        charges += parseFloat($scope.Model.OrderChargeDiscounts[i].Amount)
                    }

                    if ($scope.Model.OrderChargeDiscounts[i].SubCategory == 'Discount' && $scope.Model.OrderChargeDiscounts[i].Checked) {
                        discounts += parseFloat($scope.Model.OrderChargeDiscounts[i].Amount)
                    }
                }

                editFee = parseFloat($scope.Model.EditFee);

                var result = charges + editFee - discounts;

                return result;
            }

            if ($scope.Model.CancellationFee) {
                cancellationFee = parseFloat($scope.Model.CancellationFee);
            }

            if ($scope.Model.ProrateFee) {
                prorateFee = parseFloat($scope.Model.ProrateFee);
            }

            if ($scope.Model && $scope.Model.OrderChargeDiscounts) {
                for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {

                    if ($scope.Model.OrderChargeDiscounts[i].SubCategory == 'Charge' && $scope.Model.OrderChargeDiscounts[i].Checked) {
                        charges += parseFloat($scope.Model.OrderChargeDiscounts[i].Amount)
                    }

                    if ($scope.Model.OrderChargeDiscounts[i].SubCategory == 'Discount' && $scope.Model.OrderChargeDiscounts[i].Checked) {
                        discounts += parseFloat($scope.Model.OrderChargeDiscounts[i].Amount)
                    }
                }

                var result = charges + cancellationFee + prorateFee - discounts;

                return result;
            }

            return cancellationFee + prorateFee;
        }

        $scope.calculateBalance = function () {

            if ($scope.Model) {
                return $scope.calculateTotalAmount() - $scope.Model.PaidAmount;
            }
            else {
                return 0;
            }
        }

        $scope.sortByTuition = function (item) {
            var result = 0;

            switch (item.Category) {
                case 'EntryFee':
                    {
                        result = 0;
                        break;
                    }
                case 'CustomCharge':
                    {
                        result = 1000;
                        break;
                    }
                case 'CustomDiscount':
                    {
                        result = 2000;
                        break;
                    }
                default:
                    {
                        result = 100;
                        break;
                    }
            }

            if (!item.Checked) {
                result += 10000;
            }

            return result;
        }

        $scope.isApplyAutomatic = true;

        $scope.ApplyAutomaticChargeDiscount = function () {

            var totalInstallments = $scope.calculateTotalInstallments();
            var totalCalculateTotalAmount = $scope.calculateTotalAmount();

            var differanceAmount = $scope.calculateDifference();// $scope.calculateTotalInstallments() - $scope.calculateTotalAmount();


            var countDontPayInstallmentsDiscount = 0;

            var countDontPayInstallmentsCharge = 0;

            for (var i = 0; i < $scope.Model.OrderInstallments.length; i++) {
                if (!$scope.Model.OrderInstallments[i].WasDeleted && !$scope.Model.OrderInstallments[i].IsDeletedSessions && $scope.Model.OrderInstallments[i].Type != 'AddOn' && $scope.Model.OrderInstallments[i].PaidAmount == 0) {
                    countDontPayInstallmentsCharge++;
                }
            }

            for (var i = 0; i < $scope.Model.OrderInstallments.length; i++) {
                if (!$scope.Model.OrderInstallments[i].WasDeleted && !$scope.Model.OrderInstallments[i].IsDeletedSessions && $scope.Model.OrderInstallments[i].Type != 'AddOn' && $scope.Model.OrderInstallments[i].PaidAmount == 0 && $scope.Model.OrderInstallments[i].Amount != 0) {
                    countDontPayInstallmentsDiscount++;
                }
            }

            var partOfEveryMonth = 0;

            if (differanceAmount == 0) {
                jbToast.warning("Nothing to do, the amount difference is zero.");
            }
            else if (differanceAmount > 0) {

                partOfEveryMonth = (differanceAmount) / countDontPayInstallmentsCharge;

                partOfEveryMonth = Math.floor(partOfEveryMonth * 100) / 100;

                for (var i = 0; i < $scope.Model.OrderInstallments.length; i++) {
                    if (!$scope.Model.OrderInstallments[i].WasDeleted && !$scope.Model.OrderInstallments[i].IsDeletedSessions && $scope.Model.OrderInstallments[i].Type != 'AddOn' && $scope.Model.OrderInstallments[i].PaidAmount == 0) {

                        $scope.Model.OrderInstallments[i].Amount += partOfEveryMonth;

                    }
                }

                var newTotalInstallments = $scope.calculateTotalInstallments();
                var reminderAmountAfterApply = newTotalInstallments - totalCalculateTotalAmount;

                for (var i = 0; i < $scope.Model.OrderInstallments.length; i++) {
                    if (!$scope.Model.OrderInstallments[i].WasDeleted && !$scope.Model.OrderInstallments[i].IsDeletedSessions && $scope.Model.OrderInstallments[i].Type != 'AddOn' && $scope.Model.OrderInstallments[i].PaidAmount == 0) {

                        if (i == countDontPayInstallmentsCharge) {
                            reminderAmountAfterApply = reminderAmountAfterApply * (-1)
                            $scope.Model.OrderInstallments[i].Amount += reminderAmountAfterApply;
                        }

                    }
                }


                $scope.isApplyAutomatic = false;
                jbToast.success("Successfully divided " + differanceAmount + " among " + countDontPayInstallmentsCharge + " installments.");
            }
            else {

                differanceAmount = differanceAmount * (-1);

                partOfEveryMonth = (differanceAmount) / countDontPayInstallmentsDiscount;

                partOfEveryMonth = Math.floor(partOfEveryMonth * 100) / 100;

                for (var i = 0; i < $scope.Model.OrderInstallments.length; i++) {
                    if (!$scope.Model.OrderInstallments[i].WasDeleted && !$scope.Model.OrderInstallments[i].IsDeletedSessions && $scope.Model.OrderInstallments[i].Type != 'AddOn' && $scope.Model.OrderInstallments[i].PaidAmount == 0) {

                        if ($scope.Model.OrderInstallments[i].Amount > 0) {

                            var correctAmount = $scope.Model.OrderInstallments[i].Amount - partOfEveryMonth;

                            if (correctAmount > 0) {

                                $scope.Model.OrderInstallments[i].Amount -= partOfEveryMonth;
                            } else {

                                $scope.Model.OrderInstallments[i].Amount = 0;

                            }
                        }
                    }
                }


                var newTotalInstallments = $scope.calculateTotalInstallments();

                var reminderAmountAfterApply = newTotalInstallments - totalCalculateTotalAmount;


                for (var i = 0; i < $scope.Model.OrderInstallments.length; i++) {
                    if (!$scope.Model.OrderInstallments[i].WasDeleted && !$scope.Model.OrderInstallments[i].IsDeletedSessions && $scope.Model.OrderInstallments[i].Type != 'AddOn' && $scope.Model.OrderInstallments[i].PaidAmount == 0) {

                        if (i == countDontPayInstallmentsDiscount - 1) {

                            var differenceAmount = $scope.Model.OrderInstallments[i].Amount - reminderAmountAfterApply;

                            if (differenceAmount > 0) {

                                $scope.Model.OrderInstallments[i].Amount -= reminderAmountAfterApply;
                            } else {

                                $scope.Model.OrderInstallments[i].Amount = $scope.Model.OrderInstallments[i].Amount;

                            }

                        }

                    }
                }

                $scope.isApplyAutomatic = false;
                jbToast.success("Successfully divided " + differanceAmount + " among " + countDontPayInstallmentsDiscount + " installments.");
            }

        }

        $scope.UndoApplayAmount = function () {

            for (var i = 0; i < $scope.Model.OrderInstallments.length; i++) {
                if (!$scope.Model.OrderInstallments[i].WasDeleted && !$scope.Model.OrderInstallments[i].IsDeletedSessions && $scope.Model.OrderInstallments[i].Type != 'AddOn' && $scope.Model.OrderInstallments[i].PaidAmount == 0) {

                    $scope.Model.OrderInstallments[i].Amount = $scope.Model.OrderInstallments[i].OldAmountBeforeEdit;
                }
            }

            $scope.isApplyAutomatic = true;

            jbToast.success("Successfully rolled back the automatic adjustment of the installments.");
        }


        $scope.continue = function () {
            if ($scope.Model.PageStep == 'OrderDetail') {
                if ($scope.calculateTotalAmount() < 0) {

                    jbToast.error('The order amount can not be negative.');
                }
                else {
                    if ($scope.cancelMode()) {
                        $scope.Model.PageStep = 'Review';
                    }
                    else {
                        $scope.Model.PageStep = 'PaymentPlan';
                    }
                }
            }
            else if ($scope.Model.PageStep == 'PaymentPlan') {

                if (!$scope.isAnyInstallmentAmountLessThanPaidAmount() && $scope.isInstallmentsEqualsTotal()) {

                    $scope.Model.PageStep = 'Review';
                }
                else {

                    if ($scope.isAnyInstallmentAmountLessThanPaidAmount()) {

                        var message = 'The installment amount cannot be less than the amount already paid.';
                        jbToast.error(message);

                    }

                    if (!$scope.isInstallmentsEqualsTotal()) {

                        var message = 'The total sum of installments and charges is: ' + $scope.calculateTotalInstallments().formatCurrencyWithPenny(2) + ' while the order total is: ' + $scope.calculateTotalAmount().formatCurrencyWithPenny(2);
                        jbToast.error(message);

                    }
                }
            }
            else {
                $scope.edit();
            }
        }

        $scope.isAnyInstallmentAmountLessThanPaidAmount = function () {

            var result = false;

            if ($scope.hasInstallment()) {

                for (var i = 0; i < $scope.Model.OrderInstallments.length; i++) {

                    if (parseFloat($scope.Model.OrderInstallments[i].Amount) < parseFloat($scope.Model.OrderInstallments[i].PaidAmount)) {
                        result = true;
                        return result;
                    }

                }

                return result;
            }

        }

        $scope.isInstallmentsEqualsTotal = function () {
            var totalInstallment = $scope.calculateTotalInstallments();
            var totalAmount = $scope.calculateTotalAmount();
            if (!$scope.hasInstallment() || (totalAmount >= (totalInstallment - 0.1) && (totalAmount <= (totalInstallment + 0.1))) || $scope.cancelMode()) {
                return true;
            }

            return false;
        }

        $scope.back = function () {

            if ($scope.Model.PageStep == 'PaymentPlan') {
                $scope.Model.PageStep = 'OrderDetail';
            }
            else if ($scope.Model.PageStep == 'Review') {
                if ($scope.cancelMode()) {
                    $scope.Model.PageStep = 'OrderDetail';
                }
                else {
                    $scope.Model.PageStep = 'PaymentPlan';
                }
            }
            else {
                history.back();
            }
        }

        $scope.formatDate = function (date) {

            if (date != null && date != 'undefined') {
                return new Date(date).format('mm/dd/yyyy');
            }
        }
    }])
})();