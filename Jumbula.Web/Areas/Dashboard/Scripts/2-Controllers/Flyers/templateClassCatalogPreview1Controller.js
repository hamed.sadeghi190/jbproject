﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('templateClassCatalogPreview1Controller', ['$scope', '$http', 'jbToast', '$stateParams', '$state', 'templateEditorService', '$timeout', function ($scope, $http, jbToast, $stateParams, $state, templateEditorService, $timeout) {
        window.app.globalObjects.pageClass = 'form-view';
        window.app.globalObjects.initializeView();

        $scope.baseUrl = window.location.origin + "/Dashboard/Flyer/";
        $scope.MC.stateParams = $stateParams;
   
        // init model.
        $scope.Model = {};
        $scope.Model.MailTemplate = {};
        $scope.Model.CountProgram = 0;
        // use service.
        $scope.mailDesignerInit = function () {
            return templateEditorService.mailDesignerInit(this);
        }

        $scope.existingImages = templateEditorService.existingImages;   

        $http.get($scope.baseUrl + '/CreateEditFlyer').success(function (data, status, headers, config) {

            $scope.Model = data.Data;
        });

        $scope.useHtmlBreaks = function (str) {
            if (str)
                return str.replace(new RegExp('\n', 'g'), "<br>").replace(new RegExp('\r', 'g'), "<br>");
        }

        $scope.SaveFlyer = function () {
            console.log($scope.Model);
            $http.post($scope.baseUrl + '/SaveFlyerClassCatalog', { model: $scope.Model, typeFlyer: 1 }).success(function (data, status, headers, config) {
                if (data.Status == true) {
                    jbToast.success("Flyer saved");
                } else {
                    jbToast.error("An error ocured while saving");
                }
            }).error(function () {
                jbToast.error("An error ocured while saving");
            });
        }

        var timer = false;

        $scope.Model = {};


        $scope.Model.SchoolId = 0;
        $scope.Model.SeasonId = 0;


        $http.get($scope.baseUrl + '/GetAllSchool') //GetProvider delete
        .success(function (data, status, headers, config) {

            $scope.AllSchools = data;
        });

        $scope.updateChosenScope = function () {
            $("select[chosen]").trigger('chosen:updated');
        };


        $scope.$watch('AllSeasons', function () {
            if (timer) {
                $timeout.cancel(timer);
            }
            timer = $timeout(function () {
                $scope.updateChosenScope();
            }, 1500);
        });

        $scope.fillSeasonData = function (schoolId) {
            $http.get($scope.baseUrl + '/GetClubSeasons', { params: { clubId: schoolId } }).success(function (data, status, headers, config) {  ///GetClubSeasonsProvider delete
                $scope.AllSeasons = data;

            }).error(function (data, status, headers, config) {
                jbToast.error(data.Message);
            });

            $scope.Model.SchoolId = schoolId;

        }


        var checkValidation = function (schoolId, seasonId,name) {
          
            if (is.undefined(schoolId) || schoolId <= 0) {
                jbToast.error("Please select a school.");
                return false;
            }
            else if (is.undefined(seasonId) || seasonId <= 0) {
                jbToast.error("Please select a season.");
                return false;
            }
            else {
                if (name == null) {
                    jbToast.error("Please enter flyer name.");
                    return false;
                }
                else {
                    return true;
                }
              
            }
        }

        $scope.FillData = function () {

            if (checkValidation($scope.Model.SchoolId, $scope.Model.SeasonId, $scope.Model.Name)) {
               
                var seasonId = $scope.Model.SeasonId;
                var schoolId = $scope.Model.SchoolId;
                var name = $scope.Model.Name;
                $http.get($scope.baseUrl + '/GetDataFlyerClassCatalog', { params: { clubId: schoolId, seasonId: seasonId, name: name } }).success(function (data, status, headers, config) {  ///GetClubSeasonsProvider delete

                    $scope.Model = data;              
                    $scope.Model.SeasonId = seasonId;
                    $scope.Model.SchoolId = schoolId;
                 
                }).error(function (data, status, headers, config) {
                    jbToast.error(data.Message);
                });
               
            }
        }
    }]);

})();