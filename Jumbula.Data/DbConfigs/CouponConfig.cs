﻿using Jumbula.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Data.DbConfigs
{
    public class CouponConfig : EntityTypeConfiguration<Coupon>
    {
        public CouponConfig()
        {
            Property(model => model.Name).HasMaxLength(256).IsRequired();
            Property(model => model.Code).HasMaxLength(256).IsRequired();
            HasMany(model => model.OrderChargeDiscounts).WithOptional(model => model.Coupon).HasForeignKey(model => model.CouponId);
            HasMany(model => model.Programs).WithMany(model => model.Coupons);
        }
    }
}
