﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ClubCustomAttributesViewModel
    {
        public ClubCustomAttributesViewModel()
        {

        }

        public ClubCustomAttributesViewModel(Club club)
        {
            var clubAttributes = Ioc.ClubBusiness.GetAttributes(club.Id);

            var ptaFeeAttr = clubAttributes.SingleOrDefault(p => p.Attribute.Name == AttributeName.PTAFee);
            var leadGenerationAttr = clubAttributes.SingleOrDefault(p => p.Attribute.Name == AttributeName.LeadGeneration);

            if (ptaFeeAttr != null)
            {
                this.HasPTAFee = true;
                this.PTAFee = ptaFeeAttr.Value;
            }

            if (leadGenerationAttr != null)
            {
                this.HasLeadGeneration = true;
                this.LeadGeneration = leadGenerationAttr.Value;
            }
        }
        
        public string PTAFee { get; set; }
        public string LeadGeneration { get; set; }
        public bool HasPTAFee { get; set; }
        public bool HasLeadGeneration { get; set; }

        public bool HasAnyCustomAttributes
        {
            get
            {
                return HasPTAFee || HasLeadGeneration;
            }
        }
    }
}