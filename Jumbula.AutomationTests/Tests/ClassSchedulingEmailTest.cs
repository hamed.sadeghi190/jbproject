﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Helper.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace Jumbula.AutomationTests.Tests
{
    class ClassSchedulingEmailTest : BaseTest<ClassSchedulingEmailElements>
    {
        public ClassSchedulingEmailTest(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        ProgramAndOrderElements programAndOrderElements;
        public void SendConfirmationEmail()
        {
            GoToSendConfirmation("Test Notifications - Has order");
            //Assert.AreEqual("Confirmation Email", Elements.PageTitle.Text);
            CheckRecipients();

            // Check Subject and body
            Assert.AreEqual("Class confirmation for Test Notifications - Has order", Elements.Subject.GetAttribute("value"));
            Assert.AreEqual("The class has been confirmed.", Elements.EmailBody.Text);
            WebDriver.SwitchTo().DefaultContent();
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SendBtn.Click();
            ExtraAPI.Wait(10000);

            // Send with plain text
            Assert.AreEqual("Success", Elements.MessageboxTitle.Text);
            Assert.AreEqual("Email is sent successfully.", Elements.MessageboxBody.Text);
            Elements.MessageboxTitle.Click();

            // Send with new template
            GoToSendConfirmation("Test Notifications - Has order");
            Assert.AreEqual("The class has been confirmed.", Elements.EmailBody.Text);
            WebDriver.SwitchTo().DefaultContent();
            Elements.HtmlOption.Click();
            Actions actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.NewTemplateName);
            actions.Perform();
            Elements.TemplateList.Click();
            ExtraAPI.Wait(1000);
            Elements.NewTemplateOption.Click();
            ExtraAPI.Wait(1000);
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SendBtn.Click();
            ExtraAPI.Wait(3000);
            Assert.AreEqual("Please enter a name for new templates.", Elements.NewTemplateName.GetAttribute("data-original-title"));
            Elements.NewTemplateName.SendKeys("Confirmation template");
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SendBtn.Click();
            ExtraAPI.Wait(10000);
            Assert.AreEqual("Success", Elements.MessageboxTitle.Text);
            Assert.AreEqual("Email is sent successfully.", Elements.MessageboxBody.Text);
            Elements.MessageboxTitle.Click();

            // Send with created temmplate
            GoToSendConfirmation("Test Notifications - Has order");

            Elements.HtmlOption.Click();
            actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.NewTemplateName);
            actions.Perform();
            Elements.TemplateList.Click();
            ExtraAPI.Wait(1000);
            Elements.ConfirmationTemplateOption.Click();
            ExtraAPI.Wait(1000);
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SendBtn.Click();
            ExtraAPI.Wait(10000);
            Assert.AreEqual("Success", Elements.MessageboxTitle.Text);
            Assert.AreEqual("Email is sent successfully.", Elements.MessageboxBody.Text);
            Elements.MessageboxTitle.Click();

            // Program doesn't have any orders
            GoToSendConfirmation("Test Notifications - Doesn't have order");
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SendBtn.Click();
            ExtraAPI.Wait(10000);
            Assert.AreEqual("Error", Elements.MessageboxTitle.Text);
            Assert.AreEqual("You do not have recipients email.", Elements.MessageboxBody.Text);
            Elements.MessageboxTitle.Click();
        }
        public void SendCancellationEmail()
        {
            GoToSendCancellation("Test Notifications - Has order");
            //Assert.AreEqual("Cancellation Email", Elements.PageTitle.Text);
           // CheckRecipients();

            // Check Subject and body
            Assert.AreEqual("Class cancellation for Test Notifications - Has order", Elements.Subject.GetAttribute("value"));
            Assert.AreEqual("The class has been cancelled.", Elements.EmailBody.Text);
            WebDriver.SwitchTo().DefaultContent();
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SendBtn.Click();
            ExtraAPI.Wait(10000);

            // Send with plain text
            Assert.AreEqual("Success", Elements.MessageboxTitle.Text);
            Assert.AreEqual("Email is sent successfully.", Elements.MessageboxBody.Text);
            Elements.MessageboxTitle.Click();

            // Send with new template
            GoToSendCancellation("Test Notifications - Has order");
            Assert.AreEqual("The class has been cancelled.", Elements.EmailBody.Text);
            WebDriver.SwitchTo().DefaultContent();
            Elements.HtmlOption.Click();
            Actions actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.NewTemplateName);
            actions.Perform();
            Elements.TemplateList.Click();
            ExtraAPI.Wait(1000);
            Elements.NewTemplateOption.Click();
            ExtraAPI.Wait(1000);
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SendBtn.Click();
            ExtraAPI.Wait(3000);
            Assert.AreEqual("Please enter a name for new templates.", Elements.NewTemplateName.GetAttribute("data-original-title"));
            Elements.NewTemplateName.SendKeys("Cancellation template");
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SendBtn.Click();
            ExtraAPI.Wait(10000);
            Assert.AreEqual("Success", Elements.MessageboxTitle.Text);
            Assert.AreEqual("Email is sent successfully.", Elements.MessageboxBody.Text);
            Elements.MessageboxTitle.Click();

            // Send with created temmplate
            GoToSendCancellation("Test Notifications - Has order");

            Elements.HtmlOption.Click();
            actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.NewTemplateName);
            actions.Perform();
            Elements.TemplateList.Click();
            ExtraAPI.Wait(1000);
            Elements.CancellationTemplateOption.Click();
            ExtraAPI.Wait(1000);
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SendBtn.Click();
            ExtraAPI.Wait(10000);
            Assert.AreEqual("Success", Elements.MessageboxTitle.Text);
            Assert.AreEqual("Email is sent successfully.", Elements.MessageboxBody.Text);
            Elements.MessageboxTitle.Click();

            // Program doesn't have any orders
            GoToSendConfirmation("Test Notifications - Doesn't have order");
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SendBtn.Click();
            ExtraAPI.Wait(10000);
            Assert.AreEqual("Error", Elements.MessageboxTitle.Text);
            Assert.AreEqual("You do not have recipients email.", Elements.MessageboxBody.Text);
            Elements.MessageboxTitle.Click();
        }

        private void CheckRecipients()
        {
            Elements.ViewRecipientsBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("freetrial14@jumbula.com", Elements.Row1Email.Text);
            Assert.AreEqual("freetrial11@jumbula.com", Elements.Row2Email.Text);
            Assert.IsFalse(Elements.Row3Exist);
            // Search Recipients
            Elements.SearchTerm.SendKeys("11");
            Elements.SearchIcon.Click();
            ExtraAPI.Wait(1000);
            Assert.AreEqual("freetrial11@jumbula.com", Elements.Row1Email.Text);
            Assert.IsFalse(Elements.Row2Exist);

            Elements.BackLink.Click();
            ExtraAPI.Wait(1000);
        }

        private void GoToSendConfirmation(string programName)
        {
            // Search program
            programAndOrderElements = new ProgramAndOrderElements(WebDriver);
            programAndOrderElements.SearchTerm.SendKeys(programName);
            programAndOrderElements.SearchBtn.Click();
            ExtraAPI.Wait(5000);

            // Go to send confirmation notification
            programAndOrderElements.Row1Actions.Click();
            ExtraAPI.Wait(1000);
            programAndOrderElements.Row1ClassSchedulingEmailAction.Click();
            ExtraAPI.Wait(1000);
            programAndOrderElements.Row1ConfirmationEmailAction.Click();
            ExtraAPI.Wait(5000);
        }

        private void GoToSendCancellation(string programName)
        {
            // Search program
            programAndOrderElements = new ProgramAndOrderElements(WebDriver);
            programAndOrderElements.SearchTerm.SendKeys(programName);
            programAndOrderElements.SearchBtn.Click();
            ExtraAPI.Wait(5000);

            // Go to send confirmation notification
            programAndOrderElements.Row1Actions.Click();
            ExtraAPI.Wait(1000);
            programAndOrderElements.Row1ClassSchedulingEmailAction.Click();
            ExtraAPI.Wait(1000);
            programAndOrderElements.Row1CancelletionEmailAction.Click();
            ExtraAPI.Wait(5000);
        }
    }
}
