﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Jumbula.Web.Mvc.UIHelpers;
using Constants=Jumbula.Common.Constants.Constants;


namespace Jumbula.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("Scripts/{*pathInfo}");
            routes.IgnoreRoute("images/{*pathInfo}");
            routes.IgnoreRoute("content/{*pathInfo}");
            routes.IgnoreRoute("bundles/{*pathInfo}");
            routes.IgnoreRoute("fonts/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });

            routes.MapRoute(
               name: "Schedule",
               url: "Schedule/{action}/{id}",
               defaults: new { controller = "Schedule", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
             name: "Error",
             url: "Error/{code}",
             defaults: new { controller = "Home", action = "Error", code = UrlParameter.Optional },
             namespaces: new string[] { "Jumbula.Web.Controllers" }
           );


            routes.MapRoute(
             name: "Default",
             url: "{action}",
             defaults: new { controller = "Home", action = "Index" },
             namespaces: new string[] { "Jumbula.Web.Controllers" },
             constraints: new { pageUrl = new HomeActionsRouteConstraint() }
          );

            routes.MapRoute(
              name: "Home",
              url: "",
              defaults: new { controller = "Home", action = "Index" },
              namespaces: new string[] { "Jumbula.Web.Controllers" },
              constraints: new { pageUrl = new NotBrandedModeOnlyRouteConstraint() }
           );

            routes.MapRoute(
                name: "DataManipulation",
                url: "DataManipulation/{action}/{id}",
                defaults: new { controller = "DataManipulation", action = "Index", id = UrlParameter.Optional }
            );

            #region hard code for em and jbpage release

            routes.MapRoute(
                 name: "AzadehSpring2016",
                 url: "Spring2016",
                 defaults: new { controller = "Season", action = "Index", id = UrlParameter.Optional },
                 namespaces: new string[] { "Jumbula.Web.Controllers" }
               );

            routes.MapRoute(
                name: "AzadehSummerCamp",
                url: "SummerCamp",
                defaults: new { controller = "Season", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "Jumbula.Web.Controllers" }
             );

            routes.MapRoute(
               name: "AzadehSpringCamp",
               url: "SpringCamp",
               defaults: new { controller = "Season", action = "Index", id = UrlParameter.Optional },
               namespaces: new string[] { "Jumbula.Web.Controllers" }
            );

            routes.MapRoute(
                 name: "SCASpringProgram",
                 url: "SCASpringProgram",
                 defaults: new { controller = "Season", action = "Index", id = UrlParameter.Optional },
                 namespaces: new string[] { "Jumbula.Web.Controllers" }
            );

            #endregion

            routes.MapRoute(
              name: "LoginNotBranded",
              url: "Login",
              defaults: new { controller = "Account", action = "LoginNotBranded" },
              namespaces: new string[] { "Jumbula.Web.Controllers" },
              constraints: new { pageUrl = new NotBrandedModeOnlyRouteConstraint() }
            );


            routes.MapRoute(
              name: "LoginBranded",
              url: "Login",
              defaults: new { controller = "Account", action = "LoginBranded" },
              namespaces: new string[] { "Jumbula.Web.Controllers" },
              constraints: new { pageUrl = new BrandedModeOnlyRouteConstraint() }
            );

            routes.MapRoute(
              name: "SignUp",
              url: "SignUp",
              defaults: new { controller = "Account", action = "Register" },
              namespaces: new string[] { "Jumbula.Web.Controllers" },
              constraints: new { pageUrl = new BrandedModeOnlyRouteConstraint() }
            );

            routes.MapRoute(
                name: "LoginBranded2",
                url: "Login2",
                defaults: new { controller = "Account", action = "Login2" },
                namespaces: new string[] { "Jumbula.Web.Controllers" },
                constraints: new { pageUrl = new BrandedModeOnlyRouteConstraint() }
            );


            routes.MapRoute(
             name: "DedicatedAccount",
             url: "DedicatedAccount/CheckIsUserNameExist/",
             defaults: new { controller = "Account", action = "CheckIsUserNameExist" },
             namespaces: new string[] { "Jumbula.Web.Controllers" }
           );

            routes.MapRoute(
              name: "AccountBranded",
              url: "Account/{action}",
              defaults: new { controller = "Account", action = "index" },
              namespaces: new string[] { "Jumbula.Web.Controllers" },
              constraints: new { pageUrl = new BrandedModeOnlyRouteConstraint() }
           );

            routes.MapRoute(
               name: "AccountNotBranded",
               url: "Account/{action}",
               defaults: new { controller = "Account", action = "index" },
               namespaces: new string[] { "Jumbula.Web.Controllers" },
               constraints: new { pageUrl = new NotBrandedModeOnlyRouteConstraint() }
            );

            routes.MapRoute(
                 name: "File",
                 url: "File/{action}",
                 defaults: new { controller = "File", action = "index" },
                 namespaces: new string[] { "Jumbula.Web.Controllers" }
                 //constraints: new { pageUrl = new BrandedModeOnlyRouteConstraint() }
              );
            routes.MapRoute(
                name: "Markets",
                url: "Markets/{action}",
                defaults: new { controller = "Markets", action = "Index" },
                namespaces: new string[] { "Jumbula.Web.Controllers" }
            );

            routes.MapRoute(
              name: "StaticPages",
              url: "{action}/{section}",
              defaults: new { controller = "Pages", action = "Index", section = UrlParameter.Optional },
              constraints: new { pageUrl = new StaticPagesRouteConstraint() }
           );

            routes.MapRoute(
             name: "Pages",
             url: "Pages/{action}/{id}",
             defaults: new { controller = "Pages", action = "Index", id = UrlParameter.Optional }
           );

            routes.MapRoute(
             name: "KendoEditorFiles",
             url: "KendoEditorFiles/{action}/{id}",
             defaults: new { controller = "KendoEditorFiles", action = "Read", id = UrlParameter.Optional },
             namespaces: new string[] { "Jumbula.Web.Controllers" }
          );


            routes.MapRoute(
              name: "Registrant",
              url: "Registrant/{action}/{id}",
              defaults: new { controller = "Registrant", action = "GetFamiliesProfile", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             name: "Message",
             url: "Message/{action}/{id}",
             defaults: new { controller = "Message", action = "Index", id = UrlParameter.Optional }
           );

            routes.MapRoute(
              name: "FollowupForm",
              url: "FollowupForm/{action}",
              defaults: new { controller = "FollowupForm", action = "Index", id = UrlParameter.Optional }
           );

            routes.MapRoute(
                name: "SubDomain",
                url: "SubDomain/{action}/{id}",
                defaults: new { controller = "SubDomain", action = "Index", id = UrlParameter.Optional }
                );

            routes.MapRoute(
                 name: "Email",
                 url: "Email/{action}",
                 defaults: new { controller = "Email", action = "Index", id = UrlParameter.Optional },
                  namespaces: new string[] { "Jumbula.Web.Controllers" }
                );

            routes.MapRoute(
                name: "Migrate",
                url: "Migrate/{action}",
                defaults: new { controller = "Migrate", action = "Index", id = UrlParameter.Optional }
                );

            routes.MapRoute(
                 name: "Club",
                 url: "club/{action}/{id}",
                 defaults: new { controller = "Club", action = "Index", id = UrlParameter.Optional },
                 constraints: new { program = new RestrictedClubActionRouteConstraint() },
                 namespaces: new string[] { "Jumbula.Web.Controllers" }
                );

            routes.MapRoute(
                name: "Paypal",
                url: "Paypal/{action}/{id}",
                defaults: new { controller = "Paypal", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
             name: "Stripe",
             url: "Stripe/{action}/{id}",
             defaults: new { controller = "Stripe", action = "Index", id = UrlParameter.Optional }
         );
            routes.MapRoute(
           name: "StripeListener",
           url: "StripeWebhook/{action}",
           defaults: new { controller = "StripeWebhook", action = "StripeWebhookListner", id = UrlParameter.Optional }
       );
            routes.MapRoute(
               name: "PlayerProfile",
               url: "PlayerProfile/{action}/{id}",
               defaults: new { controller = "PlayerProfile", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "FormBuilder",
               url: "formbuilder/{action}/{id}",
               defaults: new { controller = "FormBuilder", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Report",
               url: "Report/{action}/{id}",
               defaults: new { controller = "Report", action = "Index", id = UrlParameter.Optional }
               );

            routes.MapRoute(
               name: "Forms",
               url: "Forms/{action}/{id}",
               defaults: new { controller = "Forms", action = "DisplayForms", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Engine",
               url: "engine/{action}/{id}",
               defaults: new { controller = "Engine", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              name: "Support",
              url: "Support/{action}/{id}",
              defaults: new { controller = "Support", action = "Index", id = UrlParameter.Optional }
              );

            routes.MapRoute(
                name: "FeedBack",
                url: "FeedBack/{action}/{id}",
                defaults: new { controller = "FeedBack", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ContactUs",
                url: "ContactUs/{action}/{id}",
                defaults: new { controller = "ContactUs", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ClubMembers",
                url: "ClubMembers/{action}/{id}",
                defaults: new { controller = "ClubMembers", action = "DisplayMembers", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Coupon",
                url: "Coupon/{action}/{id}",
                defaults: new { controller = "Coupon", action = "DisplayCoupons", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Discount",
               url: "Discount/{action}/{id}",
               defaults: new { controller = "Discount", action = "Discounts", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "PaymentPlan",
               url: "PaymentPlan/{action}/{id}",
               defaults: new { controller = "PaymentPlan", action = "PaymentPlans", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              name: "DisplayConfirmation",
              url: "DisplayConfirmation/{cid}",
              defaults: new { controller = "Order", action = "DisplayConfirmation" }
            );

            routes.MapRoute(
              name: "Cart",
              url: "cart/{action}",
              defaults: new { controller = "Cart", action = "Index" }
            );

            routes.MapRoute(
               name: "Order",
               url: "order/{action}/{id}",
               defaults: new { controller = "Order", action = "Index", id = UrlParameter.Optional }
           );

            routes.MapRoute(
                name: "Upload",
                url: "upload/{action}/{id}",
                defaults: new { controller = "Upload", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Program",
                url: "Program/{action}/{id}",
                defaults: new { controller = "Program", action = "Index", id = UrlParameter.Optional },
                constraints: new { program = new RestrictedProgramActionRouteConstraint() },
                namespaces: new string[] { "Jumbula.Web.Controllers" }
            );

            routes.MapRoute(
            name: "SeasonGetUrlForLogin",
            url: "Season/GetUrlForLogin",
            defaults: new { controller = "JbPage", action = "GetUrlForLogin" },
            namespaces: new string[] { "Jumbula.Web.Controllers" }
         );

            routes.MapRoute(
               name: "Season",
               url: "Season/{action}",
               defaults: new { controller = "JbPage", pageId = UrlParameter.Optional },
               constraints: new { season = new RestrictedSeasonActionRouteConstraint() },
               namespaces: new string[] { "Jumbula.Web.Controllers" }
            );

            routes.MapRoute(
              name: "JbPage",
              url: "JbPage/{action}",
              defaults: new { controller = "JbPage" },
              constraints: new { season = new RestrictedSeasonActionRouteConstraint() },
              namespaces: new string[] { "Jumbula.Web.Controllers" }
           );

            routes.MapRoute(
                name: "RegisterProgram",
                url: "{seasonDomain}/{domain}/Register/{ists}",
                defaults: new { controller = "Register", action = "Register", ists = UrlParameter.Optional },
                constraints: new { club = new BrandedModeOnlyRouteConstraint(true) },
                namespaces: new string[] { "Jumbula.Web.Controllers" }
           );

            routes.MapRoute(
               name: "MultipleRegisterProgram",
               url: "{seasonDomain}/Register/{grade}",
               defaults: new { controller = "Register", action = "MultipleRegister", grade = UrlParameter.Optional },
               constraints: new { club = new BrandedModeOnlyRouteConstraint(true) },
               namespaces: new string[] { "Jumbula.Web.Controllers" }
            );

            routes.MapRoute(
                name: "Register",
                url: "Register/{action}/{id}",
                defaults: new { controller = "Register", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "Jumbula.Web.Controllers" }
            );

            routes.MapRoute(
               name: Constants.W_Route_ShowSpecificTournament,
               url: "Tournament/{domain}",
               defaults: new { controller = "chess", action = "Display" },
               constraints: new { club = new BrandedModeOnlyRouteConstraint(true) },
               namespaces: new string[] { "Jumbula.Web.Controllers" }
            );

            routes.MapRoute(
              name: "DonationSubDomain",
              url: "donation",
              defaults: new { controller = "Club", action = "Donation" },
              constraints: new { club = new BrandedModeOnlyRouteConstraint(true) },
              namespaces: new string[] { "Jumbula.Web.Controllers" }
           );

            #region

            routes.MapRoute(
                name: "SeasonManage",
                url: "{seasonDomain}/Manage/{isIframe}/{saveType}/{preview}/{mode}",
                defaults: new { controller = "JbPage", action = "Manage", isIframe = UrlParameter.Optional, saveType = UrlParameter.Optional, preview = false },
                constraints: new { club = new BrandedModeOnlyRouteConstraint(true) },
                namespaces: new string[] { "Jumbula.Web.Controllers" }
            );

            routes.MapRoute(
               name: "SeasonDelete",
               url: "{seasonDomain}/Delete",
               defaults: new { controller = "JbPage", action = "Delete" },
               constraints: new { club = new BrandedModeOnlyRouteConstraint(true) },
               namespaces: new string[] { "Jumbula.Web.Controllers" }
           );

            routes.MapRoute(
                 name: "SeasonUploadCover",
                 url: "{seasonDomain}/UploadCover",
                 defaults: new { controller = "JbPage", action = "UploadCover" },
                 constraints: new { club = new BrandedModeOnlyRouteConstraint(true) },
                 namespaces: new string[] { "Jumbula.Web.Controllers" }
            );

            routes.MapRoute(
                name: "RemoveSeasonUploadCover",
                url: "{seasonDomain}/RemoveUploadedCover",
                defaults: new { controller = "JbPage", action = "RemoveUploadedCover" },
                constraints: new { club = new BrandedModeOnlyRouteConstraint(true) },
                namespaces: new string[] { "Jumbula.Web.Controllers" }
           );

            routes.MapRoute(
               name: "SeasonUpdatePageSettings",
               url: "{seasonDomain}/UpdatePageSettings",
               defaults: new { controller = "JbPage", action = "UpdatePageSettings" },
               constraints: new { club = new BrandedModeOnlyRouteConstraint(true) },
               namespaces: new string[] { "Jumbula.Web.Controllers" }
           );

            routes.MapRoute(
              name: "SeasonGetElementTemplates",
              url: "{seasonDomain}/GetElementTemplates",
              defaults: new { controller = "JbPage", action = "GetElementTemplates" },
              constraints: new { club = new BrandedModeOnlyRouteConstraint(true) },
              namespaces: new string[] { "Jumbula.Web.Controllers" }
            );

            #endregion


            routes.MapRoute(
              name: "HomePageBranded",
              url: "",
              defaults: new { controller = "JbPage", action = "Index" },
              constraints: new { club = new BrandedModeOnlyRouteConstraint() },
              namespaces: new string[] { "Jumbula.Web.Controllers" }
            );

            routes.MapRoute(
              name: "HomePageEdit",
              url: "Edit",
              defaults: new { controller = "JbPage", action = "Index", mode = "edit" },
              constraints: new { club = new BrandedModeOnlyRouteConstraint() },
              namespaces: new string[] { "Jumbula.Web.Controllers" }
            );

            routes.MapRoute(
            name: "HomePageView",
            url: "View",
            defaults: new { controller = "JbPage", action = "Index", mode = "view" },
            constraints: new { club = new BrandedModeOnlyRouteConstraint() },
            namespaces: new string[] { "Jumbula.Web.Controllers" }
          );

            routes.MapRoute(
                 name: Constants.W_Route_ShowSpecificProgram,
                 url: "{seasonDomain}/{domain}",
                 defaults: new { controller = "Program", action = "Display" },
                 constraints: new { club = new BrandedModeOnlyRouteConstraint(true) },
                 namespaces: new string[] { "Jumbula.Web.Controllers" }
           );

            routes.MapRoute(
               name: Constants.W_Route_ShowSpecificProgramInViewMode,
               url: "{seasonDomain}/{domain}/View",
               defaults: new { controller = "Program", action = "View" },
               constraints: new { club = new BrandedModeOnlyRouteConstraint(true) },
               namespaces: new string[] { "Jumbula.Web.Controllers" }
            );
            routes.MapRoute(
                name: "Chess",
                url: "chess/{action}/{id}",
                defaults: new { controller = "Chess", action = "Index", id = UrlParameter.Optional }
           );

            routes.MapRoute(
                name: "Verification",
                url: "Verification/{action}/{id}",
                defaults: new { controller = "Verification", action = "Index", id = UrlParameter.Optional }
           );

        }
    }

    public class RestrictedClubActionRouteConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            string[] restrictedActions = new string[] { "display" };

            string action = values["action"].ToString();

            if (restrictedActions.Where(p => p.Equals(action.ToLower())).Any())
            {
                return false;
            }

            return true;
        }
    }

    public class HomeActionsRouteConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            if (httpContext.Request.Url.HasSubDaomin())
            {
                return false;
            }

            string[] homeActions = {
                                       "solution",
                                       "pricing",
                                       "channel",
                                       "error",
                                       "portfolio",
                                       "contact",
                                       "newslettersubscribe",
                                       "comparisontoactivityhero",
                                       "moreconvincing",
                                       "freetrial",
                                       "agreement",
                                       "blog",
                                       "freetrial",
                                       "forgotpassword",
                                       "sendrequestdemo"
                                   };

            string action = values["action"].ToString();

            if (homeActions.Where(p => p.Equals(action, StringComparison.OrdinalIgnoreCase)).Any())
            {
                return true;
            }

            return false;
        }
    }

    public class StaticPagesRouteConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            string[] staticPages = {
                                       "bsj",
                                       "balletsanjose",
                                       "svb",
                                       "funmandarin",
                                       "ventana",
                                       "ljesfall2015",
                                       "learnerschessfall2015",
                                       "chessodysseyprograms",
                                       "sequoia"
                                   };

            string action = values["action"].ToString();

            if (staticPages.Where(p => p.Equals(action, StringComparison.OrdinalIgnoreCase)).Any())
            {
                return true;
            }

            return false;
        }
    }

    public class RestrictedProgramActionRouteConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            if ((routeDirection == RouteDirection.UrlGeneration))
            {
                string[] restrictedActions = new string[] { "display", "register" };

                string action = values["action"].ToString();

                if (restrictedActions.Where(p => p.Equals(action.ToLower())).Any())
                {
                    return false;
                }
            }

            return true;
        }
    }

    public class RestrictedSeasonActionRouteConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            if ((routeDirection == RouteDirection.UrlGeneration))
            {
                string[] restrictedActions = new string[] { "index" };

                string action = values["action"].ToString();

                if (restrictedActions.Where(p => p.Equals(action.ToLower())).Any())
                {
                    return false;
                }
            }

            return true;
        }
    }

    public class RestrictedSubdomainActionRouteConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            if ((routeDirection == RouteDirection.UrlGeneration))
            {
                string[] restrictedActions = new string[] { "chooseprofile" };

                string action = values["action"].ToString();

                if (restrictedActions.Where(p => p.Equals(action.ToLower())).Any())
                {
                    return false;
                }
            }

            return true;
        }
    }

    public class ClubDomainRouteConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            if (values["clubDomain"].ToString().Contains('/'))
            {
                return false;
            }

            return true;
        }
    }

    public class NotBrandedModeOnlyRouteConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            if (httpContext.Request.Url.HasSubDaomin())
            {
                return false;
            }

            return true;
        }
    }

    public class BrandedModeOnlyRouteConstraint : IRouteConstraint
    {
        private bool _setClubDomain;
        public BrandedModeOnlyRouteConstraint(bool setClubDomain = false)
        {
            _setClubDomain = setClubDomain;
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            if (httpContext.Request.Url.HasSubDaomin())
            {
                if (_setClubDomain && !values.Keys.Contains("clubDomain"))
                {
                    values.Add("clubDomain", httpContext.Request.Url.GetSubDomain());
                }

                return true;
            }

            return false;
        }
    }
}