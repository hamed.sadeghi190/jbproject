﻿using Jumbula.Common.Utilities;
using Jumbula.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Jumbula.Core.Domain;

namespace Jumbula.Data.Repositories
{
    public class ContactRepository : BaseRepository<ContactPerson>, IContactRepository
    {
        public ContactRepository(IEntitiesContext context)
            : base(context)
        {
        }

        public PaginatedList<ContactPerson> GetClubContacts(List<int> clubIds, Expression<Func<ContactPerson, bool>> predicate, string emailAddress, List<Sort> sorts, int pageIndex, int pageSize)
        {
            var staffContactIds = Context.Set<ClubStaff>().Where(s => clubIds.Contains(s.ClubId) && !s.IsDeleted && (string.IsNullOrEmpty(emailAddress) || (!string.IsNullOrEmpty(emailAddress) && s.JbUserRole.User.UserName.Contains(emailAddress)))).Select(s => s.ContactId).ToList();

            var contacts = GetList().Where(c => (clubIds.Contains(c.Club_Id.Value) && (string.IsNullOrEmpty(emailAddress) || !string.IsNullOrEmpty(emailAddress) && c.Email.Contains(emailAddress))) || staffContactIds.Contains(c.Id)).Distinct();
            contacts = contacts.Where(predicate);

            contacts = contacts.OrderBy(sorts).AsQueryable();
            var total = contacts.Count();

            contacts = contacts.Paginate(pageIndex, pageSize);
            return contacts.ToPaginatedList(pageIndex, pageSize, total);
        }
    }
}
