﻿using Jumbula.Common.Utilities;

namespace Jumbula.Core.Model.Report.Filter
{
    public class DataModel
    {
        public PaginatedList<object> DataSource { get; set; }
        public int TotalCount { get; set; }
    }
}
