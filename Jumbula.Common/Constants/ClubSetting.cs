﻿namespace Jumbula.Common.Constants
{
    public static class ClubSetting
    {
        public const string AutoChargePolicyBlockTitle = "The {0} additional attempt";

        //ToDo, we need data-migration setting field if we want change this count 
        public const int CountOfAttempts = 5;
                                                                           
        public const string AutoChargeDefaultParentSuccessEmailTemplate = "Congratulations, the following auto-charged payment has been received.";
        public const string AutoChargeDefaultAdminSuccessEmailTemplate = "Dear Team,\n\nPlease be advised that the following transaction has been successfully processed.\nParent has been informed via email.\n\nBest,\nJumbula Support Team";

        public const string AutoChargeAttemptOneDefaultParentFailedEmailTemplate = "Dear Client,\n\nThis email is to inform you that your recent auto-charged transaction was declined. We will attempt to run this transaction one more time.\nYour prompt attention to this matter would be greatly appreciated. If you have any queries regarding this account, please contact our office as soon as possible.";
        public const string AutoChargeAttemptOneDefaultAdminFailEmailTemplate = "Dear Team,\n\nPlease be advised that the following transaction has failed.\nParent has been informed via email.\n\nBest,\nJumbula Support Team";

        public const string AutoChargeDefaultParentFailedEmailTemplate = "Dear Client,\n\nThis email is to inform you that your recent auto-charged transaction was declined. We will attempt to run this transaction one more time.\nYour prompt attention to this matter would be greatly appreciated. If you have any queries regarding this account, please contact our office as soon as possible.";
        public const string AutoChargeDefaultAdminFailEmailTemplate = "Dear Team,\n\nPlease be advised that the following transaction has failed.\nParent has been informed via email.\n\nBest,\nJumbula Support Team";
    }
}
