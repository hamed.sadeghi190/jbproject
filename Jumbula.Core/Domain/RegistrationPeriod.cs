﻿using System;

namespace Jumbula.Core.Domain
{
    public class RegistrationPeriod
    {
        public RegistrationPeriod()
        {

        }

        public RegistrationPeriod(RegistrationPeriod registrationPeriod, DateTime? seasonOpenDateTime, DateTime? seasonCloseDateTime)
        {
            RegisterStartDate = seasonOpenDateTime.HasValue ? seasonOpenDateTime.Value.Date : registrationPeriod.RegisterStartDate;
            RegisterEndDate = seasonCloseDateTime.HasValue ? seasonCloseDateTime.Value.Date : registrationPeriod.RegisterEndDate;

            RegisterStartTime = seasonOpenDateTime.HasValue ? seasonOpenDateTime.Value.TimeOfDay : registrationPeriod.RegisterStartTime;
            RegisterEndTime = seasonCloseDateTime.HasValue ? seasonCloseDateTime.Value.TimeOfDay : registrationPeriod.RegisterEndTime;
        }

        //[Required(ErrorMessage="Register start date is required")]
        public DateTime? RegisterStartDate { get; set; }
        public TimeSpan? RegisterStartTime { get; set; }

        //[Required(ErrorMessage = "Register end date is required")]
        public DateTime? RegisterEndDate { get; set; }
        public TimeSpan? RegisterEndTime { get; set; }
    }

}
