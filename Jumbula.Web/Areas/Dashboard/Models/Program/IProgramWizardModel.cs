﻿using SportsClub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    interface IProgramWizardModel
    {
        long Id { get; set; }

        PageMode PageMode { get;  }

        ProgramPageStep PageStep { get; set; }

        ProgramPageStep LastCreatedPage { get; set; }

        SaveType SaveType { get; set; }

        ////
        string ClubDomain { get; set; }

        string SeasonDomain { get; set; }

        string Domain { get; set; }

        string Name { get; set; }

        ProgramStatus Status { get; set; }

        ProgramRegStatus RegStatus { get; set; }

        ProgramRunningStatus RunningStatus { get; set; }

        ProgramTypeCategory TypeCategory { get; set; }
        ////
    }
}
