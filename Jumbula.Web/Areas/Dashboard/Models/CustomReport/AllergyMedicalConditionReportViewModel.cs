﻿using System.Collections.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class AllergyMedicalConditionReportViewModel
    {
        public string SectionName { get; set; }
        public List<AllergyMedicalConditionReportParticipantViewModel> SectionParticipant { get; set; }
    }
}