﻿using Jumbula.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Data.DbConfigs
{
    public class PaypalIPNConfig : EntityTypeConfiguration<PaypalIPN>
    {
        public PaypalIPNConfig()
        {
            HasKey(c => c.Id);

        }
    }
}
