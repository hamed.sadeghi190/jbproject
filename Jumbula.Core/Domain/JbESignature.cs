﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class JbESignature : BaseEntity
    {
        public DateTime CreateDate { get; set; }
        public string DocumentName { get; set; }
        public int SenderId { get; set; }
        [ForeignKey("SenderId")]
        public virtual Club Sender { get; set; }
        public int? ReciverId { get; set; }
        [ForeignKey("ReciverId")]
        public virtual Club Reciver { get; set; }
        [StringLength(400)]
        public string Subject { get; set; }
        [StringLength(2000)]
        public string Content { get; set; }
        public StorageMetaData MetaData { get; set; }
        public string Signature { get; set; }
        public DateTime? DeadLine { get; set; }
        public SignatureStatus Status { get; set; }
        public int? MessageId { get; set; }
        [ForeignKey("MessageId")]
        public virtual MessageHeader Message { get; set; }
        public DateTime? SignatureDate { get; set; }
        public string SignatureTitle { get; set; }
        public string SignedBy { get; set; }
        [StringLength(200)]
        public string ReciverName { get; set; }

    }

    public enum SignatureStatus : byte
    {
        [Description("In progress")]
        Inprogress = 0,
        [Description("Completed")]
        Completed = 1,
        [Description("Voided")]
        Voided = 2,
        [Description("Declined")]
        Declined =3

    }

}
