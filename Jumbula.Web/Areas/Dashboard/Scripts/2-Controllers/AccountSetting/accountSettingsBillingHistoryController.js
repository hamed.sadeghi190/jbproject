﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('accountSettingsBillingHistoryController', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'account-settings-view';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/AccountSettings", order: 2, menu: "SETTINGS" });
        $scope.baseUrl = window.location.origin + "/Dashboard/AccountSetting";


        $scope.mainGridDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: $scope.baseUrl + '/GetAllBillingHistories',
                        cache: false,
                        type: "POST",
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: '',
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: null,
                                printMode: false
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            sortable: false,
            serverFiltering: true,
            serverSorting: false,
            serverPaging: true,
            pageSize: 10,
        });

        $scope.mainGridOptions = {
            dataSource: $scope.mainGridDataSource,
            sortable: false,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            columns: [
                {
                    field: "Date",
                    title: "Transaction date"
                },
                
                {
                    field: "Description",
                    title: "Description",
                    template: "#if(Credit >0) {# <a ui_sref='BillingHistoryReceipt({billingId:#=Id#})'  class='block'> #= Description # </a> #} else {# <p>#=Description#</p> #}#"
                }
            , {
                field: "StrDebit",
                title: "Debit",
            },
             {
                 field: "StrCredit",
                 title: "Credit",
             },
             {
                 field: "Balance",
                 title: 'Balance',
                 template: "<span>#if(Balance >0) {# <p class='fg-red'>#=(Balance).formatCurrencyWithPenny(2)#</p> #} else if(Balance <0) {# <p class='fg-green'>#=(Balance).formatCurrencyWithPenny(2)#</p> #} else if(Balance ==0) {# <p>#=Balance#</p> #}#"
             }

            ],
        };


        $scope.saveToPDF = function () {

            
            $http.post($scope.baseUrl + '/GetAllBillingHistories', {
                term: '',
                skip: 0,
                take: 0,
                page: 0,
                pageSize: 0,
                sort: null,
                printMode: true
            }, {
                headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
            }).success(function (data) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                var blob = new Blob([data], { type: "application/pdf" });

                var fileURL = URL.createObjectURL(blob);
                a.href = fileURL;
                a.download = "Billing History.pdf";
                a.click();
            });

        }

    }]);
})();