﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SchoolEnrollmentsPdfViewModel
    {
        public SchoolEnrollmentsPdfViewModel()
        {
            //Schools = new List<Models.SchoolsEnrollmentReportViewModel>();
        }
        public string ClubLogo { get; set; }

        public List<SchoolsEnrollmentReportViewModel> Schools { get; set; }
    }
}