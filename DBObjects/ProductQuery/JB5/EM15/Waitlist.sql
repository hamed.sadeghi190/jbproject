
CREATE TABLE [dbo].[WaitLists](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[UserId] [int] NOT NULL,
	[ProgramId] [bigint] NOT NULL,
	[Grade] [int] NOT NULL,
CONSTRAINT [PK_dbo.WaitLists] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[WaitLists] ADD  DEFAULT ((0)) FOR [Grade]
GO

ALTER TABLE [dbo].[WaitLists]  WITH CHECK ADD  CONSTRAINT [FK_dbo.WaitLists_dbo.JbUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[JbUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[WaitLists] CHECK CONSTRAINT [FK_dbo.WaitLists_dbo.JbUsers_UserId]
GO

ALTER TABLE [dbo].[WaitLists]  WITH CHECK ADD  CONSTRAINT [FK_dbo.WaitLists_dbo.Programs_ProgramId] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Programs] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[WaitLists] CHECK CONSTRAINT [FK_dbo.WaitLists_dbo.Programs_ProgramId]
GO
