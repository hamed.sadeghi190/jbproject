﻿

namespace Jumbula.Common.Enums
{
    public enum AvatarSizeType
    {
        Large,
        Medium,
        Small
    }
}
