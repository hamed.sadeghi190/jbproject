﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('orderController', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/Season", order: 1, menu: "SEASONS" });
        $scope.baseUrl = window.location.origin + "/Order";
        $scope.seasonName = $stateParams.seasonDomain;
        $scope.programName = $stateParams.programDomain;
        $scope.MC.stateParams = $stateParams;
        $scope.Model = {};
        $scope.Model.Id = $stateParams.orderId;



        $http.get(window.location.origin + '/Dashboard/Order/GetDetails/?orderId=' + $scope.Model.Id).success(function (data, status, headers, config) {
            $scope.Model = data;

        }).error(function (data, status, headers, config) {
            $scope.MC.handleErrors($scope, data);
        });

        $scope.showDetail=function(itemId)
        {
            window.goBackToOverView = true;
            $state.go('OrderItem', { seasonDomain: $scope.MC.stateParams.seasonDomain, orderItemId: itemId });
        }
        $scope.goBack = function () {
            if ("goBackToOverView" in window) {
                if (window.goBackToOverView) {
                    window.goBackToOverView = false;
                    window.history.back();
                }
                
            } else {
                window.history.back();
            }
        }
    }]);
})();