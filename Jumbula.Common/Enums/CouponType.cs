﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum CouponType
    {
        [Description("Most expensive order in the cart")]
        MostExpensiveOrder = 0,

        [Description("Each order in the cart")]
        AllSchedule = 1,

        [Description("Most expensive order of each program")]
        MostExpensiveSchedule = 2
    }
}
