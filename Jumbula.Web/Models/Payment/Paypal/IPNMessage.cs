﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;

namespace SportsClub.Models.Payment
{
    public class IPNMessage
    {
    
        private NameValueCollection nvcMap = new NameValueCollection();

       
        public string IpnRequest = string.Empty;


        private void Initialize(NameValueCollection nvc)
        {
            List<string> items = new List<string>();
            try
            {
                if (nvc.HasKeys())
                {
                    foreach (string key in nvc.Keys)
                    {
                        items.Add(string.Concat(key, "=", System.Web.HttpUtility.UrlEncode(nvc[key], IpnEncoding)));
                        nvcMap.Add(key, nvc[key]);
                    }
                    IpnRequest = string.Join("&", items.ToArray()) ;
                }
            }
            catch (System.Exception ex)
            {
                LogHelper.LogException(ex);
            }
        }
       
     
        public IPNMessage(NameValueCollection nvc)
        {
            this.Initialize(nvc);
        }


       


        public IPNMessage(byte[] parameters)
        {

            this.Initialize(HttpUtility.ParseQueryString(IpnEncoding.GetString(parameters), IpnEncoding));
        }

        public IPNMessage(string paypalIpn)
        {
            this.Initialize(HttpUtility.ParseQueryString(paypalIpn, IpnEncoding));
        }

        public string Preapprovalkey
        {
            get
            {
                return this.nvcMap["preapproval_key"] != null ? this.nvcMap["preapproval_key"] : string.Empty;
            }
        }

        public bool PreapprovalApproved
        {
            get
            {
                var approved = false;
                if( this.nvcMap["approved"] != null )
                {
                    bool.TryParse(this.nvcMap["approved"],out approved);
                }
                return approved;
            }
        }
        public NameValueCollection IpnMap
        {
            get
            {
                return nvcMap;
            }
        }

       
        public string TransactionType
        {
            get
            {
                return this.nvcMap["txn_type"] != null ? this.nvcMap["txn_type"] :
                    (this.nvcMap["transaction_type"] != null ? this.nvcMap["transaction_type"] : null);
            }
        }

        public PaymentDetailStatus Status
        {
            get
            {
                PaymentDetailStatus status = PaymentDetailStatus.ERROR;
                if(this.nvcMap["status"] != null)
                {

                    Enum.TryParse<PaymentDetailStatus>(this.nvcMap["status"], out status);
                   
                }
                else if (this.nvcMap["payment_status"] != null)
                    {
                        Enum.TryParse<PaymentDetailStatus>(this.nvcMap["payment_status"], out status);
                    }
                return status;
            }
        }

        public string TransactionId
        {
            get
            {
                if (this.nvcMap["txn_id"] != null)
                {
                    return this.nvcMap["txn_id"];
                }
                else if (this.nvcMap["transaction_id"] != null)
                {
                    return this.nvcMap["transaction_id"];
                }
                else if (this.nvcMap["transaction[0].id"]!=null)
                {
                    return this.nvcMap["transaction[0].id"];
                }
                return string.Empty;
            }
        }
        public string SenderEmail
        {
            get { return (this.nvcMap["sender_email"]!=null)? nvcMap["sender_email"]:string.Empty;}
        }
        public string ActionType
        {
            get { return (this.nvcMap["action_type"] != null) ? nvcMap["action_type"] : string.Empty; }
        }
        public string PaymentRequestDate
        {
            get { return (this.nvcMap["payment_request_date"] != null) ? nvcMap["payment_request_date"] : string.Empty; }
        }
        public string PayKey
        {
            get { return (this.nvcMap["pay_key"] != null) ? nvcMap["pay_key"] : string.Empty; }
        }

       
        public Encoding IpnEncoding = Encoding.GetEncoding("windows-1252");
     
    }
}