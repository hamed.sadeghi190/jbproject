﻿namespace Jumbula.Core.Model.Email
{
    public class AttachmentModel
    {
        public string FileName { get; set; }

        public float FileSize { get; set; }

        public string FileUrl { get; set; }
    }
}
