﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('editContactController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', 'jbRandom', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService, jbRandom) {
        window.app.globalObjects.pageClass = 'catalog-create-view';
        $scope.MC.openBlade({ url: "Dashboard/Blade/Contact", order: 0, menu: "CONTACT" });
        window.app.globalObjects.initializeView();
        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        $http.get('Dashboard/Contact/GetEdit', { params: { id: $stateParams.contactId } }).success(function (data, status, headers, config) {
            $scope.Model = data;
        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.submit = function () {

            $http.post('Dashboard/Contact/SaveEdit', { model: $scope.Model })
                    .success(function (data, status, headers, config) {

                        if (data.Status) {
                            jbToast.success("Contact edited successfully.");
                            $state.go('Contacts');
                        }
                        else {
                            $scope.MC.handleErrors($scope, data);
                        }

                    });
        }

    }]);
})();

