﻿using Jb.Framework.Common;
using Jumbula.Core.Business;
using System;
using System.Linq;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class VerificationBusiness :  IVerificationBusiness
    {
        private readonly IRepository<UserVerification> _userVerificationRepository;
        public VerificationBusiness(IRepository<UserVerification> userVerificationRepository)
        {
            _userVerificationRepository = userVerificationRepository;
        }

        public UserVerification Get(string phone, string code)
        {
            var utcNow = DateTime.UtcNow.AddHours(-48);

            if (_userVerificationRepository.GetList().Any(c => c.PhoneNumber == phone &&  c.CreatedDate>utcNow))
            {
                var verification= _userVerificationRepository.GetList().Where(c => c.PhoneNumber == phone ).OrderByDescending(c => c.Id).First();
                if(verification.Code.Equals(code))
                {
                    return verification;
                }
            }
            return null;
              
        }

        public bool IsTokenValid(string token)
        {
            return _userVerificationRepository.GetList().Any(c => c.Token == token);
        }

        public UserVerification GetByToken(string token)
        {
            if(IsTokenValid(token))
            {
                return _userVerificationRepository.GetList().SingleOrDefault(c => c.Token == token);
            }

            return null;
        }

        public OperationStatus Create(UserVerification userVerification)
        {
            return _userVerificationRepository.Create(userVerification);
        }

        public OperationStatus Update(UserVerification userVerification)
        {
            return _userVerificationRepository.Update(userVerification);
        }
    }
}
