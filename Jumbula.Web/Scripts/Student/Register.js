﻿var steps = {
    registrationOptions: {
        submitText: "Continue",
        //submitHint: singleTuitionMode ? "Please select your tuition option and click on Continue" : "Please select your tuition options and click on Continue.",
        // submitHint: "Please select your tuition options and click on Continue.",
        nextStep: hasMultipleProfiles && !hasISTS ? "selectProfile" : "registrationForm"
    },
    selectProfile: {
        submitText: "Continue",
        // submitHint: "Who you are registering for? Please select their name.",
        nextStep: "registrationForm"
    },
    registrationForm: {
        submitText: "Continue",
        // submitHint: "Please fill out the form and click on Continue.",
        nextStep: selectNextStep()
    },
    registrationWaivers: {
        submitText: "Continue",
        // submitHint: "Please check , add charges and then press \"Continue\"",
        nextStep: !isDropIn && !isPunchCard && hasPaymentplans ? "selectPaymentPlan" : "submit"
    },
    selectPaymentPlan: {
        submitText: "Continue",
        //submitHint: "How would you like to pay? Select a payment plan then press \"Continue\"",
        nextStep: "submit"
    }
};

var errorKeys = [];
var selectedChessSchedule;
var selectedChessSection;

$(document).ready(function () {

    if (clubDomain == 'bsj' && programType == 'Camp' && programName.substring(0, 'Summer Sessions'.length) == 'Summer Sessions') {
        checkMinSchedule = true;
        minSchedule = 4;
    }
    if (serverMessage != null && serverMessage != "") {
        toastr.warning(serverMessage, null, { timeOut: 20000 });
    }
    $(".chess_schedule_radio_changed").each(function (index, item) {

        if ($(item).is(":checked")) {
            var radioId = $(item).attr("id");

            startDate = $("#" + radioId + "_StartDate").val();
            endDate = $("#" + radioId + "_EndDate").val();

            $("#chess_schedule_startdate").val(startDate);
            $("#chess_schedule_enddate").val(endDate);

        }
    });
    if ($('input[data-clientid="DoB"]').length && $('input[data-clientid="DoB"]').is(":visible")) {
        $('input[data-clientid="DoB"]').mask('99/99/9999', { placeholder: "MM/DD/YYYY" });
        $('input[data-clientid="DoB"]').blur(function () {

            if ($("#register-form #adult-confirmation").length > 0) {
            }
        });
    }

    $(".select-profile").click(function () {
        var selectedProfile = $(this).attr("data-profileId");
        if (selectedProfile >= 0) {
            $("#register-form #ProfileId").val(selectedProfile);

            $("#register-form").submit();
        }
    });

    var paymentPlanId = $("#register-form #PaymentPlanId").val();
    if (programType == 'Subscription') {
        paymentPlanId = 1;
    }

    $('[id^="paymentPlan-msg-"]').hide();
    if (paymentPlanId != null) {
        $('#paymentPlan-' + paymentPlanId).prop("checked", true);
    }
    $(".select-paymentplan").click(function () {
        var selectedpayment = $(this).attr("data-paymentplanId");
        $('[id^="paymentPlan-msg-"]').hide();
        $('#paymentPlan-msg-' + selectedpayment).show();

        if (selectedpayment > 0) {
            $("#register-form #PaymentPlanId").val(selectedpayment);
            $('#paymentPlan-0').prop("checked", false);
        } else {
            $("#register-form #PaymentPlanId").val(0);
            $('.select-paymentplan').not(this).prop('checked', false);
        }
    });
    $(".chess_schedule_radio_changed").change(function () {

        if ($(this).is(":checked")) {
            var radioId = $(this).attr("id");

            selectedChessSchedule = $(this).val();
            $("#selectedChessSchedule").val(selectedChessSchedule);

            startDate = $("#" + radioId + "_StartDate").val();
            endDate = $("#" + radioId + "_EndDate").val();

            $("#chess_schedule_startdate").val(startDate);
            $("#chess_schedule_enddate").val(endDate);

        }
    });

    $(".chess_section_radio_changed").change(function () {

        if ($(this).is(":checked")) {

            selectedChessSection = $(this).val();
            $("#selectedChessSection").val(selectedChessSection);
        }
    });

    $("#changeSelectProfile").click(changeSelectedProfile);
    $("#registerNewProfile").click(prepareFormForAddNewMember);

    if (charges != null && charges.length > 0) {
        populateTotalAmount(charges);
        prepareCheckoutBox(charges);
        $("#ChargeDiscountsText").val(JSON.stringify(charges));
        updateSchedules();
        for (i = 0; i < charges.length; i++) {
            if (programType == 'Calendar') {
                selectDay(charges[i]);
            }
            else if (charges[i].Category == 'EntryFee') {
                var tution = '#tuition-' + charges[i].ScheduleId + '-' + charges[i].ChargeId
                activeTuitionChange($(tution + " .tuition-select input").prop('checked', true), true);
                var tutions = tution + ' .charge-bar';
                for (j = 0; j < charges.length; j++) {
                    if (charges[j].Category != 'EntryFee' && charges[j].ParentChargeId == charges[i].ChargeId && !charges[j].IsMandatory)
                        $(tutions).append('<li class="last-item"><span class="title">' + charges[j].Name + '(' + symbol + charges[j].Amount + ')' + '</span><span onclick="removeCharge(this)" data-chargeId="'
                            + charges[j].ChargeId + '" data-category="' + charges[j].Category + '" data-scheduleid="' + charges[j].ScheduleId +
                            '" data-name="' + charges[j].Name + '"  data-mandatory="' + charges[j].IsMandatory + '" class="remove jbi-remove"></span></li>');
                }
            }
        }
    } else
        $(".ChargeDiscounts").attr('disabled', true);

    if ((hasISTS && isPostBack) || (!hasISTS && (isPostBack || isSelectProfile))) {
        {

            step("registrationForm");
            if (programType == 'Calendar')
                loadCalendar();
        }

        participantInfo.FirstName = $("[data-clientid='ParticipantSection'] [data-clientid='FirstName']").val().trim();
        participantInfo.LastName = $("[data-clientid='ParticipantSection'] [data-clientid='LastName']").val().trim();
        if ($("[data-clientid='ParticipantSection'] [data-clientid='Gender']").val()) {
            participantInfo.Gender = $("[data-clientid='ParticipantSection'] [data-clientid='Gender']").val().trim();
        }
    } else {
        step("registrationOptions");
    }

    var targetPageStep = $("#targetPageStep").val();
    if (targetPageStep != null && targetPageStep != '') {
        charges.length = 0;
        //$("#DesiredStartDate2").val() = null;

        step(targetPageStep);
    }

    setCurrentTuitionByMouseEnter();

    $("#mainHeader .navbar-nav>li a:not(.dropdown-toggle)").click(function (e) {
        if (!confirm("Are you sure? If you leave this page, your cart will be lost.")) {
            e.preventDefault();
        }
    });

    $(window).resize(initSize).scroll(initSize);
    initSize();



    var AccordionTitle = $('.program-schedule-accordion >div> h3'),
        AccordionContent = $('.program-schedule-accordion>div > .accordion-content');
    console.log($(".program-schedule-accordion>div").size());
    if ($(".program-schedule-accordion>div").size() == 1) {
        AccordionContent.show();
    }
    else {
        AccordionContent.hide();
    }
    AccordionTitle.on('click', function () {
        AccordionContent.not($(this).closest('div').find('.accordion-content')).slideUp(400);
        AccordionTitle.not(this).removeClass("ui-accordion-header-active");
        $(this).closest('div').find('.accordion-content').slideToggle(400);
        $(this).toggleClass("ui-accordion-header-active");
    });
    $(".program-schedule-accordion >div> h3>a.preven-parent").click(function (e) {
        e.stopPropagation();
    });
});

var nextStep = function () {
    step(steps[currentStep].nextStep);
}

var prepareFormForAddNewMember = function () {
    if (!changedProfileConfirmed) {
        $('div[data-clientid="' + participantSectionName + '"]').find('input.form-control').val('');
    }
    $("#register-form #IsSelectProfile").val(false);
    $("#register-form #ProfileId").val(null);
    $("#attendeeName").html("a new participant");
    resetValidation($("#register-form"));
    registerSomeoneElse = true;
    step("registrationForm");
}

var changeSelectedProfile = function () {
    isSelectProfile = false;
    step("selectProfile");
}

var initSize = function () {
    $("#contentContainer, #stepsContainer").css("min-height", 0).css("min-height", $(document).outerHeight() - $("#mainHeader > .navbar").outerHeight());
    $("#registrationOptions .schedule .charges ul li:visible").each(function () {
        $(this).find(".title").outerWidth($(this).outerWidth() - $(this).find(".price").outerWidth() - $(this).find(".plus").outerWidth());
    });

};


if (is.desktop() && $(window).outerHeight() > 570 && is.not.safari()) {


    $(window).scroll(function () {

        if ($("#registrationOptions.active").height() > 700 || $("#selectProfile.active").height() > 700 || $("#registrationForm.active").height() > 700 || $("#selectPaymentPlan.active").height() > 700 || $("#registrationWaivers.active").height() > 700) {
            $('#stepsContainer > div').affix({
                offset: {
                    top: 0,
                    bottom: 0
                }
            });
            $('#registration #sideBar').affix({
                offset: {
                    top: 180,
                    bottom: 0
                }
            });

        }
    });
}

if (is.safari()) {
    var safariInit = function () {
        initSize();
        $("#contentContainer, #stepsContainer").css("float", "left");
        $("#continueContainer").css("left", ($(window).outerWidth() >= 1200) ? $("#stepsContainer").outerWidth() : 0);
        $("#stepsContainer > div").css({ "float": "left", "width": "100%" });
    };
    $(window).on("resize", safariInit).on("scroll", safariInit);
    safariInit();
}

var tuitionStateSet = function ($this) {
    
    var tuitionCheck = $('#registrationOptions .schedule .tuitions .check'),
        activeScheduleCheck = $('#registrationOptions .schedule.active .tuitions .check'),
        schedule = $('#registrationOptions .schedule'),
            programScheduleAccordion = $('.program-schedule-accordion.registration-ac');


    if ($this.prop("checked") == true) {
        $this.parents(".tuition").addClass("active");

        if (schedule.find('.content').hasClass('is-camp')) {
            activeScheduleCheck.removeClass('jbi-checked-box').addClass('jbi-check-box');
            $this.parents(".tuition .check").removeClass('jbi-check-box').addClass('jbi-checked-box');
        } else {
            tuitionCheck.removeClass('jbi-checked-box').addClass('jbi-check-box');
            $this.parents(".tuition .check").removeClass('jbi-check-box').addClass('jbi-checked-box')
        }
    } else {
        $this.parents(".tuition").removeClass("active");
        $this.parents(".tuition .check").removeClass('jbi-checked-box').addClass('jbi-check-box');
    }
    programScheduleAccordion.find(".tuition.active .jbi-check-box").addClass('jbi-checked-box').removeClass('jbi-check-box');
}

var setTuitionAsCurrent = function (tuition, schedule) {
    $("#registrationOptions .schedule .tuition.current").removeClass("current");
    tuition.addClass("current");
    schedule.find(".charges .tution-title").text(tuition.find("label .title").text());
    schedule.find(".charges ul li > .plus").attr("data-parent-charge", tuition.find(".check input").data("chargeid"));
}

var setFirstActiveTuitionAsCurrent = function (lastCurrent, schedule) {
    if (schedule.find(".tuitions .tuition.active").size() == 0) {
        schedule.find(".charges").slideUp();
    } else {
        setTuitionAsCurrent(lastCurrent, schedule);
    }
}

var setCurrentTuitionByMouseEnter = function () {
    $("#registrationOptions .schedule .tuition")
        .unbind("mouseenter.setAsCurrent")
        .filter(".active")
        .bind("mouseenter.setAsCurrent", function () {
            var tuition = $(this);
            var schedule = tuition.parents(".schedule");
            setTuitionAsCurrent(tuition, schedule);
        });
}

var scheduleStateChange = function () {
    var allSchedules = $("#registrationOptions .schedule"),
        firstParent = $(this).parent(".schedule");
    if (!firstParent.hasClass("active")) {
        allSchedules.removeClass("active").find(".content").slideUp();
        $(this).next().slideDown().parents(".schedule").addClass("active");
    } else {
        $(this).parent(".schedule").removeClass("active").find(".content").slideUp();
    }
}

var step = function (targetStep) {

    var forwardStep = targetStep != "submit" ? steps[targetStep].nextStep : "submit",
        isSteppingForward = true,
        shouldValidate = true;
    currentStep = $("#contentContainer > div > .step.active").attr("id");
    if (targetStep != "submit") {
        var forwardStep = steps[targetStep].nextStep;
        while (forwardStep != "submit") {
            if (forwardStep == currentStep) {
                shouldValidate = false;
                break;
            }
            forwardStep = steps[forwardStep].nextStep;
        }
    }

    if (targetStep != currentStep && shouldValidate) {
        switch (currentStep) {
            case "registrationOptions":
                {
                    if (charges.length == 0) {

                        var message = 'Please select a tution option.';

                        if ($("#SubscriptionMode").val() == 'DropIn') {

                            message = 'Please select a session for the drop-in.';
                        }

                        toastr.error(message);
                        return;
                    }
                    else if (checkMinSchedule && schedules.length < minSchedule) {
                        toastr.error("Please select at least four schedules.");
                        return;
                    }

                    if (programType == 'ChessTournament') {

                        var hasAnySection = false;
                        $(".tourny-reg-section .chess-sections input").each(function (index) {
                            if ($(this).is(':checked')) {
                                hasAnySection = true;
                            }
                        });

                        if (!hasAnySection) {
                            toastr.error("Please select at least one section.");
                            return;
                        }

                        var hasAnySchedule = false;
                        $(".tourny-reg-section .chess-schedules input").each(function (index) {
                            if ($(this).is(':checked')) {
                                hasAnySchedule = true;
                            }
                        });

                        if (!hasAnySchedule) {
                            toastr.error("Please select at least one schedule.");
                            return;
                        }
                    }

                    if (programType == 'Subscription' && currentStep == 'registrationOptions' && (targetStep == 'selectProfile' || (targetStep == 'registrationForm' && !hasMultipleProfiles))) {

                        checkSubscriptionClassValidation(targetStep, registerSomeoneElse, programType);
                        return;
                    }
                    if (programType == 'BeforeAfterCare' && currentStep == 'registrationOptions' && (targetStep == 'selectProfile' || (targetStep == 'registrationForm' && !hasMultipleProfiles))) {

                        checkBeforeAfterCareValidation(targetStep, registerSomeoneElse, programType);
                        return;
                    }
                }

                break;
            case "selectPaymentPlan":
                if ($("#register-form #ProfileId").val() == null) {
                    toastr.error("Please select someone to register for.");
                    return;
                }

                break;
            case "selectProfile":
                {
                    if (!(isSelectProfile || hasISTS || $("#register-form #ProfileId").val() > 0 || registerSomeoneElse)) {
                        toastr.error("Please select a profile or register someone else.");
                        return;
                    }
                }
                break;
            case "registrationForm":

                var registerForm = $("#register-form");

                var isValid = registerForm.valid();

                if (!isValid) {
                    scrollToNotValidSection();
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: '/Register/CheckFormRestrictions',
                        data: registerForm.serialize(), // serializes the form's elements.
                        beforeSend: function () {
                            try {
                                // remove previous errors
                                for (var i = 0; i < errorKeys.length; i++) {
                                    registerForm.find("[name='" + errorKeys[i] + "']")
                                        .removeClass('input-validation-error');
                                    registerForm.find("[data-valmsg-for='" + errorKeys[i] + "']")
                                        .removeClass('field-validation-error');
                                    registerForm.find("[data-valmsg-for='" + errorKeys[i] + "']").find('span').html('');
                                }
                                errorKeys = [];

                            } catch (ex) {
                            }

                            blockUI();
                        },
                        complete: function () {
                            unblockUI();
                        },
                        success: function (data) {
                            if (data.HasError) {

                                try {
                                    errorKeys = data.ErrorKeys;
                                    validator = registerForm.validate();
                                    validator.showErrors(data.Errors);
                                    scrollToNotValidSection();

                                    return;
                                } catch (ex) {
                                    data.HasError = false;
                                }

                            }

                            if (!data.HasError) {

                                if ($("#register-form #adult-confirmation").length > 0 &&
                                    $("#adult-confirmation-container").is(":visible") &&
                                    !$("#adult-confirmation").is(":checked")) {
                                    $("#register-form").validate().showErrors({
                                        "adult-confirmation": "Please select adult confirmation."
                                    });
                                    toastr.error("Please correct all the errors.");
                                    return;
                                } else {
                                    if (!registerForm.valid()) {
                                        toastr.error("Please correct all the errors.");
                                        return;
                                    }
                                }

                                var participantInfoHasChanged = (participantInfo.FirstName !=
                                    $("[data-clientid='ParticipantSection'] [data-clientid='FirstName']").val()
                                    .trim() ||
                                    participantInfo.LastName !=
                                    $("[data-clientid='ParticipantSection'] [data-clientid='LastName']").val().trim());
                                if ((!hasISTS) &&
                                    (!changedProfileConfirmed) &&
                                    (isSelectProfile && participantInfoHasChanged)) {
                                    //toastr.warning("You edited " + participantInfo.FirstName + "'s profile, is it right? If want to save the info as a new profile please click 'No'<br><br><button class='btn btn-toastr' onclick='changedProfileConfirmed = true; nextStep();'>Yes, please save new changes</button><br><br><button class='btn btn-toastr' onclick='changedProfileConfirmed = true; prepareFormForAddNewMember(); nextStep();'>No, save as a new profile</button> ");
                                    //toastr.warning("You edited " + participantInfo.FirstName + "'s profile, is it right? <br><br><button class='btn btn-toastr' onclick='changedProfileConfirmed = true; nextStep();'>Yes, please save new changes</button>");
                                    //return;
                                }

                                finalizeStep(targetStep, registerSomeoneElse, programType);
                            }
                        }
                    });
                }
                break;
            case "registrationWaivers":
                var errorCount = 0;

                if (hasWaivers) {

                    $('.waiver-confirmation').each(function () {

                        if ($(this).data('is-required') == "True" && $(this).is(":checked") == false) {

                            errorCount++;
                            toastr.error("You need to agree or sign the waivers in order to continue.");
                            return;
                        }

                    });

                    $(".signiture-confirmation").each(function (e) {

                        if ($(this).data('is-required') == "True" && (!$(this).val() || $(this).val() == "image/jsignature;base30,")) {
                            toastr.error("You need to agree or sign the waivers in order to continue.");
                            errorCount++;
                            return;
                        }
                    })

                    if (errorCount != 0) {
                        return;
                    }


                }
                break;
        }


    }

    if (currentStep != 'registrationForm' || !shouldValidate) {
        finalizeStep(targetStep, registerSomeoneElse, programType);
    }
}

var scrollToNotValidSection = function () {
    $('html, body').animate({
        scrollTop: $(".input-validation-error").closest('.JbSection').offset().top
    }, 2000);
}

var finalizeStep = function (targetStep, registerSomeoneElse, programType) {


    if (targetStep != "submit") {
        registerSomeoneElse = false;
        $("[data-done-when],[data-active-when]").each(function () {
            $(this).removeClass("in-progress");
            if (is.existy($(this).data("done-when")) && is.include($(this).data("done-when"), targetStep)) {
                $(this).addClass("done");
            }
            if (is.existy($(this).data("active-when")) && is.include($(this).data("active-when"), targetStep)) {
                $(this).removeClass("done").addClass("in-progress");
            }
        });
        $("#continueContainer #continue").text(steps[targetStep].submitText).data("target", steps[targetStep].nextStep);
        $("#continueContainer #note").text(steps[targetStep].submitHint);
        $("#contentContainer > div > .step").removeClass("active");
        $("#" + targetStep).addClass("active");

        if (targetStep == "selectProfile") {
            $("#register-form #IsSelectProfile").val(false);
            $("#register-form #ProfileId").val(null);
        }
        else if (programType == 'Calendar' && targetStep == "registrationOptions") {
            loadCalendar();
        }

        $("body,html").stop().animate({ scrollTop: 0 }, 300)
    } else {
        $('#register-form').submit();
    }
}

var checkSubscriptionClassValidation = function (targetStep, registerSomeoneElse, programType) {

    var subscriptionMode = 'Normal';

    if (isDropIn) {
        subscriptionMode = 'DropIn';
    }


    if (subscriptionMode == 'Normal') {
        var days = [];
        var selectedChargeId = 0;

        $('.day-item').each(function (e) {

            var checked = this.checked;
            if (checked) {
                days.push($(this).attr('data-day'));
            }
        });

        populateDayHiddens(days);

        $('.tuition-select').each(function (e) {

            if ($(this).hasClass('jbi-checked-box')) {
                selectedChargeId = $(this).find('input[name=chargeEntryFee]').val();
            }
        })

        $.ajax({
            method: "POST",
            dataType: 'json',
            url: "/Register/CheckSubscriptionValidations",
            data: {
                desiredStartDate: $('#desiredStartDate').val(),
                programId: $('#ProgramId').val(),
                days: days,
                selectedChargeId: selectedChargeId
            },
            success: function (data) {

                if (data.HasError) {

                    for (var i = 0; i < data.Errors.length; i++) {
                        toastr.error(data.Errors[i]);
                    }

                    return;
                }
                else {
                    finalizeStep(targetStep, registerSomeoneElse, programType);
                }
            },
            error: function () {
                return;
            },
            complete: function () {

                return;
            }
        })
    }
    else {
        finalizeStep(targetStep, registerSomeoneElse, programType);
    }
}


var checkBeforeAfterCareValidation = function (targetStep, registerSomeoneElse, programType) {

    var programMode = 'Normal';

    if (isDropIn) {
        programMode = 'DropIn';
    }
    else if (isPunchCard) {
        programMode = 'PunchCard'
    }


    if (programMode == 'Normal') {
        var days = [];
        var selectedChargeId = 0;


        $('.day-item').each(function (e) {

            var checked = this.checked;
            if (checked) {
                days.push($(this).attr('data-day'));
            }
        });

        populateDayHiddens(days);

        $('.tuition-select').each(function (e) {

            if ($(this).hasClass('jbi-checked-box')) {
                selectedChargeId = $(this).find('input[name=chargeEntryFee]').val();
            }
        })

        $.ajax({
            method: "POST",
            dataType: 'json',
            url: "/Register/CheckBeforeAfterCareValidation",
            data: {
                desiredStartDate: $('#desiredStartDate').val(),
                programId: $('#ProgramId').val(),
                days: days,
                selectedChargeId: selectedChargeId
            },
            success: function (data) {

                if (data.HasError) {

                    for (var i = 0; i < data.Errors.length; i++) {
                        toastr.error(data.Errors[i]);
                    }

                    return;
                }
                else {
                    finalizeStep(targetStep, registerSomeoneElse, programType);
                }
            },
            error: function () {
                return;
            },
            complete: function () {
                return;
            }
        })
    }
    else {
        finalizeStep(targetStep, registerSomeoneElse, programType);
    }
}

var populateDayHiddens = function (days) {
    var hiddens = '';

    if (days) {
        for (var i = 0; i < days.length; i++) {

            var hidden = '<input name ="Days" type = "hidden" value="' + days[i] + '" />';

            hiddens += hidden + ' ';
        }
    }

    $('#dayOfWeeks').html(hiddens);
}

var activeTuitionChange = function (sender, editMode) {
    var $this = $(sender);
    var schedule = $this.parents(".schedule");
    var tuition = $this.parents(".tuition");
    var selected = $this.prop('checked');
    var lastCurrent = schedule.find(".tuitions .tuition.current");

    if (singleTuitionMode && selected) {
        $(schedule).find(".tuitions .tuition").removeClass("active");
        $(schedule).find(".tuitions input.entryFeeCheckBox").prop('checked', false).each(function () {
            if (!$this.is($(this))) {
                removeCharge($(this));
            }
        });
        $this.prop('checked', true);

        var selectedChargeId = $this.prop('checked', true).attr('data-chargeid');
        var schedualeId = $this.prop('checked', true).attr('data-scheduleid');
        var parentCategory = $this.prop('checked', true).attr('data-parent-charge');
        var programNumberDayOfWeek = $this.prop('checked', true).attr('data-programDays');

        if (parentCategory != (-10) && programType == 'BeforeAfterCare') {

            $.ajax({
                method: "POST",
                dataType: 'json',
                url: "/Register/HaveLargestNumberOfDay",
                data: {
                    schedualeId: schedualeId,
                    selectedChargeId: selectedChargeId
                },
                success: function (data) {

                    var days = $(".day-item");
                    if (data.IsLargest && data.SelectedChargeNumberDays == programNumberDayOfWeek) {
                        for (var i = 0; i < days.length; i++) {
                            if (!days[i].checked) {
                                days[i].checked = true;
                                checkDayTuition(i);
                            } else {
                                continue;
                            }
                        }
                    }
                    else {
                        for (var i = 0; i < days.length; i++) {                           
                                days[i].checked = false;
                        }

                        clearDays();
                    }
                },
                error: function () {
                },
                complete: function () {
                }
            })
        }
    }

    if (!editMode && selected)
        addCharge($this);
    else if (!editMode)
        removeCharge($this);

    tuitionStateSet($this);
    setTuitionAsCurrent(tuition, schedule);
    if (schedule.find(".tuitions .tuition.current.active").size() == 0) {
        setFirstActiveTuitionAsCurrent($this.parents(".tuition").html() === lastCurrent.html() ? schedule.find(".tuitions .tuition.active").first() : lastCurrent, schedule);
    } else {
        schedule.find(".charges").slideDown();
        setTimeout(function () { initSize(); });
    }
    setCurrentTuitionByMouseEnter();

}

$("#registrationOptions .schedule .tuitions .check input").change(function () {

    var subscriptionMode = 'Normal';

    var needUncheckPrevious = false;
    if (programType == 'BeforeAfterCare') {
        needUncheckPrevious = isSwitchBeforeAfterChargeNeedUncheckPrevious($(this));
    }

    var needUncheckPreviousTuition = false;
    if (programType == 'Camp') {
        needUncheckPreviousTuition = isNeedSelectOneMode($(this));
    }

    var selectedInputId = $(this).attr('id');
    var calendarId = $(this).attr('data-scheduleid');
    var programHasDropIn = false;

    if (programType == 'Camp' && selectedInputId == "programIsDropIn" + calendarId) {
        programHasDropIn = true;
    }

    if (needUncheckPrevious) {

        $("#shareModal").find(".modal-header").css('display', 'block');
        $(".modal-title").html('Registering for multiple programs');
        $(".modal-body").html('If you need to register for two different programs, please complete the registration for the first program and then add the second program from the cart.');

        $('#shareModal').modal();

    } else if (needUncheckPreviousTuition) {
        $("#shareModal").find(".modal-header").css('display', 'block');
        $(".modal-title").html('Registering for drop-ins');
        $(".modal-body").html('It is not possible to register for full week schedules and drop-in days simultaneously. Please make your selection and continue to the cart, you will then be able to come back to this step to select drop-in days.');

        $('#shareModal').modal();
    }
    else {
        if ($(this).hasClass('punch-card') && $(this).is(':checked')) {
            subscriptionMode = 'PunchCard';
        }

        if (hasDropIn() || programHasDropIn) {
            if (!programHasDropIn) {
                resetDropIn();
            }
            else {
                resetProgramDropIn(calendarId);
            }
            if ($(this).hasClass('drop-in') && $(this).is(':checked')) {
                subscriptionMode = 'DropIn';
            }
        }
        $("#SubscriptionMode").val(subscriptionMode);

        if (subscriptionMode == 'DropIn') {
            isDropIn = true;
            if (programType == 'Camp') {

                $('#calender' + calendarId).show();
            } else {
                $('#classDays').hide();
                $('#calendar').show();
            }
        }
        else if (subscriptionMode == 'PunchCard') {
            isPunchCard = true;
            $('#classDays').hide();
            $('#calendar').hide();
            $('#calender' + calendarId).hide();
        }
        else {
            isPunchCard = false;
            isDropIn = false;
            $('#classDays').show();
            $('#calendar').hide();
            $('#calender' + calendarId).hide();

            if (programType == 'Camp') {
                resetProgramDropIn(calendarId);
            }
        }

        activeTuitionChange(this, false);
    }

}).each(function () {
    tuitionStateSet($(this));
});

var isNeedSelectOneMode = function (item) {
    var scheduleId = item.attr('data-scheduleid');
    var scheduleMode = item.attr('data-scheduleMode');
    var isDropIn = item.hasClass('drop-in');
    var parentChargeId = item.attr('data-parent-charge')

    var previousChoice = getSelectedTuition();

    if (previousChoice == 'undefined' || previousChoice == null) {
        return false;
    }

    if (previousChoice.ScheduleId != scheduleId && previousChoice.ParentChargeId != -10 && parentChargeId == -10) {
        return true;
    }

    if (previousChoice.ScheduleId != scheduleId && previousChoice.ParentChargeId == -10 && parentChargeId != -10) {
        return true;
    }

    if (previousChoice.ScheduleId == scheduleId) {

        for (var i = 0; i < charges.length; i++) {
            if (charges[i].ScheduleId != scheduleId && charges[i].ParentChargeId <= 0 && parentChargeId > 0) {
                return true;
            }
        }

        if (charges.length > 0) {
            var isDifferenceSchedule = false;
            for (var j = 0; j < charges.length; j++) {
                if (charges[j].ScheduleId != scheduleId) {
                    isDifferenceSchedule = true;
                }
            }

            if (isDifferenceSchedule && previousChoice.ParentChargeId > 0 && parentChargeId == -10) {
                return true;
            }
        }

        if (addDropInCharges.length > 1) {
            if (previousChoice.ParentChargeId == -10 && parentChargeId > 0) {
                return true;
            }
        }
    }

}

var isSwitchBeforeAfterChargeNeedUncheckPrevious = function (item) {
    var scheduleId = item.attr('data-scheduleid');
    var scheduleMode = item.attr('data-scheduleMode');
    var isDropIn = item.hasClass('drop-in');
    var isPunchCard = item.hasClass('punch-card');

    var previousChoice = getSelectedTuition();

    if (previousChoice == 'undefined' || previousChoice == null) {
        return false;
    }

    if ((previousChoice.IsDropIn && isDropIn) || (previousChoice.IsPunchCard && isPunchCard)) {
        return false;
    }

    if (previousChoice != null && (isDropIn || isPunchCard)) {
        return true;
    }

    if (previousChoice.IsDropIn || previousChoice.IsPunchCard) {
        return true;
    }

    if (scheduleMode == 'Both') {
        return false;
    }


    if (previousChoice.ScheduleId != scheduleId) {
        return true;
    }

    return false;
}

var getSelectedTuition = function () {

    var charges = [];
    $('.entryFeeCheckBox').each(function () {
        if ($(this).parent().hasClass('jbi-checked-box')) {
            var charge = toCharge($(this));

            if ($(this).hasClass('punch-card')) {
                charge.IsPunchCard = true;
            }

            charges.push(charge);
        }
    });

    $('.drop-in').each(function () {
        if ($(this).parent().hasClass('jbi-checked-box')) {
            var dropIn = toCharge($(this));
            dropIn.IsDropIn = true;
            charges.push(dropIn);
        }
    });

    var result = null;

    if (charges.length > 0) {
        result = charges[0];
    }

    return result;
}

var hasDropIn = function () {

    var result = false;

    if ($('#EnableDropIn').val() == 'True') {
        result = true;
    }

    return result;
}

var resetDropIn = function () {


    charges = [];
    calendarItems = [];

    $("input.calendar-day").each(function () {
        $(this).prop("checked", false);
    });

}

var resetProgramDropIn = function (scheduleId) {

    for (var i = 0; i < charges.length; i++) {

        if (charges[i].ScheduleId == scheduleId) {
            charges.splice(i, 1);
            calendarItems.splice(i, 1);
            $("input.calendar-day").each(function () {
                if ($(this)[0].dataset.scheduleid == scheduleId) {
                    $(this)[0].checked = false;
                }
            });
        }
    }

    for (var i = 0; i < charges.length; i++) {
        if (charges[i].ParentChargeId == 0 && charges[i].ScheduleId == scheduleId) {
            charges.splice(i, 1);
            calendarItems.splice(i, 1);
        }
    }

}

$("#registrationOptions .schedule").each(function () {
    var schedule = $(this);
    schedule.find(".content").css("display", "block");
    var chargesHeight = schedule.find(".charges").outerHeight();
    var tuitionsHeight = schedule.find(".tuitions").outerHeight();
    if (chargesHeight < tuitionsHeight) {
        schedule.find(".charges").css("height", tuitionsHeight);
    }
    if (schedule.find(".content").hasClass('is-camp')) {
        schedule.find(".content").css("display", "none");
        if ($('#contentContainer .schedule .content').size() == 1) {
            schedule.addClass('active');
            schedule.find(".content").css("display", "block");
        }
    }
});

$("#registrationOptions .schedule .charges ul li .plus").click(function () {
    addCharge($(this));
});


$("#registrationOptions .schedule .dates").click(scheduleStateChange);

$(".stepper[data-target]").click(function (e) {
    e.preventDefault();

    var targetStep = $(this).data("target");
    step(targetStep);
});

$("#registrationOptions .schedule .tuitions .price .edit-price").click(function (e) {
    e.preventDefault();

    $("#tuitionPriceEditModal #priceInput").val($(this).attr("data-priceamount")).data("priceId", $(this).attr("data-priceid"));
    $("#tuitionPriceEditModal").modal({
        show: true
    });
});

function changePrice(the) {

    var priceId = $(the).data("priceId");
    var senderLabelId = "lbl_price_amount_" + priceId;
    var price = $(the).val();
    $("#" + senderLabelId).text(symbol + price);
    $("#registrationOptions .schedule .tuitions .price .edit-price[data-priceid='" + priceId + "']").attr("data-priceamount", price);
    $(".entryFeeCheckBox").each(function (index) {

        if ($(this).attr("data-chargeid") == priceId) {
            $(this).attr("data-amount", price);
            hiddenId = "changedTutionPrice-" + priceId;
            hiddenPriceId = "TutionPrice-" + priceId;
            $("#" + hiddenId).val(price);
            $("#is" + hiddenId).val(true);
            if ($(this).is(':checked')) {
                for (var i = 0; i < charges.length; i++) {
                    if (charges[i].ChargeId == priceId) {
                        charges[i].Amount = price;
                    }
                }
                populateTotalAmount(charges);
                prepareCheckoutBox(charges);
                $("#ChargeDiscountsText").val(JSON.stringify(charges));
                updateSchedules();
            }
        }
    });
}
function updateSchedules() {

    schedules = [];
    if (charges != null) {
        var i = 0;
        var k = 0;
        for (i = 0; i < charges.length; i++) {
            var isNotExist = true;
            if (i != 0) {

                for (j = 0; j < schedules.length; j++) {
                    if (schedules[j] == charges[i].ScheduleId) {
                        isNotExist = false;
                    }

                }
            }
            if (isNotExist) {
                schedules[k] = charges[i].ScheduleId;
                k++;
            }

        }
    }
}
function selectNextStep() {

    if (hasWaivers && !hasISTS) {
        return "registrationWaivers";
    }

    if (hasPaymentplans && !isDropIn && !isPunchCard) {
        return "selectPaymentPlan";
    }
    return "submit";
}



var addDropInCharges = [];

function addCharge(sender) {

    var charge = toCharge(sender);

    $(".extraServicePopup").css("display", "block");

    if (programType == 'Camp' && charge.ParentChargeId == -10) {
        addDropInCharges.push(charge)
        $(".extraServicePopup").css("display", "none");
    }

    var tution = '#tuition-' + charge.ScheduleId + '-' + charge.ParentChargeId + ' .charge-bar';

    if (charge.ChargeId != charge.ParentChargeId) {

        $(tution).append('<li data-mandatory="false" class="last-item"><span class="title ellipsis" title="' + charge.Name + '">'
            + charge.Name + '(' + symbol + charge.Amount + ')' + '</span><span onclick="removeCharge(this)" data-scheduleid="'
            + charge.ScheduleId + '" data-chargeId="' + charge.ChargeId + '" data-category="'
            + charge.Category + '" data-name="' + charge.Name + '"  data-mandatory="false" class="remove jbi-remove"></span></li>');
    }
    else {

        if ($(tution).find("li[data-mandatory='true']").length > 0) {
            $(tution).find("li[data-mandatory='true']").each(function (index, data) {
                charges.push(toCharge($(data).find(".mandatory-charge")));
            });
        }
    }

    if (charge.ParentChargeId != -10) {

        charges.push(charge);
    }
    populateTotalAmount(charges);
    prepareCheckoutBox(charges);

    var jsonCharges = JSON.stringify(charges);

    $("#ChargeDiscountsText").val(jsonCharges);
    updateSchedules();
}


function removeCharge(sender) {

    var parentChargeId = $(sender).attr("data-parent-charge");

    if (parentChargeId == null)
        parentChargeId = $(sender).parents(".tuition").find(".entryFeeCheckBox").attr("data-chargeid");

    var scheduleId = $(sender).attr("data-scheduleid");
    var chargeId = $(sender).attr("data-chargeid");
    var category = $(sender).attr("data-category").toLowerCase();
    var name = $(sender).attr("data-name").toLowerCase();
    var id = $(sender).attr("data-id");

    var isDropIn = sender instanceof jQuery ? sender.hasClass('calendar-day') : false;


    if (programType == "Camp" && parentChargeId == -10) {

        for (var i = 0; i < addDropInCharges.length; i++) {

            if (addDropInCharges[i].ScheduleId == scheduleId) {
                addDropInCharges.splice(i, 1)
            }
        }
    }

    for (var i = 0; i < charges.length; i++) {
        if (charges[i].ScheduleId == scheduleId && charges[i].ChargeId == chargeId && charges[i].ParentChargeId == parentChargeId && charges[i].Name.toLowerCase() == name && charges[i].Id == id) {
            if (isDropIn) {
                charges.splice(i, 1);
            }
            else if (charges[i].Category.toLowerCase() == 'entryfee') {

                for (var j = 0; j < charges.length; j++) {
                    if (charges[j].ScheduleId == scheduleId && charges[j].ParentChargeId == parentChargeId) {
                        charges.splice(j, 1);
                        j = -1;
                    }
                }
                break;
            }
            else {
                charges.splice(i, 1);
                break;
            }
        }
    }
    if (category != 'entryfee')
        $(sender).parent().remove();
    else
        $(sender).parents('.tuition').find('.charge-bar li span[data-mandatory="false"]').parent("li").remove();

    populateTotalAmount(charges);
    prepareCheckoutBox(charges);
    $("#ChargeDiscountsText").val(JSON.stringify(charges));
    updateSchedules();
}

function populateTotalAmount(checkoutBoxItems) {
    totalAmount = 0;
    totalFeeAmount = 0;
    totalDiscount = 0;


    var count = 0;
    var f = false;
    for (var i = 0; i < checkoutBoxItems.length; i++) {

        if (checkoutBoxItems[i].Subcategory.trim().toLowerCase() == 'charge') {

            if (programType == 'BeforeAfterCare' && isDropIn) {


                var itemAmount = parseFloat(checkoutBoxItems[i].Amount);
                if (hasSameDaySession(checkoutBoxItems, checkoutBoxItems[i])) {
                    itemAmount = itemAmount / 2
                }

                totalFeeAmount += itemAmount;
            }
            else {

                totalFeeAmount += parseFloat(checkoutBoxItems[i].Amount);
            }
        }
        if (checkoutBoxItems[i].Subcategory.trim().toLowerCase() == 'discount') {
            totalDiscount += parseFloat(checkoutBoxItems[i].Amount);
        }
    }

    totalAmount = (totalFeeAmount - totalDiscount).toFixed(2);
}

function hasSameDaySession(checkoutBoxItems, lastCheckedChargeItem) {
    var result = false;

    for (var i = 0; i < checkoutBoxItems.length; i++) {

        if (checkoutBoxItems[i].Date == lastCheckedChargeItem.Date && checkoutBoxItems[i].Id != lastCheckedChargeItem.Id) {
            return true;
        }
    }

    return result;
}

function prepareCheckoutBox(checkoutBoxItems) {

    var html = '';
    var schedules = '';
    var oldTotal = Number($("#totalPrice").text().replace(/[^0-9\.]/g, ""));
    // calendar items
    charges = charges.sort(function (a, b) {
        if (a.ScheduleId > b.ScheduleId)
            return 1;
        if (a.ScheduleId < b.ScheduleId)
            return -1;
        if (a.ScheduleId == b.ScheduleId)
            if (a.ChargeId > b.ChargeId)
                return 1;
            else if (a.ChargeId > b.ChargeId)
                return 1;
            else if (a.ChargeId < b.ChargeId)
                return -1;
            else
                return 0;
    });

    var doneCheckoutBoxItems = [];

    for (var i = 0; i < checkoutBoxItems.length; i++) {
        if (checkoutBoxItems[i].Category.trim().toLowerCase() == 'entryfee' && !isDropIn) {
            html += '<li>';
            if (schedules.indexOf('|' + checkoutBoxItems[i].ScheduleId + '|') < 0) {
                //html += programType.toLowerCase() == "beforeaftercare" ? '<em>' + checkoutBoxItems[i].ScheduleTitle + '</em><div class="clearfix">' : '';
                html += programType.toLowerCase() != "class" ? '<em class="section-title">' + checkoutBoxItems[i].ScheduleTitle + '</em><div class="clearfix">' : '';
                schedules += '|' + checkoutBoxItems[i].ScheduleId + '|'; //for group by schedules
            }
            html += '<span class="title ellipsis" title="' + checkoutBoxItems[i].Name + '">' + checkoutBoxItems[i].Name + '</span><span class="price">' + symbol + checkoutBoxItems[i].Amount + '</span><div class="clearfix"></div>';
            html += '<ul class="charges">';
            for (var j = 0; j < checkoutBoxItems.length; j++) {
                if (checkoutBoxItems[j].Category.trim().toLowerCase() != 'entryfee' && checkoutBoxItems[j].ParentChargeId == checkoutBoxItems[i].ChargeId) {
                    html += '<li><span class="title ellipsis" title="' + checkoutBoxItems[j].Name + '">' + checkoutBoxItems[j].Name + '</span><span class="price">' + symbol + checkoutBoxItems[j].Amount + '</span><div class="clearfix"></div></li>';
                }
            }
            html += '</ul></li>';
        } else if (checkoutBoxItems[i].Subcategory.trim() == 'discount') {
            html += '<li class=\"pull-left discount\"><span class=\"pull-left\">' + checkoutBoxItems[i].Name + '</span><span class=\"pull-right\">' + symbol + checkoutBoxItems[i].Amount + '</span></li>';
        }

        //don't show amount in title for before after drop-in
        if (isDropIn) {
            html += '<li>';
            if (hasSameDaySession(doneCheckoutBoxItems, checkoutBoxItems[i]) && programType != 'Camp') {
                html += '<span class="title ellipsis" title="' + checkoutBoxItems[i].Name + '">' + checkoutBoxItems[i].Name + '</span><span class="price">' + '</span><div class="clearfix"></div>';
            }
            else {
                html += '<span class="title ellipsis" title="' + checkoutBoxItems[i].Name + '">' + checkoutBoxItems[i].Name + '</span><span class="price">' + symbol + checkoutBoxItems[i].Amount + '</span><div class="clearfix"></div>';
            }

            doneCheckoutBoxItems.push(checkoutBoxItems[i]);
        }
    }



    if (checkoutBoxItems.length == 0) {
        $("#cheoutboxContainer ul.tuitions").addClass("empty").html("<li></li>");
    } else {
        $("#cheoutboxContainer ul.tuitions").removeClass("empty").html(html);
    }
    $("#totalPrice").text(symbol + oldTotal);

    var counterOptions = {
        useEasing: true, useGrouping: true, separator: ',', decimal: '.', prefix: symbol, suffix: ''
    };

    var scheduleDate = $('#scheduleDate').val();

    if (scheduleDate != '' && scheduleDate != null) {
        $('#cheoutboxContainer').find('.section-title').html(scheduleDate);
    }
    
    var counter = new countUp(
        "totalPrice",
        oldTotal,
        totalAmount,
        2,
        1,
        counterOptions);

    counter.start();
}

function toCharge(sender) {
    var charge = {
    };
    charge.ChargeId = $(sender).attr("data-chargeid");
    charge.ParentChargeId = $(sender).attr("data-parent-charge");
    charge.ScheduleId = $(sender).attr("data-scheduleid");
    charge.ScheduleTitle = $(sender).attr("data-schedule-title");
    charge.Name = $(sender).attr("data-name");
    charge.Description = $(sender).attr("data-description") == null ? "" : $(sender).attr("data-description");
    charge.Amount = $(sender).attr("data-amount");
    charge.Category = $(sender).attr("data-category");
    charge.Subcategory = $(sender).attr("data-subcategory");
    charge.IsMandatory = eval($(sender).attr("data-mandatory") == null ? false : $(sender).attr("data-mandatory").toLowerCase());
    charge.Id = $(sender).attr("data-id");
    charge.Date = $(sender).attr("data-dayDate");
    //charge.Dates = $(sender).attr("data-schedule-date");
    return charge;
}

function resetValidation(form) {
    form.validate().resetForm();
    form.find(".field-validation-error span").remove();
    form.find(".field-validation-error").removeClass("field-validation-error").addClass("field-validation-valid");

    if (programType == 'ChessTournament') {
        $("#selectedChessSchedule").val(selectedChessSchedule);
    }
    if (programType == 'ChessTournament') {
        $("#selectedChessSection").val(selectedChessSection);
    }

}


/*Calendar*/
function loadCalendar() {
    try {
        $("#scheduler").data("kendoScheduler").dataSource.read();
    }
    catch (ex) {
    }
}
function changeCalendarItem(sender) {
    var id = $(sender).data('id');
    var scheduleId = $(sender).data('scheduleid');

    var dayDate = $(sender).data('daydate');
    var count = 0;
    var isSecondSessionInDay = false;
    if ($(sender).prop('checked')) {
        calendarItems.push({ "Id": id, "ScheduleId": scheduleId, "DayDate": dayDate });

        addCharge($(sender));
    }
    else {
        for (i = 0; i < calendarItems.length; i++) {
            if (calendarItems[i].Id == id && calendarItems[i].ScheduleId == scheduleId) {
                calendarItems.splice(i, 1);
                break;
            }
        }

        removeCharge($(sender));
    }

    $('#CalendarSelectedItems').val(JSON.stringify(calendarItems));
};


function schedulerChange(e) {
    $.each(calendarItems, function (i, val) {
        $checkTarget = $('#chkday' + val.Id);
        $checkTarget.attr('checked', 'checked');
    });

    for (var i = 0; i < charges.length; i++) {
        selectDay(charges[i]);
    }
}

function selectDay(charge) {
    for (var i = 0; i < $("input.calendar-day").length; i++) {
        if ($($("input.calendar-day")[i]).data("scheduleid") == charge.ScheduleId
            && $($("input.calendar-day")[i]).data("chargeid") == charge.ChargeId
            && $($("input.calendar-day")[i]).data("name").toLowerCase() == charge.Name.toLowerCase()) {
           // $($("input.calendar-day")[i]).prop("checked", true);
        }
    }
}

function selectDays() {
    $("#ddlSelectSchedule").data("kendoDropDownList")._selectedValue;
    var start = new Date($("#cldStartDate").data("kendoDatePicker").value())
    var end = new Date($("#cldEndDate").data("kendoDatePicker").value())
    var scheduleId = $("#ddlSelectSchedule").data("kendoDropDownList").value();
    var sch = $("#scheduler").data("kendoScheduler");
    for (var i = 0; i < $(sch.items()).find("input").length; i++) {
        var item = $($(sch.items()).find("input")[i]);
        if (item.data("scheduleid") == scheduleId && new Date(item.data("dodate")) >= start && new Date(item.data("dodate")) <= end) {
            if (!item.prop("checked")) {
                item.prop("checked", true);
                changeCalendarItem(item);
            }
        }
    }
}

function unSelectDays() {
    $($($("#scheduler").data("kendoScheduler").items()).find("input")).prop("checked", false);
    charges = charges.splice(charges.length);
    populateTotalAmount(charges);
    prepareCheckoutBox(charges);
}

function changeProgramCalendarItem(sender) {

    var id = $(sender).data('id');
    var scheduleId = $(sender).data('scheduleid');

    var dayDate = $(sender).data('daydate');
    var count = 0;
    var isSecondSessionInDay = false;



    if ($(sender).prop('checked')) {
        calendarItems.push({ "Id": id, "ScheduleId": scheduleId, "DayDate": dayDate });

        addCharge($(sender));
    }
    else {
        for (i = 0; i < calendarItems.length; i++) {
            if (calendarItems[i].Id == id && calendarItems[i].ScheduleId == scheduleId) {
                calendarItems.splice(i, 1);
                addDropInCharges.splice(i, 1)
                break;
            }
        }

        removeCharge($(sender));
    }

    //$('#CalendarSelectedItems').val(JSON.stringify(calendarItems));
};
