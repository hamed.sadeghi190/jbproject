﻿namespace Jumbula.Common.Base
{
    public abstract class BaseViewModel : IBaseViewModel<int>
    {
        public int Id { get; set; }

        public override string ToString()
        {
            return base.ToString().Split('.')[base.ToString().Split('.').Length - 1].Replace("ViewModel", "");
        }
    }

    public abstract class BaseViewModel<TBaseEntity, TPrimaryKey> : IBaseViewModel<TPrimaryKey>
        where TBaseEntity : BaseEntity<TPrimaryKey>
        where TPrimaryKey : struct
    {
        public TPrimaryKey Id { get; set; }
        public override string ToString()
        {
            return base.ToString().Split('.')[base.ToString().Split('.').Length - 1].Replace("ViewModel", "");
        }
        public TModel ToModel<TModel>() where TModel : class, new()
        {
            return Jumbula.Common.Utilities.AutoMapper.Map<TModel>(this);
        }
        public TModel ToModel<TModel>(TModel dbEntity) where TModel : BaseEntity<TPrimaryKey>, new()
        {
            Jumbula.Common.Utilities.AutoMapper.Map(this, dbEntity);
            return dbEntity as TModel;
        }
    }

    public abstract class BaseViewModel<TBaseEntity> : IBaseViewModel<int>
        where TBaseEntity : BaseEntity
    {
        public int Id { get; set; }
        public string PageTitle { get; set; }
        public override string ToString()
        {
            return base.ToString().Split('.')[base.ToString().Split('.').Length - 1].Replace("ViewModel", "");
        }
        public TModel ToModel<TModel>() where TModel : class, new()
        {
            return Jumbula.Common.Utilities.AutoMapper.Map<TModel>(this);
        }
        public TModel ToModel<TModel>(TModel dbEntity) where TModel : BaseEntity, new()
        {
            Jumbula.Common.Utilities.AutoMapper.Map(this, dbEntity);
            return dbEntity as TModel;
        }
    }
}
