﻿using Jb.Framework.Common;
using System;
using System.Linq;
using System.Linq.Expressions;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IWaitListBusiness
    {
        WaitList Get(int Id);
        WaitList Get(Expression<Func<WaitList, bool>> predicate);
        IQueryable<WaitList> GetList();

        IQueryable<WaitList> GetList(long programId);

        IQueryable<WaitList> GetList<TKey>(Expression<Func<WaitList, bool>> predicate, Expression<Func<WaitList, TKey>> orderBy);
        OperationStatus Update(WaitList entity);
        OperationStatus Create(WaitList entity);
        //bool IsAlreadyExistInWaitList(long programId, int userId, string firstName, string lastName);
        OperationStatus Delete(int entityId);
        OperationStatus Delete(WaitList entity);
        OperationStatus DeleteWhere(Expression<Func<WaitList, bool>> filter = null, Func<IQueryable<WaitList>, IOrderedQueryable<WaitList>> orderBy = null, string includeProperties = "");
        IQueryable<WaitList> GetListByUser(long UserId);

        WaitList GetById(int Id);

        bool IsAlreadyExistInWaitListByScheduleId(long scheduleId, int userId, string firstName, string lastName);
    }
}
