﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.WebLogic.ClubsService;
using Jumbula.Web.WebLogic.UserService;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SportsClub.Models;
using SportsClub.Models.Register;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Controllers
{

    public class ProgramController : JbBaseController
    {
        #region fields
        private readonly IProgramBusiness _programBusiness;
        #endregion

        #region Constractors
        public ProgramController(IProgramBusiness programBusiness)
        {
            _programBusiness = programBusiness;
        }
        #endregion


        [HttpGet]
        [AllowAnonymous]
        public virtual ActionResult View(string clubDomain, string seasonDomain, string domain)
        {
            return DisplayProgram(clubDomain, seasonDomain, domain, true);
        }

        [HttpGet]
        [AllowAnonymous]
        public virtual ActionResult Display(string clubDomain, string seasonDomain, string domain)
        {
            return DisplayProgram(clubDomain, seasonDomain, domain, false);
        }

        private ActionResult DisplayProgram(string clubDomain, string seasonDomain, string domain, bool isViewMode)
        {
            var club = Ioc.ClubBusiness.Get(clubDomain);
            var clubSetting = club.Setting;
            var programBusiness = Ioc.ProgramBusiness;
            var seasonBusiness = Ioc.SeasonBusiness;

            Program program = programBusiness.Get(club.Id, seasonDomain, domain);

            if (program == null)
            {
                PageNotFound();
            }
            if (isViewMode == false && (program.SaveType == SaveType.Draft))
            {
                isViewMode = true;
            }

            if (program.SaveType == SaveType.Draft && program.LastCreatedPage == ProgramPageStep.Step1)
            {
                PageNotFound();
            }
            if (program.TypeCategory == ProgramTypeCategory.Calendar)
            {
                foreach (var item in program.ProgramSchedules)
                {
                    item.RegistrationPeriod.RegisterEndDate = item.Sessions.Max(a => a.End);
                }
            }

            var programRegStatus = programBusiness.GetRegStatus(program);

            var programRunningStatus = programBusiness.GetRunningStatus(program);

            var model = program.ToViewModel<ProgramPageViewModel>();

            model.IsTestMode = program.Season.Status == SeasonStatus.Test;

            RoleCategory? currentUserRole = null;

            if (this.IsUserAuthenticated())
            {
                currentUserRole = this.GetCurrentUserRole();

                var allClubStaffs = club.ClubStaffs.Where(s => s.Status == StaffStatus.Active && !s.IsFrozen).ToList();

                if (allClubStaffs.Any())
                {
                    var activUserId = allClubStaffs.Where(s => s.JbUserRole.UserId == this.GetCurrentUserId()).Select(s => s.JbUserRole.User.Id).FirstOrDefault();
                    model.IsStaffActive = activUserId == this.GetCurrentUserId() ? true : false;
                }

            }

            var hideGoogleMapInClassPage = false;
            //string logoUrl = Ioc.ClubBusiness.GetClubLogoURL(program.Club.Domain, program.Club.Logo, program.Club.CategoryId);

            if (club.PartnerId.HasValue)
            {
                var partner = Ioc.ClubBusiness.Get(club.PartnerId.Value);
                if (partner.Domain == "flexacademies")
                {
                    model.ClubDomain = "flexacademies";
                }

                if (partner.Setting?.AppearanceSetting != null)
                {
                    model.ShowProviderNameInClassPage = partner.Setting.AppearanceSetting.ShowProviderNameInClassPage;
                    hideGoogleMapInClassPage = partner.Setting.AppearanceSetting.HideGoogleMapInClassPage;
                }
            }

            model.IsViewMode = isViewMode;
            model.IsDraft = program.SaveType == SaveType.Draft;
            model.ClubName = program.Club.Name;
            model.ClubId = program.ClubId;
            model.SeasonId = program.SeasonId;
            model.CapacityLeft = programBusiness.CapacityLeft(program, program.Season.Status == SeasonStatus.Test);
            model.ProgramRegStatus = programBusiness.GetEmProgramStatus(program, program.Season.Status == SeasonStatus.Test);
            model.Testimonials = program.Testimonials;
            model.HasTestimonials = program.HasTestimonial;
            model.HideProgramDates = clubSetting != null ? (clubSetting.AppearanceSetting != null ? clubSetting.AppearanceSetting.HideProgramDates : false) : false;
            model.HideEnrollmentCapacity = clubSetting != null ? clubSetting.HideEnrollmentCapacityInClassPage : false;
            model.HidePhoneNumber = clubSetting != null ? (clubSetting.AppearanceSetting != null ? clubSetting.AppearanceSetting.HidePhoneNumber : false) : false;
            model.ERButtonTextLabel = clubSetting != null ? (clubSetting.AppearanceSetting != null ? (!string.IsNullOrEmpty(clubSetting.AppearanceSetting.ERButtonTextLabel) ? clubSetting.AppearanceSetting.ERButtonTextLabel : Constants.DefaultERButtonTextLabel) : Constants.DefaultERButtonTextLabel) : Constants.DefaultERButtonTextLabel;
            model.TestimonialsTitle = clubSetting != null ? (clubSetting.AppearanceSetting != null ? (!string.IsNullOrEmpty(clubSetting.AppearanceSetting.TestimonialsTitle) ? clubSetting.AppearanceSetting.TestimonialsTitle : Constants.DefaultTestimonialsTitle) : Constants.DefaultTestimonialsTitle) : Constants.DefaultTestimonialsTitle;


            if (program.IsWaitListAvailable && program.TypeCategory != ProgramTypeCategory.Camp && programBusiness.IsFull(program, program.Season.Status == SeasonStatus.Test))
            {
                model.IsWaitListMode = true;
            }

            model.DynamicStyle = ClubService.Get().GetDynamicStyle(clubSetting.AppearanceSetting);

            if (Ioc.ProgramBusiness.IsTeamChessTourney(program))
            {
                model.IsTeamRegistration = true;
            }

            if (program.TypeCategory == ProgramTypeCategory.ChessTournament)
            {
                var chessTourneyAttribute = ((TournamentScheduleAttribute)program.ProgramSchedules.ElementAt(0).Attributes);
                model.ShowViewEntries = !program.ClubDomain.Equals("alabamachess", StringComparison.OrdinalIgnoreCase) && !Ioc.ProgramBusiness.IsTeamChessTourney(program) && !chessTourneyAttribute.HideViewEntries;
            }

            IProgramBodyViewModel programBody = null;
            DateTime now = DateTimeHelper.GetCurrentLocalDateTime(program.Club.TimeZone);
            model.HasProvider = program.OutSourceSeasonId.HasValue;
            bool weeklyEmail = false;

            if (model.HasProvider && program.OutSourceSeason != null)
            {
                var providerClub = Ioc.ClubBusiness.Get(program.OutSourceSeason.ClubId);
                model.ProviderName = providerClub.Name;
                var providerFirstContact = providerClub.ContactPersons.First();
                model.ProviderContact = string.Format(Constants.F_FullName, providerFirstContact.FirstName, providerFirstContact.LastName);
                model.ProviderPhone = providerFirstContact.Phone;
                model.ProviderEmail = providerFirstContact.Email;
                weeklyEmail = (providerClub.CatalogSetting != null && providerClub.CatalogSetting.WeeklyEmail);
            }

            //get images and source images for slider
            model.ImageUrls = new List<string>();

            if (program.CatalogId.HasValue && program.Catalog != null && !string.IsNullOrEmpty(program.Catalog.ImageFileName))
            {
                model.ImageUrls.Add(program.Catalog.ImageFileName);

                if (program.Catalog.ImageGallery_Id.HasValue)
                {
                    var fileNames = program.Catalog.ImageGallery.Images.Where(i => i.Status == EventStatusCategories.open).Select(i => i.Path).ToList();

                    if (fileNames != null)
                    {
                        model.ImageUrls.AddRange(fileNames);
                        model.ListOfImagesAndSource = GetCatalogDicImagesURL(model.ImageUrls, program.Catalog.ImageGallery.ClubDomain);
                    }
                }
                else
                {
                    model.ListOfImagesAndSource = GetCatalogDicImagesURL(model.ImageUrls, program.Catalog.Club.Domain);
                }
            }
            else if (program.ImageGalleryId.HasValue)
            {
                var fileNames = program.ImageGallery.Images.Where(i => i.Status == EventStatusCategories.open).Select(i => i.Path).ToList();

                if (fileNames != null)
                {
                    model.ImageUrls.AddRange(fileNames);
                    model.ListOfImagesAndSource = GetCatalogDicImagesURL(model.ImageUrls, program.ImageGallery.ClubDomain);
                }
            }
            //end operation
            if (model.HasPartner)
            {
                if (program.Club.PartnerClub != null)
                {
                    model.PartnerName = program.Club.PartnerClub.Name;
                    model.PartnerEmail = program.Club.PartnerClub.ContactPersons.Any() ? program.Club.PartnerClub.ContactPersons.First().Email : Constants.DefaultPartnerEmail;
                }
                else
                {
                    model.PartnerName = Constants.DefaultPartnerName;
                    model.PartnerEmail = Constants.DefaultPartnerEmail;
                }
            }

            switch (program.TypeCategory)
            {
                case ProgramTypeCategory.Class:
                    {
                        var programScheduleMode = programBusiness.GetClassScheduleTime(program);

                        programBody = new SimpleProgramBodyViewModel()
                        {
                            Schedule = new ProgramBodySchedule()
                            {
                                Prices = program.ProgramSchedules.First(s => !s.IsDeleted).Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && c.IsDeleted == false)
                                .Select(c =>
                                new ChargeViewModel
                                {
                                    Name = c.Name,
                                    Charge = c,
                                    Amount = _programBusiness.CalculateProgramChargeAmount(club, c).Value,
                                    Category = c.Category,
                                    Capacity = c.Capacity,
                                    Description = c.Description,
                                    EarlyBirds = c.Attributes.EarlyBirds,
                                    HasEarlyBird = c.HasEarlyBird,
                                    IsProrate = programBusiness.IsProgramScheduleProrate(program.ProgramSchedules.ElementAt(0), c),
                                    ProratePrice = !c.Attributes.IsProrate ? 0 : programBusiness.CalculateProratePrice(program.ProgramSchedules.ElementAt(0), c),
                                    LeftSessions = programBusiness.CalculateLeftSessions(program.ProgramSchedules.ElementAt(0)),
                                    TotalSessions = programBusiness.CalculateTotalSessions(program.ProgramSchedules.ElementAt(0)),
                                    IsMandatory = c.Attributes.IsMandatory,
                                    NumberOfRegistration = Ioc.OrderItemBusiness.RegisteredCount(c.Id, program.Season.Status == SeasonStatus.Test)
                                })
                                .ToList(),

                                Services = program.ProgramSchedules.First()
                                   .Charges.Where(c => (c.Category == ChargeDiscountCategory.Lunch ||
                                    c.Category == ChargeDiscountCategory.DayCare ||
                                    c.Category == ChargeDiscountCategory.CustomCharge) && c.IsDeleted == false).
                                    ToList(),
                                Restrict = program.ProgramSchedules.First().AttendeeRestriction,
                                RegStatus = programRegStatus,

                                RegistrationPeriod = new RegistrationPeriod(program.ProgramSchedules.First().RegistrationPeriod, seasonBusiness.GetGeneralRegOpenDate(program.Season), seasonBusiness.GetGeneralRegCloseDate(program.Season)),

                                RecurrenceTime = (ScheduleAttribute)program.ProgramSchedules.First(o => o.IsDeleted == false).Attributes

                            },

                            Currency = program.Club.Currency,
                            Address = program.ClubLocation.PostalAddress,
                            ClubDomain = program.Club.Domain,
                            ClubLogo = program.Club.Logo,
                            Club = program.Club,
                            ClubTimeZone = program.Club.TimeZone,
                            Description = program.Description,
                            Domain = program.Domain,
                            EnablePriceHidden = club.Setting != null ? club.Setting.ClubProgram.EnablePriceHidden ? !club.Setting.ClubProgram.HideAllProgramsPrice ? club.Setting.ClubProgram.HiddenPricePrograms != null ? club.Setting.ClubProgram.HiddenPricePrograms.Any(p => p == program.Id) : false : true : false : false,
                            EndDate = programBusiness.EndDate(program).Value,
                            StartDate = programBusiness.StartDate(program).Value,
                            Id = program.Id,
                            LocationName = program.ClubLocation.Name,
                            HideGoogleMapInClassPage = hideGoogleMapInClassPage,
                            RoomAssignment = program.Room,
                            Name = program.Name,
                            SeasonDomain = program.Season.Domain,
                            WeeklyEmail = weeklyEmail ? Constants.Weekly_Email_Msg : string.Empty,
                            MaterialsNeeded = program.Catalog != null ? program.Catalog.MaterialsNeeded : string.Empty,
                            ClassDates = programBusiness.GetClassDates(program.ProgramSchedules.First(), programScheduleMode),
                            DisableCapacityRestriction = program.DisableCapacityRestriction,
                        };

                        var firstSchedule = program.ProgramSchedules.First(o => o.IsDeleted == false);

                        if (firstSchedule.Attributes.Capacity > 0)
                        {
                            ((SimpleProgramBodyViewModel)programBody).MaxEnrollment = firstSchedule.Attributes.Capacity.ToString();
                        }
                        if (firstSchedule.Attributes.MinimumEnrollment > 0)
                        {
                            ((SimpleProgramBodyViewModel)programBody).MinEnrollment = firstSchedule.Attributes.MinimumEnrollment.ToString();
                        }


                        model.Categories = program.Categories.Select(s =>
                            new SportsClub.Models.CategoryViewModel
                            {
                                Id = s.Id,
                                Name = s.Name,
                            })
                            .ToList();

                        model.Body = programBody;

                        model.HasNoSession = ((SimpleProgramBodyViewModel)model.Body).Schedule.Prices.All(c => c.HasNoSession);
                    }
                    break;
                case ProgramTypeCategory.Subscription:
                    {
                        var firstSchedule = program.ProgramSchedules.First(o => o.IsDeleted == false);

                        var subscriptionAttribute = firstSchedule.Attributes as ScheduleSubscriptionAttribute;
                        var today = DateTimeHelper.ConvertUtcDateTimeToLocal(DateTime.UtcNow, program.Club.TimeZone);

                        programBody = new SubscriptionBodyViewModel()
                        {
                            Schedule = new ProgramBodySchedule()
                            {
                                Prices = program.ProgramSchedules.First().Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && c.IsDeleted == false)
                                .Select(c =>
                                new ChargeViewModel
                                {
                                    Name = c.Name,
                                    Amount = c.Amount,
                                    Category = c.Category,
                                    Capacity = c.Capacity,
                                    Description = c.Description,
                                    EarlyBirds = c.Attributes.EarlyBirds,
                                    HasEarlyBird = c.HasEarlyBird,
                                    IsProrate = programBusiness.IsProgramScheduleProrate(program.ProgramSchedules.ElementAt(0), c),
                                    ProratePrice = !c.Attributes.IsProrate ? 0 : programBusiness.CalculateProratePrice(program.ProgramSchedules.ElementAt(0), c),
                                    LeftSessions = programBusiness.CalculateLeftSessions(program.ProgramSchedules.ElementAt(0)),
                                    TotalSessions = programBusiness.CalculateTotalSessions(program.ProgramSchedules.ElementAt(0)),
                                    IsMandatory = c.Attributes.IsMandatory,
                                    NumberOfRegistration = Ioc.OrderItemBusiness.RegisteredCount(c.Id, program.Season.Status == SeasonStatus.Test)
                                })
                                .ToList(),

                                Services = program.ProgramSchedules.First()
                                   .Charges.Where(c => (c.Category == ChargeDiscountCategory.Lunch ||
                                    c.Category == ChargeDiscountCategory.DayCare ||
                                    c.Category == ChargeDiscountCategory.CustomCharge) && c.IsDeleted == false).
                                    ToList(),
                                Restrict = program.ProgramSchedules.First().AttendeeRestriction,
                                RegStatus = programRegStatus,

                                RegistrationPeriod = new RegistrationPeriod(program.ProgramSchedules.First().RegistrationPeriod, seasonBusiness.GetGeneralRegOpenDate(program.Season), seasonBusiness.GetGeneralRegCloseDate(program.Season)),

                            },
                            Currency = program.Club.Currency,
                            ApplicationFeeAmount = firstSchedule.Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ApplicationFee) ? firstSchedule.Charges.First(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ApplicationFee).Amount : 0,
                            Address = program.ClubLocation.PostalAddress,
                            ClubDomain = program.Club.Domain,
                            EnablePriceHidden = club.Setting != null ? club.Setting.ClubProgram.EnablePriceHidden ? !club.Setting.ClubProgram.HideAllProgramsPrice ? club.Setting.ClubProgram.HiddenPricePrograms != null ? club.Setting.ClubProgram.HiddenPricePrograms.Any(p => p == program.Id) : false : true : false : false,
                            ClubLogo = program.Club.Logo,
                            ClubTimeZone = program.Club.TimeZone,
                            Description = program.Description,
                            Domain = program.Domain,
                            EndDate = programBusiness.EndDate(program).Value,
                            StartDate = programBusiness.StartDate(program).Value,
                            Id = program.Id,
                            LocationName = program.ClubLocation.Name,
                            HideGoogleMapInClassPage = hideGoogleMapInClassPage,
                            RoomAssignment = program.Room,
                            Name = program.Name,
                            SeasonDomain = program.Season.Domain,
                            WeeklyEmail = weeklyEmail ? Constants.Weekly_Email_Msg : string.Empty,
                            MaterialsNeeded = program.Catalog != null ? program.Catalog.MaterialsNeeded : string.Empty,

                            SubscriptionParts = _programBusiness.GetUpcomingSchedules(program.ScheduleParts.ToList(), today).Select(s =>
                                new ProgramSchedulePartViewModel
                                {
                                    StartDate = s.StartDate,
                                    EndDate = s.EndDate,
                                    DueDate = s.DueDate.HasValue ? s.DueDate : null
                                })
                                .ToList(),
                            Price = string.Join(", ", subscriptionAttribute.Subscriptions.Select(p => CurrencyHelper.FormatCurrencyWithoutPenny(p.Amount, program.Club.Currency))),

                        };

                        model.Categories = program.Categories.Select(s =>
                            new SportsClub.Models.CategoryViewModel
                            {
                                Id = s.Id,
                                Name = s.Name,
                            })
                            .ToList();

                        model.Body = programBody;

                        break;
                    }
                case ProgramTypeCategory.Camp:
                    {
                        var programSchedules = program.ProgramSchedules.Where(s => !s.IsDeleted).ToList().OrderBy(s => s.StartDate).ThenByDescending(s => s.EndDate);

                        programBody = new CampProgramBodyViewModel()
                        {
                            Address = program.ClubLocation.PostalAddress,
                            ClubDomain = program.Club.Domain,
                            EnablePriceHidden = club.Setting != null ? club.Setting.ClubProgram.EnablePriceHidden ? !club.Setting.ClubProgram.HideAllProgramsPrice ? club.Setting.ClubProgram.HiddenPricePrograms != null ? club.Setting.ClubProgram.HiddenPricePrograms.Any(p => p == program.Id) : false : true : false : false,
                            ClubLogo = program.Club.Logo,
                            ClubTimeZone = program.Club.TimeZone,
                            Description = program.Description,
                            Domain = program.Domain,
                            EndDate = programBusiness.EndDate(program).Value,
                            StartDate = programBusiness.StartDate(program).Value,
                            Id = program.Id,
                            LocationName = program.ClubLocation.Name,
                            HideGoogleMapInClassPage = hideGoogleMapInClassPage,
                            Name = program.Name,
                            SeasonDomain = program.Season.Domain,
                            Currency = program.Club.Currency,
                            EnabledWaitList = program.IsWaitListAvailable,
                            DisableCapacityRestriction = program.DisableCapacityRestriction,
                            Schedules = programSchedules.Select(s =>
                            new ProgramBodySchedule
                            {
                                Id = s.Id,
                                EndDate = s.EndDate,
                                Prices = s.Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && c.IsDeleted == false).Select(c =>
                                    new ChargeViewModel
                                    {
                                        Name = c.Name,
                                        Amount = c.Amount,
                                        Category = c.Category,
                                        Capacity = c.Capacity,
                                        Description = c.Description,
                                        EarlyBirds = c.Attributes.EarlyBirds,
                                        HasEarlyBird = c.HasEarlyBird,
                                        IsProrate = c.Attributes.IsProrate,
                                        ProratePrice = !c.Attributes.IsProrate ? 0 : programBusiness.CalculateProratePrice(program.ProgramSchedules.ElementAt(0), c),
                                        LeftSessions = programBusiness.CalculateLeftSessions(program.ProgramSchedules.ElementAt(0)),
                                        IsMandatory = c.Attributes.IsMandatory,
                                        TutionIsFull = Ioc.ProgramBusiness.IsFull(c, s.Program.Season.Status == SeasonStatus.Test),
                                        DropInLable = ((ScheduleAttribute)s.Attributes).EnableDropIn ? c.Attributes.DropInName + " " + CurrencyHelper.FormatCurrencyWithPenny(c.Attributes.DropInPrice.Value, club.Currency):string.Empty,
                                    })
                                .ToList(),

                                IsCapacityFull = Ioc.ProgramBusiness.IsCapacityFull(s.Charges.ToList()),
                                RecurrenceTime = (ScheduleAttribute)s.Attributes,
                                IsCampOvernight = s.IsCampOvernight,
                                RegistrationPeriod = s.RegistrationPeriod,
                                RegStatus = programRegStatus,
                                Restrict = s.AttendeeRestriction,
                                //EnabledDropIn = (ScheduleAttribute)s.Attributes.EnableDropIn,
                                Services = s.Charges.Where(c => (c.Category == ChargeDiscountCategory.Lunch || c.Category == ChargeDiscountCategory.DayCare || c.Category == ChargeDiscountCategory.CustomCharge) && c.IsDeleted == false).ToList(),
                                StartDate = s.StartDate,
                                TypeCategory = program.TypeCategory,
                                Title = s.Title
                            })
                            .ToList()
                        };

                        model.Body = programBody;
                    }
                    break;
                case ProgramTypeCategory.SeminarTour:
                    {
                        programBody = new SeminarProgramBodyViewModel()
                        {
                            Schedule = new ProgramBodySchedule()
                            {
                                Prices = program.ProgramSchedules.First().Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && c.IsDeleted == false)
                                .Select(c =>
                                    new ChargeViewModel
                                    {
                                        Name = c.Name,
                                        Amount = c.Amount,
                                        Category = c.Category,
                                        Capacity = c.Capacity,
                                        Description = c.Description,
                                        EarlyBirds = c.Attributes.EarlyBirds,
                                        HasEarlyBird = c.HasEarlyBird,
                                        IsProrate = c.Attributes.IsProrate,
                                        ProratePrice = !c.Attributes.IsProrate ? 0 : programBusiness.CalculateProratePrice(program.ProgramSchedules.ElementAt(0), c),
                                        LeftSessions = programBusiness.CalculateLeftSessions(program.ProgramSchedules.ElementAt(0)),
                                        IsMandatory = c.Attributes.IsMandatory,
                                    })
                                    .ToList(),

                                Services = program.ProgramSchedules.First()
                                   .Charges.Where(c => (c.Category == ChargeDiscountCategory.Lunch || c.Category == ChargeDiscountCategory.DayCare || c.Category == ChargeDiscountCategory.CustomCharge) && c.IsDeleted == false).ToList(),

                                Restrict = program.ProgramSchedules.First().AttendeeRestriction,
                                RegStatus = programRegStatus,
                                RegistrationPeriod = program.ProgramSchedules.First().RegistrationPeriod,

                                RecurrenceTime = (ScheduleAttribute)program.ProgramSchedules.First(o => o.IsDeleted == false).Attributes
                            },
                            Currency = program.Club.Currency,
                            Address = program.ClubLocation.PostalAddress,
                            ClubDomain = program.Club.Domain,
                            ClubLogo = program.Club.Logo,
                            EnablePriceHidden = club.Setting != null ? club.Setting.ClubProgram.EnablePriceHidden ? !club.Setting.ClubProgram.HideAllProgramsPrice ? club.Setting.ClubProgram.HiddenPricePrograms != null ? club.Setting.ClubProgram.HiddenPricePrograms.Any(p => p == program.Id) : false : true : false : false,
                            ClubTimeZone = program.Club.TimeZone,
                            Description = program.Description,
                            Domain = program.Domain,
                            EndDate = programBusiness.EndDate(program).Value,
                            StartDate = programBusiness.StartDate(program).Value,
                            Id = program.Id,
                            LocationName = program.ClubLocation.Name,
                            HideGoogleMapInClassPage = hideGoogleMapInClassPage,
                            Name = program.Name,
                            SeasonDomain = program.Season.Domain,
                        };

                        model.Categories = program.Categories.Select(s =>
                            new SportsClub.Models.CategoryViewModel
                            {
                                Id = s.Id,
                                Name = s.Name,
                            })
                            .ToList();

                        model.Body = programBody;
                    }
                    break;
                case ProgramTypeCategory.Calendar:
                    {
                        programBody = new CalendarBodyViewModel()
                        {
                            Address = program.ClubLocation.PostalAddress,
                            ClubDomain = program.Club.Domain,
                            EnablePriceHidden = club.Setting != null ? club.Setting.ClubProgram.EnablePriceHidden ? !club.Setting.ClubProgram.HideAllProgramsPrice ? club.Setting.ClubProgram.HiddenPricePrograms != null ? club.Setting.ClubProgram.HiddenPricePrograms.Any(p => p == program.Id) : false : true : false : false,
                            ClubLogo = program.Club.Logo,
                            ClubTimeZone = program.Club.TimeZone,
                            Description = program.Description,
                            Domain = program.Domain,
                            EndDate = programBusiness.EndDate(program).Value,
                            StartDate = programBusiness.StartDate(program).Value,
                            Id = program.Id,
                            LocationName = program.ClubLocation.Name,
                            Name = program.Name,
                            SeasonDomain = program.Season.Domain,
                            DisableCapacityRestriction = program.DisableCapacityRestriction,

                            Schedules = program.ProgramSchedules.Where(s => !s.IsDeleted).Select(s =>
                                new ProgramBodySchedule
                                {
                                    EndDate = s.EndDate,
                                    Prices = program.ProgramSchedules.First().Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && c.IsDeleted == false).Select(c =>
                                        new ChargeViewModel
                                        {
                                            Name = c.Name,
                                            Amount = c.Amount,
                                            Category = c.Category,
                                            Capacity = c.Capacity,
                                            Description = c.Description,
                                            EarlyBirds = c.Attributes.EarlyBirds,
                                            HasEarlyBird = c.HasEarlyBird,
                                            IsProrate = c.Attributes.IsProrate,
                                            ProratePrice = !c.Attributes.IsProrate ? 0 : programBusiness.CalculateProratePrice(program.ProgramSchedules.ElementAt(0), c),
                                            LeftSessions = programBusiness.CalculateLeftSessions(program.ProgramSchedules.ElementAt(0)),
                                            IsMandatory = c.Attributes.IsMandatory,
                                        })
                                    .ToList(),

                                    RecurrenceTime = (ScheduleAttribute)s.Attributes,
                                    RegistrationPeriod = s.RegistrationPeriod,
                                    RegStatus = programRegStatus,
                                    Restrict = s.AttendeeRestriction,

                                    Services = s.Charges.Where(c => (c.Category == ChargeDiscountCategory.Lunch || c.Category == ChargeDiscountCategory.DayCare || c.Category == ChargeDiscountCategory.CustomCharge) && c.IsDeleted == false).ToList(),

                                    StartDate = s.StartDate,
                                    TypeCategory = program.TypeCategory,
                                    Title = s.Title
                                })
                            .ToList()
                        };

                        model.Body = programBody;
                    }
                    break;
                case ProgramTypeCategory.ChessTournament:
                    {
                        var chessTourneyAttribute = ((TournamentScheduleAttribute)program.ProgramSchedules.ElementAt(0).Attributes);

                        programBody = new ChessTourneyBodyViewModel()
                        {
                            Schedule = new ProgramBodySchedule()
                            {
                                Prices = program.ProgramSchedules.First().Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && c.IsDeleted == false)
                                .Select(c =>
                                    new ChargeViewModel
                                    {
                                        Name = c.Name,
                                        Amount = c.Amount,
                                        Category = c.Category,
                                        Capacity = c.Capacity,
                                        Description = c.Description,
                                        EarlyBirds = c.Attributes.EarlyBirds,
                                        HasEarlyBird = c.HasEarlyBird
                                    })
                                    .ToList(),
                                Services = program.ProgramSchedules.First()
                                   .Charges.Where(c => (c.Category == ChargeDiscountCategory.Lunch || c.Category == ChargeDiscountCategory.DayCare || c.Category == ChargeDiscountCategory.CustomCharge) && !c.IsDeleted).ToList(),

                                Restrict = program.ProgramSchedules.First().AttendeeRestriction,
                                RegStatus = programRegStatus,
                                RegistrationPeriod = program.ProgramSchedules.First().RegistrationPeriod,
                                EndDate = programBusiness.EndDate(program).Value,
                                StartDate = programBusiness.StartDate(program).Value,

                            },
                            Currency = program.Club.Currency,
                            Address = program.ClubLocation.PostalAddress,
                            ClubDomain = program.Club.Domain,
                            ClubLogo = program.Club.Logo,
                            EnablePriceHidden = club.Setting != null ? club.Setting.ClubProgram.EnablePriceHidden ? !club.Setting.ClubProgram.HideAllProgramsPrice ? club.Setting.ClubProgram.HiddenPricePrograms != null ? club.Setting.ClubProgram.HiddenPricePrograms.Any(p => p == program.Id) : false : true : false : false,
                            ClubTimeZone = program.Club.TimeZone,
                            Description = program.Description,
                            Domain = program.Domain,
                            EndDate = programBusiness.EndDate(program).Value,
                            StartDate = programBusiness.StartDate(program).Value,
                            Id = program.Id,
                            LocationName = program.ClubLocation.Name,
                            Name = program.Name,
                            SeasonDomain = program.Season.Domain,
                            HideGoogleMapInClassPage = hideGoogleMapInClassPage,
                            ByeNote = chessTourneyAttribute.ByeNote,
                            FullEntries = chessTourneyAttribute.FullEntries,
                            LastRoundBye = chessTourneyAttribute.LastRoundBye,
                            MaxOfByes = chessTourneyAttribute.MaxOfByes,
                            NumberOfRounds = chessTourneyAttribute.NumberOfRounds,
                            PrizeFund = chessTourneyAttribute.PrizeFund,
                            PrizeFundGuarantee = chessTourneyAttribute.PrizeFundGuarantee,
                            Schedules = chessTourneyAttribute.Schedules,
                            Sections = chessTourneyAttribute.Sections,
                            Trophies = chessTourneyAttribute.Trophies,
                            TournamentType = chessTourneyAttribute.TournamentType,
                            DisableCapacityRestriction = program.DisableCapacityRestriction
                        };

                        model.Body = programBody;
                    }
                    break;
                case ProgramTypeCategory.BeforeAfterCare:
                    {

                        var firstSchedule = program.ProgramSchedules.First(o => !o.IsFreezed && !o.IsDeleted);

                        var scheduleAttribute = firstSchedule.Attributes as ScheduleAfterBeforeCareAttribute;
                        var programBodyModel = new BeforeAfterCareBodyViewModel();

                        var beforeScheduleTimes = new List<string>();
                        var afterScheduleTimes = new List<string>();

                        foreach (var schedule in program.ProgramSchedules.Where(s => !s.IsFreezed && !s.IsDeleted && s.ScheduleMode != TimeOfClassFormation.Both))
                        {
                            if (schedule.ScheduleMode == TimeOfClassFormation.AM)
                            {
                                beforeScheduleTimes = Ioc.ProgramBusiness.GetScheduleTime(schedule);
                            }
                            else
                            {
                                afterScheduleTimes = Ioc.ProgramBusiness.GetScheduleTime(schedule);
                            }
                        }

                        var comboTime = program.ProgramSchedules.Any(s => !s.IsDeleted && s.ScheduleMode == TimeOfClassFormation.Both) ? Ioc.ProgramBusiness.GetComboTime(program.ProgramSchedules.Where(s => !s.IsDeleted && s.ScheduleMode != TimeOfClassFormation.Both).ToList()) : null;

                        programBodyModel.Currency = program.Club.Currency;
                        programBodyModel.ApplicationFeeAmount = firstSchedule.Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ApplicationFee) ? firstSchedule.Charges.First(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ApplicationFee).Amount : 0;
                        programBodyModel.Address = program.ClubLocation.PostalAddress;
                        programBodyModel.ClubDomain = program.Club.Domain;
                        programBodyModel.ClubLogo = program.Club.Logo;
                        programBodyModel.EnablePriceHidden = club.Setting != null ? club.Setting.ClubProgram.EnablePriceHidden ? !club.Setting.ClubProgram.HideAllProgramsPrice ? club.Setting.ClubProgram.HiddenPricePrograms != null ? club.Setting.ClubProgram.HiddenPricePrograms.Any(p => p == program.Id) : false : true : false : false;
                        programBodyModel.ClubTimeZone = program.Club.TimeZone;
                        programBodyModel.Description = program.Description;
                        programBodyModel.PaymentSchedule = scheduleAttribute.PaymentScheduleMode;
                        programBodyModel.Domain = program.Domain;
                        programBodyModel.EndDate = programBusiness.EndDate(program).Value;
                        programBodyModel.StartDate = programBusiness.StartDate(program).Value;
                        programBodyModel.Id = program.Id;
                        programBodyModel.LocationName = program.ClubLocation.Name;
                        programBodyModel.HideGoogleMapInClassPage = hideGoogleMapInClassPage;
                        programBodyModel.RoomAssignment = program.Room;
                        programBodyModel.Name = program.Name;
                        programBodyModel.SeasonDomain = program.Season.Domain;
                        programBodyModel.WeeklyEmail = weeklyEmail ? Constants.Weekly_Email_Msg : string.Empty;
                        programBodyModel.MaterialsNeeded = program.Catalog != null ? program.Catalog.MaterialsNeeded : string.Empty;

                        var today = DateTimeHelper.ConvertUtcDateTimeToLocal(DateTime.UtcNow, program.Club.TimeZone);
                        var upcomingScheduleParts = _programBusiness.GetUpcomingSchedules(program.ScheduleParts.ToList(), today);

                        programBodyModel.ProgramScheduleParts = upcomingScheduleParts.Select(s =>
                                new ProgramSchedulePartViewModel
                                {
                                    StartDate = s.StartDate,
                                    EndDate = s.EndDate,
                                    DueDate = s.DueDate.HasValue ? s.DueDate : null
                                })
                                .ToList();
                        programBodyModel.Schedules = program.ProgramSchedules.Where(s => !s.IsFreezed && !s.IsDeleted).OrderByDescending(s => s.ScheduleMode).Select(s =>
                            new ProgramBodySchedule
                            {
                                Prices = s.Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && c.IsDeleted == false).Select(c =>
                                 new ChargeViewModel
                                 {
                                     Name = c.Name,
                                     Amount = c.Amount,
                                     Category = c.Category,
                                     Capacity = c.Capacity,
                                     Description = c.Description,
                                     EarlyBirds = c.Attributes.EarlyBirds,
                                     HasEarlyBird = c.HasEarlyBird,
                                     IsProrate = c.Attributes.IsProrate,
                                     ProratePrice = !c.Attributes.IsProrate ? 0 : programBusiness.CalculateProratePrice(program.ProgramSchedules.ElementAt(0), c),
                                     LeftSessions = programBusiness.CalculateLeftSessions(program.ProgramSchedules.ElementAt(0)),
                                     IsMandatory = c.Attributes.IsMandatory,
                                     ChargeId = c.Id
                                 })

                         .ToList(),

                                ScheduleMode = s.ScheduleMode,
                                Title = s.Title,
                                Description = s.Description,
                                ComboTime = s.ScheduleMode == TimeOfClassFormation.Both ? comboTime : null,
                                Times = s.ScheduleMode == TimeOfClassFormation.AM ? beforeScheduleTimes : afterScheduleTimes,
                            })
                        .ToList();

                        programBodyModel.DefaultSchedule = new ProgramBodySchedule()
                        {
                            Days = scheduleAttribute.Days,
                            RegistrationPeriod = firstSchedule.RegistrationPeriod,
                            RegStatus = programRegStatus,
                            Restrict = firstSchedule.AttendeeRestriction,
                            Services = firstSchedule.Charges.Where(c => (c.Category == ChargeDiscountCategory.Lunch || c.Category == ChargeDiscountCategory.DayCare || c.Category == ChargeDiscountCategory.CustomCharge) && c.IsDeleted == false).ToList(),
                            StartDate = firstSchedule.StartDate,
                            TypeCategory = program.TypeCategory,
                            EndDate = firstSchedule.EndDate,
                        };

                        foreach (var schedule in programBodyModel.Schedules)
                        {
                            foreach (var charge in schedule.Prices.Where(p => p.Category == ChargeDiscountCategory.EntryFee))
                            {
                                var dbCharge = program.ProgramSchedules.Where(s => !s.IsFreezed && !s.IsDeleted).SelectMany(c => c.Charges).Where(c => c.Id == charge.ChargeId).FirstOrDefault();

                                var chargeWeekDay = ((BeforeAfterCareChargeAttribute)dbCharge.Attributes).NumberOfClassDay;

                                charge.ChargeNumber = int.Parse(chargeWeekDay);
                            }

                            schedule.Prices = schedule.Prices.Where(p => p.Category == ChargeDiscountCategory.EntryFee).OrderByDescending(n => n.ChargeNumber).ToList();
                        }

                        programBodyModel.DropInPunchcard = new List<DropInPunchardModel>();

                        if (scheduleAttribute.EnableDropIn)
                        {
                            programBodyModel.DropInPunchcard.Add(new DropInPunchardModel
                            {
                                Name = scheduleAttribute.DropInSessionLabel,
                                Amount = scheduleAttribute.DropInSessionPrice,

                            });

                        }

                        if (scheduleAttribute.PunchCard.Enabled)
                        {
                            programBodyModel.DropInPunchcard.Add(new DropInPunchardModel
                            {
                                Name = scheduleAttribute.PunchCard.Title,
                                Amount = scheduleAttribute.PunchCard.Amount.HasValue ? scheduleAttribute.PunchCard.Amount.Value : 0,

                            });
                        }

                        model.Body = programBodyModel;
                    }
                    break;
            }

            model.RegStatus = programRegStatus;

            model.Body.Address = program.ClubLocation.PostalAddress;

            model.Body.LocationName = program.ClubLocation.Name;

            model.ClubTimeZone = program.Club.TimeZone;

            model.Body.ClubLogo = Ioc.ClubBusiness.GetClubLogoURL(program.Club.Domain, program.Club.Logo);

            model.Map = new GoogleMapsViewModel()
            {
                Name = program.ClubLocation.Name,
                PostalAddress = program.ClubLocation.PostalAddress,
                ContainerClass = "map-modal-container",
                ContainerId = "mapModalCotainer",
                HideGoogleMapInClassPage = hideGoogleMapInClassPage,
            };

            var clubInfo = Ioc.ClubBusiness.GetHelpInformationSetting(club);

            model.ClubName = clubInfo.Name;
            model.ClubEmail = clubInfo.Email;
            model.ClubPhoneNumber = clubInfo.Phone;
            model.PhoneExtension = clubInfo.PhoneExtension;
            model.IsFromHomePage = false;
            return View("Display", model);
        }

        [Authorize(Roles = "Admin, Parent, Manager")]
        public virtual ActionResult ReadSchedule([DataSourceRequest]DataSourceRequest request, string clubDomain, string seasonDomain, string domain, bool hasSchedule = true)
        {
            var sections = new List<CalendarModel>();
            var club = Ioc.ClubBusiness.Get(clubDomain);
            var dateNow = Ioc.ClubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow);
            var schedules = Ioc.ProgramBusiness.Get(club.Id, seasonDomain, domain).ProgramSchedules.Where(s => !s.IsDeleted);
            foreach (var schedule in schedules)
            {
                sections.AddRange(schedule.Sessions.Select(s => new CalendarModel()
                {
                    TaskID = s.Id,
                    Selected = false,
                    Start = DateTime.SpecifyKind(s.Start, DateTimeKind.Utc),
                    End = DateTime.SpecifyKind(s.End, DateTimeKind.Utc),
                    DoDate = DateTime.SpecifyKind(s.Start, DateTimeKind.Utc),
                    IsAllDay = false,
                    Title = schedule.Title,// s.Start.ToShortTimeString() + "-" + s.End.ToShortTimeString(),
                    ScheduleId = schedule.Id,
                    ScheduleTitle = schedule.Title,
                    ChargeId = schedule.Charges.First(c => c.Category == ChargeDiscountCategory.EntryFee).Id,
                    Amount = schedule.Charges.First(c => c.Category == ChargeDiscountCategory.EntryFee).HasEarlyBird &&
                      dateNow.Subtract(s.Start).Days >= schedule.Charges.First(c => c.Category == ChargeDiscountCategory.EntryFee).Attributes.EarlyBirds.First().PriorDay
                      ? schedule.Charges.First(c => c.Category == ChargeDiscountCategory.EntryFee).Attributes.EarlyBirds.First().Price
                      : schedule.Charges.First().Amount,
                    ChargeName = s.Start.ToString("MMM dd") + " " + s.Start.ToString("hh:mm") + "-" + s.End.ToString("hh:mm"),
                    ChargeCategory = ChargeDiscountCategory.EntryFee.ToString(),
                    Description2 = schedule.Title,
                }).ToList());
            }


            return Json(sections.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        //Get dictionery from image and source
        public Dictionary<string, string> GetCatalogDicImagesURL(List<string> fileNames, string clubDomain)
        {
            var ImageUrls = new Dictionary<string, string>();
            foreach (var name in fileNames)
            {
                string imageUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, string.Format(@"{0}\{1}", clubDomain, Constants.Path_CatalogItems_Folder), name).AbsoluteUri;
                string largeImageUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, string.Format(@"{0}\{1}", clubDomain, Constants.Path_CatalogItems_Folder), "large-" + name).AbsoluteUri;
                var isExists = StorageManager.IsExist(largeImageUri);
                var existLargUri = isExists ? largeImageUri : null;
                if (string.IsNullOrEmpty(name))
                {
                    imageUri = string.Empty;
                }
                if (!string.IsNullOrEmpty(imageUri) && !string.IsNullOrEmpty(existLargUri))
                {

                    ImageUrls.Add(imageUri, existLargUri);
                }
                else
                {
                    ImageUrls.Add(imageUri, null);
                }
            }
            return ImageUrls;
        }

    }
}