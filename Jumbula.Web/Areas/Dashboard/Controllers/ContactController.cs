﻿using System.Web.Mvc;
using Jumbula.Common.Constants;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Contact;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class ContactController : DashboardBaseController
    {
        #region Fields
        private readonly IContactPersonBusiness _contactPersonBusiness;
        #endregion

        #region Constractors
        public ContactController(IContactPersonBusiness contactPersonBusiness)
        {
            _contactPersonBusiness = contactPersonBusiness;
        }
        #endregion

        #region ActionMethods

        [HttpGet]
        public ActionResult GetCreate()
        {
            var result = _contactPersonBusiness.GetCreate(ActiveClub.Id);

            return JsonNet(result);
        }

        [HttpPost]
        [JbAudit(JbAction.Contact_Add)]
        public ActionResult Create(ContactCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                SetActivityDescription(Constants.AuditingConstants.AuditingAddFormat, $"{model.FirstName} {model.LastName}");
                var result = _contactPersonBusiness.Create(model, ActiveClub.Id);

                return JsonNet(result);
            }

            return JsonNet(false);
        }

        public ActionResult GetFilters()
        {
            var result = _contactPersonBusiness.GetFilters(ActiveClub.Id);
            return JsonNet(result);
        }

        public ActionResult GetListSettings()
        {
            var result = _contactPersonBusiness.GetListSettings(ActiveClub.Id);

            return JsonNet(result);
        }

        [HttpPost]
        public ActionResult GetList(PaginationModel paginationModel, ContactFilterViewModel filters)
        {
            var result = _contactPersonBusiness.GetPartnerContacts(ActiveClub.Id, filters, paginationModel.Sort, paginationModel.Page, paginationModel.PageSize);

            return JsonNet(result);
        }

        [HttpGet]
        public ActionResult GetEdit(int id)
        {
            var result = _contactPersonBusiness.GetEdit(id, ActiveClub);

            return JsonNet(result);
        }

        [HttpPost]
        [JbAudit(JbAction.Contact_Edit)]
        public ActionResult SaveEdit(ContactEditViewModel model)
        {
            CheckEditContactValidation(model);

            if (!ModelState.IsValid) return JsonNet(false);

            SetActivityDescription(Constants.AuditingConstants.AuditingChangeFormat, $"{model.FirstName} {model.LastName}");
            var result = _contactPersonBusiness.Update(model, ActiveClub);

            return JsonNet(result);

        }

        [HttpGet]
        public ActionResult GetSettings()
        {
            var result = _contactPersonBusiness.GetSettings(ActiveClub.Id);

            return JsonNet(result);
        }

        [HttpPost]
        [JbAudit(JbAction.ContactSettings_Save)]
        public ActionResult SaveSettings(ContactSettingsViewModel model)
        {
            SetActivityDescription(Constants.AuditingConstants.ContactSaveSettings);
            var result = _contactPersonBusiness.UpdateSettings(model, ActiveClub.Id);

            return JsonNet(result);
        }

        [HttpPost]
        [JbAudit(JbAction.Contact_Delete)]
        public ActionResult Delete(int id)
        {
            var contact = _contactPersonBusiness.Get(id);
            SetActivityDescription(Constants.AuditingConstants.AuditingDeleteFormat, $"{contact.FirstName} {contact.LastName}");
            var result = _contactPersonBusiness.Delete(id);

            return JsonNet(result);
        }
        #endregion

        #region Validations
        private void CheckEditContactValidation(ContactEditViewModel model)
        {
            if (model.IsPrimary)
            {
                if (model.ShowEmail && string.IsNullOrWhiteSpace(model.Email))
                {
                    ModelState.AddModelError("model.Email", "Email address is required for primary contacts.");
                }

                if (model.ShowPhone && string.IsNullOrWhiteSpace(model.Phone))
                {
                    ModelState.AddModelError("model.Phone", "Phone number is required for primary contacts.");
                }
            }
        }
        #endregion
    }
}