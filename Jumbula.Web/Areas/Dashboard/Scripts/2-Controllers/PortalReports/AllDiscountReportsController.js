﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('AllDiscountReportsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        $scope.Model = customReportService.getModelFromStrorage();

        if (is.undefined($scope.Model)) {
            $scope.Model = {};
        }

        $scope.Total = 0;
       

        $http.get(baseUrl + '/GetAllDiscountInDate', { params: { start: $scope.Model.From, end: $scope.Model.To, calucalteTotal: true } })
            .success(function (data, status, headers, config) {
                $scope.Total = data.Total;
                $scope.TotalDiscounts = data.TotalDiscounts;
                $scope.TotalScholar = data.TotalScholar;
                $scope.Date = data.Date;
                $scope.ClubName = data.ClubName;
                prepareGrid();
            });

        $scope.Model.checkedIds = {};
        var detailExportPromises = [];

        var prepareGrid = function () {
            $scope.AllDiscountReportsDataSource = new kendo.data.DataSource({

                type: "json",
                transport: {
                    read: {
                        url: baseUrl + '/GetAllScholarshipDiscounts',
                        type: "POST",
                        data: function () {
                            return {
                                start: $scope.Model.From,
                                end: $scope.Model.To
                            };
                        },
                    },
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                resizable: true,
                batch: true,
            });

            $scope.DiscountReportGridOptions = {
                excel: {
                    allPages: true,
                    fileName: "Scholarship_Discount.xlsx",
                    proxyURL: "/Dashboard/CustomReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.push({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.push({
                        cells: [{
                            value: "Total scholarships: " + $scope.TotalScholar
                        }]
                    });
                    rows.push({
                        cells: [{
                            value: "Total discounts: " + $scope.TotalDiscounts
                        }]
                    });
                    rows.push({
                        cells: [{
                            value: "Total scholarships/discounts: " + $scope.Total
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: $scope.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Scholarship/Discount - " + $scope.ClubName
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.AllDiscountReportsDataSource,
                resizable: true,
                detailTemplate: kendo.template($("#template").html()),
                dataBound: function () {
                    var grid = $("#DiscountReportGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);

                    var data = grid.dataSource.data();
                    var isEmptyScholarshipAmount1 = false;
                    var isEmptyScholarshipAmount2 = false;
                    var isEmptyScholarshipAmount3 = false;
                    var isEmptyDiscountName1 = false;
                    var isEmptyDiscountName2 = false;
                    var isEmptyDiscountName3 = false;
                    var isEmptyDiscountName4 = false;
                    var isEmptyDiscountName5 = false;

                    for (var i = 0 ; i < data.length; i++) {
                        if (data[i].ScholarshipAmount1 != "") {
                            isEmptyScholarshipAmount1 = true;
                        }

                        if (data[i].ScholarshipAmount2 != "") {
                            isEmptyScholarshipAmount2 = true;
                        }

                        if (data[i].ScholarshipAmount3 != "") {
                            isEmptyScholarshipAmount3 = true;
                        }

                        if (data[i].DiscountName1 != "") {
                            isEmptyDiscountName1 = true;
                        }

                        if (data[i].DiscountName2 != "") {
                            isEmptyDiscountName2 = true;
                        }

                        if (data[i].DiscountName3 != "") {
                            isEmptyDiscountName3 = true;
                        }

                        if (data[i].DiscountName4 != "") {
                            isEmptyDiscountName4 = true;
                        }

                        if (data[i].DiscountName5 != "") {
                            isEmptyDiscountName5 = true;
                        }
                    }

                    if (!isEmptyScholarshipAmount1) {

                        grid.hideColumn("ScholarshipAmount1");

                    }
                    if (!isEmptyScholarshipAmount2) {

                        grid.hideColumn("ScholarshipAmount2");

                    }
                    if (!isEmptyScholarshipAmount3) {

                        grid.hideColumn("ScholarshipAmount3");

                    }
                    if (!isEmptyDiscountName1) {

                        grid.hideColumn("DiscountName1");
                        grid.hideColumn("DiscountAmount1");

                    }
                    if (!isEmptyDiscountName2) {

                        grid.hideColumn("DiscountName2");
                        grid.hideColumn("DiscountAmount2");
                    }
                    if (!isEmptyDiscountName3) {

                        grid.hideColumn("DiscountName3");
                        grid.hideColumn("DiscountAmount3");
                    }
                    if (!isEmptyDiscountName4) {

                        grid.hideColumn("DiscountName4");
                        grid.hideColumn("DiscountAmount4");
                    }
                    if (!isEmptyDiscountName5) {

                        grid.hideColumn("DiscountName5");
                        grid.hideColumn("DiscountAmount5");
                    }

                    var view = this.dataSource.view();
                    for (var i = 0; i < view.length; i++) {
                        if ($scope.Model.checkedIds[view[i].UserId]) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .addClass("k-state-selected")
                            .find(".checkbox")
                            .attr("checked", "checked");
                        }
                    }

                    //Hide expand/collapse row 
                    $(".k-hierarchy-col").remove();
                    $(".k-hierarchy-cell").remove();

                },
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5,
                },
                sortable: true,
                columns: [

                    {
                        template: "<input type='checkbox' class='checkbox' />",
                        width: "30px",
                        headerTemplate: '<input type="checkbox" class="checkAll" />'
                    },
                    {
                        field: "User",
                        title: "Full name",
                        width: "180px",
                    },
                    {
                        field: "ItemName",
                        title: "Program",
                        width: "210px",
                    },
                    {
                        field: "SeasonName",
                        title: "Season",
                        width: "170px",
                    },
                    {
                        field: "Date",
                        title: "Date",
                        width: "150px",
                    },
                    {
                        field: "Confirmation",
                        title: "Confirmation",
                        width: "170px",
                    },
                    {
                        field: "DiscountName1",
                        title: "Discount 1",
                        width: "170px"
                    },
                    {
                        field: "DiscountAmount1",
                        title: "Amount ($)",
                        width: "150px",
                    },
                    {
                        field: "DiscountName2",
                        title: "Discount 2",
                        width: "170px"
                    },
                    {
                        field: "DiscountAmount2",
                        title: "Amount ($)",
                        width: "150px",
                    },
                    {
                        field: "DiscountName3",
                        title: "Discount 3",
                        width: "170px"
                    },
                    {
                        field: "DiscountAmount3",
                        title: "Amount ($)",
                        width: "150px",
                    },
                    {
                        field: "DiscountName4",
                        title: "Discount 4",
                        width: "170px"
                    },
                    {
                        field: "DiscountAmount4",
                        title: "Amount ($)",
                        width: "150px",
                    },
                    {
                        field: "DiscountName5",
                        title: "Discount 5",
                        width: "170px"
                    },
                    {
                        field: "DiscountAmount5",
                        title: "Amount ($)",
                        width: "150px",
                    },
                    {
                        field: "ScholarshipAmount1",
                        title: "Scholarship 1 ($)",
                        width: "210px"
                    },
                    {
                        field: "ScholarshipAmount2",
                        title: "Scholarship 2 ($)",
                        width: "210px",
                    },
                    {
                        field: "ScholarshipAmount3",
                        title: "Scholarship 3 ($)",
                        width: "210px",
                    },
                    {
                        field: "TotalDiscount",
                        title: "Total ($)",
                        width: "150px",
                    }
                ]
            }
        }

        $(document.body).on('click', '#DiscountReportGrid table .checkbox', selectRow);
        $(document.body).on('click', '#DiscountReportGrid table .checkAll', checkAll);

        function selectRow() {
            var checked = this.checked,
            row = $(this).closest("tr"),
            grid = $("#DiscountReportGrid").data("kendoGrid"),
            dataItem = grid.dataItem(row);
            $scope.Model.checkedIds[dataItem.UserId] = checked;

            if (checked) {
                //-select the row
                row.addClass("k-state-selected");
            } else {
                //-remove selection
                row.removeClass("k-state-selected");
            }
        }

        function checkAll() {
            var grid = $("#DiscountReportGrid").data("kendoGrid");
            var state = $("#DiscountReportGrid table > thead > tr > th:nth-child(1) > input").prop("checked");
            var ckbox;
            grid.tbody.find("tr.k-master-row").each(function () {
                var $this = $(this);
                ckbox = $this.find("td .checkbox");
                ckbox.prop("checked", state);
                var dataItem = grid.dataItem($this);
                $scope.Model.checkedIds[dataItem.UserId] = state;
                if (state) {
                    //-select the row
                    $this.addClass("k-state-selected");
                } else {
                    //-remove selection
                    $this.removeClass("k-state-selected");
                }
            });
        }


        $scope.MakeCampaign = function () {
            var checked = [];
            for (var i in $scope.Model.checkedIds) {
                if ($scope.Model.checkedIds[i]) {
                    checked.push(i);
                }
            }
            // Redirect to the special action
            if (checked.length > 0) {
                $http.post(baseUrl + "/SendEmailToMembers", { membersIds: checked, reportName: "ScholarshipDiscount" })
                    .success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            $state.go('EmailCampaignAddStep2', { campaignId: data.Data, campaignType: 'UserList' });
                        } else {
                            jbToast.error(data.Message);
                        }
                    }).error(function (data, status, headers, config) {
                        //alert(data);
                    });
            } else {
                jbToast.error("Please select at least a participant to send mail.");
            }
        };

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#DiscountReportGrid").getKendoGrid().saveAsExcel();
        }
    }
    ]);

})();