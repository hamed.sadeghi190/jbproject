﻿using System;
using Jumbula.Common.Enums;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Report;
using Jumbula.Core.Model.Report.Filter;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Jumbula.Business.ReportBuilders
{
    public class SignOutSheetReportBuilderBusiness : BaseReportBuilder
    {
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IFormBusiness _formBusiness;
        private readonly INewReportBusiness _reportBusiness;

        private const string SeasonDomainParam = "seasonDomain";
        private const string ProgramIdParam = "programId";

        private string _seasonDomain;

        public SignOutSheetReportBuilderBusiness()
        {
            _orderItemBusiness = (IOrderItemBusiness)DependencyResolver.Current.GetService(typeof(IOrderItemBusiness));
            _formBusiness = (IFormBusiness)DependencyResolver.Current.GetService(typeof(IFormBusiness));
            _reportBusiness = (INewReportBusiness)DependencyResolver.Current.GetService(typeof(INewReportBusiness));
        }

        public override ReportName ReportName => ReportName.SignOutSheet;
        public override PdfTemplateName? PdfTemplate => PdfTemplateName.Default;
        public override ExcelTemplateName? ExcelTemplate => ExcelTemplateName.Default;

        public override ReportDataModel BindData(Dictionary<string, object> parameters, ClubBaseInfoModel clubBaseInfo, PaginationModel paginationModel, List<ReportGridSort> sort = null)
        {
            if (GetParameter(parameters, ProgramIdParam) == null)
            {
                return new ReportDataModel(null, null);
            }

            var programId = long.Parse(GetParameter(parameters, ProgramIdParam).ToString());

            var orderItems = _orderItemBusiness.GetProgramOrderItems(programId);

            if (orderItems == null || !orderItems.Any())
            {
                return new ReportDataModel(null, null);
            }

            var data = orderItems.OrderBy(o => o.Player.Contact.FirstName).Paginate(paginationModel.Page, paginationModel.PageSize).ToList()
                .Select(i => new
                {
                    ParticipantFirstName = _formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.FirstName),
                    ParticipantLastName = _formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.LastName),
                    ParentFirstName = _formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName),
                    ParentLastName = _formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.LastName),
                    ParentPrimaryPhone = _formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }),
                    ParentAuthorizedAdult = _formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, "Authorized Adult"),
                    ParentAuthorizedAdultPhone = _formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, "Authorized Adult Phone"),
                })
                .ToList();

            var result = data.ToPaginatedList<object>(paginationModel.Page, paginationModel.PageSize, orderItems.Count());

            return new ReportDataModel(result, null);
        }

        public override List<IJbReportFilterElement> GetFilters()
        {
            throw new System.NotImplementedException();
        }

        public override List<ReportHeader> GetHeaders()
        {
            throw new System.NotImplementedException();
        }

        public override ReportGridModel GetReportGrid()
        {
            throw new System.NotImplementedException();
        }

        public override List<ReportGridColumn> GetColumns()
        {
            throw new NotImplementedException();
        }

        public override PaginatedList<object> GetData()
        {
            throw new NotImplementedException();
        }

        public override void Initiate()
        {
            throw new NotImplementedException();
        }

        public override ReportModel GetSchema(Dictionary<string, object> parameters, ClubBaseInfoModel clubBaseInfo)
        {
            _seasonDomain = GetParameter(parameters, SeasonDomainParam).ToString();

            //Columns = new List<KendoColumn>
            //{
            //    new KendoColumn
            //    {
            //        field = "ParticipantFirstName",
            //        title = "Participant first name",
            //        width = "190px"
            //    },
            //    new KendoColumn
            //    {
            //        field = "ParticipantLastName",
            //        title = "Last name",
            //        width = "190px"
            //    },
            //    new KendoColumn
            //    {
            //        field = "ParentFirstName",
            //        title = "Parent first name",
            //        width = "190px"
            //    },
            //    new KendoColumn
            //    {
            //        field = "ParentLastName",
            //        title = "Last name",
            //        width = "190px"
            //    },
            //    new KendoColumn
            //    {
            //        field = "ParentPrimaryPhone",
            //        title = "Primary phone",
            //        width = "175px"
            //    },
            //    new KendoColumn
            //    {
            //        field = "ParentAuthorizedAdult",
            //        title = "Authorized adult name",
            //        width = "200px"
            //    },
            //    new KendoColumn
            //    {
            //        field = "ParentAuthorizedAdultPhone",
            //        title = "Phone",
            //        width = "90px"
            //    },
            //    new KendoColumn
            //    {
            //        field = "PickupSignature",
            //        title = "Pick-up signature",
            //        width = "180px"
            //    }
            //};

            var headerParams = new Dictionary<string, object> { { "clubId", clubBaseInfo.Id }, { "seasonDomain", _seasonDomain } };

            //Header = _reportBusiness.GetReportHeaders(headerParams, ReportHeader.Date, ReportHeader.SeasonName);

            //Filter = new Dictionary<string, object>
            //{
            //    {
            //        ReportFilterElementName.Program.ToString(),
            //        new ReportFilterModel
            //        {
            //            Data = _reportBusiness.GetSeasonPrograms(_seasonDomain, clubBaseInfo.Id),
            //            ActionFilter = ReportFilterAction.PrepareData,
            //        }
            //    }
            //};

            //Export = new Dictionary<string, object> { { ReportExportType.Pdf.ToString(), PdfTemplate.Default }, { ReportExportType.Excel.ToString(), null } };

            ReportGridModel = new ReportGridModel
            {
                //columns = Columns,
                PageSize = 50
            };
            InitializeReportModel();
            return Model;
        }
    }

}
