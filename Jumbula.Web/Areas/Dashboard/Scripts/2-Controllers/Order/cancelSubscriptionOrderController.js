﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('cancelSubscriptionOrderController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'cancelSubscriptionOrderService', function ($scope, $http, $log, $stateParams, $state, jbToast, cancelSubscriptionOrderService) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();

        if (!$scope.MC.account.isPartner) {
            $scope.MC.openBlade({ url: "Dashboard/Blade/Season", order: 1, menu: "SEASONS" });
        }
        $scope.baseUrl = window.location.origin + "/Dashboard/Order";
        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $stateParams.seasonDomain;
        $scope.orderItemId = $stateParams.orderId;

        var returnedByBack = $state.returnByBack();
        $scope.Model = {};

        if (returnedByBack) {
            $scope.Model = cancelSubscriptionOrderService.get();
        }
        if (!returnedByBack || $scope.Model == null) {

            $http.get($scope.baseUrl + '/GetCancelSubscriptionOrder/?orderItemId=' + $stateParams.orderId).success(function (data, status, headers, config) {
               
                if (data.Status == false) {

                    jbToast.error("We have encountered an internal error and cannot submit your changes. Please contact Jumbula support (support@jumbula.com) and provide a screenshot of this page.")

                } else {
                    $scope.Model = data;

                }

            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }


        $scope.Continue = function () {
                $http.post($scope.baseUrl + '/CheckValidationCancelSubscriptionItem', { model: $scope.Model })
                 .success(function (data, status, headers, config) {
                     if (data.Status == true) {

                         cancelSubscriptionOrderService.set($scope.Model);
                         $state.forward('ConfirmCancelSubscriptionItem', { seasonDomain: $scope.seasonName, orderId: $scope.orderItemId }, 'CancelSubscriptionOrder', { seasonDomain: $scope.seasonName, orderId: $scope.orderItemId }, function () { });
                     }
                     else {
                         $scope.MC.handleErrors($scope, data);
                     }
                 })
        }

        $scope.DisplayeCancellationFeeBox = function (displaye) {

            if (displaye) {
                $scope.Model.ShowCancellationFeeBox = true;
            } else {
                $scope.Model.ShowCancellationFeeBox = false;
                $scope.Model.CancellationFee = 0;
            }
        }

        $scope.formatDate = function (date) {

            if (date != '' && date != null && date != 'undefined') {
                return new Date(date).format('mm/dd/yyyy');
            }
        }

        if (returnedByBack) {
            $scope.MC.backQueueItems.pop();
        }

    }]);
})();