﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('ReconciliationPrepReportController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'county-providers-view';
            window.app.globalObjects.initializeView();

            $scope.MC.openBlade({ url: "Dashboard/Blade/FinanceReports", order: 1, menu: "PortalReports" });

            var baseUrl = window.location.origin + '/Dashboard/PortalReport';

            $scope.stateParams = $stateParams;
            $scope.MC.stateParams = $stateParams;
            $scope.Model = {};

            $scope.reportName = "";
            var nameModel = customReportService.getModelFromStrorage();
            var seasonId = nameModel.seasonId;
            var schoolId = nameModel.schoolId;
            $scope.filter = {};
            $scope.filter.printMode = false;
            var dataInfo = {};

            $http.get(baseUrl + '/GetReportHeader', { params: { clubId: schoolId, seasonId: seasonId } }).success(function (data, status, headers, config) {

                $scope.ClubName = data.ClubName;
                $scope.Date = data.Date;
                $scope.ProviderName = data.ProviderName;
                $scope.SelectedClubName = data.SelectedClubName;
                $scope.SeasonName = data.SeasonName;
                dataInfo = data;
                prepareGrid();
            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });


            var prepareGrid = function () {
                $scope.ReconciliationPrepReportGridOptions = {

                    excel: {
                        allPages: true,
                        fileName: dataInfo.ClubName + " (Reconciliation Prep).xlsx",
                        proxyURL: "/Dashboard/PortalReport/Save"
                    },
                    excelExport: function (e) {

                        var rows = e.workbook.sheets[0].rows;                      

                        rows.unshift({
                            cells: [{
                                value: ""
                            }]
                        });
                        rows.unshift({
                            cells: [{
                                value: dataInfo.Date
                            }]
                        });
                        rows.unshift({
                            cells: [{
                                value: dataInfo.SelectedClubName + " - " + dataInfo.SeasonName
                            }]
                        });
                        rows.unshift({
                            cells: [{
                                value: "Reconciliation Prep - " + dataInfo.ClubName
                            }]
                        });

                        cfpLoadingBar.complete();
                    },
                    dataSource: {
                        type: "json",
                        transport: {
                            read: {
                                url: baseUrl + '/GetReconciliationPrepReportInfo?seasonId=' + seasonId + '&schoolId=' + schoolId,
                                dataType: "json",
                                data: function () {
                                    return {
                                        seasonId: seasonId,
                                        schoolId: schoolId,
                                        printMode: $scope.filter.printMode
                                    };
                                },
                            }
                        },
                        schema: {
                            total: "TotalCount",
                            data: "DataSource"
                        },
                        batch: true,
                        serverSorting: false,
                        resizable: true,
                    },
                    scrollable: true,
                    sortable: true,
                    resizable: true,
                    dataBound: function (e) {
                        customReportService.initializeDoubleScroll(e);
                        $scope.oldColumns = $("#ReconciliationPrepReportGrid").data("kendoGrid").columns;
                    },
                    editable: false,

                    columns: [

                           {
                               field: "ProviderName",
                               title: "Provider name",
                               width: "260px",
                           },
                           {
                               field: "ClassName",
                               title: "Class name",
                               width: "200px",
                           },
                            {
                                field: "GradeRange",
                                title: "Grade range",
                                width: "120px",

                            },
                           {
                               field: "ClassFee",
                               title: "Class fee ($)",
                               width: "130px",
                           },
                            {
                                field: "EMRatestr",
                                title: "EM rate",
                                width: "80px",

                            },

                    ]
                };
            }

            $scope.saveToExcel = function () {
                cfpLoadingBar.start();
                $("#ReconciliationPrepReportGrid").getKendoGrid().saveAsExcel();
            }

            $scope.saveToPDF = function () {
                $scope.filter.printMode = true;
                $("#ReconciliationPrepReportGrid").data("kendoGrid").dataSource.read();

            }
            $scope.print = function () {
                $scope.filter.printMode = true;
                $("#ReconciliationPrepReportGrid").data("kendoGrid").dataSource.read();
            }
        }
    ]);


})();