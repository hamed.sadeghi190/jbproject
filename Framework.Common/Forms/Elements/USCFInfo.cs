﻿using Jb.Framework.Common.Forms.Validation;
using Jumbula.Common.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;

namespace Jb.Framework.Common.Forms
{
    public class USCFInfo : JbBaseElement, ICustomElement
    {
        const string ValueFormat = "ID:{0},Rating Quick:{1},Regular Rating:{2},Expiration Date:{3}";
        public USCFInfo()
        {
            TypeTitle = "USCF Info";
            ElementId = this.GetType().Name + StaticRandom.Instance.Next(1, int.MaxValue);
            //Debug.Print("Element ID " + ElementId);
            AccessRole = new Dictionary<string, AccessMode>();
            Validations = new List<BaseElementValidator>();
        }

        [NotMapped]
        [Display(Name = "Name")]
        public string PlayerName { get; set; }


        [Display(Name = "USCF ID")]
        public string UscfId { get; set; }

        [Display(Name = "USCF expiration date")]
        public string UscfExpiration { get; set; }

        [Display(Name = "USCF regular rating (approximate ok)")]
        public string UscfRatingReg { get; set; }

        [Display(Name = "USCF quick rating")]
        public string UscfRatingQuick { get; set; }

        public bool IsRequired { get; set; }

        public override object GetValue()
        {
            if (UscfId != null || UscfRatingQuick != null || UscfRatingReg != null || UscfExpiration != null)
                return string.Format(ValueFormat, UscfId, UscfRatingQuick, UscfRatingReg, UscfExpiration);
            else
                return null;
        }

        public override void SetValue(object value)
        {
            if (value == null)
                return;

            string pattern = "^" + Regex.Replace(ValueFormat, @"\{[0-9]+\}", "(.*?)") + "$";
            Regex r = new Regex(pattern);
            Match m = r.Match(value.ToString());
            UscfId = m.Groups[0].ToString();
            UscfRatingQuick = m.Groups[1].ToString();
            UscfRatingReg = m.Groups[2].ToString();
            UscfExpiration = m.Groups[3].ToString();
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public override ElementSize Size { get; set; }

    }
}