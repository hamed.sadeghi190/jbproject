﻿using Jb.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class StudentAttendanceBusiness : IStudentAttendanceBusiness
    {
        private readonly IClubBusiness _clubBusiness;
        private readonly IOrderSessionBusiness _orderSessionBusiness;
        private readonly IRepository<StudentAttendance> _studentAttendanceRepository;
        private readonly IEntitiesContext _dataContext;

        public StudentAttendanceBusiness(IClubBusiness clubBusiness, IOrderSessionBusiness orderSessionBusiness, IRepository<StudentAttendance> studentAttendanceRepository, IEntitiesContext dataContext)
        {
            _clubBusiness = clubBusiness;
            _orderSessionBusiness = orderSessionBusiness;
            _studentAttendanceRepository = studentAttendanceRepository;
            _dataContext = dataContext;
        }

        public OperationStatus Create(StudentAttendance studentAttendance)
        {
            return _studentAttendanceRepository.Create(studentAttendance);
        }

        public  StudentAttendance Get(int id)
        {
            return _studentAttendanceRepository.Get(id);
        }

        public IQueryable<StudentAttendance> GetList(Expression<Func<StudentAttendance, bool>> predicate)
        {
            return _studentAttendanceRepository.GetList(predicate);
        }

        public IQueryable<StudentAttendance> GetList()
        {
            return _studentAttendanceRepository.GetList();
        }

        public IQueryable<StudentAttendance> GetList(List<long> orderSessionId)
        {
            return _studentAttendanceRepository.GetList(c => orderSessionId.Contains(c.OrderSessionId));
        }

        public OperationStatus Update(StudentAttendance studentAttendance)
        {
            return _studentAttendanceRepository.Update(studentAttendance);
        }

        public OperationStatus UpdateAttendances(List<StudentAttendance> attendances)
        {
            OperationStatus result = new OperationStatus { Status = false };

            var clubBusiness = _clubBusiness;

            var orderSessionIds = attendances.Select(a => a.OrderSessionId);
            var orderSessions = _orderSessionBusiness.GetList().Where(o => orderSessionIds.Contains(o.Id));

            var attendanceIdsForEdit = _dataContext.Set<StudentAttendance>().Where(s => orderSessionIds.Contains(s.OrderSessionId)).ToList();

            foreach (var item in attendanceIdsForEdit)
            {
                var attendance = attendances.Single(o => o.OrderSessionId == item.OrderSessionId);

                var clubId = orderSessions.SingleOrDefault(o => o.Id == item.OrderSessionId).OrderItem.ProgramSchedule.Program.ClubId;
                item.AttendanceDate = clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow);
                item.AttendanceStatus = attendance.AttendanceStatus;
                item.AttendanceStaffId = attendance.AttendanceStaffId;
            }

            var attendancesForAdd = attendances.Where(s => !attendanceIdsForEdit.Select(a => a.OrderSessionId).Contains(s.OrderSessionId)).ToList();
            foreach (var item in attendancesForAdd)
            {
                var clubId = orderSessions.SingleOrDefault(o => o.Id == item.OrderSessionId).OrderItem.ProgramSchedule.Program.ClubId;
                item.AttendanceDate = clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow);
                _dataContext.Set<StudentAttendance>().Add(item);
            }
            
            result.Status = _dataContext.SaveChanges() > 0;

            return result;
        }

        public OperationStatus UpdateDismissal(List<StudentAttendance> attendances)
        {
            OperationStatus result = new OperationStatus { Status = false };

            var clubBusiness = _clubBusiness;

            var orderSessionIds = attendances.Select(a => a.OrderSessionId);
            var orderSessions = _orderSessionBusiness.GetList().Where(o => orderSessionIds.Contains(o.Id));

            var attendanceIdsForEdit = _dataContext.Set<StudentAttendance>().Where(s => orderSessionIds.Contains(s.OrderSessionId)).ToList();

            foreach (var item in attendanceIdsForEdit)
            {
                var attendance = attendances.Single(o => o.OrderSessionId == item.OrderSessionId);

                if (attendance.IsDismissed)
                {
                    var clubId = orderSessions.SingleOrDefault(o => o.Id == item.OrderSessionId).OrderItem.ProgramSchedule.Program.ClubId;
                    item.DismissalDate = clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow);
                    item.IsDismissed = attendance.IsDismissed;
                    item.DismissalInfoSerialized = attendance.DismissalInfoSerialized;
                    item.DismissalStaffId = attendance.DismissalStaffId;
                }
                else
                {
                    item.DismissalDate = null;
                    item.IsDismissed = attendance.IsDismissed;
                    item.DismissalInfoSerialized = null;
                    item.DismissalStaffId = null;
                }
            }

            var attendancesForAdd = attendances.Where(s => !attendanceIdsForEdit.Select(a => a.OrderSessionId).Contains(s.OrderSessionId)).ToList();
            foreach (var item in attendancesForAdd)
            {
                var clubId = orderSessions.SingleOrDefault(o => o.Id == item.OrderSessionId).OrderItem.ProgramSchedule.Program.ClubId;
                item.DismissalDate = clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow);
                _dataContext.Set<StudentAttendance>().Add(item);
            }

            result.Status = _dataContext.SaveChanges() > 0;

            return result;
        }

        public AttendanceStatus? GetStudentStatus(int profileId, long scheduleId, int sessionId)
        {
            var query = _dataContext.Set<StudentAttendance>().SingleOrDefault(s => s.OrderSession.OrderItem.PlayerId == profileId && s.OrderSession.OrderItem.ProgramScheduleId.Value == scheduleId && s.OrderSession.ProgramSessionId == sessionId);
            var status = query != null ? query.AttendanceStatus : null;

            return status;
        }
    }
}
