ALTER TABLE [dbo].[ClubStaffs] ADD  DEFAULT ((0)) FOR [UserRoleId]

ALTER TABLE [dbo].[ClubStaffs] ADD  DEFAULT ((0)) FOR [UserRoleId]
GO

ALTER TABLE [dbo].[ClubStaffs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClubStaffs_dbo.Clubs_ClubId] FOREIGN KEY([ClubId])
REFERENCES [dbo].[Clubs] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ClubStaffs] CHECK CONSTRAINT [FK_dbo.ClubStaffs_dbo.Clubs_ClubId]
GO

ALTER TABLE [dbo].[ClubStaffs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClubStaffs_dbo.ContactPersons_ContactId] FOREIGN KEY([ContactId])
REFERENCES [dbo].[ContactPersons] ([Id])
GO

ALTER TABLE [dbo].[ClubStaffs] CHECK CONSTRAINT [FK_dbo.ClubStaffs_dbo.ContactPersons_ContactId]
GO

ALTER TABLE [dbo].[ClubStaffs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClubStaffs_dbo.JbUserRoles_UserRoleId] FOREIGN KEY([UserRoleId])
REFERENCES [dbo].[JbUserRoles] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ClubStaffs] CHECK CONSTRAINT [FK_dbo.ClubStaffs_dbo.JbUserRoles_UserRoleId]
GO



ALTER TABLE [dbo].[ClubStaffs] add [UserRoleId] [int] NOT NULL
Go

----------------------------

alter table clubstaffs drop column userid
Go
alter table clubstaffs drop [PK_dbo.ClubStaffs]
Go
alter table clubstaffs drop [FK_dbo.ClubAdmins_dbo.UserProfile_UserId]
Go


