﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.CreditCard;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.UserService;
using Newtonsoft.Json.Linq;
using SportsClub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.WebLogic.Payment
{
    public abstract class JbBasePaymentGateway
    {

        public JbBasePaymentGateway(PaymentGateway paymentGateway)
        {
            _paymentGateway = paymentGateway;
        }

        protected PaymentGateway _paymentGateway;
        protected Client _client;
        protected List<OrderInstallment> installments = null;
        protected decimal payableAmount;
        protected Dictionary<string, string> metadata = null;
        protected Core.Domain.Club club = null;
        protected string username = HttpContext.Current.User.Identity.IsCurrentUserExist() ? JbUserService.GetCurrentUserName() : string.Empty;
        protected int userId = 0;
        protected bool isOrderTestMode = false;
        protected Order order = null;
        protected ParentBillingMethodsViewModel modelParentBilling = new ParentBillingMethodsViewModel();
        protected UserCreditCard CreditCardExistWithCustomer = null;
        protected bool newDefault = false;
        protected bool isUpdate = false;
        protected List<TransactionActivity> transactions = null;
        protected UserCreditCard creditCard = null;
        protected PaymentResponseModel resultResponse = new PaymentResponseModel();
        protected string ipn;
        protected CurrencyCodes currency;
        protected decimal refundAmount;
        protected Random rnd = new Random();
        protected void SetOrderandInvoice(PayViewModel model)
        {

            var creditCards = Ioc.UserCreditCardBusiness;
            var userCreditCard = creditCards.GetUserCreditCards(JbUserService.GetCurrentUserId(), PaymentGateway.Stripe);

            var ipn = model.CreateTokenResponse;
            Dictionary<string, string> metadata = null;
            var username = HttpContext.Current.User.Identity.IsCurrentUserExist() ? JbUserService.GetCurrentUserName() : string.Empty;
            Order order = null;
            var isOrderTestMode = false;
            Club club = null;
            JbPaymentService paymentService = new JbPaymentService();

            decimal payableAmount = model.PayableAmount;

            List<TransactionActivity> transactions = null;

            var userId = 0;
            switch (model.ActionCaller)
            {
                case JumbulaSubSystem.Order:
                    {
                        order = Ioc.OrderBusiness.GetOrdersByConfirmationID(model.ConfirmationId);
                        username = (!string.IsNullOrEmpty(username)) ? username : order.User.UserName;
                        metadata = paymentService.GenerateMetaData(JumbulaSubSystem.Order, order);
                        payableAmount = Ioc.OrderBusiness.CalculateOrderPayableAmount(order);
                        club = Ioc.ClubBusiness.Get(order.ClubId);
                        userId = order.UserId;
                        isOrderTestMode = !order.IsLive;
                        break;
                    }
                //add actionCaller for invoice
                case JumbulaSubSystem.Invoice:
                    {
                        var paymentDetaiId = Ioc.PaymentDetailBusiness.GetByInvoiceId(model.InvoiceId.Value).Id;
                        var tokenForInvoice = Ioc.TransactionActivityBusiness.GetList().Where(t => t.PaymentDetailId == paymentDetaiId).FirstOrDefault().Token;
                        transactions = Ioc.TransactionActivityBusiness.GetInvoicedByToken(tokenForInvoice).ToList(); ;
                        payableAmount = model.PayableAmount;
                        metadata = paymentService.GenerateMetaData(JumbulaSubSystem.Invoice, null, null, transactions);
                        club = Ioc.ClubBusiness.Get(transactions.First().ClubId);
                        username = (!string.IsNullOrEmpty(username)) ? username : transactions.First().Order.User.UserName;
                        userId = transactions.First().Order.UserId;
                        isOrderTestMode = !(transactions.First().OrderId.HasValue ? transactions.First().Order.IsLive : transactions.First().OrderItem.Order.IsLive);
                        break;
                    }
            }
        }

        protected bool isCreditCardExist(PayViewModel model, PaymentGateway typePayment, int userId)
        {

            List<UserCreditCard> creditCardAll = null;
            if (model.PaymentMethod == PaymentMethod.Card)
            {
                creditCardAll = Ioc.UserCreditCardBusiness.GetCreditCards(model.CardNumber, userId, typePayment).ToList();
                if (creditCardAll != null && creditCardAll.Count > 0)
                {
                    return true;
                }
                else { return false; }

            }
            return false;
        }
        protected int GetCommission(long clientId, decimal amount)
        {
            var client = Ioc.ClientBusiness.GetById(clientId);
            int commission = 0;
            if (client.PricePlan.TransactionFee > 0)
            {
                commission = (int)(((amount * client.PricePlan.TransactionFee) / 100) * 100);
            }
            return commission;
        }


        protected int GetCountActive(IQueryable<UserCreditCard> userCreditCard)
        {
            var countActive = 0;

            foreach (var item in userCreditCard)
            {
                if (item.IsDefault)
                {
                    countActive++;
                }
            }
            return countActive;
        }

        public static string GetBrandCard(string cardNumber)
        {
            var firstNumber = cardNumber.Substring(0, 1);
            var brand = "";
            if (firstNumber == "3")
            {
                brand = "Amex";
            }
            else if (firstNumber == "4")
            {
                brand = "Visa";
            }
            else if (firstNumber == "5")
            {
                brand = "MasterCard";
            }
            else if (firstNumber == "6")
            {
                brand = "DiscoverCard";
            }
            return brand;
        }


        public static List<string> SplitName(string Name)
        {
            //var names = Name.Split(' ');
            string[] names = Name.ToString().Trim().Split(new char[] { ' ' }, 2);
            var listNames = new List<string>();

            if (names.Length > 1) { listNames.Add(names[0]); listNames.Add(names[1]); } else { listNames.Add(Name); listNames.Add(Name); }
            return listNames;
        }

        public string GetCardId(UserCreditCard selectedCreditCard)
        {
            string cardId = string.Empty;

            if (string.IsNullOrEmpty(selectedCreditCard.CardId))
            {
                var json = selectedCreditCard.CustomerResponse;

                if (json != null)
                {
                    dynamic data = JObject.Parse(json);

                    switch (_paymentGateway)
                    {
                        case PaymentGateway.Stripe:
                            {
                                cardId = data.DefaultSourceId;
                            }
                            break;
                        case PaymentGateway.AuthorizeNet:
                            {
                                cardId = data.customerProfileId;
                            }
                            break;
                        default:
                            {
                                throw new NotSupportedException("We didn't support this type of payment.");
                            }
                    }


                }
            }
            else
            {
                cardId = selectedCreditCard.CardId;
            }

            return cardId;
        }

        protected PaymentModels GetPaymentModelsForSelectedCard(UserCreditCard creditCard)
        {
            return new PaymentModels()
            {
                CardNumber = creditCard.LastDigits,
                Month = creditCard.ExpiryMonth,
                Year = creditCard.ExpiryYear,
                CardHolderName = creditCard.Name,
                Address = creditCard.Address,
                UserId = creditCard.UserId,
            };
        }

        protected PaymentModels GetPaymentModels(PayViewModel model)
        {
            return new PaymentModels()
            {
                CardNumber = model.CardNumber,
                Month = model.Month,
                Year = (model.Year2Char + 2000).ToString(),
                CardHolderName = model.CardHolderName,
                Address = model.Address,
                StrAddress = model.Address != null ? model.Address.Address : string.Empty,
                CVV = model.CVV,
                UserId = model.UserId,
            };
        }
        public void SetParameters(PayViewModel model)
        {
            payableAmount = model.PayableAmount;
            JbPaymentService paymentService = new JbPaymentService();

            switch (model.ActionCaller)
            {
                case JumbulaSubSystem.Installment:
                    {
                        installments = Ioc.InstallmentBusiness.GetListByToken(model.ConfirmationId).ToList();
                        payableAmount = installments.Sum(c => c.Balance);
                        metadata = paymentService.GenerateMetaData(JumbulaSubSystem.Installment, null, installments);
                        club = Ioc.ClubBusiness.Get(installments.First().OrderItem.Order.ClubId);
                        username = (!string.IsNullOrEmpty(username)) ? username : installments.First().OrderItem.Order.User.UserName;
                        userId = installments.First().OrderItem.Order.UserId;
                        isOrderTestMode = !installments.First().OrderItem.Order.IsLive;
                        break;
                    }
                case JumbulaSubSystem.Order:
                    {
                        order = Ioc.OrderBusiness.GetOrdersByConfirmationID(model.ConfirmationId);
                        username = (!string.IsNullOrEmpty(username)) ? username : order.User.UserName;
                        metadata = paymentService.GenerateMetaData(JumbulaSubSystem.Order, order);
                        payableAmount = Ioc.OrderBusiness.CalculateOrderPayableAmount(order);
                        club = Ioc.ClubBusiness.Get(order.ClubId);
                        userId = order.UserId;
                        isOrderTestMode = !order.IsLive;
                        break;
                    }
                case JumbulaSubSystem.Invoice:
                    {
                        if (model.InvoiceId.HasValue)
                        {
                            var paymentDetaiId = Ioc.PaymentDetailBusiness.GetByInvoiceId(model.InvoiceId.Value).Id;
                            var tokenForInvoice = Ioc.TransactionActivityBusiness.GetList().Where(t => t.PaymentDetailId == paymentDetaiId).FirstOrDefault().Token;
                            transactions = Ioc.TransactionActivityBusiness.GetInvoicedByToken(tokenForInvoice).ToList();
                        }
                        else { transactions = Ioc.TransactionActivityBusiness.GetInvoicedByToken(model.ConfirmationId).ToList(); model.InvoiceId = transactions.First().PaymentDetail.InvoiceId; }
                        payableAmount = model.PayableAmount;
                        metadata = paymentService.GenerateMetaData(JumbulaSubSystem.Invoice, null, null, transactions);
                        club = Ioc.ClubBusiness.Get(transactions.First().ClubId);
                        username = (!string.IsNullOrEmpty(username)) ? username : transactions.First().Order.User.UserName;
                        userId = transactions.First().Order.UserId;
                        isOrderTestMode = !(transactions.First().OrderId.HasValue ? transactions.First().Order.IsLive : transactions.First().OrderItem.Order.IsLive);
                        break;
                    }
                case JumbulaSubSystem.MakeaPayment:
                    {
                        transactions = Ioc.TransactionActivityBusiness.GetDraftByToken(model.ConfirmationId).ToList();
                        var transaction = transactions != null && transactions.Any() ? transactions.FirstOrDefault() : null;
                        order = transaction != null ? transaction.Order : null;
                        payableAmount = transactions.Sum(c => c.Amount);
                        metadata = paymentService.GenerateMetaData(JumbulaSubSystem.MakeaPayment, null, null, transactions);
                        club = Ioc.ClubBusiness.Get(transactions.First().ClubId);
                        username = (!string.IsNullOrEmpty(username)) ? username : transactions.First().Order.User.UserName;
                        userId = transactions.First().Order.UserId;
                        isOrderTestMode = !(transactions.First().OrderId.HasValue ? transactions.First().Order.IsLive : transactions.First().OrderItem.Order.IsLive);
                        break;
                    }
                default:
                    {
                        throw new NotSupportedException(string.Format(Constants.F_ActionCallerNotSupported, model.ActionCaller));
                    }
            }
        }

        protected ParentBillingMethodsViewModel GetModelParentBilling(PayViewModel model)
        {
            return new ParentBillingMethodsViewModel()
            {
                CardHolderName = model.CardHolderName,
                Month = model.Month,
                Year = (model.Year2Char + 2000).ToString(),
                Address = model.Address != null ? model.Address.Address : string.Empty,
                IsDefault = model.IsDefault,
                Last4Digit = model.CardNumber.Substring((model.CardNumber.Length) - 4, 4),
                CardNumber = model.CardNumber,
                Year2Char = model.Year2Char,
                PostalAddress = model.Address,
                CVV = model.CVV
            };

        }

        protected void SpecifyDefault(int countActive, PayViewModel model, int countInstallment)
        {
            var isSubscription = false;
            var isAutocharge = false;
            var orderItems = new List<OrderItem>();

            if (model.ActionCaller == JumbulaSubSystem.MakeaPayment)
            {
                orderItems = order.OrderItems.ToList();
                isAutocharge = order.IsAutoCharge;
            }
            else
            {
                orderItems = model.Cart.Order.OrderItems.Where(o => o.Order.IsAutoCharge).ToList();
            }

            foreach (var item in orderItems)
            {
                countInstallment += item.Installments.Count();
                if (item.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                {
                    isSubscription = true;
                }
            }

            if (countActive >= 1 && model.IsDefault == false)//Is Preferrd in express payment method and checkbox not checked
                newDefault = false;
            else if (countActive >= 1 && model.IsDefault == true)
            {
                newDefault = true;
                isUpdate = true; //for remove preferrd previous card
            }
            else if (countActive < 1 && model.IsDefault == true)
            {
                newDefault = true;
            }
            else if ((countActive < 1 && model.IsDefault == false && countInstallment > 0) || isSubscription)//in installment
            {
                if (model.ActionCaller == JumbulaSubSystem.MakeaPayment && !isAutocharge)
                {
                    newDefault = false;
                    model.IsDefault = false;
                }
                else
                {
                    newDefault = true;
                    model.IsDefault = true;
                }

            }
        }

        protected void SetCreditCardForUpdate(PayViewModel model)
        {
            var address = model.Address;

            creditCard.LastDigits = model.CardNumber.Substring((model.CardNumber.Length) - 4, 4);
            creditCard.ExpiryYear = (model.Year2Char + 2000).ToString();
            creditCard.ExpiryMonth = model.Month.Length == 1 ? ("0" + model.Month) : model.Month;
            creditCard.Address.City = address.City;
            creditCard.Address.Street = address.Street;
            creditCard.Address.Country = address.Country;
            creditCard.Address.Address = address.Address;
            creditCard.Address.StreetNumber = address.StreetNumber;
            creditCard.Address.AutoCompletedAddress = address.AutoCompletedAddress;
            creditCard.Address.State = address.State;
            creditCard.Address.Route = address.Route;
            creditCard.Address.Lng = address.Lng;
            creditCard.Address.Lat = address.Lat;
            creditCard.Address.Zip = address.Zip;
            creditCard.Address.TimeZone = address.TimeZone;
            creditCard.Name = model.CardHolderName;
            creditCard.IsDefault = model.IsDefault;
            creditCard.UserId = userId;

            if (order.IsAutoCharge && model.ActionCaller == JumbulaSubSystem.MakeaPayment)
            {
                creditCard.TypeCreditCard = UserCreditCardType.InstallmentOfflineRegistration;
            }
        }

        protected void FailurePayment(PayViewModel model)
        {
            string error = "";

            foreach (var item in resultResponse.cardErrors)
            {

                if (item.Key.Contains("errorex") || item.Key == "error")
                {

                    error += item.Value + "/";
                }
            }
            if (!string.IsNullOrEmpty(resultResponse.errorMsg))
            {
                error = resultResponse.errorMsg;
            }
            var transactionId = Ioc.PaymentDetailBusiness.GetTransactionIdFromResponse(ipn);
            var paymentDetail = new PaymentDetail(username, PaymentDetailStatus.ERROR, model.Token, PaymentMethod.Card, currency, ipn, error, transactionId, JbUserService.GetCurrentUserRoleType(), null, model.InvoiceId);

            var orderdb = Ioc.OrderBusiness;
            switch (model.ActionCaller)
            {
                case JumbulaSubSystem.Order:

                    orderdb.SetPaymentTransaction(order, paymentDetail, TransactionStatus.Failure);
                    orderdb.Update(order);

                    break;
                case JumbulaSubSystem.Installment:

                    foreach (var item in installments)
                    {
                        orderdb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Failure, item, DateTime.UtcNow);
                        Ioc.InstallmentBusiness.Update(item);
                    }

                    break;
                case JumbulaSubSystem.Invoice:

                    foreach (var item in transactions)
                    {
                        if (item.Installment == null)
                        {
                            orderdb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Failure, null, DateTime.UtcNow, item.Amount, item.OrderItem, null, "", model.ConfirmationId, "", TransactionCategory.Invoice);
                        }
                        else
                        {
                            orderdb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Failure, item.Installment, DateTime.UtcNow, item.Amount, null, null, "", model.ConfirmationId, "", TransactionCategory.Invoice);
                        }
                    }

                    Ioc.OrderItemBusiness.Update(transactions.First().OrderItem);
                    break;
                case JumbulaSubSystem.MakeaPayment:

                    foreach (var item in transactions)
                    {
                        if (item.Installment == null)
                        {
                            orderdb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Failure, null, null, item.Amount, item.OrderItem, null, "", "", "", TransactionCategory.TakePayment);
                        }
                        else
                        {
                            orderdb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Failure, item.Installment, null, item.Amount, null, null, "", "", "", TransactionCategory.TakePayment);
                        }
                    }

                    Ioc.OrderItemBusiness.Update(transactions.First().OrderItem);
                    break;

            }

        }

        protected void ErrorHandle(Exception ex, ControllerContext controllerContext, JumbulaSubSystem actionCaller)
        {
            resultResponse.errorMsg = "You have successfully charged the user, but the balance may not reflect the correct amount. Do not charge the user again. Please contact Jumbula at support@jumbula.com and report this issue.";

            if (ex != null)
            {
                if (ex.InnerException == null)
                {
                    resultResponse.cardErrors.Add("errorex", ex.Message);
                }
                else
                {
                    resultResponse.cardErrors.Add("errorex", ex.InnerException.Message);
                }
            }
            if (WebConfigHelper.IsProductionMode)
            {
                var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, club.Name);
                string emailSubject = string.Concat("Error in update", " ", actionCaller, " ", club.Name);

                string emailBody = ViewHelper.RenderViewToString(controllerContext, @"~/Views/Shared/Email/ErrorInTakePayment.cshtml", null);
                Ioc.OldEmailBusiness.SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, emailBody, true);
            }
        }


    }
}