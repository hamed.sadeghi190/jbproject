﻿using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IEmailTemplateBusiness
    {
        EmailTemplate Get(int id);
        IQueryable<EmailTemplate> GetClubTemplates(int clubId);
        OperationStatus UpdateTemplate(EmailTemplate template);
        OperationStatus CreateTemplate(EmailTemplate template);
        OperationStatus SaveTemplate(EmailTemplateModel template, int clubId, int? templateId);
        OperationStatus DeleteTemplate(int templateId);
        EmailTemplate GetDefaultClubTemplate();
        string UploadImageTemplate(string image, string clubDomain, string folderName);
        EmailTemplateModel GetEmailTemplate(int? templateId);
    }
}
