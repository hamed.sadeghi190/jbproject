﻿using Newtonsoft.Json;
using System;
using Stripe.Infrastructure;
using System.Runtime.CompilerServices;

namespace SportsClub.Models
{
    public class StripeWebhookEvent
    {
        public StripeWebhookEvent()
        {
        }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("created")]
        public DateTime? Created { get; set; }
        [JsonProperty("data")]
        public StripeWebhookEventData Data { get; set; }
        [JsonProperty("livemode")]
        public bool LiveMode { get; set; }
        [JsonProperty("pending_webhooks")]
        public int PendingWebhooks { get; set; }
        [JsonProperty("request")]
        public string Request { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("user_id")]
        public string UserId { get; set; }
    }


    public class StripeWebhookEventData
    {
        public StripeWebhookEventData() { }

       
        [JsonProperty("object")]
        public dynamic Object { get; set; }
       
        [JsonProperty("previous_attributes")]
        public dynamic PreviousAttributes { get; set; }
    }

}