﻿using Jb.Framework.Common;
using Jb.Framework.Common.Forms;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IReportBusiness
    {
        EventRoaster GetReport(int reportId);

        IQueryable<EventRoaster> GetReports(string clubDomain, long seasonId);

        IQueryable<EventRoaster> GetReports(string clubDomain);

        OperationStatus Create(EventRoaster eventRoaster);

        OperationStatus Update(EventRoaster eventRoaster);

        OperationStatus Delete(int reportId);

        List<JbForm> GetFolloupFormReports(int clubId);

        JbForm GenerateReportFollowupForm(JbForm jbForm);
        EventRoaster GetClubRosterReport(string clubDomain, long seasonId); 
        void setDefaultColumn(JbForm jbForm);
        OperationStatus CopyCustomReportToSeason(int sourceReportId, List<long> destinationSeasonIds);
    }
}
