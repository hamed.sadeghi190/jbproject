﻿
using System.ComponentModel;


namespace Jumbula.Common.Enums
{
    public enum ChargeDiscountCategory : short
    {

        [Description("No condition")]
        AllProgramAllParticipant = -110,
        [Description("Same participant")]
        AllProgramSameParticipant = -190,
        [Description("All program and different participant")]
        AllProgramDiffParticipant = -120,
        [Description("Same program")]
        SameProgramAllParticipant = -130,
        [Description("Same program and same participant")]
        SameProgramSameParticipant = -140,
        [Description("Same program and different participant")]
        SameProgramDiffParticipant = -150,
        [Description("Different program")]
        DiffProgramAllParticipant = -160,
        [Description("Different program and same participant")]
        DiffProgramSameParticipant = -170,
        [Description("Different program and differnt participant")]
        DiffProgramDiffParticipant = -180,
        [Description("Custom")]
        CustomDiscount = -5,
        [Description("Paid-in-full discount")]
        FullPaySubscription = -6,
        [Description("Multi Person")]
        MultiPerson = -10,
        [Description("Multi Program")]
        MultiProgram = -15,
        [Description("Multi Schedule")]
        MultiSchedule = -20,

        [Description("Coupon")]
        Coupon = -100,

        //-----charges
        [Description("Entry Fee")]
        EntryFee = 5,
        [Description("Custom charge")]
        CustomCharge = 10,
        [Description("Lunch")]
        Lunch = 15,
        [Description("Day care")]
        DayCare = 20,
        Playup = 30,
        Reentry = 35,
        GmIm = 40,
        Econ = 45,
        UscfMem = 50,
        [Description("Payment plan fee")]
        InstallmentFee = 55,
        //-----
        Donation = 100,
        [Description("Convenience fee")]
        Convenience = 101,
        [Description("Late registration charge")]
        LateRegistrationFee = 120,
        [Description("Surcharge")]
        Surcharge = 130,
        [Description("Partner surcharge")]
        PartnerSurcharge = 140,
        [Description("Application fee")]
        ApplicationFee = 150,
        [Description("Partner application fee")]
        PartnerApplicationFee = 160,
        [Description("Late pick up fee")]
        LatePickupFee = 170,
        [Description("Cancellation fee")]
        CancellationFee = 180,
        [Description("Edit fee")]
        EditFee = 190,
        [Description("Transfer fee")]
        TransferFee = 200,
        [Description("Prorate fee")]
        ProrateFee = 210,
    }
}
