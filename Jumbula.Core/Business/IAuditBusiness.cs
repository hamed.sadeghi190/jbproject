﻿using Jumbula.Common.Utilities;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.ActivityLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Jumbula.Core.Business
{
    public interface IAuditBusiness
    {
        void Add(JbAudit audit);
        JbAudit Get(Guid id);
        IQueryable<JbAudit> GetClubAudits(string clubdomain);
        IQueryable<JbAudit> GetList();
        IQueryable<JbAudit> GetList(string zone);
        IQueryable<JbAudit> GetParentAudits(string clubdomain, int parentId, string activity = null, string userName = null);
        void SetResult(Guid id, int? statusCode = null, string description = null, string ownerKey = null);
        void SetSuccess(Guid id, int statusCode, string description = null, string ownerKey = null);
        void SetDescription(Guid id, string description);
        string GetDevice(HttpRequestBase request);

        PaginatedList<ActivityLogItemViewModel> GetAdminAudits(string clubdomain, ActivityLogFilterViewModel filters,
            List<Sort> sorts, int pageIndex, int pageSize);

        ActivityLogFilterViewModel GetFilters();

        ActivityLogDetailViewModel GetDetail(Guid id);
    }
}
