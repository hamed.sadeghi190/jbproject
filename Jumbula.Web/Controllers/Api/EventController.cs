﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using SportsClub.Models.Api;

namespace Jumbula.Web.Controllers.Api
{
    public class CourseController : ApiController
    {
        public List<ProgramInfoServiceModel> Get(string clubDomain, string seasonDomain)
        {
            var club = Ioc.ClubBusiness.Get(clubDomain);

            return Ioc.ProgramBusiness.GetList(club.Id, seasonDomain, string.Empty, null, null, SaveType.Publish, ProgramTypeCategory.Class).ToList().Select(c => 
            new ProgramInfoServiceModel()
            {
                id = c.Id,
                title = c.Name,
                start = c.ProgramSchedules.Min(i => i.StartDate),
                end = c.ProgramSchedules.Max(i => i.EndDate),
                domain = c.Domain,
                seasonDomain = seasonDomain,
                clubDomain = c.ClubDomain,
                location = c.ClubLocation.PostalAddress.Address
            })
            .ToList();

        }
    }

    public class CampController : ApiController
    {
        public List<ProgramInfoServiceModel> Get(string clubDomain, string seasonDomain)
        {
            var clubId = Ioc.ClubBusiness.Get(clubDomain).Id;

            return Ioc.ProgramBusiness.GetList(clubId, seasonDomain, string.Empty, null, null, SaveType.Publish, ProgramTypeCategory.Camp).Where(p => p.Status != ProgramStatus.Frozen).Select(c => 
            new ProgramInfoServiceModel()
            {
                id = c.Id,
                title = c.Name,
                start = c.ProgramSchedules.Where(d=>d.IsDeleted==false) .Min(i => i.StartDate),
                end = c.ProgramSchedules.Where(d => d.IsDeleted == false).Max(i => i.EndDate),
                domain = c.Domain,
                seasonDomain = seasonDomain,
                clubDomain = clubDomain,
                location = c.ClubLocation.PostalAddress.Address
            })
            .ToList();
        }
    }
}