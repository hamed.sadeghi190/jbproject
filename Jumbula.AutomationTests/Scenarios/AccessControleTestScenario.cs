﻿using Jumbula.AutomationTests.Tests;
using NUnit.Framework;

namespace Jumbula.AutomationTests.Scenarios
{
    public class AccessControleTestScenario : BaseTestScenario
    {
        [Test]
        public void TestMethod()
        {
            base.Browser = Browser.Chrome;
            AccessControlTests accessControlTests = new AccessControlTests(WebDriver);
            //accessControlTests.PartnerStaffs_LoginInMembers();
            //accessControlTests.DistrictStaffs_LoginInMembers();
            accessControlTests.PartnerStaffs_LoginInPartner();
        }
    }
}
