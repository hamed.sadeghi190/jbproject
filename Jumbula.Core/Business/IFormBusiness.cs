﻿using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using System;
using System.Collections.Generic;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Business
{
    public interface IFormBusiness
    {
        JbForm GetDefaultRegistrationForm(bool loadForFirstTimeGenerate = false);

        JbForm GetDefaultSchoolRegistrationForm(bool loadForFirstTimeGenerate = false);

        string GetPlayerHomeRoom(JbForm jbForm);

        string GetPlayerSchoolName(JbForm jbForm);

        bool? GetPermissionPhotography(JbForm jbForm);

        JbForm GetDefaultTeamTournamnetRegistrationForm();

        SchoolGradeType? GetPlayerGrade(JbForm jbForm);

        List<IJbBaseElement> GetFormElements(JbForm jbForm, string elementName);

        string GetPlayerGradeAsString(JbForm jbForm, bool showAbbreviationGrade = false);

        string GetPlayerFirstName(JbForm jbForm);

        string GetPlayerLastName(JbForm jbForm);

        JbAddress GetPlayerAddress(JbForm jbForm);
        Genders? GetPlayerGender(JbForm jbForm);
        string GetEmergencyContactFirstName(JbForm jbForm);
        string GetEmergencyContactLastName(JbForm jbForm);

        object GetPlayerGradeObject(JbForm jbForm);

        JbAddress GetParentAddress(JbForm jbForm);

        string GetParentPhone(JbForm jbForm);

        string GetFormValue(JbForm jbForm, SectionsName sectionName, ElementsName elementName, List<string> alternativeElementNames = null);

        string GetFormValue(JbForm jbForm, SectionsName sectionName, string elementName, bool checkContins = false);

        string GetFormValue(JbForm jbForm, SectionsName sectionName, List<string> elementNames);
        string GetFormValue(JbForm jbForm, string sectionName, string elementName, bool checkContins = false);

        string GetFormValue(JbForm jbForm, string elementName);

        string GetTeacher(JbForm jbForm);

        List<JbElementItem> GetTeacherItems(JbForm jbform);
        string GetAuthorizedAdult(JbForm jbForm, SectionsName? sectionName);

        string GetAuthorizedAdultPhone(JbForm jbForm, SectionsName? sectionName);

        List<string> GetAuthorizedAdultNames(JbForm jbForm);

        List<string> GetAuthorizedAdultPhones(JbForm jbForm);

        void RemoveHiddenItems(JbForm jbForm);
        string GetPlayerEmail(JbForm jbForm);
        string GetParentEmail(JbForm jbForm);
        string GetParent2Email(JbForm jbForm);
        string GetParentFirstName(JbForm jbForm);
        string GetParentLastName(JbForm jbForm);
        string GetParent2FirstName(JbForm jbForm);
        string GetParent2LastName(JbForm jbForm);
        Genders? GetParent2Gender(JbForm jbForm);
        GenderCategories GetParent2GenderNew(JbForm jbForm);
        ParentRelationship? GetParent2RelationshipEnum(JbForm jbForm);
        ParentRelationship? GetParent1RelationshipEnum(JbForm jbForm);
        string GetParent2RelationShip(JbForm jbForm);
        string GetPlayerDoctorPhone(JbForm jbForm);
        string GetPlayerDoctorName(JbForm jbForm);
        string GetPlayerDoctorLocation(JbForm jbForm);
        bool? GetPlayerNutAllergy(JbForm jbForm);
        string GetPlayerSelfAdministerMedicationDescription(JbForm jbForm);
        bool? GetPlayerSelfAdministerMedication(JbForm jbForm);
        Genders? GetParentGender(JbForm jbForm);
        DateTime? GetParentDoB(JbForm jbForm);
        DateTime? GetParent2DoB(JbForm jbForm);
        DateTime? GetParticipantDoB(JbForm jbForm);
        string GetParentCellPhone(JbForm jbForm);
        string GetParent2CellPhone(JbForm jbForm);
        string GetParentOccupation(JbForm jbForm);
        string GetParent2Occupation(JbForm jbForm);
        string GetParentEmployer(JbForm jbForm);
        string GetParent2Employer(JbForm jbForm);

        GenderCategories GetParent1Gender(JbForm jbForm);

        string GetParent1AlternatePhone(JbForm jbForm);

        JbAddress GetParent1Address(JbForm jbForm);
        string GetParent2AlternatePhone(JbForm jbForm);
        JbAddress GetParent2Address(JbForm jbForm);
        bool GetAuthorizePickup1IsEmergencyContact(JbForm jbForm);
        bool GetAuthorizePickup2IsEmergencyContact(JbForm jbForm);
        bool GetAuthorizePickup3IsEmergencyContact(JbForm jbForm);
        bool GetAuthorizePickup4IsEmergencyContact(JbForm jbForm);
        string GetEmergencyContactPrimeryPhone(JbForm jbForm);
        string GetEmergencyContactAlternatePhone(JbForm jbForm);
        string GetEmergencyContactWorkPhone(JbForm jbForm);
        Relationship? GetEmergencyContactRelationShip(JbForm jbForm);
        string GetInsuranceCompanyName(JbForm jbForm);
        string GetInsuranceCompanyPhone(JbForm jbForm);
        string GetInsurancePolicyNumber(JbForm jbForm);
        string GetElementNameAttr(JbForm jbForm, SectionsName sectionName, ElementsName elementName);

        int GetElementIndexInSection(JbSection section, JbBaseElement element);

        void SetContactFullName(JbForm jbForm, PlayerProfile player);

        JbSection GetSection(JbForm jbForm, SectionsName sectionName);

        JbSection GetSection(JbForm jbForm, string sectionName);

        JbBaseElement GetElement(JbSection jbSection, ElementsName elementName);

        JbBaseElement GetElement(JbSection jbSection, string elementName);

        List<ElementsName> GetDefaultFormElements();

        List<JbBaseElement> GetFormElements(JbForm jbForm);

        void AddRequiredToGender(JbForm jbForm);
        void AddRequiredToGrade(JbForm jbForm);

        void AddRequiredToDob(JbForm jbForm);

        #region Flex
        string GetDismissalFromEnrichment(JbForm jbForm);
        string GetAllergies(JbForm jbForm);
        string GetMedicalConditions(JbForm jbForm);
        string GetParent1Name(JbForm jbForm);
        string GetAuthorizedPickup1Name(JbForm jbForm);
        string GetEmergencyContactName(JbForm jbForm);
        string GetParent1Phone(JbForm jbForm);
        string GetAuthorizedPickup1Phone(JbForm jbForm);
        string GetEmergencyContactPhone(JbForm jbForm);
        string GetParent1Email(JbForm jbForm);
        string GetAuthorizedPickup1Email(JbForm jbForm);
        string GetAuthorizedPickup2Email(JbForm jbForm);
        string GetAuthorizedPickup3Email(JbForm jbForm);
        string GetEmergencyContactEmail(JbForm jbForm);
        string GetPlayerGradeOrAgeAsString(JbForm jbForm);
        string GetAuthorizedPickup2Name(JbForm jbForm);
        string GetAuthorizedPickup2Phone(JbForm jbForm);
        string GetAuthuraizePikeUp1RelationShip(JbForm jbForm);
        string GetAuthuraizePikeUp2RelationShip(JbForm jbForm);
        string GetEmergency1RelationShip(JbForm jbForm);
        string GetEmergency2RelationShip(JbForm jbForm);
        string GetEmergencyContact2Phone(JbForm jbForm);
        string GetEmergencyContact2Name(JbForm jbForm);
        string GetParent1RelationShip(JbForm jbForm);
        string Parent1HomePhone(JbForm jbForm);
        string Parent2HomePhone(JbForm jbForm);
        string GetParent2Name(JbForm jbForm);
        string GetDeitary(JbForm jbForm);
        string GetOtherInformation(JbForm jbForm);
        string GetMedication(JbForm jbForm);
        #endregion

        #region
        string GetApolloTeacher(JbForm jbForm);

        #endregion

        void SetElementEmpty(IJbBaseElement element);

        void SetParticipantFieldsReadonly(JbForm jbForm);

        GenderCategories GetParticipantGender(JbForm jbForm);
        string GetPlayerAllergies(JbForm jbForm);
        string GetPlayerMedicalConditions(JbForm jbForm);
        string GetAuthorizePickup1FirstName(JbForm jbForm);
        string GetAuthorizePickup1LastName(JbForm jbForm);
        string GetAuthorizePickup1PrimaryPhone(JbForm jbForm);
        string GetAuthorizePickup1AlternatePhone(JbForm jbForm);
        Relationship? GetAuthorizePickup1Relationship(JbForm jbForm);
        string GetAuthorizePickup2FirstName(JbForm jbForm);
        string GetAuthorizePickup2LastName(JbForm jbForm);
        string GetAuthorizePickup2PrimaryPhone(JbForm jbForm);
        string GetAuthorizePickup2AlternatePhone(JbForm jbForm);
        Relationship? GetAuthorizePickup2Relationship(JbForm jbForm);
        string GetAuthorizePickup3FirstName(JbForm jbForm);
        string GetAuthorizePickup3LastName(JbForm jbForm);
        string GetAuthorizePickup3PrimaryPhone(JbForm jbForm);
        string GetAuthorizePickup3AlternatePhone(JbForm jbForm);
        Relationship? GetAuthorizePickup3Relationship(JbForm jbForm);
        string GetAuthorizePickup4FirstName(JbForm jbForm);
        string GetAuthorizePickup4LastName(JbForm jbForm);
        string GetAuthorizePickup4PrimaryPhone(JbForm jbForm);
        string GetAuthorizePickup4AlternatePhone(JbForm jbForm);
        Relationship? GetAuthorizePickup4Relationship(JbForm jbForm);
        string GetParent2Phone(JbForm jbForm);

        string GetPlayerNutAllergyDescription(JbForm jbForm);

        bool? GetPlayerHasAllergeis(JbForm jbForm);

        bool? GetPlayerHasSpecialNeeds(JbForm jbForm);

        string GetMorningProgramArrival(JbForm jbForm);
    }
}
