﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{

    public class EventRoaster : BaseEntity
    {

        [Required]
        public string Name { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        //[Required]
        //public string Events { get; set; }

        [Required]
        public DateTime LastUpdateDate { get; set; }

        [Required]
        [StringLength(128)]
        public string ClubDomain { get; set; } // must unique within club domain

        public bool HasHeader { get; set; }
        //public EventRoasterType Type { get; set; }

        public long? SeasonId { get; set; }
        public virtual Season Season { get; set; }

        public int? JbForm_Id { get; set; }
        [ForeignKey("JbForm_Id")]
        public virtual JbForm JbForm { get; set; }

        public virtual ICollection<JbForm> FollowupForms{ get; set; }
        public EventRoasterType EventType { get; set; }
        public CustomReportType CustomType { get; set; }
    }

    public enum EventRoasterType : byte
    {
        Custom,
        Roster
    }

    public enum CustomReportType : byte
    {
        Admin,
        Parent
    }
}
