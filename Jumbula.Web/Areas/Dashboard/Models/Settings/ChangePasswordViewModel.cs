﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ChangePasswordViewModel
    {
        public int SelectedStaff { get; set; }
        public IEnumerable<SelectKeyValue<int>> ClubStaffs { get; set; }


        public string Username { get; set; }
        [Display(Name = "Current password")]
        [Required(ErrorMessage = "{0} is required.")]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

       
        [Display(Name = "New password")]
        [Required(ErrorMessage = "{0} is required.")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Display(Name = "Confirm password")]
        [Required(ErrorMessage = "{0} is required.")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string ClubName { get; set; }
        public int ClubId { get; set; }

    }
}