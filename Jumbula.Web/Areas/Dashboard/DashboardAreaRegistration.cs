﻿using System.Web.Mvc;

namespace Jumbula.Web.Areas.Dashboard
{
    public class DashboardAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Dashboard";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Dashboard_View",
                url: "Dashboard/{controller}/GetView/{viewName}",
                defaults: new { controller = "Home", action = "GetView", viewName = "" },
                namespaces: new string[] { "Jumbula.Web.Areas.Dashboard.Controllers" }
            );

            context.MapRoute(
                name: "Dashboard_Home",
                url: "Dashboard/{action}",
                defaults: new { controller = "Home", action = "Index" },
                namespaces: new string[] { "Jumbula.Web.Areas.Dashboard.Controllers" }
            );

            context.MapRoute(
                name: "Dashboard_Default",
                url: "Dashboard/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "Jumbula.Web.Areas.Dashboard.Controllers" }
            );

            context.MapRoute(
              name: "Dashboard_Season",
              url: "Dashboard/season/{action}/{id}",
              defaults: new { controller = "Season", action = "Index", id = UrlParameter.Optional },
               namespaces: new string[] { "Jumbula.Web.Areas.Dashboard.Controllers" }
          );
        }
    }
}