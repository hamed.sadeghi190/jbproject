﻿

namespace Jumbula.Common.Enums
{
    public enum PaymentPlanStatus
    {
        Started = 0,
        Stopped = 1,
    }
}
