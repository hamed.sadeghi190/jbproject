
GO

/****** Object:  Table [dbo].[JbFieldSettings]    Script Date: 11/14/2017 10:14:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[JbFieldSettings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FieldName] [nvarchar](max) NULL,
	[Class] [int] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[ShowInGrid] [bit] NOT NULL,
	[ClubId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.JbFieldSettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[JbFieldSettings]  WITH CHECK ADD  CONSTRAINT [FK_dbo.JbFieldSettings_dbo.Clubs_ClubId] FOREIGN KEY([ClubId])
REFERENCES [dbo].[Clubs] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[JbFieldSettings] CHECK CONSTRAINT [FK_dbo.JbFieldSettings_dbo.Clubs_ClubId]
GO


