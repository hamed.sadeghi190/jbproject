﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('addStaffFromPartnerController', [
		'$scope', '$http', 'jbToast', '$state', '$stateParams', function ($scope, $http, jbToast, $state, $stateParams) {
		    window.app.globalObjects.pageClass = 'staff-manage-view';
		    window.app.globalObjects.initializeView();

		    $scope.MC.stateParams = $stateParams;



		    $http.get('Dashboard/Staff/GetPartnerStaffs')
                .success(function (data, status, headers, config) {
                    $scope.Model = data;
                });



		    $scope.add = function () {

		        $http.post('Dashboard/Staff/AddPartnerStaff', { model: $scope.Model })
					.success(function (data, status, headers, config) {

					    if (data.Status) {

					        $scope.errors = null;

					        jbToast.success("Staff added successfully.", "");

					        $state.go('ManageStaffs');
					    } else {
					        $scope.MC.handleErrors($scope, data);
					    }
					});
		    }


		}
    ]);

})();