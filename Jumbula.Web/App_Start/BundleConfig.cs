﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.Optimization;

namespace Jumbula.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = true;

            bundles.UseCdn = true;

            #region Common
                #region Utilities
                    #region Mask
                    bundles.Add(new ScriptBundle("~/bundles/Scripts/Common/Utilities/Mask/js")
                               .Include("~/Scripts/Libraries/jquery.mask.min.js")
                               .Include("~/Scripts/Common/Utilities/Mask/use-jquery-mask.js"));
                    #endregion
                #endregion
            #endregion

            #region Parent
            bundles.Add(new ScriptBundle("~/bundles/FormWizard")
                            .Include("~/Scripts/FormWizard/jquery.form.js"));

            #region RegisterPage

            bundles.Add(new StyleBundle("~/bundles/registerPageStyles")
                .Include("~/Content/Student/Register/Registration.css"));

            bundles.Add(new ScriptBundle("~/bundles/registerPageScripts")
                .Include("~/Scripts/Libraries/countUp.min.js")
                .Include("~/Scripts/Google/googlemaps.js"));

            bundles.Add(new ScriptBundle("~/bundles/register")
                .Include("~/Scripts/Student/Register.js"));
            #endregion

            #region ProgramPage

            bundles.Add(new StyleBundle("~/bundles/programPageStyles")
                .Include("~/Content/Student/Program/Display.css")
                .Include("~/Content/Frameworks/toastr.css")
                .Include("~/Content/Frameworks/Owl/owl.carousel.css"));

            bundles.Add(new ScriptBundle("~/bundles/programPageScripts")
                .Include("~/Scripts/Libraries/jQuery/jQueryUi/jquery-ui-1.10.4.min.js")
                .Include("~/Scripts/Google/googlemaps.js")
                .Include("~/Scripts/Libraries/toastr.js")
                .Include("~/Scripts/Libraries/owl.carousel.min.js"));
            #endregion

            #region Other
            bundles.Add(new StyleBundle("~/bundles/payStyles")
                .Include("~/Content/Student/Pay/Pay.css"));
            #endregion

            #region Cart

            bundles.Add(new StyleBundle("~/bundles/CartStyles")
                .Include("~/Content/Student/Cart/Cart.css")
                .Include("~/Content/Frameworks/MetroUi/metro-bootstrap.min.css")
                .Include("~/Content/Frameworks/toastr.css")
                .Include("~/Content/Student/Stripe/Stripe.css")
                .Include("~/Content/jquery.countdown.css")
            );

            bundles.Add(new ScriptBundle("~/bundles/CartScripts")
                    .Include("~/Scripts/Libraries/jquery.mask.min.js")
                    .Include("~/Scripts/Libraries/countUp.min.js")
                    .Include("~/Scripts/Libraries/toastr.js")
                //.Include("~/Scripts/Libraries/jQuery/jquery.plugin.min.js")
                //.Include("~/Scripts/Libraries/jQuery/jquery.countdown.min.js")
            );
            #endregion

            #endregion

            #region Bootstrap
            bundles.Add(new ScriptBundle("~/bundles/alert")
                            .Include("~/Scripts/Bootstraper/alert.js"));

            bundles.Add(new ScriptBundle("~/bundles/button")
                            .Include("~/Scripts/Bootstraper/button.js"));

            bundles.Add(new ScriptBundle("~/bundles/dropdown")
                            .Include("~/Scripts/Bootstraper/dropdown.js"));

            bundles.Add(new ScriptBundle("~/bundles/modal")
                            .Include("~/Scripts/Bootstraper/modal.js"));

            bundles.Add(new ScriptBundle("~/bundles/transition")
                            .Include("~/Scripts/Bootstraper/transition.js"));
            #endregion

            #region Google
            bundles.Add(new ScriptBundle("~/resource/bundles/googlemaps")
                            .Include("~/Scripts/Google/googlemaps.js"));

            bundles.Add(new ScriptBundle("~/bundles/infobox")
                            .Include("~/Scripts/Google/infobox.js"));

            bundles.Add(new ScriptBundle("~/bundles/markerwithlabel")
                            .Include("~/Scripts/Google/markerwithlabel.js"));
            #endregion

            #region Slider
            bundles.Add(new ScriptBundle("~/resource/bundles/slider")
                            .Include("~/Scripts/Slider/wowslider.js",
                                     "~/Scripts/Slider/slider.js"));
            #endregion

            #region Jquery

            bundles.Add(new ScriptBundle("~/bundles/jqueryValidateUnobtrusive")
                            .Include("~/Scripts/Libraries/jQuery/jquery.validate.min.js")
                            .Include("~/Scripts/Libraries/jQuery/jquery.validate.unobtrusive.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jQuery")
                  .Include("~/Scripts/Libraries/jQuery/jquery-2.1.3.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/unobtrusive-ajax")
                            .Include("~/Scripts/Libraries/jQuery/jquery.unobtrusive-ajax.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/validate")
                            .Include("~/Scripts/Libraries/jQuery/jquery.validate.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/validate-unobtrusive")
                            .Include("~/Scripts/Libraries/jQuery/jquery.validate.unobtrusive.min.js"));

            #endregion

            #region Elements
            bundles.Add(new ScriptBundle("~/bundles/placeholder_shim_v2")
                            .Include("~/Scripts/placeholder_shim_v2.js"));

            bundles.Add(new ScriptBundle("~/bundles/popover")
                            .Include("~/Scripts/popover.js"));

            bundles.Add(new ScriptBundle("~/bundles/searchBoxAutoComplate")
                            .Include("~/Scripts/searchBoxAutoComplate.js"));

            bundles.Add(new ScriptBundle("~/bundles/showLoading")
                            .Include("~/Scripts/showLoading.js"));

            bundles.Add(new ScriptBundle("~/bundles/socialnetworkSDK")
                            .Include("~/Scripts/socialnetworkSDK.js"));

            bundles.Add(new ScriptBundle("~/resource/bundles/tooltip")
                            .Include("~/Scripts/tooltip.js"));

            bundles.Add(new ScriptBundle("~/bundles/icheck")
                            .Include("~/Scripts/icheck.js"));


            bundles.Add(new ScriptBundle("~/bundles/jsignature")
                            .Include("~/Scripts/JSSignature/jSignature.min.js",
                                     "~/Scripts/JSSignature/jSignature.min.noconflict.js"));
            #endregion

            #region Kendo
            bundles.Add(new StyleBundle("~/bundles/EditorStyle")
                            .Include("~/Content/Frameworks/KendoUI/kendo.default.min.css"
                                   , "~/Content/Frameworks/KendoUI/kendo.common.min.css"
                                   , "~/Content/Frameworks/KendoUI/kendo.common-bootstrap.min.css"));
            #endregion

            #region Global
            bundles.Add(new StyleBundle("~/bundles/globalStyle")
                            .Include("~/Content/Frameworks/Bootstrap/bootstrap.css"
                                     , "~/Content/Frameworks/Bootstrap/bootstrap-theme.css"
                                     , "~/Content/Frameworks/Bootstrap/bootstrap-custom.css"
                                     , "~/Content/Frameworks/toastr.css"
                                     , "~/Content/Student/parent-common.css"
                                     , "~/Content/Shared/Style.css"
                                     , "~/Content/Student/Style.css"
                                     , "~/Content/Shared/jumbula-icons.css"));


            bundles.Add(new ScriptBundle("~/bundles/globalScript")
                            .Include("~/Scripts/Libraries/Bootstrap/bootstrap.min.js"
                                    , "~/Scripts/Shared/is.js"
                                    , "~/Scripts/Libraries/toastr.js"
                                    , "~/Scripts/Libraries/eModal.js"
                                    , "~/Scripts/Libraries/jQuery/jquery.blockUI.min.js"));
            #endregion

            #region Jbpage styles
            bundles.Add(new StyleBundle("~/bundles/JbPageStyle")
                              .Include("~/Areas/Dashboard/Content/loading-bar.css"
                                      , "~/Content/Frameworks/KendoUI/kendo.common.min.css"
                                      , "~/Content/Frameworks/MetroUi/metro-bootstrap.min.css"
                                      , "~/Content/Frameworks/KendoUI/kendo.metro.min.css"
                                      , "~/Content/JbPage/Style.css"
                                      , "~/Content/Frameworks/KendoUI/kendo.common-bootstrap.min.css"
                                      , "~/Scripts/JbPage/Directives/Tooltips/ImageTooltip/ImageTooltip.css"));
            #endregion

            #region order Dashboard
            bundles.Add(new StyleBundle("~/bundles/orderStyles")
                .Include("~/Content/Student/Dashboard/Order.css"));

            #endregion

            #region JbPage script

            //jbpageLibraryScripts
            bundles.Add(new ScriptBundle("~/bundles/jbpageLibraryScripts")
                            .Include("~/areas/dashboard/scripts/0-third-party/loading-bar.js"
                                    , "~/scripts/libraries/ui-bootstrap-tpls-0.13.3.min.js"
                                    , "~/scripts/libraries/angularjs/angular-drag-and-drop-lists.js"
                                    , "~/areas/dashboard/scripts/6-utilities/dateformat.js"
                                    , "~/scripts/jbpage/angular-file-upload.min.js"));

            //angularMasonryScripts
            bundles.Add(new ScriptBundle("~/bundles/angularMasonryScripts")
                           .Include("~/scripts/jbpage/angular-masonry-packed.min.js"));

            //jbpageScripts
            bundles.Add(new ScriptBundle("~/bundles/jbpageScripts")
                             .Include("~/Scripts/JbPage/Season.js"));

            //jbpageDirectiveScripts
            bundles.Add(new ScriptBundle("~/bundles/jbpageDirectiveScripts")
                            .Include("~/Areas/Dashboard/Scripts/3-Directives/angular-img-cropper.min.js"
                              , "~/Scripts/JbPage/Directives/Tooltips/ImageTooltip/ImageTooltip.js"));
            //jbpageModuleScripts
            bundles.Add(new ScriptBundle("~/bundles/jbpageModuleScripts")
                            .IncludeDirectory("~/Scripts/JbPage/Modules","*.js", true));
            #endregion

            #region Dashboard scripts

            //jqueryScripts
            bundles.Add(new ScriptBundle("~/bundles/jqueryScripts")
                            .Include("~/Scripts/Libraries/jQuery/jquery-2.1.3.min.js"
                                    , "~/Scripts/Libraries/jQuery/jQueryUi/jquery-ui-1.10.4.min.js"));

            //signalRScripts
            bundles.Add(new ScriptBundle("~/bundles/signalRScripts")
                            .Include("~/Scripts/Libraries/jQuery/jquery.signalR-2.2.1.min.js"));

            //dashboardLibraryScripts
            bundles.Add(new ScriptBundle("~/bundles/dashboardLibraryScripts")
                            .Include("~/Scripts/Libraries/countUp.min.js"
                                    , "~/Scripts/Shared/is.js"));

            //angularJsScripts
            bundles.Add(new ScriptBundle("~/bundles/angularJsScripts")
                           .Include("~/Scripts/Libraries/AngularJS/angular.js"
                                   , "~/Scripts/Libraries/AngularJS/angular-sanitize.js"
                                   , "~/Scripts/Libraries/AngularJS/angular-animate.min.js"));

            //metroUiScripts
            bundles.Add(new ScriptBundle("~/bundles/metroUiScripts")
                .IncludeDirectory("~/Scripts/Libraries/MetroUi", "*.js", false));

            //dashboardScripts
            bundles.Add(new ScriptBundle("~/bundles/dashboardThirdPartyScripts")
                        .IncludeDirectory("~/Areas/Dashboard/Scripts/0-third-party", "*.js", false));

            bundles.Add(new ScriptBundle("~/bundles/dashboardAppScripts")
                        .IncludeDirectory("~/Areas/Dashboard/Scripts/1-App", "*.js", false));

            bundles.Add(new ScriptBundle("~/bundles/dashboardControllersScripts")
                        .IncludeDirectory("~/Areas/Dashboard/Scripts/2-Controllers", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/dashboardDirectivesScripts")
                         .IncludeDirectory("~/Areas/Dashboard/Scripts/3-Directives", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/dashboardFiltersScripts")
                         .IncludeDirectory("~/Areas/Dashboard/Scripts/4-Filters", "*.js", false));

            bundles.Add(new ScriptBundle("~/bundles/dashboardServicesScripts")
                        .IncludeDirectory("~/Areas/Dashboard/Scripts/5-Services", "*.js", false));

            bundles.Add(new ScriptBundle("~/bundles/dashboardUtilitiesScripts")
                        .IncludeDirectory("~/Areas/Dashboard/Scripts/6-Utilities", "*.js", false));

            //kenodUiScripts
            bundles.Add(new ScriptBundle("~/bundles/kenodUiScripts")
                .Include("~/Scripts/Libraries/KendoUI/kendo.all.min.js"));


            #endregion

            #region Dashboard styles

            //jumbulaIcons
            bundles.Add(new StyleBundle("~/bundles/jumbulaIcons")
                .Include("~/Content/Shared/jumbula-icons.css"));

            //sharedStyles
            bundles.Add(new StyleBundle("~/bundles/sharedStyles")
                .Include("~/Content/Shared/Style.css"));

            //kendoCommonStyles
            bundles.Add(new StyleBundle("~/bundles/kendoCommonStyles")
                .Include("~/Content/Frameworks/KendoUI/kendo.common-bootstrap.min.css"
                , "~/Content/Frameworks/KendoUI/kendo.common.min.css"));

            //metroUiStyles
            bundles.Add(new StyleBundle("~/bundles/metroUiStyles")
                .IncludeDirectory("~/Content/Frameworks/MetroUi", "*.css", false));

            //dashboardStyles
            bundles.Add(new StyleBundle("~/bundles/dashboardStyles")
                      .IncludeDirectory("~/Areas/Dashboard/Content", "*.css", false)
                      .Include("~/Areas/Dashboard/Content/flyer/FlyerStyle.css"));
            //directivesStyles
            bundles.Add(new StyleBundle("~/bundles/directivesStyles")
                .IncludeDirectory("~/Areas/Dashboard/Scripts/3-Directives/", "*.css", true));

            #endregion

            #region Parent Dashboard styles
            bundles.Add(new StyleBundle("~/bundles/ParentDashboardStyles")
               .Include("~/Content/Student/Dashboard/Dashboard.css")
            .Include("~/Content/Student/Dashboard/Scheduler.css"));

            #endregion



            bundles.Add(new CustomBundle("~/bundles/basedata"));

            // Clear all items from the ignore list to allow minified CSS and JavaScript files in debug mode
            bundles.IgnoreList.Clear();

            // Add back the default ignore list rules sans the ones which affect minified files and debug mode
            bundles.IgnoreList.Ignore("*.intellisense.js");
            bundles.IgnoreList.Ignore("*-vsdoc.js");
            bundles.IgnoreList.Ignore("*.debug.js", OptimizationMode.WhenEnabled);
            bundles.IgnoreList.Ignore("~/bundles/angularMasonryScripts", OptimizationMode.WhenEnabled);
        }
    }

    public class CustomBundle : ScriptBundle
    {
        public CustomBundle(string virtualPath)
            : base(virtualPath)
        {
            this.Builder = new CustomBuilder();
        }

        public CustomBundle(string virtualPath, string cdnPath) : base(virtualPath, cdnPath) { }
    }

    public class CustomBuilder : IBundleBuilder
    {
        public string BuildBundleContent(Bundle bundle, BundleContext context, IEnumerable<FileInfo> files)
        {
            var content = new StringBuilder();
            foreach (var fileInfo in files)
            {
                var parser = new Microsoft.Ajax.Utilities.JSParser(Read(fileInfo));

                parser.Settings.AddRenamePair("delete", "foodelete");
                parser.Settings.AddRenamePair("null", "foonull");
                parser.Settings.AddRenamePair("class", "fooclass");
                parser.Settings.AddRenamePair("catch", "foocatch");
                parser.Settings.AddRenamePair("finally", "foofinally");
                parser.Settings.AddRenamePair("continue", "foocontinue");
                parser.Settings.AddRenamePair("function", "foofunction");

                content.Append(parser.Parse(parser.Settings).ToCode());
                content.Append(";");
            }

            return content.ToString();
        }

        private string Read(FileInfo file)
        {
            using (var r = file.OpenText())
            {
                return r.ReadToEnd();
            }
        }
    }
}