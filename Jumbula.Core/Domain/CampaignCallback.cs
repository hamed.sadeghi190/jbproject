﻿using System;
using Jumbula.Common.Base;
using Newtonsoft.Json;

namespace Jumbula.Core.Domain
{
    public class CampaignCallback : BaseEntity
    {
        [JsonIgnore]
        public string JsonBody { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
