﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('multiScheduleDiscountController', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'discounts-view';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;

        $scope.baseUrl = window.location.origin + "/Dashboard/Discount";

        $scope.discountId = 0;
        if ($stateParams.discountId) {
            $scope.discountId = $stateParams.discountId;
        }

        $scope.discountCategory = "MultiSchedule";

        $scope.isCustomDiscount = false;
        $scope.nopLabel = "Number of users";
        $scope.pageTitle = "Multi person discount";
        $scope.discountTypeLabel = "user";
        $scope.isMultiPerson = false;

        $scope.campDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: {
                    url: window.location.origin + "/Dashboard/program/GetListForDropDown?seasonDomain=" + $scope.MC.stateParams.seasonDomain + "&type=Camp",
                    cache: false,
                    dataType: "json"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        return {
                            term: document.activeElement.value
                        }
                    }
                }
            }
        });

        $scope.getProgramsList = function () {
            if ($scope.Model && $scope.Model.ProgramsList) {
                return $scope.Model.ProgramsList;
            } else {
                return [];
            }
        }

        $http.get($scope.baseUrl + '/CreateEditDiscount?id=' + $scope.discountId + '&seasonDomain=' + $scope.MC.stateParams.seasonDomain + '&discountCategory=' + $scope.discountCategory + '&type=camp').success(function (data, status, headers, config) {
            $scope.Model = data;
            $scope.discountCategory = $scope.Model.CategoryDescription;

            $scope.nopLabel = "Number of schedules";
            $scope.pageTitle = "Multi schedule discount";
            $scope.discountTypeLabel = "schedule";
            $scope.applyitems = [
                { text: $scope.Model.ApplyItems[0].Text, value: $scope.Model.ApplyItems[0].Value },
                { text: $scope.Model.ApplyItems[1].Text.format($scope.Model.NOP.Text.toOrdinal()), value: $scope.Model.ApplyItems[1].Value },
                { text: $scope.Model.ApplyItems[2].Text.format($scope.Model.NOP.Text.toOrdinal()), value: $scope.Model.ApplyItems[2].Value }
            ];
            var nop = $scope.Model.NOP.Value;
            var selectedIndex = $scope.Model.ApplyType;
            $scope.applyType = $scope.applyitems[selectedIndex];
        });

        $scope.changeApplyType = function () {
            $scope.applyitems = [
              { text: $scope.Model.ApplyItems[0].Text, value: $scope.Model.ApplyItems[0].Value },
              { text: $scope.Model.ApplyItems[1].Text.format($scope.Model.NOP.Text.toOrdinal()), value: $scope.Model.ApplyItems[1].Value },
              { text: $scope.Model.ApplyItems[2].Text.format($scope.Model.NOP.Text.toOrdinal()), value: $scope.Model.ApplyItems[2].Value }
            ];
            $scope.applyType = $scope.applyitems[$scope.applyType.value];
        }

        $scope.campOptions = {
            placeholder: "Select camps...",
            dataTextField: "Name",
            dataValueField: "Id",
            autoBind: false,
            dataSource: $scope.campDataSource
        };

        $scope.saveDiscount = function () {
            $scope.Model.ApplyType = $scope.applyType.value;

            $http.post($scope.baseUrl + '/SaveDiscount', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $state.go('Discounts', { seasonDomain: $scope.MC.stateParams.seasonDomain })
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        };
    }]);
})();