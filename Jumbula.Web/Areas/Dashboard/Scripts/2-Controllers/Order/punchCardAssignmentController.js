﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('punchCardAssignmentController', ['$scope', '$http', '$log', '$stateParams', 'jbToast', '$state', 'jbConfirmation', function ($scope, $http, $log, $stateParams, jbToast, $state, jbConfirmation) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/Season", order: 1, menu: "SEASONS" });

        $scope.baseUrl = window.location.origin + "/Order";
        $scope.seasonName = $stateParams.seasonDomain;
        $scope.MC.stateParams = $stateParams;

        $scope.Model = {};

        var assignMode = "Assign";

        $scope.IsSelected = [];
        var selectedProgramSessions = [];

        var orderSessionId = null;

        var nowDateTime = angular.element(document.querySelector('#nowDateTime')).val();

        if ($stateParams.sessionId > 0) {
            assignMode = "Reassign";
            orderSessionId = $stateParams.sessionId;
        }

        var isReassignMode = assignMode == "Reassign";

        $scope.Model = {};

        $http.get($scope.baseUrl + '/GetPunchCardCalendarItems?orderItemId=' + $stateParams.orderItemId + '&orderSessionId=' + $stateParams.sessionId + '&loadCapacity=' + false).success(function (data, status, headers, config) {
            $scope.Model = data;

            for (var i = 0; i < $scope.Model.length; i++) {
                if ($scope.Model[i].IsSelected) {
                    $scope.IsSelected[$scope.Model[i].ProgramSessionId] = true;
                }
            }

            startSchedular();
        });

        var startSchedular = function () {

            $scope.schedulerOptions = {
                date: new Date(nowDateTime),
                startTime: new Date(nowDateTime),
                width: "1000px",
                editable: false,
                views: [
                    { type: "month", selected: true },
                    "month",
                ],
                timezone: "Etc/UTC",
                dataBound: function (e) {
                },
                dataSource: {
                    batch: true,
                    transport: {
                        read: {
                            url: window.location.origin + '/Dashboard/Order/GetPunchCardCalendarItems?orderItemId=' + $stateParams.orderItemId + '&orderSessionId=' + orderSessionId,
                            dataType: "json"
                        },
                        parameterMap: function (options, operation) {
                            if (operation !== "read" && options.models) {
                                return { models: kendo.stringify(options.models) };
                            }
                        },
                    },

                    schema: {
                        model: {
                            id: "taskId",
                            fields: {
                                taskId: { from: "TaskID", type: "number" },
                                title: { from: "Title", defaultValue: "No title", validation: { required: true } },
                                start: { type: "date", from: "Start" },
                                end: { type: "date", from: "End" },
                                startTimezone: { from: "StartTimezone" },
                                endTimezone: { from: "EndTimezone" },
                                description: { from: "Description" },
                                recurrenceId: { from: "RecurrenceID" },
                                recurrenceRule: { from: "RecurrenceRule" },
                                recurrenceException: { from: "RecurrenceException" },
                                ownerId: { from: "OwnerID", defaultValue: 1 },
                                isAllDay: false,
                                isAssigned: { from: "IsAssigned" },
                                isSelected: { from: "IsSelected" },
                                programSessionId: { from: "ProgramSessionId" },
                                date: { from: "Date" },
                                isFull: { from: "IsFull" }
                            }
                        }
                    },

                },
            };
        }

        $scope.selectSession = function (programSessionId, date, isFull) {

            if (isFull && $scope.IsSelected[programSessionId]) {
                jbConfirmation
                   .yes(function () {
                       manipulateSelectItem(programSessionId, date);
                   })
                    .no(function () {
                        $scope.IsSelected[programSessionId] = false;
                    })
                  .title('Capacity warning')
                  .message('Are you sure you want to assign to this full session?')
                  .confirm();
            }
            else {
                manipulateSelectItem(programSessionId, date);
            }
        }

        var manipulateSelectItem = function (programSessionId, date) {

            if ($scope.IsSelected[programSessionId]) {
                for (var i = 0; i < $scope.Model.length; i++) {
                    if ($scope.Model[i].Date != date) {
                        for (var j = 0; j < $scope.Model.length; j++) {
                            if ($scope.Model[j].ProgramSessionId != programSessionId && $scope.Model[j].Date != date) {
                                $scope.IsSelected[$scope.Model[j].ProgramSessionId] = false;
                            }
                        }
                        break;
                    }
                }
            }
        }

        $scope.save = function () {

            var selectedSessionIds = [];

            for (var i = 0; i < $scope.Model.length; i++) {
                if ($scope.IsSelected[$scope.Model[i].ProgramSessionId]) {
                    selectedSessionIds.push($scope.Model[i].ProgramSessionId);
                }
            }

            $scope.url = $scope.baseUrl + '/AssignPunchCardSession';
            $scope.postModel = { orderItemId: $stateParams.orderItemId, programSessionIds: selectedSessionIds };

            if (assignMode == "Assign") {
                $http.post($scope.baseUrl + '/CheckSessionsAreInFreezedSchedule', { orderItemId: $stateParams.orderItemId, programSessionIds: selectedSessionIds })
                    .success(function (data, status, headers, config) {
                        if (data.status) {
                            var message = "This session " + data.Sessions + " is on the freezed schedule. Are you sure you want to proceed with this action?" ;
                            jbConfirmation.message(message)
                                .yes(function () {
                                    if (selectedSessionIds.length > 0) {
                                        $http.post($scope.url, $scope.postModel)
                                            .success(function (data, status, headers, config) {

                                                if (data.Status) {
                                                    $state.go('OrderItem', { seasonDomain: $scope.MC.stateParams.seasonDomain, orderItemId: $stateParams.orderItemId })
                                                }
                                                else {
                                                    $scope.MC.handleErrors($scope, data);
                                                }
                                            })
                                    }
                                    else {
                                        jbToast.error('At least one session must be selected.');
                                    }
                                })
                                .confirm();
                        }
                        else {
                            if (selectedSessionIds.length > 0) {
                                $http.post($scope.url, $scope.postModel)
                                    .success(function (data, status, headers, config) {

                                        if (data.Status) {
                                            $state.go('OrderItem', { seasonDomain: $scope.MC.stateParams.seasonDomain, orderItemId: $stateParams.orderItemId })
                                        }
                                        else {
                                            $scope.MC.handleErrors($scope, data);
                                        }
                                    })
                            }
                            else {
                                jbToast.error('At least one session must be selected.');
                            }
                        }
                    });
            }

            if (assignMode == "Reassign") {
                $scope.url = $scope.baseUrl + '/ReassignPunchCardSession';
                $scope.postModel = { orderItemId: $stateParams.orderItemId, orderSessionId: $stateParams.sessionId, programSessionIds: selectedSessionIds };

            if (selectedSessionIds.length > 0) {
                $http.post($scope.url, $scope.postModel)
                    .success(function (data, status, headers, config) {

                        if (data.Status) {
                            $state.go('OrderItem', { seasonDomain: $scope.MC.stateParams.seasonDomain, orderItemId: $stateParams.orderItemId })
                        }
                        else {
                            $scope.MC.handleErrors($scope, data);
                        }
                    })
            }
            else {
                jbToast.error('At least one session must be selected.');
            }

        }

        }
    }]);
})();