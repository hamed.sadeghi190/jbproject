﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Jumbula.Common.Enums;


namespace SportsClub.Models.Home
{
    public class FreeTrialViewModel
    {
        [Required]
        [Display(Name = "Full name")]
        [DataMember(Name = "FULLNAME")]
        public string FullName { get; set; }

        

        [Required]
        [Display(Name = "Email")]
        [DataMember(Name = "EMAIL")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Phone")]
        [DataMember(Name = "PHONE")]
        public string Phone { get; set; }

        [Display(Name = "Organization name")]
        [DataMember(Name = "BIZNAME")]
        public string BizName { get; set; }

        [Display(Name = "Organization website")]
        [DataMember(Name = "BIZWEB")]
        public string BizWeb { get; set; }

        [Display(Name = "Annual revenue")]
        [DataMember(Name = "REVENUE")]
        public string Revenue { get; set; }

        public string Address { get; set; }

        public TimeZone TimeZone { get; set; }

        [Display(Name = "How you heared about us")]
        [DataMember(Name = "HOWHEAR")]
        public string HowHear { get; set; }

        [Display(Name = "Message")]
        [DataMember(Name = "MESSAGE")]
        public string Message { get; set; }
        [Display(Name = "Domain")]
        [DataMember(Name = "DOMAIN")]
        public string ClubDomain { get; set; }
        [Display(Name = "Organization website")]
        [DataMember(Name = "WEBSITE")]
        public string WebSite { get; set; }
    }

    public class EnterpriseViewModel
    {
        
        public string FullName { get; set; }


        public string Email { get; set; }


        public string Phone { get; set; }


        public string BizName { get; set; }


        public string BizWeb { get; set; }

        public string Revenue { get; set; }

        public string Address { get; set; }

        public TimeZone TimeZone { get; set; }

        public string Message { get; set; }
    }
    public class VerifyModel
    {
        public string PhoneNumber { get; set; }

        public int Code { get; set; }

        public VerificationMethod VerificationMethod { get; set; }
    }

    public enum VerificationMethod
    {
        Sms = 0,
        Phone = 1
    }
}