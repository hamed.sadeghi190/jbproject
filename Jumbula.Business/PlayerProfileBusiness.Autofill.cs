﻿
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public partial class PlayerProfileBusiness : IPlayerProfileBusiness
    {

        public void AutoFillProfileForm(JbForm jbForm, int userId, long programId, int? profileId)
        {
            var formBusiness = _formBusiness;

            var family = _familyBusiness.GetFamily(userId);
            var defaultFormElementNames = formBusiness.GetDefaultFormElements();
            var lastForm = _orderBusiness.GetLastOrderRegForm(userId, profileId, programId);

            if (!profileId.HasValue || profileId.Value == 0)
            {
                // Fill for new profile
                AutoFillProfileForm(jbForm, userId, family, null, lastForm, defaultFormElementNames);
            }
            else
            {
                // Fill for existing profile
                var profile = Get(profileId.Value);
                AutoFillProfileForm(jbForm, userId, family, profile, lastForm, defaultFormElementNames);
            }

            var defaultSections = new List<SectionsName>()
            {
                SectionsName.ParticipantSection, SectionsName.EmergencyContactSection,
                SectionsName.ParentGuardianSection, SectionsName.Parent2GuardianSection,
                SectionsName.AuthorizedPickup1, SectionsName.AuthorizedPickup2, SectionsName.AuthorizedPickup3, SectionsName.AuthorizedPickup4,
                SectionsName.SchoolSection, SectionsName.InsuranceSection, SectionsName.HealthSection,
                SectionsName.Miscellaneous, SectionsName.PhotographySection, SectionsName.ContactSection
            };

            if (lastForm != null)
            {
                if (profileId.HasValue)
                {
                    AutoFillNotDefaultEelements(jbForm, lastForm, true, null, null, defaultSections);
                }
            }
        }

        public void AutoFillProfileForm(JbForm jbForm, int userId, Family family, PlayerProfile profile, JbForm lastOrderForm, List<ElementsName> defaultFormElementNames)
        {
            var parents = GetList(userId).Where(u => u.Relationship == RelationshipType.Parent);
            var authorizedPickups = family != null && family.Contacts != null ? family.Contacts.Where(c => c.Type == FamilyContactType.AuthorizedPickup && c.Status == FamilyContactStatusType.Active) : null;

            var formBusiness = _formBusiness;

            var isDifferentPlayerMode = profile == null;

            // Participant section
            if (profile != null)
            {
                var participantSection = formBusiness.GetSection(jbForm, SectionsName.ParticipantSection);
                if (participantSection != null)
                {
                    var filledElements = AutoFillParticipantSection(participantSection, profile, defaultFormElementNames);
                    if (lastOrderForm != null)
                    {
                        AutoFillNotDefaultEelements(jbForm, lastOrderForm, isDifferentPlayerMode, SectionsName.ParticipantSection.ToString(), filledElements);
                    }
                }

                // Health section
                var healthSection = formBusiness.GetSection(jbForm, SectionsName.HealthSection);
                if (healthSection != null && profile.Info != null)
                {
                    var filledElements = AutoFillHealthSection(healthSection, profile.Info, defaultFormElementNames);

                    if (lastOrderForm != null && !isDifferentPlayerMode)
                    {
                        AutoFillNotDefaultEelements(jbForm, lastOrderForm, isDifferentPlayerMode, SectionsName.HealthSection.ToString(), filledElements);
                    }
                }

                // School section
                var schoolSection = formBusiness.GetSection(jbForm, SectionsName.SchoolSection);
                if (schoolSection != null && profile.Info != null)
                {
                    var filledElements = AutoFillSchoolSection(schoolSection, profile.Info, defaultFormElementNames);

                    if (lastOrderForm != null && !isDifferentPlayerMode)
                    {
                        AutoFillNotDefaultEelements(jbForm, lastOrderForm, isDifferentPlayerMode, SectionsName.SchoolSection.ToString(), filledElements);
                    }
                }

                var photographySection = formBusiness.GetSection(jbForm, SectionsName.PhotographySection);
                if (photographySection != null && profile.Info != null)
                {
                    var filledElements = AutoFillPhotographySection(photographySection, profile.Info, defaultFormElementNames);

                    if (lastOrderForm != null && !isDifferentPlayerMode)
                    {
                        AutoFillNotDefaultEelements(jbForm, lastOrderForm, isDifferentPlayerMode, SectionsName.PhotographySection.ToString(), filledElements);
                    }
                }
            }

            // Parent section
            var parentSection = formBusiness.GetSection(jbForm, SectionsName.ParentGuardianSection);
            if (parentSection != null)
            {
                var parent = parents.FirstOrDefault();
                var filledElements = AutoFillParentSection(parentSection, parent, defaultFormElementNames);
            }

            // Parent2 section
            var parent2Section = formBusiness.GetSection(jbForm, SectionsName.Parent2GuardianSection);
            if (parent2Section != null)
            {
                var parent2 = parents.OrderBy(p => p.Id).Skip(1).FirstOrDefault();
                var filledElements = AutoFillParentSection(parent2Section, parent2, defaultFormElementNames);
            }

            if (family != null)
            {
                // Emergency contact
                var emergencyContactSection = formBusiness.GetSection(jbForm, SectionsName.EmergencyContactSection);
                var emergencyContact = family != null ? family.Contacts.Where(c => c.Type == FamilyContactType.Emergency && c.Status == FamilyContactStatusType.Active) : null;
                if (emergencyContactSection != null && emergencyContact != null && emergencyContact.Any() && emergencyContact.FirstOrDefault().Contact != null)
                {
                    var filledElements = AutoFillFamilyContactSection(emergencyContactSection, emergencyContact.FirstOrDefault(), defaultFormElementNames);
                }

                // Insurance section
                var insuranceSection = formBusiness.GetSection(jbForm, SectionsName.InsuranceSection);
                if (insuranceSection != null)
                {
                    var filledElements = AutoFillInsuranceSection(insuranceSection, family, defaultFormElementNames);

                    if (lastOrderForm != null && !isDifferentPlayerMode)
                    {
                        AutoFillNotDefaultEelements(jbForm, lastOrderForm, isDifferentPlayerMode, SectionsName.InsuranceSection.ToString(), filledElements);
                    }
                }
            }

            // Miscellaneous section
            var miscellaneousSecttion = formBusiness.GetSection(jbForm, SectionsName.Miscellaneous);
            if (miscellaneousSecttion != null)
            {
                if (lastOrderForm != null)
                {
                    var howDidYouHearAboutUs = formBusiness.GetFormValue(lastOrderForm, SectionsName.Miscellaneous, ElementsName.HearAboutUs, null);

                    var filledElements = AutoFillMiscellaneousSection(miscellaneousSecttion, howDidYouHearAboutUs, defaultFormElementNames);

                    if (!isDifferentPlayerMode)
                    {
                        AutoFillNotDefaultEelements(jbForm, lastOrderForm, isDifferentPlayerMode, SectionsName.Miscellaneous.ToString(), filledElements);
                    }
                }
            }

            // Contact section
            var contactSecttion = formBusiness.GetSection(jbForm, SectionsName.ContactSection);
            if (contactSecttion != null)
            {
                if (lastOrderForm != null)
                {
                    PostalAddress address = null;

                    var addressElement = formBusiness.GetPlayerAddress(lastOrderForm);
                    if (addressElement != null)
                    {
                        address = new PostalAddress()
                        {
                            Street = addressElement.AddressLine1,
                            Street2 = addressElement.AddressLine2,
                            City = addressElement.City,
                            State = addressElement.State,
                            Country = addressElement.Country,
                            Zip = addressElement.Zip
                        };
                    }

                    var phoneNumber = formBusiness.GetFormValue(lastOrderForm, SectionsName.ContactSection, ElementsName.Phone);

                    var filledElements = AutoFillContactSection(contactSecttion, address, phoneNumber, defaultFormElementNames);

                    if (!isDifferentPlayerMode)
                    {
                        AutoFillNotDefaultEelements(jbForm, lastOrderForm, isDifferentPlayerMode, SectionsName.ContactSection.ToString(), filledElements);
                    }
                }
            }

            if (authorizedPickups != null && authorizedPickups.Any())
            {
                // Authorized Pickup 1 section
                var authorizedPickup1Secttion = formBusiness.GetSection(jbForm, SectionsName.AuthorizedPickup1);
                if (authorizedPickup1Secttion != null)
                {
                    var authorizedPickup1 = authorizedPickups.FirstOrDefault();
                    if (authorizedPickup1 != null)
                    {
                        var filledElements = AutoFillAuthorizedPickupSection(authorizedPickup1Secttion, authorizedPickup1, defaultFormElementNames);

                        //if (lastOrderForm != null)
                        //{
                        //    AutoFillNotDefaultEelements(jbForm, lastOrderForm, isDifferentPlayerMode, SectionsName.AuthorizedPickup1.ToString(), filledElements);
                        //}
                    }
                }

                // Authorized Pickup 2 section
                var authorizedPickup2Secttion = formBusiness.GetSection(jbForm, SectionsName.AuthorizedPickup2);
                if (authorizedPickup2Secttion != null)
                {
                    var authorizedPickup2 = authorizedPickups.OrderBy(o => o.Id).Skip(1).FirstOrDefault();
                    if (authorizedPickup2 != null)
                    {
                        var filledElements = AutoFillAuthorizedPickupSection(authorizedPickup2Secttion, authorizedPickup2, defaultFormElementNames);
                    }
                }

                // Authorized Pickup 3 section
                var authorizedPickup3Secttion = formBusiness.GetSection(jbForm, SectionsName.AuthorizedPickup3);
                if (authorizedPickup3Secttion != null)
                {
                    var authorizedPickup3 = authorizedPickups.Skip(2).OrderBy(o => o.Id).FirstOrDefault();
                    if (authorizedPickup3 != null)
                    {
                        var filledElements = AutoFillAuthorizedPickupSection(authorizedPickup3Secttion, authorizedPickup3, defaultFormElementNames);
                    }
                }

                // Authorized Pickup 4 section
                var authorizedPickup4Secttion = formBusiness.GetSection(jbForm, SectionsName.AuthorizedPickup4);
                if (authorizedPickup4Secttion != null)
                {
                    var authorizedPickup4 = authorizedPickups.Skip(3).OrderBy(o => o.Id).FirstOrDefault();
                    if (authorizedPickup4 != null)
                    {
                        var filledElements = AutoFillAuthorizedPickupSection(authorizedPickup4Secttion, authorizedPickup4, defaultFormElementNames);
                    }
                }

            }
        }

        private List<ElementsName> AutoFillParticipantSection(JbSection jbSection, PlayerProfile profile, List<ElementsName> defaultFormElementNames)
        {
            var result = new List<ElementsName>();

            var formBusiness = _formBusiness;

            var defaultSectionElements = jbSection.Elements.Where(e => defaultFormElementNames.Select(d => d.ToString()).Contains(e.Name));

            foreach (var formElement in defaultSectionElements)
            {
                var formElementName = EnumHelper.ParseEnum<ElementsName>(formElement.Name);

                string elementValue = null;

                switch (formElementName)
                {
                    case ElementsName.FirstName:
                        {
                            elementValue = profile.Contact.FirstName;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.LastName:
                        {
                            elementValue = profile.Contact.LastName;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.Gender:
                        {
                            elementValue = profile.Gender.ToDescription();
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.DoB:
                        {
                            elementValue = profile.Contact.DoB.ToString();
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.Grade:
                        {
                            // Grade should not fill
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.Email:
                        {
                            elementValue = profile.Contact.Email;
                            result.Add(formElementName);
                        }
                        break;
                    ////////////
                    case ElementsName.Age:
                        break;

                }

                if (elementValue != null)
                {
                    formElement.SetValue(elementValue);
                }
            }

            return result;
        }

        private List<ElementsName> AutoFillFamilyContactSection(JbSection jbSection, FamilyContact familyContact, List<ElementsName> defaultFormElementNames)
        {
            var result = new List<ElementsName>();
            var formBusiness = _formBusiness;

            var defaultSectionElements = jbSection.Elements.Where(e => defaultFormElementNames.Select(d => d.ToString()).Contains(e.Name));

            foreach (var formElement in defaultSectionElements)
            {
                var formElementName = EnumHelper.ParseEnum<ElementsName>(formElement.Name);
                string elementValue = null;

                switch (formElementName)
                {
                    case ElementsName.FirstName:
                        {
                            elementValue = familyContact.Contact.FirstName;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.LastName:
                        {
                            elementValue = familyContact.Contact.LastName;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.Email:
                        {
                            elementValue = familyContact.Contact.Email;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.EmailAddress:
                        {
                            elementValue = familyContact.Contact.Email;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.HomePhone:
                        {
                            elementValue = familyContact.Contact.Phone;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.WorkPhone:
                        {
                            elementValue = familyContact.Contact.Work;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.AlternatePhone:
                        {
                            elementValue = familyContact.Contact.Cell;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.Relationship:
                        {
                            elementValue = familyContact.Relationship.HasValue ? ((int)familyContact.Relationship).ToString() : "";
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.Phone:
                        {
                            elementValue = familyContact.Contact.Phone;
                            result.Add(formElementName);
                        }
                        break;
                }

                if (elementValue != null)
                {
                    formElement.SetValue(elementValue);
                }
            }

            return result;
        }

        private List<ElementsName> AutoFillParentSection(JbSection jbSection, PlayerProfile parent, List<ElementsName> defaultFormElementNames)
        {
            var result = new List<ElementsName>();
            if (parent != null)
            {
                var defaultSectionElements = jbSection.Elements.Where(e => defaultFormElementNames.Select(d => d.ToString()).Contains(e.Name));

                foreach (var formElement in defaultSectionElements)
                {
                    var formElementName = EnumHelper.ParseEnum<ElementsName>(formElement.Name);
                    string elementValue = null;

                    switch (formElementName)
                    {
                        case ElementsName.FirstName:
                            {
                                elementValue = parent.Contact.FirstName;
                                result.Add(formElementName);
                            }
                            break;
                        case ElementsName.LastName:
                            {
                                elementValue = parent.Contact.LastName;
                                result.Add(formElementName);
                            }
                            break;
                        case ElementsName.Email:
                            {
                                elementValue = parent.Contact.Email;
                                result.Add(formElementName);
                            }
                            break;
                        case ElementsName.Gender:
                            {
                                elementValue = parent.Gender.ToString();
                                result.Add(formElementName);
                            }
                            break;
                        case ElementsName.DateOfBirth:
                            {
                                elementValue = parent.Contact.DoB.ToString();
                                result.Add(formElementName);
                            }
                            break;
                        case ElementsName.Cell:
                            {
                                elementValue = parent.Contact.Cell;
                                result.Add(formElementName);
                            }
                            break;
                        case ElementsName.Occupation:
                            {
                                elementValue = parent.Contact.Occupation;
                                result.Add(formElementName);
                            }
                            break;
                        case ElementsName.Employer:
                            {
                                elementValue = parent.Contact.Employer;
                                result.Add(formElementName);
                            }
                            break;
                        case ElementsName.Phone:
                            {
                                elementValue = parent.Contact.Phone;
                                result.Add(formElementName);
                            }
                            break;
                        case ElementsName.AlternatePhone:
                            {
                                elementValue = parent.Contact.Cell;
                                result.Add(formElementName);
                            }
                            break;
                        case ElementsName.ParentRelationship:
                            {
                                elementValue = parent.Contact.Relationship.HasValue ? ((int)parent.Contact.Relationship.Value).ToString() : null;
                                result.Add(formElementName);
                            }
                            break;
                        case ElementsName.Address:
                            {
                                FillAddress(formElement as JbAddress, parent.Contact.Address);
                                result.Add(formElementName);
                                continue;
                            }
                    }

                    if (elementValue != null)
                    {
                        formElement.SetValue(elementValue);
                    }
                }

            }

            return result;
        }

        private List<ElementsName> AutoFillSchoolSection(JbSection jbSection, PlayerInfo profileInfo, List<ElementsName> defaultFormElementNames)
        {
            var result = new List<ElementsName>();
            var defaultSectionElements = jbSection.Elements.Where(e => defaultFormElementNames.Select(d => d.ToString()).Contains(e.Name));

            foreach (var formElement in defaultSectionElements)
            {
                var formElementName = EnumHelper.ParseEnum<ElementsName>(formElement.Name);
                string elementValue = null;

                switch (formElementName)
                {
                    case ElementsName.SchoolName:
                        {
                            elementValue = profileInfo.SchoolName;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.Name:
                        {
                            elementValue = profileInfo.SchoolName;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.Homeroom:
                        {
                            elementValue = profileInfo.HomeRoom;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.Grade:
                        {
                            result.Add(formElementName);
                        }
                        break;
                }

                if (elementValue != null)
                {
                    formElement.SetValue(elementValue);
                }
            }

            return result;
        }

        private List<ElementsName> AutoFillInsuranceSection(JbSection jbSection, Family family, List<ElementsName> defaultFormElementNames)
        {
            var result = new List<ElementsName>();
            var defaultSectionElements = jbSection.Elements.Where(e => defaultFormElementNames.Select(d => d.ToString()).Contains(e.Name));

            foreach (var formElement in defaultSectionElements)
            {
                var formElementName = EnumHelper.ParseEnum<ElementsName>(formElement.Name);
                string elementValue = null;

                switch (formElementName)
                {
                    case ElementsName.CompanyName:
                        {
                            elementValue = family.InsuranceCompanyName;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.PolicyNumber:
                        {
                            elementValue = family.InsurancePolicyNumber;
                            result.Add(formElementName);
                        }
                        break;
                }

                if (elementValue != null)
                {
                    formElement.SetValue(elementValue);
                }
            }

            return result;

        }

        private List<ElementsName> AutoFillAuthorizedPickupSection(JbSection jbSection, FamilyContact familyContact, List<ElementsName> defaultFormElementNames)
        {
            var result = new List<ElementsName>();
            var defaultSectionElements = jbSection.Elements.Where(e => defaultFormElementNames.Select(d => d.ToString()).Contains(e.Name));

            foreach (var formElement in defaultSectionElements)
            {
                var formElementName = EnumHelper.ParseEnum<ElementsName>(formElement.Name);
                string elementValue = null;

                switch (formElementName)
                {
                    case ElementsName.FirstName:
                        {
                            elementValue = familyContact.Contact.FirstName;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.LastName:
                        {
                            elementValue = familyContact.Contact.LastName;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.Phone:
                        {
                            elementValue = familyContact.Contact.Phone;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.AlternatePhone:
                        {
                            elementValue = familyContact.Contact.Cell;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.Email:
                        {
                            elementValue = familyContact.Contact.Email;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.Relationship:
                        {
                            elementValue = familyContact.Relationship.HasValue ? ((int)familyContact.Relationship.Value).ToString() : null;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.IsEmergencyContact:
                        {
                            elementValue = familyContact.IsEmergencyContact.ToString();
                            result.Add(formElementName);
                        }
                        break;
                }

                if (elementValue != null)
                {
                    formElement.SetValue(elementValue);
                }
            }

            return result;
        }

        private List<ElementsName> AutoFillHealthSection(JbSection jbSection, PlayerInfo profileInfo, List<ElementsName> defaultFormElementNames)
        {
            var result = new List<ElementsName>();
            var defaultSectionElements = jbSection.Elements.Where(e => defaultFormElementNames.Select(d => d.ToString()).Contains(e.Name));

            foreach (var formElement in defaultSectionElements)
            {
                var formElementName = EnumHelper.ParseEnum<ElementsName>(formElement.Name);
                string elementValue = null;

                switch (formElementName)
                {
                    case ElementsName.SelfAdministerMedicationDescription:
                        {
                            elementValue = profileInfo.SelfAdministerMedicationDescription;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.AllergiesInfo:
                        {
                            elementValue = profileInfo.Allergies;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.NutAllergyDescription:
                        {
                            elementValue = profileInfo.NutAllergyDescription;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.MedicalConditions:
                        {
                            elementValue = profileInfo.SpecialNeeds;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.DoctorName:
                        {
                            elementValue = profileInfo.DoctorName;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.DoctorPhone:
                        {
                            elementValue = profileInfo.DoctorPhone;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.Address:
                        {
                            elementValue = profileInfo.DoctorLocation;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.SelfAdministerMedication:
                        {
                            elementValue = profileInfo.SelfAdministerMedication == true ? "1" : profileInfo.SelfAdministerMedication == false ? "0" : string.Empty;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.NutAllergy:
                        {
                            elementValue = profileInfo.NutAllergy == true ? "1" : profileInfo.NutAllergy == false ? "0" : string.Empty;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.HasMedicalConditions:
                        {
                            elementValue = profileInfo.HasSpecialNeeds == true ? "1" : profileInfo.HasSpecialNeeds == false ? "0" : string.Empty;
                            result.Add(formElementName);
                        }
                        break;
                    case ElementsName.HasAllergiesInfo:
                        {
                            elementValue = profileInfo.HasAllergies == true ? "1" : profileInfo.HasAllergies == false ? "0" : string.Empty;
                            result.Add(formElementName);
                        }
                        break;
                }

                if (elementValue != null)
                {
                    formElement.SetValue(elementValue);
                }
            }

            return result;
        }

        private List<ElementsName> AutoFillPhotographySection(JbSection jbSection, PlayerInfo profileInfo, List<ElementsName> defaultFormElementNames)
        {
            var result = new List<ElementsName>();
            var defaultSectionElements = jbSection.Elements.Where(e => defaultFormElementNames.Select(d => d.ToString()).Contains(e.Name));

            foreach (var formElement in defaultSectionElements)
            {
                var formElementName = EnumHelper.ParseEnum<ElementsName>(formElement.Name);
                string elementValue = null;

                switch (formElementName)
                {
                    case ElementsName.PermissionPhotography:
                        {
                            // elementValue = profileInfo.HasPermissionPhotography.HasValue ? (profileInfo.HasPermissionPhotography.Value ? "1" : "0") : null;

                            result.Add(formElementName);
                        }
                        break;
                }

                if (elementValue != null)
                {
                    formElement.SetValue(elementValue);
                }
            }

            return result;
        }

        private List<ElementsName> AutoFillMiscellaneousSection(JbSection jbSection, string howDidYouHearAboutUs, List<ElementsName> defaultFormElementNames)
        {
            var result = new List<ElementsName>();
            var defaultSectionElements = jbSection.Elements.Where(e => defaultFormElementNames.Select(d => d.ToString()).Contains(e.Name));

            foreach (var formElement in defaultSectionElements)
            {
                var formElementName = EnumHelper.ParseEnum<ElementsName>(formElement.Name);
                string elementValue = null;

                switch (formElementName)
                {
                    case ElementsName.HearAboutUs:
                        {
                            var enumValue = EnumHelper.GetValueFromDescription<HearAboutUs?>(howDidYouHearAboutUs);

                            if (enumValue != null)
                            {
                                var value = (int)enumValue;
                                elementValue = value.ToString();
                            }

                            result.Add(formElementName);
                        }
                        break;
                }

                if (elementValue != null)
                {
                    formElement.SetValue(elementValue);
                }
            }

            return result;
        }

        private List<ElementsName> AutoFillContactSection(JbSection jbSection, PostalAddress address, string phoneNumber, List<ElementsName> defaultFormElementNames)
        {
            var result = new List<ElementsName>();
            var defaultSectionElements = jbSection.Elements.Where(e => defaultFormElementNames.Select(d => d.ToString()).Contains(e.Name));

            foreach (var formElement in defaultSectionElements)
            {
                var formElementName = EnumHelper.ParseEnum<ElementsName>(formElement.Name);
                string elementValue = null;

                switch (formElementName)
                {
                    case ElementsName.Address:
                        {
                            if (address != null)
                            {
                                FillAddress(formElement as JbAddress, address);
                            }
                            result.Add(formElementName);
                            continue;
                        }
                    case ElementsName.Phone:
                        {
                            elementValue = phoneNumber;
                            result.Add(formElementName);
                        }
                        break;
                }

                if (elementValue != null)
                {
                    formElement.SetValue(elementValue);
                }
            }

            return result;
        }

        private void AutoFillNotDefaultEelements(JbForm jbForm, JbForm lastOrderForm, bool isDifferentPlayerMode, string sectionName = null, List<ElementsName> excludedFormElementNames = null, List<SectionsName> excludedFormSectionNames = null)
        {
            var stringExcludedFormElementNames = excludedFormElementNames != null ? excludedFormElementNames.Select(d => d.ToString()).ToList() : new List<string>();
            var stringExcludedFormSectionNames = excludedFormSectionNames != null ? excludedFormSectionNames.Select(d => d.ToString()).ToList() : new List<string>();
            List<IJbBaseElement> sections = null;

            if (!string.IsNullOrEmpty(sectionName))
            {
                sections = lastOrderForm.Elements.Where(e => e is JbSection && e.Name == sectionName).ToList();
            }

            if (stringExcludedFormSectionNames != null && stringExcludedFormSectionNames.Any())
            {
                sections = lastOrderForm.Elements.Where(e => e is JbSection && !stringExcludedFormSectionNames.Contains(e.Name)).ToList();
            }

            foreach (var rootElement in sections)
            {
                if (rootElement is JbSection)
                {

                    if (!jbForm.Elements.Any(e => e != null &&
                        (e.ElementId == rootElement.ElementId || (e.Name != null && e.Name == rootElement.Name) ||
                        e.Title.EqualsIgnoreSpaces(rootElement.Title))))
                        continue;

                    var section = jbForm.Elements.FirstOrDefault(e => e != null &&
                        (e.ElementId == rootElement.ElementId || (e.Name != null && e.Name == rootElement.Name) ||
                        e.Title.EqualsIgnoreSpaces(rootElement.Title)));

                    if (section != null)
                        foreach (var selemnt in (rootElement as JbSection).Elements)
                        {
                            if (stringExcludedFormElementNames.Contains(selemnt.Name))
                                continue;

                            if (selemnt is JbSignature)
                                continue;

                            if (selemnt is JbTextEditor)
                                continue;

                            if (selemnt is JbParagraph)
                                continue;

                            if (!(section as JbSection).Elements.Any(e => e != null &&
                                (e.ElementId == selemnt.ElementId || e.Title.EqualsIgnoreSpaces(selemnt.Title) || (e.Name != null && e.Name == selemnt.Name))))
                                continue;
                            if (selemnt is ICustomElement)
                                continue;

                            var element = (section as JbSection).Elements.FirstOrDefault(e => e != null &&
                                (e.ElementId == selemnt.ElementId || e.Title.EqualsIgnoreSpaces(selemnt.Title) || (e.Name != null && e.Name == selemnt.Name)));
                            if (element != null)
                            {
                                if (element.GetType() != selemnt.GetType())
                                    continue;

                                if (element is JbDropDown)
                                    if (!(element as JbDropDown).EqualsItems(selemnt as JbDropDown))
                                        continue;

                                if (element is JbAddress)
                                {
                                    var sourceAddress = selemnt as JbAddress;
                                    var address = new PostalAddress()
                                    {
                                        City = sourceAddress.City,
                                        Country = sourceAddress.Country,
                                        State = sourceAddress.State,
                                        Street = sourceAddress.AddressLine1,
                                        Street2 = sourceAddress.AddressLine2,
                                        Zip = sourceAddress.Zip
                                    };

                                    FillAddress(element as JbAddress, address);

                                    return;
                                }

                                object elementValue = null;

                                if (selemnt is JbDropDown)
                                {
                                    var dropDown = selemnt as JbDropDown;
                                    var items = dropDown.Items;
                                    var text = selemnt.GetValue() != null ? selemnt.GetValue().ToString() : null;

                                    elementValue = items.Any(i => i.Text == text) ? items.First(i => i.Text == text).Value : null;
                                }
                                else
                                {
                                    elementValue = selemnt.GetValue();
                                }

                                element.SetValue(elementValue);
                            }
                        }
                }
            }
        }

        private void FillAddress(JbAddress addressElement, PostalAddress address)
        {
            if (address != null)
            {
                addressElement.AddressLine1 = address.Street;
                addressElement.AddressLine2 = address.Street2;
                addressElement.City = address.City;
                addressElement.State = address.State;
                addressElement.Zip = address.Zip;

                ManipulateAddressCountry(addressElement, address.Country);
            }
        }

        private void ManipulateAddressCountry(JbAddress addressElement, string country)
        {
            switch (addressElement.AddressType)
            {
                case AddressType.US:
                    {
                        addressElement.Country = Countries.UnitedStates.ToString();
                    }
                    break;
                case AddressType.Canada:
                    {
                        addressElement.Country = Countries.Canada.ToString();
                    }
                    break;
                case AddressType.INTERNATIONAL:
                    {
                        addressElement.Country = country;
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
