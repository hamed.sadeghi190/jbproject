﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Helper.Element;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Tests
{
    class BladeTest : BaseTest<BladeElements>
    {
        public BladeTest(IWebDriver webDriver)
            :base(webDriver)
        {

        }
        #region Season
        public void GoToProgramAndOrders()
        {
            Elements.SeasonsLink.Click();
            ExtraAPI.Wait(3000);
            Elements.Fall2017SeasonLink.Click();
            ExtraAPI.Wait(3000);
            Elements.ProgramAndOrderLink.Click();
            ExtraAPI.Wait(5000);
        }
        #endregion Season
        public void GoToCreateClass()
        {
            Elements.SeasonsLink.Click();
            ExtraAPI.Wait(3000);
            Elements.Fall2017SeasonLink.Click();
            ExtraAPI.Wait(3000);
            Elements.SetupLink.Click();
            ExtraAPI.Wait(3000);
            Elements.ProgramLink.Click();
            ExtraAPI.Wait(3000);
            Elements.AddProgramLink.Click();
            ExtraAPI.Wait(3000);
            Elements.AddClassLink.Click();
            ExtraAPI.Wait(5000);
        }
        #region Organization
        public void GoToSchools()
        {
            Elements.OrganizationLink.Click();
            ExtraAPI.Wait(3000);
            Elements.SchoolsLink.Click();
            ExtraAPI.Wait(5000);
        }
        public void GoToSubsidies()
        {
            Elements.OrganizationLink.Click();
            ExtraAPI.Wait(3000);
            Elements.GovernmentSubsidiesLink.Click();
            ExtraAPI.Wait(5000);
        }
        public void GoToAddSubsidy()
        {
            Elements.OrganizationLink.Click();
            ExtraAPI.Wait(3000);
            Elements.GovernmentSubsidiesLink.Click();
            ExtraAPI.Wait(3000);
            Elements.AddSubsidyLink.Click();
            ExtraAPI.Wait(5000);
        }
        public void GoToDistricts()
        {
            Elements.OrganizationLink.Click();
            ExtraAPI.Wait(3000);
            Elements.SchoolDistrictsLink.Click();
            ExtraAPI.Wait(5000);
           
        }
        public void GoToAddDistrict()
        {
            Elements.OrganizationLink.Click();
            ExtraAPI.Wait(3000);
            Elements.SchoolDistrictsLink.Click();
            ExtraAPI.Wait(3000);
            Elements.AddDistrictLink.Click();
            ExtraAPI.Wait(5000);

        }
        #endregion Organization
        #region Setting
        public void GoToNotificationSetting()
        {
            Elements.SettingLink.Click();
            ExtraAPI.Wait(3000);
            Elements.NotificationsLink.Click();
            ExtraAPI.Wait(5000);
        }
        public void GoToEditHomeSite()
        {
            Elements.SettingLink.Click();
            ExtraAPI.Wait(3000);
            Elements.JumbulaHomeSiteLink.Click();
            ExtraAPI.Wait(3000);
            Elements.EditSiteLink.Click();
            ExtraAPI.Wait(5000);
        }
        #endregion Setting
        #region Contact
        public void GoToContactSetting()
        {
            Elements.ContactsLink.Click();
            ExtraAPI.Wait(3000);
            Elements.ContactSettingsLink.Click();
            ExtraAPI.Wait(5000);
        }
        public void GoToContacts()
        {
            Elements.ContactsLink.Click();
            ExtraAPI.Wait(3000);
            //Elements.AllContactsLink.Click();
            ExtraAPI.Wait(5000);
        }
        #endregion Contact

    }
}
