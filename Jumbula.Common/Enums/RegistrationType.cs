﻿

using System.ComponentModel.DataAnnotations;

namespace Jumbula.Common.Enums
{
    public enum RegistrationType
    {
        [Display(Name = "All Sessions")]
        AllSession,
        [Display(Name = "Dropins")]
        Dropin,
        [Display(Name = "Prorating")]
        ProRating
    }
}
