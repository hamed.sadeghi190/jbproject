﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum CertificateOfInsuranceTypes
    {
        [Description("Arlington (APS)")]
        Arlington = 1,
        [Description("Fairfax (FCPS)")]
        Fairfax = 2,
        [Description("Loudoun (LCPS)")]
        Loudoun = 3,
        [Description("Prince William (PWCS)")]
        Prince_William = 4,
        [Description("District of Columbia (DC)")]
        District_Of_Columbia = 5,
        [Description("Montgomery (MCPS)")]
        Montgomery = 6,
        [Description("Private School")]
        PrivateSchool = 7,
        [Description("Alexandria City")]
        AlexandriaCity = 8,
        [Description("Bergen County")]
        BergenCounty = 9,
        [Description("Essex County")]
        EssexCounty = 10,
        [Description("Hudson County")]
        HudsonCounty = 11,
        [Description("Middlesex County")]
        MiddlesexCounty = 12,
        [Description("Morris County")]
        MorrisCounty = 13,
        [Description("Passaic County")]
        PassaicCounty = 14,
        [Description("Greenwich Public Schools")]
        GreenwichPublicSchools = 15,
        [Description("Fairfield Public Schools")]
        FairfieldPublicSchools = 16,
        [Description("Westchester County")]
        WestchesterCounty = 17,
        [Description("General corporation")]
        GeneralCorporation = 18,
    }
}
