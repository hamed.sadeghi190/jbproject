﻿using Jb.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IPendingPreapprovalTransactionBusiness
    {
        IQueryable<PendingPreapprovalTransaction> GetList();
        PendingPreapprovalTransaction Get(long PendingPreapprovalId);
        IQueryable<PendingPreapprovalTransaction> GetListByDueDate(DateTime dueDate);
        IQueryable<PendingPreapprovalTransaction> GetListByOrderId(long orderId);
        OperationStatus Update(PendingPreapprovalTransaction pendingPreapproval);
        OperationStatus Create(PendingPreapprovalTransaction pendingPreapproval);
        bool OrderHasPendingTransaction(long orderId);
        OperationStatus CreateList(List<PendingPreapprovalTransaction> pendingTransaction);

        OperationStatus DeleteWhere(Expression<Func<PendingPreapprovalTransaction, bool>> filter = null, Func<IQueryable<PendingPreapprovalTransaction>, IOrderedQueryable<PendingPreapprovalTransaction>> orderBy = null, string includeProperties = "");
    }
}
