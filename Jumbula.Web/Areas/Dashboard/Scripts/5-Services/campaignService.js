﻿(function () {
    'use strict';

    angular.module('jbTools').service("CampaignService", [function () {

        var _model;

        function setModel(Data) {
            _model = Data;
        }

        function getModel() {
            return _model;
        }

        function setModelInCookie(name, value, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 1 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = name + "=" + value + "; " + expires;
        }

        function getModelFromCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ')
                    c = c.substring(1);
                if (c.indexOf(name) == 0)
                    return c.substring(name.length, c.length);
            }
            return "";
        }

        function setModelInStorage(data, objName) {
            if (typeof (Storage) !== "undefined") {
                sessionStorage.setItem(objName, JSON.stringify(data));
            } else {
                setModelInCookie(objName, JSON.stringify(data), 1);
            }
        }

        function getModelFromStrorage(objName) {
            var model = {};
            if (typeof (Storage) !== "undefined") {
                if (sessionStorage[objName]) {
                    var retrievedModel = sessionStorage.getItem(objName);
                    model = JSON.parse(retrievedModel);
                }
            } else {
                var modelString = getModelFromCookie(objName);
                model = JSON.parse(modelString);
            }
            return model;
        }

        return {
            set: setModel,
            get: getModel,
            setModelInStorage: setModelInStorage,
            getModelFromStrorage: getModelFromStrorage
        }
    }]);
})();