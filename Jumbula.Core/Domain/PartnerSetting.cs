﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Constants=Jumbula.Common.Constants.Constants;


namespace Jumbula.Core.Domain
{
    public class PartnerSetting : ClubSetting
    {
        public PartnerSetting()
        {
            PortalParameters = new PartnerSettingPortalParameter();
            Reports = new List<string>();
            ClubFlyerSetting = new ClubFlyerSetting();
        }

        public PartnerSettingPortalParameter PortalParameters { get; set; }
        public ClubFlyerSetting ClubFlyerSetting { get; set; }
        public List<string> Reports { get; set; }

        public string BackGroundCheckEmailReceiver { get; set; }
    }

    public class PartnerSettingPortalParameter
    {
        public bool ShowCommitionRate { get; set; }

        public bool ShowSchoolAdvancedSetting { get; set; }

        public bool ShowSchoolSeasonAdvancedSetting { get; set; }

        public bool ReadStripeSettingsFromMember { get; set; }

        public bool ShowWelocomeToJumbula { get; set; }
        public bool ShowSchoolsInHome { get; set; }
        public bool ReadInvoiceSettingsfromMember { get; set; }
        public bool EmailProviderAfterInvitinganInstructor { get; set; }

        public bool ReadRegistrationFeeFromMember { get; set; }
        public bool ReadLoginTabFromPartner { get; set; }
        public bool ReadMessageNewFamilyFromPartner { get; set; }

        [Required(ErrorMessage = "Email content is required.")]
        public string EmailContent { get; set; }
        public string PriceOptionNote { get; set; }

        public bool HelpInformationFromPartner { get; set; }
        public bool NotificationEmailFromPartner { get; set; }
        public bool ReadAutoChargePolicyFromMember { get; set; }
        public bool ReadDelinquentPolicyFromMember { get; set; }

        public bool ReadProgramsPolicyFromMember { get; set; }

        public bool ReadGASettingFromPartner { get; set; }

        private bool? _showGenderForCatalog;
        public bool? ShowGenderForCatalog
        {
            get
            {
                return _showGenderForCatalog.HasValue ? _showGenderForCatalog.Value : true;
            }
            set
            {
                _showGenderForCatalog = value;
            }
        }

        private string _priceOptionFeeInputBoxLabel;

        public string PriceOptionFeeInputBoxLabel
        {
            get
            {
                return !string.IsNullOrEmpty(_priceOptionFeeInputBoxLabel) ? _priceOptionFeeInputBoxLabel : Constants.PriceOption_Lable;
            }

            set
            {
                _priceOptionFeeInputBoxLabel = value;
            }
        }
        public bool ReadDependentCareReportInfoFromMember { get; set; }
        public bool ReadDuplicateEnrollmentsFromMember { get; set; }
    }


    public class ClubFlyerSetting
    {
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
    }
}
