﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('chandgeDaysController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbToast) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();

        if (!$scope.MC.account.isPartner) {
            $scope.MC.openBlade({ url: "Dashboard/Blade/Season", order: 1, menu: "SEASONS" });
        }
        $scope.baseUrl = window.location.origin + "/Dashboard/Order/";
        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $stateParams.seasonDomain;
        $scope.orderItemId = $stateParams.orderId;

        $scope.Model = {};

        $http.get($scope.baseUrl + '/EditSubscriptionProgramDays/?orderItemId=' + $stateParams.orderId).success(function (data, status, headers, config) {
            $scope.Model = data;

            for (var i = 0; i < $scope.Model.AllProgramDays.length; i++) {

                for (var j = 0; j < $scope.Model.Days.length; j++) {

                    if ($scope.Model.AllProgramDays[i].Title == $scope.Model.Days[j]) {

                        $scope.Model.AllProgramDays[i].IsChecked = true;
                    }
                }
            }

    
        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.Save = function () {
            $http.post($scope.baseUrl + '/EditSubscriptionProgramDays', { model: $scope.Model })
             .success(function (data, status, headers, config) {
                 if (data.Status) {
                     $scope.errors = null;
                     jbToast.success('Class days updated successfully.');
                     $state.go("OrderItem", { seasonDomain: $stateParams.seasonDomain, orderItemId: $stateParams.orderId })
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }

    }]);
})();