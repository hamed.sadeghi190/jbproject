﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jumbula.Core.Domain
{
    public class ProgramAttributeValue
    {
        [ForeignKey("ProgramId")]

        public virtual Program Program { get; set; }

        [Column(Order = 0), Key]
        public long ProgramId { get; set; }

        [ForeignKey("AttributeId")]
        public virtual JbAttribute Attribute { get; set; }

        [Column(Order = 1), Key]
        public int AttributeId { get; set; }

        public string Value { get; set; }

        public int? AttributeOptionId { get; set; }

        [ForeignKey("AttributeOptionId")]
        public virtual JbAttributeOption AttributeOption { get; set; }
    }
}
