﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Common.Helper;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ReconciliationPrepReportViewModel
    {
        public string ProviderName { get; set; }

        public string ClassName { get; set; }

        public string GradeRange { get; set; }

        public int? EMRate { get; set; }

        public string EMRatestr
        {
            get
            {

                return EMRate.HasValue && EMRate != 0 ? CurrencyHelper.FormatPercentWithoutPenny(EMRate) : "-";


            }
        }

        public decimal ClassFee { get; set; }
    }
}