﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class ClubPageInfoViewModel
    {
        // for links
        public string ClubDomain { get; set; }
        public int ClubCategoryId { get; set; }

        // left side
        public string Name { get; set; }
        public PostalAddress Address { get; set; }
        public ContactPerson Owner { get; set; }
        public string Tel { get; set; }
        public string Site { get; set; }
        public string SiteLink { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public string LogoUri { get; set; }
        public string MeetupTimes { get; set; }
        // affiliator 
        public bool HasAffiliator { get; set; }
        public string AffiliatorLogoUri { get; set; }
        public string AffiliatorDomain { get; set; }
        public string AffiliatorName { get; set; }
        public string ClubCoverPath { get; set; }
        // right side
        public string Announce { get; set; }

        public bool IsEmailType { get; set; }
    }

}