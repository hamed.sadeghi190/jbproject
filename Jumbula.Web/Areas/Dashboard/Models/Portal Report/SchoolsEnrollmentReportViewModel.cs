﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SchoolsEnrollmentReportViewModel
    {
        public SchoolsEnrollmentReportViewModel()
        {
            //Programs = new List<SchoolsProgramsEnrollmentReportViewModel>();
        }

        public string SchoolName { get; set; }
        public string SchoolLogo { get; set; }
        public SeasonNames SeasonName { get; set; }
        public int Year { get; set; }
        public int TotalEnrollment { get; set; }
        public List<SchoolsProgramsEnrollmentReportViewModel> Programs { get; set; }
    }
}