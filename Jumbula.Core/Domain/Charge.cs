﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Newtonsoft.Json;

namespace Jumbula.Core.Domain
{
    public class Charge : BaseEntity<long>
    {
        public Charge()
        {
            Category = ChargeDiscountCategory.EntryFee;
            AmountType = ChargeDiscountType.Fixed;
            Programs = new List<Program>();
        }
        private ChargeAttribute _attributes;
        public ChargeDiscountType AmountType { get; set; }

        [StringLength(256)]
        public string Name { get; set; }

        [StringLength(1024)]
        public string Description { get; set; }

        public decimal Amount { get; set; }

        public int Capacity { get; set; }

        public bool DisableForZeroAmount { get; set; }

        [DefaultValue(ChargeDiscountCategory.EntryFee)]
        public ChargeDiscountCategory Category { get; set; }

        public MetaData MetaData { get; set; }

        public bool HasEarlyBird { get; set; }

        [StringLength(1024)]
        [Column("Attributes")]
        public string AttributesSerialized { get; set; }

        [NotMapped]
        public ChargeAttribute Attributes
        {
            get
            {
                if (string.IsNullOrEmpty(AttributesSerialized))
                {
                    return new ChargeAttribute();
                }

                if (this.ProgramSchedule != null && this.ProgramSchedule.Program.TypeCategory == ProgramTypeCategory.Subscription && this.Category == ChargeDiscountCategory.EntryFee)
                {
                    return JsonConvert.DeserializeObject<SubscriptionChargeAttribute>(AttributesSerialized);
                }
                if (this.ProgramSchedule != null && this.ProgramSchedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare && this.Category == ChargeDiscountCategory.EntryFee)
                {
                    return JsonConvert.DeserializeObject<BeforeAfterCareChargeAttribute>(AttributesSerialized);
                }

                return JsonConvert.DeserializeObject<ChargeAttribute>(AttributesSerialized);
            }

            set
            {
                AttributesSerialized = JsonConvert.SerializeObject(value);
            }
        }

        public bool IsDeleted { get; set; }

        [ForeignKey("ProgramScheduleId")]
        public virtual ProgramSchedule ProgramSchedule { get; set; }

        public long? ProgramScheduleId { get; set; }
        public int? ClubId { get; set; }
        public ChargeApplyType ApplyType { get; set; }

        public virtual ICollection<ProgramSchedulePartCharge> SchdulePartCharges { get; set; }

        public virtual ICollection<ProgramSession> ProgramSessions { get; set; }


        [ForeignKey("SeasonId")]
        public virtual Season Season { get; set; }

        public long? SeasonId { get; set; }

        public bool IsAllProgram { get; set; }

        public virtual ICollection<Program> Programs { get; set; }
    }

    public class ChargeAttribute
    {
        public List<EarlyBird> EarlyBirds { get; set; }

        public TimeSpan? StartTime { get; set; }

        public TimeSpan? EndTime { get; set; }

        public bool IsMandatory { get; set; }

        public bool IsProrate { get; set; }
        public DateTime? ProrateStartDate { get; set; }
        public decimal? ProrateSessionPrice { get; set; }
        public string Color { get; set; }

        public decimal? DropInPrice { get; set; }
        public string DropInName { get; set; }
    }

    public class SubscriptionChargeAttribute : ChargeAttribute
    {
        public long SubscriptionScheduleId { get; set; }

        public int WeekDays { get; set; }
    }
    public class BeforeAfterCareChargeAttribute : ChargeAttribute
    {
        public string NumberOfClassDay { get; set; }
    }
    public enum ChargeApplyType : byte
    {
        [Description("Each order in the cart")]
        OrderItem,
        [Description("Once for the entire cart")]
        Cart,
        [Description("One time per season for the entire family")]
        PerFamilyPerSeason,
        [Description("One time per season for each participant")]
        PerParticipantPerSeason,
    }

}
