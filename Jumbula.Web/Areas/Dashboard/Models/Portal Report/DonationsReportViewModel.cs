﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class DonationsReportViewModel
    {
        public string ParentFirstName { get; set; }
        public string ParentLastName { get; set; }
        public string ParentEmail { get; set; }
        public string ParticipantFirstName { get; set; }
        public string ParticipantLastName { get; set; }       
        public string ConfirmationId { get; set; }
        public string DonationAmount { get; set; }
        public string UserEmail { get; set; }
        public string Date { get; set; }
        public long Id { get; set; }
        public bool HasDetail { get; set; }
    }
}