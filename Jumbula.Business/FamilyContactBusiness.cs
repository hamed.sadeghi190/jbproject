﻿using Jb.Framework.Common;
using Jumbula.Core.Business;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class FamilyContactBusiness :  IFamilyContactBusiness
    {
        private readonly IFamilyBusiness _familyBusiness;
        private readonly IRepository<FamilyContact> _famailyContactRepository;

        public FamilyContactBusiness(IFamilyBusiness familyBusiness, IRepository<FamilyContact> famailyContactRepository)
        {
            _familyBusiness = familyBusiness;
            _famailyContactRepository = famailyContactRepository;
        }

        public FamilyContact GetFamilyContact(int Id)
        {
            return _famailyContactRepository.Get(p => p.Id == Id);
        }
        public OperationStatus Delete(FamilyContact familyContact)
        {
            familyContact = GetFamilyContact(familyContact.Id);
            familyContact.Status = FamilyContactStatusType.Deleted;
            return _famailyContactRepository.Update(familyContact);

        }
        public OperationStatus DeleteFamilyContact(int id)
        {
            var familyContact = GetFamilyContact(id);

            var result = Delete(familyContact);

            return result;
        }

        public List<FamilyContact> GetAuthorizedPickups(int userId)
        {
            var family = _familyBusiness.GetFamily(userId);
            var authorizedPickups = new List<FamilyContact>();

            if (family == null)
            {
                family = _familyBusiness.AddUserFamily(userId);
            }

            if (family.Contacts != null)
            {
                authorizedPickups = family.Contacts.Where(c => c.Type == FamilyContactType.AuthorizedPickup && c.Status == FamilyContactStatusType.Active).ToList();
            }

            return authorizedPickups;
        }
        public List<FamilyContact> GetEmergencyContacts(int userId)
        {
            var family = _familyBusiness.GetFamily(userId);
            var emergencyContacts = new List<FamilyContact>();
            if (family == null)
            {
                family = _familyBusiness.AddUserFamily(userId);
            }
            if (family.Contacts != null)
            {
                emergencyContacts = family.Contacts.Where(c => c.Type == FamilyContactType.Emergency && c.Status == FamilyContactStatusType.Active).ToList();
            }

            return emergencyContacts;
        }

        public OperationStatus Create(FamilyContact entity)
        {
            return _famailyContactRepository.Create(entity);
        }
    }
}
