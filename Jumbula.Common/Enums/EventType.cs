﻿

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jumbula.Common.Enums
{
    public enum EventType
    {
        [Display(Name = "Event Type")]
        [Description("Event Type")]
        None = -1,
        Tourney = 0,
        Camp = 1,
        [Display(Name = "Class")]
        [Description("Class")]
        Course = 2, /* used instead of class, which is a researved word :) */
        [Description("Day care")]
        Calendar = 3,
    }
}
