using System;
using System.Collections.Generic;
using System.Linq;
using Jb.Framework.Common.Forms.JsonFormatter;

using Newtonsoft.Json;
using JsonHelper = Jumbula.Common.Helper.JsonHelper;

namespace Jb.Framework.Common.Forms
{
    [JsonConverter(typeof(JbElementConverter))]
    public class JbDropDown : JbBaseElement, IInputElement
    {
       

        private string _stringElements;
        public string DefaultText { get; set; }
        public List<JbElementItem> Items { get; set; }

        [JsonIgnore]
        public string JsonItems
        {
            get
            {
                if (Items != null && Items.Any())
                {
                    JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };
                    _stringElements = Jumbula.Common.Helper.JsonHelper.JsonSerializer(Items, settings);
                }
                return _stringElements;

            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _stringElements = value;
                    JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };
                    Items = JsonHelper.JsonDeserialize<List<JbElementItem>>(_stringElements, settings);
                }
            }
        }


        public string Value
        {
            get;
            set;
        }

        public override object GetValue()
        {
            return (Items != null && Items.Any()) && Items.Any(i => i.Value == Value) ? Items.Single(i => i.Value == Value).Text : null ;
        }

        public object GetKey()
        {
            return (Items != null && Items.Any()) && Items.Any(i => i.Value == Value) ? Items.Single(i => i.Value == Value).Value : null;
        }

        public bool IsRequired
        {
            get;
            set;
        }

        public bool EqualsItems(JbDropDown destDropDown)
        {
            if (!(Items != null && Items.Any()))
                return false;

            if (Items.Count != destDropDown.Items.Count)
                return false;
            for (int i = 0; i < Items.Count; i++)
            {
                if (Items[i].Value != destDropDown.Items[i].Value || Items[i].Text != destDropDown.Items[i].Text)
                    return false;
            }
            return true;
        }

    }
}