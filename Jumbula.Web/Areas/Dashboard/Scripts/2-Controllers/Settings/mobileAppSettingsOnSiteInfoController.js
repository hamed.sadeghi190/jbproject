﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('mobileAppSettingsOnSiteInfoController',
        [
            '$scope', '$http', 'jbToast', 'uiComponentsService', function ($scope, $http, jbToast, uiComponentsService) {
                window.app.globalObjects.pageClass = 'school-siteinfo-settings-view';
                window.app.globalObjects.initializeView();

               

                $http.get('Dashboard/Setting/EditSchoolSettingsOnSiteInfo').success(
                    function (data) {
                        $scope.Model = data;

                    }).error(function (data) {
                        jbToast.error("error " + data);
                    });


                $scope.save = function () {


                    $http.post('Dashboard/Setting/EditSchoolSettingsOnSiteInfo', { model: $scope.Model })
                        .success(function (data) {

                            if (data.Status === true) {
                                $scope.errors = null;
                                jbToast.success("On-Site info updated successfully.", "");
                            } else {
                                $scope.MC.handleErrors($scope, data);
                            }
                        });
                }

                $scope.toggleTooltip = function (containerId) {
                    if (arguments[3])
                        uiComponentsService.toggleTooltip(containerId, arguments[1], arguments[2], arguments[3]);
                    else if (arguments[2])
                        uiComponentsService.toggleTooltip(containerId, arguments[1], arguments[2]);
                    else if (arguments[1])
                        uiComponentsService.toggleTooltip(containerId, arguments[1]);
                    else
                        uiComponentsService.toggleTooltip(containerId);
                }

            }
        ]);

})();