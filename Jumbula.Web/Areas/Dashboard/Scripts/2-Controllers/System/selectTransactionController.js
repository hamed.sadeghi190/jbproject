﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('selectTransactionController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        //filter
        $scope.baseUrl = window.location.origin + "/Dashboard/System";

        $http.get($scope.baseUrl + '/GetTotalTransaction').success(function (data, status, headers, config) {
            $scope.Model = data;
        }).error(function (data, status, headers, config) {
            jbToast.error(data);
        });
        $scope.stateParams = $stateParams;


        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/System';

        var totalAmount;

        $scope.Model = customReportService.getModelFromStrorage();

       // var colors = ["#c2e4ff", "#8fcdff", "#5cb7ff", "#29a0ff", "#0088f5", "#006cc2", "#00508f", "#00335c"];
        var colors = ["#e892a4", "#db530e", "#9e0020"];

        function getTotalTransaction() {
            $http.get(baseUrl + '/GetTotalTransaction', { params: { start: $scope.Model.UpdateFrom, end: $scope.Model.UpdateTo, PaymentMethod: $scope.Model.PaymentMethod } }).success(function (data, status, headers, config) {

            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }

        getTotalTransaction();
        function getTotalTransaction() {
            $http.get(baseUrl + '/GetPieChartTransactionReport', { params: { start: $scope.Model.UpdateFrom, end: $scope.Model.UpdateTo, PaymentMethod: $scope.Model.PaymentMethod } }).success(function (data, status, headers, config) {
            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }

        getTotalTransaction();
        $scope.mainGridDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: baseUrl + '/GetTotalTransaction',
                    type: "POST",
                    data: function () {
                        return {
                            end: $scope.Model.UpdateTo,
                            start: $scope.Model.UpdateFrom
                        };
                    },
                },
            },
            pageSize: 50,
            batch: true,
            scrollable: true,
            serverPaging: true,
            serverSorting: true,

            schema: {
                data: "data",
                total: "total"
            },

        });
        $scope.mainGridOptions = {
            dataSource: $scope.mainGridDataSource,
            sortable: true,
            resizable: false,
            columns: [
                {
                    field: "TotalAmount",
                    title: "Total Transaction",
                    width: "200px",
                    format: "{0:c2}",
                },
                {
                    field: "TotalRevenue",
                    title: "Total Revenue",
                    width: "200px",
                    format: "{0:c2}",

                },

                {
                    field: "Creditcard",
                    title: "Credit Card",
                    width: "200px",
                    format: "{0:c2}",
                },

                 {
                     field: "Echeck",
                     title: "Echeck",
                     width: "200px",
                     format: "{0:c2}",
                 },
                 {
                     field: "PayPal",
                     title: "PayPal",
                     width: "200px",
                     format: "{0:c2}",
                 },
                 {
                     field: "Cash",
                     title: "Cash",
                     width: "200px",
                     format: "{0:c2}",
                 },
                 {
                     field: "Check",
                     title: "Check",
                     width: "200px",
                     format: "{0:c2}",
                 },
                 {
                     field: "WireTransfer",
                     title: "WireTransfer",
                     width: "200px",
                     format: "{0:c2}",
                 },
                 {
                     field: "DebitCard",
                     title: "Debit Card",
                     width: "200px",
                     format: "{0:c2}",
                 },
                 {
                     field: "Scholarship",
                     title: "Scholarship",
                     width: "200px",
                     format: "{0:c2}",
                 },
                 {
                     field: "Credit",
                     title: "Credit",
                     width: "200px",
                     format: "{0:c2}",
                 },
                 {
                     field: "Other",
                     title: "Other",
                     width: "200px",
                     format: "{0:c2}",
                 },

            ],
        }

        $scope.filterData = function () {
            getTotalTransaction();
            $scope.mainGridOptions.dataSource.read();
            $scope.pieChartOption.dataSource.read();
        }

        $scope.clearFilters = function () {
            $scope.Model.UpdateFrom = "";
            $scope.Model.UpdateTo = "";
            $scope.Model.PaymentMethod = "";
            $scope.mainGridOptions.dataSource.read();
            $scope.pieChartOption.dataSource.read();
        }

        $scope.dataSourcePieChart = new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl + "/GetPieChartTransactionReport",
                    cache: false,
                    dataType: "json"
                },
                data: function () {
                    return {
                        end: $scope.Model.UpdateTo,
                        start: $scope.Model.UpdateFrom
                    };
                },

            },
            sort: {
                field: "Price",
                dir: "asc"
            },
            
        });

        $scope.pieChartOption = {
            dataSource: $scope.dataSourcePieChart,
            legend: {
                visible: false,
            },
            chartArea: {
                background: ""
            },
            seriesDefaults: {
                type: "pie",
                overlay: {
                    gradient: "none"
                },
            },
            seriesColors: colors,
            series: [{
                field: "Price",
                categoryField: "ProgramName",
                padding: 0
            }],
            tooltip: {
                visible: true,
                format: "N0",
                template: "#= category # (#= kendo.format('{0:P}',percentage)#) - (#= kendo.format('{0:C}', value)#)"
            }
        };

    }
    ]);

})()
