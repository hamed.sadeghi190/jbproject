

/****** Object:  Table [dbo].[JbAttributeOptions]    Script Date: 10/31/2017 3:48:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[JbAttributeOptions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[AttributeId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.JbAttributeOptions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[JbAttributeOptions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.JbAttributeOptions_dbo.JbAttributes_AttributeId] FOREIGN KEY([AttributeId])
REFERENCES [dbo].[JbAttributes] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[JbAttributeOptions] CHECK CONSTRAINT [FK_dbo.JbAttributeOptions_dbo.JbAttributes_AttributeId]
GO


