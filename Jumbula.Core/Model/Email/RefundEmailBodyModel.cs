﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using System.Collections.Generic;

namespace Jumbula.Core.Model.Email
{
    public class RefundEmailBodyModel
    {

        public RefundEmailBodyModel()
        {
            var url = System.Web.HttpContext.Current.Request.Url;
            RootSiteUrl = string.Format(Constants.F_HttpsSchemeAuthority, url.Authority);
            ClubUrl = $"{RootSiteUrl}/{ClubDomain}";
        }

        public string ClubDomain { get; set; }
        public string RefundNote { get; set; }
        public string ClubImageUrl { get; set; }
        public string AppliedText { get; set; }
        public string PaidAmount { get; set; }
        public List<RefundOrderEmailBodyItemModel> ReservationItems { get; set; }
        public string OrderConfirmationId { get; set; }
        public string ClubName { get; set; }
        public string ClubUrl { get; set; }
        public string ClubEmail { get; set; }
        public string ClubPhone { get; set; }
        public CurrencyCodes ClubCurrency { get; set; }
        public string RootSiteUrl { get; set; }
        public string BodyMailTitle { get; set; }
        public string PayPalInfo { get; set; }
        public string PlayerFullName { get; set; }
        public string OrderPaidDate { get; set; }
    }
}
