﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;

namespace Jumbula.Core.Domain
{
    public class ClubFormTemplate : BaseEntity, IProtectedResource
    {
        public ClubFormTemplate()
        {
            //Programs = new HashSet<Program>();
        }

        public string Title { get; set; }

        public FormType FormType { get; set; }

        public int ClubId { get; set; }

        [ForeignKey("ClubId")]
        public virtual Club Club { get; set; }

        public int JbForm_Id { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime DateCreated { get; set; }

        public DefaultFormType? DefaultFormType { get; set; }

        public FollowUpFormMode FollowUpFormMode { get; set; }

        [ForeignKey("JbForm_Id")]
        public virtual JbForm JbForm { get; set; }

        public virtual ICollection<Program> Programs { get; set; }

        public bool DoesTheClubOwnIt(int clubId)
        {
            if (Club == null || (Club != null && ClubId == clubId))
            {
                return true;
            }

            return false;
        }

        public bool DoesTheParentOwnIt(int userId)
        {
            return false;
        }
    }

    public enum DefaultFormType
    {
        Primary,
        School,
        TeamTournament
    }
}
