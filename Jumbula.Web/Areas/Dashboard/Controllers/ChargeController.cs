﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Controllers;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class ChargeController : DashboardBaseController
    {
        #region Fields
        private readonly IProgramBusiness _programBusiness;
        private readonly ISeasonBusiness _seasonBusiness;
        private readonly IChargeBusiness _chargeBusiness;
        #endregion
        #region Constractors
        public ChargeController(IProgramBusiness programBusiness, ISeasonBusiness seasonBusiness, IChargeBusiness chargeBusiness)
        {
            _programBusiness = programBusiness;
            _seasonBusiness = seasonBusiness;
            _chargeBusiness = chargeBusiness;
        }
        #endregion
        public virtual ActionResult Charges()
        {
            return View();
        }
        public virtual ActionResult ApplicationFee()
        {
            return View();
        }

        [HttpGet]
        public virtual JsonNetResult GetAllCharges(string seasonDomain)
        {
            var club = ActiveClub;
            var seasonId = _seasonBusiness.Get(seasonDomain, club.Id).Id;

            var charges = _chargeBusiness.
               GetList(seasonId).ToList().Select(c => new SeasonChargeViewModel() { Id = c.Id, Name = c.Name, Category = c.Category, Amount = c.Amount, StrAmount = CurrencyHelper.FormatCurrencyWithPenny(c.Amount, club.Currency) }).ToList();

            return JsonNet(new { DataSource = charges, TotalCount = charges.Count });
        }
        
        [HttpGet]

        public virtual JsonNetResult GetSeasonApllicationFee(string seasonDomain)
        {
            var model = new CreateEditChargeViewModel();
            var club = ActiveClub;
            var seasonId = _seasonBusiness.Get(seasonDomain, club.Id).Id;

            var applicationFee = _chargeBusiness.GetList(seasonId).FirstOrDefault(a => a.Category == ChargeDiscountCategory.ApplicationFee);
            model.SelectedPrograms = new List<string>();

            if (applicationFee != null)
            {
                model.Amount = applicationFee.Amount;
                model.Category = applicationFee.Category;
                model.ApplicationFeeApplyType = applicationFee.ApplyType;
                model.Name = applicationFee.Name;
                model.HasEarlyBird = applicationFee.HasEarlyBird;

                if (applicationFee.HasEarlyBird)
                {
                    var attribute = applicationFee.Attributes;
                    model.ExpireDate = attribute.EarlyBirds.FirstOrDefault()?.ExpireDate;
                    model.EarlyBirdAmount = attribute.EarlyBirds.FirstOrDefault()?.Price;
                }

                model.SelectedPrograms.AddRange(applicationFee.Programs.Select(c => c.Id.ToString()));
                model.IsAllProgram = applicationFee.IsAllProgram;
            }
            else
            {
                model.ApplicationFeeApplyType = ChargeApplyType.Cart;
            }

            var programs = _programBusiness.GetList().Where(p =>p.Status == ProgramStatus.Open && p.SeasonId == seasonId);
            model.ProgramsList = _programBusiness.GetSeasonProgramItems(seasonId);
     
            model.SeasonId = seasonId;

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult SaveApplicationFee(CreateEditChargeViewModel model)
        {
            var season = _seasonBusiness.Get(model.SeasonId);

            ApplicationFeeValidationCheck(model);

            if (ModelState.IsValid)
            {
                if (season.Charges != null && season.Charges.Any(c => c.IsDeleted == false && c.Category == ChargeDiscountCategory.ApplicationFee))
                {
                    var applicationFeeCharge = season.Charges.First(c => c.IsDeleted == false && c.Category == ChargeDiscountCategory.ApplicationFee);
                    applicationFeeCharge.IsDeleted = true;
                }

                if (season.Charges == null)
                {
                    season.Charges = new List<Charge>();
                }

                var earlyBirds = new List<EarlyBird>();
                if (model.HasEarlyBird)
                {
                    if (model.EarlyBirdAmount != null)
                        earlyBirds.Add(new EarlyBird
                        {
                            ExpireDate = model.ExpireDate,
                            Price = model.EarlyBirdAmount.Value
                        });
                }

                var selectedPrograms = new List<Program>();
                if (!model.IsAllProgram && model.SelectedPrograms != null && model.SelectedPrograms.Any())
                {
                    var programIds = model.SelectedPrograms.Select(c => long.Parse(c)).ToList();
                    selectedPrograms.AddRange(_programBusiness.GetList().Where(s => programIds.Contains(s.Id)));
                }
            
                season.Charges.Add(new Charge()
                {
                    Amount = model.Amount ?? 0,
                    AmountType = ChargeDiscountType.Fixed,
                    ApplyType = model.ApplicationFeeApplyType,
                    SeasonId = model.SeasonId,
                    IsDeleted = false,
                    MetaData = new MetaData() { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow },
                    Name = model.Name,
                    Category = ChargeDiscountCategory.ApplicationFee,
                    HasEarlyBird = model.HasEarlyBird,
                    Attributes = new ChargeAttribute()
                    {
                        EarlyBirds = earlyBirds
                    },
                    IsAllProgram = model.IsAllProgram,
                    Programs = !model.IsAllProgram && model.SelectedPrograms != null && model.SelectedPrograms.Any()? selectedPrograms : null
                });

                var res = _seasonBusiness.Update(season);
                return Json(new JResult { Status = res.Status });
            }

            return JsonFormResponse();
        }

        private void ApplicationFeeValidationCheck(CreateEditChargeViewModel model)
        {
            if (model.Amount.HasValue && model.Amount.Value == 0)
            {
                ModelState.AddModelError("model.Amount", "The value is not valid for amount.");
            }

            if (model.ApplicationFeeApplyType == ChargeApplyType.Cart)
            {
                ModelState.AddModelError("model.ApplicationFeeApplyType", "Applies to is required");
            }

            if (model.IsAllProgram)
            {
                ModelState.Remove("model.SelectedPrograms");
            }
            else if(model.SelectedPrograms == null)
            {
                ModelState.AddModelError("model.SelectedPrograms", "");
            }

            if (!model.HasEarlyBird)
            {
                ModelState.Remove("model.ExpireDate");
                ModelState.Remove("model.EarlyBirdAmount");
            }
        }
        public virtual JsonResult DeleteCharge(long chargeId)
        {
            var res = _chargeBusiness.Delete(chargeId);
            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }
    }
}
