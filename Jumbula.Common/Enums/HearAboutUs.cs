﻿

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jumbula.Common.Enums
{
    public enum HearAboutUs
    {
        [Description("Flyer from my child's backpack")]
        [Display(Name = "Flyer from my child's backpack")]
        FlyerChildsBackpack = 1,

        [Description("Flyer from the school office")]
        [Display(Name = "Flyer from the school office")]
        FlyerCchoolOffice = 2,

        [Description("School website")]
        [Display(Name = "School website")]
        SchoolWebsite = 3,

        [Description("School newsletter or email")]
        [Display(Name = "School newsletter or email")]
        NewsletterOrEmail = 4,

        [Description("Classroom teacher/administrator")]
        [Display(Name = "Classroom teacher/administrator")]
        ClassroomTeacher = 5,

        [Description("Friend/word of mouth")]
        [Display(Name = "Friend/word of mouth")]
        Friend = 6,

        [Description("Facebook/Twitter")]
        [Display(Name = "Facebook/Twitter")]
        FacebookTwitter = 7,

        [Description("Local news article")]
        [Display(Name = "Local news article")]
        LocalNewsArticle = 8,

        [Description("Returning customer")]
        [Display(Name = "Returning customer")]
        ReturningCustomer = 9,

        [Description("Other")]
        [Display(Name = "Other")]
        Other = 100,
    }
}
