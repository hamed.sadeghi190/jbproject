﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;

namespace Jumbula.Core.Domain
{
    public class WaitList : BaseEntity, IProtectedResource
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual JbUser User { get; set; }

        public long ProgramId { get; set; }

        [ForeignKey("ProgramId")]
        public virtual Program Program { get; set; }

        public long ScheduleId { get; set; }

        [ForeignKey("ScheduleId")]
        public virtual ProgramSchedule Schedule { get; set; }

        [MaxLength(20)]
        public string PhoneNumber { get; set; }

        public bool IsLive { get; set; }

        public SchoolGradeType Grade { get; set; }
        public DateTime Date { get; set; }

        public bool DoesTheClubOwnIt(int clubId)
        {
            if (Program.ClubId == clubId)
                return true;

            return false;
        }

        public bool DoesTheParentOwnIt(int userId)
        {
            if (UserId == userId)
                return true;

            return false;
        }
    }
}
