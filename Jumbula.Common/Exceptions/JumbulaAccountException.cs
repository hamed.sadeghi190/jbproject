﻿using System;

namespace Jumbula.Common.Exceptions
{
    public abstract class JumbulaAccountException : JumbulaException
    {
        protected JumbulaAccountException(Exception e)
            : base(Constants.Constants.M_E_Account, e)
        {

        }
    }
}
