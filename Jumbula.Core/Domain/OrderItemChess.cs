﻿using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class OrderItemChess : BaseEntity<long>
    {

        [StringLength(128)]
        public string Section { get; set; }

        [StringLength(128)]
        public string Schedule { get; set; }

        [StringLength(128)]
        public string Byes { get; set; }

        [StringLength(128)]
        public string Options { get; set; }

        [Display(Name = "USCF ID")]
        [MaxLength(128)]
        public string UscfId { get; set; }

        [Display(Name = "USCF expiration date")]
        [MaxLength(128)]
        public string UscfExpiration { get; set; }

        [Display(Name = "USCF regular rating (approximate ok)")]
        [MaxLength(128)]
        public string UscfRatingReg { get; set; }

        [Display(Name = "USCF quick rating")]
        [MaxLength(128)]
        public string UscfRatingQuick { get; set; }
    }
}
