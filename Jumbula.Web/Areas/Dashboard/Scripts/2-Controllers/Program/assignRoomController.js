﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('assignRoomController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbToast) {

        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        $scope.init = function () {
            app.globalObjects.initializeView();
        }

        var programId = $stateParams.programId;

        $http.get('/Dashboard/Program/GetAssignRoom?programId=' + programId).success(function (data, status, headers, config) {

            $scope.Model = data;
        });

        $scope.save = function () {
            $http.post('/Dashboard/Program/SaveAssignRoom', { programId: programId, model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status) {

                        jbToast.success("Room assigned successfully.");

                        $state.go('ProgramsReview', { seasonDomain: $scope.MC.stateParams.seasonDomain })
                    }
                })
        }

    }]);
})();