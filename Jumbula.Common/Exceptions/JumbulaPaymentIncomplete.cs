﻿using System;

namespace Jumbula.Common.Exceptions
{
    public class JumbulaPaymentIncomplete : JumbulaPaymentException
    {
        public JumbulaPaymentIncomplete(Exception e)
            : base(e)
        {
            throw new Exception(e.ToString());
        }
    }
}
