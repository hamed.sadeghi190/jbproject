﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class CartBusiness : ICartBusiness
    {
        private readonly IOrderBusiness _orderBusiness;
        private readonly ISeasonBusiness _seasonBusiness;
        private readonly IUserProfileBusiness _userProfileBusiness;
        private readonly IClubBusiness _clubBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IProgramBusiness _programBusiness;
        private readonly ISubscriptionBusiness _subscriptionBusiness;
        private readonly IChargeBusiness _chargeBusiness;

        public CartBusiness(IOrderBusiness orderBusiness, ISeasonBusiness seasonBusiness, IUserProfileBusiness userProfileBusiness, IClubBusiness clubBusiness, IOrderItemBusiness orderItemBusiness, IProgramBusiness programBusiness, ISubscriptionBusiness subscriptionBusiness, IChargeBusiness chargeBusiness)
        {
            _orderBusiness = orderBusiness;
            _seasonBusiness = seasonBusiness;
            _userProfileBusiness = userProfileBusiness;
            _clubBusiness = clubBusiness;
            _orderItemBusiness = orderItemBusiness;
            _programBusiness = programBusiness;
            _subscriptionBusiness = subscriptionBusiness;
            _chargeBusiness = chargeBusiness;
        }

        public Cart Get(int userId, int clubId)
        {
            var cart = new Cart();
            var order = _orderBusiness.GetOrderInCart(userId, clubId);
            if (order == null)
            {
                order = new Order();
            }
            cart.Order = order;
            return cart;
        }


        public bool CheckExistCoupon(Cart cart, int couponId)
        {
            foreach (var item in cart.Order.RegularOrderItems)
                if (item.OrderChargeDiscounts.Any(cd => cd.Category == ChargeDiscountCategory.Coupon && cd.CouponId == couponId))
                    return true;

            return false;
        }

        public List<Coupon> GetAllCoupons(Cart cart)
        {
            return cart.Order.RegularOrderItems.SelectMany(item => item.OrderChargeDiscounts.Where(orc => !orc.IsDeleted && orc.Category == ChargeDiscountCategory.Coupon).Select(orc => orc.Coupon)).
                  GroupBy(coupon => new { coupon.SeasonId, coupon.Id }).
                  Select(couponGrp => couponGrp.First()).ToList();
        }

        #region Preregistration

        public void ManipulatePreregisterationStutus(List<OrderItem> orderItems)
        {
            foreach (var item in orderItems.Where(i => i.ItemStatus != OrderItemStatusCategories.deleted))
            {
                var isSeasonInPreregistrationMode = false;

                if (item.SeasonId.HasValue)
                {
                    isSeasonInPreregistrationMode = _seasonBusiness.IsPreRegistrationMode(item.Season);
                }

                var seasonRegStatus = SeasonRegStatus.GeneralOpen;
                var hasSeasonPreregistration = false;

                if (item.SeasonId.HasValue)
                {
                    seasonRegStatus = _seasonBusiness.GetRegStatus(item.Season);

                    hasSeasonPreregistration = item.Season.Setting != null && item.Season.Setting.SeasonProgramInfoSetting != null ? item.Season.Setting.SeasonProgramInfoSetting.HasPreRegistration : false;
                }

                if ((seasonRegStatus == SeasonRegStatus.GeneralOpen || seasonRegStatus == SeasonRegStatus.GeneralClosed || seasonRegStatus == SeasonRegStatus.Closed || seasonRegStatus == SeasonRegStatus.LateOpen) && item.PreRegistrationStatus == PreRegistrationStatus.Initiated)
                {
                    item.ItemStatus = OrderItemStatusCategories.initiated;
                    item.PreRegistrationStatus = PreRegistrationStatus.Added;
                }
                else if (item.ItemStatus == OrderItemStatusCategories.initiated && (isSeasonInPreregistrationMode || (hasSeasonPreregistration && seasonRegStatus != SeasonRegStatus.GeneralOpen)))
                {
                    item.ItemStatus = OrderItemStatusCategories.notInitiated;
                    item.PreRegistrationStatus = PreRegistrationStatus.Initiated;
                }
            }
        }


        public void ManipulatePreregisterationStutus(Order order)
        {
            ManipulatePreregisterationStutus(order.OrderItems.ToList());
        }

        public void ManipulatePreregisterationStutus(Cart cart)
        {
            ManipulatePreregisterationStutus(cart.Order);
        }
        #endregion

        #region Lottery
        public void ManipulateLotteryStutus(List<OrderItem> orderItems)
        {
            foreach (var item in orderItems.Where(i => i.ItemStatus != OrderItemStatusCategories.deleted))
            {
                var isSeasonInLotteryMode = false;

                if (item.SeasonId.HasValue)
                {
                    isSeasonInLotteryMode = _seasonBusiness.IsLotteryMode(item.Season);
                }

                var seasonRegStatus = SeasonRegStatus.GeneralOpen;
                var hasSeasonLottery = false;

                if (item.SeasonId.HasValue)
                {
                    seasonRegStatus = _seasonBusiness.GetRegStatus(item.Season);

                    hasSeasonLottery = item.Season.Setting != null && item.Season.Setting.SeasonProgramInfoSetting != null ? item.Season.Setting.SeasonProgramInfoSetting.HasLottery : false;
                }

                if (item.LotteryStatus == LotteryStatus.Won && seasonRegStatus != SeasonRegStatus.GeneralClosed && seasonRegStatus != SeasonRegStatus.Closed)
                {
                    item.ItemStatus = OrderItemStatusCategories.initiated;
                }
                else if (item.ItemStatus == OrderItemStatusCategories.initiated  && (isSeasonInLotteryMode || (hasSeasonLottery && seasonRegStatus != SeasonRegStatus.GeneralOpen)))
                {
                    item.ItemStatus = OrderItemStatusCategories.notInitiated;
                    item.LotteryStatus = LotteryStatus.Initiated;
                }
            }
        }

        public void ManipulateLotteryStutus(Order order)
        {
            ManipulateLotteryStutus(order.OrderItems.ToList());
        }

        public void ManipulateLotteryStutus(Cart cart)
        {
            ManipulateLotteryStutus(cart.Order);
        }

        #endregion

        public bool CheckCartItemsValidation(Cart cart, ref Dictionary<long, string> errors)
        {
            var result = true;

            var regularItems = cart.Order.RegularOrderItems.Where(r => r.ProgramScheduleId.HasValue);

            foreach (var regularItem in regularItems)
            {
                var errorMessages = ValidateOrderItem(regularItem);

                if (string.IsNullOrEmpty(errorMessages)) continue;

                errors.Add(regularItem.Id, errorMessages);

                result = false;
            }

            return result;
        }

        private string ValidateOrderItem(OrderItem orderItem)
        {
            var errorMessages = "";

            if (!IsProgramActive(orderItem))
            {
                errorMessages = " The administrator has recently frozen this program and you can no longer register for it.";
            }
            else if (!IsOrderItemOpen(orderItem))
            {
                errorMessages = "The administrator has recently closed the registration period for this program and you can no longer register for it.";
            }
            else if (orderItem.ValidationMessages.Any())
            {
                errorMessages = orderItem.ValidationMessages.First();
            }

            return errorMessages;
        }

        private bool IsOrderItemValid(OrderItem orderItem, ref string errorMessage)
        {
            var result = true;


            if (!IsProgramActive(orderItem))
            {
                result = false;
                errorMessage = "The administrator has recently frozen this program and you can no longer register for it.";
            }

            if (result && !IsOrderItemOpen(orderItem))
            {
                result = false;
                errorMessage = "The administrator has recently closed the registration period for this program and you can no longer register for it.";
            }

            return result;
        }

        private bool IsProgramActive(OrderItem orderItem)
        {
            if (orderItem.ProgramSchedule.Program.Status == ProgramStatus.Open)
            {
                return true;
            }

            return false;
        }

        private bool IsOrderItemOpen(OrderItem orderItem)
        {
            if (orderItem.IsMultiRegister)
            {
                var regStatus = _seasonBusiness.GetRegStatus(orderItem.Season);

                if (regStatus == SeasonRegStatus.EarlyOpen || regStatus == SeasonRegStatus.GeneralOpen || regStatus == SeasonRegStatus.LateOpen || (orderItem.LotteryStatus == LotteryStatus.Won && orderItem.ItemStatus == OrderItemStatusCategories.initiated))
                {
                    return true;
                }
            }
            else
            {
                var regStatus = _programBusiness.GetRegStatus(orderItem.ProgramSchedule.Program);

                if (regStatus == ProgramRegStatus.Open)
                {
                    return true;
                }
            }

            return false;
        }

        public void CalculateRegistrationFee(Order order)
        {
            if (order == null || order.OrderItems == null || !order.RegularOrderItems.Any())
            {
                return;
            }

            var club = _clubBusiness.Get(order.ClubId);
            Charge registrationFee = new Charge();
            var isTestMode = !order.IsLive;
            DateTime now = DateTimeHelper.GetCurrentLocalDateTime(club.TimeZone);

            foreach (var orderItem in order.RegularOrderItems.Where(c => c.ProgramScheduleId.HasValue && c.ProgramTypeCategory != ProgramTypeCategory.Subscription && c.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare && c.Mode != OrderItemMode.DropIn))
            {
                var applicationFee = _chargeBusiness.GetProgramSeasonCharges(orderItem.ProgramSchedule.Program , ChargeDiscountCategory.ApplicationFee).FirstOrDefault();

                if (orderItem.GetOrderChargeDiscounts() != null && orderItem.GetOrderChargeDiscounts().ToList().Any(c => c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.PartnerApplicationFee))
                {
                    var listDeletedCharge = new List<OrderChargeDiscount>();

                    foreach (var charge in orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.PartnerApplicationFee).ToList())
                    {
                        listDeletedCharge.Add(charge);
                        orderItem.TotalAmount -= charge.Amount;
                    }

                    if (listDeletedCharge.Count > 0)
                    {
                        _orderItemBusiness.RemoveOrderItemCharges(listDeletedCharge);
                    }
                }

                if (applicationFee != null)
                {
                    var applicationAmount = applicationFee.Amount;

                    if (applicationFee.Attributes != null && applicationFee.Attributes.EarlyBirds != null && applicationFee.Attributes.EarlyBirds.Any())
                    {
                        foreach (var earlyBrid in applicationFee.Attributes.EarlyBirds.OrderBy(e => e.ExpireDate))
                        {
                            if (now.Date <= earlyBrid.ExpireDate.Value.Date)
                            {
                                applicationAmount = earlyBrid.Price;
                                break;
                            }
                        }
                    }

                    var applicationFeeType = applicationFee.ApplyType;

                    switch (applicationFeeType)
                    {
                        case ChargeApplyType.OrderItem:
                            {
                                orderItem.OrderChargeDiscounts.Add(new OrderChargeDiscount
                                {
                                    Amount = applicationAmount,
                                    Category = applicationFee.Category,
                                    ChargeId = applicationFee.Id,
                                    Description = applicationFee.Description,
                                    Name = applicationFee.Name,
                                    Subcategory = ChargeDiscountSubcategory.Charge,

                                });

                                orderItem.TotalAmount += applicationAmount;
                            }
                            break;
                        case ChargeApplyType.PerFamilyPerSeason:
                            {
                                var hasPaidApplicationFeeInItems = _userProfileBusiness.HasPaidApplicationFeeInItems(order.ClubId, order.UserId, orderItem.SeasonId.Value, isTestMode);

                                if (!hasPaidApplicationFeeInItems)
                                {
                                    orderItem.OrderChargeDiscounts.Add(new OrderChargeDiscount
                                    {
                                        Amount = applicationAmount,
                                        Category = applicationFee.Category,
                                        ChargeId = applicationFee.Id,
                                        Description = applicationFee.Description,
                                        Name = applicationFee.Name,
                                        Subcategory = ChargeDiscountSubcategory.Charge
                                    });

                                    orderItem.TotalAmount += applicationAmount;
                                }
                            }
                            break;
                        case ChargeApplyType.PerParticipantPerSeason:
                            {
                                var hasPaidApplicationFeeParticipant = _userProfileBusiness.HasPaidApplicationFeeParticipant(orderItem.PlayerId.Value, orderItem.SeasonId.Value, isTestMode);

                                if (!hasPaidApplicationFeeParticipant)
                                {
                                    orderItem.OrderChargeDiscounts.Add(new OrderChargeDiscount
                                    {
                                        Amount = applicationAmount,
                                        Category = applicationFee.Category,
                                        ChargeId = applicationFee.Id,
                                        Description = applicationFee.Description,
                                        Name = applicationFee.Name,
                                        Subcategory = ChargeDiscountSubcategory.Charge
                                    });

                                    orderItem.TotalAmount += applicationAmount;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        public decimal CalculateOrderItemTuitionPrice(OrderItem orderItem, decimal amount)
        {
            var result = orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare ? amount : orderItem.EntryFee;

            var club = orderItem.Order.Club;
            var showCombinedPriceOnlyForClub = false;

            if (club.Setting != null)
            {
                showCombinedPriceOnlyForClub = club.Setting.ShowCombinedPriceOnly;
            }

            foreach (var charge in orderItem.GetOrderChargeDiscounts().Where(c => (int)c.Category > 0 && c.Category != ChargeDiscountCategory.EntryFee))
            {
                if (!showCombinedPriceOnlyForClub) continue;
                if (charge.Category != ChargeDiscountCategory.Surcharge &&
                    charge.Category != ChargeDiscountCategory.PartnerSurcharge) continue;
                var chargeAmount = charge.Amount;

                result += chargeAmount;
            }

            return Math.Floor(result * 100) / 100;
        }

        public bool IsCartBelongsToUser(int currentUserId, int userId, bool isUserAdminForClub)
        {
            return userId == currentUserId || isUserAdminForClub;
        }
    }
}
