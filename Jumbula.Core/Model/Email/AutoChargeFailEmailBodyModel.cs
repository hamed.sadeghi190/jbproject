﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using System.Collections.Generic;
using System.Linq;

namespace Jumbula.Core.Model.Email
{
    public class AutoChargeFailEmailBodyModel
    {
        public string ClubDomain { get; set; }
        public string SeasonDomain { get; set; }
        public string ClubImageUrl { get; set; }
        public string ClubName { get; set; }
        public string TransactionId { get; set; }
        public string ClubEmail { get; set; }
        public string ClubPhone { get; set; }
        public CurrencyCodes ClubCurrency { get; set; }
        public string InstallmentDate { get; set; }
        public string BodyMailTitle { get; set; }
        public string PlayerFullName { get; set; }
        public string TotalAmount => CurrencyHelper.FormatCurrencyWithPenny(ReservationItems?.Sum(c => c.Price) ?? 0, ClubCurrency);

        public List<AutoChargeEmailBodyItemModel> ReservationItems { get; set; }

        public string ErrorMessage { get; set; }
        public string LastDigits { get; set; }

        public bool IsAdminMode { get; set; }
        public EmailBodyGenerationMode PreviewMode { get; set; } = EmailBodyGenerationMode.Normal;
    }
}
