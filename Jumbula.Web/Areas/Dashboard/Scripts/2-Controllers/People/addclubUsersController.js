﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('addclubUsersController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast','addNewUserService', function ($scope, $http, $log, $stateParams, $state, jbToast,addNewUserService) {
        window.app.globalObjects.pageClass = 'add-new-family-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;
       
        $scope.baseUrl = window.location.origin + "/Dashboard/People";
        $scope.MC.stateParams = $stateParams;

       
        $scope.returnPage = addNewUserService.get();
        $scope.save = function () {
           
            $http.post($scope.baseUrl + '/CreateUser', { model: $scope.Model })
                  .success(function (data, status, headers, config) {
                      if (data.Status == true) {
                          if ($scope.returnPage.page == "Register") {
                              $state.go("Register", { seasonDomain: $scope.returnPage.stateParams.seasonDomain, programId: $scope.returnPage.stateParams.programId, Type: $scope.stateParams.type });
                          }
                          else {
                              $state.go('Families');
                          }
                      }
                      else {
                          $scope.MC.handleErrors($scope, data);
                      }
                  });
        };

        $scope.back = function () {
            if ($scope.returnPage.page == "Register") {
                $state.go("Register", { seasonDomain: $scope.returnPage.stateParams.seasonDomain, programId: $scope.returnPage.stateParams.programId, Type: $scope.stateParams.type });
            }
           
            else {
                $state.go('Families');
            }
           
        }

    }]);
})();