﻿(function () {
    "use strict";

    angular.module("dashboardApp").controller("reportsContentController",
        [
            "$scope", "$http", "jbToast", "uiComponentsService", function ($scope, $http, jbToast, uiComponentsService) {
            
                window.app.globalObjects.initializeView();

                $http.get('Dashboard/Setting/GetReportsContents').success(function (data, status, headers, config) {
                    $scope.Model = data;

                }).error(function (data, status, headers, config) {
                    jbToast.error("error " + data);
                });

                $scope.save = function () {

                    $http.post('Dashboard/Setting/SaveReportsContents', { model: $scope.Model })
                        .success(function (data, status, headers, config) {

                            if (data.Status) {

                                $scope.errors = null;

                                jbToast.success('Notifications updated successfully.');
                            }
                            else {
                                $scope.MC.handleErrors($scope, data);
                            }
                        })
                }

                $scope.setDepententCaredefault = function (value) {
                    $scope.Model.DependentCareDescription = value;
                }

                $scope.toggleTooltip = function (containerId) {
                    if (arguments[3])
                        uiComponentsService.toggleTooltip(containerId, arguments[1], arguments[2], arguments[3]);
                    else if (arguments[2])
                        uiComponentsService.toggleTooltip(containerId, arguments[1], arguments[2]);
                    else if (arguments[1])
                        uiComponentsService.toggleTooltip(containerId, arguments[1]);
                    else
                        uiComponentsService.toggleTooltip(containerId);
                }
            }
        ]);
})();