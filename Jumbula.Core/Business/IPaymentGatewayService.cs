﻿using Jumbula.Core.Domain;
using Jumbula.Core.Model.Order;
using System.Collections.Generic;

namespace Jumbula.Core.Business
{
    public interface IPaymentGatewayService
    {
        RefundResultModel Refund(TransactionActivity transactionActivity, string clubDomain, decimal refundAmount, bool isTestMode, Dictionary<string, string> metadata);

        PaymentDetailModel GetPaymentDetails(string clubDomain, string payKey,
            IEnumerable<TransactionActivity> oldRefunded, bool isPaypalTransaction, bool isTestMode);
    }
}
