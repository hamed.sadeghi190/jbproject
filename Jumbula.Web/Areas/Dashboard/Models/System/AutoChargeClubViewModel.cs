﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class AutoChargeClubViewModel
    {
        public int ClubId { get; set; }
        public string ClubName { get; set; }
        public string Description { get; set; }

        public decimal ChargeAmount { get; set; }
        public decimal Balance { get; set; }
        public DateTime Date { get; set; }

        public string BillingId { get; set; }

    }
}