﻿using System;
using System.Data.Entity.ModelConfiguration;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public class PaypalIPN : BaseEntity<long>
    {
        public string TransactionType { get; set; }
        public string TransactionId { get; set; }
        public string ParentTransactionID { get; set; }
        public string IPN { get; set; }
        public bool IsValid { get; set; }
        public DateTime CreateDate { get; set; }
        public PaypalIPNCategories PaypalIPNCategory { get; set; }
        public string Token { get; set; }
        public JumbulaSubSystem PaypalCaller { get; set; }
        public bool IsProceed { get; set; }
        public bool IsActionTaken { get; set; }
    }

    public enum PaypalIPNCategories:byte
    {
        payment=0,
        refund=1,
        preapproval=2,
        IpnNotification=3,
        customIPN=4
    }

}
