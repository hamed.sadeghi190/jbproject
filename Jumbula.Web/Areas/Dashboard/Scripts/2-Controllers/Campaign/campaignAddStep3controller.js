﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('campaignAddStep3controller', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'campaigns-add-step3';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + "/Dashboard/Campaign";

        $scope.campaignId = $scope.MC.stateParams.campaignId;

        // init model.
        $scope.Model = {};

        $http.get(baseUrl + '/CampaignAddEditStep3', { params: { campaignId: $scope.campaignId } })
            .success(function (data, status, headers, config) {
                $scope.Model = data;
            });

        $scope.saveStep3 = function () {
            $http.post(baseUrl + '/CampaignAddEditStep3', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $state.go('EmailCampaignPreviewStep4', { campaignId: $scope.MC.stateParams.campaignId, campaignType: $scope.MC.stateParams.campaignType })

                    } else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        }
    }]);
})();