update Clubs set TimeZone = 2 where TimeZone = 24 -- Phoenix Standard Time

update Clubs set TimeZone = 19 where TimeZone = 21 -- Midway Islands Time

update Clubs set TimeZone = 0 where TimeZone = 25 -- Indiana Eastern Standard Time

