﻿using Jumbula.Common.Enums;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Jumbula.Data.Repositories
{
    public class EmailRepository : BaseRepository<Email>, IEmailRepository
    {
        public EmailRepository(IEntitiesContext context)
            : base(context)
        {

        }

        public IQueryable<Email> GetPendingEmails(EmailCategory? emailCategory = null)
        {
            var desiredNotSentTime = DateTime.UtcNow.AddHours(-1);

            return GetList(e => e.Status == EmailSendStatus.Accepted || ((e.Status == EmailSendStatus.Failed || e.Status == EmailSendStatus.Pending) && e.LastAttemptTime < desiredNotSentTime && e.AttemptNumber < Common.Constants.Email.MaxSendAttempt) && !emailCategory.HasValue || (emailCategory.HasValue && e.Category == emailCategory.Value)).AsNoTracking();
        }

        public OperationStatus UpdateEmailRecipientEvents(IEnumerable<EmailRecipient> recipients)
        {
            var emailIds = recipients.Select(r => r.EmailId).Distinct();

            var emails = GetList(e => emailIds.Contains(e.Id));

            foreach (var recipient in recipients)
            {
                var email = emails.SingleOrDefault(e => e.Id == recipient.EmailId);

                if (email != null)
                {
                    var emailRecipient = email.Recipients.SingleOrDefault(e => e.EmailAddress.Equals(recipient.EmailAddress));

                    if (emailRecipient != null)
                    {
                        if (!emailRecipient.Bounced)
                        {
                            emailRecipient.Bounced = recipient.Bounced;
                            emailRecipient.BounceDate = recipient.BounceDate;
                        }

                        if (!emailRecipient.Clicked)
                        {
                            emailRecipient.ClickDate = recipient.ClickDate;
                            emailRecipient.Clicked = recipient.Clicked;
                        }

                        if (!emailRecipient.Deferred)
                        {
                            emailRecipient.DefereDate = recipient.DefereDate;
                            emailRecipient.Deferred = recipient.Deferred;
                        }

                        if (!emailRecipient.Delivered)
                        {
                            emailRecipient.DeliverDate = recipient.DeliverDate;
                            emailRecipient.Delivered = recipient.Delivered;
                        }

                        if (!emailRecipient.Dropped)
                        {
                            emailRecipient.DropDate = recipient.DropDate;
                            emailRecipient.Dropped = recipient.Dropped;
                        }

                        if (!emailRecipient.Opened)
                        {
                            emailRecipient.Opened = recipient.Opened;
                            emailRecipient.OpenDate = recipient.OpenDate;
                        }

                        if (!emailRecipient.SpamReport)
                        {
                            emailRecipient.SpamReport = recipient.SpamReport;
                            emailRecipient.SpamReportDate = recipient.SpamReportDate;
                        }

                        if (!emailRecipient.Unsubscribe)
                        {
                            emailRecipient.Unsubscribe = recipient.Unsubscribe;
                            emailRecipient.UnsubscribeDate = recipient.UnsubscribeDate;
                        }

                        if (!emailRecipient.Blocked)
                        {
                            emailRecipient.Blocked = recipient.Blocked;
                        }
                    }
                }
            }

            return new OperationStatus { Status = Context.SaveChanges() > 0 };
        }
    }
}
