﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('googleAnalyticsController', ['$scope', '$http', 'jbToast', function ($scope, $http, jbToast) {
        window.app.globalObjects.pageClass = 'forms_View';
        window.app.globalObjects.initializeView();

        $http.get('Dashboard/ToolBox/GetGoogleAnalytics')
            .success(function (data, status, headers, config) {
                $scope.Model = data;
            });

        $scope.save = function () {

            $http.post('Dashboard/ToolBox/SaveGoogleAnalytics', { model: $scope.Model })
                .success(function (data, status, headers, config) {

                    if (data.Status) {

                        $scope.errors = null;

                        jbToast.success("Google analytics settings updated successfully.");

                    } else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        };

    }]);
})();