---@mn---2014/11/11------

IF OBJECT_ID('[dbo].[EntityForms]') IS NULL
BEGIN
	CREATE TABLE [dbo].[EntityForms](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[JbFormId] [int] NOT NULL,
		[EventId] [int] NULL)
END

------<< ADD PRIMARY KEY >>----
IF NOT EXISTS(SELECT 1 FROM sys.objects WHERE NAME='PK_EntityForms')
BEGIN
	ALTER TABLE [dbo].[EntityForms] ADD CONSTRAINT [PK_EntityForms] PRIMARY KEY (Id DESC)
END
GO