﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('PricingReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;
        $scope.IsReportRun = false;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        $scope.Model = customReportService.getModelFromStrorage();

        var model = $scope.Model;


        $scope.Model.Filters = {};
        $scope.Model.Year = 0;
        $scope.Model._SelectedClubs = [];
        $scope.ClubTypes = [{ Value: "school", Title: "Schools" }, { Value: "provider", Title: "Providers" }, { Value: "district", Title: "School districts" }];

        $scope.Model.IsAllPrograms = "true";

        $scope.Model.ClubType = "school";
        $scope.Model.IsAllClubs = "true";

        $scope.updateChosenRadio = function () {
            $scope.Model.IsAllClubs = "true";
        }


        $scope.getClubs = function (clubType) {
            if (clubType === "school")
                getAllSchools();
            else if (clubType === "provider")
                getAllProviders();
            else
                getAllDistricts();
        }

        $http.get(baseUrl + '/GetFullSeasonClassListReportHeader').success(function (data) {

            $scope.Years = data.Years;
            $scope.Model.AllSeason = data.Seasons;

            $scope.Model.Year = data.Years[0].Value;
            $scope.Model.SeasonName = data.Seasons[0].Value;
        }).error(function (data) {
            jbToast.error("error " + data);
        });


        $http.post(baseUrl + '/FillBaseFilters', { model: $scope.Model })
            .success(function (data, status, headers, config) {

                for (var i = 0; i < data.OrderStatus.length; i++) {
                    if (data.OrderStatus[i].Value == "transferIn") {
                        data.OrderStatus.splice(i, 1);
                    }
                    if (data.OrderStatus[i].Value == "transferOut") {
                        data.OrderStatus.splice(i, 1);
                    }
                    if (data.OrderStatus[i].Value == "canceled") {
                        data.OrderStatus.splice(i, 1);
                    }
                    if (data.OrderStatus[i].Value == "refund") {
                        data.OrderStatus.splice(i, 1);
                    }
                }

                $scope.Model.Filters = {};
                $scope.Model.Filters.Status = "regular";
                $scope.Model.Filters.OrderStatus = data.OrderStatus;
            }).error(function (data, status, headers, config) {
            });


        $scope.clearFilters = function () {
            $scope.Model.Filters = {};
            $scope.Model.Filters.Status = "regular";
            $scope.searchTerm = "";
            $scope.pricingReportGridOptions.dataSource.read();
        }


        $scope.prepareGrid = function () {

            if (!$scope.IsReportRun) {

                $scope.IsReportRun = true;

                $scope.pricingReportsDataSource = new kendo.data.DataSource({
                    dataType: "json",
                    transport:
                        {
                            read: {
                                url: baseUrl + '/GetPricingReport',
                                type: "POST",
                                data: function () {
                                    return {
                                        model: $scope.Model
                                    };
                                },
                            },

                        },
                    schema: {
                        total: "TotalCount",
                        data: "DataSource"
                    },
                    serverSorting: false,
                    serverPaging: true,
                    pageSize: 50,
                    batch: true,
                    resizable: true,
                    requestStart: function () {
                        kendo.ui.progress($("[kendo-grid]"), true);
                    },
                    requestEnd: function () {
                        kendo.ui.progress($("[kendo-grid]"), false);
                    }
                });

                $scope.pricingReportGridOptions = {
                    excel: {
                        allPages: true,
                        fileName: 'MigrationClassListReport.xlsx',
                        proxyURL: baseUrl + '/KendoExport',
                    },
                    excelExport: function (e) {
                        cfpLoadingBar.complete();
                    },
                    dataSource: $scope.pricingReportsDataSource,
                    sortable: { mode: 'single' },
                    resizable: true,
                    scrollable: true,
                    pageable: {
                        refresh: true,
                        pageSizes:
                            [
                                50, 100, 200
                            ],
                        buttonCount: 5
                    },
                    dataBound: function () {
                        var grid = $("#pricingReportGrid").data("kendoGrid");
                        customReportService.initializeDoubleScroll(grid);
                    },
                    columns: [
                        {
                            field: "UniqueId",
                            title: "Migration ID",
                            width: "150px"
                        },
                        {
                            field: "ClubName",
                            title: "School",
                            width: "170px",
                        },
                        {
                            field: "ProviderName",
                            title: "Provider",
                            width: "170px",
                        },
                        {
                            field: "ProgramName",
                            title: "Program",
                            width: "170px",
                        },
                        {
                            field: "Activity",
                            title: "Surcharge price",
                            width: "140px",
                            template: '<span style="white-space: pre-line" > #=Activity# </span>'
                        },
                        {
                            field: "ParentPrice",
                            title: "Wizard price",
                            width: "250px",
                            template: '<span style="white-space: pre-line" > #=ParentPrice# </span>'
                        },
                        {
                            field: "VendorPrice",
                            title: "Vendor price",
                            width: "250px",
                            template: '<span style="white-space: pre-line" > #=VendorPrice# </span>'
                        }
                    ]
                }
            } else {

                $scope.pricingReportGridOptions.dataSource.page(1);
                $("#pricingReportGrid").data("kendoGrid").dataSource.read();
            }

        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#pricingReportGrid").getKendoGrid().saveAsExcel();
        }

        function getAllSchools() {
            $http.get(baseUrl + '/GetAllSchools')
                .success(function (data) {
                    $scope.AllClubs = data.AllSchools;
                    var timer = false;

                    $scope.updateChosenScope = function () {
                        $("select[chosen]").trigger('chosen:updated');
                    };

                    $scope.$watch("AllClubs", function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });
                });
        }

        function getAllProviders() {
            $http.get(baseUrl + "/GetAllProvider")
                .success(function (data) {
                    $scope.AllClubs = data;
                    var timer = false;

                    $scope.updateChosenScope = function () {
                        $("select[chosen]").trigger('chosen:updated');
                    };

                    $scope.$watch('AllClubs', function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });
                });
        }

        function getAllDistricts() {
            $http.get(baseUrl + '/GetAllPartnerSchoolDistricts')
                .success(function (data) {
                    $scope.AllClubs = data.AllDisctricts;
                    var timer = false;

                    $scope.updateChosenScope = function () {
                        $("select[chosen]").trigger('chosen:updated');
                    };

                    $scope.$watch("AllClubs", function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });
                });
        }

    }]);
})();