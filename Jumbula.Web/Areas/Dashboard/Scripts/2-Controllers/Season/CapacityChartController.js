﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('CapacityChartController', ['$scope', '$http', 'jbToast', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, jbToast, $stateParams, $state, jbConfirmation) {
        window.app.globalObjects.pageClass = 'campaigns-view';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;


        var baseUrl = window.location.origin + '/Dashboard/Season';
        $scope.Model = {};
        var colors = ["#c2e4ff", "#8fcdff", "#5cb7ff", "#29a0ff", "#0088f5", "#006cc2", "#00508f", "#00335c"];

        $http.get(baseUrl + "/GetSeasonTitle", { params: { seasonDomain: $scope.MC.stateParams.seasonDomain } })
            .success(function (data, status, headers, config) {
                $scope.Model.Title = data.seasonTitle;
            });

        $scope.dataSourceBarChart = new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl + "/GetCapacityBarChartReport",
                    cache: false,
                    dataType: "json"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        return {
                            seasonDomain: $scope.MC.stateParams.seasonDomain,
                        }
                    }
                }
            },
                   });
        $scope.barChartOption = {
            dataSource: $scope.dataSourceBarChart,
            title: {
            },
            legend: {
                visible: false
            },
            chartArea: {
                background: "",
            },
            seriesDefaults: {
                type: "bar",
                stack: true,
                spacing: 0.7,
                gap: 0.30,
            },
            seriesColors: colors,
            series: [{
                field: "FillSpot",
                name: "Filled spots",
                opacity: 0.6,
                overlay: {
                    gradient: "none"
                },
                color: function (point) {

                    return "#71CAF2";
                }
            },

            {
                field: "MinimumCapacity",
                name: "Minimum capacity remaining",
                opacity: 0.4,
                overlay: {
                    gradient: "none"
                },
                color: function (point) {
                    return "#F4c514";
                }
            }
            , {
                field: "OpenSpot ",
                name: "Open spots",
                opacity: 0.3,
                highField: "Capacity",
                overlay: {
                    gradient: "none"
                },
               
                color: function (point) {

                    return "#ACDFFC";

                },
                tooltip: {
            visible: true,
            template: function (point) {
                if (point.dataItem.OpenSpot > 0 ) {
                    return point.series.name + ":" + (point.dataItem.Capacity - point.dataItem.FillSpot);
                }
                else {
                    return tooltip.hide();
                }
            }
        }
            },
            {
                field: "ExtraCapacity",
                name: "Over enrollment",
                opacity: 0.4,
                overlay: {
                    gradient: "none"
                },
                color: function (point) {
                    if (point.dataItem.ExtraCapacity > 0) {
                        return "#FA7578"
                    }
                }
            },
            {
                field: "WaitList",
                name: "Waitlist",
                opacity: 0.4,
                overlay: {
                    gradient: "none"
                },
                color: function (point) {
                    return "#8EFED6";
                },
                labels: {
                    visible: true,
                    template: "#if(dataItem.Capacity){# #= dataItem.Capacity# #if(dataItem.FillSpot>0){# | #=dataItem.FillSpot # #}else{# | #= '-' # #}# #if(dataItem.ExtraCapacity>0){# | #=dataItem.ExtraCapacity# #}else{# | #= '-' # #}# #if(dataItem.WaitList>0){# | #=dataItem.WaitList# #}else{# | #= '-' # #}# #}\
                                if(dataItem.Capacity<=0 ){# #= '-'# #if(dataItem.FillSpot>0){# | #=dataItem.FillSpot # #}else{# | #= '-' # #}# #if(dataItem.ExtraCapacity>0){# | #=dataItem.ExtraCapacity# #}else{# | #= '-' # #}# #if(dataItem.WaitList>0){# | #=dataItem.WaitList# #}else{# | #= '-' # #}# #}\
                               if(dataItem.ExtraCapacity==0 && dataItem.WaitList==0 && dataItem.FillSpot==0 && dataItem.ExtraCapacity==0){# #=''#  #}#"
                },
                tooltip: {
                    visible: true,
                    template: function (point) {
                        if (point.dataItem.WaitList > 0) {
                            return point.series.name + ":" + point.value;
                        } else {
                               return tooltip.hide();
                        }
                    }
                }
            },
            ],
            categoryAxis: {
                field: "ProgramFullName",
                labels: {
                    mirror: false    //Name program out of chart if false
                },
                majorGridLines: {
                    visible: false   // partitioning chart
                },
                majorTicks: {
                    visible: false
                },

            },
            valueAxis: {


                majorUnit: 5,
                line: {
                    visible: false
                },
                visible: true,
            },
            tooltip: {
                visible: true,
                template: function (point) {

                    return point.series.name + ":" + point.value;

                },
                color: "black"
            },
            dataBound: function (e) {
                var chart = $("#chart");
                if (chart.data("kendoChart").options.categoryAxis.dataItems.length > 30) {
                    chart.parents('#chartContainer').outerHeight(chart.data("kendoChart").options.categoryAxis.dataItems.length * 30);
                    chart.data("kendoChart").redraw();
                }
            }
        };

    }]);
})();