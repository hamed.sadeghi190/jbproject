﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Common.Exceptions;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Identity;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.WebLogic.UserService;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using Constants = Jumbula.Common.Constants.Constants;
using LogHelper = Jumbula.Common.Helper.LogHelper;
using Jumbula.Core.Model.Form;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class ClubController : DashboardBaseController
    {
        private readonly IApplicationUserManager<JbUser, int> _applicationUserManager;
        private readonly IFollowupFormBusiness _followupFormBusiness;
        private readonly IClubBusiness _clubBusiness;
        private readonly ILocationBusiness _locationBusiness;

        public ClubController(IClubBusiness clubBusiness, IApplicationUserManager<JbUser, int> applicationUserManager, IFollowupFormBusiness followupFormBusiness, ILocationBusiness locationBusiness)
        {
            _applicationUserManager = applicationUserManager;
            _followupFormBusiness = followupFormBusiness;
            _clubBusiness = clubBusiness;
            _locationBusiness = locationBusiness;
        }



        public virtual ActionResult Waivers()
        {
            return View();
        }

        [JbAuthorize(JbAction.Member_View)]
        public virtual ActionResult Clubs()
        {
            return View();
        }
        public virtual ActionResult EditClubUser()
        {
            return View();
        }
        public virtual ActionResult ResetPassword()
        {

            return View();
        }

        public virtual ActionResult ClubManage()
        {
            return View();
        }

        public virtual ActionResult ManageLocation()
        {
            return View();
        }
        public virtual ActionResult ClubSubsidiesManage()
        {
            return View();
        }
        public virtual ActionResult AddSubsidy()
        {
            return View();
        }
        public virtual ActionResult AdditionalInformation()
        {
            return View();
        }
        public virtual ActionResult SchoolDistrictManage()
        {
            return View();
        }
        public virtual ActionResult AddDistrict()
        {
            return View();
        }
        public virtual ActionResult AddDistrictStep2()
        {
            return View();
        }

        public ActionResult SeasonReplicate()
        {
            return View();
        }

        public virtual ActionResult GetWaiversPaging()
        {
            var model = Ioc.ClubBusiness.GetWaivers(ActiveClub.Id).Select(w =>
                new
                {
                    Name = w.Name,
                    Id = w.Id,
                    DateCreated =
                        Jumbula.Common.Helper.DateTimeHelper.ConvertUtcDateTimeToLocal(w.DateCreated, ActiveClub.TimeZone),
                    IsRequired = w.IsRequired ? "Yes" : "No",
                    Order = w.Order
                })
                .OrderBy(w => w.Order)
                .ToList();

            return JsonNet(new { DataSource = model, TotalCount = model.Count });
        }
        [HttpGet]
        public virtual JsonNetResult GetSchoolListForDropDown(string term)
        {
            var partnerId = Ioc.ClubBusiness.Get(ActiveClub.Id).PartnerId;
            var allschools = Ioc.ClubBusiness.GetAllPartnerSchools(partnerId.HasValue ? partnerId.Value : 0);
            var termValue = Request.Params["term"] ?? string.Empty;
            if (!string.IsNullOrEmpty(termValue))
            {
                allschools = allschools.Where(c => c.Name.StartsWith(termValue));
            }

            var model = allschools.OrderBy(c => c.Name).ToList().Select(c => new SelectKeyValue<int>()
            {
                Text = c.Name,
                Value = c.Id
            });

            return JsonNet(model, new JsonSerializerSettings());
        }

        [HttpGet]
        public virtual JsonNetResult GetClubSeasons(int clubId, string term, bool addPlaceHolder = false)
        {

            var allSeasons = Ioc.SeasonBusiness.GetList(clubId);
            var termValue = Request.Params["term"] ?? string.Empty;
            if (!string.IsNullOrEmpty(termValue))
            {
                allSeasons = allSeasons.Where(c => c.Title.StartsWith(termValue));
            }
            var model = new List<SelectKeyValue<long>>();
            if (addPlaceHolder)
            {
                model.Add(new SelectKeyValue<long>() { Text = "Select season ...", Value = 0 });
            }
            model.AddRange(allSeasons.OrderByDescending(c => c.MetaData.DateCreated).ToList().Select(c => new SelectKeyValue<long>()
            {
                Text = c.Title,
                Value = c.Id
            }).ToList());

            return JsonNet(model, new JsonSerializerSettings());
        }

        public virtual ActionResult GetWaivers()
        {

            var waivers = Ioc.ClubBusiness.GetWaivers(ActiveClub.Id).ToList().Select(d =>
                new SelectKeyValue<string>
                {
                    Text = d.Name,
                    Value = d.Id.ToString()
                });

            return JsonNet(waivers);
        }

        public virtual ActionResult GetWaiver(int waiverId)
        {

            var waiver = Ioc.ClubBusiness.GetWaivers(ActiveClub.Id).Single(w => w.Id == waiverId);

            return JsonNet(new { Id = waiver.Id, Name = waiver.Name, Text = waiver.Text, IsRequired = waiver.IsRequired, WaiverConfirmationType = waiver.WaiverConfirmationType });
        }

        [HttpGet]
        public virtual JsonNetResult ClubPage()
        {

            var model = new ClubPageViewModel();
            model.OrganizationTypes = new List<JbTitleValue>();
            model.OrganizationTypes.Add(new JbTitleValue() { Title = "All", Value = -1 });
            model.OrganizationTypes.AddRange(
                Ioc.ClubBusiness.GetClubTypeList()
                    .Where(c => c.ParentId.HasValue)
                    .GroupBy(c => new { id = c.ParentId.Value, name = c.ParentType.Name })
                    .Select(v => new JbTitleValue() { Title = v.Key.name, Value = v.Key.id })
                    .ToList());

            model.FilterTypes = DropdownHelpers.ToSelectList<FilterTypes>();

            model.OrganizationActiveTypes = new List<SelectKeyValue<bool?>>()
            {
                new SelectKeyValue<bool?>{Text = "All", Value = null},
                new SelectKeyValue<bool?>{Text = "Active", Value = true},
                new SelectKeyValue<bool?>{Text = "Inactive", Value = false},
            };

            return JsonNet(model);
        }

        public virtual ActionResult EditWaiver(ClubWaiversViewModel model)
        {
            if (ModelState.IsValid)
            {
                Ioc.ClubBusiness.UpdateWaiver(new ClubWaiver { Id = model.Id, Name = model.Name, Text = model.Text, IsRequired = model.IsRequired, WaiverConfirmationType = model.WaiverConfirmationType, Order = model.Order });

                return Json(new JResult { Status = true });
            }

            return JsonFormResponse();
        }

        public virtual ActionResult DeleteWaiver(int id)
        {
            Ioc.ClubBusiness.DeleteWaiver(id);

            return Json(new JResult { Status = true });
        }

        [HttpPost]
        public virtual ActionResult CreateWaiver(ClubWaiversViewModel model)
        {
            if (ModelState.IsValid)
            {
                var waivers = Ioc.ClubBusiness.Get(ActiveClub.Id).ClubWaivers;

                var order = -1;

                if (waivers != null && waivers.Any())
                {
                    order = waivers.OrderBy(w => w.Order).LastOrDefault().Order;
                }

                var result = Ioc.ClubBusiness.CreateWaiver(new ClubWaiver
                {
                    Club_Id = ActiveClub.Id,
                    DateCreated = DateTime.Now,
                    Name = model.Name,
                    Text = model.Text,
                    IsRequired = model.IsRequired,
                    WaiverConfirmationType = model.WaiverConfirmationType,
                    Order = ++order
                });

                return Json(new JResult { Status = result.Status });
            }

            return JsonFormResponse();
        }

        [HttpPost]
        public virtual ActionResult UpdateWaiverOrder(List<ClubWaiversViewModel> model)
        {
            var result = new OperationStatus();
            var clubBusiness = Ioc.ClubBusiness;
            var order = 0;

            var waiverIds = model.Select(w => w.Id).ToList();

            var waivers = clubBusiness.GetWaivers(waiverIds);

            foreach (var item in model)
            {
                var waiver = waivers.Single(w => w.Id == item.Id);

                waiver.Order = order;

                order++;
            }

            result = clubBusiness.Save();

            return Json(new JResult(result));
        }

        public virtual ActionResult GetLocation(int id)
        {
            CreateEditLocationViewModel model = null;

            if (id == 0)
            {
                model = new CreateEditLocationViewModel();
            }
            else
            {
                var locaton = _locationBusiness.GetClubLocation(id);

                model = new CreateEditLocationViewModel()
                {
                    Id = locaton.Id,
                    Name = locaton.Name,
                    Address = locaton.PostalAddress.Address
                };
            }

            return JsonNet(model);
        }

        public virtual ActionResult GetLocations()
        {
            var locations = _locationBusiness.GetClubLocationsByClubId(ActiveClub.Id).ToList().Select(d =>
                new SelectKeyValue<string>
                {
                    Text =
                        d.PostalAddress.Address +
                        (!string.IsNullOrEmpty(d.Name) ? $" ({d.Name})" : string.Empty),
                    Value = d.Id.ToString()
                });

            return JsonNet(locations);
        }

        public virtual ActionResult CreateLocation(CreateEditLocationViewModel model)
        {
            if (ModelState.IsValid)
            {
                PostalAddress postalAddress = new PostalAddress();

                GoogleMap.GetGeo(model.Address, ref postalAddress);

                var fullClub = Ioc.ClubBusiness.Get(ActiveClub.Id);

                ClubLocation clubLocation = new ClubLocation()
                {
                    Club = fullClub,
                    Name = model.Name,
                    PostalAddress = postalAddress
                };

                OperationStatus res = _locationBusiness.CreateLocation(clubLocation);

                return
                    Json(new JResult
                    {
                        Status = res.Status,
                        Message = res.Message,
                        RecordsAffected = res.RecordsAffected,
                        Data = clubLocation.Id.ToString()
                    });
            }

            return JsonFormResponse();
        }

        public virtual ActionResult EditLocation(CreateEditLocationViewModel model)
        {
            if (ModelState.IsValid)
            {
                PostalAddress postalAddress = new PostalAddress();

                GoogleMap.GetGeo(model.Address, ref postalAddress);

                var fullClub = Ioc.ClubBusiness.Get(ActiveClub.Id);

                ClubLocation clubLocation = new ClubLocation()
                {
                    Id = model.Id,
                    Club = fullClub,
                    Name = model.Name,
                    PostalAddress = postalAddress,
                };

                OperationStatus res = _locationBusiness.UpdateLocation(clubLocation);

                return
                    Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return JsonFormResponse();
        }

        public virtual ActionResult DeleteLocation(int id)
        {
            OperationStatus res = _locationBusiness.DeleteLocation(id);

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }

        public virtual ActionResult IsLocationAssignToAnotherProgram(long? programId, int locationId)
        {

            var result = _locationBusiness.IsLocationAssignToAnotherProgram(ActiveClub.Id, programId, locationId);

            return Json(result);
        }
        public virtual ActionResult DeleteFormTemplate(int id)
        {
            OperationStatus res = Ioc.ClubBusiness.DeleteFormTemplate(id);

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }

        public virtual ActionResult GetFormTemplates(FormType formType)
        {
            var formTemplates = Ioc.ClubBusiness.GetFormTemplates(ActiveClub.Id, formType).ToList().Select(d =>
                new SelectKeyValue<string>
                {
                    Text = d.Title,
                    Value = d.Id.ToString()
                })
                .ToList();

            if (formType == FormType.FollowUp)
            {
                formTemplates.AddRange(Ioc.ClubBusiness.GetFormTemplates(ActiveClub.Id, FormType.UploadForm).Select(s =>
                    new SelectKeyValue<string>
                    {
                        Value = s.Id.ToString(),
                        Text = s.Title,
                    })
                    .ToList());
            }

            return JsonNet(formTemplates);
        }

        public virtual ActionResult GetFormTemplatesPaging()
        {
            var model = Ioc.ClubBusiness.GetFormTemplates(ActiveClub.Id, FormType.FollowUp).Select(s =>
                new FollowUpFormViewModel
                {
                    Id = s.Id,
                    Title = s.Title,
                    Type = "Online",
                    DateCreated =
                        Jumbula.Common.Helper.DateTimeHelper.ConvertUtcDateTimeToLocal(s.DateCreated,
                            ActiveClub.TimeZone),
                    FollowUpFormMode = s.FollowUpFormMode,
                })
                .ToList();

            model.AddRange(Ioc.ClubBusiness.GetFormTemplates(ActiveClub.Id, FormType.UploadForm).Select(s =>
                new FollowUpFormViewModel
                {
                    Id = s.Id,
                    Title = s.Title,
                    Type = "Supplemental",
                    DateCreated =
                        Jumbula.Common.Helper.DateTimeHelper.ConvertUtcDateTimeToLocal(s.DateCreated,
                            ActiveClub.TimeZone),
                    FollowUpFormMode = s.FollowUpFormMode,

                })
                .ToList());

            model = model.OrderByDescending(s => s.Id).ToList();

            return JsonNet(new { DataSource = model, TotalCount = model.Count });
        }

        public virtual ActionResult GetJbForm(int templateId, string formType)
        {
            var model = new FormCreateEditViewModel();

            if (templateId != 0)
            {
                var formTemplates = Ioc.ClubBusiness.GetFormTemplates(ActiveClub.Id, null);

                model.FormTemplate = templateId.ToString();

                model.FormTemplates =
                    formTemplates.Where(f => f.FormType == EnumHelper.ParseEnum<FormType>(formType)).Select(c =>
                        new SelectKeyValue<string>
                        {
                            Text = c.Title,
                            Value = c.Id.ToString()
                        })
                        .OrderBy(s => s.Value)
                        .ToList();

                var formTemplate = formTemplates.SingleOrDefault(t => t.Id == templateId);
                CheckResourceForAccess(formTemplate);

                model.JbForm = formTemplate.JbForm;

                model.DefaultFormType = formTemplate.DefaultFormType;

                model.JbForm.CurrentMode = AccessMode.Design;

                model.JbForm.ChangeElementsMode(AccessMode.Edit, AccessMode.Design);
            }
            else
            {
                model.FormTemplates = new List<SelectKeyValue<string>>();

                model.JbForm = new JbForm();

                model.JbForm.RefEntityName = "Program";

                model.JbForm.Title = string.Empty;

                model.JbForm.CurrentMode = AccessMode.Design;

                model.JbForm.Elements.Add(new JbSection() { Title = "Section title", CurrentMode = AccessMode.Design });
            }

            return JsonNet(model);
        }

        public virtual ActionResult GetForm(int formId)
        {

            var model = new FormCreateEditViewModel();

            model.FormTemplates = new List<SelectKeyValue<string>>();
            var jbForm = Ioc.JbFormBusiness.Get(formId);
            model.JbForm = jbForm;
            model.JbForm.CurrentMode = AccessMode.Design;
            model.JbForm.ChangeElementsMode(AccessMode.Edit, AccessMode.Design);

            return JsonNet(model);
        }

        [HttpGet]
        public ActionResult GetSupplementalFormViewDetail(int formId)
        {
            var result = _followupFormBusiness.GetSupplementalForm(formId);

            return JsonNet(result);
        }

        public virtual ActionResult GetUrlForLogin(string clubDomain)
        {
            var url = string.Empty;
            var userName = this.GetCurrentUserName();

            var host = Request.Url.Host;
            var scheme = Request.Url.Scheme;
            var loginByTokenUrl = Ioc.UserProfileBusiness.GetUrlForLogin(clubDomain, userName);

            host = string.Concat(host.Split('.')[1], ".", host.Split('.')[2]);

            url = string.Concat(scheme, "://", clubDomain, ".", host, "/", loginByTokenUrl);

            var model = new { Url = url };

            return JsonNet(model);
        }

        public virtual ActionResult GetClubInfo()
        {
            var clubBaseInfo = this.GetCurrentClubBaseInfo();
            var club = Ioc.ClubBusiness.Get(clubBaseInfo.Id);

            var currentUserRole = this.GetCurrentUserRole();

            var isPartner = currentUserRole == RoleCategory.Partner;

            dynamic model = new ExpandoObject();

            model.Id = clubBaseInfo.Id;
            model.ClubType = clubBaseInfo.ClubType;
            model.Currency = clubBaseInfo.Currency;
            model.Domain = clubBaseInfo.Domain;
            model.HasPartner = clubBaseInfo.HasPartner;
            model.Id = clubBaseInfo.Id;
            model.IsSchool = clubBaseInfo.IsSchool;
            model.Logo = clubBaseInfo.Logo;
            model.Site = clubBaseInfo.Site;
            model.TimeZone = clubBaseInfo.TimeZone;
            model.IsPartner = isPartner;
            model.Role = currentUserRole;
            model.ShowWelcomeToJumbula = true;
            model.ShowSchoolsInHome = true;
            model.Name = clubBaseInfo.Name;

            var staffs = Ioc.ClubBusiness.GetClubsForUser(this.GetCurrentUserName(), currentUserRole).Where(s => !s.Club.IsDeleted && !s.IsDeleted && !s.Club.IsInActive && !s.Club.Domain.Equals(this.GetCurrentClubDomain(), StringComparison.CurrentCultureIgnoreCase)).ToList();

            if (staffs.Any())
            {
                model.IsMultipleClubAdmin = true;
                model.Clubs = staffs.Select(s =>
                   new
                   {
                       Name = s.Club.Name,
                       Domain = s.Club.Domain,
                       Role = s.RoleOnClub == RoleCategory.Parent ? "Family" : s.RoleOnClub.ToString()
                   })
                   .ToList();
            }

            var lastSeason = club.Seasons.LastOrDefault(s => s.Status != SeasonStatus.Deleted);

            model.Tour = new
            {
                LastSeasonDomain = lastSeason != null ? lastSeason.Domain : string.Empty,
                HasSeason = lastSeason != null ? true : false
            };
            if (clubBaseInfo.HasPartner)
            {
                model.PartnerSite = clubBaseInfo.PartnerSite;

                model.PartnerLogo = "/images/em-logo.jpg";

                if (club.PartnerClub.Domain.Equals("em", StringComparison.OrdinalIgnoreCase))
                {
                    model.PartnerLogo = "/images/em-logo.jpg";
                }
                else if (club.PartnerClub.Domain.Equals("baroodycamps", StringComparison.OrdinalIgnoreCase))
                {
                    model.PartnerLogo = "/images/baroody-logo.jpg";
                }
                else if (club.PartnerClub.Domain.Equals("generationinfocus", StringComparison.OrdinalIgnoreCase))
                {
                    model.PartnerLogo = "/images/generationinfocus-logo.jpg";
                }
                else
                {
                    model.PartnerLogo = clubBaseInfo.PartnerLogo;
                }

                var partnerSettings = Ioc.ClubBusiness.GetPartnerSetting(club.PartnerId.Value);

                model.ShowWelcomeToJumbula = partnerSettings == null || partnerSettings.PortalParameters == null || partnerSettings.PortalParameters.ShowWelocomeToJumbula;
                model.ShowSchoolsInHome = partnerSettings == null || partnerSettings.PortalParameters == null || partnerSettings.PortalParameters.ShowSchoolsInHome;

            }

            if (isPartner)
            {
                var partnerSetting = Ioc.ClubBusiness.GetPartnerSetting(clubBaseInfo.Id);
                model.PortalParameters = partnerSetting.PortalParameters != null ? partnerSetting.PortalParameters : new PartnerSettingPortalParameter();
            }

            return JsonNet(model);
        }


        [HttpGet]
        public virtual ActionResult ChangeClubPassword(int clubId)
        {
            try
            {
                ChangePasswordViewModel model = new ChangePasswordViewModel()
                {
                    ClubId = clubId,
                    ClubName = Ioc.ClubBusiness.Get(clubId).Name
                };

                if (this.GetCurrentUserRole() != RoleCategory.Partner)
                {
                    PageNotFound();
                }
                var clubStaff = Ioc.ClubBusiness.Get(clubId).ClubStaffs;

                model.ClubStaffs = clubStaff.Select(c => new SelectKeyValue<int>() { Text = string.Format("{0} {1} {2}", c.JbUserRole.User.UserName, c.Contact != null ? "(" + c.Contact.FirstName : "", c.Contact != null ? c.Contact.LastName + ")" : ""), Value = c.JbUserRole.UserId });

                return JsonNet(model);
            }
            catch
            {
                return JsonNet(new ChangePasswordViewModel() { ClubId = clubId });
            }
        }


        [HttpPost]
        public virtual ActionResult ChangeClubPassword(ChangePasswordViewModel model)
        {

            if (this.GetCurrentUserRole() != RoleCategory.Partner)
            {
                PageNotFound();
            }


            ModelState.Remove("model.OldPassword");
            if (model.SelectedStaff < 1)
            {
                ModelState.AddModelError("SelectedStaff", "Club staff is required");
            }
            if (ModelState.IsValid)
            {

                try
                {
                    var user = _applicationUserManager.FindById(model.SelectedStaff);

                    _applicationUserManager.UserTokenProvider = new DataProtectorTokenProvider<JbUser, int>(new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Sample").Create("EmailConfirmation"));

                    string token = _applicationUserManager.GeneratePasswordResetToken(user.Id);

                    if (!_applicationUserManager.ResetPassword(model.SelectedStaff, token, model.NewPassword).Succeeded)
                    {
                        throw new JumbulaTokenExpiredException(new Exception()); // is this true????????????
                    }
                }
                catch (Exception ex)
                {
                    Json(new JResult
                    {
                        Status = false,
                        Message = ex.Message,
                        RecordsAffected = 0
                    });
                }

                return
                    Json(new JResult
                    {
                        Status = true,
                        Message = "Password changed successfully.",
                        RecordsAffected = 1
                    });
            }

            return base.JsonFormResponse();
        }

        [HttpGet]
        public virtual ActionResult EditUserRole(int clubId)
        {
            if (this.GetCurrentUserRole() != RoleCategory.Partner)
            {
                PageNotFound();
            }
            try
            {
                EditClubUserRoleViewModel model = new EditClubUserRoleViewModel();

                var club = Ioc.ClubBusiness.Get(clubId);
                model.ClubName = club.Name;
                model.ClubUsers = club.ClubStaffs.Where(s => !s.IsDeleted).Select(c => new UserViewModel() { Id = c.JbUserRole.UserId, UserClubRole = c.JbUserRole.Role.Name, UserClubRoleId = c.UserRoleId, UserName = c.JbUserRole.User.UserName }).ToList();
                model.ClubId = clubId;
                return JsonNet(model);
            }
            catch
            {
                return JsonNet(new EditClubUserRoleViewModel());
            }
        }


        [HttpPost]
        public virtual ActionResult EditUserRole(EditClubUserRoleViewModel model)
        {
            var user = new JbUser();
            if (this.GetCurrentUserRole() != RoleCategory.Partner)
            {
                PageNotFound();
            }

            var newUser = updateUserCondition.NoChange;

            if (model.SelectedUserId < 1)
            {
                ModelState.AddModelError("SelectedUserId", "Club staff is required");
            }
            else
            {
                user = _applicationUserManager.FindById(model.SelectedUserId);

                if (!string.IsNullOrEmpty(model.NewUserName) && !user.UserName.Equals(model.NewUserName, StringComparison.OrdinalIgnoreCase))
                {
                    if (_applicationUserManager.IsUserExist(model.NewUserName))
                    {
                        newUser = updateUserCondition.ExistingUser;
                        if (model.ClubUsers.Any(c => c.UserName.ToLower() == model.NewUserName.ToLower()))
                        {
                            ModelState.AddModelError("NewUserName", "Sorry, it looks like " + model.NewUserName + " belongs to an existing user.");
                        }
                    }
                    else
                    { newUser = updateUserCondition.NewUser; }
                }
            }

            if (ModelState.IsValid)
            {

                try
                {
                    var res = new OperationStatus() { Status = false };
                    var isRoleChanged = false;
                    var userClubRole = Ioc.ClubBusiness.Get(model.ClubId).ClubStaffs.FirstOrDefault(c => c.JbUserRole.UserId == model.SelectedUserId);
                    if (userClubRole != null && !userClubRole.JbUserRole.Role.Name.Equals(model.SelectedRole, StringComparison.OrdinalIgnoreCase))
                    {
                        isRoleChanged = true;
                    }
                    var newrole = Ioc.UserProfileBusiness.GetRole((RoleCategory)Enum.Parse(typeof(RoleCategory), model.SelectedRole));
                    switch (newUser)
                    {
                        case updateUserCondition.NewUser:

                            user.UserName = model.NewUserName;
                            if (isRoleChanged)
                            {
                                user.Roles.First(c => c.Id == model.ClubUsers.FirstOrDefault(d => d.Id == model.SelectedUserId).UserClubRoleId).Role = newrole;
                            }

                            _applicationUserManager.Update(user);
                            break;
                        case updateUserCondition.NoChange:

                            if (isRoleChanged)
                            {
                                user.Roles.First(c => c.Id == model.ClubUsers.FirstOrDefault(d => d.Id == model.SelectedUserId).UserClubRoleId).Role = newrole;
                            }
                            _applicationUserManager.Update(user);

                            break;
                        case updateUserCondition.ExistingUser:
                            {
                                var newuser = _applicationUserManager.FindByName(model.NewUserName);
                                var club = Ioc.ClubBusiness.Get(model.ClubId);
                                club.ClubStaffs.Add(new ClubStaff() { Status = StaffStatus.Active, IsDeleted = false, ClubId = model.ClubId, JbUserRole = new JbUserRole() { UserId = newuser.Id, RoleId = newrole.Id } });

                                var deletedStaff = club.ClubStaffs.FirstOrDefault(c => c.JbUserRole.UserId == user.Id);

                                if (deletedStaff != null)
                                {
                                    var deleteStatus = Ioc.ClubBusiness.DeleteStaff(deletedStaff.Id);

                                    //club.ClubStaffs.Remove(deletedUser);
                                    if (deleteStatus.Status)
                                    {
                                        res = Ioc.ClubBusiness.Update(club);
                                    }
                                }
                            }
                            break;

                    };

                    return
                  Json(new JResult
                  {
                      Status = true,
                      Message = "User information updated successfully.",
                      RecordsAffected = 1
                  });
                }
                catch (Exception ex)
                {
                    Json(new JResult
                    {
                        Status = false,
                        Message = ex.Message,
                        RecordsAffected = 0
                    });
                }


            }

            return base.JsonFormResponse();
        }

        public virtual JsonNetResult GetSubClubs(PaginationModel paginationModel, string term, string typeId,
           string startDate, string endDate, string typeMember, string activeType = "Active", FilterTypes filterType = FilterTypes.Name)
        {
            try
            {
                var relatedClubs = Enumerable.Empty<Club>().AsQueryable();

                if (typeMember == "Schools")
                {
                    relatedClubs = Ioc.ClubBusiness.GetRelatedClubs(ActiveClub.Id).Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.School);
                }
                else
                {
                    relatedClubs = Ioc.ClubBusiness.GetRelatedClubs(ActiveClub.Id).Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.Provider);
                }

                if (!string.IsNullOrEmpty(term))
                {
                    switch (filterType)
                    {
                        case FilterTypes.Name:
                            {
                                relatedClubs = relatedClubs.Where(c => c.Name.ToLower().Contains(term.ToLower()));
                            }
                            break;
                        case FilterTypes.Domain:
                            {
                                relatedClubs = relatedClubs.Where(c => c.Domain.ToLower().Contains(term.ToLower()));
                            }
                            break;
                        default:
                            break;
                    }
                }

                if (activeType == "Active")
                {
                    relatedClubs = relatedClubs.Where(c => c.IsInActive == false);
                }
                if (activeType == "Inactive")
                {
                    relatedClubs = relatedClubs.Where(c => c.IsInActive == true);
                }

                if (filterType == FilterTypes.ExpirationDate && (!string.IsNullOrWhiteSpace(startDate) || !string.IsNullOrWhiteSpace(endDate)))
                {
                    var listRelatedClubs = relatedClubs.ToList();

                    if (!string.IsNullOrWhiteSpace(startDate) && !string.IsNullOrWhiteSpace(endDate))
                    {
                        var sDate = DateTime.Parse(startDate);
                        var edate = DateTime.Parse(endDate);
                        edate = edate.AddHours(24 - edate.Hour);

                        relatedClubs =
                            listRelatedClubs.Where(
                                oitem =>
                                    (oitem.ExpirationDate.HasValue && oitem.ExpirationDate.Value >= sDate.Date) && (oitem.ExpirationDate.HasValue && oitem.ExpirationDate.Value < edate.Date)).AsQueryable();

                    }
                    else if (!string.IsNullOrWhiteSpace(startDate))
                    {
                        var date = DateTime.Parse(startDate);
                        relatedClubs =
                            listRelatedClubs.Where(
                                oitem =>
                                    oitem.ExpirationDate.HasValue && oitem.ExpirationDate.Value >= date.Date).AsQueryable();
                    }

                    else if (!string.IsNullOrWhiteSpace(endDate))
                    {
                        var date = DateTime.Parse(endDate);
                        date = date.AddHours(24 - date.Hour);
                        relatedClubs =
                           listRelatedClubs.Where(
                                oitem =>
                                    oitem.ExpirationDate.HasValue && oitem.ExpirationDate.Value < date.Date).AsQueryable();
                    }
                }

                var counts = relatedClubs.Count();
                var pagedResult = relatedClubs.OrderBy(c => c.Name).Skip((paginationModel.Page - 1) * paginationModel.PageSize).Take(paginationModel.PageSize);

                var model = pagedResult.ToList().Select(c => new ClubViewModel()
                {
                    County = c.Address != null ? (!string.IsNullOrEmpty(c.Address.County) ? c.Address.County : "-") : "-",
                    Description = c.Description,
                    Domain = c.Domain,
                    AgreementExpirationDate = c.ExpirationDate,
                    Id = c.Id,
                    Name = c.Name,
                    Phone = c.ContactPersons != null ? c.ContactPersons.First().Phone : "",
                    ExtensionPhone = c.ContactPersons != null ? c.ContactPersons.First().PhoneExtension : "",
                    Email = c.ContactPersons != null ? c.ContactPersons.First().Email : "",
                    FirstName = c.ContactPersons != null ? c.ContactPersons.First().FirstName : "",
                    LastName = c.ContactPersons != null ? c.ContactPersons.First().LastName : "",
                    typeMerge = (c.ClubType != null && c.ClubType.ParentType != null) ? string.Format(Constants.F_CategoryFormat, c.ClubType.ParentType.Name, c.ClubType.Name) : string.Empty,
                    CommissionRateString = !c.IsSchool ? c.PartnerCommisionRate.ToString() : "-",
                    ClubType = c.ClubType.ParentType.Name,
                });

                var datasource = model.ToList();
                return JsonNet(new { data = datasource, total = counts }, new Newtonsoft.Json.JsonSerializerSettings(), "");

            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
            return JsonNet(null, new Newtonsoft.Json.JsonSerializerSettings(), "");
        }


        public virtual JsonNetResult CreateEditClub(int clubId, string type)
        {
            ClubViewModel model = new ClubViewModel(type, true);

            model.AllStaffTitles.Add(new SelectKeyValue<string>() { Text = "Select", Value = null });

            var staffTitlesSorted = Ioc.ClubBusiness.GetStaffTitles(DropdownHelpers.ToSelectList<StaffTitle>());

            model.AllStaffTitles.AddRange(staffTitlesSorted);


            var partnerClub = Ioc.ClubBusiness.Get(ActiveClub.Id);

            model.AllCategories = Ioc.CategoryBuisness.GetList()
                .Where(e => e.ParentId != null)
                .ToList().Select(x => new SelectKeyValue<int>() { Group = x.ParentCategory.Name, Text = x.Name, Value = x.Id }).ToList();
            model.PartnerAdmins = Ioc.ClubBusiness.GetAllUserWithSpecificRolesForClubs(ActiveClub.Id,
                 new List<RoleCategory> { RoleCategory.Admin, RoleCategory.Contributor, RoleCategory.Instructor, RoleCategory.Manager, RoleCategory.Partner, RoleCategory.SchoolContributor })
                 .Select(c => new SelectKeyValue<int>() { Text = c.UserName, Value = c.Id }).ToList();

            var partnerAdminKeys = model.PartnerAdmins.Select(p => p.Value).ToList();
            model.PartnerMessageResponders = partnerClub.ClubStaffs.Where(s => s.Status == StaffStatus.Active && !s.IsFrozen && !s.IsDeleted && partnerAdminKeys.Contains(s.JbUserRole.UserId)).Select(s => new SelectKeyValue<int> { Value = s.Id, Text = s.JbUserRole.User.UserName }).ToList();

            if (clubId > 0)
            {
                var club = Ioc.ClubBusiness.Get(clubId);

                model.Name = club.Name;
                model.Id = club.Id;
                model.Site = club.Site;
                model.Domain = club.Domain;
                model.Phone = club.ContactPersons.Any() ? club.ContactPersons.First().Phone : "";
                model.ExtensionPhone = club.ContactPersons.Any() ? club.ContactPersons.First().PhoneExtension : "";
                model.Email = club.ContactPersons.Any() ? club.ContactPersons.First().Email : "";

                model.CustomAttributes = new ClubCustomAttributesViewModel(club);

                model.FirstName = club.ContactPersons.Any() ? club.ContactPersons.First().FirstName : "";
                model.LastName = club.ContactPersons.Any() ? club.ContactPersons.First().LastName : "";
                model.Title = club.ContactPersons.Any() ? club.ContactPersons.First().Title : null;
                model.SelectedCategory = club.CategoryId.HasValue ? club.CategoryId.Value : 0;
                model.SelectedSubType = club.TypeId;
                model.SelectedTimeZone = ((int)club.TimeZone).ToString();
                //model.SelectedRole = club.ClubStaffs.First().JbUserRole.Role.Name;            
                model.CommissionRate = club.PartnerCommisionRate;
                model.Description = club.Description;
                model.Address = club.ClubLocations != null & club.ClubLocations.Any() ? club.ClubLocations.FirstOrDefault().PostalAddress.Address : "";
                model.IsInActive = club.IsInActive;

                if (club.Setting != null)
                {
                    model.MemberSince = club.Setting.MemberSince;
                    model.AgreementExpirationDate = club.Setting.AgreementExpirationDate;
                    model.TaxIDFEIN = club.Setting.TaxIDFEIN;
                    model.TaxIDSSN = club.Setting.TaxIDSSN;
                    model.EMDiscountSchools = club.Setting.EMDiscountSchools;
                    model.Provider1099 = club.Setting.Provider1099;
                    model.TaxExemptNonprofit = club.Setting.TaxExemptNonprofit;
                    model.SelectedMessageResponder = club.Setting.PartnerMessageResponder;
                }


                var clubAdmins = club.ClubStaffs.Where(c => c.Status != StaffStatus.Pending && !c.IsDeleted && c.JbUserRole.Role.Name.Equals(RoleCategory.Admin.ToString(), StringComparison.OrdinalIgnoreCase)
                                                                                            || c.JbUserRole.Role.Name.Equals(RoleCategory.Contributor.ToString(), StringComparison.OrdinalIgnoreCase)
                                                                                            || c.JbUserRole.Role.Name.Equals(RoleCategory.Instructor.ToString(), StringComparison.OrdinalIgnoreCase)
                                                                                            || c.JbUserRole.Role.Name.Equals(RoleCategory.Manager.ToString(), StringComparison.OrdinalIgnoreCase)
                                                                                            || c.JbUserRole.Role.Name.Equals(RoleCategory.Partner.ToString(), StringComparison.OrdinalIgnoreCase)
                                                                                            || c.JbUserRole.Role.Name.Equals(RoleCategory.SchoolContributor.ToString(), StringComparison.OrdinalIgnoreCase));



                model.IsSchool = club.IsSchool;

            }
            else
            {
                model.CustomAttributes = new ClubCustomAttributesViewModel(partnerClub);
            }

            return JsonNet(model);
        }

        public virtual ActionResult IsClubSchool(int typeId)
        {
            var result = Ioc.ClubBusiness.IsClubSchool(typeId);

            return JsonNet(new { result = result, Status = true });
        }

        public virtual JsonNetResult GetClubStaff(string term)
        {


            if (!string.IsNullOrEmpty(term))
            {
                term = term.ToLower();
            }

            IQueryable<JbUser> query = Ioc.ClubBusiness.GetAllUserWithSpecificRoleForClubs(ActiveClub.Id,
                RoleCategory.Admin);

            if (!string.IsNullOrEmpty(term))
            {
                term = term.ToLower();

                query = query.Where(item => item.UserName.ToLower().StartsWith(term));


            }
            if (query.Any())
            {
                var total = query.Count();
                var model = query.Select(u => new UserViewModel()
                {
                    Id = u.Id,
                    UserName = u.UserName
                }).ToList();


                return JsonNet(new { Data = model, Total = total });

            }
            return JsonNet(new { Data = new List<UserViewModel>(), Total = 0 });
        }

        [HttpPost]
        public virtual JsonResult DeleteClub(int clubId)
        {
            var club = Ioc.ClubBusiness.Get(clubId);
            var res = new OperationStatus() { Status = false, Message = Constants.F_Club_Delete };

            if ((!Ioc.OrderBusiness.GetList().Any(c => c.ClubId == clubId && c.OrderStatus == OrderStatusCategories.completed)) && club.ClubType.EnumType != ClubTypesEnum.SchoolDistrict)
            {
                res = Ioc.ClubBusiness.Delete(clubId);
            }
            else if (club.ClubType.EnumType == ClubTypesEnum.SchoolDistrict)
            {
                var listUpdateSchools = new List<Club>();
                var schoolsDistrict = Ioc.ClubBusiness.GetList().Where(s => s.DistrictId == club.Id);

                if (schoolsDistrict != null && schoolsDistrict.Any())
                {
                    schoolsDistrict.ToList().ForEach(s => s.DistrictId = null);

                    foreach (var school in schoolsDistrict)
                    {
                        if (school.Setting != null)
                        {
                            school.Setting.NCESID = null;
                            school.Setting.FirstProviderId = null;
                            school.Setting.SecondProviderId = null;
                        }

                        listUpdateSchools.Add(school);
                    }

                    Ioc.ClubBusiness.UpdateClubs(listUpdateSchools);
                }

                res = Ioc.ClubBusiness.Delete(clubId);
            }

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }

        [HttpPost]
        public virtual ActionResult SaveClub(ClubViewModel model)
        {
            try
            {
                var address = new PostalAddress();
                CheckModelIntegrity(ModelState, model, ref address);

                if (ModelState.IsValid)
                {

                    OperationStatus res;

                    if (model.Id > 0)
                    {
                        res = EditClub(model, address);
                    }
                    else
                    {
                        res = CreateClub(model, address);
                    }

                    return
                        Json(new JResult
                        {
                            Status = res.Status,
                            Message = res.Message,
                            RecordsAffected = res.RecordsAffected
                        });
                }
                return JsonFormResponse();
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
            return Json(new JResult { Status = false, Message = Constants.M_E_CreateEdit, RecordsAffected = 0 });
        }

        public virtual ActionResult GetProvidersSchools()
        {
            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);

            var currentClubId = this.GetCurrentClubId();
            var currentClub = Ioc.ClubBusiness.Get(currentClubId);

            var partner = currentClub.PartnerClub;

            var schools = Ioc.ClubBusiness.GetAllPartnerSchools(partner.Id).Where(s => !s.IsInActive);

            var model = schools
                              .OrderBy(item => item.Name)
                              .Skip((page - 1) * pageSize)
                              .Take(pageSize).ToList()
                              .Select(s => new { Name = s.Name, County = s.ClubLocations.Any() ? (!string.IsNullOrEmpty(s.ClubLocations.First().PostalAddress.County) ? s.ClubLocations.First().PostalAddress.County : Constants.S_Dash) : Constants.S_Dash });

            return JsonNet(new { DataSource = model, TotalCount = schools.Count() });
        }
        [HttpGet]
        public virtual ActionResult GetSubsidyInformation()
        {
            var model = new SubsidyViewModel();

            model.ClubId = ActiveClub.Id;
            var listStates = Ioc.CountryBusiness.GetStates().ToList();
            model.States = listStates.Select(s => new SelectKeyValue<int> { Value = s.Id, Text = s.Name }).ToList();

            return JsonNet(model);
        }
        [HttpPost]
        public virtual ActionResult SaveSubsidy(SubsidyViewModel model)
        {
            if (model.SelectedState == 0)
            {
                ModelState.AddModelError("model.SelectedState", "State is required.");
            }
            if (model.Name != null)
            {
                var Allsubsidies = Ioc.SubsidyBusiness.GetAllSubsidiesClub(ActiveClub.Id);
                var IsNameInDb = Allsubsidies.Where(s => s.Name == model.Name);
                if (IsNameInDb.Any())
                {
                    ModelState.AddModelError("model.Name", "This name exists.");
                }
            }

            if (ModelState.IsValid)
            {
                OperationStatus res = new OperationStatus();

                var subsidy = new Subsidy()
                {
                    PartnerId = model.ClubId,
                    Name = model.Name,
                    State = model.SelectedState,
                    MetaData = new MetaData()
                    {
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow
                    },
                    Website = model.WebSite,
                };

                res = Ioc.SubsidyBusiness.Create(subsidy);

                return Json(new JResult { Data = subsidy.Id.ToString(), Status = res.Status, Message = "", RecordsAffected = 0 });
            }
            return JsonFormResponse();
        }
        [HttpGet]
        public virtual JsonNetResult CreateSubsidyStep2(int subsidyId)
        {
            var model = new SubsidyStep2ViewModel();
            var subsidy = Ioc.SubsidyBusiness.Get(subsidyId);

            var listStates = Ioc.CountryBusiness.GetStates().ToList();

            model.States = listStates.Select(s => new SelectKeyValue<int> { Value = s.Id, Text = s.Name }).ToList();
            model.SelectedState = subsidy.State;
            model.Name = subsidy.Name;
            model.Id = subsidy.Id;
            model.WebSite = subsidy.Website;
            model.Instructions = subsidy.Instructions;

            var subsidyContact = subsidy.ContactId != null ? subsidy.Contact : null;

            if (subsidyContact != null)
            {
                model.FirstName = subsidyContact.FirstName;
                model.LastName = subsidyContact.LastName;
                model.Phone = subsidyContact.Phone;
                model.Email = subsidyContact.Email;

            }
            return JsonNet(model);
        }
        [HttpPost]
        public virtual ActionResult SaveSubsidyStep2(SubsidyStep2ViewModel model)
        {
            OperationStatus res = new OperationStatus();
            var subsidy = Ioc.SubsidyBusiness.Get(model.Id);

            if (model.SelectedState == 0)
            {
                ModelState.AddModelError("model.SelectedState", "State is required.");
            }
            if (model.Name != null)
            {
                var Allsubsidies = Ioc.SubsidyBusiness.GetAllSubsidiesClub(ActiveClub.Id);
                var IsNameInDb = Allsubsidies.Where(s => s.Name == model.Name && s.Name != subsidy.Name);
                if (IsNameInDb.Any())
                {
                    ModelState.AddModelError("model.Name", "This name exists.");
                }
            }

            if (ModelState.IsValid)
            {
                subsidy.Name = model.Name;
                subsidy.State = model.SelectedState;
                subsidy.Website = model.WebSite;
                subsidy.Instructions = model.Instructions;

                if (!string.IsNullOrEmpty(model.FirstName) || !string.IsNullOrEmpty(model.LastName) || !string.IsNullOrEmpty(model.Email) || !string.IsNullOrEmpty(model.Phone))
                {
                    subsidy.Contact = new ContactPerson()
                    {
                        Email = model.Email,
                        Phone = model.Phone,
                        LastName = model.LastName,
                        FirstName = model.FirstName,
                        IsPrimary = true
                    };
                }

                subsidy.MetaData = new MetaData()
                {
                    DateCreated = DateTime.UtcNow,
                    DateUpdated = DateTime.UtcNow
                };

                res = Ioc.SubsidyBusiness.Update(subsidy);

                return Json(new JResult { Status = res.Status, Message = "", RecordsAffected = 0 });
            }

            return JsonFormResponse();
        }

        [HttpPost]
        public virtual JsonNetResult GetAllSubsidies(PaginationModel paginationModel, string term)
        {
            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);
            var Subsidies = Ioc.SubsidyBusiness.GetAllSubsidiesClub(ActiveClub.Id).ToList();

            if (!string.IsNullOrEmpty(term))
            {
                Subsidies = Subsidies.Where(s => s.Name.Contains(term)).ToList();
            }

            var model = Subsidies.OrderByDescending(c => c.Name).Skip((paginationModel.Page - 1) * paginationModel.PageSize).Take(paginationModel.PageSize).ToList().Select(c => new SubsidyViewModel()
            {
                Name = c.Name,
                Phone = c.Contact != null ? c.Contact.Phone != null ? PhoneNumberHelper.FormatPhoneNumber(c.Contact.Phone) : "-" : "-",
                Email = c.Contact != null ? c.Contact.Email != null ? c.Contact.Email : "-" : "-",
                WebSite = c.Website != null ? c.Website : "-",
                StateName = Ioc.CountryBusiness.GetState(c.State).Name,
                Id = c.Id
            });

            var datasource = model.ToList();
            return JsonNet(new { data = datasource, total = Subsidies.Count() });
        }
        [HttpGet]
        public virtual JsonNetResult GetCreateDistrict()
        {
            var model = new AddDistrictViewModel();
            var club = ActiveClub;

            model.ClubId = club.Id;
            var listCountries = Ioc.CountryBusiness.GetUsAndCsCountries();
            model.Countries = listCountries.Select(s => new SelectKeyValue<int> { Value = s.Id, Text = s.Name }).ToList();

            return JsonNet(model);

        }
        [HttpPost]
        public virtual ActionResult SaveDistrictStep1(AddDistrictViewModel model)
        {
            var address = new PostalAddress();
            ValidationCheckDistrict(ModelState, model, ref address);

            if (ModelState.IsValid)
            {
                OperationStatus res;

                Club club = new Club();
                var date = DateTime.UtcNow;
                club.MetaData = new MetaData()
                {
                    DateCreated = date,
                    DateUpdated = date
                };
                club.Address = address;
                club.ClubLocations = new List<ClubLocation>()
                        {
                            new ClubLocation()
                            {
                                Name = string.Empty,
                                PostalAddress = address
                            }

                        };

                club.Currency = CurrencyCodes.USD;
                club.TimeZone = (Jumbula.Common.Enums.TimeZone)(int.Parse(model.SelectedTimeZone));
                club.PartnerId = ActiveClub.Id;
                club.Client = new Client();
                club.Client.PaymentMethods = new ClientPaymentMethod();
                var priceplan = Ioc.PricePlanBusiness.GetFreeTrialPlan();
                club.Client.PricePlan = priceplan;
                club.Client.PricePlanId = priceplan.Id;

                club.Name = model.Name.Replace(Constants.S_DoubleQuote, string.Empty)
                .Replace(Constants.S_BackSlash, string.Empty);
                club.Domain = model.Domain;

                var typeId = Ioc.ClubBusiness.GetClubTypeList().Where(t => t.EnumType == ClubTypesEnum.SchoolDistrict).FirstOrDefault().Id;
                club.TypeId = typeId;
                res = Ioc.ClubBusiness.Create(club);

                if (res.Status)
                {
                    var clubId = Ioc.ClubBusiness.Get(model.Domain).Id;
                    var page = Ioc.PageBusiness.GetDefaultPage(club);

                    page.Header.LogoUrl = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);

                    if (string.IsNullOrEmpty(page.Header.LogoUrl))
                    {
                        page.Header.LogoUrl = "~/Images/club-nosport.png";
                    }

                    page.SaveType = SaveType.Publish;
                    page.ClubId = clubId;
                    Ioc.PageBusiness.CreateEdit(page);
                }

                return Json(new JResult { Data = club.Id.ToString(), Status = res.Status, Message = "", RecordsAffected = 0 });
            }

            return JsonFormResponse();
        }
        public virtual JsonNetResult FillSchoolGridForDistrict(PaginationModel paginationModel, List<int> schoolIds)
        {
            var model = new List<SchoolDistrictGridViewModel>();

            if (schoolIds != null)
            {
                var schools = Ioc.ClubBusiness.GetRelatedClubs(ActiveClub.Id).Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.School).Where(s => schoolIds.ToList().Contains(s.Id)).ToList();

                model = schools.OrderByDescending(c => c.Id).Select(c => new SchoolDistrictGridViewModel()
                {
                    Name = c.Name,
                    Domain = c.Domain,
                    FirstProviderId = c.Setting.FirstProviderId != null ? c.Setting.FirstProviderId : "",
                    SecondProviderId = c.Setting.SecondProviderId != null ? c.Setting.SecondProviderId : "",
                    NCESId = c.Setting.NCESID > 0 ? c.Setting.NCESID : null,
                    Id = c.Id,
                }).ToList();
            }

            return JsonNet(model);

        }

        public virtual JsonNetResult GetAllSchoolDistricts(PaginationModel paginationModel, string term)
        {
            var partner = ActiveClub;
            var schoolDistrict = Ioc.ClubBusiness.GetList().Where(c => c.ClubType.EnumType == ClubTypesEnum.SchoolDistrict && c.PartnerId == partner.Id && !c.IsDeleted);

            if (!string.IsNullOrEmpty(term))
            {
                schoolDistrict = schoolDistrict.Where(s => s.Name.Contains(term));
            }

            var model = schoolDistrict.ToList().Select(c => new ClubViewModel
            {
                Name = c.Name,
                Domain = c.Domain,
                Address = c.Address.Address,
                Id = c.Id
            });

            var datasource = model.ToList().OrderBy(c => c.Name).Page(paginationModel);
            return JsonNet(new { data = datasource, total = schoolDistrict.Count() });
        }
        private void ValidationCheckDistrict(ModelStateDictionary modelState, AddDistrictViewModel model, ref PostalAddress address)
        {
            if (!string.IsNullOrEmpty(model.Address))
            {
                try
                {

                    GoogleMap.GetGeo(model.Address, ref address);
                }
                catch
                {
                    ModelState.AddModelError("Address", "Address is not valid.");
                }
            }

            if (!string.IsNullOrEmpty(model.Domain) && Ioc.ClubBusiness.IsClubDomainExist(model.Domain, model.Id))
            {
                ModelState.AddModelError("Domain", "The domain already exists.");
            }
            if (model.Address == null)
            {
                ModelState.AddModelError("model.Address", "");
            }
            if (model.SelectedTimeZone == "-1")
            {
                ModelState.AddModelError("model.SelectedTimeZone", "Time zone is required.");
            }
        }
        [HttpGet]
        public virtual JsonNetResult CreateDistrictStep2(int clubId)
        {
            var model = new AddInfoDistrictViewModel();
            var partner = Ioc.ClubBusiness.Get(ActiveClub.Id);
            var district = Ioc.ClubBusiness.Get(clubId);

            model = FillStaffsDistrict(district, model);
            model.AllSubsidies = Ioc.SubsidyBusiness.GetAllSubsidiesClub(ActiveClub.Id).Select(c => new SelectKeyValue<int>() { Text = c.Name, Value = c.Id }).ToList();

            var schools =
               Ioc.ClubBusiness.GetRelatedClubs(ActiveClub.Id)
                   .Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.School && (c.DistrictId == null || c.DistrictId == district.Id))
                   .Select(s => new SelectKeyValue<int>() { Text = s.Name, Value = s.Id })
                   .OrderBy(s => s.Text).ToList();

            model.AllSchools = schools;

            var contactDistrict = Ioc.ClubBusiness.GetDistrictContact(clubId).
                Where(c => c.Title == StaffTitle.SchoolSuperintendent).FirstOrDefault();

            if (contactDistrict != null)
            {
                model.SuperintendentEmail = contactDistrict.Email;
                model.SuperintendentName = contactDistrict.FirstName;
                model.SuperintendentLastName = contactDistrict.LastName;
                model.SuperintendentPhone = contactDistrict.Phone;
            }

            model.AllValueforStaffingRatio = GetValueForStaffingRatio();

            if (district.Setting != null)
            {
                if (district.Setting.SalesContactUserId > 0)
                {
                    model.SelectedSalesContact = district.Setting.SalesContactUserId;
                }
                if (!string.IsNullOrEmpty(district.Setting.RevenueShare))
                {
                    model.RevenueShare = district.Setting.RevenueShare;
                }
                if (district.Setting.StaffingRatio > 0)
                {
                    model.SelectedStaffingRatio = district.Setting.StaffingRatio;
                }
                if (district.Setting.NCESID > 0)
                {
                    model.NcesId = district.Setting.NCESID;
                }
            }

            var districtSchools = Ioc.ClubBusiness.GetAllPartnerSchools(ActiveClub.Id).Where(c => c.DistrictId == district.Id).ToList();
            model.SelectedSchools = districtSchools.Select(s => s.Id).ToList();

            var districtSubsidies = Ioc.ClubBusiness.GetAllSubsidiesDistrict(clubId).ToList().OrderBy(s => s.Id);
            model.SelectedSubsidies = districtSubsidies.Select(s => s.SubsidyId).ToList();

            //model.SelectedSchools=
            model.Name = district.Name;
            model.Domain = district.Domain;
            model.Address = district.ClubLocations != null & district.ClubLocations.Any() ? district.ClubLocations.FirstOrDefault().PostalAddress.Address : "";
            model.SelectedTimeZone = ((int)district.TimeZone).ToString();
            model.DistrictId = clubId;

            return JsonNet(model);
        }
        [HttpPost]
        public virtual ActionResult SaveDistrictStep2(AddInfoDistrictViewModel model)
        {
            var address = new PostalAddress();
            ValidationCheckDistrictStep2(ModelState, model, ref address);

            OperationStatus res = new OperationStatus();

            if (ModelState.IsValid)
            {
                var clubSchools = Ioc.ClubBusiness.GetAllPartnerSchools(ActiveClub.Id);
                var district = Ioc.ClubBusiness.Get(model.DistrictId);
                var partner = district.PartnerClub;

                district.Name = model.Name;
                district.Domain = model.Domain;
                district.TimeZone = (Jumbula.Common.Enums.TimeZone)(int.Parse(model.SelectedTimeZone));
                district.Address = address;

                if (district.ClubLocations != null && district.ClubLocations.Any())
                {
                    district.ClubLocations.First().PostalAddress = address;
                }
                else
                {
                    district.ClubLocations = new List<ClubLocation>()
                   {
                    new ClubLocation()
                    {
                        Name = string.Empty,
                        PostalAddress = address
                    }
                  };
                }

                //Add area manager staffs
                if (model.SelectedAreaManager != null && model.SelectedAreaManager.Any())
                {
                    var districtStaffs = district.ClubStaffs.Where(c => c.Contact != null && c.Contact.Title == StaffTitle.AreaManager);
                    district = Ioc.ClubBusiness.SetStaffsForNewClub(district, districtStaffs, model.SelectedAreaManager);
                }
                else
                {
                    district.ClubStaffs.Where(c => c.Contact != null && c.Contact.Title == StaffTitle.AreaManager).ToList().ForEach(s => s.IsDeleted = true);
                }

                //Add regionalr director staffs
                if (model.SelectedRegionalDirector != null && model.SelectedRegionalDirector.Any())
                {
                    var districtStaffs = district.ClubStaffs.Where(c => c.Contact != null && c.Contact.Title == StaffTitle.SchoolRegionalDirector);
                    district = Ioc.ClubBusiness.SetStaffsForNewClub(district, districtStaffs, model.SelectedRegionalDirector);
                }
                else
                {
                    district.ClubStaffs.Where(c => c.Contact != null && c.Contact.Title == StaffTitle.SchoolRegionalDirector).ToList().ForEach(s => s.IsDeleted = true);
                }

                if (model.SelectedSalesContact > 0)
                {
                    var salescontact = Ioc.ClubBusiness.GetDistrictContact(district.Id).Where(c => c.Title == StaffTitle.SalesContact).FirstOrDefault();
                    if (salescontact != null)
                    {
                        Ioc.ContactPersonBusiness.Delete(salescontact.Id);
                    }

                    var contact = partner.ClubStaffs.Where(s => s.Id == model.SelectedSalesContact).FirstOrDefault().Contact;
                    var contactPerson = new ContactPerson()
                    {
                        FirstName = contact.FirstName,
                        LastName = contact.LastName,
                        Email = contact.Email,
                        Cell = contact.Cell,
                        Nickname = contact.Nickname,
                        DoB = contact.DoB,
                        Phone = contact.Phone,
                        Employer = contact.Employer,
                        PhoneExtension = contact.PhoneExtension,
                        Work = contact.Work,
                        Occupation = contact.Occupation,
                        Club_Id = district.Id,
                        Title = StaffTitle.SalesContact,
                        IsPrimary = false
                    };
                    district.ContactPersons.Add(contactPerson);
                }
                else
                {
                    var salescontact = Ioc.ClubBusiness.GetDistrictContact(district.Id).Where(c => c.Title == StaffTitle.SalesContact).FirstOrDefault();
                    if (salescontact != null)
                    {
                        Ioc.ContactPersonBusiness.Delete(salescontact.Id);
                    }
                }

                if (district.Setting == null)
                {
                    district.Setting = new ClubSetting();
                }

                var salesContact = (district.Setting).SalesContactUserId;
                var revenueShare = (district.Setting).RevenueShare;
                var staffingRatio = (district.Setting).StaffingRatio;
                var ncesId = (district.Setting).NCESID;

                salesContact = model.SelectedSalesContact.HasValue ? model.SelectedSalesContact.Value : 0;
                district.Setting.SalesContactUserId = salesContact;

                revenueShare = model.RevenueShare != null ? model.RevenueShare : null;
                district.Setting.RevenueShare = revenueShare;

                staffingRatio = model.SelectedStaffingRatio.HasValue ? model.SelectedStaffingRatio.Value : 0;
                district.Setting.StaffingRatio = staffingRatio;

                ncesId = model.NcesId.HasValue ? model.NcesId.Value : 0;
                district.Setting.NCESID = ncesId;

                var contactDistrict = Ioc.ClubBusiness.GetDistrictContact(district.Id).
                    Where(c => c.Title == StaffTitle.SchoolSuperintendent).FirstOrDefault();

                if (contactDistrict != null)
                {
                    contactDistrict.FirstName = model.SuperintendentName;
                    contactDistrict.LastName = model.SuperintendentLastName;
                    contactDistrict.Email = model.SuperintendentEmail;
                    contactDistrict.Phone = model.SuperintendentPhone;
                    contactDistrict.Title = StaffTitle.SchoolSuperintendent;
                }
                else if (!string.IsNullOrEmpty(model.SuperintendentEmail) || !string.IsNullOrEmpty(model.SuperintendentName) || !string.IsNullOrEmpty(model.SuperintendentPhone) || !string.IsNullOrEmpty(model.SuperintendentLastName))
                {
                    ContactPerson contact = new ContactPerson()
                    {
                        Email = model.SuperintendentEmail,
                        Club_Id = district.Id,
                        Phone = model.SuperintendentPhone,
                        FirstName = model.SuperintendentName,
                        LastName = model.SuperintendentLastName,
                        Title = StaffTitle.SchoolSuperintendent,
                        IsPrimary = true
                    };

                    district.ContactPersons.Add(contact);
                }

                var districtSubsidies = Ioc.ClubBusiness.GetAllSubsidiesDistrict(district.Id).ToList();

                if (model.ListSelectedSubsidiesForSave != null)
                {
                    if (districtSubsidies.Count > 0)
                    {
                        var subsidyForDelete = districtSubsidies.Where(s => !model.ListSelectedSubsidiesForSave.Contains(s.SubsidyId)).ToList();

                        foreach (var item in subsidyForDelete)
                        {
                            model.DeletedSubsidiesId.Add(item.SubsidyId);
                        }
                    }

                    var listSelectedSubsidies = model.ListSelectedSubsidiesForSave.Distinct();

                    foreach (var item in listSelectedSubsidies)
                    {
                        if (item > 0)
                        {
                            var isDistrictSubsidyInDb = districtSubsidies.Where(s => s.SubsidyId == item).FirstOrDefault();

                            if (isDistrictSubsidyInDb == null)
                            {
                                ClubSubsidy clubSubsidies = new ClubSubsidy()
                                {
                                    SubsidyId = item,
                                };
                                district.DistrictSubsidies.Add(clubSubsidies);
                            }
                        }
                    }
                }
                else if (districtSubsidies.Count > 0)
                {
                    model.DeletedSubsidiesId.AddRange(districtSubsidies.Select(s => s.SubsidyId));
                }

                if (model.DeletedSubsidiesId.Count > 0)
                {
                    var deletesubsidies = districtSubsidies.Where(c => model.DeletedSubsidiesId.Contains(c.SubsidyId)).ToList();
                    Ioc.ClubBusiness.DeleteDistrictSubSidies(deletesubsidies);
                }

                var districtSchools = Ioc.ClubBusiness.GetAllPartnerSchools(ActiveClub.Id).Where(c => c.DistrictId == district.Id).ToList();
                var schoolIds = new List<int>();

                if (model.SelectedSchools != null && model.ListSelectedSchool != null)
                {
                    var listSaveSchool = model.ListSelectedSchool.Select(s => s.Id).ToList();
                    if (districtSchools.Count > 0)
                    {
                        foreach (var item in districtSchools)
                        {
                            item.Setting.FirstProviderId = null;
                            item.Setting.SecondProviderId = null;
                            item.Setting.NCESID = null;
                        }

                        Ioc.ClubBusiness.UpdateClubs(districtSchools);

                        schoolIds.AddRange(listSaveSchool);
                    }
                    else
                    {
                        schoolIds.AddRange(listSaveSchool);
                    }

                    var schools = AsignDestrictToSchools(district.Id, schoolIds);

                    //Asign provider id to district schools
                    if (model.ListSelectedSchool != null)
                    {
                        var schoolsHaveProviderId = model.ListSelectedSchool.Where(c => c.Id != 0 && clubSchools.Select(s => s.Id).ToList().Contains(c.Id)).ToList();
                        var result = AsignProviderIdToDistrictSchools(schools.ToList(), schoolsHaveProviderId);
                    }
                }
                else if (districtSchools.Count > 0)
                {
                    foreach (var item in districtSchools)
                    {
                        item.Setting.FirstProviderId = null;
                        item.Setting.SecondProviderId = null;
                        item.Setting.NCESID = null;
                    }

                    var schoolDistrict = districtSchools.Where(c => c.DistrictId == district.Id).ToList();
                    schoolDistrict.ForEach(s => s.DistrictId = null);

                    Ioc.ClubBusiness.UpdateClubs(districtSchools);
                }

                district.IsSettingChanged = true;
                res = Ioc.ClubBusiness.Update(district);

                return Json(new JResult { Status = res.Status, Message = "", RecordsAffected = 0 });
            }

            return JsonFormResponse();
        }
        private void ValidationCheckDistrictStep2(ModelStateDictionary modelState, AddInfoDistrictViewModel model, ref PostalAddress address)
        {
            if (!string.IsNullOrEmpty(model.Address))
            {
                try
                {
                    GoogleMap.GetGeo(model.Address, ref address);
                }
                catch
                {
                    ModelState.AddModelError("Address", "Address is not valid.");
                }
            }

            if (!string.IsNullOrEmpty(model.Domain) && Ioc.ClubBusiness.IsClubDomainExist(model.Domain, model.DistrictId))
            {
                ModelState.AddModelError("Domain", "The domain already exists.");
            }
            if (model.Address == null)
            {
                ModelState.AddModelError("model.Address", "");
            }
            if (model.SelectedTimeZone == "-1")
            {
                ModelState.AddModelError("model.SelectedTimeZone", "Time zone is required.");
            }
        }

        public virtual JsonNetResult FillSubsidyGridForDistrict(int districtId, List<int> subsidyIds)
        {
            var allSubsidies = new List<Subsidy>();
            var model = new List<SubsidyViewModel>();

            if (subsidyIds != null && subsidyIds.Where(i => i != 0).ToList().Count > 0)
            {
                var districtSubsidiesInDb = Ioc.ClubBusiness.GetAllSubsidiesDistrict(districtId).Select(s => s.Subsidy);
                allSubsidies.AddRange(districtSubsidiesInDb.Where(s => subsidyIds.Contains(s.Id)));

                var newSubsidiesId = subsidyIds.Where(c => !districtSubsidiesInDb.Select(s => s.Id).ToList().Contains(c));
                if (newSubsidiesId.Any() || newSubsidiesId != null)
                {
                    var newSubsidies = Ioc.SubsidyBusiness.GetAllSubsidiesClub(ActiveClub.Id).Where(s => newSubsidiesId.ToList().Contains(s.Id));
                    allSubsidies.AddRange(newSubsidies);
                }


                model = allSubsidies.ToList().Select(c => new SubsidyViewModel
                {
                    Name = c.Name,
                    Id = c.Id,
                    StateName = Ioc.CountryBusiness.GetState(c.State).Name,
                    WebSite = c.Website != null ? c.Website : null,

                }).ToList();
            }


            var dataSource = model.ToList();

            return JsonNet(new { DataSource = dataSource, TotalCount = allSubsidies.Count() });
        }

        [HttpPost]
        public ActionResult SaveHomePageReplicate(int districtId, int? memberId)
        {
            var result = Ioc.PageBusiness.Replicate(districtId, memberId);

            return JsonNet(new { Status = true });
        }

        private OperationStatus CreateClub(ClubViewModel model, PostalAddress address)
        {

            Club club = new Club();

            club.ClubStaffs = new List<ClubStaff>();
            club.PartnerCommisionRate = model.CommissionRate;
            club.Setting = new ClubSetting()
            {
                AgreementExpirationDate = model.AgreementExpirationDate,
                EMDiscountSchools = model.EMDiscountSchools,
                MemberSince = model.MemberSince,
                TaxIDFEIN = model.TaxIDFEIN,
                TaxIDSSN = model.TaxIDSSN,
                Provider1099 = model.Provider1099,
                TaxExemptNonprofit = model.TaxExemptNonprofit,
                PartnerMessageResponder = model.SelectedMessageResponder
            };

            var partnetusers = Ioc.ClubBusiness.GetAllUserWithSpecificRolesForClubs(ActiveClub.Id,
                new List<RoleCategory> { RoleCategory.Admin, RoleCategory.Contributor, RoleCategory.Instructor, RoleCategory.Manager, RoleCategory.Partner, RoleCategory.SchoolContributor });

            var contactPerson = new ContactPerson()
            {
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Phone = model.Phone,
                Title = model.Title,
                PhoneExtension = model.ExtensionPhone,
                IsPrimary = true
            };
            club.ContactPersons.Add(contactPerson);
            var date = DateTime.UtcNow;
            club.MetaData = new MetaData()
            {
                DateCreated = date,
                DateUpdated = date
            };

            club.Address = address;
            club.Site = model.Site;
            club.IsInActive = model.IsInActive;

            club.ClubLocations = new List<ClubLocation>()
            {
                new ClubLocation()
                {
                    Name = string.Empty,
                    PostalAddress = address
                }
            };
            club.Currency = CurrencyCodes.USD;
            club.TimeZone = (Jumbula.Common.Enums.TimeZone)(int.Parse(model.SelectedTimeZone));
            club.PartnerId = ActiveClub.Id;
            club.Client = new Client();
            club.Client.PaymentMethods = new ClientPaymentMethod();
            var priceplan = Ioc.PricePlanBusiness.GetFreeTrialPlan();
            club.Client.PricePlan = priceplan;
            club.Client.PricePlanId = priceplan.Id;

            club.Name = model.Name.Replace(Constants.S_DoubleQuote, string.Empty)
                .Replace(Constants.S_BackSlash, string.Empty);
            club.Domain = model.Domain;

            if (model.SelectedCategory > 0)
            {
                club.CategoryId = model.SelectedCategory;
            }

            club.TypeId = model.SelectedSubType;
            club.Description = model.Description;

            var clubAttributeValues = new Dictionary<AttributeName, string>();

            if (model.IsSchool)
            {
                clubAttributeValues.Add(AttributeName.PTAFee, model.CustomAttributes.PTAFee != null ? model.CustomAttributes.PTAFee.ToString() : string.Empty);
            }
            else
            {
                clubAttributeValues.Add(AttributeName.LeadGeneration, model.CustomAttributes.LeadGeneration != null ? model.CustomAttributes.LeadGeneration.ToString() : string.Empty);
            }

            Ioc.ClubBusiness.SetAttributes(club, clubAttributeValues, ActiveClub.Id);

            var result = Ioc.ClubBusiness.Create(club);
            if (result.Status)
            {
                try
                {
                    var clubId = Ioc.ClubBusiness.Get(model.Domain).Id;
                    var page = Ioc.PageBusiness.GetDefaultPage(club);

                    page.Header.LogoUrl = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);

                    if (string.IsNullOrEmpty(page.Header.LogoUrl))
                    {
                        page.Header.LogoUrl = "~/Images/club-nosport.png";
                    }

                    page.SaveType = SaveType.Publish;
                    page.ClubId = clubId;
                    Ioc.PageBusiness.CreateEdit(page);
                }
                catch (Exception ex)
                {
                    LogHelper.LogException(ex);
                }
            }
            return result;
        }

        private OperationStatus EditClub(ClubViewModel model, PostalAddress address)
        {
            var club = Ioc.ClubBusiness.Get(model.Id);
            club.MetaData.DateUpdated = DateTime.UtcNow;

            List<JbUserRole> deletedUserRoles = null;

            club.Setting.AgreementExpirationDate = model.AgreementExpirationDate;
            club.Setting.EMDiscountSchools = model.EMDiscountSchools;
            club.Setting.MemberSince = model.MemberSince;
            club.Setting.TaxIDFEIN = model.TaxIDFEIN;
            club.Setting.TaxIDSSN = model.TaxIDSSN;
            club.Setting.Provider1099 = model.Provider1099;
            club.Setting.TaxExemptNonprofit = model.TaxExemptNonprofit;

            club.Setting.PartnerMessageResponder = model.SelectedMessageResponder;

            club.IsSettingChanged = true;
            var contactPerson = club.ContactPersons.First(c => c.IsPrimary);

            contactPerson.Email = model.Email;
            contactPerson.FirstName = model.FirstName;
            contactPerson.LastName = model.LastName;
            contactPerson.Phone = model.Phone;
            contactPerson.PhoneExtension = model.ExtensionPhone;
            contactPerson.Title = model.Title;
            club.PartnerCommisionRate = model.CommissionRate;
            club.Address = address;
            club.IsInActive = model.IsInActive;
            club.TimeZone = (Common.Enums.TimeZone)(int.Parse(model.SelectedTimeZone));

            var clubAttributeValues = new Dictionary<AttributeName, string>();

            if (model.IsSchool)
            {
                clubAttributeValues.Add(AttributeName.PTAFee, model.CustomAttributes.PTAFee != null ? model.CustomAttributes.PTAFee.ToString() : string.Empty);
            }
            else
            {
                clubAttributeValues.Add(AttributeName.LeadGeneration, model.CustomAttributes.LeadGeneration != null ? model.CustomAttributes.LeadGeneration.ToString() : string.Empty);
            }

            Ioc.ClubBusiness.SetAttributes(club, clubAttributeValues);

            if (club.ClubLocations != null && club.ClubLocations.Any())
            {
                club.ClubLocations.First().PostalAddress = address;
            }
            else
            {
                club.ClubLocations = new List<ClubLocation>()
                {
                    new ClubLocation()
                    {
                        Name = string.Empty,
                        PostalAddress = address
                    }
                };
            }

            var partnetusers = Ioc.ClubBusiness.GetAllUserWithSpecificRolesForClubs(ActiveClub.Id,
                 new List<RoleCategory> { RoleCategory.Admin, RoleCategory.Contributor, RoleCategory.Instructor, RoleCategory.Manager, RoleCategory.Partner, RoleCategory.SchoolContributor }).ToList();

            var clubAdmins = club.ClubStaffs;//.Where(c => c.JbUserRole.Role.Name.Equals(RoleCategory.Admin.ToString(), StringComparison.OrdinalIgnoreCase));
            var partnetusersId = partnetusers.Select(c => c.Id);
            var oldAdmins =
                 clubAdmins.Where(
                     c =>
                        partnetusersId.Contains(c.JbUserRole.UserId));

            club.Name = model.Name.Replace(Constants.S_DoubleQuote, string.Empty)
                    .Replace(Constants.S_BackSlash, string.Empty);
            club.Domain = model.Domain;
            if (model.SelectedCategory > 0)
            {
                club.CategoryId = model.SelectedCategory;
            }
            else
            {
                club.CategoryId = null;
            }
            club.TypeId = model.SelectedSubType;
            club.Description = model.Description;
            club.Site = model.Site;

            return Ioc.ClubBusiness.Update(club, deletedUserRoles);
        }

        [HttpGet]
        public ActionResult GetSeasonReplicate(int clubId, int? memberId)
        {
            DistrictSeasonReplicateViewModel model = new DistrictSeasonReplicateViewModel();

            var seasons = Ioc.SeasonBusiness.GetList(clubId);

            model.Seasons.Add(new SelectKeyValue<long?> { Value = null, Text = "Select season to replicate" });

            model.Seasons.AddRange(seasons
                                    .Select(s =>
                                             new SelectKeyValue<long?>
                                             {
                                                 Text = s.Title,
                                                 Value = s.Id
                                             })
                                             .ToList());

            model.DistrictId = clubId;

            if (memberId.HasValue && memberId.Value != 0)
            {
                model.MemberId = memberId;
                var member = Ioc.ClubBusiness.Get(memberId.Value);
                model.MemberName = member.Name;
            }

            return JsonNet(model);
        }

        [HttpPost]
        public ActionResult SaveSeasonReplicate(DistrictSeasonReplicateViewModel model)
        {
            var hasAnyMember = Ioc.ClubBusiness.GetRelatedClubs(model.DistrictId).Any();

            if (!hasAnyMember)
            {
                return JsonNet(new JResult() { Status = false, Message = "This district has no any member." });
            }

            if (ModelState.IsValid)
            {
                var district = Ioc.ClubBusiness.Get(model.DistrictId);

                var season = Ioc.SeasonBusiness.Get(model.SelectedSeason.Value);

                var reuslt = Ioc.SeasonBusiness.ReplicateDistrictSeasonToMembers(district.Id, season.Id, model.MemberId);

                return JsonNet(new JResult(reuslt));
            }

            return JsonFormResponse();
        }

        [HttpGet]
        public virtual JsonNetResult GetAllMembers()
        {
            var clubId = ActiveClub.Id;

            var allMembers = new List<SelectKeyValue<long>>();

            allMembers =
              Ioc.ClubBusiness.GetRelatedClubs(clubId)
                  .Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.School || c.ClubType.ParentId.Value == (int)ClubTypesEnum.Provider)
                  .Select(c => new SelectKeyValue<long> { Text = c.Name, Value = c.Id })
                  .OrderBy(c => c.Text)
                  .ToList();


            var model = new { AllMembers = allMembers, IsAllSchools = "true" };

            return JsonNet(model);
        }

        private void CheckModelIntegrity(ModelStateDictionary ModelState, ClubViewModel model, ref PostalAddress address)
        {

            if (!string.IsNullOrEmpty(model.Address))
            {
                try
                {
                    GoogleMap.GetGeo(model.Address, ref address);
                }
                catch
                {
                    ModelState.AddModelError("Address", "Address is not valid.");
                }
            }

            if (!string.IsNullOrEmpty(model.Domain) && Ioc.ClubBusiness.IsClubDomainExist(model.Domain, model.Id))
            {
                ModelState.AddModelError("Domain", "The domain already exists.");
            }
            if (model.SelectedTimeZone == "-1")
            {
                ModelState.AddModelError("SelectedTimeZone", "Time zone is required");
            }
            if (model.SelectedSubType == 0)
            {
                ModelState.AddModelError("SelectedSubType", "Type is required");
            }

        }
        private IQueryable<Club> AsignDestrictToSchools(int destrictId, List<int> schoolIds)
        {
            var schools = Ioc.ClubBusiness.GetAllPartnerSchools(ActiveClub.Id);
            if (schools.Where(c => c.DistrictId == destrictId).Any())
            {
                var schoolDistrict = schools.Where(c => c.DistrictId == destrictId).ToList();
                schoolDistrict.ForEach(s => s.DistrictId = null);
            }
            schools.Where(s => schoolIds.Contains(s.Id)).ToList().ForEach(c => c.DistrictId = destrictId);

            return schools;
        }
        private OperationStatus AsignProviderIdToDistrictSchools(List<Club> schools, List<SchoolDistrictGridViewModel> schoolsModel)
        {
            OperationStatus res = new OperationStatus();
            var listUpdatedSchool = new List<Club>();

            var schoolAddInEdit = schoolsModel.Where(n => !schools.Select(s => s.Id).Contains(n.Id));

            foreach (var school in schools)
            {
                foreach (var item in schoolsModel)
                {
                    if (school.Id == item.Id)
                    {
                        if (school.Setting == null)
                        {
                            school.Setting = new ClubSetting();
                        }

                        var firstProviderId = (school.Setting).FirstProviderId;
                        var secondProviderId = (school.Setting).SecondProviderId;
                        var ncesId = (school.Setting).NCESID;

                        firstProviderId = !string.IsNullOrEmpty(item.FirstProviderId) ? item.FirstProviderId : null;
                        school.Setting.FirstProviderId = firstProviderId;

                        secondProviderId = !string.IsNullOrEmpty(item.SecondProviderId) ? item.SecondProviderId : null;
                        school.Setting.SecondProviderId = secondProviderId;

                        ncesId = item.NCESId.HasValue ? item.NCESId.Value : 0;
                        school.Setting.NCESID = ncesId;

                        listUpdatedSchool.Add(school);
                    }
                }
            }


            if (listUpdatedSchool.Count > 0)
            {
                Ioc.ClubBusiness.UpdateClubs(listUpdatedSchool);
            }
            else
            {
                res.Status = true;
            }

            return res;
        }
        private AddInfoDistrictViewModel FillStaffsDistrict(Club district, AddInfoDistrictViewModel model)
        {
            var partner = Ioc.ClubBusiness.Get(ActiveClub.Id);
            var districtStaffs = district.ClubStaffs.Where(s => s.Status == StaffStatus.Active && !s.IsFrozen && !s.IsDeleted);

            var usersClub = Ioc.ClubBusiness.GetAllUserWithSpecificRolesForClubs(ActiveClub.Id,
                 new List<RoleCategory> { RoleCategory.Admin, RoleCategory.Contributor, RoleCategory.Instructor, RoleCategory.Manager, RoleCategory.Partner, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.RestrictedManager, RoleCategory.OnsiteSupportManager })
                 .Select(c => new SelectKeyValue<int>() { Text = c.UserName, Value = c.Id });

            var partnerAdminKeys = usersClub.Select(p => p.Value).ToList();
            var activeStaffs = partner.ClubStaffs.Where(s => s.Status == StaffStatus.Active && !s.IsFrozen && !s.IsDeleted && partnerAdminKeys.Contains(s.JbUserRole.UserId));

            model.AreaManagerStaffs = activeStaffs.Where(s => s.Contact != null && s.Contact.Title == StaffTitle.AreaManager).Select(s => new SelectKeyValue<int> { Value = s.Id, Text = s.JbUserRole.User.UserName }).OrderBy(s => s.Text).ToList();
            model.RegionalDirectoStaffs = activeStaffs.Where(s => s.Contact != null && s.Contact.Title == StaffTitle.SchoolRegionalDirector).Select(s => new SelectKeyValue<int> { Value = s.Id, Text = s.JbUserRole.User.UserName }).OrderBy(s => s.Text).ToList();
            model.SalesContactStaffs = activeStaffs.Where(s => s.Contact != null && s.Contact.Title == StaffTitle.SalesContact).Select(s => new SelectKeyValue<int> { Value = s.Id, Text = s.JbUserRole.User.UserName }).OrderBy(s => s.Text).ToList();

            if (districtStaffs != null && districtStaffs.Any())
            {
                model.SelectedAreaManager = model.AreaManagerStaffs.Where(a => districtStaffs.Select(s => s.JbUserRole.User.UserName).ToList().Contains(a.Text)).Select(d => d.Value).ToList();
                model.SelectedRegionalDirector = model.RegionalDirectoStaffs.Where(a => districtStaffs.Select(s => s.JbUserRole.User.UserName).ToList().Contains(a.Text)).Select(d => d.Value).ToList();
            }

            return model;
        }
        private enum updateUserCondition
        {
            NewUser,
            ExistingUser,
            NoChange
        }
        private List<SelectKeyValue<int>> GetValueForStaffingRatio()
        {
            var result = new List<SelectKeyValue<int>>();
            for (int i = 8; i < 26; i++)
            {
                result.Add(new SelectKeyValue<int> { Text = i.ToString(), Value = i });

            }

            return result;
        }
    }
}