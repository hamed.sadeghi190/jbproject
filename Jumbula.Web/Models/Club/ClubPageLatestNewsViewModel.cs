﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class ClubPageLatestNewsViewModel
    {
        public ClubPageLatestNewsViewModel()
        {
            EndPageElement = false;
        }

        public string ClubDomain { get; set; }
        public int ClubCategoryId { get; set; }
        public int NewsId { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public DateTime DateTime { get; set; }
        public int NewsCount { get; set; }
        public int PageNo { get; set; }
        public bool EndPageElement { get; set; }
    }

}