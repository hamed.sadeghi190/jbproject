﻿using System;

namespace Jumbula.Common.Exceptions
{
    public class JumbulaAccountAlreadyConfirmedException : JumbulaAccountException
    {
        public JumbulaAccountAlreadyConfirmedException(Exception e)
            : base(e)
        {
        }
    }

}
