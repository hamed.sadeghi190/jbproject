﻿using System;
using System.Collections.Generic;
using System.Linq;
using AuthorizeNet.Util;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Mvc.UIHelpers;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ClassProfitsViewModel
    {
        public ClassProfitsViewModel()
        {

            AllSeason = SeedSeasonValue();
            YearSeason = SeedYearValue();
        }
        List<SelectKeyValue<string>> SeedSeasonValue()
        {

            return DropdownHelpers.ToSelectList<SeasonNames>();
        }
        public List<SelectKeyValue<string>> AllSeason { get; set; }

        public List<SelectKeyValue<int>> YearSeason { get; set; }
        List<SelectKeyValue<int>> SeedYearValue()
        {

            var result = new List<SelectKeyValue<int>>()
            {
                new SelectKeyValue<int> { Value = 0, Text = "Select year"},
                new SelectKeyValue<int> { Value = 2016, Text = "2016"},
                new SelectKeyValue<int> { Value = 2017, Text = "2017"},
                new SelectKeyValue<int> { Value = 2018, Text = "2018"},
                new SelectKeyValue<int> { Value = 2019, Text = "2019"},
                new SelectKeyValue<int> { Value = 2020, Text = "2020"},
                new SelectKeyValue<int> { Value = 2021, Text = "2021"},
                new SelectKeyValue<int> { Value = 2022, Text = "2022"},
                new SelectKeyValue<int> { Value = 2023, Text = "2023"},
                new SelectKeyValue<int> { Value = 2024, Text = "2024"},
                new SelectKeyValue<int> { Value = 2025, Text = "2025"},

        };
            return result;
        }
        public string ProviderName { get; set; }
        public string SchoolName { get; set; }
        public string ClassName { get; set; }
        public int NumberOfWeeks { get; set; }
        public string Days { get; set; }
        public CurrencyCodes Currency { get; set; }
        public int Students { get; set; }
        public string StrStudents => string.Format("{0}", Students.ToString());
        public decimal ParentFee { get; set; }
        public string StrParentFee => CurrencyHelper.FormatCurrencyWithPenny(ParentFee, Currency, true);
        public decimal GrossIncome => Students * ParentFee;
        public string StrGrossIncome => CurrencyHelper.FormatCurrencyWithPenny(GrossIncome, Currency, true); 
        public decimal PTAFee { get; set; }
        public string StrPTAFee => CurrencyHelper.FormatCurrencyWithPenny(PTAFee, Currency, true); 

        public decimal TotalPTAFee => Students * PTAFee;
        public string StrTotalPTAFee => CurrencyHelper.FormatCurrencyWithPenny(TotalPTAFee, Currency, true);

        public decimal GrossPTAFee => GrossIncome - TotalPTAFee;
        public string StrGrossPTAFee => CurrencyHelper.FormatCurrencyWithPenny(GrossPTAFee, Currency, true); 
        public decimal VendorFee { get; set; }
        public string StrVendorFee => CurrencyHelper.FormatCurrencyWithPenny(VendorFee, Currency, true); 

        public decimal GrossVendorFee => Students * VendorFee;
        public string StrGrossVendorFee => CurrencyHelper.FormatCurrencyWithPenny(GrossVendorFee, Currency, true);  

        public decimal PaytoVendor => (1 - PercentLeadGen / 100) * GrossVendorFee;
        public string StrPaytoVendor => CurrencyHelper.FormatCurrencyWithPenny(PaytoVendor, Currency, true); 

        public decimal FXAProfit => GrossPTAFee - PaytoVendor;
        public string StrFXAProfit => CurrencyHelper.FormatCurrencyWithPenny(FXAProfit, Currency,true); 

        public decimal LeadGen => GrossVendorFee * PercentLeadGen / 100;
        public string StrLeadGen => CurrencyHelper.FormatCurrencyWithPenny(LeadGen, Currency, true); 

        public decimal MgmtFee
        {
            get
            {
                try
                {
                    return Math.Round(PercentMgmtFee / 100 * GrossPTAFee);
                }
                catch (DivideByZeroException)
                {
                    return 0;
                }
            }
        }
        public string StrMgmtFee => CurrencyHelper.FormatCurrencyWithPenny(MgmtFee, Currency); 
        public decimal PercentLeadGen { get; set; }
        public string StrPercentLeadGen => $"{PercentLeadGen}{"%"}";

        public decimal PercentMgmtFee
        {
            get
            {
                try
                {
                    return Math.Round(Math.Abs((1 - (VendorFee / (ParentFee - PTAFee))) * 100), 2);
                }
                catch(DivideByZeroException)
                {
                    return 0;
                }
            }
        }
        public string StrPercentMgmtFee => $"{PercentMgmtFee}%";
    }

}