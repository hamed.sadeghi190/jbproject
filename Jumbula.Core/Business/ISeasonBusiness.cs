﻿using Jb.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;
using Jumbula.Core.Model.Generic;
using System.Web.Mvc;

namespace Jumbula.Core.Business
{
    public interface ISeasonBusiness
    {
        IQueryable<Season> GetList();
        IQueryable<Season> GetList(int clubId);

        Season Get(long Id);
        Season Get(string seasonDomain, int clubId);
        OperationStatus Create(Season season);
        OperationStatus Update(Season season);
        OperationStatus Delete(Season season);
        OperationStatus Delete(long seasonId);

        OperationStatus SaveEmailTemplate(EmailTemplate template, long seasonId);

        OperationStatus UpdateEmailTemplate(EmailTemplate template, long seasonId);
        EmailTemplate GetSeasonEmailTemplate(long seasonId);

        bool DoesSeasonDomainExist(string domain, int clubId);

        string GenerateSeasonDomain(string seasonTitle, int clubId);
        bool DoesSeasonTitleExist(string title, int clubId, long seasonId);

        DateTime GetFirstStartDateClassInSeason(string seasonDomain);

        SeasonRegStatus GetRegStatus(Season season);

        ClubFormTemplate GetSeasonRegistrationForm(Season season);

        List<ClubWaiver> GetSeasonWaivers(Season season);

        DateTime? GetGeneralRegOpenDate(Season season);

        DateTime? GetGeneralRegCloseDate(Season season);

        DateTime? GetLotteryCloseDate(Season season);

        bool IsPreRegistrationMode(Season season);

        bool IsLotteryMode(Season season);

        bool IsGeneralRegistrationOpen(Season season);

        OperationStatus ReplicateDistrictSeasonToMembers(int districtId, long seasonId, int? memberId);
        Charge GetApplicationFee(Season season);
        List<SelectListItem> GetKeyValueClubSeasons(int clubId, bool includeAllOption);

        bool HasSeasonWaiver(Season season);

        List<PaymentPlan> GetMultRegisterPaymentPlan(Season season, int userId);

        bool HasSeasonMultRegisterPaymentPlan(Season season, int userId);

        List<SelectKeyValue<long>> GetLiveSeasonsClub(int clubId, string exceptedSeasonDomains = null);
    }
}
