﻿

namespace Jumbula.Common.Enums
{
    public enum PolicyCategories
    {
        Refund = 1, // start with 1 b/c the Id of first policy entry is db is 1
        Waiver,
        MedicalRelease,
        General,
        Proof

    }

}
