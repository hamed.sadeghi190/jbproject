﻿using System;
using System.Web.Mvc;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Model.ActivityLog;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Controllers;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class ActivityLogController : DashboardBaseController
    {
        #region Fields
        private readonly IAuditBusiness _auditBusiness;
        #endregion

        #region Constractor
        public ActivityLogController(IAuditBusiness auditBusiness)
        {
            _auditBusiness = auditBusiness;
        }

        #endregion


        #region ActionMethods

        [HttpGet]
        public JsonNetResult GetFilters()
        {
            var result = _auditBusiness.GetFilters();

            return JsonNet(result);
        }

        [HttpPost]
        public JsonNetResult GetList(PaginationModel paginationModel, ActivityLogFilterViewModel filters)
        {
            var result = _auditBusiness.GetAdminAudits(ActiveClub.Domain, filters, paginationModel.Sort, paginationModel.Page, paginationModel.PageSize);

            return JsonNet(result);
        }

        [HttpGet]
        public JsonNetResult GetDetail(Guid id)
        {
            var result = _auditBusiness.GetDetail(id);

            return JsonNet(result);
        }
        #endregion
    }
}