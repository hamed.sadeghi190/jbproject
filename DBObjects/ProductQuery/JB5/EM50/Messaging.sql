
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MessageHeaders](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [nvarchar](max) NULL,
	[ChatId] [nvarchar](max) NULL,
	[Type] [tinyint] NOT NULL DEFAULT ((0)),
	[Attribute] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.MessageHeaders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO




/****** Object:  Table [dbo].[Messages]    Script Date: 5/4/2017 11:42:19 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Messages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Text] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[MeassageHeaderId] [int] NULL,
	[SenderId] [int] NOT NULL DEFAULT ((0)),
	[ClubId] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_dbo.Messages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[Messages]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Messages_dbo.Clubs_ClubId] FOREIGN KEY([ClubId])
REFERENCES [dbo].[Clubs] ([Id])
GO

ALTER TABLE [dbo].[Messages] CHECK CONSTRAINT [FK_dbo.Messages_dbo.Clubs_ClubId]
GO

ALTER TABLE [dbo].[Messages]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Messages_dbo.JbUsers_SenderId] FOREIGN KEY([SenderId])
REFERENCES [dbo].[JbUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Messages] CHECK CONSTRAINT [FK_dbo.Messages_dbo.JbUsers_SenderId]
GO

ALTER TABLE [dbo].[Messages]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Messages_dbo.MessageHeaders_MeassageHeaderId] FOREIGN KEY([MeassageHeaderId])
REFERENCES [dbo].[MessageHeaders] ([Id])
GO

ALTER TABLE [dbo].[Messages] CHECK CONSTRAINT [FK_dbo.Messages_dbo.MessageHeaders_MeassageHeaderId]
GO

















/****** Object:  Table [dbo].[UserMessages]    Script Date: 5/4/2017 11:42:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UserMessages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MessageId] [int] NOT NULL,
	[ClubId] [int] NOT NULL,
	[ReceiverId] [int] NOT NULL DEFAULT ((0)),
	[IsRead] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_dbo.UserMessages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[UserMessages]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserMessages_dbo.Clubs_ClubId] FOREIGN KEY([ClubId])
REFERENCES [dbo].[Clubs] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[UserMessages] CHECK CONSTRAINT [FK_dbo.UserMessages_dbo.Clubs_ClubId]
GO

ALTER TABLE [dbo].[UserMessages]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserMessages_dbo.JbUsers_ReceiverId] FOREIGN KEY([ReceiverId])
REFERENCES [dbo].[JbUsers] ([Id])
GO

ALTER TABLE [dbo].[UserMessages] CHECK CONSTRAINT [FK_dbo.UserMessages_dbo.JbUsers_ReceiverId]
GO

ALTER TABLE [dbo].[UserMessages]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserMessages_dbo.Messages_MessageId] FOREIGN KEY([MessageId])
REFERENCES [dbo].[Messages] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[UserMessages] CHECK CONSTRAINT [FK_dbo.UserMessages_dbo.Messages_MessageId]
GO


