﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.UserService;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using Constants=Jumbula.Common.Constants.Constants;


namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class AdvanceSettingController : DashboardBaseController
    {


        public virtual ActionResult BackgroundCheck()
        {
            return View();
        }

        public virtual ActionResult LogoSetting()
        {
            return View();
        }


        [HttpGet]
        [JbAuthorize(JbAction.AdvanceSetting_BackgroundCheck_View)]
        public virtual JsonNetResult EditAdvanceSettingBackgroundCheck()
        {
            var model = new AdvanceBackgroundCheckViewModel();
            if (this.GetCurrentUserRole()==RoleCategory.Admin)
            {
                model.IsAdmin = true;
            }
         
            var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);

            if (club.Setting != null && club.Setting.InstructorBackgroundCheckCertificate != null)
            {

                if(club.Setting.InstructorBackgroundCheckCertificateDate == null || !club.Setting.InstructorBackgroundCheckCertificateDate.Any())
                {
                    club.Setting.InstructorBackgroundCheckCertificateDate = new List<string>();

                    foreach (var item in club.Setting.InstructorBackgroundCheckCertificate)
                    {
                        club.Setting.InstructorBackgroundCheckCertificateDate.Add("-");
                    }
                }


                for (int i = 0; i < club.Setting.InstructorBackgroundCheckCertificate.Count; i++)
                {
                    var certificate = club.Setting.InstructorBackgroundCheckCertificate[i];
                    var certificateDate = club.Setting.InstructorBackgroundCheckCertificateDate[i] != null ? club.Setting.InstructorBackgroundCheckCertificateDate[i] : "-";

                    model.BackgroundCertificatesNames.Add(certificate);
                    model.BackgroundCertificatesDates.Add(certificateDate);

                    model.BackgroundCertificatesURLs.Add(StorageManager.GetUriByPath(Constants.Path_ClubFilesContainer + "/" + ActiveClub.Domain + Constants.Path_InstructorBackgroundCheck + "/" + certificate).AbsoluteUri);
                }

            }

            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.AdvanceSetting_BackgroundCheck_Save)]
        public virtual ActionResult EditAdvanceSettingBackgroundCheck(AdvanceBackgroundCheckViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var res = new OperationStatus() { Status = true };
                    var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

                    if (model.BackgroundCertificatesNames != null && model.BackgroundCertificatesNames.Any())
                    {
                        if (club.Setting.InstructorBackgroundCheckCertificate == null)
                        {
                            club.Setting.InstructorBackgroundCheckCertificate = new List<string>();
                        }

                        if (club.Setting.InstructorBackgroundCheckCertificateDate == null)
                        {
                            club.Setting.InstructorBackgroundCheckCertificateDate = new List<string>();
                        }

                        club.Setting.InstructorBackgroundCheckCertificate = model.BackgroundCertificatesNames;
                        club.Setting.InstructorBackgroundCheckCertificateDate = model.BackgroundCertificatesDates;
                        club.IsSettingChanged = true;

                        res = Ioc.ClubBusiness.Update(club);
                        if (res.Status)
                        {
                           EmailService.Get(this.ControllerContext).SendBackGroundCheckEmailToEM(model, club);
                        }
                    }
                    else
                    {
                        if (club.Setting.InstructorBackgroundCheckCertificate != null)
                        {
                            club.Setting.InstructorBackgroundCheckCertificate.Clear();
                            club.Setting.InstructorBackgroundCheckCertificate = null;
                            club.IsSettingChanged = true;

                            res = Ioc.ClubBusiness.Update(club);
                            if (res.Status)
                            {
                               EmailService.Get(this.ControllerContext).SendBackGroundCheckEmailToEM(model, club);
                            }
                        }
                    }

                    return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });

                }

                return base.JsonFormResponse();
            }
            catch (Exception ex)
            {
                return Json(new JResult { Status = false, Message = ex.Message, RecordsAffected = 0 });
            }
        }


        [HttpGet] 
        public virtual JsonNetResult GetSchoolLogoSetting()
        {
            var model = new LogoSettingViewModel();
           
            var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);

            if (club.Setting != null )
            {
                model.ShowSchoolLogo = club.Setting.ShowSchoolLogo;
            }
            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult SaveSchoolLogoSetting(LogoSettingViewModel model)
        {
            var res = new OperationStatus() { Status = true };
  
            var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);

            if (club.Setting != null )
            {
                club.Setting.ShowSchoolLogo = model.ShowSchoolLogo;
            }

            club.IsSettingChanged = true;
            res = Ioc.ClubBusiness.Update(club);

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }
    }
}