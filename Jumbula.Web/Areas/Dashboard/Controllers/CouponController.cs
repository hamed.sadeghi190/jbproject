﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.WebConfig;
using Kendo.Mvc.Extensions;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class CouponController : DashboardBaseController
    {


        [HttpPost]
        public virtual JsonNetResult GetJsonData(string seasonDomain, int skip, int take, int page, int pageSize, List<GridSort> sort = null)
        {
            var club = base.ActiveClub;

            var termValue = Request.Params["term"] ?? string.Empty;

            var seasonId = Ioc.SeasonBusiness.Get(seasonDomain, club.Id).Id;
            int total = 0;
            var coupons = Ioc.CouponBusiness.
                GetList(seasonId).ToList().Select(c => c.ToViewModel<CouponItemViewModel>()).ToList();

            if (sort != null)
            {
                foreach (var s in sort)
                {
                    switch (s.field)
                    {
                        case "Name":
                            {
                                coupons = (s.dir == "asc") ? coupons.OrderBy(o => o.Name).ToList() : coupons.OrderByDescending(o => o.Name).ToList();
                                break;
                            }
                        case "Status":
                            {
                                coupons = (s.dir == "asc") ? coupons.OrderBy(o => o.Status).ToList() : coupons.OrderByDescending(o => o.Status).ToList();
                                break;
                            }
                        default:
                            break;
                    }
                }
            }

            List<CouponItemViewModel> filteredData = null;

            if (string.IsNullOrEmpty(termValue))
            {
                filteredData = coupons.Skip(skip).Take(take).ToList();
                total = coupons.Count();
            }
            else
            {
                var filters = coupons.Where(r => r.Name.ToLower().Contains(termValue.ToLower())).OrderBy(o => o.Id);
                total = filters.Count();
                filteredData = filters.Skip(skip).Take(take).ToList();
            }

            var dataSource = filteredData;

            return JsonNet(new { DataSource = dataSource, TotalCount = total });
        }

        public virtual JsonNetResult GetBaseData()
        {
            dynamic model = new ExpandoObject();
            var eventStatusCategories = Enum.GetValues(typeof(EventStatusCategories))
                .Cast<EventStatusCategories>()
                .Select(v => new JbTitleValue<string>() { Title = v.ToString(), Value = ((int)v).ToString() })
                .ToList();
            model.EventStatusCategories = eventStatusCategories;
            model.Seasons = Ioc.SeasonBusiness.GetList().Select(s => new JbTitleValue<long>() { Title = s.Title, Value = s.Id }).ToList();
            return JsonNet(model);
        }

        public virtual JsonNetResult CreateEditJson(int id = 0)
        {
            var coupon = new CouponItemViewModel();
            if (id > 0)
            {
                var coupondb = Ioc.CouponBusiness.Get(id);
                CheckResourceForAccess(coupondb);

                coupon = coupondb.ToViewModel<CouponItemViewModel>();
                coupon.SelectedEvents = coupondb.Programs.Select(e => e.Id).ToList();
                //coupon.SelectedUsers = coupondb.Users.ToList().Select(e => new JbTitleValue(e.UserName, e.Id));
            }
            return JsonNet(coupon);
        }

        public virtual JsonNetResult UploadCouponsJson(int id = 0)
        {
            var coupon = new CouponUploadItemViewModel();
            if (id > 0)
            {
                var coupondb = Ioc.CouponBusiness.Get(id);
                CheckResourceForAccess(coupondb);

                coupon = coupondb.ToViewModel<CouponUploadItemViewModel>();
                coupon.SelectedEvents = coupondb.Programs.Select(e => e.Id).ToList();

            }
            return JsonNet(coupon);
        }

        public virtual ActionResult Coupons()
        {
            return View();
        }

        public virtual ActionResult CouponManage()
        {
            return View();
        }
        public virtual ActionResult CouponsUpload()
        {
            return View();
        }

        [HttpPost]
        public virtual ActionResult CreateEdit(CouponItemViewModel model)
        {
            var coupon = model;
            Coupon couponDb;
            OperationStatus res;

            if (ModelState.IsValid)
            {
                var club = base.ActiveClub;

                coupon.SeasonId = Ioc.SeasonBusiness.Get(coupon.SeasonDomain, club.Id).Id;

                if (coupon.Id > 0)
                {
                    couponDb = coupon.ToModel<Coupon>(Ioc.CouponBusiness.Get(coupon.Id));
                }
                else
                    couponDb = coupon.ToModel<Coupon>();

                if (!coupon.IsAllProgram && coupon.SelectedEvents != null && coupon.SelectedEvents.Any())
                {
                    couponDb.Programs.Clear();
                    couponDb.Programs.AddRange(Ioc.ProgramBusiness.GetList(coupon.SelectedEvents).ToList());
                    couponDb.IsAllProgram = false;
                }
                else
                {
                    couponDb.Programs.Clear();
                    couponDb.IsAllProgram = true;
                }

                if (coupon.AmounType == ChargeDiscountType.Percent && coupon.Amount > 100)
                {
                    ModelState.AddModelError("Amount", "Amount value invalid,please input number less than 100.");
                    return JsonFormResponse();
                }
                if (coupon.Id == 0)
                {
                    couponDb.StartDate = couponDb.StartDate.ToUniversalTime();
                    couponDb.EndDate = couponDb.EndDate.ToUniversalTime();

                    couponDb.MetaData.DateCreated = DateTime.UtcNow;
                    couponDb.MetaData.DateUpdated = DateTime.UtcNow;
                    couponDb.AddType = CouponAddType.Manually;
                    if (Ioc.CouponBusiness.IsExistCouponCode(coupon.SeasonId.Value, coupon.Code))
                    {
                        ModelState.AddModelError("Code", "Coupon code already exists,please input another one.");
                        return JsonFormResponse();
                    }
                    res = Ioc.CouponBusiness.Create(couponDb);
                }
                else
                {
                    if (Ioc.CouponBusiness.IsExistCouponCode(coupon.SeasonId.Value, coupon.Code, coupon.Id))
                    {
                        ModelState.AddModelError("Code", "Coupon code already exists,please input another one");
                        return JsonFormResponse();
                    }
                    couponDb.MetaData.DateUpdated = DateTime.UtcNow;
                    res = Ioc.CouponBusiness.Update(couponDb);
                }

                return Json(new JResult { Status = res.Status ? true : res.RecordsAffected == 0 && res.Message == null ? true : false, Message = res.Message + res.ExceptionInnerMessage, RecordsAffected = res.RecordsAffected });
            }

            return JsonFormResponse();
        }



        [HttpPost]
        public virtual JsonResult Delete(int couponId)
        {
            var res = Ioc.CouponBusiness.Delete(couponId);
            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }



        [HttpPost]
        public virtual JsonNetResult UploadedList(HttpPostedFileBase uploadedList)
        {
            var csvList = Jumbula.Common.Helper.FileHelper.ConveretCsvToList(uploadedList.InputStream);

            var couponCodes = csvList.Select(o => o.First()).ToList();

            return JsonNet(new { Data = "Add", uploadedList = couponCodes, Status = true });

        }

        public virtual JsonNetResult DeleteUploadedList(string[] fileNames)
        {
            var clubDomain = ActiveClub.Domain;
            var fileName = clubDomain + "UploadedList.csv";
            var sourcePath = StorageManager.PathCombine(clubDomain, fileName);
            var storageManager = new StorageManager(WebConfigHelper.AzureStorageConnection,
                Constants.Path_ClubFilesContainer);
            var targetPath = StorageManager.PathCombine(clubDomain, fileName);
            if (
                StorageManager.IsExist(
                    StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain,
                        fileName).AbsoluteUri))
            {
                storageManager.ReadFile(sourcePath, targetPath);
                storageManager.DeleteBlob(clubDomain, fileName);
            }

            //var file = StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain, fileName).AbsoluteUri;

            return JsonNet(new JResult { Data = "Remove", Status = true });
        }

        public virtual FileResult DownloadSampleCsv()
        {
            var file = Server.MapPath("~/Content/sampleCoupon.csv");
            if (System.IO.File.Exists(file))
            {
                return File(file, "application/csv", "Sample Csv For Upload.csv");
            }
            else
            {
                throw new HttpException(404, "File not found");
            }
        }

        [HttpPost]
        public virtual ActionResult SaveUploadedList(CouponUploadItemViewModel model)
        {
            Coupon couponDb;
            var club = ActiveClub;
            var coupons = model;
            if (model.CouponsList.Count <= 0)
            {
                ModelState.AddModelError("model.CouponsLists", "Please select the coupon codes import file.");
            }
            if (ModelState.IsValid)
            {
                couponDb = coupons.ToModel<Coupon>();

                coupons.SeasonId = Ioc.SeasonBusiness.Get(coupons.SeasonDomain, club.Id).Id;

                couponDb.StartDate = couponDb.StartDate.ToUniversalTime();
                couponDb.EndDate = couponDb.EndDate.ToUniversalTime();

                couponDb.MetaData.DateCreated = DateTime.UtcNow;
                couponDb.MetaData.DateUpdated = DateTime.UtcNow;


                if (!coupons.IsAllProgram && coupons.SelectedEvents != null && coupons.SelectedEvents.Any())
                {
                    couponDb.Programs.Clear();
                    couponDb.Programs.AddRange(Ioc.ProgramBusiness.GetList(coupons.SelectedEvents).ToList());
                    couponDb.IsAllProgram = false;
                }
                else
                {
                    couponDb.Programs.Clear();
                    couponDb.IsAllProgram = true;
                }
                if (coupons.AmounType == ChargeDiscountType.Percent && coupons.Amount > 100)
                {
                    ModelState.AddModelError("Amount", "Amount value invalid,please input number less than 100.");
                    return JsonFormResponse();
                }

                var result = new OperationStatus();
                if (model.IsSkipFirstRow)
                {
                    string firstRow = model.CouponsList[0];
                    model.CouponsList.Remove(firstRow);
                }
                var listCoupons = new List<Coupon>();

                List<dynamic> listCodeResult = new List<dynamic>();

                foreach (var code in model.CouponsList)
                {
                    if (listCoupons.Select(c => c.Code).Contains(code.Trim()))
                    {
                        dynamic rows = new ExpandoObject();
                        rows.CouponCode = code;
                        rows.Status = "Coupon code already exists in file";
                        listCodeResult.Add(rows);
                    }
                    else
                    {
                        if (Ioc.CouponBusiness.IsExistCouponCode(coupons.SeasonId, code.Trim()))
                        {
                            dynamic rows = new ExpandoObject();
                            rows.CouponCode = code;
                            rows.Status = "Coupon code already exists in db";
                            listCodeResult.Add(rows);
                        }
                        else
                        {

                            listCoupons.Add(new Coupon()
                            {
                                Name = coupons.Name,
                                Code = code.Trim(),
                                StartDate = couponDb.StartDate,
                                EndDate = couponDb.EndDate,
                                AmounType = coupons.AmounType,
                                Amount = model.Amount,
                                IsAllProgram = couponDb.IsAllProgram,
                                IsOneTimeUse = coupons.IsOneTimeUse,
                                CouponType = coupons.CouponType,
                                Programs = couponDb.Programs,
                                SeasonId = coupons.SeasonId,
                                MetaData = couponDb.MetaData,
                                Season = couponDb.Season,
                                AddType = CouponAddType.Upload,
                            });

                            dynamic rows = new ExpandoObject();
                            rows.CouponCode = code;
                            rows.Status = "Imported";
                            listCodeResult.Add(rows);

                        }
                    }
                }

                var fileCsv = ConvertResultToCsv(listCodeResult.ToList());
                if (listCoupons.Any())
                {
                    result = Ioc.CouponBusiness.CreateList(listCoupons);
                }
                else
                {
                    result.Status = true;
                }
                var listFile = new JbFile();
                listFile.Name = coupons.Name;
                listFile.Content = fileCsv;

                //couponDb.Season.Files.Add(new SeasonFile { File = listFile,SeasonId= coupons.SeasonId });
                //couponDb.Season.

                var res = Ioc.JbFileBusiness.Create(listFile);
                var resultseason = Ioc.SeasonFileBusiness.Create(coupons.SeasonId, listFile.Id);

                return JsonNet(new { Status = result.Status, fileId = listFile.Id });
            }
            return JsonFormResponse();
        }

        public byte[] ConvertResultToCsv(List<dynamic> rows)
        {
            StringBuilder csv = new StringBuilder();

            foreach (var row in rows)
            {
                var newLine = string.Concat(row.CouponCode, ", ", row.Status, ", ");

                csv.AppendLine(newLine);
            }

            byte[] result = Encoding.ASCII.GetBytes(csv.ToString());
            return result;
        }


        [HttpGet]
        public virtual ActionResult DownloadResult(int id)
        {
            var dataFileCoupon = Ioc.JbFileBusiness.Get(id);
            return File(dataFileCoupon.Content, "application/ms-excel", string.Format("{0}_Result.csv", dataFileCoupon.Name));
        }

        [HttpGet]
        public virtual ActionResult Save(string contentType, int fileId, string fileName)
        {
            var dataFileCouponbyte = Ioc.JbFileBusiness.Get(fileId).Content;
            //string base64String = Convert.ToBase64String(dataFileCouponbyte, 0, dataFileCouponbyte.Length);
            //var fileContents = Convert.FromBase64String(base64String);
            return File(dataFileCouponbyte, contentType, fileName);
        }

    }
}
