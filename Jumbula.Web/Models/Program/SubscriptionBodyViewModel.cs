﻿using Jumbula.Common.Enums;
using System;
using System.Collections.Generic;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class SubscriptionBodyViewModel : BaseViewModel<Program, long>, IProgramBodyViewModel
    {
        public PostalAddress Address { get; set; }
     
        public string ClubLogo { get; set; }
       
        public Jumbula.Common.Enums.TimeZone  ClubTimeZone { get; set; }

        public string Description { get; set; }
        
        public DateTime? EndDate { get; set; }

        public bool EnablePriceHidden { get; set; }
        public string LocationName { get; set; }
      
        public DateTime? StartDate { get; set; }
        public bool DisableCapacityRestriction { get; set; }
        public string ClubDomain { get; set; }

        public string SeasonDomain { get; set; }

        public bool HideGoogleMapInClassPage { get; set; }
        public string Domain { get; set; }
        public string WeeklyEmail { get; set; }
        public string MaterialsNeeded { get; set; }
        public string Name { get; set; }
        public string RoomAssignment { get; set; }

        public ProgramBodySchedule Schedule { get; set; }

        public string Price { get; set; }

        public string PriceLabel { get; set; }

        public CurrencyCodes Currency { get; set; }

        public List<SubscriptionItemViewModel> SubscriptionItems { get; set; }

        public List<ProgramSchedulePartViewModel> SubscriptionParts { get; set; }

        public bool HasApplicationFee
        {
            get
            {
                return ApplicationFeeAmount != 0;
            }
        }

        public decimal ApplicationFeeAmount { get; set; }
    }

    public class SubscriptionItemViewModel
    {
        public decimal Amount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime DueDate { get; set; }
    }
}