﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('seasonSetupController',
        [
            '$scope', '$http', '$stateParams', '$state', 'jbConfirmation', '$sce',
            function($scope, $http, $stateParams, $state, jbConfirmation, $sce) {
                window.app.globalObjects.pageClass = 'season-setup';
                window.app.globalObjects.initializeView();
                $scope.baseUrl = window.location.origin + "/Dashboard/Season";
                $scope.MC.openBlade({ url: "Dashboard/Blade/SeasonSetup", order: 2, menu: "SEASONS" });
                $scope.MC.stateParams = $stateParams;

                $scope.deleteSeason = function() {

                    jbConfirmation
                        .yes(function() {
                            $http.post($scope.baseUrl + '/Delete', { seasonDomain: $scope.MC.stateParams.seasonDomain })
                                .success(function(data, status, headers, config) {
                                    if (data.Status === true) {
                                        $scope.MC.closeAllBlades();
                                        $scope.MC.getSeasons();
                                        $state.go('Seasons');
                                    } else {
                                        $scope.MC.handleErrors($scope, data);
                                    }
                                });
                        })
                        .confirm();
                };

                $scope.archiveSeason = function() {

                    jbConfirmation
                        .yes(function() {
                            $http.post($scope.baseUrl + '/Archive',
                                    { seasonDomain: $scope.MC.stateParams.seasonDomain })
                                .success(function(data, status, headers, config) {
                                    if (data.Status === true) {
                                        $scope.MC.closeAllBlades();
                                        $scope.MC.getSeasons();
                                        $state.go('Seasons');
                                    } else {
                                        $scope.MC.handleErrors($scope, data);
                                    }
                                });
                        })
                        .confirm();
                };


               
            }
        ]);
})();