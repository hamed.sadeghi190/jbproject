﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;

namespace SportsClub.Models.Sistem
{
    public class LogViewModel
    {
        [Display(Name="Log Category")]
        public LogCategory? LogCategory { get; set; }
    }
}