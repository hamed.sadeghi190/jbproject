﻿using System.Collections.Generic;
using Jumbula.Common.Utilities;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Report;

namespace Jumbula.Core.Business
{
    public interface IReportBuilderBusiness
    {
        ReportModel GetSchema(Dictionary<string, object> parameters, ClubBaseInfoModel clubBaseInfo);
        ReportDataModel BindData(Dictionary<string, object> parameters, ClubBaseInfoModel clubBaseInfo,
            PaginationModel paginationModel, List<ReportGridSort> sort = null);
    }
}
