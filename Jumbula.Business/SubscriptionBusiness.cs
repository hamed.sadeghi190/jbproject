﻿
using Jb.Framework.Common;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Web;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class SubscriptionBusiness : ISubscriptionBusiness
    {
        private readonly DateTime _nowDateTime;
        private readonly IClubBusiness _clubBusiness;
        private IOrderItemBusiness _orderItemBusiness;
        private IProgramSessionBusiness _programSessionBusiness;
        private ISeasonBusiness _seasonBusiness;
        private readonly IUserProfileBusiness _userProfileBusiness;
        private IProgramBusiness _programBusiness;
        private ICartBusiness _cartBusiness;
        private readonly IRepository<Program, long> _subscriptionRepository;
        private readonly IEntitiesContext _dataContext;
        private readonly IChargeBusiness _chargeBusiness;

        public SubscriptionBusiness(IClubBusiness clubBusiness,
            IUserProfileBusiness userProfileBusiness,
            IRepository<Program, long> subscriptionRepository,
            IEntitiesContext dataContext, IChargeBusiness chargeBusiness)
        {
            _clubBusiness = clubBusiness;
            _userProfileBusiness = userProfileBusiness;
            _subscriptionRepository = subscriptionRepository;
            _dataContext = dataContext;

            _nowDateTime = DateTime.UtcNow;

            _chargeBusiness = chargeBusiness;
        }

        public IOrderItemBusiness OrderItemBusiness
        {
            get { return _orderItemBusiness; }
            set { _orderItemBusiness = value; }
        }

        public IProgramBusiness ProgramBusiness
        {
            get { return _programBusiness; }
            set { _programBusiness = value; }
        }

        public IProgramSessionBusiness ProgramSessionBusiness
        {
            get { return _programSessionBusiness; }
            set { _programSessionBusiness = value; }
        }

        public ISeasonBusiness SeasonBusiness
        {
            get { return _seasonBusiness; }
            set { _seasonBusiness = value; }
        }

        public ICartBusiness CartBusiness
        {
            get { return _cartBusiness; }
            set { _cartBusiness = value; }
        }

        public List<ProgramSubscriptionItem> GenerateSubscriptionSchedules(DateTime startDate, DateTime endDate, decimal amount, int dueDate)
        {
            var result = new List<ProgramSubscriptionItem>();

            var date = startDate.Date;



            while (date < endDate)
            {
                var item = new ProgramSubscriptionItem();

                item.Amount = amount;
                item.StartDate = date;

                var candidateEndDate = date.AddDays(29);

                if (candidateEndDate > endDate)
                {
                    item.EndDate = endDate;
                }
                else
                {
                    item.EndDate = candidateEndDate;
                }

                #region DueDate
                int? dueDateMonth = null;
                int? dueDateYear = null;

                var day = date.Day;
                if (day < dueDate)
                {
                    dueDateMonth = date.AddMonths(-1).Month;
                    dueDateYear = date.AddMonths(-1).Year;
                }
                else
                {
                    dueDateMonth = date.Month;
                    dueDateYear = date.Year;
                }

                var dueDateDay = dueDate;

                DateTime paymentDueDate = new DateTime(dueDateYear.Value, dueDateMonth.Value, dueDateDay);
                #endregion

                item.DueDate = paymentDueDate;
                item.Id = Guid.NewGuid().ToString();

                result.Add(item);

                date = item.EndDate.AddDays(1);
            }

            return result;
        }

        public bool IsSubscriptionSchedulesPreviouslyEdited(long programId)
        {
            var program = _programBusiness.GetList().Single(p => p.Id == programId);

            var currentSubscriptionItems = new List<ProgramSubscriptionItem>();
            var attributes = new ScheduleSubscriptionAttribute();
            var programSchedule = new ProgramSchedule();

            if (program.ProgramSchedules.Any() && program.ProgramSchedules.Last(p => !p.IsDeleted).Attributes != null)
            {
                programSchedule = program.ProgramSchedules.Last(p => !p.IsDeleted);
                attributes = program.ProgramSchedules.Last(p => !p.IsDeleted).Attributes as ScheduleSubscriptionAttribute;
                currentSubscriptionItems = ((ScheduleSubscriptionAttribute)(program.ProgramSchedules.Last(p => !p.IsDeleted).Attributes)).SubscriptionItems;
            }

            var generatedItems = new List<ProgramSubscriptionItem>();//  GenerateSubscriptionSchedules(programSchedule.StartDate, programSchedule.EndDate, attributes.Subscription.Price, attributes.Subscription.Occurances, attributes.Subscription.DaysBeforeBilling, true, false, currentSubscriptionItems, true);

            if (currentSubscriptionItems.Count() != generatedItems.Count())
            {
                return true;
            }

            for (int i = 0; i < currentSubscriptionItems.Count(); i++)
            {
                if (currentSubscriptionItems[i].Amount != generatedItems[i].Amount)
                {
                    return true;
                }

                if (currentSubscriptionItems[i].DueDate != generatedItems[i].DueDate)
                {
                    return true;
                }

                if (currentSubscriptionItems[i].EndDate != generatedItems[i].EndDate)
                {
                    return true;
                }

                if (currentSubscriptionItems[i].StartDate != generatedItems[i].StartDate)
                {
                    return true;
                }
            }

            return false;
        }

        public List<ProgramSubscriptionItem> GetAppliedSubscriptionItems(int clubId, List<ProgramSubscriptionItem> subscriptionItems, DateTime? desiredStartDate)
        {
            var result = new List<ProgramSubscriptionItem>();

            if (!desiredStartDate.HasValue)
            {
                return result;
            }

            var desiredDate = desiredStartDate.Value;

            var acceptableItems = new List<ProgramSubscriptionItem>();

            var appliedSubscriptionItem = GetAppliedSubscriptionItem(clubId, subscriptionItems, desiredStartDate);

            if (appliedSubscriptionItem != null)
            {
                acceptableItems = subscriptionItems.Where(a => a.DueDate >= appliedSubscriptionItem.DueDate).ToList();
            }

            result = acceptableItems.OrderBy(o => o.DueDate).ToList();

            return result;
        }
        public List<ProgramSchedulePart> GetAppliedProgramParts(List<ProgramSchedulePart> programScheduleParts, ProgramSchedule schedule, DateTime? desiredStartDate)
        {
            var result = new List<ProgramSchedulePart>();

            if (!desiredStartDate.HasValue)
            {
                return result;
            }

            var desiredDate = desiredStartDate.Value;

            var acceptableItems = new List<ProgramSchedulePart>();

            var appliedProgramPart = GetAppliedProgramPart(schedule, desiredStartDate);

            if (appliedProgramPart != null)
            {
                acceptableItems = programScheduleParts.Where(a => a.DueDate >= appliedProgramPart.DueDate).ToList();
            }

            result = acceptableItems.OrderBy(o => o.DueDate).ToList();

            return result;
        }
        public decimal CalculateSubscriptionCoupon(OrderItem orderItem, Coupon coupon)
        {
            decimal result = 0;

            var charge = new SubscriptionChargeDiscount(coupon);

            var subscriptionItems = GetProgramSubscriptionItems(orderItem);

            var programParts = _programBusiness.GetScheduleParts(orderItem.ProgramSchedule.ProgramId);
            var appliedSubscriptionItems = GetAppliedProgramParts(programParts, orderItem.ProgramSchedule, orderItem.DesiredStartDate);

            //calculate program amount for when have percent coupon
            var orderCharge = orderItem.OrderChargeDiscounts.Single(o => !o.IsDeleted && o.Category == ChargeDiscountCategory.EntryFee);

            var selectedCharge = orderCharge.Charge;

            var itemAmount = selectedCharge.Amount;

            var programPartCharges = _programBusiness.GetSchedulePartCharges(selectedCharge);
            var appliedProgramPart = GetAppliedProgramPart(orderItem.ProgramSchedule, orderItem.DesiredStartDate);

            if (orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                foreach (var item in appliedSubscriptionItems)
                {
                    decimal amount = 0;
                    var isItemCurrent = false;

                    if (appliedProgramPart != null && appliedProgramPart.DueDate == item.DueDate)
                    {
                        isItemCurrent = true;
                    }
                    if (isItemCurrent)
                    {
                        if (IsProrateProgramPart(item, orderItem.ProgramSchedule, orderItem.DesiredStartDate))
                        {
                            var selectedDays = orderItem.Attributes.WeekDays;
                            amount = CalculateBeforeAfterCareProrateAmount(item, orderItem.DesiredStartDate.Value, selectedCharge, selectedDays);
                        }
                        else
                        {
                            amount = programPartCharges.Where(c => c.SchedulePartId == item.Id).FirstOrDefault().ChargeAmount;
                        }
                    }
                    else
                    {
                        amount = programPartCharges.Where(c => c.SchedulePartId == item.Id).FirstOrDefault().ChargeAmount;
                    }

                    result += charge.CalculateItemWithCoupon(amount);
                }
            }
            else
            {
                foreach (var item in appliedSubscriptionItems)
                {
                    result += charge.CalculateItemWithCoupon(itemAmount);
                }
            }


            var applicationFee = GetApplicationFee(orderItem);

            if (applicationFee != null && orderItem.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.PartnerApplicationFee))
            {
                //result += charge.Calculate(applicationFee.Amount, true);

                result += charge.CalculateItemWithCoupon(applicationFee.Amount, true);
            }

            return result;
        }

        public decimal GetFirstSubscriptionAmount(List<ProgramSubscriptionItem> subscriptionItems, OrderItem orderItem)
        {
            decimal result = 0;

            var charge = GetSelectedCharge(orderItem);

            subscriptionItems.ForEach(s => s.Amount = charge.Amount);

            foreach (var item in subscriptionItems)
            {
                if (IsItemCurrent(orderItem.ProgramSchedule, item, orderItem.DesiredStartDate))
                {
                    result += CalculateItemAmount(item, orderItem);
                }
            }

            return result;
        }

        public decimal GetFirstItemAmount(List<ProgramSchedulePart> programParts, OrderItem orderItem)
        {
            decimal result = 0;


            var charge = GetSelectedCharge(orderItem);
            var programPartCharges = _programBusiness.GetSchedulePartCharges(charge);
            var appliedProgramPart = GetAppliedProgramPart(orderItem.ProgramSchedule, orderItem.DesiredStartDate);

            foreach (var programPart in programParts)
            {
                var isItemCurrent = false;

                if (appliedProgramPart != null && appliedProgramPart.DueDate == programPart.DueDate)
                {
                    isItemCurrent = true;
                }
                if (isItemCurrent)
                {
                    decimal partAmount = 0;
                    if (orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                    {
                        partAmount = programPartCharges.Where(c => c.SchedulePartId == programPart.Id).FirstOrDefault().ChargeAmount;
                    }
                    else
                    {
                        partAmount = charge.Amount;
                    }

                    result += CalculateItemAmount(orderItem, partAmount, appliedProgramPart);
                }
            }

            return Math.Floor(result * 100) / 100;

        }

        public decimal CalculateCurrentScheduleAmount(Program program, DateTime desiredStartDate, int selectedChargeId, List<DayOfWeek> days)
        {
            decimal result = 0;
            var programParts = _programBusiness.GetScheduleParts(program.Id);
            var charge = program.ProgramSchedules.SelectMany(s => s.Charges).FirstOrDefault(c => c.Id == selectedChargeId);
            var programPartCharges = _programBusiness.GetSchedulePartCharges(charge);
            var appliedProgramPart = GetAppliedProgramPart(charge.ProgramSchedule, desiredStartDate);

            foreach (var programPart in programParts)
            {
                var isItemCurrent = false;

                if (appliedProgramPart != null && appliedProgramPart.DueDate == programPart.DueDate)
                {
                    isItemCurrent = true;
                }
                if (isItemCurrent)
                {
                    decimal partAmount = 0;

                    partAmount = programPartCharges.Where(c => c.SchedulePartId == programPart.Id).FirstOrDefault().ChargeAmount;

                    result += CalculateScheduleAmount(charge.ProgramSchedule, charge, partAmount, desiredStartDate, days, appliedProgramPart);
                }
            }

            return Math.Floor(result * 100) / 100;
        }
        public decimal GetTotalSubscriptionAmount(List<ProgramSubscriptionItem> subscriptionItems, OrderItem orderItem)
        {
            decimal result = 0;

            var charge = GetSelectedCharge(orderItem);
            var clubId = orderItem.Order.ClubId;

            subscriptionItems.ForEach(s => s.Amount = charge.Amount);

            var appliedSubscriptionItems = GetAppliedSubscriptionItems(clubId, subscriptionItems, orderItem.DesiredStartDate);

            foreach (var item in appliedSubscriptionItems)
            {
                result += CalculateItemAmount(item, orderItem);
            }

            if (orderItem.PaymentPlanType == PaymentPlanType.FullPaid)
            {
                var fullPayDiscount = GetFullPayDiscount(orderItem.ProgramSchedule, result);

                result -= fullPayDiscount;
            }

            return result;
        }

        public decimal GetTotalAmount(List<ProgramSchedulePart> programScheduleParts, OrderItem orderItem)
        {
            decimal result = 0;

            var charge = GetSelectedCharge(orderItem);

            var appliedProgramParts = GetAppliedProgramParts(programScheduleParts, orderItem.ProgramSchedule, orderItem.DesiredStartDate);
            var programPartCharges = _programBusiness.GetSchedulePartCharges(charge);

            foreach (var programPart in appliedProgramParts)
            {
                decimal partAmount = 0;
                if (orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                {
                    partAmount = programPartCharges.Where(c => c.SchedulePartId == programPart.Id).FirstOrDefault().ChargeAmount;
                }
                else
                {
                    partAmount = charge.Amount;
                }

                result += CalculateItemAmount(orderItem, partAmount, programPart);
            }

            if (orderItem.PaymentPlanType == PaymentPlanType.FullPaid)
            {
                var fullPayDiscount = GetFullPayDiscount(orderItem.ProgramSchedule, result);

                result -= fullPayDiscount;
            }

            return result;

        }

        public ProgramSubscriptionItem GetAppliedSubscriptionItem(int clubId, List<ProgramSubscriptionItem> subscriptionItems, DateTime? desiredStartDate)
        {
            ProgramSubscriptionItem result = null;
            var club = _clubBusiness.Get(clubId);
            DateTime nowClubDateTime = DateTimeHelper.GetCurrentLocalDateTime(club.TimeZone);
            var dateTime = desiredStartDate.HasValue ? desiredStartDate : nowClubDateTime;

            var minDueDate = subscriptionItems.Min(s => s.DueDate);
            var maxDueDate = subscriptionItems.Max(s => s.DueDate);
            var maxEndDate = subscriptionItems.Max(s => s.EndDate);

            var orderedSubscriptionItems = subscriptionItems.OrderBy(s => s.DueDate);

            if (!desiredStartDate.HasValue)
            {
                return subscriptionItems.First();
            }

            if (dateTime.Value.Date <= minDueDate.Date)
            {
                result = orderedSubscriptionItems.FirstOrDefault();
            }
            else if (dateTime.Value.Date > maxEndDate)
            {
                result = null;
            }
            else
            {
                result = orderedSubscriptionItems.SingleOrDefault(o => o.StartDate.Date <= dateTime.Value.Date && dateTime.Value.Date <= o.EndDate);
            }

            return result;
        }

        public ProgramSubscriptionItem GetAppliedSubscriptionItem(ProgramSchedule programSchedule, DateTime? desiredStartDate)
        {
            var clubId = programSchedule.Program.ClubId;
            var subscriptionAttribute = GetSubscriptionAtribute(programSchedule);
            var subscriptionItems = subscriptionAttribute.SubscriptionItems;

            return GetAppliedSubscriptionItem(clubId, subscriptionItems, desiredStartDate);
        }

        public ProgramSchedulePart GetAppliedProgramPart(ProgramSchedule programSchedule, DateTime? desiredStartDate)
        {
            ProgramSchedulePart result = null;
            var club = programSchedule.Program.Club;
            var programParts = _programBusiness.GetScheduleParts(programSchedule.ProgramId);

            DateTime nowClubDateTime = DateTimeHelper.GetCurrentLocalDateTime(club.TimeZone);
            var dateTime = desiredStartDate.HasValue ? desiredStartDate : nowClubDateTime;

            var minDueDate = programParts.Min(s => s.DueDate);
            var maxDueDate = programParts.Max(s => s.DueDate);
            var maxEndDate = programParts.Max(s => s.EndDate);

            var orderedProgramPartItems = programParts.OrderBy(s => s.DueDate);

            if (!desiredStartDate.HasValue)
            {
                return programParts.First();
            }

            if (dateTime.Value.Date <= minDueDate.Value.Date)
            {
                result = orderedProgramPartItems.FirstOrDefault();
            }
            else if (dateTime.Value.Date > maxEndDate)
            {
                result = null;
            }
            else
            {
                result = orderedProgramPartItems.SingleOrDefault(o => o.StartDate.Date <= dateTime.Value.Date && dateTime.Value.Date <= o.EndDate);
            }

            return result;
        }
        public Charge GetSelectedCharge(ProgramSchedule programSchedule, long chargeId)
        {
            return programSchedule.Charges.SingleOrDefault(c => c.Id == chargeId);
        }

        public Charge GetSelectedCharge(OrderItem orderItem)
        {
            return GetSelectedCharge(orderItem.ProgramSchedule, orderItem.OrderChargeDiscounts.Single(o => !o.IsDeleted && o.Category == ChargeDiscountCategory.EntryFee).ChargeId.Value);
        }

        public decimal GetFirstSubscriptionAmount(ProgramSchedule programSchedule, DateTime? desiredStartDate, long selectedChargeId = 0)
        {
            decimal result = 0;
            var club = programSchedule.Program.Club;
            DateTime nowClubDateTime = DateTimeHelper.GetCurrentLocalDateTime(club.TimeZone);

            var subscriptionAttribute = GetSubscriptionAtribute(programSchedule);
            var subscriptionItems = subscriptionAttribute.SubscriptionItems;
            var selectedSubscriptionSchedule = GetSelectedCharge(programSchedule, selectedChargeId);

            var isProrate = subscriptionAttribute.IsProrate;
            var prorateTime = subscriptionAttribute.ProrateTime;

            subscriptionItems.ForEach(s => s.Amount = selectedSubscriptionSchedule.Amount);

            foreach (var item in subscriptionItems)
            {
                if (IsItemCurrent(programSchedule, item, desiredStartDate))
                {
                    result += CalculateItemAmount(item, isProrate, prorateTime, nowClubDateTime);
                }
            }

            return result;
        }

        public decimal GetFullPayDiscount(ProgramSchedule programSchedule, decimal totalAmount)
        {
            decimal fullPaidDiscount = 0;

            if (programSchedule.Program.TypeCategory == ProgramTypeCategory.Subscription)
            {
                var subscriptionAttribute = GetSubscriptionAtribute(programSchedule);
                fullPaidDiscount = subscriptionAttribute.FullPayDiscount;
            }
            else
            {
                var beforeAfterCareAttribute = GetBeforAfterCareAtribute(programSchedule);
                fullPaidDiscount = beforeAfterCareAttribute.FullPayDiscount;
            }


            var subscrptionDiscount = new SubscriptionChargeDiscount()
            {
                Amount = fullPaidDiscount,
                AmountType = ChargeDiscountType.Percent,
                CalculationType = CalculationType.OnlyTuition,
                Category = ChargeDiscountCategory.FullPaySubscription,
                Subcategory = ChargeDiscountSubcategory.Discount,
            };

            return subscrptionDiscount.Calculate(totalAmount);
        }

        public decimal CalculateSubscriptionProrateAmount(ProgramSubscriptionItem prorateItem, OrderItem orderItem)
        {
            decimal result = 0;

            var subscriptionAttribute = GetSubscriptionAtribute(orderItem);

            var prorateTime = subscriptionAttribute.ProrateTime;

            result = CalculateSubscriptionProrateAmount(prorateItem, prorateTime);

            return result;
        }

        public List<ProgramSubscriptionItem> GetProgramSubscriptionItems(OrderItem orderItem)
        {
            List<ProgramSubscriptionItem> result = new List<ProgramSubscriptionItem>();

            var subscriptinAttribute = GetSubscriptionAtribute(orderItem);

            result = subscriptinAttribute.SubscriptionItems;

            return result;
        }

        public decimal CalclulateSurcharge(OrderItem orderItem, Charge surcharge)
        {
            decimal result = 0;

            if (orderItem.Mode != OrderItemMode.Noraml)
            {
                return result;
            }

            var charge = new SubscriptionChargeDiscount(surcharge, true);

            switch (orderItem.ProgramTypeCategory)
            {
                case ProgramTypeCategory.Subscription:
                    {
                        var programScheduleParts = _programBusiness.GetScheduleParts(orderItem.ProgramSchedule.ProgramId);

                        var appliedSubscriptionItems = GetAppliedProgramParts(programScheduleParts, orderItem.ProgramSchedule, orderItem.DesiredStartDate);

                        var orderCharge = orderItem.OrderChargeDiscounts.Single(o => !o.IsDeleted && o.Category == ChargeDiscountCategory.EntryFee);

                        var selectedCharge = orderCharge.Charge;
                        var amount = selectedCharge.Amount;

                        foreach (var item in appliedSubscriptionItems)
                        {
                            foreach (var discount in orderItem.GetOrderChargeDiscounts().Where(d => d.Subcategory == ChargeDiscountSubcategory.Discount))
                            {
                                var subDiscount = new SubscriptionChargeDiscount(discount, appliedSubscriptionItems.Count());

                                amount = subDiscount.Apply(amount);
                            }

                            result += charge.Calculate(amount);
                        }

                        var applicationFee = GetApplicationFee(orderItem);

                        if (applicationFee != null && orderItem.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.PartnerApplicationFee))
                        {
                            result += charge.Calculate(applicationFee.Amount);
                        }
                    }
                    break;
                case ProgramTypeCategory.BeforeAfterCare:
                    {
                        var programScheduleParts = _programBusiness.GetScheduleParts(orderItem.ProgramSchedule.ProgramId);

                        var appliedProgramParts = GetAppliedProgramParts(programScheduleParts, orderItem.ProgramSchedule, orderItem.DesiredStartDate);

                        var orderCharge = orderItem.OrderChargeDiscounts.Single(o => !o.IsDeleted && o.Category == ChargeDiscountCategory.EntryFee);
                        //var selectedCharge = GetSelectedCharge(orderItem);
                        var selectedCharge = orderCharge.Charge;
                        var programPartCharges = _programBusiness.GetSchedulePartCharges(selectedCharge);
                        var appliedProgramPart = GetAppliedProgramPart(orderItem.ProgramSchedule, orderItem.DesiredStartDate);


                        foreach (var programPart in appliedProgramParts)
                        {
                            decimal amount = 0;
                            var isItemCurrent = false;

                            if (appliedProgramPart != null && appliedProgramPart.DueDate == programPart.DueDate)
                            {
                                isItemCurrent = true;
                            }
                            if (isItemCurrent)
                            {
                                if (IsProrateProgramPart(programPart, orderItem.ProgramSchedule, orderItem.DesiredStartDate))
                                {
                                    var selectedDays = orderItem.Attributes.WeekDays;
                                    amount = CalculateBeforeAfterCareProrateAmount(programPart, orderItem.DesiredStartDate.Value, selectedCharge, selectedDays);
                                }
                                else
                                {
                                    amount = programPartCharges.Where(c => c.SchedulePartId == programPart.Id).FirstOrDefault().ChargeAmount;
                                }
                            }
                            else
                            {
                                amount = programPartCharges.Where(c => c.SchedulePartId == programPart.Id).FirstOrDefault().ChargeAmount;
                            }

                            foreach (var discount in orderItem.GetOrderChargeDiscounts().Where(d => d.Subcategory == ChargeDiscountSubcategory.Discount))
                            {
                                var subDiscount = new SubscriptionChargeDiscount(discount, appliedProgramParts.Count());

                                amount = subDiscount.Apply(amount);
                            }

                            result += charge.Calculate(amount);

                        }

                        var applicationFee = GetApplicationFee(orderItem);

                        if (applicationFee != null)
                        {
                            result += charge.Calculate(applicationFee.Amount);
                        }

                    }
                    break;
                default:
                    break;
            }
            return result;
        }

        public decimal CalculateSubscriptionPayableAmount(OrderItem orderItem)
        {
            decimal result = 0;

            if (orderItem.PaymentPlanType == PaymentPlanType.FullPaid)
            {
                return orderItem.CalculateTotalAmount();
            }

            switch (orderItem.ProgramTypeCategory)
            {
                case ProgramTypeCategory.Subscription:
                    {
                        var subsctriptionAttribute = GetSubscriptionAtribute(orderItem);

                        var programParts = _programBusiness.GetScheduleParts(orderItem.ProgramSchedule.ProgramId);

                        var isFullPaidEnabled = subsctriptionAttribute.HasPayInFullOption;

                        if (orderItem.PaymentPlanType == PaymentPlanType.FullPaid && isFullPaidEnabled)
                        {
                            result = GetTotalAmount(programParts, orderItem);
                        }
                        else
                        {
                            result = GetFirstItemAmount(programParts, orderItem);
                        }

                        result += CalculateApplicationFeeAmount(orderItem);

                        if (result < 0)
                        {
                            result = 0;
                        }
                    }
                    break;
                case ProgramTypeCategory.BeforeAfterCare:
                    {
                        var programParts = _programBusiness.GetScheduleParts(orderItem.ProgramSchedule.ProgramId);

                        var beforeAfterAttribute = GetBeforAfterCareAtribute(orderItem.ProgramSchedule);
                        var isFullPaidEnabled = beforeAfterAttribute.HasPayInFullOption;

                        var getFirstInstallmentInRegistrationTime = beforeAfterAttribute.GetFirstPaymentAtTheRegisterTime;

                        if (orderItem.PaymentPlanType == PaymentPlanType.FullPaid && isFullPaidEnabled)
                        {
                            result = GetTotalAmount(programParts, orderItem);
                        }
                        else
                        {
                            var passSchedulePartDudate = GetPassScheduleDueDate(orderItem, programParts);

                            if (passSchedulePartDudate.Count > 0)
                            {
                                foreach (var passPart in passSchedulePartDudate)
                                {
                                    result += getAmountSchedulePart(orderItem, passPart);
                                }

                            }
                            else if (getFirstInstallmentInRegistrationTime)
                            {
                                result = GetFirstItemAmount(programParts, orderItem);
                            }
                        }

                        if (orderItem.OrderChargeDiscounts.Any(o => !o.IsDeleted && o.Category == ChargeDiscountCategory.ApplicationFee || o.Category == ChargeDiscountCategory.PartnerApplicationFee))
                        {
                            result += CalculateApplicationFeeAmount(orderItem);
                        }

                        if (result < 0)
                        {
                            result = 0;
                        }
                    }
                    break;
                default:
                    break;
            }

            return Math.Floor(result * 100) / 100;
        }

        public List<ProgramSchedulePart> GetPassScheduleDueDate(OrderItem orderItem, List<ProgramSchedulePart> scheduleParts)
        {
            var passScheduleparts = new List<ProgramSchedulePart>();

            var charge = GetSelectedCharge(orderItem);

            var club = orderItem.Order.Club;
            DateTime nowClubDateTime = DateTimeHelper.GetCurrentLocalDateTime(club.TimeZone);

            var appliedProgramParts = GetAppliedProgramParts(scheduleParts, orderItem.ProgramSchedule, orderItem.DesiredStartDate);

            foreach (var part in appliedProgramParts)
            {
                if (part.DueDate <= nowClubDateTime)
                {
                    passScheduleparts.Add(part);
                }
            }
            return passScheduleparts;
        }

        public int GetPassScheduleDueDate(OrderItem orderItem)
        {
            var countPassScheduleparts = 0;
            var club = orderItem.Order.Club;
            DateTime nowClubDateTime = DateTimeHelper.GetCurrentLocalDateTime(club.TimeZone);

            foreach (var item in orderItem.Installments.Where(i => i.Type != InstallmentType.AddOn))
            {
                if (item.InstallmentDate <= nowClubDateTime)
                {
                    countPassScheduleparts++;
                }
            }
            return countPassScheduleparts;
        }
        public decimal getAmountSchedulePart(OrderItem orderItem, ProgramSchedulePart Schedulepart)
        {
            decimal result = 0;
            var charge = GetSelectedCharge(orderItem);
            var programPartCharges = _programBusiness.GetSchedulePartCharges(charge);

            decimal partAmount = programPartCharges.Where(c => c.SchedulePartId == Schedulepart.Id).FirstOrDefault().ChargeAmount;
            result += CalculateItemAmount(orderItem, partAmount, Schedulepart);

            return result;
        }
        public void SetInstallmentsStatus(OrderItem orderItem, DateTime date)
        {
            var itemPayableAmount = CalculateSubscriptionPayableAmount(orderItem);

            orderItem.PaidAmount = itemPayableAmount;

            decimal appliedPayableAmount = 0;
            int i = 0;

            while (appliedPayableAmount < itemPayableAmount)
            {
                var installmentItem = orderItem.Installments.Where(o => !o.IsDeleted).ToList()[i];
                i++;

                installmentItem.Status = OrderStatusCategories.completed;
                installmentItem.PaidAmount = installmentItem.Amount;
                installmentItem.PaidDate = date;
                installmentItem.NoticeSent = true;
                installmentItem.Token = Guid.NewGuid().ToString("N");

                appliedPayableAmount += installmentItem.Amount;
            }
        }

        public bool IsInstallmentItemIsForPay(OrderItem orderItem, OrderInstallment orderInstallment, TransactionStatus transactionStatus)
        {
            var itemPayableAmount = CalculateSubscriptionPayableAmount(orderItem);

            orderItem.PaidAmount = transactionStatus == TransactionStatus.Success ? itemPayableAmount : orderItem.PaidAmount;

            decimal appliedPayableAmount = 0;
            int i = 0;

            var itemInstallments = orderItem.Installments;

            while (appliedPayableAmount < itemPayableAmount)
            {
                var installmentItem = itemInstallments[i];

                if (installmentItem.Id == orderInstallment.Id)
                {
                    return true;
                }

                i++;
                appliedPayableAmount += installmentItem.Amount;
            }

            return false;
        }

        public void CalculateInstallment(OrderItem orderItem, List<OrderInstallment> installmentList)
        {
            var item = orderItem;
            var order = orderItem.Order;
            List<OrderInstallment> installments = new List<OrderInstallment>();


            var programParts = _programBusiness.GetScheduleParts(item.ProgramSchedule.ProgramId);

            var subscriptionItems = GetProgramSubscriptionItems(orderItem);

            var programSchedule = orderItem.ProgramSchedule;

            var orderCharge = orderItem.OrderChargeDiscounts.Single(o => !o.IsDeleted && o.Category == ChargeDiscountCategory.EntryFee);

            var selectedCharge = orderCharge.Charge;

            order.IsAutoCharge = true;

            var applicationFeeInstallmentItem = GenerateApplicationFeeItem(orderItem);

            if (applicationFeeInstallmentItem != null)
            {
                installments.Add(applicationFeeInstallmentItem);
            }

            var appliedSubscriptionItems = GetAppliedProgramParts(programParts, orderItem.ProgramSchedule, orderItem.DesiredStartDate);

            foreach (var subscriptionItem in appliedSubscriptionItems)
            {
                var installmentItem = GenerateInstallmentFromProgramPart(subscriptionItem, orderItem, selectedCharge.Amount);

                if (installmentItem != null)
                {
                    installments.Add(installmentItem);
                }
            }

            item.Installments = installments;
            SetTheCorrectAmountForChargeDiscount(item, appliedSubscriptionItems);
            installmentList.AddRange(installments);

        }

        public void CalculateOrderInstallment(OrderItem orderItem, List<OrderInstallment> installmentList)
        {
            var item = orderItem;
            var order = orderItem.Order;
            List<OrderInstallment> installments = new List<OrderInstallment>();

            var programParts = _programBusiness.GetScheduleParts(item.ProgramSchedule.ProgramId);

            var programSchedule = orderItem.ProgramSchedule;

            var orderCharge = orderItem.OrderChargeDiscounts.Single(o => o.Category == ChargeDiscountCategory.EntryFee);

            var selectedCharge = orderCharge.Charge;
            order.IsAutoCharge = true;
            var isTestMode = !order.IsLive;
            decimal ApplicationAmount = 0;

            var applicationFee = _chargeBusiness.GetProgramSeasonCharges(orderItem.ProgramSchedule.Program, ChargeDiscountCategory.ApplicationFee).FirstOrDefault();

            var listDeletedCharge = new List<OrderChargeDiscount>();
            if (orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                if (orderItem.GetOrderChargeDiscounts() != null && orderItem.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.PartnerApplicationFee))
                {
                    foreach (var charge in orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.PartnerApplicationFee).ToList())
                    {
                        listDeletedCharge.Add(charge);
                        orderItem.TotalAmount -= charge.Amount;
                    }

                    if (listDeletedCharge.Count > 0)
                    {
                        _orderItemBusiness.RemoveOrderItemCharges(listDeletedCharge);
                    }
                }

                if (applicationFee != null)
                {
                    ApplicationAmount = applicationFee.Amount;
                    DateTime now = DateTimeHelper.GetCurrentLocalDateTime(order.Club.TimeZone);

                    if (applicationFee.Attributes != null && applicationFee.Attributes.EarlyBirds != null && applicationFee.Attributes.EarlyBirds.Any())
                    {
                        foreach (var earlyBrid in applicationFee.Attributes.EarlyBirds.OrderBy(e => e.ExpireDate))
                        {
                            if (now.Date <= earlyBrid.ExpireDate.Value.Date)
                            {
                                ApplicationAmount = earlyBrid.Price;
                                break;
                            }
                        }
                    }

                    var applicationFeeType = applicationFee.ApplyType;

                    switch (applicationFeeType)
                    {
                        case ChargeApplyType.OrderItem:
                            {
                                item.OrderChargeDiscounts.Add(new OrderChargeDiscount
                                {
                                    Amount = ApplicationAmount,
                                    Category = applicationFee.Category,
                                    ChargeId = applicationFee.Id,
                                    Description = applicationFee.Description,
                                    Name = applicationFee.Name,
                                    Subcategory = ChargeDiscountSubcategory.Charge,

                                });

                                item.TotalAmount += ApplicationAmount;
                            }
                            break;
                        case ChargeApplyType.PerFamilyPerSeason:
                            {
                                var hasPaidApplicationFeeInItems = _userProfileBusiness.HasPaidApplicationFeeInItems(order.ClubId, order.UserId, item.SeasonId.Value, isTestMode);

                                if (!hasPaidApplicationFeeInItems)
                                {
                                    item.OrderChargeDiscounts.Add(new OrderChargeDiscount
                                    {
                                        Amount = ApplicationAmount,
                                        Category = applicationFee.Category,
                                        ChargeId = applicationFee.Id,
                                        Description = applicationFee.Description,
                                        Name = applicationFee.Name,
                                        Subcategory = ChargeDiscountSubcategory.Charge
                                    });

                                    item.TotalAmount += ApplicationAmount;
                                }
                            }
                            break;
                        case ChargeApplyType.PerParticipantPerSeason:
                            {
                                var hasPaidApplicationFeeParticipant = _userProfileBusiness.HasPaidApplicationFeeParticipant(item.PlayerId.Value, item.SeasonId.Value, isTestMode);

                                if (!hasPaidApplicationFeeParticipant)
                                {
                                    item.OrderChargeDiscounts.Add(new OrderChargeDiscount
                                    {
                                        Amount = ApplicationAmount,
                                        Category = applicationFee.Category,
                                        ChargeId = applicationFee.Id,
                                        Description = applicationFee.Description,
                                        Name = applicationFee.Name,
                                        Subcategory = ChargeDiscountSubcategory.Charge
                                    });

                                    item.TotalAmount += ApplicationAmount;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            var applicationFeeInstallmentItem = GenerateApplicationFeeItem(orderItem);

            if (applicationFeeInstallmentItem != null)
            {
                installments.Add(applicationFeeInstallmentItem);
            }
            var appliedProgramParts = GetAppliedProgramParts(programParts, orderItem.ProgramSchedule, orderItem.DesiredStartDate);

            var programPartCharges = _programBusiness.GetSchedulePartCharges(selectedCharge);

            foreach (var programPart in appliedProgramParts)
            {
                decimal partAmount = programPartCharges.Where(c => c.SchedulePartId == programPart.Id).FirstOrDefault().ChargeAmount;

                var installmentItem = GenerateInstallmentFromProgramPart(programPart, orderItem, partAmount);

                if (installmentItem != null)
                {
                    installments.Add(installmentItem);
                }
            }

            item.Installments = installments;
            SetTheCorrectAmountForChargeDiscount(item, appliedProgramParts, programPartCharges);
            installmentList.AddRange(installments);
        }

        public int GetAppliedSubscriptionItemsCount(OrderItem orderItem)
        {
            int result = 0;

            var programScheduleParts = _programBusiness.GetScheduleParts(orderItem.ProgramSchedule.ProgramId);
            var applied = GetAppliedProgramParts(programScheduleParts, orderItem.ProgramSchedule, orderItem.DesiredStartDate);

            result = applied.Count();

            return result;
        }

        private void SetTheCorrectAmountForChargeDiscount(OrderItem item, List<ProgramSchedulePart> appliedProgramParts, List<ProgramSchedulePartCharge> programPartCharges = null)
        {
            if (item.GetOrderChargeDiscounts() != null && item.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Subcategory == ChargeDiscountSubcategory.Discount && c.Category != ChargeDiscountCategory.Coupon))
            {
                foreach (var discount in item.GetOrderChargeDiscounts().Where(c => c.Subcategory == ChargeDiscountSubcategory.Discount && c.Category != ChargeDiscountCategory.Coupon))
                {
                    if (discount.Discount.AmountType == ChargeDiscountType.Fixed)
                    {
                        if (item.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                        {
                            discount.Amount = (discount.Amount * appliedProgramParts.Count);
                        }
                        else
                        {
                            decimal amount = 0;
                            foreach (var programPart in appliedProgramParts)
                            {
                                decimal partAmount = programPartCharges.Where(c => c.SchedulePartId == programPart.Id).FirstOrDefault().ChargeAmount;

                                var partDiscountAmount = Math.Abs(discount.Amount) > partAmount ? partAmount : discount.Amount;

                                amount += partDiscountAmount; 
                            }

                            discount.Amount = (-1) * amount;
                        }
                    }
                }
            }
        }

        public Charge GetApplicationFee(Program program)
        {
            var programSchedule = program.ProgramSchedules.Last(s => !s.IsDeleted);
            var charge = programSchedule.Charges.SingleOrDefault(c => c.Category == ChargeDiscountCategory.ApplicationFee && !c.IsDeleted);
            var club = program.Club;
            DateTime nowClubDateTime = DateTimeHelper.GetCurrentLocalDateTime(club.TimeZone);

            if (charge != null)
            {
                if (charge.Attributes.EarlyBirds != null && charge.Attributes.EarlyBirds.Any())
                {
                    foreach (var earlyBrid in charge.Attributes.EarlyBirds.OrderBy(e => e.ExpireDate))
                    {
                        if (nowClubDateTime <= earlyBrid.ExpireDate.Value.Date)
                        {
                            charge.Amount = earlyBrid.Price;
                            break;
                        }
                    }
                }
            }

            return charge;
        }

        public Charge GetCharge(long id)
        {
            var charge = _dataContext.Set<Charge>().SingleOrDefault(i => i.Id == id);

            return charge;
        }

        public ProgramSchedulePart GetSubscriptionItemFromDesiredStartDate(Program program, DateTime desiredStartDate)
        {
            var programSchedule = program.ProgramSchedules.Last(s => !s.IsDeleted);

            //var subscriptionItems = GetSubscriptionAtribute(programSchedule).SubscriptionItems;
            //var result = GetAppliedSubscriptionItem(programSchedule, desiredStartDate);

            var programParts = _programBusiness.GetScheduleParts(program.Id);

            var result = GetAppliedProgramPart(programSchedule, desiredStartDate);

            return result;
        }

        public void ManipulateCartOrderItem(OrderItem orderItem)
        {
            var programScheduleParts = _programBusiness.GetScheduleParts(orderItem.ProgramSchedule.ProgramId);

            switch (orderItem.ProgramTypeCategory)
            {
                case ProgramTypeCategory.Subscription:
                    {
                        if (orderItem.PaymentPlanType == PaymentPlanType.Installment)
                        {
                            var subscriptionItem = GetAppliedProgramPart(orderItem.ProgramSchedule, orderItem.DesiredStartDate);
                            var selectedCharge = GetSelectedCharge(orderItem);

                            orderItem.Start = subscriptionItem.StartDate;
                            orderItem.End = subscriptionItem.EndDate;
                            orderItem.EntryFee = selectedCharge.Amount;
                        }
                        else
                        {
                            var appliedSubscripitionItems = GetAppliedProgramParts(programScheduleParts, orderItem.ProgramSchedule, orderItem.DesiredStartDate);

                            if (orderItem.Mode == OrderItemMode.DropIn)
                            {
                                var programSessions = GetDropInProgramSessions(orderItem.OrderSessions.ToList());

                                orderItem.Start = programSessions.Min(m => m.StartDateTime);
                                orderItem.End = programSessions.Max(m => m.EndDateTime);
                            }
                            else
                            {
                                orderItem.Start = appliedSubscripitionItems.Min(m => m.StartDate);
                                orderItem.End = appliedSubscripitionItems.Max(m => m.EndDate);
                            }
                        }
                    }
                    break;
                case ProgramTypeCategory.BeforeAfterCare:
                    {

                        if (orderItem.PaymentPlanType == PaymentPlanType.Installment)
                        {
                            var programAttribiuts = GetBeforAfterCareAtribute(orderItem.ProgramSchedule);
                            var getInstallmentInRegisterTime = programAttribiuts.GetFirstPaymentAtTheRegisterTime;

                            decimal amount = 0;


                            var programSchedulePart = GetAppliedProgramPart(orderItem.ProgramSchedule, orderItem.DesiredStartDate);

                            var selectedCharge = GetSelectedCharge(orderItem);

                            var programPartCharges = _programBusiness.GetSchedulePartCharges(selectedCharge);

                            var appliedProgramPart = GetAppliedProgramPart(orderItem.ProgramSchedule, orderItem.DesiredStartDate);

                            foreach (var programPart in programScheduleParts)
                            {
                                var isItemCurrent = false;

                                if (appliedProgramPart != null && appliedProgramPart.DueDate == programPart.DueDate)
                                {
                                    isItemCurrent = true;
                                }
                                if (isItemCurrent)
                                {
                                    if (IsProrateProgramPart(programPart, orderItem.ProgramSchedule, orderItem.DesiredStartDate))
                                    {
                                        var selectedDays = orderItem.Attributes.WeekDays;
                                        amount = CalculateBeforeAfterCareProrateAmount(programPart, orderItem.DesiredStartDate.Value, selectedCharge, selectedDays);
                                    }
                                    else
                                    {
                                        amount = programPartCharges.Where(c => c.SchedulePartId == programPart.Id).FirstOrDefault().ChargeAmount;
                                    }
                                }
                            }

                            orderItem.Start = programSchedulePart.StartDate;
                            orderItem.End = programSchedulePart.EndDate;
                            orderItem.EntryFee = Math.Floor(amount * 100) / 100;
                        }

                        else
                        {
                            var appliedProgramScheduleParts = GetAppliedProgramParts(programScheduleParts, orderItem.ProgramSchedule, orderItem.DesiredStartDate);

                            switch (orderItem.Mode)
                            {
                                case OrderItemMode.Noraml:
                                    {
                                        orderItem.Start = appliedProgramScheduleParts.Min(m => m.StartDate);
                                        orderItem.End = appliedProgramScheduleParts.Max(m => m.EndDate);
                                    }
                                    break;
                                case OrderItemMode.DropIn:
                                    {
                                        var programSessions = GetDropInProgramSessions(orderItem.OrderSessions.ToList());

                                        orderItem.Start = programSessions.Min(m => m.StartDateTime);
                                        orderItem.End = programSessions.Max(m => m.EndDateTime);
                                    }
                                    break;
                                case OrderItemMode.PunchCard:
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        public dynamic GetOrderItemAdditionalInformation(OrderItem orderItem)
        {
            var selectedCharge = GetSelectedCharge(orderItem);

            var attribute = GetBeforAfterCareAtribute(orderItem.ProgramSchedule);
            var paymentMode = attribute.PaymentScheduleMode == "Monthly" ? "Monthly payments" : attribute.PaymentScheduleMode == "BiWeekly" ? "Bi-weekly payments" : "Weekly payments";
            var Days = orderItem.Attributes.WeekDays;
            var itemDays = string.Empty;

            if (Days != null && Days.Any())
            {
                itemDays = string.Join(", ", Days);
            }

            dynamic model = new ExpandoObject();
            model.SelectedAmount = selectedCharge.Amount;
            model.PaymentMode = paymentMode;
            model.OrderItemDays = itemDays;

            return model;
        }

        #region Total amount

        public decimal GetTotalSubscriptionAmount(ProgramSchedule programSchedule, DateTime? desiredStartDate, long selectedChargeId = 0)
        {
            decimal result = 0;
            var clubId = programSchedule.Program.ClubId;

            var subscrtiptionAttribute = GetSubscriptionAtribute(programSchedule);
            var appliedSubscriptionItems = GetAppliedSubscriptionItems(clubId, subscrtiptionAttribute.SubscriptionItems, desiredStartDate);

            var selectedSubscriptionSchedule = GetSelectedCharge(programSchedule, selectedChargeId);

            appliedSubscriptionItems.ForEach(a => a.Amount = selectedSubscriptionSchedule.Amount);

            result = appliedSubscriptionItems.Sum(s => CalculateItemAmount(s, programSchedule));

            return result;
        }

        private decimal CalculateItemAmount(ProgramSubscriptionItem subscriptionItem, ProgramSchedule programSchedule)
        {
            decimal result = subscriptionItem.Amount;

            var club = programSchedule.Program.Club;
            DateTime nowClubDateTime = DateTimeHelper.GetCurrentLocalDateTime(club.TimeZone);

            var subscrtiptionAttribute = GetSubscriptionAtribute(programSchedule);

            if (IsProrate(subscriptionItem, subscrtiptionAttribute.IsProrate, nowClubDateTime))
            {
                result = CalculateSubscriptionProrateAmount(subscriptionItem, subscrtiptionAttribute.ProrateTime);
            }

            result = ApplyItemDiscounts(result, programSchedule);

            result = ApplyItemCharges(result, programSchedule);

            return result;
        }

        private decimal ApplyItemDiscounts(decimal subscriptionAmount, ProgramSchedule programSchedule)
        {
            decimal result = subscriptionAmount;

            var discounts = GetDiscounts(programSchedule);

            foreach (var discount in discounts)
            {
                result = discount.Apply(result);
            }

            return result;
        }

        private decimal ApplyItemCharges(decimal subscriptionAmount, ProgramSchedule programSchedule)
        {
            decimal result = subscriptionAmount;

            var charges = GetCharges(programSchedule);

            foreach (var charge in charges)
            {
                result = charge.Apply(result);
            }

            return result;
        }

        public decimal GetTotalBeforeAfterCareAmount(ProgramSchedule programSchedule, DateTime? desiredStartDate, List<DayOfWeek> selectedDays, long selectedChargeId = 0)
        {
            decimal result = 0;

            var programScheduleParts = _programBusiness.GetScheduleParts(programSchedule.ProgramId);

            var appliedProgramParts = GetAppliedProgramParts(programScheduleParts, programSchedule, desiredStartDate);

            var selectedScheduleCharge = GetSelectedCharge(programSchedule, selectedChargeId);

            var programPartCharges = _programBusiness.GetSchedulePartCharges(selectedScheduleCharge);

            foreach (var programPart in appliedProgramParts)
            {
                decimal amount = 0;
                if (IsProrateProgramPart(programPart, programSchedule, desiredStartDate))
                {
                    amount = CalculateBeforeAfterCareProrateAmount(programPart, desiredStartDate.Value, selectedScheduleCharge, selectedDays);
                }
                else
                {
                    if (programSchedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
                    {
                        amount = programPartCharges.Where(c => c.SchedulePartId == programPart.Id).FirstOrDefault().ChargeAmount;
                    }
                    else // is subscription
                    {
                        amount = selectedScheduleCharge.Amount;
                    }
                }

                amount = ApplyItemDiscounts(amount, programSchedule);

                amount = ApplyItemCharges(amount, programSchedule);

                result += amount;
            }

            return result;
        }

        #endregion

        private OrderInstallment GenerateApplicationFeeItem(OrderItem orderItem)
        {
            OrderInstallment result = null;

            if (orderItem.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.PartnerApplicationFee))
            {
                result =
                  new OrderInstallment
                  {
                      Amount = CalculateApplicationFeeAmount(orderItem),
                      EnableReminder = false,
                      InstallmentDate = DateTime.UtcNow,
                      PaidAmount = 0,
                      Status = OrderStatusCategories.initiated,
                      Type = InstallmentType.AddOn
                  };
            }

            return result;
        }

        private bool IsProrate(ProgramSubscriptionItem subscriptionItem, OrderItem orderItem)
        {
            var result = false;
            var club = orderItem.Order.Club;
            DateTime nowClubDateTime = DateTimeHelper.GetCurrentLocalDateTime(club.TimeZone);

            var subscriptinAttribute = GetSubscriptionAtribute(orderItem);

            result = IsProrate(subscriptionItem, subscriptinAttribute.IsProrate, nowClubDateTime);

            return result;
        }

        public bool IsProrateProgramPart(ProgramSchedulePart appliedProgramPart, ProgramSchedule schedule, DateTime? desiredStartDate)
        {
            var result = false;

            var scheduleAttribute = GetBeforAfterCareAtribute(schedule);

            if (scheduleAttribute.IsProrate)
            {
                result = IsProrate(appliedProgramPart, desiredStartDate.Value, scheduleAttribute.IsProrate);
            }

            return result;
        }

        private bool IsProrate(ProgramSchedulePart appliedProgramPart, DateTime desiredStartDate, bool isProrate)
        {
            var result = false;

            if (isProrate)
            {
                result = appliedProgramPart.StartDate <= desiredStartDate && appliedProgramPart.EndDate >= desiredStartDate;
            }

            return result;
        }
        private bool IsProrate(ProgramSubscriptionItem subscriptionItem, bool isProrate, DateTime nowClubDateTime)
        {
            var result = false;

            if (isProrate)
            {
                result = subscriptionItem.StartDate <= nowClubDateTime && subscriptionItem.EndDate >= nowClubDateTime;
            }

            return result;
        }

        private decimal CalculateSubscriptionProrateAmount(ProgramSubscriptionItem prorateItem, DateTime prorateTime)
        {
            decimal result = 0;

            if (prorateItem != null)
            {
                var date = prorateItem.StartDate;
                var endDate = prorateItem.EndDate.AddMinutes(prorateTime.TimeOfDay.TotalMinutes);

                var dates = new List<DateTime>();

                while (date <= endDate)
                {
                    dates.Add(date);
                    date = date.AddDays(1);
                }

                var endWeekDays = dates.Where(d => d.DayOfWeek == DayOfWeek.Friday);

                var weeks = endWeekDays.Count();
                var passedWeek = dates.Count(d => d.DayOfWeek == DayOfWeek.Friday && d.Date < _nowDateTime);
                var remainingWeeks = weeks - passedWeek;

                var prorateAmount = remainingWeeks * prorateItem.Amount / weeks;

                result += prorateAmount;
            }

            return result;
        }

        private ScheduleSubscriptionAttribute GetSubscriptionAtribute(OrderItem orderItem)
        {
            var result = GetSubscriptionAtribute(orderItem.ProgramSchedule);

            return result;
        }

        public ScheduleSubscriptionAttribute GetSubscriptionAtribute(ProgramSchedule programSchedule)
        {

            var result = JsonConvert.DeserializeObject<ScheduleSubscriptionAttribute>(programSchedule.AttributesSerialized);

            return result;
        }

        public ScheduleAfterBeforeCareAttribute GetBeforAfterCareAtribute(ProgramSchedule programSchedule)
        {

            var result = JsonConvert.DeserializeObject<ScheduleAfterBeforeCareAttribute>(programSchedule.AttributesSerialized);

            return result;
        }
        public decimal CalculateChargeForAppliedItems(OrderItem orderItem, OrderChargeDiscount charge)
        {
            decimal result = 0;

            if (orderItem.PaymentPlanType == PaymentPlanType.FullPaid)
            {
                return charge.Amount;
            }

            if (charge.Category != ChargeDiscountCategory.Surcharge && charge.Category != ChargeDiscountCategory.PartnerSurcharge)
            {
                return charge.Amount;
            }
            else
            {
                switch (orderItem.ProgramTypeCategory)
                {
                    case ProgramTypeCategory.Subscription:
                        {
                            var appliedItem = GetAppliedProgramPart(orderItem.ProgramSchedule, orderItem.DesiredStartDate);

                            var orderCharge = orderItem.OrderChargeDiscounts.Single(o => !o.IsDeleted && o.Category == ChargeDiscountCategory.EntryFee);

                            var selectedCharge = orderCharge.Charge;

                            var amount = selectedCharge.Amount;

                            var applicationFee = GetApplicationFee(orderItem);

                            var surcharges = GetSurcharges(orderItem);

                            var surCharge = charge.Category == ChargeDiscountCategory.PartnerSurcharge
                                ? surcharges.FirstOrDefault(s => s.Category == ChargeDiscountCategory.PartnerSurcharge)
                                : surcharges.FirstOrDefault(s => s.Category == ChargeDiscountCategory.Surcharge);

                            foreach (var discount in orderItem.GetOrderChargeDiscounts().Where(d => d.Subcategory == ChargeDiscountSubcategory.Discount))
                            {
                                var subDiscount = new SubscriptionChargeDiscount(discount);

                                amount = subDiscount.Apply(amount);
                            }

                            result = surCharge.Calculate(amount);
                            if (applicationFee != null)
                            {
                                result += surCharge.Calculate(applicationFee.Amount);
                            }
                        }
                        break;
                    case ProgramTypeCategory.BeforeAfterCare:
                        {
                            var appliedItem = GetAppliedProgramPart(orderItem.ProgramSchedule, orderItem.DesiredStartDate);

                            var orderCharge = orderItem.OrderChargeDiscounts.Single(o => !o.IsDeleted && o.Category == ChargeDiscountCategory.EntryFee);

                            var selectedCharge = orderCharge.Charge;

                            var programPartCharges = _programBusiness.GetSchedulePartCharges(selectedCharge);

                            decimal amount = 0;

                            if (IsProrateProgramPart(appliedItem, orderItem.ProgramSchedule, orderItem.DesiredStartDate))
                            {
                                var selectedDays = orderItem.Attributes.WeekDays;
                                amount = CalculateBeforeAfterCareProrateAmount(appliedItem, orderItem.DesiredStartDate.Value, selectedCharge, selectedDays);
                            }
                            else
                            {
                                amount = programPartCharges.Where(c => c.SchedulePartId == appliedItem.Id).FirstOrDefault().ChargeAmount;
                            }

                            var applicationFee = GetApplicationFee(orderItem);

                            var surcharges = GetSurcharges(orderItem);

                            var surCharge = charge.Category == ChargeDiscountCategory.PartnerSurcharge
                                ? surcharges.FirstOrDefault(s => s.Category == ChargeDiscountCategory.PartnerSurcharge)
                                : surcharges.FirstOrDefault(s => s.Category == ChargeDiscountCategory.Surcharge);


                            foreach (var discount in orderItem.OrderChargeDiscounts.Where(d => !d.IsDeleted && d.Subcategory == ChargeDiscountSubcategory.Discount))
                            {
                                var subDiscount = new SubscriptionChargeDiscount(discount);

                                amount = subDiscount.Apply(amount);
                            }

                            result = surCharge.Calculate(amount);

                            if (applicationFee != null && orderItem.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.PartnerApplicationFee))
                            {
                                result += surCharge.Calculate(applicationFee.Amount);
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            return result;
        }

        public decimal CalculateDiscountForAppliedItems(OrderItem orderItem, OrderChargeDiscount discount)
        {
            if (orderItem.PaymentPlanType == PaymentPlanType.FullPaid)
            {
                return Math.Abs(discount.Amount);
            }

            decimal result = 0;

            switch (orderItem.ProgramTypeCategory)
            {
                case ProgramTypeCategory.Subscription:
                    {
                        var appliedItem = GetAppliedProgramPart(orderItem.ProgramSchedule, orderItem.DesiredStartDate);

                        var orderCharge = orderItem.OrderChargeDiscounts.Single(o => !o.IsDeleted && o.Category == ChargeDiscountCategory.EntryFee);

                        var selectedCharge = orderCharge.Charge;

                        var amount = selectedCharge.Amount;

                        var programDiscount = discount.Discount;

                        if (programDiscount != null)
                        {
                            var subscriptionDiscount = new SubscriptionChargeDiscount(programDiscount);

                            if (subscriptionDiscount != null)
                            {
                                result = subscriptionDiscount.Calculate(amount);
                            }
                        }
                        else
                        {
                            var subscriptionDiscount = new SubscriptionChargeDiscount(discount);

                            result = subscriptionDiscount.Calculate(amount);
                        }
                    }
                    break;
                case ProgramTypeCategory.BeforeAfterCare:
                    {
                        var appliedItem = GetAppliedProgramPart(orderItem.ProgramSchedule, orderItem.DesiredStartDate);

                        var orderCharge = orderItem.OrderChargeDiscounts.Single(o => !o.IsDeleted && o.Category == ChargeDiscountCategory.EntryFee);

                        var selectedCharge = orderCharge.Charge;

                        var programPartCharges = _programBusiness.GetSchedulePartCharges(selectedCharge);

                        decimal amount = 0;

                        if (IsProrateProgramPart(appliedItem, orderItem.ProgramSchedule, orderItem.DesiredStartDate))
                        {
                            var selectedDays = orderItem.Attributes.WeekDays;
                            amount = CalculateBeforeAfterCareProrateAmount(appliedItem, orderItem.DesiredStartDate.Value, selectedCharge,selectedDays);
                        }
                        else
                        {
                            amount = programPartCharges.Where(c => c.SchedulePartId == appliedItem.Id).FirstOrDefault().ChargeAmount;
                        }

                        var programDiscount = discount.Discount;

                        if (programDiscount != null)
                        {
                            var beforeAfterCareDiscount = new SubscriptionChargeDiscount(programDiscount);

                            if (beforeAfterCareDiscount != null)
                            {
                                result = beforeAfterCareDiscount.Calculate(amount);
                            }
                        }
                        else
                        {
                            var beforeAfterCareDiscount = new SubscriptionChargeDiscount(discount);

                            result = beforeAfterCareDiscount.Calculate(amount);
                        }
                    }
                    break;
                default:
                    break;
            }

            return result;
        }

        public decimal CalculateDiscountForShowInView(OrderItem orderItem, OrderChargeDiscount discount, decimal totalDiscountAmount)
        {
            decimal result = 0;

            var countInstallment = orderItem.Installments.Where(i => i.Amount > 0 && i.Type == InstallmentType.Noraml).ToList().Count;
            var applicationFee = orderItem.Installments.Where(i => i.Type == InstallmentType.AddOn).ToList();
            var couponMode = discount.Coupon.CouponCalculateType;

            var orderCharge = orderItem.OrderChargeDiscounts.Single(o => !o.IsDeleted && o.Category == ChargeDiscountCategory.EntryFee);
            var selectedCharge = orderCharge.Charge;

            var appliedItem = GetAppliedProgramPart(orderItem.ProgramSchedule, orderItem.DesiredStartDate);

            decimal amount = 0;

            if (orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                var programPartCharges = _programBusiness.GetSchedulePartCharges(selectedCharge);
                if (IsProrateProgramPart(appliedItem, orderItem.ProgramSchedule, orderItem.DesiredStartDate))
                {
                    var selectedDays = orderItem.Attributes.WeekDays;
                    amount = CalculateBeforeAfterCareProrateAmount(appliedItem, orderItem.DesiredStartDate.Value, selectedCharge, selectedDays);
                }
                else
                {
                    amount = programPartCharges.Where(c => c.SchedulePartId == appliedItem.Id).FirstOrDefault().ChargeAmount;
                }
            }
            else
            {
                amount = selectedCharge.Amount;
            }

            if (applicationFee.Count > 0 && couponMode == CalculationType.TotalAmount)
            {
                var count = (countInstallment + 1);

                if (discount.Coupon.AmounType == ChargeDiscountType.Fixed)
                {
                    var appAmount = _clubBusiness.GetSeasonApplicationFee(orderItem.Season);
                    if (amount == 0)
                    {
                        result = appAmount != null ? appAmount.Amount : 0;
                    }
                    else if (amount < discount.Coupon.Amount)
                    {
                        result = amount + appAmount.Amount;
                    }
                    else
                    {
                        if (orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                        {
                            result = (totalDiscountAmount / count) + (discount.Coupon.Amount);
                        }
                        else
                        {
                            result = discount.Coupon.Amount;
                            var orderAppAmount = orderItem.GetOrderChargeDiscounts().Where(o => o.Category == ChargeDiscountCategory.ApplicationFee).FirstOrDefault().Amount;

                            if (orderAppAmount > discount.Coupon.Amount)
                            {
                                result += discount.Coupon.Amount;
                            }
                            else
                            {
                                result += orderAppAmount;
                            }
                        }
                    }
                }
                else
                {
                    decimal appAmount = 0;
                    if (orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                    {

                        appAmount = _clubBusiness.GetSeasonApplicationFee(orderItem.Season).Amount;
                    }
                    else
                    {
                        appAmount = orderItem.GetOrderChargeDiscounts().Where(o => o.Category == ChargeDiscountCategory.ApplicationFee).FirstOrDefault().Amount;
                    }

                    var instAmount = amount + appAmount;
                    var coupomAmount = (instAmount * discount.Coupon.Amount) / 100;

                    if (orderItem.PaymentPlanType == PaymentPlanType.Installment)
                    {
                        result = coupomAmount;
                    }
                }
            }
            else
            {
                if (orderItem.PaymentPlanType == PaymentPlanType.Installment)
                {
                    if (discount.Coupon.AmounType == ChargeDiscountType.Percent)
                    {

                        result = (amount * discount.Coupon.Amount) / 100;
                    }
                    else
                    {
                        if (amount == 0)
                        {
                            result = 0;
                        }
                        else if ((discount.Discount != null && amount < discount.Discount.Amount) || (discount.Coupon != null && amount < discount.Coupon.Amount))
                        {
                            result = amount;
                        }
                        else
                        {
                            result = totalDiscountAmount / countInstallment;
                        }
                    }
                }
                else
                {
                    result = totalDiscountAmount;
                }
            }
            return result;
        }
        public List<ProgramSession> GetDropInProgramSessions(Program program, List<long> sessionIds)
        {
            var result = new List<ProgramSession>();

            result = _programSessionBusiness.GetList(program).Where(p => sessionIds.Contains(p.Id)).ToList();

            return result;
        }

        public List<ProgramSession> GetDropInProgramSessions(ProgramSchedule schedule, List<long> sessionIds)
        {
            var result = new List<ProgramSession>();

            result = _programSessionBusiness.GetProgramSessions(schedule).Where(p => sessionIds.Contains(p.Id)).ToList();

            return result;
        }
        public List<ProgramSession> GetDropInProgramSessions(List<OrderSession> orderSessions)
        {
            List<ProgramSession> result = null;

            result = orderSessions.Select(o => o.ProgramSession).ToList();

            return result;
        }

        private decimal CalculateApplicationFeeAmount(OrderItem orderItem)
        {
            decimal result = 0;

            var applicationFee = GetApplicationFee(orderItem);

            result = applicationFee != null ? applicationFee.Amount : 0;

            result = ApplyItemDiscounts(result, orderItem, true);

            result = ApplyItemCharges(result, orderItem);

            return result;
        }

        private decimal CalculateApplicationFeeAmount(ProgramSchedule programSchedule)
        {
            decimal result = 0;

            var applicationFee = GetApplicationFee(programSchedule);

            result = applicationFee != null ? applicationFee.Amount : 0;

            return result;
        }

        private SubscriptionChargeDiscount GetApplicationFee(OrderItem orderItem)
        {
            SubscriptionChargeDiscount result = null;

            if (orderItem.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.PartnerApplicationFee))
            {
                var applicationFeeCharge = orderItem.OrderChargeDiscounts.SingleOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.PartnerApplicationFee);

                result = new SubscriptionChargeDiscount(applicationFeeCharge);
            }

            return result;
        }

        private SubscriptionChargeDiscount GetApplicationFee(ProgramSchedule programSchedule)
        {
            SubscriptionChargeDiscount result = null;

            if (programSchedule.Charges.Any(c => c.Category == ChargeDiscountCategory.ApplicationFee))
            {
                var applicationFeeCharge = programSchedule.Charges.Single(c => c.Category == ChargeDiscountCategory.ApplicationFee);

                result = new SubscriptionChargeDiscount(applicationFeeCharge);
            }

            return result;
        }

        private OrderInstallment GenerateInstallmentFromSubscriptionItem(ProgramSubscriptionItem subscriptionItem, OrderItem orderItem)
        {

            OrderInstallment result = new OrderInstallment
            {
                Amount = CalculateItemAmount(subscriptionItem, orderItem),
                EnableReminder = false,
                InstallmentDate = subscriptionItem.DueDate,
                PaidAmount = 0,
                Status = OrderStatusCategories.initiated,
            };

            return result;
        }
        private OrderInstallment GenerateInstallmentFromProgramPart(ProgramSchedulePart programPart, OrderItem orderItem, decimal partAmount)
        {

            OrderInstallment result = new OrderInstallment
            {
                Amount = CalculateItemAmount(orderItem, partAmount, programPart),
                EnableReminder = false,
                InstallmentDate = programPart.DueDate.Value,
                PaidAmount = 0,
                Status = OrderStatusCategories.initiated,
                ProgramSchedulePartId = programPart.Id,
            };

            return result;
        }
        public decimal CalculateBeforeAfterCareProrateAmount(ProgramSchedulePart prorateItem, DateTime desiredStartDate, Charge charge, List<DayOfWeek> days)
        {
            decimal result = 0;

            var club = charge.ProgramSchedule.Program.Club;

            if (club.PartnerId.HasValue && club.PartnerClub.Domain == "kidstable")
            {
                result = CalculateProrateAmountBySessions(prorateItem, desiredStartDate, charge, days);

                return result;
            }

            if (prorateItem != null && desiredStartDate > prorateItem.StartDate)
            {
                var date = prorateItem.StartDate;
                var endDate = prorateItem.EndDate;
                var allScheduledates = new List<DateTime>();

                //get days in the schedulePart
                while (date <= endDate)
                {
                    allScheduledates.Add(date);
                    date = date.AddDays(1);
                }

                var desiredStartDays = 0;

                foreach (var partdate in allScheduledates)
                {
                    if (partdate < desiredStartDate)
                    {
                        desiredStartDays++;
                    }
                }

                var remainingDays = allScheduledates.Count - desiredStartDays;

                var programPartCharges = _programBusiness.GetSchedulePartCharges(charge);

                decimal proratePartAmount = programPartCharges.Where(c => c.SchedulePartId == prorateItem.Id).FirstOrDefault().ChargeAmount;

                var prorateAmount = (remainingDays * proratePartAmount / allScheduledates.Count);

                result += prorateAmount;
            }
            else
            {
                var programPartCharges = _programBusiness.GetSchedulePartCharges(charge);
                decimal amount = programPartCharges.Where(c => c.SchedulePartId == prorateItem.Id).FirstOrDefault().ChargeAmount;

                result = amount;
            }

            return Math.Floor(result * 100) / 100;
        }

        private decimal CalculateProrateAmountBySessions(ProgramSchedulePart prorateItem, DateTime startDate, Charge charge, List<DayOfWeek> selectedDays)
        {
            decimal result = 0;
            var programPartCharges = _programBusiness.GetSchedulePartCharges(charge);

            var selectedDaysSessionInMonth = new List<ProgramSession>();

            var currentScheduleSessionsInPart = GetProgramPartSessions(prorateItem, charge.ProgramSchedule).ToList();

            foreach (var session in currentScheduleSessionsInPart)
            {
                if (selectedDays.Contains(session.StartDateTime.DayOfWeek))
                {
                    selectedDaysSessionInMonth.Add(session);
                }
            }

            var orderSessionsInMonth = selectedDaysSessionInMonth.Where(s => s.StartDateTime >= startDate).ToList();

            var firstOfCurrentSessions = selectedDaysSessionInMonth.OrderBy(c => c.StartDateTime).First();
            var firstSessionInMonth = orderSessionsInMonth.OrderBy(s => s.StartDateTime).First();

            if (prorateItem != null && firstSessionInMonth.StartDateTime > firstOfCurrentSessions.StartDateTime)
            {
                //var partAmount = programPartCharges.FirstOrDefault(c => c.ProgramPartId == prorateItem.Id);

                var partAmount = programPartCharges.FirstOrDefault(c => c.SchedulePartId == prorateItem.Id);

                var amount = partAmount != null ? partAmount.ChargeAmount : charge.Amount;

                result = orderSessionsInMonth.Count * amount / selectedDaysSessionInMonth.Count;
            }
            else if (prorateItem != null)
            {
                var proratePart = programPartCharges.FirstOrDefault(c => c.SchedulePartId == prorateItem.Id);

                var amount = proratePart != null ? proratePart.ChargeAmount : charge.Amount;

                result = amount;
            }

            return result;
        }

        public IEnumerable<ProgramSession> GetProgramPartSessions(ProgramSchedulePart schedulePart, ProgramSchedule currentSchedule)
        {
            var programSessions = new List<ProgramSession>();

            if (currentSchedule.ScheduleMode == TimeOfClassFormation.Both)
            {
                var schedules = currentSchedule.Program.ProgramSchedules.Where(s => s.ScheduleMode != TimeOfClassFormation.Both);

                foreach (var schedule in schedules)
                {
                    programSessions.AddRange(ProgramSessionBusiness.GetList(schedule));
                }
            }
            else
            {
                programSessions = ProgramSessionBusiness.GetList(currentSchedule);
            }

            var currentScheduleSessions = programSessions.Where(s => s.StartDateTime.Date >= schedulePart.StartDate.Date && s.EndDateTime.Date <= schedulePart.EndDate.Date).ToList();

            return currentScheduleSessions;
        }
        private decimal ApplyItemCharges(decimal subscriptionAmount, OrderItem orderItem)
        {
            var result = subscriptionAmount;

            var orderCharges = new List<SubscriptionChargeDiscount>();

            orderCharges.AddRange(GetSurcharges(orderItem));

            foreach (var orderCharge in orderCharges)
            {
                result = orderCharge.Apply(result);
            }

            return result;
        }

        private decimal ApplyItemDiscounts(decimal subscriptionAmount, OrderItem orderItem, bool isAdditionalCharge = false)
        {
            var partAmount = subscriptionAmount;
            var result = subscriptionAmount;
            var discounts = GetDiscounts(orderItem);

            foreach (var discount in discounts)
            {
                result = discount.Apply(result, partAmount, isAdditionalCharge);
            }

            return result;
        }

        private List<SubscriptionChargeDiscount> GetDiscounts(OrderItem orderItem)
        {
            var result = orderItem.GetOrderChargeDiscounts().Where(c => c.Subcategory == ChargeDiscountSubcategory.Discount && c.Category != ChargeDiscountCategory.Coupon).Select(c => new SubscriptionChargeDiscount(c)).ToList();

            result.AddRange(orderItem.GetOrderChargeDiscounts().Where(c => c.Subcategory == ChargeDiscountSubcategory.Discount && c.Category == ChargeDiscountCategory.Coupon).Select(c => new SubscriptionChargeDiscount(c.Coupon)).ToList());

            return result;
        }

        private List<SubscriptionChargeDiscount> GetDiscounts(ProgramSchedule programSchedule)
        {
            var result = new List<SubscriptionChargeDiscount>();

            //result.AddRange(GetCoupons(programSchedule));

            return result;
        }

        private List<SubscriptionChargeDiscount> GetCharges(ProgramSchedule programSchedule)
        {
            var result = new List<SubscriptionChargeDiscount>();



            return result;
        }

        private decimal CalculateCoupon(decimal subscriptionAmount, SubscriptionChargeDiscount discount, bool isAdditionalCharge = false)
        {
            var result = subscriptionAmount;

            if (isAdditionalCharge && discount.CalculationType == CalculationType.OnlyTuition)
            {
                return result;
            }

            //var couponAmount = coupon.AmounType == ChargeDiscountType.Fixed ? coupon.Amount : (coupon.Amount * subscriptionAmount / 100);

            result = discount.Apply(result);

            return result;
        }

        private List<SubscriptionChargeDiscount> GetSurcharges(OrderItem orderItem)
        {
            var result = new List<SubscriptionChargeDiscount>();

            var club = orderItem.Order.Club;
            var chargeDiscounts = orderItem.GetOrderChargeDiscounts().Select(c => new SubscriptionChargeDiscount(c));

            Charge clubSurcharge = null;

            Charge partnerSurcharge = null;

            if (chargeDiscounts != null && chargeDiscounts.Any())
            {
                if (club.Charges != null && club.Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge))
                {
                    clubSurcharge = club.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge);
                }

                if (club.PartnerClub != null && club.PartnerClub.Charges != null && club.PartnerClub.Charges.Any(c => c.IsDeleted == false && c.Category == ChargeDiscountCategory.PartnerSurcharge))
                {
                    partnerSurcharge = club.PartnerClub.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.PartnerSurcharge);
                }
            }

            if (clubSurcharge != null)
            {
                result.Add(new SubscriptionChargeDiscount(clubSurcharge, true));
            }

            if (partnerSurcharge != null)
            {
                result.Add(new SubscriptionChargeDiscount(partnerSurcharge, true));
            }

            return result;
        }
        private bool IsItemCurrent(ProgramSchedule programScedule, ProgramSubscriptionItem subscriptionItem, DateTime? desiredStartDate)
        {
            //var appliedSubscriptionItem = GetAppliedSubscriptionItem(programScedule, desiredStartDate);
            var appliedSubscriptionItem = GetAppliedProgramPart(programScedule, desiredStartDate);

            if (appliedSubscriptionItem != null && appliedSubscriptionItem.DueDate == subscriptionItem.DueDate)
            {
                return true;
            }

            return false;
        }

        public decimal CalculateItemAmount(ProgramSubscriptionItem subscriptionItem, OrderItem orderItem)
        {
            decimal result = subscriptionItem.Amount;

            if (IsProrate(subscriptionItem, orderItem))
            {
                result = CalculateSubscriptionProrateAmount(subscriptionItem, orderItem);
            }

            if (orderItem.PaymentPlanType == PaymentPlanType.Installment)
            {
                result = ApplyItemDiscounts(result, orderItem);

                result = ApplyItemCharges(result, orderItem);
            }

            return result;
        }

        public decimal CalculateItemAmount(OrderItem orderItem, decimal amount, ProgramSchedulePart appliedProgramPart = null)
        {
            decimal result = amount;

            if (appliedProgramPart != null)
            {
                if (IsProrateProgramPart(appliedProgramPart, orderItem.ProgramSchedule, orderItem.DesiredStartDate))
                {
                    var selectedDays = orderItem.Attributes.WeekDays;
                    var selectedCharge = GetSelectedCharge(orderItem);
                    result = CalculateBeforeAfterCareProrateAmount(appliedProgramPart, orderItem.DesiredStartDate.Value, selectedCharge, selectedDays);
                }
            }

            if (orderItem.PaymentPlanType == PaymentPlanType.Installment)
            {
                result = ApplyItemDiscounts(result, orderItem);

                result = ApplyItemCharges(result, orderItem);
            }

            return result;
        }

        private decimal CalculateScheduleAmount(ProgramSchedule schedule, Charge charge, decimal amount, DateTime desiredStartDate, List<DayOfWeek> days, ProgramSchedulePart appliedProgramPart = null)
        {
            decimal result = amount;

            if (appliedProgramPart != null)
            {
                if (IsProrateProgramPart(appliedProgramPart, schedule, desiredStartDate))
                {
                    result = CalculateBeforeAfterCareProrateAmount(appliedProgramPart, desiredStartDate, charge, days);
                }
            }

            return result;
        }
        private decimal CalculateItemAmount(ProgramSubscriptionItem subscriptionItem, bool isProrate, DateTime prorateTime, DateTime nowClubDateTime)
        {
            decimal result = subscriptionItem.Amount;

            if (IsProrate(subscriptionItem, isProrate, nowClubDateTime))
            {
                result = CalculateSubscriptionProrateAmount(subscriptionItem, prorateTime);
            }

            return result;
        }

        public int MigrateSessions()
        {

            var listProgramIds = new List<long>() { 56877 };
            var programs = _dataContext.Set<Program>().Where(p => p.TypeCategory == ProgramTypeCategory.Subscription && p.Status != ProgramStatus.Deleted && p.LastCreatedPage >= ProgramPageStep.Step2);
            var staticPrograms = programs.Where(p => listProgramIds.Contains(p.Id));

            foreach (var item in staticPrograms)
            {
                var sessionAddResult = CreateProgram(item);
            }

            var result = _dataContext.SaveChanges();

            return result;
        }

        public OperationStatus CreateProgram(Program program)
        {
            var result = new OperationStatus();

            var res = DeleteProgram(program);

            var programScheduleParts = new List<ProgramSchedulePart>();

            foreach (var programSchedule in program.ProgramSchedules.Where(s => !s.IsDeleted).ToList())
            {
                var startDate = programSchedule.StartDate;
                var endDate = programSchedule.EndDate;

                var subscriptionAttribute = GetSubscriptionAtribute(programSchedule);

                var currentSubscriptionItemAmount = ((ScheduleSubscriptionAttribute)(programSchedule.Attributes)).Subscriptions.Max(s => s.Amount);
                var duedate = ((ScheduleSubscriptionAttribute)(programSchedule.Attributes)).DueDate;

                var subscriptionItems = GenerateSubscriptionSchedules(startDate, endDate, currentSubscriptionItemAmount, duedate);// subscriptionAttribute.SubscriptionItems;
                var days = subscriptionAttribute.Days;

                foreach (var item in subscriptionItems)
                {
                    programSchedule.ProgramSessions = Create(item, days);
                }
            }

            program.ScheduleParts.ToList().AddRange(CreateSchesuleParts(program));

            _dataContext.Set<ProgramSchedulePart>().AddRange(programScheduleParts);

            return result;
        }

        public List<ProgramSchedulePart> CreateSchesuleParts(Program program)
        {
            var result = new List<ProgramSchedulePart>();

            switch (program.TypeCategory)
            {
                case ProgramTypeCategory.Subscription:
                    {
                        foreach (var programSchedule in program.ProgramSchedules.Where(s => !s.IsDeleted))
                        {
                            var startDate = programSchedule.StartDate;
                            var endDate = programSchedule.EndDate;

                            var subscriptionAttribute = GetSubscriptionAtribute(programSchedule);

                            var currentSubscriptionItemAmount = ((ScheduleSubscriptionAttribute)(programSchedule.Attributes)).Subscriptions.Max(s => s.Amount);
                            var duedate = ((ScheduleSubscriptionAttribute)(programSchedule.Attributes)).DueDate;

                            var subscriptionItems = GenerateSubscriptionSchedules(startDate, endDate, currentSubscriptionItemAmount, duedate);// subscriptionAttribute.SubscriptionItems;
                            var days = subscriptionAttribute.Days;

                            if (days != null)
                            {
                                foreach (var item in subscriptionItems)
                                {
                                    var programSchedulePart = new ProgramSchedulePart()
                                    {
                                        StartDate = item.StartDate,
                                        EndDate = item.EndDate,
                                        ProgramId = programSchedule.Program.Id,
                                    };

                                    result.Add(programSchedulePart);
                                }
                            }
                        }
                    }
                    break;
                default:
                    {
                        throw new NotImplementedException();
                    }
            }

            return result;
        }

        public List<ProgramSession> Create(ProgramSubscriptionItem subscriptionItem, List<ProgramScheduleDay> days)
        {
            var result = new List<ProgramSession>();

            var startDate = subscriptionItem.StartDate;
            var endDate = subscriptionItem.EndDate;

            for (DateTime dateTime = startDate; dateTime <= endDate; dateTime = dateTime.AddDays(1))
            {
                if (days.Select(d => d.DayOfWeek).Contains(dateTime.DayOfWeek))
                {
                    var day = days.Single(d => d.DayOfWeek == dateTime.DayOfWeek);

                    var startTime = day.StartTime;
                    if (!startTime.HasValue)
                    {
                        startTime = new TimeSpan(0, 0, 0);
                    }

                    var endTime = day.EndTime;
                    if (!endTime.HasValue)
                    {
                        endTime = new TimeSpan(23, 59, 59);
                    }

                    var programSession = new ProgramSession()
                    {
                        StartDateTime = dateTime.Date.Add(startTime.Value),
                        EndDateTime = dateTime.Date.Add(endTime.Value),
                    };

                    result.Add(programSession);
                }
            }

            return result;
        }
        public OperationStatus DeleteProgram(Program program)
        {
            var result = new OperationStatus();

            var schedules = program.ProgramSchedules.Where(s => !s.IsDeleted);

            //var scheduleIds = schedules.Where(s => !s.IsDeleted).Select(s => s.Id).ToList();

            var orderSessions = schedules.SelectMany(o => o.OrderItems).SelectMany(o => o.OrderSessions);

            var programSessionParts = _dataContext.Set<ProgramSchedulePart>().Where(p => p.ProgramId == program.Id);

            if (_dataContext.Set<OrderSession>().Any() && orderSessions.Any())
            {
                _dataContext.Set<OrderSession>().RemoveRange(orderSessions);
            }

            _dataContext.Set<ProgramSchedulePart>().RemoveRange(programSessionParts);

            return result;
        }

        //public ProgramSubscriptionItem GetMonthDesiredStartDate(OrderItem orderItem, DateTime desiredStartdate)
        //{
        //    var scheduleMonths = orderItem.ProgramSchedule.Attributes as ScheduleSubscriptionAttribute;
        //    var scheduleOldDesiredDate = new ProgramSubscriptionItem();

        //    foreach (var schedule in scheduleMonths.SubscriptionItems)
        //    {
        //        if (desiredStartdate <= schedule.EndDate && desiredStartdate >= schedule.StartDate)
        //        {
        //            scheduleOldDesiredDate = schedule;
        //            break;
        //        }
        //    }

        //    return scheduleOldDesiredDate;
        //}

        public ProgramSchedulePart GetMonthDesiredStartDate(long programId, DateTime desiredStartdate)
        {
            var programParts = _programBusiness.GetScheduleParts(programId);
            var scheduleOldDesiredDate = new ProgramSchedulePart();

            foreach (var schedulePart in programParts)
            {
                if (desiredStartdate <= schedulePart.EndDate && desiredStartdate >= schedulePart.StartDate)
                {
                    scheduleOldDesiredDate = schedulePart;
                    break;
                }
            }

            return scheduleOldDesiredDate;
        }
        public bool IsFull(OrderItem orderItem)
        {
            var result = false;

            // check sessions capacity
            var attributes = GetSubscriptionAtribute(orderItem);
            var programSessionBusiness = _programSessionBusiness;
            var programSessions = orderItem.OrderSessions.Select(s => s.ProgramSession).ToList();
            var capacity = attributes.Capacity;

            foreach (var item in programSessions)
            {
                if (programSessionBusiness.IsFull(item, capacity))
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        private class SubscriptionChargeDiscount
        {
            public SubscriptionChargeDiscount()
            {

            }

            public SubscriptionChargeDiscount(Charge charge, bool applyOnTotalAmount = false)
            {
                var amount = charge.Amount;

                if (charge.HasEarlyBird && charge.Attributes.EarlyBirds != null && charge.Attributes.EarlyBirds.Any())
                {
                    foreach (var earlyBrid in charge.Attributes.EarlyBirds.OrderBy(e => e.ExpireDate))
                    {
                        if (DateTime.UtcNow.Date <= earlyBrid.ExpireDate.Value.Date)
                        {
                            amount = earlyBrid.Price;
                            break;
                        }
                    }
                }

                Amount = charge.Amount;
                AmountType = charge.AmountType;
                Category = charge.Category;
                Subcategory = ChargeDiscountSubcategory.Charge;
                CalculationType = applyOnTotalAmount ? CalculationType.TotalAmount : CalculationType.OnlyTuition;

            }

            public SubscriptionChargeDiscount(Discount discount)
            {
                Amount = discount.Amount;
                AmountType = discount.AmountType;
                Category = discount.Category;
                Subcategory = ChargeDiscountSubcategory.Discount;
            }

            public SubscriptionChargeDiscount(OrderChargeDiscount orderChargeDiscount, int? subscriptionCount = null)
            {
                var amount = Math.Abs(orderChargeDiscount.Amount);

                if (subscriptionCount.HasValue)
                {
                    amount = Convert.ToDecimal(amount / subscriptionCount);
                }

                AmountType = ChargeDiscountType.Fixed;

                Category = orderChargeDiscount.Category;
                if (orderChargeDiscount.ChargeId.HasValue)
                {
                    var charge = orderChargeDiscount.Charge;
                    if (charge != null && charge.HasEarlyBird && charge.Attributes.EarlyBirds != null && charge.Attributes.EarlyBirds.Any())
                    {
                        foreach (var earlyBrid in charge.Attributes.EarlyBirds.OrderBy(e => e.ExpireDate))
                        {
                            if (DateTime.UtcNow.Date <= earlyBrid.ExpireDate.Value.Date)
                            {
                                amount = earlyBrid.Price;
                                break;
                            }
                        }
                    }
                }
                if (orderChargeDiscount.DiscountId.HasValue)
                {
                    var discount = orderChargeDiscount.Discount;
                    amount = discount.Amount;
                    AmountType = discount.AmountType;
                    Category = discount.Category;
                }
                Amount = amount;

                Subcategory = orderChargeDiscount.Subcategory;
            }

            public SubscriptionChargeDiscount(Coupon coupon)
            {
                Amount = coupon.Amount;
                AmountType = coupon.AmounType;
                Category = ChargeDiscountCategory.Coupon;
                Subcategory = ChargeDiscountSubcategory.Discount;
                CalculationType = coupon.CouponCalculateType;
            }

            private decimal _amount;

            public decimal Amount
            {
                get
                {
                    return _amount;
                }
                set
                {
                    _amount = Math.Abs(value);
                }
            }

            public ChargeDiscountType AmountType { get; set; }

            public ChargeDiscountCategory Category { get; set; }

            public ChargeDiscountSubcategory Subcategory { get; set; }

            public CalculationType CalculationType { get; set; }

            public decimal Calculate(decimal amount, decimal mainAmount = 0, bool isAdditionalCharge = false)
            {
                decimal result = 0;

                var amount2 = mainAmount == 0 ? amount : mainAmount;

                if (isAdditionalCharge && CalculationType == CalculationType.OnlyTuition)
                {
                    return result;
                }

                result = AmountType == ChargeDiscountType.Fixed ? Amount : (Amount * amount2 / 100);


                return result;
            }
            public decimal CalculateItemWithCoupon(decimal amount, bool isAdditionalCharge = false)
            {
                decimal result = 0;

                if (isAdditionalCharge && CalculationType == CalculationType.OnlyTuition)
                {
                    return result;
                }

                if (Amount > amount)
                {
                    result = AmountType == ChargeDiscountType.Fixed ? amount : (Amount * amount / 100);
                }
                else
                {
                    result = AmountType == ChargeDiscountType.Fixed ? Amount : (Amount * amount / 100);
                }

                return result;
            }
            public decimal Apply(decimal amount, decimal mainAmount = 0, bool isAdditionalCharge = false)
            {
                var result = amount;

                var chargeDiscountAmount = Calculate(amount, mainAmount, isAdditionalCharge);

                switch (Subcategory)
                {
                    case ChargeDiscountSubcategory.Charge:
                        {
                            result += chargeDiscountAmount;
                        }
                        break;
                    case ChargeDiscountSubcategory.Discount:
                        {
                            if (chargeDiscountAmount > amount)
                            {
                                result -= amount;
                            }
                            else
                            {
                                result -= chargeDiscountAmount;
                            }
                        }
                        break;
                    default:
                        break;
                }

                return result;
            }
        }
    }
}
