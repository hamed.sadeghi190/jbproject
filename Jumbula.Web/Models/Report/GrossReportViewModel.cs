﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using Jumbula.Core.Domain.Generic;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Models
{
    public class GrossReportViewModel
    {
        public List<KeyValue> DayAndAmount { get; set; }

        public SelectList Monthes { get; set; }

        [Required]
        public int selectedMonth { get; set; }

        public GrossReportViewModel(List<KeyValue> data)
        {
            DayAndAmount = data;
            SeedMonths();
        }

        private void SeedMonths()
        {
            Monthes = new SelectList(DateTimeFormatInfo.InvariantInfo.MonthNames);
        }
    }
}