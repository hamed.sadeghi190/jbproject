﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class County : BaseEntity
    {
        public int Id { get; set; }

        [StringLength(64)]
        public string Name { get; set; }

        [ForeignKey("StateId")]
        public virtual State State { get; set; }

        public int StateId { get; set; }

    }
}
