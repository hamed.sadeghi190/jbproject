﻿using Jb.Framework.Common;
using Jumbula.Common.Enums;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IPageBusiness
    {
        JbPage Get(int id);

        JbPage Get(int clubId, SaveType? saveType);

        JbPage Get(string clubDomain, SaveType? saveType);
        IQueryable<JbPage> GetList();
        OperationStatus CreateEdit(JbPage page);
        OperationStatus Delete(int id);
        JbPage GetDefaultPage(Club club);

        OperationStatus DeleteClubPage(int clubId);

        OperationStatus Replicate(int districtId, int? memberId);
    }
}
