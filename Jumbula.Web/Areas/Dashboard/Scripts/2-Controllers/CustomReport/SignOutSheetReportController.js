﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('SignOutSheetReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;


        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        var model = {};
        var dataInfo = {};

        $scope.Model = customReportService.getModelFromStrorage();

        $http.get(baseUrl + '/GetSignOutSheetReportDetail', { params: { programId: $scope.Model._SelectedPrograms } }).success(function (data, status, headers, config) {

            $scope.ProgramName = data.ProgramName;
            $scope.Date = data.Date;
            dataInfo = data;
            manipulateRosterGrid();

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        var model = $scope.Model;


        var manipulateRosterGrid = function () {
            $scope.SignOutSheetReportDataSource = new kendo.data.DataSource({

                transport: {
                    read: {
                        url: baseUrl + "/SignOutSheetReportRun",
                        dataType: "json",
                        type: "POST",
                        data: function () {
                            return {
                                programId: model._SelectedPrograms
                            };
                        },
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                resizable: true,
            });

            $scope.SignOutSheetReportGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.seasonName + "_" + dataInfo.ProgramDomain + " (Sign-outSheet).xlsx",
                    proxyURL: "/Dashboard/CustomReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;
                    rows.push({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.SignOutSheetReportDataSource,
                resizable: true,
                dataBound: function () {
                    var grid = $("#SignOutSheetReportGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },
                sortable: true,
                columns: [

                    {
                        field: "ParticipantFirstName",
                        title: "Participant first name",
                        width: "190px"
                    }, {
                        field: "ParticipantLastName",
                        title: "Last name",
                        width: "120px"
                    }, {
                        field: "ParentFirstName",
                        title: "Parent first name",
                        width: "160px"
                    }, {
                        field: "ParentLastName",
                        title: "Last name",
                        width: "120px"
                    }, {
                        field: "ParentPrimaryPhone",
                        title: "Primary phone",
                        width: "145px"
                    }, {
                        field: "ParentAuthorizedAdult",
                        title: "Authorized adult name",
                        width: "200px"
                    }, {
                        field: "ParentAuthorizedAdultPhone",
                        title: "Phone",
                        width: "90px"
                    }, {
                        field: "PickupSignature",
                        title: "Pick-up signature",
                        width: "150px"
                    }
                ]
            }
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#SignOutSheetReportGrid").getKendoGrid().saveAsExcel();
        }

        $scope.saveToPDF = function () {
            cfpLoadingBar.start();

            $scope.Model.printMode = true;
            $http.post(baseUrl + '/SignOutSheetReportRun', {
                programId: model._SelectedPrograms,
                printMode: $scope.Model.printMode
            }, {
                    headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
                }).success(function (data) {
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    var blob = new Blob([data], { type: "application/pdf" });

                    var fileURL = URL.createObjectURL(blob);
                    a.href = fileURL;
                    a.download ="(Sign-outSheet).pdf";
                    a.click();
                });
            $scope.Model.printMode = false;
        }

        $scope.print = function () {
            cfpLoadingBar.start();
            printGrid();
            cfpLoadingBar.complete();
        }


        function printGrid() {
            var gridElement = $('#SignOutSheetReportGrid'),
                printableContent = '',
                win = window.open('', '', 'width=800, height=500, resizable=1, scrollbars=1'),
                doc = win.document.open();

            var htmlStart =
                '<!DOCTYPE html>' +
                '<html>' +
                '<head>' +
                '<meta charset="utf-8" />' +
                '<title></title>' +
                '<div> <ul style="list-style:none"> ' + dataInfo.Date + ' </li> </ul> </div>' +
                '<link href="http://kendo.cdn.telerik.com/' + kendo.version + '/styles/kendo.common.min.css" rel="stylesheet" /> ' +
                '<style>' +
                'html { font: 11pt sans-serif; text-align: left }' +
                '.k-grid { border-top-width: 0; }' +
                '.k-grid, .k-grid-content { height: auto !important; padding: 20px; text-align: left}' +
                '.k-grid-content { overflow: visible !important; padding: 20px;}' +
                '.k-grid .k-grid-header  th { padding-bottom: 20px }' +
                '.k-grid-toolbar, .k-grid-pager > .k-link { display: none; }' +
                '</style>' +
                '</head>' +
                '<body>';

            var htmlEnd =
                    '</body>' +
                    '</html>';

            var gridHeader = gridElement.children('.k-grid-header');
            if (gridHeader[0]) {
                var thead = gridHeader.find('thead').clone().addClass('k-grid-header');
                printableContent = gridElement
                    .clone()
                        .children('.k-grid-header').remove()
                    .end()
                        .children('.k-grid-content')
                            .find('table')
                                .first()
                                    .children('tbody').before(thead)
                                .end()
                            .end()
                        .end()
                    .end()[0].outerHTML;
            } else {
                printableContent = gridElement.clone()[0].outerHTML;
            }

            doc.write(htmlStart + printableContent + htmlEnd);
            doc.close();
            win.print();
        }

    }
    ]);

})();