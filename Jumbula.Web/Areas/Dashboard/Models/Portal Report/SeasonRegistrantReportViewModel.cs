﻿using System;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SeasonRegistrantReportViewModel
    {
        public string RegistrantName { get; set; }
        public CurrencyCodes Currency { get; set; }
        /// <summary>
        /// ////
        /// </summary>
        public string Confirmation { get; set; }
        public decimal TotalClassFees { get; set; }

        public string TotalClassFeesstr
        {
            get
            {
                return CurrencyHelper.FormatCurrencyWithPenny(TotalClassFees, Currency);
            }
        }
        public decimal CashPayment { get; set; }
        public decimal AmtPTAPTOfundedScholarship { get; set; }

        public decimal totalamount { get; set; }
        public decimal ProvFundedScholarship { get; set; }

        public decimal SubtotalClassFeesPaid
        {
            get
            {
                return TotalClassFees - (Math.Abs(ProvFundedScholarship) + Math.Abs(AmtPTAPTOfundedScholarship) + Math.Abs(CashPayment) + Math.Abs(CustomDiscount));//< 0 ? 0 : TotalClassFees - (Math.Abs(ProvFundedScholarship) + Math.Abs(AmtPTAPTOfundedScholarship) + Math.Abs(CashPayment));
            }
        }
        public decimal feepaid { get; set; }
      
        public decimal PTAPTODonationpaid { get; set; }
        public decimal ActivityFeepaid { get; set; }

        // public string TotalPTAPTOFullstr { get { return Utilities.FormatCurrencyWithPenny(TotalPTAPTOFull, Currency); } }
        public decimal TotalPayment
        {
            get
            {
                return CustomCharge + LateRegistrationfees + ActivityFeepaid + PTAPTODonationpaid + feepaid + SubtotalClassFeesPaid ;
            }
        }
        public decimal LateRegistrationfees { get; set; }
        public decimal CancellationFeePaid { get; set; }
        public decimal? RefundAmount { get; set; }

        public decimal CustomDiscount { get; set; }
        public decimal CustomCharge { get; set; }

        public decimal TotalStripeFees
        {
            get
            {
                return Convert.ToDecimal(TotalPayment * Convert.ToDecimal(0.029));
            }
        }
        public decimal JumbulaFeePaid
        {
            get
            {
                return Convert.ToDecimal(TotalPayment * Convert.ToDecimal(0.0085));
            }
        }     
        
    }

}