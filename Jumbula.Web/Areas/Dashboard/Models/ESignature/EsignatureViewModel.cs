﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class EsignatureViewModel: BaseViewModel<JbESignature>
    {
        [Required(ErrorMessage = "{0} is required.")]
        public string Subject { get; set; }
        public string FileName { get; set; }
        public List<SelectKeyValue<int>> PartnerMembers { get; set; }
        [Display(Name = "Choose member")]
        [Required(ErrorMessage = "{0} is required.")]
        public int SelectedMember { get; set; }
        public int SenderId { get; set; }
        public int ReciverId { get; set; }
        public byte[] File { get; set; }
        public byte SignedFile { get; set; }
        public PageMode PageMode { get; set; }
        public DateTime? DeadLine { get; set; }
        public string Content { get; set; }
    }
    public class ESignatureMessageViewModel
    {
        public string ChatId { get; set; }
        public string Subject { get; set; }
        public string Text { get; set; }
        public int SenderId { get; set; }
        public int ReciverId { get; set; }
        public int reciverClubId { get; set; }
        public int SenderClubId { get; set; }
        public MessageType messageType { get; set; }

    }
}