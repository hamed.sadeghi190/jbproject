﻿using System;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class UserVerification : BaseEntity
    {
        public UserVerification()
        {
            IsVerified = false;
            CreatedDate = DateTime.UtcNow;
        }

        [StringLength(4)]
        public string Code { get; set; }

        public DateTime CreatedDate { get; set; }

        public string PhoneNumber { get; set; }

        public bool IsVerified { get; set; }
        public string Token { get; set; }
        public string ClubDomain { get; set; }
    }
}
