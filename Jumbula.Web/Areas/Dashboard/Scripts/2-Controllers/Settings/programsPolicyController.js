﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('programsPolicyController', ['$scope', '$http', '$state', 'jbToast', function ($scope, $http, $state, jbToast) {
        window.app.globalObjects.pageClass = 'general-settings-view';
        window.app.globalObjects.initializeView();

        $scope.Model = {};
        $http.get('Dashboard/Setting/GetProgramsPolicy').success(function (data, status, headers, config) {
            $scope.Model = data;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.saveSubscriptionPolicy = function () {

            $http.post('Dashboard/Setting/SaveSubscriptionProgramPolicy', { model: $scope.Model })
             .success(function (data, status, headers, config) {

                 if (data.Status) {

                     $scope.errors = null;

                     jbToast.success('Policy updated successfully.');
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }

        $scope.saveGeneralPolicy = function () {

            $http.post('Dashboard/Setting/SaveGeneralProgramsPolicy', { model: $scope.Model })
                .success(function (data, status, headers, config) {

                    if (data.Status) {

                        $scope.errors = null;

                        jbToast.success('Policy updated successfully.');
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                })
        }

        $scope.ShowInfo = function () {

            $(".RefundAutmatically").slideToggle();
        }

    }])
})();