﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.ActivityLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Jumbula.Core.Business;
using Jumbula.Core.Data;

namespace Jumbula.Business
{
    public class AuditBusiness : IAuditBusiness
    {
        #region Fields
        private readonly IAuditRepository _auditRepository;
        #endregion

        #region Constractor
        public AuditBusiness(IAuditRepository auditRepository)
        {
            _auditRepository = auditRepository;
        }
        #endregion

        #region PublicActions
        public void Add(JbAudit audit)
        {
            _auditRepository.Create(audit);
        }

        public JbAudit Get(Guid id)
        {
            return _auditRepository.GetList().SingleOrDefault(a => a.Id == id);
        }

        public JbAudit Get(HttpRequestBase request)
        {
            throw new NotImplementedException();
        }

        public IQueryable<JbAudit> GetList()
        {
            return _auditRepository.GetList();
        }

        public IQueryable<JbAudit> GetList(string zone)
        {
            return _auditRepository.GetList(a => a.Zone.Equals(zone, StringComparison.OrdinalIgnoreCase));
        }

        public IQueryable<JbAudit> GetClubAudits(string clubdomain)
        {
            return _auditRepository.GetList(a => a.ClubDomain.Equals(clubdomain, StringComparison.OrdinalIgnoreCase));
        }

        public IQueryable<JbAudit> GetParentAudits(string clubdomain, int parentId, string activity = null, string userName = null)
        {
            var zone = JbZone.ParentDashboard.ToDescription();

            var ownerKey = parentId.ToString();

            var result = GetClubAudits(clubdomain).Where(a => a.OwnerKey.Equals(ownerKey) && a.Zone.Equals(zone));

            if (!string.IsNullOrEmpty(activity))
            {
                result = result.Where(a => a.Action.Equals(activity));
            }

            if (!string.IsNullOrEmpty(userName))
            {
                result = result.Where(a => a.UserName.Equals(userName));
            }

            return result;
        }

        public ActivityLogFilterViewModel GetFilters()
        {
            var result = new ActivityLogFilterViewModel();

            var activities = Enum.GetValues(typeof(JbAction))
                .Cast<JbAction>()
                .Where(a => JbZoneAttribute.GetZone(a) != null && JbZoneAttribute.GetZone(a) == JbZone.AdminDashboard)
                .Select(d => new SelectListItem { Text = d.ToDescription(), Value = d.ToString() })
                .OrderBy(a => a.Text)
                .ToList();

            activities.AddItemToFirst(DefaultDropdownItem.All);

            result.Activities = activities;

            return result;
        }

        public PaginatedList<ActivityLogItemViewModel> GetAdminAudits(string clubdomain, ActivityLogFilterViewModel filters, List<Sort> sorts, int pageIndex, int pageSize)
        {
            var adminAudits = _auditRepository.GetAdminAudits(clubdomain, GetAdminAuditFilter(filters), sorts, pageIndex, pageSize);

            var result = adminAudits.Select(MapActivityLogItemViewModel).ToPaginatedList(pageIndex, pageSize, adminAudits.TotalCount);

            return result;
        }

        public ActivityLogDetailViewModel GetDetail(Guid id)
        {
            return MapActivityLogDetailViewModel(Get(id));
        }

        public void SetResult(Guid id, int? statusCode = null, string description = null, string ownerKey = null)
        {
            var audit = Get(id);

            audit.StatusCode = statusCode;
            audit.Description = description;
            audit.OwnerKey = ownerKey;

            _auditRepository.Save();
        }

        public void SetSuccess(Guid id, int statusCode, string description = null, string ownerKey = null)
        {
            var audit = Get(id);

            audit.Status = true;
            audit.StatusCode = statusCode;
            audit.Description = description;
            audit.OwnerKey = ownerKey;

            _auditRepository.Save();
        }

        public void SetDescription(Guid id, string description)
        {
            var audit = Get(id);

            audit.Description = description;

            _auditRepository.Save();
        }

        public string GetDevice(HttpRequestBase request)
        {
            var result = request.Browser.Platform;

            if (request.Browser.IsMobileDevice)
            {
                var mobile = "Platform: " + request.Browser.Platform;
                mobile += "Manufacturer: " + request.Browser.MobileDeviceManufacturer;
                mobile += " Model: " + request.Browser.MobileDeviceModel;
                mobile += " Screen: " + request.Browser.ScreenPixelsWidth.ToString() +
                                   " x " + request.Browser.ScreenPixelsHeight.ToString();

                result = mobile;
            }

            return result;
        }

        #endregion

        #region Private methods

        private static ActivityLogItemViewModel MapActivityLogItemViewModel(JbAudit audit)
        {
            return new ActivityLogItemViewModel()
            {
                DateTime = audit.DateTime.ToString(Constants.DefaultDateTimeFormatWithAMPM),
                Action = audit.Action,
                UserName = audit.UserName,
                Status = audit.Status ? Constants.W_OK : Constants.W_Fail,
                Description = audit.Description,
                Id = audit.Id.ToString()
            };
        }

        private static ActivityLogDetailViewModel MapActivityLogDetailViewModel(JbAudit audit)
        {
            return new ActivityLogDetailViewModel()
            {
                DateTime = audit.DateTime.ToString(Constants.DefaultDateTimeFormatWithAMPM),
                UserName = audit.UserName,
                Activity = audit.Action,
                Status = audit.Status.ToString(),
                Description = audit.Description,
                Browser = audit.Browser,
                Device = audit.Device,
                IpAdress = audit.IpAddress,
                StatusCode = audit.StatusCode.ToString()
            };
        }

        private static Expression<Func<JbAudit, bool>> GetAdminAuditFilter(ActivityLogFilterViewModel filters)
        {
            Expression<Func<JbAudit, bool>> result = r => true;

            if (filters == null) return result;

            if (!string.IsNullOrEmpty(filters.UserName))
            {
                result = audit => audit.UserName == filters.UserName;
            }

            if (string.IsNullOrEmpty(filters.Activity)) return result;
            {
                var activity = EnumHelper.ParseEnum<JbAction>(filters.Activity).ToDescription();
                result = audit => audit.Action == activity;
            }

            return result;
        }

        #endregion
    }
}
