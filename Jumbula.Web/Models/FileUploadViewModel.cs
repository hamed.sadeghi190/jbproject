﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class FileUploadViewModel
    {
        public string FileName { get; set; }
        public string Title { get; set; }
        public Files File { get; set; }
        public List<Files> Files { get; set; }

    }
    public class Files
    {
        public HttpPostedFileBase SourceFile { get; set; }
        public HttpPostedFileBase MainFile { get; set; }
    }
}