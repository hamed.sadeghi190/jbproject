﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SessionAttendanceItemViewModel
    {
        public int Id { get; set; }

        public int ProfileId { get; set; }

        public long ScheduleId { get; set; }

        public int SessionId { get; set; }

        public string FullName { get; set; }

        public bool IsAbsent
        {
            get
            {
                return Status == AttendanceStatus.Absent;
            }
        }

        public bool IsLate
        {
            get
            {
                return Status == AttendanceStatus.Late;
            }
        }

        public bool IsPresent
        {
            get
            {
                return Status == AttendanceStatus.Present;
            }
        }

        public AttendanceStatus? Status { get; set; }
    }
}