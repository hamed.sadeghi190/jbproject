﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class DashboardMenuItemViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public MenuLinkType Type { get; set; }

        public int Order { get; set; }

        public string Title { get; set; }

        public string Link { get; set; }

        public string Icon { get; set; }

        public JbAction? Action { get; set; }

        public int? ParentId { get; set; }

        public string OnClick { get; set; }
    }

    public static class DashboardMenuItemsModel
    {
        public static List<DashboardMenuItemViewModel> MenuItems;

        public static void SeedMenuItems()
        {
            MenuItems = new List<DashboardMenuItemViewModel>()
            {
                new DashboardMenuItemViewModel { Id = 1,Name = "Overview", Title = "Dashboard", Link = "Overview", Icon = "icon-dashboard"},
                new DashboardMenuItemViewModel{ Id= 2,Name = "Seasons", Title = "Seasons", Link = "Seasons", Icon="icon-calendar", Action = JbAction.Season_View},
                new DashboardMenuItemViewModel{ Id = 4,Name = "Campaigns", Title = "Campaigns", Link = "Campaigns", Icon = "icon-comments-2", Action = JbAction.Campaign_View},
                new DashboardMenuItemViewModel{ Id = 3,Name = "Clubs", Title = "Members", Link = "Clubs", Icon = "icon-user", Action = JbAction.Member_View},
                new DashboardMenuItemViewModel{ Id = 5, ParentId=4,Name = "EmailCampaigns", Title = "Email Campaigns", Link = "EmailCampaigns", Icon = "icon-user", Action = JbAction.Campaign_Email_View},
                new DashboardMenuItemViewModel{ Id = 6,ParentId=4,Name = "Templates", Title = "Templates", Link = "Templates", Icon = "icon-user", Action = JbAction.Campaign_EmailTemplates_View},
            };
        }
    }

    public enum MenuLinkType
    {
        SubMenu = 0,

        ExternalLink = 1,

        FromDb = 2,

        Action = 3
    }
}