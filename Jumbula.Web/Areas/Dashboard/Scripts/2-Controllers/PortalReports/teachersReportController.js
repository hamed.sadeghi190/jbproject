﻿


(function () {
    'use strict';

    angular.module('dashboardApp').controller('teachersReportController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'county-providers-view';
            window.app.globalObjects.initializeView();

            var baseUrl = window.location.origin + '/Dashboard/PortalReport';

            var timer = false;

            $scope.Model = {};


            $scope.Model.schoolId = [];
            $scope.Model.seasonId = [];
            $scope.Model.teacher = [];

            $http.get(baseUrl + '/GetAllSchool') //GetProvider delete
            .success(function (data, status, headers, config) {

                $scope.AllSchools = data;


            });

            $scope.fillTeacherData = function (seasonId) {
                $http.get(baseUrl + '/GetAllTeacher', { params: { seasonId: seasonId } })
                .success(function (data, status, headers, config) {

                    $scope.AllTeachers = data;
                });
            }

            console.log($scope.AllTeachers)
            $scope.updateChosenScope = function () {
                $("select[chosen]").trigger('chosen:updated');
            };

            $scope.$watch('AllTeachers', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });

            $scope.$watch('AllSeasons', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });

            $scope.fillSeasonData = function (schoolId) {
                $http.get(baseUrl + '/GetClubSeasons', { params: { clubId: schoolId } }).success(function (data, status, headers, config) {  ///GetClubSeasonsProvider delete
                    $scope.AllSeasons = data;

                    $scope.Model.schoolId = schoolId;

                   
              
                }).error(function (data, status, headers, config) {
                    jbToast.error(data.Message);
                });
            }
            var checkValidation = function (schoolId, seasonId) {
                if (is.undefined(schoolId) || schoolId <= 0) {
                    jbToast.error("Please select a school.");
                    return false;
                }
                else if (is.undefined(seasonId) || seasonId <= 0) {
                    jbToast.error("Please select a season.");
                    return false;
                }
                else {
                    return true;
                }
            }

            $scope.runReport = function () {
                if (checkValidation($scope.Model.schoolId, $scope.Model.SeasonId)) {
                    customReportService.saveSchoolSeasonId($scope.Model.schoolId, $scope.Model.SeasonId);
                   
                    $state.go('TeacherParticipantsClassReport', { seasonId: $scope.Model.SeasonId, teacherName: $scope.Model.teacher});

                }
            }

        }
    ]);


})();