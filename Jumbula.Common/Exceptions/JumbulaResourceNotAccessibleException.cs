﻿using Jumbula.Common.Constants;
using System;

namespace Jumbula.Common.Exceptions
{
    public class JumbulaResourceNotAccessibleException : JumbulaException
    {
        public JumbulaResourceNotAccessibleException()
          : base(ExceptionMessage.ResourceNotAccessibleMessage)
        {
        }

        public JumbulaResourceNotAccessibleException(string resourceName)
          : base(string.Format(ExceptionMessage.ResourceNotAccessibleMessageFormat, resourceName))
        {
        }

        public JumbulaResourceNotAccessibleException(Exception e)
           : base(ExceptionMessage.ResourceNotAccessibleMessage, e)
        {
        }
    }
}
