﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ClubWaiversViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Content")]
        [StringLength(100000, ErrorMessage = "{0} must be at last {1} characters.")]
        public string Text { get; set; }
        public int SelectWaiverType { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsRequired { get; set; }
        public WaiverConfirmationType WaiverConfirmationType { get; set; }
        public int Order { get; set; }

    }


}