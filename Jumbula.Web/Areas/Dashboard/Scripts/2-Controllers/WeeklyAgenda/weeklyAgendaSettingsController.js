﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('weeklyAgendaSettingsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService) {
        window.app.globalObjects.pageClass = 'catalog-view';//seasons-view programs
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/WeeklyAgenda", order: 0, menu: "WeeklyAgenda" });
        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        $scope.HoursAfterClassEnds = [];
        for (var i = 1; i < 11; i++) {
            if (i == 1) {
                $scope.HoursAfterClassEnds.push({ Value: i, Text: i + ' hour after' });
            }
            else {
                $scope.HoursAfterClassEnds.push({ Value: i, Text: i + ' hours after' });
            }
        }

        $http.get('/Dashboard/WeeklyAgenda/GetSttings').success(function (data, status, headers, config) {

            $scope.Model = data;
        });

        $scope.save = function () {

            $http.post('/Dashboard/WeeklyAgenda/SaveSttings', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status) {

                        $scope.errors = null;

                        jbToast.success("Settings saved successfully.");
                    } else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        }

    }]);
})();

