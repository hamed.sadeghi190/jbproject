﻿using System.Collections.Generic;

namespace Jumbula.Core.Model.Report
{
    public class ReportExportModel
    {
        public List<List<string>> Rows { get; set; }
        public List<string> Columns { get; set; }
        public string ReportName { get; set; }
        public string Date { get; set; }
        public string ClubName { get; set; }
        public string PdfViewName { get; set; }
    }
}
