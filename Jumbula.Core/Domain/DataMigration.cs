﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumbula.Core.Domain
{
    public class DataMigration
    {
        public int Id { get; set; }

        public DataMigrationName Name { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? RunDate { get; set; }

        public byte[] Source { get; set; }

        public byte[] Result { get; set; }

        public string Data { get; set; }

       
    }

    public enum DataMigrationName : byte
    {
        [Description("Add Apollo members")]
        AddApolloMembers,
        [Description("Add Apollo inmotion members")]
        AddApolloInmotionMembers,
        [Description("Add club users")]
        AddUserClubs,
        [Description("Apollo add order session")]
        ApolloAddOrderSession,
        [Description("Add RAS members")]
        AddRasMembers,
        [Description("Add PS10 orders part1")]
        AddPs10OrdersPart1,
        [Description("Add PS10 orders part2")]
        AddPs10OrdersPart2,
        [Description("Replace relationship in orders with new values")]
        ReplaceRelationshipInOrdersWithNewValues,
        [Description("Add theatero profiles part1")]
        Addtheateroprofilespart1,
        [Description("Add theatero profiles part2")]
        Addtheateroprofilespart2,
    }
}
