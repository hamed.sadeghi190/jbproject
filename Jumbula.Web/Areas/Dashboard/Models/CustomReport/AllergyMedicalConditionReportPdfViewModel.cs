﻿using System.Collections.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class AllergyMedicalConditionReportPdfViewModel
    {
        public string SchoolLogo { get; set; }
        public string ClubLogo { get; set; }
        public List<AllergyMedicalConditionReportViewModel> Programs { get; set; }
    }
}