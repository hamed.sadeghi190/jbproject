﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using SportsClub.Models;
using SportsClub.Models.Register;

namespace SportsClub.Models
{
    public class ProgramBodySchedule
    {
        public long Id { get; set; }
        public ProgramStatus Status { get; set; }

        public ProgramRegStatus RegStatus { get; set; }

        public ProgramRunningStatus RunningStatus { get; set; }

        public ProgramTypeCategory TypeCategory { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Title { get; set; }
        public TimeOfClassFormation ScheduleMode { get; set; }

        public List<ProgramScheduleDay> Days { get; set; }

        public ScheduleAttribute RecurrenceTime { get; set; }
        public string Description { get; set; }
        public int TotalNumberOfRegistrants
        {
            get { return Prices.Sum(c => c.NumberOfRegistration); }
        }
        public int TotalCapacityLeft
        {
            get
            {
                if (RecurrenceTime != null && RecurrenceTime.Capacity > 0)
                {
                    return (RecurrenceTime.Capacity - TotalNumberOfRegistrants) > 0 ? (RecurrenceTime.Capacity - TotalNumberOfRegistrants) : 0;
                }

                return -1;
            }
        }

        public List<string> Times { get; set; }

        public List<string> ComboTime { get; set; }
        public AttendeeRestriction Restrict { get; set; }

        public List<Charge> Services { get; set; }

        public List<ChargeViewModel> ProgramServices { get; set; }
        public List<ChargeViewModel> Prices { get; set; }
        public RegistrationPeriod RegistrationPeriod { get; set; }
        public bool IsCampOvernight { get; set; }
        public List<ProgramScheduleDay> CampOvernightDays { get; set; }

        public bool IsCapacityFull { get; set; }
        public bool EnabledDropIn { get; set; }

        public string AgeRestrictionMessage => Ioc.ProgramBusiness.AgeRestrictionMessage(Restrict.MinAge,
            Restrict.MaxAge, Restrict.ApplyAtProgramStart);

        public string GradeRestrictionMessage => Ioc.ProgramBusiness.GradeRestrictionMessage(
            Restrict.MinGrade, Restrict.MaxGrade,
            Restrict.ApplyAtProgramStart);
    }
}