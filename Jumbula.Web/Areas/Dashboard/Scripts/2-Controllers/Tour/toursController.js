﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('toursController', ['$scope', '$http', '$stateParams', function ($scope, $http, $stateParams) {
        window.app.globalObjects.initializeView();

        window.app.globalObjects.pageClass = 'tours';

        $scope.MC.stateParams = $stateParams;
        $scope.Model = {
            Tours: [
                {
                     YouTubeId: "-Bcnga0qMNI",
                     Title: "How to create a class",
                     Cover: "/Images/Dashboard/Tours/cover-create-a-class.jpg"
                },
                {
                    YouTubeId: "TCSFh4ymbUA",
                    Title: "How to create a custom discount?",
                    Cover: "/Images/Dashboard/Tours/cover-custom-discount.jpg"
                },
               
                {
                    YouTubeId: "B0UffieZ5X4",
                    Title: "How to create a multi-program discount?",
                    Cover: "/Images/Dashboard/Tours/cover-multi-program-discount.jpg"
                },
                {
                    YouTubeId: "cVi7DX2jLN4",
                    Title: "How to create a sibling discount?",
                    Cover: "/Images/Dashboard/Tours/cover-sibling-discount.jpg"
                },
                 {
                     YouTubeId: "2KWFaoFKBFA",
                     Title: "Add a season",
                     Cover: "/Images/Dashboard/Tours/cover-add-season.jpg"
                 },
               {
                   YouTubeId: "ytA8SnHlWaQ",
                   Title: "Add a program",
                   Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
               },
                {
                    YouTubeId: "7QvtcLlZ0Cc",
                    Title: "Class Setup",
                    Cover: "/Images/Dashboard/Tours/cover-class-setup.jpg"
                },
                {
                    YouTubeId: "DVd9-qb01UI",
                    Title: "Camp Setup",
                    Cover: "/Images/Dashboard/Tours/cover-camp -setup.jpg"
                },
                 {
                     YouTubeId: "0iruH3hIKrI",
                     Title: "Program integration: Jumbula home page & website",
                     Cover: "/Images/Dashboard/Tours/cover-home-page.jpg"
                 },
                  {
                      YouTubeId: "OIFkYcKXgzo",
                      Title: "Payment methods & checkout settings",
                      Cover: "/Images/Dashboard/Tours/cover-checkout-settings.jpg"
                  },
                   {
                       YouTubeId: "9yyFQaBw1GQ",
                       Title: "View & test a program",
                       Cover: "/Images/Dashboard/Tours/cover-test-program.jpg"
                   },
                   {
                       YouTubeId: "Yq4kx0SYXTo",
                       Title: "How to set up a payment plan",
                       Cover: "/Images/Dashboard/Tours/cover-payment-plan.jpg"
                   },
                    {
                        YouTubeId: "l_6Oe4WZT38",
                        Title: "Discount setup",
                        Cover: "/Images/Dashboard/Tours/cover-discount-setup.jpg"
                    },
                     {
                         YouTubeId: "fGq5O4HbEnA",
                         Title: "How to set up a coupon",
                         Cover: "/Images/Dashboard/Tours/cover-coupon-setup.jpg"
                     },
                     {
                         YouTubeId: "9G1VoeuBOOQ",
                         Title: "Single & multiple registration",
                         Cover: "/Images/Dashboard/Tours/cover-multiple-registration.jpg"
                     }
            ]
        };
    }]);
})();