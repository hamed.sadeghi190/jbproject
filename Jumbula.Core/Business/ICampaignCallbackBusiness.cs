﻿using Jb.Framework.Common;
using System.Collections.Generic;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface ICampaignCallbackBusiness
    {
        OperationStatus Create(CampaignCallback callback);
        OperationStatus saveDetails(List<CampaignCallbackDetail> callbackDetails);
    }
}
