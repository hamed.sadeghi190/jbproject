ALTER TABLE [Clients] 
ADD [ConvenienceFeeMessage] [nvarchar](max) NULL DEFAULT ('You can avoid paying this extra charge if you pay with cash or check.')