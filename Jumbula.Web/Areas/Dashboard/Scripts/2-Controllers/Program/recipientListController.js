﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('recipientListController', ['$scope', '$http', '$log', '$stateParams', '$state', 'templateEditorService', 'jbToast', 'jbConfirmation', 'emailNotificationService', function ($scope, $http, $log, $stateParams, $state, emailNotificationService, templateEditorService, jbToast, jbConfirmation) {
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        window.app.globalObjects.pageClass = 'campaigns-add ' + $scope.MC.stateParams.campaignType;

        var baseUrl = window.location.origin + "/Dashboard/Campaign";

        // init model.
        $scope.Model = {};
        $scope.Model.MailTemplate = {};
        // use an array for attachments.
        $scope.Model.Attachments = [];

        $scope.showNewRecipientBox = false;
        $scope.addNewRecipient = [];

        $scope.showNewRecipient = function () {
            $scope.showNewRecipientBox = !$scope.showNewRecipientBox;
        }

        // use service.
        $scope.mailDesignerInit = function () {
            return templateEditorService.mailDesignerInit(this);
        }

        $scope.existingImages = templateEditorService.existingImages;

        $scope.setHeaderBackground = function (image) {
            return templateEditorService.setHeaderBackground(this, image);
        }

        $scope.getHeaderBackground = function () {
            return templateEditorService.getHeaderBackground(this);
        }

        $scope.setFooterBackground = function (image) {
            return templateEditorService.setFooterBackground(this, image);
        }

        $scope.getFooterBackground = function () {
            return templateEditorService.getFooterBackground(this);
        }

        $scope.getClubLogo = function () {
            return templateEditorService.getClubLogo(this);
        }

        $scope.enterEditing = function () {
            return templateEditorService.enterEditing(this);
        }

        $scope.saveEditing = function () {
            return templateEditorService.saveEditing(this);
        }

        $scope.cancelEditing = function () {
            return templateEditorService.cancelEditing(this);
        }
   

        $http.get(baseUrl + '/GetNewRecipientModel', { params: { id: $scope.MC.stateParams.templateId, campaignId: $scope.MC.stateParams.campaignId } })
                .success(function (data, stats, headers, config) {
                    $scope.addNewRecipient = data;
                });

        if (($scope.MC.stateParams.templateId || $scope.MC.stateParams.templateId != '' || $scope.MC.stateParams.templateId != 'undefined'))
            $http.get(baseUrl + '/GetTemplate', { params: { id: $scope.MC.stateParams.templateId, campaignId: $scope.MC.stateParams.campaignId } })
                .success(function (data, stats, headers, config) {
                    $scope.Model.MailTemplate = data;
                });

        $scope.gridOptionsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: "/Dashboard/Program/GetAllRecipients?programId=" + $scope.MC.stateParams.programId,
                        cache: false
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: $scope.searchRecipient,
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: data.sort
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverFiltering: true,
            serverSorting: true,
            serverPaging: true,
            pageSize: 10,
        });

        $scope.fillGrid = {
            dataSource: $scope.gridOptionsDataSource,
            sortable: true,
            pageable: {
                refresh: true,
                pageSizes:
                [
                    5, 10, 20, 30, 50, 100
                ],
                buttonCount: 5
            },
            columns: [
                {
                    field: "Email",
                    title: "Email",
                    width: "200px",
                }]
        };

      
        $scope.searchRecipients = function () {
            $scope.fillGrid.dataSource.read();
        }

        $scope.saveTemplate = function () {
            $http.post(baseUrl + '/SaveUpdateTemplate', { Model: $scope.Model.MailTemplate, templateId: $scope.MC.stateParams.templateId })
                .success(function (data, status, headers, config) {
                    $scope.Model.Id = parseInt(data.Data, 0);
                    if (data.Status) {
                        $scope.step = "delivery";
                        switch (data.Data) {
                            case "TemplateList":
                                {
                                    $state.go("EmailCampaignTemplates");
                                    break;
                                }
                        }
                    }
                });
        }

    }]);
})();