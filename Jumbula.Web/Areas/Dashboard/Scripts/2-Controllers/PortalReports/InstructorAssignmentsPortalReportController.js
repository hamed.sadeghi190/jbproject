﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('InstructorAssignmentsPortalReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        var model = {};
        var dataInfo = {};
        $scope.filter = {};


        $scope.Model = customReportService.getModelFromStrorage();


        var providerId = $scope.Model.schoolId;
        var seasonId = $scope.Model.seasonId;
        $scope.filter.printMode = false;

        $http.get(baseUrl + '/GetReportHeader', { params: { clubId: providerId, seasonId: seasonId } }).success(function (data, status, headers, config) {

            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
            $scope.SeasonName = data.SeasonName;
            $scope.ProviderName = data.SelectedClubName;
            dataInfo = data;
            prepareGrid();

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });


        var prepareGrid = function () {
            $scope.InstructorAssignmentsDataSource = new kendo.data.DataSource({

                transport: {
                    read: {
                        url: baseUrl + "/GetInstructorAssignmentsPortalReportInfo",
                        dataType: "json",
                        type: "POST",
                        data: function () {
                            return {
                                providerId: providerId,
                                seasonId: seasonId,
                                printMode: $scope.filter.printMode
                            };
                        },
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                resizable: true,
                batch: true,
            });

            $scope.InstructorAssignmentsGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.ClubName + " (Instructor assignments).xlsx",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.unshift({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.SeasonName + " - " + dataInfo.SelectedClubName
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Instructor assignments - " + dataInfo.ClubName
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.InstructorAssignmentsDataSource,
                resizable: true,
                dataBound: function () {
                    var grid = $("#InstructorAssignmentsGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5,
                },
                sortable: true,
                columns: [

                    {
                        field: "Name",
                        title: "Instructor name",
                        width: "140px",
                        sortable: true
                    }, {
                        field: "Status",
                        title: "Status",
                        width: "70px",
                        sortable: true
                    }, {
                        field: "DateOfBackgroundCheck",
                        title: "BGC date",
                        width: "100px",
                        sortable: true
                    }, {
                        field: "ClassAssignments",
                        title: "Class assignments",
                        width: "190px",
                        sortable: false,
                        template: '<span style="white-space: pre-line" > #=ClassAssignments# </span>'
                    }
                ]
            }
        }


        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $scope.filter.printMode = false;
            $("#InstructorAssignmentsGrid").getKendoGrid().saveAsExcel();
        }

        $scope.saveToPDF = function () {

            $scope.filter.printMode = true;
            $http.post(baseUrl + '/GetInstructorAssignmentsPortalReportInfo', {
                providerId: providerId,
                seasonId: seasonId,
                printMode: $scope.filter.printMode
            }, {
                headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
            }).success(function (data) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                var blob = new Blob([data], { type: "application/pdf" });

                var fileURL = URL.createObjectURL(blob);
                a.href = fileURL;
                a.download = $scope.ClubName + " (Instructor assignments).pdf";
                a.click();
            });
            $scope.filter.printMode = false;

        }

        $scope.print = function () {
            $scope.filter.printMode = true;
            $http.post(baseUrl + '/GetInstructorAssignmentsPortalReportInfo', {
                providerId: providerId,
                seasonId: seasonId,
                printMode: $scope.filter.printMode
            }, {
                headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
            }).success(function (data) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                var blob = new Blob([data], { type: "application/pdf" });

                var fileURL = URL.createObjectURL(blob);
                a.href = fileURL;
                a.download = $scope.ClubName + " (Instructor assignments).pdf";
                a.click();
            });
            $scope.filter.printMode = false;
        }
    }
    ]);

})();