﻿using System.ComponentModel;

namespace Jumbula.Core.Model.Report
{
    public class DailyDismissalReportBuilderViewModel
    {
        [Description("Ignore")]
        public long Id { get; set; }
        public string StudentName { get; set; }
        public string DismissalFromEnrichment { get; set; }
        public string IsPickedUp { get; set; }
        public string PickupTime { get; set; }
        public string PickupName { get; set; }
    }
}
