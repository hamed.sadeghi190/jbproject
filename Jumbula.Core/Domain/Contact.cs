﻿using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jumbula.Core.Domain
{
    public class Contact : BaseEntity
    {
        public DateTime CreatedDate { get; set; }

        public ContactCategory Category { get; set; }

        [StringLength(1024)]
        public string Note { get; set; }

        public int SenderId { get; set; }

        [ForeignKey("SenderId")]
        public virtual JbUser Sender { get; set; }

        public virtual ICollection<CommunicationHistory> Histories { get; set; }
    }
}
