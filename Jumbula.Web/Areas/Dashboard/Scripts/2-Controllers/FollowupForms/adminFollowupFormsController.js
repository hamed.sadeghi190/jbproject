﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('adminFollowupFormsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbToast) {
        window.app.globalObjects.pageClass = 'followup-page';
        window.app.globalObjects.initializeView();
        if (!$scope.MC.account.isPartner) {
            $scope.MC.openBlade({ url: "Dashboard/Blade/Season", order: 1, menu: "SEASONS" });
        }

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + "/FollowupForm";

        $http.get(baseUrl + '/GetFollowupFormByAdmin', { params: { fId: $scope.MC.stateParams.fId, editable: false } }).success(function (data, status, headers, config) {
            $scope.Model = data;
        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.editableModel = function (editable) {
            $http.get(baseUrl + '/GetFollowupFormByAdmin', { params: { fId: $scope.MC.stateParams.fId, editable: editable } }).success(function (data, status, headers, config) {
                $scope.Model = data;
                $scope.Model.Editable = editable;
            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }

        $scope.saveForm = function () {
            if ($("#followupJbForm").valid()) {
                var formData = form2js('followupJbForm', '.', true);
                $http.post(baseUrl + '/EditOrderForm', { formData: formData, fId: $scope.Model.Id, uId: $scope.Model.Guid })
                 .success(function (data, status, headers, config) {
                     if (data.Status) {
                         $http.get(baseUrl + '/GetFollowupFormByAdmin', { params: { fId: $scope.MC.stateParams.fId, editable: false } }).success(function (data, status, headers, config) {
                             $scope.Model = data;
                             $scope.Model.Editable = false;
                             jbToast.success('Form saved successfully.');
                         }).error(function (data, status, headers, config) {
                             jbToast.error("error " + data);
                         });
                     }
                     else {
                         jbToast.error(data.Message);
                     }
                 });
            }
            else {
                jbToast.error("Data invalid or missing,please enter valid form data");
            }
        };


    }]);
})();
