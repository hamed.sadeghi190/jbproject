﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.WebLogic.UserService;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Areas.Dashboard.Models.Overview;
using SportsClub.Areas.Dashboard.Models.Season;
using SportsClub.Models;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{

    public class SeasonController : DashboardBaseController
    {
        #region Fields
        private readonly IProgramSessionBusiness _programSessionBusiness;
        private readonly IProgramBusiness _programBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly ILocationBusiness _locationBusiness;
        private readonly IOrderSessionBusiness _orderSessionBusiness;
        #endregion

        #region Constractors
        public SeasonController(IProgramBusiness programBusiness, IProgramSessionBusiness programSessionBusiness, IOrderItemBusiness orderItemBusiness, ILocationBusiness locationBusiness, IOrderSessionBusiness orderSessionBusiness)
        {
            _programBusiness = programBusiness;
            _programSessionBusiness = programSessionBusiness;
            _orderItemBusiness = orderItemBusiness;
            _locationBusiness = locationBusiness;
            _orderSessionBusiness = orderSessionBusiness;
        }
        #endregion

        public virtual ActionResult SeasonAddEdit()
        {
            return View();
        }

        public virtual ActionResult CopySeasonStep2()
        {
            return View();
        }

        public virtual ActionResult CopySeasonStep3()
        {
            return View();
        }

        public virtual ActionResult SeasonManage()
        {
            return View();
        }

        public virtual ActionResult CapacityFullChart()
        {
            return View();
        }

        public virtual ActionResult SaleOverTimeFullChart()
        {
            return View();
        }

        public virtual ActionResult TotalSaleFullChart()
        {
            return View();
        }
        public virtual ActionResult TotalPaidAmountSeasonChart()
        {
            return View();
        }
        public virtual ActionResult SchoolSettingGeneral()
        {
            return View();
        }

        public virtual ActionResult SchoolSettingSchedule()
        {
            return View();
        }

        public virtual ActionResult SchoolSettingDetail()
        {
            return View();
        }

        public virtual ActionResult SchoolSettingProgramInfo()
        {
            return View();
        }

        public virtual ActionResult SchoolSettingSeasonForms()
        {
            return View();
        }

        public virtual ActionResult SchoolSettingAdditional()
        {
            return View();
        }
        public virtual ActionResult SeasonAssignRoomForPrograms()
        {
            return View();
        }
        public virtual ActionResult InstructorAssignForPrograms()
        {
            return View();
        }

        public virtual ActionResult SchoolSettingPaymentPlan()
        {
            return View();
        }



        [HttpGet]
        [JbAuthorize(JbAction.Season_View)]
        public virtual JsonNetResult Get(string seasonDomain = "")
        {
            if (!string.IsNullOrEmpty(seasonDomain))
            {
                var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);
                var model = season.ToViewModel<SeasonViewModel>();

                return JsonNet(model);
            }
            return JsonNet(new SeasonViewModel());
        }


        [HttpPost]
        [JbAuthorize(JbAction.Season_Add)]
        public virtual ActionResult CreateEdit(SeasonViewModel model)
        {
            var club = base.ActiveClub;
            CheckModelIntegrity(ModelState, model);

            if (ModelState.IsValid)
            {
                Season dbSeason;
                OperationStatus res;

                if (model.Id > 0)
                {
                    dbSeason = Ioc.SeasonBusiness.Get(model.Id);
                    //Add name and year
                    dbSeason.Year = model.Year;
                    dbSeason.Name = model.Name;

                    if (model.Title != dbSeason.Title)
                    {
                        dbSeason.Title = model.Title;
                        dbSeason.Domain = Ioc.SeasonBusiness.GenerateSeasonDomain(model.Title, club.Id);
                    }
                    dbSeason.Description = model.Description;
                    res = Ioc.SeasonBusiness.Update(dbSeason);
                }
                else
                {
                    dbSeason = model.ToModel<Season>();
                    dbSeason.ClubId = club.Id;
                    dbSeason.Domain = Ioc.SeasonBusiness.GenerateSeasonDomain(model.Title, club.Id);
                    dbSeason.Status = model.IsTestMode ? SeasonStatus.Test : SeasonStatus.Live;
                    //Add name and year
                    dbSeason.Year = model.Year;
                    dbSeason.Name = model.Name;

                    res = Ioc.SeasonBusiness.Create(dbSeason);
                }

                return Json(new JResult { Status = res.Status,Data = dbSeason.Title, Message = dbSeason.Domain, RecordsAffected = res.RecordsAffected });

            }

            return JsonFormResponse();
        }

        [JbAuthorize(JbAction.Season_Delete)]
        public virtual ActionResult Delete(string seasonDomain)
        {
            try
            {

                var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);
                const string msg = "This season has some programs and cannot be deleted";

                if (season.Programs != null && season.Programs.Any(p => p.SaveType == SaveType.Publish && p.Status != ProgramStatus.Deleted))
                {
                    return Json(new JResult { Status = false, Message = msg, RecordsAffected = 0 });
                }

                season.Status = SeasonStatus.Deleted;
                var res = Ioc.SeasonBusiness.Update(season);

                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }
            catch
            {

                return JsonFormResponse();
            }
        }

        [JbAuthorize(JbAction.Season_Archive)]
        public virtual ActionResult Archive(string seasonDomain)
        {
            try
            {
                var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);
                season.IsArchived = true;
                var res = Ioc.SeasonBusiness.Update(season);
                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }
            catch
            {

                return JsonFormResponse();
            }
        }

        [JbAuthorize(JbAction.Season_Archive)]
        [HttpGet]
        
        public virtual ActionResult UnArchive(string seasonDomain)
        {
            try
            {
                var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);
                season.IsArchived = false;
                var res = Ioc.SeasonBusiness.Update(season);
                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected }, JsonRequestBehavior.AllowGet);
            }
            catch
            {

                return JsonFormResponse(JsonRequestBehavior.AllowGet);
            }
        }

        [JbAuthorize(JbAction.Season_SwitchMode)]
        public virtual ActionResult SwitchLiveMode(string seasonDomain)
        {
            try
            {
                var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);

                season.Status = SeasonStatus.Live;
                var res = Ioc.SeasonBusiness.Update(season);

                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }
            catch
            {
                return JsonFormResponse();
            }
        }

        [JbAuthorize(JbAction.Season_SwitchMode)]
        public virtual ActionResult SwitchTestMode(string seasonDomain)
        {
            try
            {
                var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);

                season.Status = SeasonStatus.Test;
                var res = Ioc.SeasonBusiness.Update(season);

                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }
            catch (Exception ex)
            {
                return JsonFormResponse();
            }
        }

        public virtual ActionResult Display(string clubDomain, string seasonDomain)
        {
            if (!this.IsUserAdminForClub(clubDomain, null))
            {
                return HttpNotFound();
            }

            Season season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);

            var model = season.ToViewModel<SeasonDisplayViewModel>();

            return View(model);
        }

        public virtual JsonResult GetJsonData(string term)
        {
            var termValue = Request.Params["term"];
            var seasons = Ioc.SeasonBusiness.GetList()
                .Where(e => e.Title.StartsWith(termValue)).Take(10)
                .Select(x => new JbTitleValue<string> { Title = x.Title, Value = x.Domain }).ToList();
            if (!seasons.Any())
                seasons = Ioc.SeasonBusiness.GetList()
                    .Where(e => e.Title.Contains(termValue)).Take(10)
                    .Select(x => new JbTitleValue<string> { Title = x.Title, Value = x.Domain }).ToList();
            return Json(seasons, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonNetResult GetList(string term)
        {
            var termValue = Request.Params["term"];
            List<Season> seasons;

            if (string.IsNullOrEmpty(term))
            {
                List<Season> sortedSeasons = new List<Season>();

                var seasonList = Ioc.SeasonBusiness.GetList().Where(s => s.ClubId == ActiveClub.Id).ToList();
                var sortedSeasonsHasYear = seasonList.Where(s => s.Year.HasValue && s.Year > 0).OrderByDescending(s => s.Year).ThenBy(s => s.Name).ThenByDescending(s => s.MetaData.DateCreated).ToList();
                var sortedSeasonsNotHasYear = seasonList.Where(s => !s.Year.HasValue || s.Year == 0).OrderByDescending(s => s.MetaData.DateCreated).ToList();
                sortedSeasons.AddRange(sortedSeasonsHasYear);
                sortedSeasons.AddRange(sortedSeasonsNotHasYear);

                seasons = sortedSeasons;
            }

            else
                seasons = Ioc.SeasonBusiness.GetList().Where(s => s.ClubId == ActiveClub.Id &&
                    s.Title.StartsWith(termValue)).Take(10).ToList();
            if (!seasons.Any())
                seasons = Ioc.SeasonBusiness.GetList().Where(s => s.ClubId == ActiveClub.Id &&
                     s.Title.Contains(termValue)).Take(10).ToList();

            var data = seasons.Select(s => new
            {
                Id = s.Id,
                ClubDomain = ActiveClub.Domain,
                Domain = s.Domain,
                Title = s.Title,
                Description = s.Description,
                Status = s.Status,
                Archived=s.IsArchived
            });

            return JsonNet(data);
        }

        public virtual ActionResult GetSeasonRecentRegistration(string seasonDomain, ParticipantSearchType? searchType, string firstName = null, string lastName = null)
        {
            var club = ActiveClub;

            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);

            var term = Request.Params["term"];

            var season = Ioc.SeasonBusiness.Get(seasonDomain, club.Id);

            var query = Ioc.OrderItemBusiness.GetOrderItems(clubId: club.Id, term: term, isTestMode: season.Status == SeasonStatus.Test, itemStatus: OrderItemStatusCategories.showAll, instructorId: null, searchType: searchType, firstName: firstName, lastName: lastName);

            int? instructorId = null;

            if (this.GetCurrentUserRole() == RoleCategory.Instructor)
            {
                instructorId = Ioc.ClubBusiness.Get(ActiveClub.Id).ClubStaffs.Single(c => c.JbUserRole.UserId == this.GetCurrentUserId()).Id;

                query = query.Where(o => o.ProgramSchedule.Program.Instructors.Any(c => c.Id == instructorId) && o.ProgramScheduleId.HasValue && o.ProgramScheduleId > 0);
            }

            query = query.Where(item => item.SeasonId == season.Id && (item.ItemStatus == OrderItemStatusCategories.changed || item.ItemStatus == OrderItemStatusCategories.completed) && item.ProgramScheduleId.HasValue && item.ProgramScheduleId > 0);

            var model = query.OrderByDescending(item => item.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList()
                .Select(orderItem => new RecentRegistrationModel
                {
                    AttendeeName = orderItem.FullName,
                    ProgramName = orderItem.ProgramSchedule != null ? orderItem.ProgramSchedule.Program.Name : "Donation",
                    PaidAmount = CurrencyHelper.FormatCurrencyWithPenny(orderItem.TotalAmount, club.Currency),
                    DateTimeLable = DateTimeHelper.DateTimeLable(orderItem.Order.CompleteDate, club.TimeZone),
                    OrderItemId = orderItem.Id,
                    ItemStatusReason = (Int16)orderItem.ItemStatusReason,
                    SeasonDomain = orderItem.Season != null ? orderItem.Season.Domain : "",
                    ConfirmationId = orderItem.Order.ConfirmationId,
                    Schedule = orderItem.ProgramSchedule.Program.TypeCategory == ProgramTypeCategory.ChessTournament ?
                    _programBusiness.GetProgramScheduleTitleAndDate(orderItem, orderItem.OrderItemChess.Schedule) :
                    _programBusiness.GetProgramScheduleTitleAndDate(orderItem),
                    TuitionName = orderItem.EntryFeeName
                });

            var dataSource = model;

            return JsonNet(new { DataSource = dataSource, TotalCount = query.Count() });
        }

        [JbAuthorize(JbAction.TotalSale_View)]
        public virtual ActionResult GetTotalSales(string seasonDomain)
        {
            var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);
            var seasonId = season.Id;
            var seasonName = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id).Title;
            var orderItems = Ioc.OrderItemBusiness.GetOrderItems(clubId: ActiveClub.Id, seasonId: seasonId, isTestMode: season.Status == SeasonStatus.Test).Where(o => o.ItemStatus == OrderItemStatusCategories.completed);

            var reportModel = new TotalinfoModel
            {
                TotalAttendees = orderItems.Any() ? orderItems.Count() : 0,
                TotalSales = orderItems.Any() ? orderItems.Sum(o => o.TotalAmount) : 0,
                SeasonTitel = seasonName
            };

            return Json(reportModel, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult GetSeasonTitle(string seasonDomain)
        {
            var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);

            return Json(new { seasonTitle = season.Title }, JsonRequestBehavior.AllowGet);
        }

        [JbAuthorize(JbAction.CapacityReport_View)]
        public virtual ActionResult GetCapacityBarChartReport(string seasonDomain)
        {
            var club = ActiveClub;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, club.Id);
            var seasonId = season.Id;

            var seasonPrograms =
                Ioc.ProgramBusiness.GetSeasonPrograms(seasonId)
                    .Where(p => p.TypeCategory != ProgramTypeCategory.Calendar).OrderBy(n => n.Name);

            var reportModel = new List<BarChartDataModel>();

            foreach (var program in seasonPrograms.ToList())
            {
                var programSchedules = program.ProgramSchedules.Where(s => !s.IsDeleted).ToList();

                var waitListcount = program.WaitLists.Count(w => w.IsLive == (w.Program.Season.Status == SeasonStatus.Live));
                foreach (var schedule in programSchedules)
                {
                    var scheduleCharges = schedule.Charges.Where(ch => !ch.IsDeleted && ch.Category == ChargeDiscountCategory.EntryFee);
                    //var fillSpot = 0;
                    var minimumCapacity = (program.TypeCategory == ProgramTypeCategory.Camp || program.TypeCategory == ProgramTypeCategory.SeminarTour) ? 0 : schedule.Attributes.MinimumEnrollment;
                    var scheduleCapacity = (schedule.Program.TypeCategory != ProgramTypeCategory.ChessTournament && schedule.Program.TypeCategory != ProgramTypeCategory.Subscription && schedule.Program.TypeCategory != ProgramTypeCategory.BeforeAfterCare) ? ((ScheduleAttribute)schedule.Attributes).Capacity : 0;
                    bool scheduleHasCapacity = scheduleCapacity > 0;
                    bool isScheduleHasOneTuition = schedule.Charges.Count(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee) == 1;
                    var scheduleFilledSpot = Ioc.ProgramBusiness.GetRegisteredCount(schedule, program.Season.Status == SeasonStatus.Test);

                    var scheduleOpenSpot = scheduleCapacity - scheduleFilledSpot;
                    if (minimumCapacity > 0 && minimumCapacity > scheduleFilledSpot)
                    {

                        scheduleOpenSpot = scheduleCapacity - minimumCapacity;
                    }

                    var extraCapacity = (scheduleFilledSpot - schedule.Attributes.Capacity) > 0 ? (scheduleFilledSpot - schedule.Attributes.Capacity) : 0;
                    var newItem = new BarChartDataModel();

                    newItem.WaitList = waitListcount;
                    newItem.ExtraCapacity = scheduleCapacity > 0 ? extraCapacity : 0;
                    newItem.MinimumCapacity = (minimumCapacity - scheduleFilledSpot) > 0 ? (minimumCapacity - scheduleFilledSpot) : 0;
                    newItem.Date = program.MetaData.DateCreated.Date;


                    newItem.ProgramName = string.Empty;//string.Format("{0}", programName);

                    var tuitionName = schedule.Charges.FirstOrDefault() != null ? schedule.Charges.FirstOrDefault().Name : string.Empty;
                    newItem.ProgramFullName = Ioc.ProgramBusiness.GetProgramNameCustomReport(schedule, tuitionName);
                    newItem.FillSpot = scheduleFilledSpot;
                    newItem.OpenSpot = scheduleOpenSpot > 0 ? scheduleOpenSpot : 0;
                    newItem.IsFull = scheduleFilledSpot >= scheduleCapacity;
                    newItem.Capacity = scheduleCapacity;

                    if (!(!isScheduleHasOneTuition && program.TypeCategory == ProgramTypeCategory.Camp))
                    {
                        if (schedule.Program.TypeCategory == ProgramTypeCategory.Class)
                        {
                            newItem.ProgramFullName = schedule.Program.Name;
                        }
                        reportModel.Add(newItem);
                    }

                    if (isScheduleHasOneTuition)
                    {
                        if ((program.TypeCategory == ProgramTypeCategory.Camp))
                        {

                            foreach (var charge in scheduleCharges)
                            {
                                var chargeCapacity = charge.Capacity;
                                var isChargeHasCapacity = charge.Capacity > 0;
                                newItem.Capacity = chargeCapacity;
                                newItem.OpenSpot = (charge.Capacity > 0)
                                ? (charge.Capacity - newItem.FillSpot) >= 0 ? charge.Capacity - newItem.FillSpot : 0
                                : charge.Capacity;
                                if (isChargeHasCapacity)
                                {
                                    newItem.OpenSpot = (charge.Capacity > 0)
                                                ? (charge.Capacity - newItem.FillSpot) >= 0 ? charge.Capacity - newItem.FillSpot : 0
                                                : charge.Capacity;

                                    if (charge.Capacity > 0 && charge.Capacity - scheduleFilledSpot < 0)
                                    {
                                        newItem.PlusMember = scheduleFilledSpot * -1;
                                    }
                                    if (charge.Capacity > 0 && newItem.FillSpot >= charge.Capacity)
                                    {
                                        newItem.IsFull = true;
                                    }

                                }
                            }
                        }

                        if (program.TypeCategory != ProgramTypeCategory.Camp)
                        {
                            var scheduleCharge = schedule.Charges.First(c => !c.IsDeleted);

                            if (scheduleCharge.Capacity > 0)
                            {
                                reportModel.Add(getTuitionModel(scheduleCharge, true));
                            }
                        }

                        continue;
                    }
                    else
                    {
                        foreach (var charge in scheduleCharges)
                        {

                            reportModel.Add(getTuitionModel(charge));

                        }
                    }
                }
            }

            return Json(reportModel, JsonRequestBehavior.AllowGet);
        }

        private BarChartDataModel getTuitionModel(Charge charge, bool showTuitionLobel = false)
        {
            var schedule = charge.ProgramSchedule;
            var program = charge.ProgramSchedule.Program;
            var chargCapacity = charge.Capacity;
            var newChargeItem = new BarChartDataModel();

            var isChargeHasCapacity = charge.Capacity > 0;

            var chargeFilledSpot = Ioc.ProgramBusiness.GetRegisteredCount(charge, charge.ProgramSchedule.Program.Season.Status == SeasonStatus.Test);
            newChargeItem.ProgramFullName = (program.TypeCategory == ProgramTypeCategory.Camp)
                            ? (schedule.Charges.Count(c => !c.IsDeleted) == 1) ? Ioc.ProgramBusiness.GetProgramNameCustomReport(schedule, "") : Ioc.ProgramBusiness.GetProgramNameCustomReport(schedule, charge.Name)
                            : ((schedule.Charges.Count(c => !c.IsDeleted) == 1) && !showTuitionLobel) ? program.Name : Ioc.ProgramBusiness.GetProgramNameCustomReport(schedule, charge.Name);
            newChargeItem.Capacity = chargCapacity;
            newChargeItem.FillSpot = chargeFilledSpot;
            newChargeItem.OpenSpot = (charge.Capacity > 0)
                ? (charge.Capacity - newChargeItem.FillSpot) >= 0 ? charge.Capacity - newChargeItem.FillSpot : 0
                : charge.Capacity;

            if (isChargeHasCapacity)
            {
                newChargeItem.OpenSpot = (charge.Capacity > 0)
                            ? (charge.Capacity - newChargeItem.FillSpot) >= 0 ? charge.Capacity - newChargeItem.FillSpot : 0
                            : charge.Capacity;

                if (charge.Capacity > 0 && charge.Capacity - chargeFilledSpot < 0)
                {
                    newChargeItem.PlusMember = chargeFilledSpot * -1;
                }
                if (charge.Capacity > 0 && newChargeItem.FillSpot >= charge.Capacity)
                {
                    newChargeItem.IsFull = true;
                }

            }

            return newChargeItem;
        }

        [JbAuthorize(JbAction.TotalSale_View)]
        public virtual ActionResult GetSalesPieChartReport(string seasonDomain, bool isPercentage = true)
        {
            var club = ActiveClub;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, club.Id);

            var seasonPrograms = Ioc.ProgramBusiness.GetSeasonPrograms(season.Id).ToList();

            var reportModel = new List<object>();

            foreach (var program in seasonPrograms)
            {
                decimal programSales = 0;
                var programCountSales = 0;
                var programName = program.Name;

                var programOrderItems = Ioc.OrderItemBusiness.GetOrderItems(program.Id, season.Status == SeasonStatus.Test);
                if (programOrderItems != null && programOrderItems.Any())
                {
                    if (isPercentage)
                    {
                        programSales += programOrderItems.Sum(o => o.TotalAmount);
                        reportModel.Add(new { ProgramName = programName, Price = programSales });
                    }
                    else
                    {
                        programCountSales += programOrderItems.Count();
                        reportModel.Add(new { ProgramName = programName, Price = programCountSales });
                    }

                }
            }

            return Json(reportModel, JsonRequestBehavior.AllowGet);
        }

        [JbAuthorize(JbAction.TotalSale_View)]
        public virtual ActionResult GetTotalSalesPieChart(string seasonDomain)
        {
            var club = ActiveClub;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, club.Id);

            var seasonPrograms = Ioc.ProgramBusiness.GetSeasonPrograms(season.Id).ToList();

            decimal total = 0;
            long totalCount = 0;

            foreach (var program in seasonPrograms)
            {
                var programOrderItems = Ioc.OrderItemBusiness.GetOrderItems(program.Id, season.Status == SeasonStatus.Test);
                if (programOrderItems != null && programOrderItems.Any())
                {
                    total += programOrderItems.Sum(o => o.TotalAmount);
                    totalCount += programOrderItems.Count();
                }
            }
            var reportModel = new { TotalSale = CurrencyHelper.FormatCurrencyWithPenny(total, club.Currency), TotalCount = totalCount };
            return Json(reportModel, JsonRequestBehavior.AllowGet);
        }

        [JbAuthorize(JbAction.SaleDistribution_View)]
        public virtual ActionResult GetSaleOverTimeChartReport(string seasonDomain)
        {
            var club = ActiveClub;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, club.Id);

            var seasonPrograms = Ioc.ProgramBusiness.GetSeasonPrograms(season.Id);

            var reportModel = new List<SaleOverTimeModel>();

            var allOrders = new List<OrderItem>();

            foreach (var program in seasonPrograms.ToList())
            {
                var programOrderItems = Ioc.OrderItemBusiness.GetOrderItems(program.Id, season.Status == SeasonStatus.Test);
                if (programOrderItems != null && programOrderItems.Any())
                {
                    allOrders.AddRange(programOrderItems);
                }
            }

            decimal revenueTotal = 0;

            foreach (var group in allOrders.OrderBy(o => o.Order.CompleteDate.Date).GroupBy(o => o.Order.CompleteDate.Date).ToList())
            {
                var grouptotal = @group.Sum(orderItem => orderItem.TotalAmount);
                var total = allOrders.Sum(p => p.TotalAmount);
                revenueTotal = revenueTotal + grouptotal;
                reportModel.Add(new SaleOverTimeModel { Price = grouptotal, Total = CurrencyHelper.FormatCurrencyWithPenny(total, ActiveClub.Currency), Date = DateTimeHelper.ConvertUtcDateTimeToLocal(group.Key.Date, club.TimeZone).Date.ToString(Constants.DefaultDateFormatWith3CharMonth), SortDate = group.Key.Date, Revenue = revenueTotal });
            }

            return Json(reportModel.OrderBy(o => o.SortDate), JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult GetAllSeasonList()
        {
            var seasons = Ioc.SeasonBusiness.GetList(ActiveClub.Id).Select(s => new SelectKeyValue<string>
            {
                Text = s.Title,
                Value = s.Id.ToString()
            });

            var months = new List<SelectKeyValue<string>>();

            for (var i = 0; i < 12; i++)
            {
                months.Add(new SelectKeyValue<string>
                {
                    Text = (i + 1).ToString(),
                    Value = (i + 1).ToString()
                });
            }

            return JsonNet(new { seasons, months });
        }

        public virtual ActionResult CalculateCopyInfo(long seasonId)
        {
            var programNum =
                Ioc.ProgramBusiness.GetList(ActiveClub.Id)
                    .Count(p => p.SeasonId == seasonId && p.LastCreatedPage == ProgramPageStep.Step4);

            var discountNum = Ioc.DiscountBusiness.GetList(seasonId).Count(d => !d.IsDeleted);

            var couponNum = Ioc.CouponBusiness.GetList(seasonId).Count(c => !c.Deleted);

            var paymentPlanNum = Ioc.PaymentPlanBusiness.GetList(seasonId).Count(p => !p.IsDeleted);

            return JsonNet(new { programNum, discountNum, couponNum, paymentPlanNum });
        }

        [HttpPost]
        public virtual ActionResult CopySeasonStep1(SeasonViewModel model)
        {
            if (!CheckSeasonModelStep2(model))
            {
                return JsonFormResponse();
            }

            var season = Ioc.SeasonBusiness.Get(model.SelectedSeason);

            var defaultValue =
                new
                {
                    Title = "",//string.Format("CopySeason_{0}", season.Title),
                    Description = "" //string.Format("CopyDescription_{0}", season.Description)
                };

            return JsonNet(new { Data = defaultValue, Status = true });
        }

        [HttpPost]
        public virtual ActionResult CopySeasonStep2(SeasonViewModel model)
        {
            if (!CheckSeasonModelStep3(model))
            {
                return JsonFormResponse();
            }

            var seasonId = model.SelectedSeason;

            var programNum =
                Ioc.ProgramBusiness.GetList(ActiveClub.Id)
                    .Count(p => p.SeasonId == seasonId && p.LastCreatedPage == ProgramPageStep.Step4);

            var discountNum = Ioc.DiscountBusiness.GetList(seasonId).Count(d => !d.IsDeleted);

            var couponNum = Ioc.CouponBusiness.GetList(seasonId).Count(c => !c.Deleted);

            var paymentPlanNum = Ioc.PaymentPlanBusiness.GetList(seasonId).Count(p => !p.IsDeleted);

            var copyInfo = new { programNum, discountNum, couponNum, paymentPlanNum };



            return JsonNet(new { Data = copyInfo, Status = true });
        }

        public virtual ActionResult CopySeason(SeasonViewModel model)
        {
            // Start backend coding :)

            var club = ActiveClub;
            var season = Ioc.SeasonBusiness.Get(model.SelectedSeason);
            var programs =
                Ioc.ProgramBusiness.GetList(club.Id)
                    .Where(p => p.Status != ProgramStatus.Deleted &&
                                p.LastCreatedPage == ProgramPageStep.Step4 &&
                                p.SeasonId == season.Id).ToList();

            var discounts = Ioc.DiscountBusiness.GetList(season.Id).Where(d => !d.IsDeleted).Select(d => d.Id).ToList();

            var coupons = Ioc.CouponBusiness.GetList(season.Id).Where(c => !c.Deleted).Select(c => c.Id).ToList();

            var paymentPlans = Ioc.PaymentPlanBusiness.GetList(season.Id).Where(p => !p.IsDeleted).Select(p => p.Id).ToList();

            var emailTemplate = Ioc.SeasonBusiness.GetSeasonEmailTemplate(season.Id);

            var newSeason = model.ToModel<Season>();

            newSeason.ClubId = club.Id;
            newSeason.Domain = Ioc.SeasonBusiness.GenerateSeasonDomain(model.Title, club.Id);
            newSeason.Status = season.Status;
            //Add name and year
            newSeason.Name = model.Name;
            newSeason.Year = model.Year;
            Ioc.SeasonBusiness.Create(newSeason);

            foreach (var program in programs)
            {
                var copyProgram = Ioc.ProgramBusiness.CopyInSeason(program, newSeason.Id, model.ForwardDates, model.MonthsNum);
            }

            foreach (var discount in discounts)
            {
                Ioc.DiscountBusiness.CopyInSeason(discount, newSeason.Id, club.Id, newSeason.Domain);
            }

            foreach (var coupon in coupons)
            {
                Ioc.CouponBusiness.CopyInSeason(coupon, newSeason.Id, club.Id, newSeason.Domain, model.ForwardDates, model.MonthsNum);
            }

            foreach (var paymentPlan in paymentPlans)
            {
                Ioc.PaymentPlanBusiness.CopyInSeason(paymentPlan, newSeason.Id, club.Id, newSeason.Domain, model.ForwardDates, model.MonthsNum);
            }

            if (emailTemplate != null && !string.IsNullOrEmpty(emailTemplate.Template))
            {
                var newSeasonEmailTemplate = new EmailTemplate
                {
                    Template = emailTemplate.Template,
                    TemplateName = string.Format("{0}Template", newSeason.Domain),
                    Type = emailTemplate.Type,
                    Club_Id = club.Id
                };
                Ioc.SeasonBusiness.SaveEmailTemplate(newSeasonEmailTemplate, newSeason.Id);
            }

            return JsonNet(new { Status = true, SeasonDomain = newSeason.Domain });
        }

        [HttpGet]
        [JbAuthorize(JbAction.Season_SchoolSetting_GeneralInfo_View)]
        public virtual JsonNetResult EditSchoolSettingsGeneralInfo(string seasonDomain)
        {
            var model = new SchoolSeasonGeneralSettingViewModel();

            var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);

            if (season.Setting != null)
            {
                var generalInfo = ((SeasonSchoolSetting)season.Setting).SchoolSeasonGeneralSetting;

                if (generalInfo != null)
                {
                    model = generalInfo.ToViewModel<SchoolSeasonGeneralSettingViewModel>();
                    model.SelectedSeasonStage = generalInfo.Stage.HasValue ? generalInfo.Stage.Value.ToString() : string.Empty;
                }
            }

            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.Season_SchoolSetting_GeneralInfo_Save)]
        public virtual ActionResult EditSchoolSettingsGeneralInfo(SchoolSeasonGeneralSettingViewModel model, string seasonDomain)
        {

            if (ModelState.IsValid)
            {

                var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);

                if (season.Setting == null)
                {
                    season.Setting = new SeasonSchoolSetting();
                }
                var generalInfo = ((SeasonSchoolSetting)season.Setting).SchoolSeasonGeneralSetting;
                if (generalInfo == null)
                {
                    generalInfo = new SchoolSeasonGeneralSetting();
                }
                generalInfo = model.ToModel<SchoolSeasonGeneralSetting>();
                if (!string.IsNullOrEmpty(model.SelectedSeasonStage))
                {
                    generalInfo.Stage = ((SeasonStage)Enum.Parse(typeof(SeasonStage), model.SelectedSeasonStage));
                }

                ((SeasonSchoolSetting)season.Setting).SchoolSeasonGeneralSetting = generalInfo;

                season.IsSettingChanged = true;
                var res = Ioc.SeasonBusiness.Update(season);
                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return base.JsonFormResponse();
        }

        [HttpGet]
        [JbAuthorize(JbAction.Season_SchoolSetting_DetailInfo_View)]
        public virtual JsonNetResult EditSchoolSettingsDetailInfo(string seasonDomain)
        {
            var model = new SchoolSeasonDetailSettingViewModel();

            var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);

            if (season.Setting != null)
            {
                var detailInfo = ((SeasonSchoolSetting)season.Setting).SchoolSeasonDetailSetting;
                if (detailInfo != null)
                {
                    model = detailInfo.ToViewModel<SchoolSeasonDetailSettingViewModel>();
                    if (detailInfo.ListOfBackpackFlierDates != null && detailInfo.ListOfBackpackFlierDates.Any())
                    {
                        model.BackpackFlierDates = detailInfo.ListOfBackpackFlierDates;
                        model.AllBackpackFlierDates = detailInfo.ListOfBackpackFlierDates.Select(c => new SelectKeyValue<string>() { Text = c, Value = c }).ToList();
                    }
                    if (detailInfo.ListOfEmailBlastDates != null && detailInfo.ListOfEmailBlastDates.Any())
                    {
                        model.EmailBlastDates = detailInfo.ListOfEmailBlastDates;
                        model.AllEmailBlastDates = detailInfo.ListOfEmailBlastDates.Select(c => new SelectKeyValue<string>() { Text = c, Value = c }).ToList();
                    }
                }
            }
            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.Season_SchoolSetting_DetailInfo_Save)]
        public virtual ActionResult EditSchoolSettingsDetailInfo(SchoolSeasonDetailSettingViewModel model, string seasonDomain)
        {

            if (ModelState.IsValid)
            {

                var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);

                if (season.Setting == null)
                {
                    season.Setting = new SeasonSchoolSetting();
                }
                var detailInfo = ((SeasonSchoolSetting)season.Setting).SchoolSeasonDetailSetting;
                if (detailInfo == null)
                {
                    detailInfo = new SchoolSeasonDetailSetting();
                }
                detailInfo = model.ToModel<SchoolSeasonDetailSetting>();
                detailInfo.ListOfEmailBlastDates = (model.EmailBlastDates.Any()) ? model.EmailBlastDates : null;
                detailInfo.ListOfBackpackFlierDates = (model.BackpackFlierDates.Any()) ? model.BackpackFlierDates : null;

                ((SeasonSchoolSetting)season.Setting).SchoolSeasonDetailSetting = detailInfo;

                season.IsSettingChanged = true;
                var res = Ioc.SeasonBusiness.Update(season);
                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return base.JsonFormResponse();
        }

        [HttpGet]
        [JbAuthorize(JbAction.Season_SchoolSetting_ScheduleInfo_View)]
        public virtual JsonNetResult EditSchoolSettingsScheduleInfo(string seasonDomain)
        {
            var model = new SchoolSeasonscheduleSettingViewModel();

            var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);

            if (season.Setting != null)
            {
                var scheduleInfo = ((SeasonSchoolSetting)season.Setting).SchoolSeasonscheduleSetting;
                if (scheduleInfo != null)
                {
                    model = scheduleInfo.ToViewModel<SchoolSeasonscheduleSettingViewModel>();
                    if (scheduleInfo.ListOfMondayDates != null && scheduleInfo.ListOfMondayDates.Any())
                    {
                        model.MondayDates = scheduleInfo.ListOfMondayDates;
                        model.AllMondayDates = scheduleInfo.ListOfMondayDates.Select(c => new SelectKeyValue<string>() { Text = c, Value = c }).ToList();
                    }
                    if (scheduleInfo.ListOfTuesdayDates != null && scheduleInfo.ListOfTuesdayDates.Any())
                    {
                        model.TuesdayDates = scheduleInfo.ListOfTuesdayDates;
                        model.AllTuesdayDates = scheduleInfo.ListOfTuesdayDates.Select(c => new SelectKeyValue<string>() { Text = c, Value = c }).ToList();
                    }
                    if (scheduleInfo.ListOfWednesdayDates != null && scheduleInfo.ListOfWednesdayDates.Any())
                    {
                        model.WednesdayDates = scheduleInfo.ListOfWednesdayDates;
                        model.AllWednesdayDates = scheduleInfo.ListOfWednesdayDates.Select(c => new SelectKeyValue<string>() { Text = c, Value = c }).ToList();
                    }
                    if (scheduleInfo.ListOfThursdayDates != null && scheduleInfo.ListOfThursdayDates.Any())
                    {
                        model.ThursdayDates = scheduleInfo.ListOfThursdayDates;
                        model.AllThursdayDates = scheduleInfo.ListOfThursdayDates.Select(c => new SelectKeyValue<string>() { Text = c, Value = c }).ToList();
                    }
                    if (scheduleInfo.ListOfFridayDates != null && scheduleInfo.ListOfFridayDates.Any())
                    {
                        model.FridayDates = scheduleInfo.ListOfFridayDates;
                        model.AllFridayDates = scheduleInfo.ListOfFridayDates.Select(c => new SelectKeyValue<string>() { Text = c, Value = c }).ToList();
                    }
                    if (scheduleInfo.ListOfNoClassDates != null && scheduleInfo.ListOfNoClassDates.Any())
                    {
                        model.NoClassDates = scheduleInfo.ListOfNoClassDates;
                        model.AllNoClassDates = scheduleInfo.ListOfNoClassDates.Select(c => new SelectKeyValue<string>() { Text = c, Value = c }).ToList();
                    }
                }
            }
            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.Season_SchoolSetting_ScheduleInfo_Save)]
        public virtual ActionResult EditSchoolSettingsScheduleInfo(SchoolSeasonscheduleSettingViewModel model, string seasonDomain)
        {

            if (ModelState.IsValid)
            {

                var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);

                if (season.Setting == null)
                {
                    season.Setting = new SeasonSchoolSetting();
                }
                var scheduleInfo = ((SeasonSchoolSetting)season.Setting).SchoolSeasonscheduleSetting;
                if (scheduleInfo == null)
                {
                    scheduleInfo = new SchoolSeasonscheduleSetting();
                }
                scheduleInfo = model.ToModel<SchoolSeasonscheduleSetting>();
                scheduleInfo.ListOfMondayDates = (model.Monday && model.MondayDates.Any()) ? model.MondayDates : null;
                scheduleInfo.ListOfTuesdayDates = (model.Tuesday && model.TuesdayDates.Any()) ? model.TuesdayDates : null;
                scheduleInfo.ListOfWednesdayDates = (model.Wednesday && model.WednesdayDates.Any()) ? model.WednesdayDates : null;
                scheduleInfo.ListOfThursdayDates = (model.Thursday && model.ThursdayDates.Any()) ? model.ThursdayDates : null;
                scheduleInfo.ListOfFridayDates = (model.Friday && model.FridayDates.Any()) ? model.FridayDates : null;
                scheduleInfo.ListOfNoClassDates = (model.NoClassDates.Any()) ? model.NoClassDates : null;
                ((SeasonSchoolSetting)season.Setting).SchoolSeasonscheduleSetting = scheduleInfo;

                season.IsSettingChanged = true;
                var res = Ioc.SeasonBusiness.Update(season);
                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return base.JsonFormResponse();
        }

        [HttpGet]
        public virtual JsonNetResult GetSchoolSettingProgramInfo(string seasonDomain)
        {
            var model = new SeasonProgramInfoSettingViewModel();

            var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);

            if (season.Setting != null)
            {
                var programInfo = season.Setting.SeasonProgramInfoSetting;
                if (programInfo != null)
                {
                    model = programInfo.ToViewModel<SeasonProgramInfoSettingViewModel>();

                    if (programInfo.EarlyRegistrationOpens.HasValue)
                    {
                        model.EarlyRegistrationOpenDate = programInfo.EarlyRegistrationOpens.Value.Date;
                        model.EarlyRegistrationOpenTime = new DateTime(programInfo.EarlyRegistrationOpens.Value.Ticks);
                    }

                    if (programInfo.EarlyRegistrationCloses.HasValue)
                    {
                        model.EarlyRegistrationCloseDate = programInfo.EarlyRegistrationCloses.Value.Date;
                        model.EarlyRegistrationCloseTime = new DateTime(programInfo.EarlyRegistrationCloses.Value.Ticks);
                    }

                    if (programInfo.GeneralRegistrationOpens.HasValue)
                    {
                        model.GeneralRegistrationOpenDate = programInfo.GeneralRegistrationOpens.Value.Date;
                        model.GeneralRegistrationOpenTime = new DateTime(programInfo.GeneralRegistrationOpens.Value.Ticks);
                    }

                    if (programInfo.GeneralRegistrationCloses.HasValue)
                    {
                        model.GeneralRegistrationCloseDate = programInfo.GeneralRegistrationCloses.Value.Date;
                        model.GeneralRegistrationCloseTime = new DateTime(programInfo.GeneralRegistrationCloses.Value.Ticks);
                    }

                    if (programInfo.LateRegistrationOpens.HasValue)
                    {
                        model.LateRegistrationOpenDate = programInfo.LateRegistrationOpens.Value.Date;
                        model.LateRegistrationOpenTime = new DateTime(programInfo.LateRegistrationOpens.Value.Ticks);
                    }

                    if (programInfo.LateRegistrationCloses.HasValue)
                    {
                        model.LateRegistrationCloseDate = programInfo.LateRegistrationCloses.Value.Date;
                        model.LateRegistrationCloseTime = new DateTime(programInfo.LateRegistrationCloses.Value.Ticks);
                    }

                    if (programInfo.PreRegistrationOpens.HasValue)
                    {
                        model.PreRegistrationOpenDate = programInfo.PreRegistrationOpens.Value.Date;
                        model.PreRegistrationOpenTime = new DateTime(programInfo.PreRegistrationOpens.Value.Ticks);
                    }

                    if (programInfo.PreRegistrationCloses.HasValue)
                    {
                        model.PreRegistrationCloseDate = programInfo.PreRegistrationCloses.Value.Date;
                        model.PreRegistrationCloseTime = new DateTime(programInfo.PreRegistrationCloses.Value.Ticks);
                    }

                    if (programInfo.LotteryOpens.HasValue)
                    {
                        model.LotteryOpenDate = programInfo.LotteryOpens.Value.Date;
                        model.LotteryOpenTime = new DateTime(programInfo.LotteryOpens.Value.Ticks);
                    }

                    if (programInfo.LotteryCloses.HasValue)
                    {
                        model.LotteryCloseDate = programInfo.LotteryCloses.Value.Date;
                        model.LotteryCloseTime = new DateTime(programInfo.LotteryCloses.Value.Ticks);
                    }

                    if (programInfo.MiscellaneousInformationRostersDue.HasValue)
                    {
                        model.MiscellaneousInformationRostersDueDate = programInfo.MiscellaneousInformationRostersDue.Value.Date;
                    }

                    if (programInfo.MiscellaneousInformationPaymentsIssued.HasValue)
                    {
                        model.MiscellaneousInformationPaymentsIssuedDate = programInfo.MiscellaneousInformationPaymentsIssued.Value.Date;
                    }

                    model.LateRegistrationFee = programInfo.LateRegistrationFee;
                    model.LateRegistrationFeeMessage = programInfo.LateRegistrationFeeMessage;
                    model.EnableLateRegistrationFee = model.LateRegistrationFee > 0;
                    if (string.IsNullOrEmpty(model.EarlyRegisterPINMessage))
                    {
                        model.EarlyRegisterPINMessage = Constants.DefaultEarlyRestrictionPinMessage;
                    }

                    if (string.IsNullOrEmpty(model.GeneralRegisterPINMessage))
                    {
                        model.GeneralRegisterPINMessage = Constants.DefaultRestrictionPinMessage;
                    }

                    if (string.IsNullOrEmpty(model.LateRegisterPINMessage))
                    {
                        model.LateRegisterPINMessage = Constants.DefaultRestrictionPinMessage;
                    }
                }
            }

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult SaveSchoolSettingProgramInfo(SeasonProgramInfoSettingViewModel model, string seasonDomain)
        {
            CheckModelIntegrity(ModelState, model);

            if (ModelState.IsValid)
            {
                var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);

                if (season.Setting == null)
                {
                    season.Setting = new SeasonSchoolSetting();
                }

                var programInfo = season.Setting.SeasonProgramInfoSetting;
                if (programInfo == null)
                {
                    programInfo = new SeasonProgramInfoSetting();
                }

                programInfo = model.ToModel<SeasonProgramInfoSetting>();

                if (model.EarlyRegistrationOpenDate.HasValue)
                {
                    programInfo.EarlyRegistrationOpens = model.EarlyRegistrationOpenDate.Value.Add(model.EarlyRegistrationOpenTime.HasValue ? model.EarlyRegistrationOpenTime.Value.TimeOfDay : new TimeSpan(0));
                }

                if (model.EarlyRegistrationCloseDate.HasValue)
                {
                    programInfo.EarlyRegistrationCloses = model.EarlyRegistrationCloseDate.Value.Add(model.EarlyRegistrationCloseTime.HasValue ? model.EarlyRegistrationCloseTime.Value.TimeOfDay : new TimeSpan(0));
                }

                if (model.GeneralRegistrationOpenDate.HasValue)
                {
                    programInfo.GeneralRegistrationOpens = model.GeneralRegistrationOpenDate.Value.Add(model.GeneralRegistrationOpenTime.HasValue ? model.GeneralRegistrationOpenTime.Value.TimeOfDay : new TimeSpan(0));
                }

                if (model.GeneralRegistrationCloseDate.HasValue)
                {
                    programInfo.GeneralRegistrationCloses = model.GeneralRegistrationCloseDate.Value.Add(model.GeneralRegistrationCloseTime.HasValue ? model.GeneralRegistrationCloseTime.Value.TimeOfDay : new TimeSpan(0));
                }

                if (model.LateRegistrationOpenDate.HasValue)
                {
                    programInfo.LateRegistrationOpens = model.LateRegistrationOpenDate.Value.Add(model.LateRegistrationOpenTime.HasValue ? model.LateRegistrationOpenTime.Value.TimeOfDay : new TimeSpan(0));
                }

                if (model.LateRegistrationCloseDate.HasValue)
                {
                    programInfo.LateRegistrationCloses = model.LateRegistrationCloseDate.Value.Add(model.LateRegistrationCloseTime.HasValue ? model.LateRegistrationCloseTime.Value.TimeOfDay : new TimeSpan(0));
                }

                if (model.PreRegistrationOpenDate.HasValue)
                {
                    programInfo.PreRegistrationOpens = model.PreRegistrationOpenDate.Value.Add(model.PreRegistrationOpenTime.HasValue ? model.PreRegistrationOpenTime.Value.TimeOfDay : new TimeSpan(0));
                }

                if (model.PreRegistrationCloseDate.HasValue)
                {
                    programInfo.PreRegistrationCloses = model.PreRegistrationCloseDate.Value.Add(model.PreRegistrationCloseTime.HasValue ? model.PreRegistrationCloseTime.Value.TimeOfDay : new TimeSpan(0));
                }

                if (model.LotteryOpenDate.HasValue)
                {
                    programInfo.LotteryOpens = model.LotteryOpenDate.Value.Add(model.LotteryOpenTime.HasValue ? model.LotteryOpenTime.Value.TimeOfDay : new TimeSpan(0));
                }

                if (model.LotteryCloseDate.HasValue)
                {
                    programInfo.LotteryCloses = model.LotteryCloseDate.Value.Add(model.LotteryCloseTime.HasValue ? model.LotteryCloseTime.Value.TimeOfDay : new TimeSpan(0));
                }

                if (model.MiscellaneousInformationRostersDueDate.HasValue)
                {
                    programInfo.MiscellaneousInformationRostersDue = model.MiscellaneousInformationRostersDueDate.Value;
                }

                if (model.MiscellaneousInformationPaymentsIssuedDate.HasValue)
                {
                    programInfo.MiscellaneousInformationPaymentsIssued = model.MiscellaneousInformationPaymentsIssuedDate.Value;
                }

                programInfo.LateRegistrationFee = model.EnableLateRegistrationFee ? model.LateRegistrationFee : 0;
                programInfo.LateRegistrationFeeMessage = model.EnableLateRegistrationFee ? model.LateRegistrationFeeMessage : string.Empty;
                season.Setting.SeasonProgramInfoSetting = programInfo;

                season.IsSettingChanged = true;
                var res = Ioc.SeasonBusiness.Update(season);
                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return base.JsonFormResponse();
        }

        [HttpGet]
        public virtual JsonNetResult GetSeasonSchoolSettingAdditional(string seasonDomain)
        {
            var model = new SchoolSettingSeasonAdditionalViewModel();

            var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);

            if (season.Setting != null)
            {
                var additionalOptions = season.Setting.SeasonAdditionalSetting;
                if (additionalOptions != null)
                {
                    model = additionalOptions.ToViewModel<SchoolSettingSeasonAdditionalViewModel>();
                }
            }

            if (string.IsNullOrEmpty(model.WaitlistPolicy))
            {
                model.WaitlistPolicy = Constants.DefaultWaitlistPolicy;
            }

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult SaveSeasonSchoolSettingAdditional(SchoolSettingSeasonAdditionalViewModel model, string seasonDomain)
        {
            if (ModelState.IsValid)
            {
                var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);

                if (season.Setting == null)
                {
                    season.Setting = new SeasonSchoolSetting();
                }

                var addtionalOptions = season.Setting.SeasonAdditionalSetting;
                if (addtionalOptions == null)
                {
                    addtionalOptions = new SeasonAdditionalSetting();
                }

                addtionalOptions = model.ToModel<SeasonAdditionalSetting>();

                season.Setting.SeasonAdditionalSetting = addtionalOptions;

                season.IsSettingChanged = true;
                var res = Ioc.SeasonBusiness.Update(season);
                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }


            return base.JsonFormResponse();
        }

        [HttpGet]
        public virtual JsonNetResult GetSchoolSettingForms(string seasonDomain)
        {
            var model = new SchoolSettingSeasonFormsViewModel();

            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

            var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);

            if (season.Setting == null)
            {
                season.Setting = new SeasonSettings();
            }

            if (season.Setting.SeasonFormsSetting == null)
            {
                season.Setting.SeasonFormsSetting = new SeasonFormsSetting();
            }

            if (!season.Setting.SeasonFormsSetting.Form.HasValue)
            {
                season.Setting.SeasonFormsSetting.Form = Ioc.SeasonBusiness.GetSeasonRegistrationForm(season).Id;
            }

            var seasonFromSettings = season.Setting.SeasonFormsSetting;

            model.SelectedForm = seasonFromSettings.Form.ToString();

            if (seasonFromSettings.Waivers != null)
            {
                model.SelectedWaivers = seasonFromSettings.Waivers.Select(w => w.ToString()).ToList();
            }

            model.Forms = club.ClubFormTemplates.Where(f => !f.IsDeleted && f.FormType == FormType.Registration).Select(f =>
              new SelectListItem
              {
                  Text = f.Title,
                  Value = f.Id.ToString()
              })
              .ToList();

            model.Waivers = club.ClubWaivers.Where(w => !w.IsDeleted).Select(f =>
                new SelectListItem
                {
                    Text = f.Name,
                    Value = f.Id.ToString()
                })
                 .ToList();

            if (!model.Forms.Any(f => f.Value == model.SelectedForm))
            {
                if (!model.Forms.Any(f => f.Text.Equals(Constants.W_Default, StringComparison.OrdinalIgnoreCase)))
                {
                    Ioc.ClubBusiness.AddDefaultTemplates(season.ClubId);

                    model.Forms = club.ClubFormTemplates.Where(f => !f.IsDeleted && f.FormType == FormType.Registration).Select(f =>
                      new SelectListItem
                      {
                          Text = f.Title,
                          Value = f.Id.ToString()
                      })
                      .ToList();
                }

                model.SelectedForm = model.Forms.SingleOrDefault(f => f.Text.Equals(Constants.W_Default, StringComparison.OrdinalIgnoreCase)).Value;
            }

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult SaveSchoolSettingForms(SchoolSettingSeasonFormsViewModel model, string seasonDomain)
        {
            if (ModelState.IsValid)
            {
                var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);

                if (season.Setting == null)
                {
                    season.Setting = new SeasonSchoolSetting();
                }

                var seasonForms = season.Setting.SeasonFormsSetting;
                if (seasonForms == null)
                {
                    seasonForms = new SeasonFormsSetting();
                }

                seasonForms.Form = int.Parse(model.SelectedForm);

                if (model.SelectedWaivers != null)
                {
                    seasonForms.Waivers = model.SelectedWaivers.Select(w => int.Parse(w)).ToList();
                }
                else
                {
                    seasonForms.Waivers = null;
                }

                season.Setting.SeasonFormsSetting = seasonForms;

                season.IsSettingChanged = true;
                var res = Ioc.SeasonBusiness.Update(season);
                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return base.JsonFormResponse();
        }

        [JbAuthorize(JbAction.Season_View)]
        public virtual ActionResult GetClubSeasons()
        {
            var seasons = new object();
            //seasons = Ioc.SeasonBusiness.GetList(ActiveClub.Id).ToList().Select(s =>
            //    new
            //        KeyValuePair<long, string>(s.Id, s.Title)).
            //        ToList();

            seasons = Ioc.SeasonBusiness.GetList(ActiveClub.Id).ToList().Select(s =>
                new
                {
                    Key = s.Id,
                    Value = s.Title,
                    Type = s.Programs.Any(p => p.TypeCategory == ProgramTypeCategory.BeforeAfterCare && p.Status != ProgramStatus.Deleted) ? "BeforeAfterCare" : null
                }).ToList();

            return JsonNet(seasons);
        }

        [JbAuthorize(JbAction.Season_View)]
        public virtual ActionResult GetClubSeasonsDomain()
        {
            var seasons = Ioc.SeasonBusiness.GetList(ActiveClub.Id).ToList().Select(s =>
                new
                    KeyValuePair<long, string>(s.Id, s.Domain)).
                    ToList();

            return JsonNet(seasons);
        }

        public virtual ActionResult GetLiveSeasonsClub()
        {
            var seasons = Ioc.SeasonBusiness.GetLiveSeasonsClub(ActiveClub.Id);

            return JsonNet(seasons);
        }

        [HttpGet]
        public virtual JsonNetResult GetAllPrograms(string seasonDomain)
        {
            var model = new List<SeasonProgramsViewModel>();
            model = Ioc.ProgramBusiness.GetList(ActiveClub.Id, seasonDomain).ToList().Select(p => new SeasonProgramsViewModel
            {
                programName = p.Name,
                programId = p.Id,
                Domain = p.Domain,
                Room = p.Room
            }).ToList();

            return JsonNet(model);
        }
        [HttpPost]
        public virtual ActionResult SaveAssignRoom(List<SeasonProgramsViewModel> model)
        {
            var programRoom = new Dictionary<long, string>();
            foreach (var item in model)
            {
                programRoom.Add(item.programId, item.Room);
            }

            var result = Ioc.ProgramBusiness.AssignRooms(programRoom);

            return Json(new JResult { Status = result.Status, Message = result.Message });
        }

        [HttpGet]
        public virtual JsonNetResult GetAllProgramsInstructors(string seasonDomain)
        {
            var model = new SeasonProgramsInstructorsViewModel();

            model.AllInstructors = Ioc.ClubBusiness.GetClubInstructors(ActiveClub.Id).Where(i => !i.IsFrozen && !i.IsDeleted).Select(l =>
                         new SelectKeyValue<int>
                         {
                             Text = l.Contact.FullName,
                             Value = l.Id
                         })
                         .OrderBy(i => i.Text)
                         .ToList();

            model.ProgramInstructors = Ioc.ProgramBusiness.GetList(ActiveClub.Id, seasonDomain).ToList().Select(p => new SeasonProgramsInstructorsItemViewModel
            {
                ProgramId = p.Id,
                ProgramName = p.Name,
                SchoolName = p.OutSourceSeason != null ? p.Club.Name : string.Empty,
                SelectedInstructors = p.Instructors.Select(i => i.Id).ToList(),
            })
            .OrderBy(p => p.ProgramName)
            .ToList();

            return JsonNet(new { Data = model });
        }

        [HttpPost]
        public virtual ActionResult SaveAssignInstructorsPrograms(SeasonProgramsInstructorsViewModel model)
        {
            var programIds = model.ProgramInstructors.Select(p => p.ProgramId).ToList();

            var selectedInstructors = model.ProgramInstructors.Select(p => p.SelectedInstructors).ToList();

            var programInstructors = new Dictionary<long, List<int>>();

            for (int i = 0; i < programIds.Count(); i++)
            {
                programInstructors.Add(programIds.ElementAt(i), selectedInstructors.ElementAt(i));
            }

            var result = Ioc.ProgramBusiness.AssignInstructorsPrograms(programInstructors, ActiveClub.Id);

            return Json(new JResult { Status = result.Status, Message = result.Message });
        }

        public virtual ActionResult GetTotalPaidMonthlyBarChartReport(string seasonDomain)
        {
            var club = ActiveClub;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, club.Id);
            var seasonId = season.Id;

            var seasonPrograms =
                Ioc.ProgramBusiness.GetSeasonPrograms(seasonId)
                    .Where(p => p.TypeCategory != ProgramTypeCategory.Calendar).OrderBy(n => n.Name);

            var model = GetTotalPaidAmountSubscriptionClass(seasonPrograms.FirstOrDefault().Id);

            var result = model.GroupBy(m => m.DueDate.Month).Select(s => new TotalPaidAmountBarChartViewModel
            {
                DueDate = s.FirstOrDefault().DueDate,
                Amount = s.Sum(m => m.Amount),
                strDueDate = s.FirstOrDefault().DueDate.ToString("MMM")
            });

            var totalSale = CurrencyHelper.FormatCurrencyWithPenny(result.Sum(r => r.Amount), ActiveClub.Currency);

            return Json(new { DataSource = result, Total = totalSale }, JsonRequestBehavior.AllowGet);
        }

        private List<TotalPaidAmountBarChartViewModel> GetTotalPaidAmountSubscriptionClass(long programId)
        {
            var orderItems = Ioc.OrderItemBusiness.GetProgramOrderItems(programId);

            var model = new List<TotalPaidAmountBarChartViewModel>();

            foreach (var orderItem in orderItems)
            {
                switch (orderItem.Mode)
                {
                    case OrderItemMode.Noraml:
                        {
                            switch (orderItem.PaymentPlanType)
                            {
                                case PaymentPlanType.FullPaid:
                                    {
                                        model.Add(new TotalPaidAmountBarChartViewModel
                                        {
                                            DueDate = orderItem.Order.CompleteDate,
                                            Amount = orderItem.PaidAmount
                                        });
                                    }
                                    break;
                                case PaymentPlanType.Installment:
                                    {
                                        var installments = orderItem.Installments.Where(i => i.OrderItem.ProgramScheduleId.HasValue);

                                        model.AddRange(installments.Where(o => o.PaidAmount.HasValue && o.PaidAmount > 0)
                                            .Select(i => new TotalPaidAmountBarChartViewModel
                                            {
                                                DueDate = i.PaidDate.Value,
                                                Amount = i.PaidAmount.Value
                                            }));
                                    }
                                    break;
                            }
                        }
                        break;
                    case OrderItemMode.DropIn:
                        {
                            //model.Add(new TotalPaidAmountBarChartViewModel
                            //{
                            //    DueDate = orderItem.DateCreated,
                            //    Amount = orderItem.PaidAmount
                            //});
                        }
                        break;
                    case OrderItemMode.PunchCard:
                        {
                            model.Add(new TotalPaidAmountBarChartViewModel
                            {
                                DueDate = orderItem.Order.CompleteDate,
                                Amount = orderItem.PaidAmount
                            });
                        }
                        break;
                }
            }

            return model;
        }

        [HttpGet]
        public virtual JsonNetResult GetSeasonPaymentPlan(string seasonDomain)
        {
            var model = new SeasonPaymentPlanViewModel();

            var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);

            model.SeasonId = season.Id;
            model.SelectedPaymentPlans = new List<string>();

            if (season.SeasonPaymentPlans.Any())
            {
                foreach (var paymentplan in season.SeasonPaymentPlans.Where(p => !p.IsDeleted))
                {
                    model.SelectedPaymentPlans.Add(paymentplan.PaymentPlanId.ToString());
                }
            }

            var paymentPlans = Ioc.PaymentPlanBusiness.GetList(season.Id).Where(s => s.IsAllProgram);

            model.PaymentPlanList = new List<SelectKeyValue<string>>();

            foreach (var plan in paymentPlans)
            {
                model.PaymentPlanList.Add(new SelectKeyValue<string>
                {
                    Text = plan.Name,
                    Value = plan.Id.ToString()
                });
            }

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult SaveSeasonPaymentPlan(SeasonPaymentPlanViewModel model)
        {
            var paymentPlanIds = new List<long>();
            var season = Ioc.SeasonBusiness.Get(model.SeasonId);
            if (model.SelectedPaymentPlans != null)
            {
                foreach (var id in model.SelectedPaymentPlans)
                {
                    paymentPlanIds.Add(long.Parse(id));
                }
            }

            var result = Ioc.SeasonPaymentPlanBusiness.Create(season, paymentPlanIds);

            return Json(new JResult { Status = result.Status, Message = result.Message });
        }

        [HttpPost]
        public ActionResult GetSeasonProgramCalendarItems(List<long> seasonIds, DateTime? startDate, DateTime? endDate)
        {
            if (!startDate.HasValue && !endDate.HasValue) return JsonNet(new List<Core.Model.Program.ProgramCalendarViewModel>());

            var result = _programBusiness.GetProgramCalendarItems(seasonIds, ActiveClub.Id, $"{Request?.Url?.Scheme}://{Request?.Url?.Host}", startDate.Value, endDate.Value);

            return JsonNet(result);
        }


        private bool CheckSeasonModelStep2(SeasonViewModel model)
        {
            // check model
            bool result = true;

            if (model.SelectedSeason == 0)
            {
                ModelState.AddModelError("Model.SelectedSeason", "Please select a season for copy.");
                result = false;
            }

            if (model.ForwardDates && !model.MonthsNum.HasValue)
            {
                ModelState.AddModelError("Model.MonthsNum", "Please select months num to push dates.");
                result = false;
            }

            return result;
        }

        private bool CheckSeasonModelStep3(SeasonViewModel model)
        {
            var result = true;

            if (String.IsNullOrEmpty(model.Title))
            {
                ModelState.AddModelError("Model.Title", "Please enter a name for new season.");
                result = false;
            }

            return result;
        }

        private void CheckModelIntegrity(ModelStateDictionary modelState, SeasonViewModel model)
        {

            if (string.IsNullOrEmpty(model.Title))
            {
                modelState.AddModelError("model.Title", "Name is required");
            }
            else if (Ioc.SeasonBusiness.DoesSeasonTitleExist(model.Title, ActiveClub.Id, model.Id))
            {

                modelState.AddModelError("model.Title", "A season with the same name exists");
            }
        }

        private void CheckModelIntegrity(ModelStateDictionary modelState, SeasonProgramInfoSettingViewModel model)
        {
            var hasGeneralRegistration = model.GeneralRegistrationOpenDate.HasValue || model.GeneralRegistrationCloseDate.HasValue;

            if (!model.EnableLateRegistrationFee)
            {
                ModelState.Remove("model.LateRegistrationFee");
            }
            else if (model.LateRegistrationFee <= 0)
            {
                ModelState.AddModelError("model.LateRegistrationFee", "Late registration fee is required");
            }

            if (model.StartDate.HasValue && model.EndDate.HasValue)
            {
                if (model.StartDate.Value > model.EndDate.Value)
                {
                    ModelState.AddModelError("StartDate", "Start date must be before end date.");
                }
            }

            if (hasGeneralRegistration)
            {
                if (!model.GeneralRegistrationCloseDate.HasValue)
                {
                    ModelState.AddModelError("GeneralRegistrationCloseDate", "General registration close date is required.");
                }

                if (!model.GeneralRegistrationOpenDate.HasValue)
                {
                    ModelState.AddModelError("GeneralRegistrationCloseDate", "General registration close date is required.");
                }

                if (!model.GeneralRegistrationOpenTime.HasValue)
                {
                    ModelState.AddModelError("GeneralRegistrationOpenTime", "General registration open time is required.");
                }

                if (!model.GeneralRegistrationCloseTime.HasValue)
                {
                    ModelState.AddModelError("GeneralRegistrationCloseTime", "General registration close time is required.");
                }

                if (model.GeneralRegistrationOpenDate.HasValue && model.GeneralRegistrationCloseDate.HasValue && model.GeneralRegistrationOpenTime.HasValue && model.GeneralRegistrationCloseTime.HasValue)
                {
                    var generalOpenDateTime = model.GeneralRegistrationOpenDate.Value.Add(model.GeneralRegistrationOpenTime.Value.TimeOfDay);
                    var generalCloseDateTime = model.GeneralRegistrationCloseDate.Value.Add(model.GeneralRegistrationCloseTime.Value.TimeOfDay);

                    if (generalOpenDateTime >= generalCloseDateTime)
                    {
                        ModelState.AddModelError("GeneralRegistrationOpenDate", "Open date must be before close date.");
                    }
                }

                if (model.HasGeneralRegisterPIN)
                {
                    if (string.IsNullOrEmpty(model.GeneralRegisterPIN))
                    {
                        ModelState.AddModelError("GeneralRegisterPIN", "Password is required.");
                    }

                    if (string.IsNullOrEmpty(model.GeneralRegisterPINMessage))
                    {
                        ModelState.AddModelError("GeneralRegisterPINMessage", "Restriction message is required.");
                    }
                }
            }

            if (model.HasEarlyRegistration && !hasGeneralRegistration)
            {
                ModelState.AddModelError("EarlyRegistrationOpenDate", "You must set the general registration dates first.");
            }

            if (model.HasLateRegistration && !hasGeneralRegistration)
            {
                ModelState.AddModelError("LateRegistrationOpenDate", "You must set the general registration dates first.");
            }

            if (model.HasEarlyRegistration)
            {
                if (!model.EarlyRegistrationOpenDate.HasValue)
                {
                    ModelState.AddModelError("EarlyRegistrationOpenDate", "Early registration open date is required.");
                }

                if (!model.EarlyRegistrationCloseDate.HasValue)
                {
                    ModelState.AddModelError("EarlyRegistrationCloseDate", "Early registration close date is required.");
                }

                if (model.EarlyRegistrationOpenDate.HasValue && model.EarlyRegistrationCloseDate.HasValue)
                {
                    var startDateTime = model.EarlyRegistrationOpenDate.Value.Add(model.EarlyRegistrationOpenTime.HasValue ? model.EarlyRegistrationOpenTime.Value.TimeOfDay : new TimeSpan(0));
                    var endDateTime = model.EarlyRegistrationCloseDate.Value.Add(model.EarlyRegistrationCloseTime.HasValue ? model.EarlyRegistrationCloseTime.Value.TimeOfDay : new TimeSpan(0));

                    if (startDateTime >= endDateTime)
                    {
                        ModelState.AddModelError("EarlyRegistrationOpenDate", "Open date must be before close date.");
                    }
                }

                if (!model.EarlyRegistrationOpenTime.HasValue)
                {
                    ModelState.AddModelError("EarlyRegistrationOpenTime", "Early registration open time is required.");
                }

                if (!model.EarlyRegistrationCloseTime.HasValue)
                {
                    ModelState.AddModelError("EarlyRegistrationCloseTime", "Early registration close time is required.");
                }

                if (model.HasEarlyRegisterPIN)
                {
                    if (string.IsNullOrEmpty(model.EarlyRegisterPIN))
                    {
                        ModelState.AddModelError("EarlyRegisterPIN", "Password is required.");
                    }

                    if (string.IsNullOrEmpty(model.EarlyRegisterPINMessage))
                    {
                        ModelState.AddModelError("EarlyRegisterPINMessage", "Restriction message is required.");
                    }
                }
            }

            if (model.HasLateRegistration)
            {
                if (!model.LateRegistrationOpenDate.HasValue)
                {
                    ModelState.AddModelError("LateRegistrationOpenDate", "Late registration open date is required.");
                }

                if (!model.LateRegistrationCloseDate.HasValue)
                {
                    ModelState.AddModelError("LateRegistrationCloseDate", "Late registration close date is required.");
                }

                if (model.LateRegistrationOpenDate.HasValue && model.LateRegistrationCloseDate.HasValue)
                {
                    var startDateTime = model.LateRegistrationOpenDate.Value.Add(model.LateRegistrationOpenTime.HasValue ? model.LateRegistrationOpenTime.Value.TimeOfDay : new TimeSpan(0));
                    var endDateTime = model.LateRegistrationCloseDate.Value.Add(model.LateRegistrationCloseTime.HasValue ? model.LateRegistrationCloseTime.Value.TimeOfDay : new TimeSpan(0));

                    if (startDateTime >= endDateTime)
                    {
                        ModelState.AddModelError("LateRegistrationOpenDate", "Open date must be before close date.");
                    }
                }

                if (!model.LateRegistrationOpenTime.HasValue)
                {
                    ModelState.AddModelError("LateRegistrationOpenTime", "Late registration open time is required.");
                }

                if (!model.LateRegistrationCloseTime.HasValue)
                {
                    ModelState.AddModelError("LateRegistrationCloseTime", "Late registration close time is required.");
                }

                if (model.HasLateRegisterPIN)
                {
                    if (string.IsNullOrEmpty(model.LateRegisterPIN))
                    {
                        ModelState.AddModelError("LateRegisterPIN", "Password is required.");
                    }

                    if (string.IsNullOrEmpty(model.LateRegisterPINMessage))
                    {
                        ModelState.AddModelError("LateRegisterPINMessage", "Restriction message is required.");
                    }
                }
            }

            if (model.HasEarlyRegistration && hasGeneralRegistration)
            {
                var earlyCloseDate = model.EarlyRegistrationCloseDate.HasValue ? model.EarlyRegistrationCloseDate.Value : DateTime.MaxValue;
                var generalOpenDate = model.GeneralRegistrationOpenDate.HasValue ? model.GeneralRegistrationOpenDate.Value : DateTime.MinValue;

                earlyCloseDate = earlyCloseDate.Add(model.EarlyRegistrationCloseTime.HasValue ? model.EarlyRegistrationCloseTime.Value.TimeOfDay : new TimeSpan(0));
                generalOpenDate = generalOpenDate.Add(model.GeneralRegistrationOpenTime.HasValue ? model.GeneralRegistrationOpenTime.Value.TimeOfDay : new TimeSpan(0));

                if (earlyCloseDate > generalOpenDate)
                {
                    ModelState.AddModelError("EarlyRegistrationCloseDate", "Early registration close date must be before general registration open date.");
                }
            }

            if (model.HasLateRegistration && hasGeneralRegistration)
            {
                var generalCloseDate = model.GeneralRegistrationCloseDate.HasValue ? model.GeneralRegistrationCloseDate.Value : DateTime.MaxValue;
                var lateOpenDate = model.LateRegistrationOpenDate.HasValue ? model.LateRegistrationOpenDate.Value : DateTime.MinValue;

                generalCloseDate = generalCloseDate.Add(model.GeneralRegistrationCloseTime.HasValue ? model.GeneralRegistrationCloseTime.Value.TimeOfDay : new TimeSpan(0));
                lateOpenDate = lateOpenDate.Add(model.LateRegistrationOpenTime.HasValue ? model.LateRegistrationOpenTime.Value.TimeOfDay : new TimeSpan(0));

                if (generalCloseDate > lateOpenDate)
                {
                    ModelState.AddModelError("LateRegistrationOpenDate", "Late registration open date must be after general registration close date.");
                }
            }

            if (model.HasPreRegistration)
            {
                if (!hasGeneralRegistration)
                {
                    ModelState.AddModelError("GeneralRegistrationOpenDate", "General registration open date is required when pre registration is enabled.");
                    ModelState.AddModelError("GeneralRegistrationCloseDate", "General registration close date is required when pre registration is enabled.");
                }

                if (!model.PreRegistrationOpenDate.HasValue)
                {
                    ModelState.AddModelError("PreRegistrationOpenDate", "Pre-registration open date is required.");
                }

                if (!model.PreRegistrationCloseDate.HasValue)
                {
                    ModelState.AddModelError("PreRegistrationCloseDate", "Pre-registration close date is required.");
                }

                if (model.PreRegistrationOpenDate.HasValue && model.PreRegistrationCloseDate.HasValue)
                {
                    var preRegisterStartDateTime = model.PreRegistrationOpenDate.Value.Add(model.PreRegistrationOpenTime.HasValue ? model.PreRegistrationOpenTime.Value.TimeOfDay : new TimeSpan(0));
                    var preRegisterEndDateTime = model.PreRegistrationCloseDate.Value.Add(model.PreRegistrationCloseTime.HasValue ? model.PreRegistrationCloseTime.Value.TimeOfDay : new TimeSpan(0));

                    if (preRegisterStartDateTime >= preRegisterEndDateTime)
                    {
                        ModelState.AddModelError("PreRegistrationOpenDate", "Open date must be before close date.");
                    }
                }

                if (model.HasEarlyRegistration && model.HasPreRegistration)
                {
                    modelState.AddModelError("EarlyRegistrationOpenDate", "You can not have pre-registration  and early registration at the same time.");
                }

                if (!model.PreRegistrationOpenTime.HasValue)
                {
                    ModelState.AddModelError("PreRegistrationOpenTime", "Pre-registration open time is required.");
                }

                if (!model.PreRegistrationCloseTime.HasValue)
                {
                    ModelState.AddModelError("PreRegistrationCloseTime", "Pre-registration close time is required.");
                }

                if (hasGeneralRegistration && model.PreRegistrationCloseDate.HasValue)
                {
                    var preEndDateTime = model.PreRegistrationCloseDate.Value.Add(model.PreRegistrationCloseTime.HasValue ? model.PreRegistrationCloseTime.Value.TimeOfDay : new TimeSpan(0));

                    var generalOpenDateTime = model.GeneralRegistrationOpenDate.Value.Add(model.GeneralRegistrationOpenTime.Value.TimeOfDay);

                    if (preEndDateTime > generalOpenDateTime)
                    {
                        ModelState.AddModelError("PreRegistrationCloseDate", "Pre-registration close date must be before general open date.");
                    }
                }
            }

            if (model.HasLottery)
            {
                if (!hasGeneralRegistration)
                {
                    ModelState.AddModelError(nameof(model.GeneralRegistrationOpenDate), "General registration open date is required when lottery is enabled.");
                    ModelState.AddModelError(nameof(model.GeneralRegistrationCloseDate), "General registration close date is required when lottery is enabled.");
                }

                if (!model.LotteryOpenDate.HasValue)
                {
                    ModelState.AddModelError(nameof(model.LotteryOpenDate), "Lottery open date is required.");
                }

                if (!model.LotteryCloseDate.HasValue)
                {
                    ModelState.AddModelError(nameof(model.LotteryCloseDate), "Lottery close date is required.");
                }

                if (model.LotteryOpenDate.HasValue && model.LotteryCloseDate.HasValue)
                {
                    var lotteryStartDateTime = model.LotteryOpenDate.Value.Add(model.LotteryOpenTime.HasValue ? model.LotteryOpenTime.Value.TimeOfDay : new TimeSpan(0));
                    var lotteryEndDateTime = model.LotteryCloseDate.Value.Add(model.LotteryCloseTime.HasValue ? model.LotteryCloseTime.Value.TimeOfDay : new TimeSpan(0));

                    if (lotteryStartDateTime >= lotteryEndDateTime)
                    {
                        ModelState.AddModelError(nameof(model.LotteryOpenDate), "Open date must be before close date.");
                    }
                }

                if (model.HasLottery && model.HasPreRegistration)
                {
                    modelState.AddModelError(nameof(model.LotteryOpenDate), "You can not have pre-registration  and lottery at the same time.");
                }

                if (model.HasEarlyRegistration && model.HasLottery)
                {
                    modelState.AddModelError("EarlyRegistrationOpenDate", "You can not have lottery and early registration at the same time.");
                }

                if (!model.LotteryOpenTime.HasValue)
                {
                    ModelState.AddModelError(nameof(model.LotteryOpenTime), "Lottery open time is required.");
                }

                if (!model.LotteryCloseTime.HasValue)
                {
                    ModelState.AddModelError(nameof(model.LotteryCloseTime), "Lottery close time is required.");
                }

                if (hasGeneralRegistration && model.LotteryCloseDate.HasValue)
                {
                    var lotteryEndDateTime = model.LotteryCloseDate.Value.Add(model.LotteryCloseTime.HasValue ? model.LotteryCloseTime.Value.TimeOfDay : new TimeSpan(0));

                    var generalOpenDateTime = model.GeneralRegistrationOpenDate.Value.Add(model.GeneralRegistrationOpenTime.Value.TimeOfDay);

                    if (lotteryEndDateTime > generalOpenDateTime)
                    {
                        ModelState.AddModelError(nameof(model.LotteryCloseDate), "Lottery close date must be before general open date.");
                    }
                }
            }
        }

        private const string DateListError = " is not a valid date. Please use this format mm/dd/yyyy to enter the dates and separate them with ','.";
    }
}
