﻿using Jumbula.Common.Helper;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Configuration;
using Jumbula.Core.Business;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Business
{
    public class StorageBusiness : IStorageBusiness
    {
        private const string DirectoryDelimeter = "/";
        private readonly CloudStorageAccount _account;
        private readonly CloudBlobClient _blobClient;
        private readonly CloudBlobContainer _blobContainer;
        private readonly string _azureStorageUrl = "";

        public StorageBusiness()
        {
            var connectionString = (WebConfigurationManager.GetSection("jumbula") as NameValueCollection)["AzureStorageConnection"];
            _azureStorageUrl = (WebConfigurationManager.GetSection("jumbula") as NameValueCollection)["AzureStorageUrl"];

            try
            {
                _account = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting(connectionString));
                _blobClient = _account.CreateCloudBlobClient();
                _blobContainer = _blobClient.GetContainerReference(Constants.Path_ClubFilesContainer);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IsExist(string path)
        {
            bool result = false;

            try
            {

                HttpWebRequest req = (HttpWebRequest)System.Net.WebRequest.Create(path);
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch
            {
                result = false;
            }

            return result;
        }

        public CloudStorageAccount GetAccountInstance()
        {
            return _account;
        }

        public void CreateContainer(string connectionString, string containerName, bool publicAccess)
        {
            try
            {
                var account = CloudStorageAccount.DevelopmentStorageAccount;
                //CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting(connectionString));
                var blobClient = account.CreateCloudBlobClient();
                var blobContainer = blobClient.GetContainerReference(containerName);

                blobContainer.CreateIfNotExists();

                if (publicAccess == true)
                {
                    blobContainer.SetPermissions(
                        new BlobContainerPermissions
                        {
                            PublicAccess = BlobContainerPublicAccessType.Blob
                        });
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DoesContainerExist(string connectionString, string containerName)
        {
            bool result = false;
            var account = CloudStorageAccount.DevelopmentStorageAccount;
            //CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting(connectionString));
            var blobClient = account.CreateCloudBlobClient();

            try
            {
                IEnumerable<CloudBlobContainer> containers = blobClient.ListContainers();
                result = containers.Any(x => x.Name == containerName);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public Uri GetUri(string containerName, string directoryName, string blobName)
        {
            string path = containerName + DirectoryDelimeter + directoryName + DirectoryDelimeter + blobName;
            return GetUri(path);
        }

        public Uri GetUri(string containerName, string parentDirName, string childDirName, string blobName)
        {
            string path = containerName + DirectoryDelimeter + parentDirName + DirectoryDelimeter + childDirName + DirectoryDelimeter + blobName;
            return GetUri(path);
        }

        public string PathCombine(string directoryName, string blobName)
        {
            return directoryName + DirectoryDelimeter + blobName;
        }

        public string PathCombine(string parentDirName, string childDirName, string blobName)
        {
            if (!string.IsNullOrEmpty(parentDirName))
            {
                return parentDirName + DirectoryDelimeter + childDirName + DirectoryDelimeter + blobName;
            }
            else
            {
                return childDirName + DirectoryDelimeter + blobName;
            }
        }

        public void UploadBlob(string blobName, Stream stream, string contentType, string containerName)
        {
            try
            {
                CloudBlobContainer blobContainer = _blobClient.GetContainerReference(containerName);

                CloudBlockBlob blob = blobContainer.GetBlockBlobReference(blobName);
                blob.Properties.ContentType = contentType;
                blob.UploadFromStream(stream);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void UploadBlob(string blobName, Stream stream, string contentType)
        {
            try
            {
                CloudBlockBlob blob = _blobContainer.GetBlockBlobReference(blobName);
                blob.Properties.ContentType = contentType;
                blob.UploadFromStream(stream);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void UploadBlob(string directoryName, string blobName, Stream stream, string contentType)
        {

            UploadBlob(PathCombine(directoryName, blobName), stream, contentType);
        }

        public void UploadBlob(string directoryName, string blobName, Stream stream, string contentType, string containerName)
        {
            UploadBlob(PathCombine(directoryName, blobName), stream, contentType, containerName);
        }

        public void UploadBlob(string parentDirName, string childDirName, string blobName, Stream stream, string contentType, string containerName)
        {
            UploadBlob(PathCombine(parentDirName, childDirName, blobName), stream, contentType, containerName);
        }

        public void DeleteBlob(string parentDirName, string childDirName, string blobName, string containerName)
        {
            try
            {
                var path = PathCombine(parentDirName, childDirName, blobName);
                DeleteBlob(path, containerName);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteBlob(string parentDirName, string blobName, string containerName)
        {
            try
            {
                var path = PathCombine(parentDirName, blobName);
                DeleteBlob(path, containerName);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteBlob(string blobPath, string containerName)
        {
            try
            {
                CloudBlobContainer blobContainer = _blobClient.GetContainerReference(containerName);

                CloudBlockBlob blob = blobContainer.GetBlockBlobReference(blobPath);
                blob.Delete();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void UploadDirectory(string sourceDirectory, string folerName, string containerName)
        {
            try
            {
                var folder = new DirectoryInfo(sourceDirectory);

                foreach (var file in folder.GetFiles())
                {
                    string blobName = file.Name;

                    if (!string.IsNullOrEmpty(folerName))
                    {
                        blobName = PathCombine(folerName, blobName);
                    }

                    using (Stream stream = file.OpenRead())
                    {
                        UploadBlob(blobName, stream, FileHelper.GetMimeTypeFromExtension(file.Extension), containerName);
                    }
                }

                foreach (DirectoryInfo dir in folder.GetDirectories())
                {
                    var prefix = dir.Name;

                    if (!string.IsNullOrEmpty(folerName))
                    {
                        prefix = PathCombine(folerName, prefix);
                    }

                    UploadDirectory(dir.FullName, prefix, containerName);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Uri[] ListDirectoryBlobFiles(string parentDirName, string childDirName, string containerName)
        {
            string path = parentDirName + DirectoryDelimeter + childDirName + DirectoryDelimeter;

            return ListDirectoryBlobFiles(path, containerName);
        }

        public Uri[] ListDirectoryBlobFiles(string parentDirName, string childDirName, string grandChildDirName, string containerName)
        {
            string path = parentDirName + DirectoryDelimeter + childDirName + DirectoryDelimeter + grandChildDirName + DirectoryDelimeter;

            return ListDirectoryBlobFiles(path, containerName);
        }

        public Uri[] ListDirectoryBlobFiles(string path, string containerName)
        {
            var uri = new HashSet<Uri>();

            CloudBlobContainer blobContainer = _blobClient.GetContainerReference(containerName);

            var list = blobContainer.ListBlobs(path, false);

            foreach (IListBlobItem item in list)
            {
                if (item.GetType() == typeof(CloudBlockBlob))
                {
                    CloudBlockBlob blob = (CloudBlockBlob)item;
                    uri.Add(blob.Uri);
                }
                else if (item.GetType() == typeof(CloudPageBlob))
                {
                    CloudPageBlob blob = (CloudPageBlob)item;
                    uri.Add(blob.Uri);
                }
                else if (item.GetType() == typeof(CloudBlobDirectory)) // do nothing
                {
                }
            }

            return uri.ToArray();
        }

        public void ReadFile(string sourcePath, string targetPath, string containerName)
        {
            CloudBlobContainer blobContainer = _blobClient.GetContainerReference(containerName);

            CloudBlockBlob sourceBlob = blobContainer.GetBlockBlobReference(sourcePath);
            CloudBlockBlob targetBlob = blobContainer.GetBlockBlobReference(targetPath);

            targetBlob.StartCopyFromBlob(sourceBlob);
        }

        public System.IO.Stream DownloadBlobAsStream(string blobUri, string containerName)
        {
            Stream mem = new MemoryStream();
            CloudBlobContainer blobContainer = _blobClient.GetContainerReference(containerName);

            CloudBlockBlob blob = blobContainer.GetBlockBlobReference(blobUri);

            if (blob != null)
            {
                blob.DownloadToStream(mem);
            }

            mem.Position = 0;

            return mem;
        }

        private Uri GetUri(string path)
        {
            var baseUri = new Uri(_azureStorageUrl + DirectoryDelimeter);
            var uri = new Uri(baseUri, path);

            return uri;
        }
    }
}
