﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class TeachersListPdfViewModel
    {
        public string ClubLogo { get; set; }
        public string SchoolLogo { get; set; }
        public List<TeachersPdfViewModel> Teachers { get; set; }
        public string SchoolName { get; set; }
        public string Date { get; set; }
    }
}