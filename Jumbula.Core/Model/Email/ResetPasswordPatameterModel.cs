﻿using Jumbula.Common.Enums;

namespace Jumbula.Core.Model.Email
{
    public class ResetPasswordParameterModel
    {
        public string UserName { get; set; }

        public RequestType RequestType { get; set; }

        public int? ClubId { get; set; }

        public string ResetPasswordUrl { get; set; }

        public string ReturnUrl { get; set; }
    }
}
