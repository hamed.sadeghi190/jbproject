﻿using Jumbula.Common.Enums;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;

namespace SportsClub.Areas.Dashboard.Models
{
    public class UserViewModel
    {
        public int Id { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "Email format is invalid.")]
        [DisplayName("Username")]
        public string UserName { get; set; }

        public int UserClubRoleId { get; set; }
        public string UserClubRole { get; set; }
        [JsonIgnore]
        public IEnumerable<PlayerProfile> Players { get; set; }


        public bool Status { get; set; }
        public string PlayersName
        {
            get
            {
                if (Players != null && Players.Any())
                {
                    return string.Join(", ", Players.Select(c => c.Contact.FullName));
                }
                return string.Empty;
            }
        }

        public string UsernameAndPlayersName
        {
            get
            {
                if (!string.IsNullOrEmpty(PlayersName))
                {
                    return string.Format("{0} ({1})", UserName, PlayersName);
                }
                return UserName;
            }

        }

        public decimal Balance
        {
            get
            {

                if (UserOrdersItems != null && UserOrdersItems.Any())
                {
                    return Ioc.AccountingBusiness.OrderItemsBalance(UserOrdersItems);
                }
                return 0;
            }
        }
        public int TotalOrders
        {
            get
            {

                if (UserOrdersItems != null && UserOrdersItems.Any())
                {
                    return UserOrdersItems.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).Any() ? UserOrdersItems.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).Count() : 0;
                }

                return 0;
            }
        }
        public decimal Credit { get; set; }

        [JsonIgnore]
        public List<OrderItem> UserOrdersItems { get; set; }

        public string ClubName { get; set; }

        public int ClubId { get; set; }

    }

}