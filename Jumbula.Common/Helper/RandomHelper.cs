﻿using System;

namespace Jumbula.Common.Helper
{
    public static class RandomHelper
    {
        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();

        public static int RandomNumber(int min, int max)
        {
            lock (syncLock)
            {
                return random.Next(min, max);
            }
        }

        public static string GenerateGuid()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}
