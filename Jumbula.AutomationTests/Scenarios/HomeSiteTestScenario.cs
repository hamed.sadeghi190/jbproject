﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Tests;
using NUnit.Framework;

namespace Jumbula.AutomationTests.Scenarios
{
    public class HomeSiteTestScenario: BaseTestScenario
    {
        [Test]
        public void SeasonGrid()
        {
            BaseUrl = "https://qaclubsand.aftersearch.com";
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            ExtraAPI.Wait(5000);
            WebDriver.Manage().Window.Maximize();

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginQa();

            // Go to edit home site
            var bladeTest = new BladeTest(WebDriver);
            //bladeTest.GoToEditHomeSite();
            
            // Season grid
            var homeSiteTest = new HomeSiteTests(WebDriver);
            WebDriver.Navigate().GoToUrl("https://qaclubsand.aftersearch.com/edit");
            ExtraAPI.Wait(60000);
            homeSiteTest.SeasonGrid();
        }
    }
}
