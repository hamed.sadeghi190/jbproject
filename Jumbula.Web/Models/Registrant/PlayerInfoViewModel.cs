﻿using Jb.Framework.Common.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Enums;
using System.Web.Mvc;

namespace SportsClub.Models
{
    public class PlayerInfoViewModel
    {
        public int Id { get; set; }
        
        [Display(Name = "Photography/video release permission")]
        public bool? HasPermissionPhotography { get; set; }

        public string StrGrade { get; set; }
        [Display(Name = "Grade")]
        public List<SelectListItem> GradeList { get; set; }
        public SchoolGradeType? Grade { get; set; }
        public List<SelectListItem> GenderList { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Allergies and dietary restrictions")]
        public string Allergies { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Medical conditions and special needs")]
        public string SpecialNeeds { get; set; }
        public string DoctorName { get; set; }
        public string DoctorLocation { get; set; }
        public string DoctorPhone { get; set; }
        public bool? SelfAdministerMedication { get; set; }
        public bool? HasAllergies { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Self-administered medication")]
        public string SelfAdministerMedicationDescription { get; set; }
        public bool? NutAllergy { get; set; }
        public bool? HasSpecialNeeds { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Nut allergy")]
        public string NutAllergyDescription { get; set; }
        public string SchoolName { get; set; }
        public string HomeRoom { get; set; }
    }
    
}