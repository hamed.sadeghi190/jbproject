﻿using System;
using System.Collections.Generic;

namespace Jumbula.Core.Model.Email
{
    public class AutoChargeSupportSummaryEmailParameterModel
    {
        public int? ClubId { get; set; }
       
        public List<long> SuccessInstallments { get; set; } = new List<long>();
        public List<Tuple<long, long>> FailedInstallments { get; set; } = new List<Tuple<long, long>>();
        public List<Tuple<long, long>> SystemFailedInstallments { get; set; } = new List<Tuple<long, long>>();
        public List<long> NotAttemptedInstallments { get; set; } = new List<long>();
        public List<long> OtherErrorInstallments { get; set; } = new List<long>();
    }
}
