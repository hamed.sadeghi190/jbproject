﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jumbula.Core.Domain
{
    public class CatalogSettingCounty
    {
        [ForeignKey("CountyId")]
        
        public virtual County County { get; set; }

        [Key, Column(Order = 0)]
        public int CountyId { get; set; }

        [ForeignKey("CatalogSettingId")]
        public virtual CatalogSetting CatalogSetting { get; set; }

        [Key, Column(Order = 1)]
        public int CatalogSettingId { get; set; }

        [Key, Column(Order = 2)]
        public bool Maybe { get; set; }
    }
}
