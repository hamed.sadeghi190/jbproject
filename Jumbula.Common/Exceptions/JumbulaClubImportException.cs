﻿using System;

namespace Jumbula.Common.Exceptions
{
    public abstract class JumbulaClubImportException : JumbulaException
    {
        protected JumbulaClubImportException(Exception e)
            : base(Constants.Constants.M_E_Import, e)
        {

        }
    }
}
