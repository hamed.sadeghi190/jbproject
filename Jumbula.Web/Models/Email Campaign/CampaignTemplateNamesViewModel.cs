﻿using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Models.Email_Campaign
{
    public class CampaignTemplateNamesViewModel : BaseViewModel<EmailTemplate>
    {
        public string TemplateName { get; set; }
    }
}