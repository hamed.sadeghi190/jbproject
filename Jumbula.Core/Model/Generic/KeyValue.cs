﻿namespace Jumbula.Core.Model.Generic
{
    public class KeyValue
    {
        public string Key { get; set; }
        public decimal Value { get; set; }
    }
}
