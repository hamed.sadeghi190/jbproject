﻿(function () {
    'use strict';
    angular.module('dashboardApp').controller('clubsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbConfirmation, jbToast) {

        window.app.globalObjects.pageClass = 'Members-view';
        $scope.MC.openBlade({ url: "Dashboard/Blade/Clubs", order: 1, menu: "Clubs" });

        $scope.MC.stateParams = $stateParams;
        $scope.type = $stateParams.type;

        if ($scope.type == "Schools") {
            $scope.pageMode = "Schools"
        } else {
            $scope.pageMode = "Providers"
        }

        $scope.baseUrl = window.location.origin + "/Dashboard/Club";

        $scope.filter = {};

        $http.get($scope.baseUrl + '/ClubPage').success(function (data, status, headers, config) {
            $scope.Model = data;
            $scope.filter.FilterTypes = 'Name';
            $scope.filter.activeType = 'Active';
        }).error(function (data, status, headers, config) {
            jbToast.error(data);
        });

        $scope.searchTerm = "";
        $scope.filter.typeId = "-1";
        $scope.filter.endDate = "";
        $scope.filter.startDate = "";
        $scope.filter.FilterTypes = "0";

        $scope.activeType = [{ Value: 'All', Title: "All" }, { Value: 'Active', Title: "Active" }, { Value: 'Inactive', Title: "Inactive" }];
        $scope.filter.activeType = 'Active';

        $scope.mainGridDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: $scope.baseUrl + '/GetSubClubs/',
                    contentType: "application/json; charset=utf-8", 
                    type: "POST"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        data = $.extend(
                           {
                               sort: null,
                               filter: null,
                               term: $scope.searchTerm,
                               typeId: $scope.filter.typeId,
                               endDate:$scope.filter.endDate ,
                               startDate: $scope.filter.startDate,
                               activeType: $scope.filter.activeType,
                               filterType: $scope.filter.FilterTypes,
                               typeMember: $scope.type,
                           }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            pageSize: 50,
            batch: true,
            serverPaging: true,
            serverSorting: false,
            schema: {
                data: "data",
                total: "total"
            },
            sortable: true
        });
      
        $scope.mainGridOptions = {
            dataSource: $scope.mainGridDataSource,
            sortable: true,
            serverPaging: true,
            dataBound: function () {
                var grid = $('#mainGrid').data('kendoGrid');
                if (grid.dataSource.data().length > 0) {

                    if (grid.dataSource.data()[0].ClubType == 'School') {
                        grid.hideColumn(6);
                    }
                }
            },
            columns: [{
                field: "Name",
                title: "Name"
            }, {
                field: "Domain",
                title: "Domain"
            }, {
                field: "AdminName",
                title: "Admin",
            }, {
                field: "StrPhone",
                title: "Phone",
            }, {
                field: "Email",
                title: "Email",
                width: "200px",
            }, {
                field: "StrAgreementExpirationDate",
                title: "Expiration date",
                width: "150px"
            },
            {
                field: "CommissionRateString",
                title: "Comm. rate",
                hidden: !$scope.MC.PortalParameters.ShowCommitionRate,
            },
            {
                field: "County",
                title: "County",
            },  {
                title: "Actions",
                width: "70px",
                headerTemplate: "<span class='actions-title'>Actions</span>",
                template: '<ul kendo-menu style="display: inline-block;" k-direction="\'left\'" k-orientation="\'horizontal\'" k-rebind="\'horizontal\'" k-on-select="">\
                     <li > ... \
                              <ul>\
                                    <li style="margin-left: 22px;" ng-click="openClub(\'#=Domain#\')" >\
                                          <span class="k-link ">Login</span>\
                                    </li>\
                                    <hr/>\<li ng-show="#=ClubType=="School"#" ui-sref="ClubManage({clubId: #=Id# ,type:\'School\'})">\
                                          <span class="k-link jbi-pencil">Edit</span>\
                                    </li>\
                                    <li ng-show="#=ClubType=="Provider"#" ui-sref="ClubManage({clubId: #=Id#,type:\'Provider\'})">\
                                          <span class="k-link jbi-pencil">Edit</span>\
                                    </li>\
                                    <li  ng-click="deleteClub( #=Id# )">\
                                          <span class="k-link jbi-remove">Delete</span>\
                                    </li>\
                                    <li style="margin-left: 25px;" ng-show="#=ClubType=="School"#" ui-sref="ClubResetPassword({clubId: #=Id#,type:\'School\'})">\
                                          <span class="k-link ">Reset password</span>\
                                    </li>\
                                    <li style="margin-left: 25px;" ng-show="#=ClubType=="Provider"#" ui-sref="ClubResetPassword({clubId: #=Id#,type:\'Provider\'})">\
                                          <span class="k-link ">Reset password</span>\
                                    </li>\
                                    <li style="margin-left: 25px;" ng-show="#=ClubType=="School"#" ui-sref="ClubUsersEdit({clubId: #=Id#,type:\'School\'})">\
                                          <span class="k-link ">Edit user</span>\
                                    </li>\
                                    <li style="margin-left: 25px;" ng-show="#=ClubType=="Provider"#" ui-sref="ClubUsersEdit({clubId: #=Id#,type:\'Provider\'})">\
                                          <span class="k-link ">Edit user</span>\
                                    </li>\
                            </ul>\
                        </li>\
                    </ul>'
            }],
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
        };

        $scope.search = function () {
            $("#mainGrid").data("kendoGrid").dataSource.read();
        };
        $scope.clearFilters = function () {
            $scope.searchTerm = "";
            $scope.filter.typeId = "-1";
            $scope.filter.subTypeId = "-1";
            $scope.filter.activeType = 'Active';
            $scope.filter.endDate = "";
            $scope.filter.startDate = "";
            $scope.filter.FilterTypes = 'Name';

            $("#mainGrid").data("kendoGrid").dataSource.read();
        }

        $scope.openClub = function (clubDomain)
        {
            var originUrl = window.location.origin;
            
            var endIndex = originUrl.indexOf(".");
            var mainDomain = originUrl.substring(8, endIndex);
           
            var url = originUrl.replace(mainDomain, clubDomain)+'/login';
          
            window.open(url);
        }

        $scope.deleteClub = function (id) {
            jbConfirmation
                .yes(function () {
                    $http.post($scope.baseUrl + '/DeleteClub', { clubId: id }).success(function (data, status, headers, config) {
                        if (data.Status == true) {

                            $scope.mainGridOptions.dataSource.read();

                        } else {
                            jbToast.error(data.Message)
                        }
                    });
                })
                .confirm();
        };

        $scope.resetClubPassword = function (id) {
            jbConfirmation
                .yes(function () {
                    $http.post($scope.baseUrl + '/DeleteClub', { clubId: id }).success(function (data, status, headers, config) {
                        if (data.Status == true) {

                            $scope.mainGridOptions.dataSource.read();

                        }
                    });
                })
                .confirm();
        };
    }]);
})();