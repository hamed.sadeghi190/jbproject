﻿using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Common.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Jumbula.Core.Data
{
    public interface IRepository<TEntity> : IDisposable where TEntity : BaseEntity
    {
        TEntity Get(int id, params Expression<Func<TEntity, object>>[] includeExpressions);

        TEntity Get(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeExpressions);

        IQueryable<TEntity> GetList(params Expression<Func<TEntity, object>>[] includeExpressions);

        IQueryable<TEntity> GetList(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeExpressions);

        IQueryable<TEntity> GetList<TKey>(Expression<Func<TEntity, bool>> predicate,
                                          Expression<Func<TEntity, TKey>> orderBy, params Expression<Func<TEntity, object>>[] includeExpressions);

        IQueryable<TEntity> GetList<TKey>(Expression<Func<TEntity, TKey>> orderBy, params Expression<Func<TEntity, object>>[] includeExpressions);

        PaginatedList<TEntity> GetList(int pageIndex, int pageSize, Expression<Func<TEntity, int>> keySelector,
            OrderBy orderBy = OrderBy.Ascending, params Expression<Func<TEntity, object>>[] includeExpressions);

        OperationStatus Save();
        OperationStatus Create(TEntity entity);
        OperationStatus Create(IEnumerable<TEntity> entities);
        OperationStatus Delete(int id);
        OperationStatus Delete(TEntity entity);
        OperationStatus DeleteWhere(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "");
        OperationStatus Update(TEntity entity);

        OperationStatus ExecuteStoreCommand(string cmdText, params object[] parameters);
    }

    public interface IRepository<TEntity, in TPrimaryKey> : IDisposable where TEntity : BaseEntity<TPrimaryKey> where TPrimaryKey : struct
    {
        TEntity Get(TPrimaryKey id, params Expression<Func<TEntity, object>>[] includeExpressions);

        TEntity Get(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeExpressions);

        IQueryable<TEntity> GetList(params Expression<Func<TEntity, object>>[] includeExpressions);

        IQueryable<TEntity> GetList(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeExpressions);

        IQueryable<TEntity> GetList<TKey>(Expression<Func<TEntity, bool>> predicate,
                                          Expression<Func<TEntity, TKey>> orderBy, params Expression<Func<TEntity, object>>[] includeExpressions);

        IQueryable<TEntity> GetList<TKey>(Expression<Func<TEntity, TKey>> orderBy, params Expression<Func<TEntity, object>>[] includeExpressions);

        OperationStatus Save();
        OperationStatus Create(TEntity entity);
        OperationStatus Create(IEnumerable<TEntity> entities);
        OperationStatus Delete(TPrimaryKey id);
        OperationStatus Delete(TEntity entity);
        OperationStatus DeleteWhere(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "");
        OperationStatus Update(TEntity entity);

        OperationStatus ExecuteStoreCommand(string cmdText, params object[] parameters);
    }

}
