﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ProgramChargesModel
    {
        public string ProgramName { get; set; }

        public string Email { get; set; }

        public long OrderItemId { get; set; }

        public long OrderId { get; set; }

        public string ConfirmationId { get; set; }

        public DateTime RegDate { get; set; }

        public string FullName { get; set; }

        public List<OrderChargeDiscount> Charges { get; set; }

        public decimal TotalPrice { get; set; }

        public string StartEndDate { get; set; }

        public int ItemStatusReason { get; set; }

    }
}