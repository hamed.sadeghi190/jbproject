﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('InstructorListPortalReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonDomain = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        var model = {};
        var dataInfo = {};


        $scope.Model = customReportService.getModelFromStrorage();

        $scope.filter = {};
        $scope.filter.printMode = false;


        $http.get(baseUrl + '/GetReportHeader').success(function (data, status, headers, config) {

            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
            dataInfo = data;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        var model = $scope.Model;

        $scope.InstructorListDataSource = new kendo.data.DataSource({
            transport:
                {
                    read: {
                        url: baseUrl + '/GetInstructorListReportInfo',
                        type: "Post",
                        data: function () {
                            return {
                                //modelStringify: $scope.ModelStr,
                                model: model,
                                printMode: $scope.filter.printMode
                            };
                        },
                    },

                },

            schema: {
                total: "TotalCount",
                data: "DataSource",
            },
            serverSorting: false,
            serverPaging: true,
            pageSize: 50,
            resizable: true,
        });

        $scope.InstructorListGridOptions = {
            excel: {
                allPages: true,
                fileName: "InstructorList.xlsx",
                proxyURL: "/Dashboard/PortalReport/Save"
            },
            excelExport: function (e) {

                var rows = e.workbook.sheets[0].rows;
                rows.unshift({
                    cells: [{
                        value: ""
                    }]
                });
                rows.unshift({
                    cells: [{
                        value: dataInfo.Date
                    }]
                });                
                rows.unshift({
                    cells: [{
                        value: "Instructor list - " + dataInfo.ClubName
                    }]
                });

                cfpLoadingBar.complete();
            },
            dataSource: $scope.InstructorListDataSource,
            resizable: true,
            dataBound: function () {

                var grid = $("#InstructorListGrid").data("kendoGrid");
                customReportService.initializeDoubleScroll(grid);
            },
            sortable: true,
            columns: [

                {
                    field: "Name",
                    title: "Instructor name",
                    width: "190px"
                }, {
                    field: "Status",
                    title: "Status",
                    width: "120px"
                }, {
                    field: "DateOfBackgroundCheck",
                    title: "BGC date",
                    width: "160px"
                }, {
                     field: "ProviderName",
                     title: "Provider name",
                     width: "190px"
                }, {
                    field: "CellPhone",
                    title: "Cell phone",
                    width: "120px"
                }, 
            ],
            pageable: {
                refresh: true,
                pageSizes:
                [
                   5, 10, 20, 30, 50, 100
                ],
                buttonCount: 5
            },

        }


        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#InstructorListGrid").getKendoGrid().saveAsExcel();
            cfpLoadingBar.complete();
        }
        $scope.saveToPDF = function () {

            $http.post(baseUrl + '/DownloadResult', { data: JSON.stringify(model) }, {
                headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
            }).success(function (data) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                var blob = new Blob([data], { type: "application/pdf" });

                var fileURL = URL.createObjectURL(blob);
                a.href = fileURL;
                a.download = "InstructorReport.pdf";
                a.click();
            });


        };


        $scope.print = function () {
            $http.post(baseUrl + '/DownloadResult', { data: JSON.stringify(model) }, {
                headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
            }).success(function (data) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                var blob = new Blob([data], { type: "application/pdf" });

                var fileURL = URL.createObjectURL(blob);
                a.href = fileURL;
                a.download = "InstructorReport.pdf";
                a.click();
            });

        }


        function printGrid() {
            var gridElement = $('#InstructorListGrid'),
                printableContent = '',
                win = window.open('', '', 'width=800, height=500, resizable=1, scrollbars=1'),
                doc = win.document.open();

            var htmlStart =
                '<!DOCTYPE html>' +
                '<html>' +
                '<head>' +
                '<meta charset="utf-8" />' +
                '<title></title>' +
                '<div> <ul style="list-style:none"> ' + "Instructor list - " + dataInfo.ClubName + ' </li> </ul> </div>' +
                '<div> <ul style="list-style:none"> ' + $scope.Model.ProviderName + ' </li> </ul> </div>' +
                '<div> <ul style="list-style:none"> ' + dataInfo.Date + ' </li> </ul> </div>' +
                '<link href="http://kendo.cdn.telerik.com/' + kendo.version + '/styles/kendo.common.min.css" rel="stylesheet" /> ' +
                '<style>' +
                'html { font: 11pt sans-serif; text-align: left }' +
                '.k-grid { border-top-width: 0; }' +
                '.k-grid, .k-grid-content { height: auto !important; padding: 20px; text-align: left}' +
                '.k-grid-content { overflow: visible !important; padding: 20px;}' +
                '.k-grid .k-grid-header  th { padding-bottom: 20px; }' +
                '.k-grid-toolbar, .k-grid-pager > .k-link { display: none; }' +
                '</style>' +
                '</head>' +
                '<body>';

            var htmlEnd =
                    '</body>' +
                    '</html>';

            var gridHeader = gridElement.children('.k-grid-header');
            if (gridHeader[0]) {
                var thead = gridHeader.find('thead').clone().addClass('k-grid-header');
                printableContent = gridElement
                    .clone()
                        .children('.k-grid-header').remove()
                    .end()
                        .children('.k-grid-content')
                            .find('table')
                                .first()
                                    .children('tbody').before(thead)
                                .end()
                            .end()
                        .end()
                    .end()[0].outerHTML;
            } else {
                printableContent = gridElement.clone()[0].outerHTML;
            }

            doc.write(htmlStart + printableContent + htmlEnd);
            doc.close();
            win.print();
        }

    }
    ]);

})();