﻿using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class ContactBusiness : IContactBusiness
    {
        #region Constants

        #endregion

        #region Fields
        private readonly IRepository<Contact> _contactRepository;
        #endregion 

        #region Constructors
        public ContactBusiness(IRepository<Contact> contactRepository)
        {
            _contactRepository = contactRepository;
        }
        #endregion 

        #region Public methods

        public OperationStatus Delete(Contact contact)
        {
            return _contactRepository.Delete(contact);
        }

        #endregion 

        #region Private methods
     

        #endregion 
    }
}
