﻿using FluentAssertions;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Email;
using Jumbula.Core.Model.People;
using Jumbula.Test.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using OrderItem = Jumbula.Core.Domain.OrderItem;
using TimeZone = Jumbula.Common.Enums.TimeZone;

namespace Jumbula.Business.Tests.Unit
{
    [TestClass]
    public class PeopleBusinessTest
    {
        #region Fields
        private Mock<IClubSettingBusiness> _mockClubSettingBusiness;
        private Mock<IOrderItemBusiness> _mockOrderItemBusiness;
        private Mock<IEmailBusiness> _mockEmailBusiness;
        private Mock<IAccountingBusiness> _mockAccountingBusiness;
        private Mock<IClubBusiness> _mockClubBusiness;
        private Mock<IPlayerProfileBusiness> _mockPlayerProfileBusiness;
        private Mock<IProgramSessionBusiness> _mockProgramSessionBusiness;
        private Mock<IProgramBusiness> _mockProgramBusiness;

        private readonly DelinquentFilterViewModel _delinquentFilterViewModel = new DelinquentFilterViewModel();
        #endregion

        #region Constants
        private const int ClubId = 10;
        private const int OrderItemId = 100;
        private const int UserId = 15;
        private readonly List<DelinquentEmailParameterRelatedEntityItemModel> _orderItemEntities = new List<DelinquentEmailParameterRelatedEntityItemModel> { new DelinquentEmailParameterRelatedEntityItemModel() { RelatedEntityName = nameof(OrderItem), RelatedEntityId = 10 }, new DelinquentEmailParameterRelatedEntityItemModel() { RelatedEntityName = nameof(OrderItem), RelatedEntityId = 110 }, new DelinquentEmailParameterRelatedEntityItemModel() { RelatedEntityName = nameof(OrderItem), RelatedEntityId = 120 } };
        #endregion

        #region Test methods

        [TestMethod]
        public void GetDelinquentList_DelinquentPolicyIsNull_ShouldReturnCountBeZero()
        {
            // Arrange
            var service = CreateService();

            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(null as DelinquentPolicy);

            // Act
            var result = service.GetDelinquentList(ClubId, _delinquentFilterViewModel, new List<Sort>(), 1, 50);

            // Assert
            result.Should().BeEmpty();
        }

        [TestMethod]
        public void GetDelinquentList_AutoChargeFailAttemptNumberIsZero_ShouldReturnCountBeZero()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyForAutoChargeFailAttemptNumber();
            delinquentPolicy.AutoChargeFailAttemptNumber = 0;

            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);

            // Act
            var result = service.GetDelinquentList(ClubId, _delinquentFilterViewModel, new List<Sort>(), 1, 50);

            // Assert
            result.Should().BeEmpty();
        }

        [TestMethod]
        public void GetDelinquentList_AutoChargeFailAttemptNumberIsNotZero_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyForAutoChargeFailAttemptNumber();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentOrderInstallmentItems(ClubId, delinquentPolicy))
                .Returns(FillDelinquentListForAutoChargeFailAttemptNumberIsNotZero().AsQueryable);

            // Act
            var result = service.GetDelinquentList(ClubId, _delinquentFilterViewModel, new List<Sort>(), 1, 50);

            // Assert
            result.Count.Should().Be(1);
            result[0].Attempts.Should().Be("2");
            result[0].PaidAmount.Should().Be("100");
            result[0].TotalAmount.Should().Be("200");
            result[0].Policy.Should().Be("P1");
        }

        [TestMethod]
        public void GetDelinquentList_FamilyBalanceNoLimitDateIsNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyForFamilyBalanceNoLimitDate();
            delinquentPolicy.FamilyBalanceNoLimitDate = null;

            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);

            // Act
            var result = service.GetDelinquentList(ClubId, _delinquentFilterViewModel, new List<Sort>(), 1, 50);

            // Assert
            result.Should().BeEmpty();
        }

        [TestMethod]
        public void GetDelinquentList_FamilyBalanceNoLimitDateIsNotNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyForFamilyBalanceNoLimitDate();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentFullPaidItems(ClubId, delinquentPolicy)).Returns(FillOrderItems().AsQueryable());

            // Act
            var result = service.GetDelinquentList(ClubId, _delinquentFilterViewModel, new List<Sort>(), 1, 50);

            // Assert
            result.Count.Should().Be(2);
            foreach (var item in result)
            {
                item.Attempts.Should().Be("");
                item.Confirmation.Should().NotBeNullOrWhiteSpace();
                item.Email.Should().NotBeNullOrWhiteSpace();
                item.PaidAmount.Should().NotBeNullOrWhiteSpace();
                item.TotalAmount.Should().NotBeNullOrWhiteSpace();
                item.Participant.Should().NotBeNullOrEmpty();
                item.Policy.Should().Be("P3");
                item.Program.Should().Be("Test Program");
                item.Season.Should().Be("Fall 2018");
                item.Status.Should().Be("");
            }
        }

        [TestMethod]
        public void GetDelinquentList_SortIsNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyForFamilyBalanceNoLimitDate();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentFullPaidItems(ClubId, delinquentPolicy)).Returns(FillOrderItems().AsQueryable());

            // Act
            var result = service.GetDelinquentList(ClubId, _delinquentFilterViewModel, null, 1, 50);

            // Assert
            result.Should().NotBeNull();
        }

        [TestMethod]
        public void GetDelinquentList_SortIsNotNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyForFamilyBalanceNoLimitDate();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentFullPaidItems(ClubId, delinquentPolicy)).Returns(FillOrderItems().AsQueryable());

            // Act
            var result = service.GetDelinquentList(ClubId, _delinquentFilterViewModel, new List<Sort> { new Sort { Field = "Id", Dir = SortDirection.Desc } }, 1, 50);

            // Assert
            result[0].RelatedEntityId = 120;
            result[1].RelatedEntityId = 110;
        }

        [TestMethod]
        public void GetDelinquentsTotal_DelinquentPolicyIsNull_ShouldReturnCountBeZero()
        {
            // Arrange
            var service = CreateService();

            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(null as DelinquentPolicy);

            // Act
            var result = service.GetDelinquentsTotal(ClubId);

            // Assert
            var expectedResult = new
            {
                TotalAmount = 0,
                TotalPaidAmount = 0,
                TotalBalanceAmount = 0
            };

            result.Should().BeEquivalentTo(expectedResult);
        }

        [TestMethod]
        public void GetDelinquentsTotal_DelinquentPolicyIsAutoChargeFailAttemptNumber_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyForAutoChargeFailAttemptNumber();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentOrderInstallmentItems(ClubId, delinquentPolicy))
                .Returns(FillDelinquentListForDelinquentPolicyIsAutoChargeFailAttemptNumber().AsQueryable);

            // Act
            var result = service.GetDelinquentsTotal(ClubId);

            // Assert
            var expectedResult = new
            {
                TotalAmount = 350,
                TotalPaidAmount = 150,
                TotalBalanceAmount = 250
            };

            result.Should().BeEquivalentTo(expectedResult);
        }

        [TestMethod]
        public void GetDelinquentsTotal_DelinquentPolicyIsAutoChargeFailAttemptNumberPaidAmountIsNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyForAutoChargeFailAttemptNumber();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentOrderInstallmentItems(ClubId, delinquentPolicy))
                .Returns(FillDelinquentListForDelinquentPolicyIsAutoChargeFailAttemptNumberPaidAmountIsNull().AsQueryable);

            // Act
            var result = service.GetDelinquentsTotal(ClubId);

            // Assert
            var expectedResult = new
            {
                TotalAmount = 350,
                TotalPaidAmount = 50,
                TotalBalanceAmount = 300
            };

            result.Should().BeEquivalentTo(expectedResult);
        }

        [TestMethod]
        public void GetDelinquentsTotal_FamilyBalanceNoLimitDateIsNotNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyForFamilyBalanceNoLimitDate();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentFullPaidItems(ClubId, delinquentPolicy)).Returns(FillOrderItems().AsQueryable());

            // Act
            var result = service.GetDelinquentsTotal(ClubId);

            // Assert
            var expectedResult = new
            {
                TotalAmount = 255.00,
                TotalPaidAmount = 210.30,
                TotalBalanceAmount = 44.70
            };

            result.Should().BeEquivalentTo(expectedResult);
        }

        [TestMethod]
        public void GetDelinquentsTotal_ResultHasBothType_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyBothPolicies();

            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentOrderInstallmentItems(ClubId, delinquentPolicy))
                .Returns(FillDelinquentListForDelinquentPolicyIsAutoChargeFailAttemptNumber().AsQueryable);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentFullPaidItems(ClubId, delinquentPolicy)).Returns(FillOrderItems().AsQueryable());

            // Act
            var result = service.GetDelinquentsTotal(ClubId);

            // Assert
            var expectedResult = new
            {
                TotalAmount = 605.00,
                TotalPaidAmount = 360.30,
                TotalBalanceAmount = 244.70
            };

            result.Should().BeEquivalentTo(expectedResult);
        }

        [TestMethod]
        public void GetDelinquentEmail_WithRecipient_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var createdClub = CreateClubWithDelinquentPolicy();
            var parameters = new DelinquentEmailParameterModel { RelatedEntity = new List<DelinquentEmailParameterRelatedEntityItemModel> { new DelinquentEmailParameterRelatedEntityItemModel { RelatedEntityName = nameof(OrderItem), RelatedEntityId = OrderItemId } }, ClubId = createdClub.Id };

            _mockEmailBusiness.Setup(x => x.GetViewModel(It.Is<EmailCategory>(y => y == EmailCategory.Delinquent), It.Is<DelinquentEmailParameterModel>(y => y.RelatedEntity.First().RelatedEntityName == nameof(OrderItem) && y.RelatedEntity.First().RelatedEntityId == OrderItemId && y.ClubId == createdClub.Id))).Returns(FillEmailViewModel(parameters));

            // Act
            var result = service.GetDelinquentEmail(nameof(OrderItem), OrderItemId, createdClub, UserId);

            // Assert
            JsonHelper.JsonDeserialize<DelinquentEmailParameterModel>(result.Parameters).RelatedEntity.First().RelatedEntityName.Should().Be(nameof(OrderItem));
            JsonHelper.JsonDeserialize<DelinquentEmailParameterModel>(result.Parameters).RelatedEntity.First().RelatedEntityId.Should().Be(OrderItemId);
            JsonHelper.JsonDeserialize<DelinquentEmailParameterModel>(result.Parameters).ClubId.Should().Be(ClubId);
            result.IsCampaignMode.Should().BeTrue();
            result.UserId.Should().Be(UserId);
        }

        [TestMethod]
        public void GetDelinquentEmails_OrderItemIdIsNull_ShouldReturnStatusToFalse()
        {
            // Arrange
            var service = CreateService();
            var createdClub = CreateClubWithDelinquentPolicy();

            // Act
            var result = service.GetDelinquentEmails(null, createdClub, UserId);

            // Assert
            result.Status.Should().BeFalse();
        }

        [TestMethod]
        public void GetDelinquentEmails_WithoutRecipients_ShouldReturnStatusToFalse()
        {
            // Arrange
            var service = CreateService();
            var createdClub = CreateClubWithDelinquentPolicy();

            // Act
            var result = service.GetDelinquentEmails(new List<DelinquentEmailParameterRelatedEntityItemModel>(), createdClub, UserId);

            // Assert
            result.Status.Should().BeFalse();
        }

        [TestMethod]
        public void GetDelinquentEmails_WithRecipients_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var createdClub = CreateClubWithDelinquentPolicy();
            var parameters = new DelinquentEmailParameterModel { RelatedEntity = _orderItemEntities, ClubId = createdClub.Id };

            _mockEmailBusiness.Setup(x => x.GetViewModel(It.Is<EmailCategory>(y => y == EmailCategory.Delinquent), It.Is<DelinquentEmailParameterModel>(y => y.RelatedEntity == _orderItemEntities && y.ClubId == createdClub.Id))).Returns(FillEmailViewModel(parameters));

            // Act
            var result = service.GetDelinquentEmails(_orderItemEntities, createdClub, UserId);

            // Assert
            JsonHelper.JsonDeserialize<DelinquentEmailParameterModel>((result.Data as EmailViewModel)?.Parameters).RelatedEntity[0].RelatedEntityName.Should().Be(_orderItemEntities[0].RelatedEntityName);
            JsonHelper.JsonDeserialize<DelinquentEmailParameterModel>((result.Data as EmailViewModel)?.Parameters).RelatedEntity[0].RelatedEntityId.Should().Be(_orderItemEntities[0].RelatedEntityId);
            JsonHelper.JsonDeserialize<DelinquentEmailParameterModel>((result.Data as EmailViewModel)?.Parameters).RelatedEntity[1].RelatedEntityName.Should().Be(_orderItemEntities[1].RelatedEntityName);
            JsonHelper.JsonDeserialize<DelinquentEmailParameterModel>((result.Data as EmailViewModel)?.Parameters).RelatedEntity[1].RelatedEntityId.Should().Be(_orderItemEntities[1].RelatedEntityId);
            JsonHelper.JsonDeserialize<DelinquentEmailParameterModel>((result.Data as EmailViewModel)?.Parameters).RelatedEntity[2].RelatedEntityName.Should().Be(_orderItemEntities[2].RelatedEntityName);
            JsonHelper.JsonDeserialize<DelinquentEmailParameterModel>((result.Data as EmailViewModel)?.Parameters).RelatedEntity[2].RelatedEntityId.Should().Be(_orderItemEntities[2].RelatedEntityId);
            JsonHelper.JsonDeserialize<DelinquentEmailParameterModel>((result.Data as EmailViewModel)?.Parameters).ClubId.Should().Be(ClubId);
            (result.Data as EmailViewModel)?.IsCampaignMode.Should().BeTrue();
            (result.Data as EmailViewModel)?.UserId.Should().Be(UserId);
            result.Status.Should().BeTrue();
        }

        [TestMethod]
        public void IsDelinquent_DelinquentPolicyIsNull_ShouldReturnFalse()
        {
            // Arrange
            var service = CreateService();

            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(null as DelinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentFullPaidItems(ClubId, null)).Returns(FillOrderItems().AsQueryable());

            // Act
            var result = service.IsDelinquent(UserId, ClubId);

            // Assert
            result.Should().BeFalse();
        }

        [TestMethod]
        public void IsDelinquent_AllowEnrollOnDelinquentIsTrue_ShouldReturnFalse()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyLimitDateGreaterThanNowAndAutoChargeFailAttemptNumberEqualToZero();
            delinquentPolicy.AllowEnrollOnDelinquent = true;

            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentFullPaidItems(ClubId, delinquentPolicy)).Returns(FillOrderItems().AsQueryable());

            // Act
            var result = service.IsDelinquent(UserId, ClubId);

            // Assert
            result.Should().BeFalse();
        }

        [TestMethod]
        public void IsDelinquent_WithoutAnyOrderItem_ShouldReturnFalse()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyLimitDateLowerThanNow();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentFullPaidItems(ClubId, delinquentPolicy)).Returns(new List<OrderItem>().AsQueryable());

            // Act
            var result = service.IsDelinquent(UserId, ClubId);

            // Assert
            result.Should().BeFalse();
        }

        [TestMethod]
        public void IsDelinquent_WithAnyOrderItemWithAnotherUserId_ShouldReturnFalse()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyLimitDateLowerThanNow();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentFullPaidItems(ClubId, delinquentPolicy)).Returns(FillOrderItemsWithAnotherUser().AsQueryable());

            // Act
            var result = service.IsDelinquent(UserId, ClubId);

            // Assert
            result.Should().BeFalse();
        }

        [TestMethod]
        public void IsDelinquent_WithAnyOrderItem_ShouldReturnTrue()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyLimitDateLowerThanNow();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentFullPaidItems(ClubId, delinquentPolicy)).Returns(FillOrderItems().AsQueryable());

            // Act
            var result = service.IsDelinquent(UserId, ClubId);

            // Assert
            result.Should().BeTrue();
        }

        [TestMethod]
        public void IsDelinquent_AutoChargeFailAttemptNumberEqualToZero_ShouldReturnFalse()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyLimitDateGreaterThanNowAndAutoChargeFailAttemptNumberEqualToZero();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);

            // Act
            var result = service.IsDelinquent(UserId, ClubId);

            // Assert
            result.Should().BeFalse();
        }

        [TestMethod]
        public void IsDelinquent_WithoutOrderInstallmentItems_ShouldReturnFalse()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyAutoChargeFailAttemptNumberNotEqualToZero();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentOrderInstallmentItems(ClubId, delinquentPolicy)).Returns(new List<OrderInstallment>().AsQueryable());

            // Act
            var result = service.IsDelinquent(UserId, ClubId);

            // Assert
            result.Should().BeFalse();
        }

        [TestMethod]
        public void IsDelinquent_WithOrderInstallmentItemsForAnotherUser_ShouldReturnFalse()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyAutoChargeFailAttemptNumberNotEqualToZero();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentOrderInstallmentItems(ClubId, delinquentPolicy)).Returns(FillOrderInstallmentsWithUserId(111).AsQueryable());

            // Act
            var result = service.IsDelinquent(UserId, ClubId);

            // Assert
            result.Should().BeFalse();
        }

        [TestMethod]
        public void IsDelinquent_WithOrderInstallmentItemsForUser_ShouldReturnTrue()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyAutoChargeFailAttemptNumberNotEqualToZero();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentOrderInstallmentItems(ClubId, delinquentPolicy)).Returns(FillOrderInstallmentsWithUserId(UserId).AsQueryable());

            // Act
            var result = service.IsDelinquent(UserId, ClubId);

            // Assert
            result.Should().BeTrue();
        }

        [TestMethod]
        public void GetDelinquentNotificationItem_DelinquentPolicyIsNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(null as DelinquentPolicy);

            // Act
            var result = service.GetDelinquentNotificationItem(UserId, CreateClubBaseInfoModel());

            // Assert
            result.Should().BeNull();
        }

        [TestMethod]
        public void GetDelinquentNotificationItem_OrderItemsIsEmpty_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyLimitDateLowerThanNow();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentFullPaidItems(ClubId, delinquentPolicy)).Returns(new List<OrderItem>().AsQueryable());

            // Act
            var result = service.GetDelinquentNotificationItem(UserId, CreateClubBaseInfoModel());

            // Assert
            result.Should().BeNull();
        }

        [TestMethod]
        public void GetDelinquentNotificationItem_OrderItemsIsNotEmptyForAnotherUser_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyLimitDateLowerThanNow();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentFullPaidItems(ClubId, delinquentPolicy)).Returns(FillOrderItems(111).AsQueryable());

            // Act
            var result = service.GetDelinquentNotificationItem(UserId, CreateClubBaseInfoModel());

            // Assert
            result.Should().BeNull();
        }

        [TestMethod]
        public void GetDelinquentNotificationItem_OrderItemsIsNotEmpty_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyLimitDateLowerThanNow();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentFullPaidItems(ClubId, delinquentPolicy)).Returns(FillOrderItems().AsQueryable());

            // Act
            var result = service.GetDelinquentNotificationItem(UserId, CreateClubBaseInfoModel());

            // Assert
            result.Should().NotBeNull();
        }

        [TestMethod]
        public void GetDelinquentNotificationItem_AutoChargeFailAttemptNumberNotZeroOrderInstallmentItemsIsEmpty_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyAutoChargeFailAttemptNumberNotEqualToZero();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentOrderInstallmentItems(ClubId, delinquentPolicy)).Returns(new List<OrderInstallment>().AsQueryable());

            // Act
            var result = service.GetDelinquentNotificationItem(UserId, CreateClubBaseInfoModel());

            // Assert
            result.Should().BeNull();
        }

        [TestMethod]
        public void GetDelinquentNotificationItem_AutoChargeFailAttemptNumberNotZeroOrderInstallmentItemsIsNotEmptyForAnotherUser_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyAutoChargeFailAttemptNumberNotEqualToZero();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentOrderInstallmentItems(ClubId, delinquentPolicy)).Returns(FillOrderInstallmentsWithUserId(111).AsQueryable());

            // Act
            var result = service.GetDelinquentNotificationItem(UserId, CreateClubBaseInfoModel());

            // Assert
            result.Should().BeNull();
        }

        [TestMethod]
        public void GetDelinquentNotificationItem_AutoChargeFailAttemptNumberNotZeroOrderInstallmentItemsIsNotEmpty_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            var delinquentPolicy = CreateDelinquentPolicyAutoChargeFailAttemptNumberNotEqualToZero();
            _mockClubSettingBusiness.Setup(x => x.ReadDelinquentPolicy(ClubId)).Returns(delinquentPolicy);
            _mockOrderItemBusiness.Setup(x => x.GetDelinquentOrderInstallmentItems(ClubId, delinquentPolicy)).Returns(FillOrderInstallmentsWithUserId(UserId).AsQueryable());

            // Act
            var result = service.GetDelinquentNotificationItem(UserId, CreateClubBaseInfoModel());

            // Assert
            result.Should().NotBeNull();
        }

        #endregion

        #region Private methods
        private PeopleBusiness CreateService()
        {
            _mockClubSettingBusiness = new Mock<IClubSettingBusiness>();
            _mockOrderItemBusiness = new Mock<IOrderItemBusiness>();
            _mockEmailBusiness = new Mock<IEmailBusiness>();
            _mockAccountingBusiness = new Mock<IAccountingBusiness>();
            _mockClubBusiness = new Mock<IClubBusiness>();
            _mockPlayerProfileBusiness = new Mock<IPlayerProfileBusiness>();
            _mockProgramSessionBusiness = new Mock<IProgramSessionBusiness>();
            _mockProgramBusiness = new Mock<IProgramBusiness>();

            return new PeopleBusiness(_mockOrderItemBusiness.Object, _mockClubSettingBusiness.Object, _mockEmailBusiness.Object, _mockAccountingBusiness.Object
                , _mockClubBusiness.Object, _mockPlayerProfileBusiness.Object, _mockProgramSessionBusiness.Object, _mockProgramBusiness.Object);
        }

        private static Club CreateClubWithDelinquentPolicy()
        {
            var club = FactoryGirl.Build<Club>();

            club.Id = ClubId;
            club.Domain = "qaclubsand";

            var settings = new ClubSetting
            {
                DelinquentPolicy = new DelinquentPolicy
                {
                    AutoChargeFailAttemptNumber = 4,
                    ManualInstallmentDelayDays = 20,
                    FamilyBalanceNoLimitDate = DateTime.Now,
                    FamilyBalanceMinPrice = 5.00M,
                    AllowEnrollOnDelinquent = false,
                    AllowEnrollTemplate = "",
                    NotAllowEnrollTemplate = "",
                    ExpelChildOnDelinquent = true,
                    ExpelChildTemplate = ""
                }
            };

            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            return club;
        }

        private static ClubBaseInfoModel CreateClubBaseInfoModel()
        {
            return new ClubBaseInfoModel
            {
                Id = ClubId,
                TimeZone = TimeZone.UTC
            };
        }

        private static DelinquentPolicy CreateDelinquentPolicyForAutoChargeFailAttemptNumber() =>
        new DelinquentPolicy
        {
            AutoChargeFailAttemptNumber = 4,
            ManualInstallmentDelayDays = 20,
            FamilyBalanceNoLimitDate = null,
            FamilyBalanceMinPrice = 5.00M,
            AllowEnrollOnDelinquent = false,
            AllowEnrollTemplate = "",
            NotAllowEnrollTemplate = "",
            ExpelChildOnDelinquent = true,
            ExpelChildTemplate = ""
        };

        private static DelinquentPolicy CreateDelinquentPolicyForFamilyBalanceNoLimitDate() =>
        new DelinquentPolicy
        {
            AutoChargeFailAttemptNumber = 0,
            ManualInstallmentDelayDays = 20,
            FamilyBalanceNoLimitDate = DateTime.Now,
            FamilyBalanceMinPrice = 5.00M,
            AllowEnrollOnDelinquent = false,
            AllowEnrollTemplate = "",
            NotAllowEnrollTemplate = "",
            ExpelChildOnDelinquent = true,
            ExpelChildTemplate = ""
        };
        private static DelinquentPolicy CreateDelinquentPolicyLimitDateGreaterThanNowAndAutoChargeFailAttemptNumberEqualToZero() =>
            new DelinquentPolicy
            {
                AutoChargeFailAttemptNumber = 0,
                ManualInstallmentDelayDays = 0,
                FamilyBalanceNoLimitDate = DateTime.Now.AddDays(1),
                FamilyBalanceMinPrice = 5.00M,
                AllowEnrollOnDelinquent = false,
                AllowEnrollTemplate = "",
                NotAllowEnrollTemplate = "",
                ExpelChildOnDelinquent = true,
                ExpelChildTemplate = ""
            };
        private static DelinquentPolicy CreateDelinquentPolicyAutoChargeFailAttemptNumberNotEqualToZero() =>
            new DelinquentPolicy
            {
                AutoChargeFailAttemptNumber = 2,
                ManualInstallmentDelayDays = 0,
                FamilyBalanceNoLimitDate = DateTime.Now.AddDays(1),
                FamilyBalanceMinPrice = 5.00M,
                AllowEnrollOnDelinquent = false,
                AllowEnrollTemplate = "",
                NotAllowEnrollTemplate = "",
                ExpelChildOnDelinquent = true,
                ExpelChildTemplate = ""
            };
        private static DelinquentPolicy CreateDelinquentPolicyLimitDateLowerThanNow() =>
            new DelinquentPolicy
            {
                AutoChargeFailAttemptNumber = 4,
                ManualInstallmentDelayDays = 20,
                FamilyBalanceNoLimitDate = DateTime.Now.AddDays(-1),
                FamilyBalanceMinPrice = 5.00M,
                AllowEnrollOnDelinquent = false,
                AllowEnrollTemplate = "",
                NotAllowEnrollTemplate = "",
                ExpelChildOnDelinquent = true,
                ExpelChildTemplate = ""
            };
        private static DelinquentPolicy CreateDelinquentPolicyBothPolicies() =>
            new DelinquentPolicy
            {
                AutoChargeFailAttemptNumber = 4,
                ManualInstallmentDelayDays = 20,
                FamilyBalanceNoLimitDate = DateTime.Now.AddDays(-1),
                FamilyBalanceMinPrice = 5.00M,
                AllowEnrollOnDelinquent = false,
                AllowEnrollTemplate = "",
                NotAllowEnrollTemplate = "",
                ExpelChildOnDelinquent = true,
                ExpelChildTemplate = ""
            };

        private static EmailViewModel FillEmailViewModel(DelinquentEmailParameterModel parameter) => new EmailViewModel
        {
            From = "hamid@jumbula.com",
            Subject = "Delinquent account",
            Parameters = JsonHelper.JsonSerializer(parameter)
        };

        private static IEnumerable<OrderItem> FillOrderItemsWithAnotherUser()
        {

            return new List<OrderItem>
            {
                FillInstallmentOrderItem(110, 125.00M, 110.15M,new DateTime(2018, 5, 9),111 ,"hamid@jumbula.com","Hamid","Tabatabaei",null),
                FillInstallmentOrderItem(110, 130.00M, 100.15M,new DateTime(2018, 12, 1),112, "hamed@jumbula.com","Hamed","Sadeghi",null)
            };
        }

        private static IEnumerable<OrderItem> FillOrderItems()
        {
            return new List<OrderItem>
            {
                FillInstallmentOrderItem(110, 125.00M, 110.15M,new DateTime(2018, 5, 9),15 ,"hamid@jumbula.com","Hamid","Tabatabaei",null),
                FillInstallmentOrderItem(120, 130.00M, 100.15M,new DateTime(2018, 12, 1),25, "hamed@jumbula.com","Hamed","Sadeghi",null)
            };
        }

        private static IEnumerable<OrderItem> FillOrderItems(int userId)
        {

            return new List<OrderItem>
            {
                FillInstallmentOrderItem(110, 125.00M, 110.15M,new DateTime(2018, 5, 9),userId ,"hamid@jumbula.com","Hamid","Tabatabaei",null),
                FillInstallmentOrderItem(110, 130.00M, 100.15M,new DateTime(2018, 12, 1),userId, "hamed@jumbula.com","Hamed","Sadeghi",null)
            };
        }

        private static IEnumerable<OrderInstallment> FillOrderInstallmentsWithUserId(int userId)
        {
            return new List<OrderInstallment>
            {
                FillInstallments(110, 125.00M, 110.15M,new OrderItem{Order = new Order{User = new JbUser{Id = userId},UserId = userId}}),
                FillInstallments(110, 125.00M, 110.15M,new OrderItem{Order = new Order{User = new JbUser{Id = userId},UserId = userId}})
            };
        }

        private static OrderItem FillInstallmentOrderItem(int id, decimal totalAmount, decimal paidAmount, DateTime orderItemDate, int userId, string emailAddress, string userFirstName, string userLastName, List<OrderInstallment> orderInstallments)
        {
            var club = CreateClubWithDelinquentPolicy();
            var programSchedule = FillProgramSchedule("Test Program");
            var season = FillSeason(SeasonNames.Fall, 2018, "Fall 2018");

            return FillInstallmentOrderItem(id, totalAmount, paidAmount,
                FillOrderInfo(club, FillUserInfo(userId, emailAddress), orderItemDate, 0, OrderStatusCategories.completed, "123-ccc-456"),
                FillPlayerInfo(userFirstName, userLastName), programSchedule, season,
                orderInstallments);
        }

        private static JbUser FillUserInfo(int userId, string emailAddress)
        {
            return new JbUser
            {
                Id = userId,
                Email = emailAddress,
                UserName = emailAddress,
            };
        }

        private static PlayerProfile FillPlayerInfo(string firstName, string lastName)
        {
            return new PlayerProfile { Contact = new ContactPerson { FirstName = firstName, LastName = lastName } };
        }

        private static Order FillOrderInfo(Club club, JbUser user, DateTime completeDate, PaymentPlanType paymentPlanType, OrderStatusCategories orderStatus, string confirmationId)
        {
            return new Order
            {
                Club = club,
                User = user,
                ClubId = club.Id,
                CompleteDate = completeDate,
                PaymentPlanType = paymentPlanType,
                OrderStatus = orderStatus,
                ConfirmationId = confirmationId
            };
        }

        private static ProgramSchedule FillProgramSchedule(string programName)
        {
            return new ProgramSchedule { Program = new Program { Name = programName } };
        }

        private static Season FillSeason(SeasonNames seasonNames, int year, string title)
        {
            return new Season { Name = seasonNames, Year = year, Title = title };
        }

        private static OrderItem FillInstallmentOrderItem(int id, decimal totalAmount, decimal paidAmount, Order order, PlayerProfile player, ProgramSchedule programSchedule, Season season, List<OrderInstallment> orderInstallments)
        {
            return new OrderItem
            {
                Id = id,
                Order = order,
                Player = player,
                ProgramSchedule = programSchedule,
                Season = season,
                TotalAmount = totalAmount,
                PaidAmount = paidAmount,
                Installments = orderInstallments
            };
        }

        private static OrderInstallment FillInstallments(int id, decimal totalAmount, decimal paidAmount, OrderItem orderItem)
        {
            return new OrderInstallment
            {
                Id = id,
                OrderItem = orderItem,
                Status = OrderStatusCategories.completed,
                Amount = totalAmount,
                PaidAmount = paidAmount,
            };
        }

        private IEnumerable<OrderInstallment> FillDelinquentListForAutoChargeFailAttemptNumberIsNotZero()
        {
            return new List<OrderInstallment>
            {
                new OrderInstallment
                {
                    OrderItem = new OrderItem
                    {
                        Order = new Order{User = new JbUser {UserName = "Hamid@Jumbula.com"}, ConfirmationId = "1234567890"},
                        ProgramSchedule = new ProgramSchedule {Program = new Program {Name = "test Winter 2019"}},
                        Season = new Season{Title = "Winter 2019"},
                        Player = new PlayerProfile{Contact = new ContactPerson{FirstName = "Hamid", LastName = "Tabatabaei"}}
                    },
                    AutoChargeAttemps = 2,
                    Amount = 200,
                    PaidAmount = 100
                }
            };
        }

        private IEnumerable<OrderInstallment> FillDelinquentListForDelinquentPolicyIsAutoChargeFailAttemptNumber()
        {
            return new List<OrderInstallment>
            {
                new OrderInstallment
                {
                    OrderItem = new OrderItem
                    {
                        Order = new Order{User = new JbUser {UserName = "Hamid@Jumbula.com"}, ConfirmationId = "1234567890"},
                        ProgramSchedule = new ProgramSchedule {Program = new Program {Name = "test Winter 2019"}},
                        Season = new Season{Title = "Winter 2019"},
                        Player = new PlayerProfile{Contact = new ContactPerson{FirstName = "Hamid", LastName = "Tabatabaei"}}
                    },
                    AutoChargeAttemps = 2,
                    Amount = 200,
                    PaidAmount = 100
                },
                new OrderInstallment
                {
                    OrderItem = new OrderItem
                    {
                        Order = new Order{User = new JbUser {UserName = "Hamid@Jumbula.com"}, ConfirmationId = "1234567890"},
                        ProgramSchedule = new ProgramSchedule {Program = new Program {Name = "test Winter 2019"}},
                        Season = new Season{Title = "Winter 2019"},
                        Player = new PlayerProfile{Contact = new ContactPerson{FirstName = "Hamid", LastName = "Tabatabaei"}}
                    },
                    AutoChargeAttemps = 2,
                    Amount = 150,
                    PaidAmount = 50
                }
            };
        }

        private IEnumerable<OrderInstallment> FillDelinquentListForDelinquentPolicyIsAutoChargeFailAttemptNumberPaidAmountIsNull()
        {
            return new List<OrderInstallment>
            {
                new OrderInstallment
                {
                    OrderItem = new OrderItem
                    {
                        Order = new Order{User = new JbUser {UserName = "Hamid@Jumbula.com"}, ConfirmationId = "1234567890"},
                        ProgramSchedule = new ProgramSchedule {Program = new Program {Name = "test Winter 2019"}},
                        Season = new Season{Title = "Winter 2019"},
                        Player = new PlayerProfile{Contact = new ContactPerson{FirstName = "Hamid", LastName = "Tabatabaei"}}
                    },
                    AutoChargeAttemps = 2,
                    Amount = 200,
                    PaidAmount = null
                },
                new OrderInstallment
                {
                    OrderItem = new OrderItem
                    {
                        Order = new Order{User = new JbUser {UserName = "Hamid@Jumbula.com"}, ConfirmationId = "1234567890"},
                        ProgramSchedule = new ProgramSchedule {Program = new Program {Name = "test Winter 2019"}},
                        Season = new Season{Title = "Winter 2019"},
                        Player = new PlayerProfile{Contact = new ContactPerson{FirstName = "Hamid", LastName = "Tabatabaei"}}
                    },
                    AutoChargeAttemps = 2,
                    Amount = 150,
                    PaidAmount = 50
                }
            };
        }

        #endregion 
    }
}
