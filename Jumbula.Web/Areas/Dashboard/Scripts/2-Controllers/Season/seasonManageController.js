﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('seasonManageController', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'seasons-view';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;

        $scope.baseUrl = window.location.origin + "/Dashboard/Season";
        $scope.title = $scope.MC.stateParams.manageAction + " season";

        if ($scope.MC.stateParams.manageAction == 'Delete')
        {
            $http.get( $scope.baseUrl+"/CanDeleteSeason?seasonDomain="+ $scope.MC.stateParams.seasonDomain).success(function (data, status, headers, config) {
                $scope.canDelete = data;

            });
        }
        $scope.cancelPublish = function () {
            $state.go('SeasonSetup', { seasonDomain: $scope.MC.stateParams.seasonDomain })
        }
        $scope.publishSeason = function () {
           
            $http.post($scope.baseUrl + '/PublishSeason', { seasonDomain: $scope.MC.stateParams.seasonDomain })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $scope.MC.getSeasons();
                        $state.go('Seasons');
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        };
        $scope.deleteSeason = function () {

            $http.post($scope.baseUrl + '/Delete', { seasonDomain: $scope.MC.stateParams.seasonDomain })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $scope.MC.getSeasons();
                        $state.go('Seasons');
                        $scope.MC.getClubbaseInfo();
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        };

      
    }]);
})();