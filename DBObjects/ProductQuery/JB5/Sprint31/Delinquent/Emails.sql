/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Emails ADD
	TemplateId int NULL,
	UserId int NULL,
	IsCampaignMode bit NOT NULL CONSTRAINT DF_Emails_IsCampaignMode1 DEFAULT ((0))
GO
ALTER TABLE dbo.Emails SET (LOCK_ESCALATION = TABLE)


ALTER TABLE [dbo].[Emails]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Emails_dbo.EmailTemplates_TemplateId] FOREIGN KEY([TemplateId])
REFERENCES [dbo].[EmailTemplates] ([Id])
GO

ALTER TABLE [dbo].[Emails] CHECK CONSTRAINT [FK_dbo.Emails_dbo.EmailTemplates_TemplateId]
GO

ALTER TABLE [dbo].[Emails]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Emails_dbo.JbUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[JbUsers] ([Id])
GO

ALTER TABLE [dbo].[Emails] CHECK CONSTRAINT [FK_dbo.Emails_dbo.JbUsers_UserId]
GO
COMMIT
