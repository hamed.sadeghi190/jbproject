﻿using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class   ParentBillingMethodsItemViewModel : BaseViewModel<UserCreditCard>
    {    
        public ParentBillingMethodsItemViewModel()
        {

        }
        public ParentBillingMethodsItemViewModel(string planType)
        {
        }
     
        public string CardNumber { get; set; }

        public string Last4Digit { get; set; }

         public int Year2Char { get; set; }

        public string Method
        {
            get
            {
                return string.Format("{0} ***{1}", Brand, Last4Digit);
            }
        }
        public string Brand { get; set; }
        public string ExpiryDate
        {
            get { return string.Format("{0}/{1}", Month, Year); }
        }

        public string CVV { get; set; }        

        public string Year { get; set; }

        public string Month { get; set; }

        public string CardHolderName { get; set; }

        public string Address { get; set; }

        public bool IsDefault { get; set; }
 
        public string CardId { get; set; }

        public long UserId { get; set; }

        public bool HasInstallment { get; set; }

        public bool IsLast { get; set; }
        
    }
}
