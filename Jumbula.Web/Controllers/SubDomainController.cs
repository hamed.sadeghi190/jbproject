﻿using System;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.WebLogic.UserService;
using SportsClub.Models;

namespace Jumbula.Web.Controllers
{
    public class SubDomainController : JbBaseController
    {
        #region Fields
        private readonly IClubSettingBusiness _clubSettingBusiness;
        #endregion

        #region Constractors
        public SubDomainController(IClubSettingBusiness clubSettingBusiness)
        {
            _clubSettingBusiness = clubSettingBusiness;
        }
        #endregion

        // GET: SubDomain
        [AllowAnonymous]
        public virtual ActionResult DisplayExpiredMessage(string domain, string clubDomain, string expiration)
        {
            ViewBag.Domain = domain;
            ViewBag.ClubDomain = clubDomain;
            ViewBag.Expiration = expiration;

            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult DisplayNotStartedMessage(string domain, string clubDomain, DateTime registerStartDate)
        {

            ViewBag.RegisterStartDate = registerStartDate;

            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult DisplayClosedSeasonMessage(int clubId, string seasonName)
        {
            var club = Ioc.ClubBusiness.Get(clubId);
            var email = string.Empty;

            if (club.PartnerId == null)
            {
                email = club.ContactPersons.First().Email;
            }
            else
            {
                email = club.PartnerClub.ContactPersons.First().Email;
            }

            ViewBag.SeasonName = seasonName;
            ViewBag.ClubEmail = email;

            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult DisplayNotOpenedSeasonMessage(string seasonName, DateTime registerStartDate)
        {
            ViewBag.SeasonName = seasonName;
            ViewBag.RegisterStartDate = registerStartDate;

            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult DisplayGeneralClosedSeasonMessage(string seasonName, DateTime lateRegisterStartDate, DateTime? lateRegisterEndDate, decimal lateRegistrationFee)
        {
            ViewBag.SeasonName = seasonName;
            ViewBag.LateRegisterStartDate = lateRegisterStartDate;
            ViewBag.LateRegisterEndDate = lateRegisterEndDate;
            ViewBag.LateRegistrationFee = lateRegistrationFee;

            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult DisplayEarlyClosedSeasonMessage(string seasonName, DateTime generalRegisterStartDate)
        {
            ViewBag.SeasonName = seasonName;
            ViewBag.GeneralRegisterStartDate = generalRegisterStartDate;

            return View();
        }

        [HttpGet]
        public virtual ActionResult DisplayLiveOrTestProgramMessage(string programDomain, string seasonDomain, bool isLive)
        {


            ViewBag.IsLive = isLive;

            return View();
        }
        [HttpGet]
        public virtual ActionResult PinRestriction(string domain, string seasonDomain, string clubDomain)
        {
            var clubId = Ioc.ClubBusiness.Get(clubDomain).Id;
            var program = Ioc.ProgramBusiness.Get(clubId, seasonDomain, domain);
            ViewBag.RegisterPINMessage = program.RegisterPINMessage;

            return View();
        }

        [HttpPost]
        public virtual ActionResult PinRestriction(string domain, string seasonDomain, string clubDomain, string pin)
        {
            var clubId = Ioc.ClubBusiness.Get(clubDomain).Id;
            var userName = this.GetCurrentUserName();
            var program = Ioc.ProgramBusiness.Get(clubId, seasonDomain, domain);
            if (!string.IsNullOrEmpty(pin) && program.HasRegisterPIN && program.RegisterPIN.Equals(pin, System.StringComparison.CurrentCultureIgnoreCase))
            {
                Response.SetCookie((new System.Web.HttpCookie((clubDomain + seasonDomain + domain + this.GetCurrentUserName() + "Pin").ToLower(), pin) { Expires = DateTime.Now.AddHours(1) }));
                if (this.GetCurrentRequestType() == RequestType.NotBranded)
                    return Redirect(string.Format("/{0}/{1}/{2}/Register", clubDomain, seasonDomain, domain));
                else
                    return Redirect(string.Format("/{0}/{1}/Register", seasonDomain, domain));

            }
            else if (string.IsNullOrEmpty(pin))
            {
                ViewBag.ErrorMessage = "Access password is required";
            }
            else
            {
                ViewBag.ErrorMessage = "Access password is invalid";
            }

            ViewBag.RegisterPINMessage = program.RegisterPINMessage;

            return View();
        }

        [HttpGet]
        public virtual ActionResult SeasonPinRestriction(string seasonDomain, string clubDomain)
        {
            var clubId = Ioc.ClubBusiness.Get(clubDomain).Id;

            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);
            var seasonRegStatus = Ioc.SeasonBusiness.GetRegStatus(season);
            var seasonSettings = season.Setting;

            switch (seasonRegStatus)
            {
                case SeasonRegStatus.EarlyOpen:
                    {
                        if (seasonSettings.SeasonProgramInfoSetting.HasEarlyRegisterPIN)
                        {
                            ViewBag.RegisterPINMessage = seasonSettings.SeasonProgramInfoSetting.EarlyRegisterPINMessage;
                        }
                    }
                    break;
                case SeasonRegStatus.GeneralOpen:
                    {
                        if (seasonSettings.SeasonProgramInfoSetting.HasGeneralRegisterPIN)
                        {
                            ViewBag.RegisterPINMessage = seasonSettings.SeasonProgramInfoSetting.GeneralRegisterPINMessage;
                        }
                    }
                    break;
                case SeasonRegStatus.LateOpen:
                    {
                        if (seasonSettings.SeasonProgramInfoSetting.HasLateRegisterPIN)
                        {
                            ViewBag.RegisterPINMessage = seasonSettings.SeasonProgramInfoSetting.LateRegisterPINMessage;
                        }
                    }
                    break;
                default:
                    break;
            }

            return View();
        }

        [HttpPost]
        public virtual ActionResult SeasonPinRestriction(string seasonDomain, string clubDomain, string pin)
        {
            var clubId = Ioc.ClubBusiness.Get(clubDomain).Id;
            var userName = this.GetCurrentUserName();
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);
            var seasonRegStatus = Ioc.SeasonBusiness.GetRegStatus(season);
            var seasonSettings = season.Setting;

            var seasonRegsterPin = string.Empty;
            var hasRegisterPin = false;
            var registerPinMessage = string.Empty;

            switch (seasonRegStatus)
            {
                case SeasonRegStatus.EarlyOpen:
                    {
                        if (seasonSettings.SeasonProgramInfoSetting.HasEarlyRegisterPIN)
                        {
                            hasRegisterPin = seasonSettings.SeasonProgramInfoSetting.HasEarlyRegisterPIN;
                            seasonRegsterPin = seasonSettings.SeasonProgramInfoSetting.EarlyRegisterPIN;
                            registerPinMessage = seasonSettings.SeasonProgramInfoSetting.EarlyRegisterPINMessage;
                        }
                    }
                    break;
                case SeasonRegStatus.GeneralOpen:
                    {
                        if (seasonSettings.SeasonProgramInfoSetting.HasGeneralRegisterPIN)
                        {
                            hasRegisterPin = seasonSettings.SeasonProgramInfoSetting.HasGeneralRegisterPIN;
                            seasonRegsterPin = seasonSettings.SeasonProgramInfoSetting.GeneralRegisterPIN;
                            registerPinMessage = seasonSettings.SeasonProgramInfoSetting.GeneralRegisterPINMessage;
                        }
                    }
                    break;
                case SeasonRegStatus.LateOpen:
                    {
                        if (seasonSettings.SeasonProgramInfoSetting.HasLateRegisterPIN)
                        {
                            hasRegisterPin = seasonSettings.SeasonProgramInfoSetting.HasLateRegisterPIN;
                            seasonRegsterPin = seasonSettings.SeasonProgramInfoSetting.LateRegisterPIN;
                            registerPinMessage = seasonSettings.SeasonProgramInfoSetting.LateRegisterPINMessage;
                        }
                    }
                    break;
                default:
                    break;
            }

            if (!string.IsNullOrEmpty(pin) && hasRegisterPin && seasonRegsterPin.Equals(pin, StringComparison.CurrentCultureIgnoreCase))
            {
                Response.SetCookie((new System.Web.HttpCookie((clubDomain + seasonDomain + this.GetCurrentUserName() + "Pin").ToLower(), pin) { Expires = DateTime.Now.AddHours(1) }));

                return Redirect(string.Format("/{0}/Register", seasonDomain));
            }
            else if (string.IsNullOrEmpty(pin))
            {
                ViewBag.ErrorMessage = "Access password is required";
            }
            else
            {
                ViewBag.ErrorMessage = "Password is not valid. Please confirm the correct password and try again.";
            }

            ViewBag.RegisterPINMessage = registerPinMessage;

            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult DisplayFrozenMessage(string domain, string clubDomain)
        {
            ViewBag.Domain = domain;
            ViewBag.ClubDomain = clubDomain;

            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult DisplayDeletedMessage(string domain, string clubDomain)
        {
            ViewBag.Domain = domain;
            ViewBag.ClubDomain = clubDomain;

            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult DisplayDelinquentMessage()
        {
            ViewBag.DelinquentPolicyNotAllowEnrollTemplate = _clubSettingBusiness.ReadDelinquentPolicy(this.GetCurrentClubId()).NotAllowEnrollTemplate;

            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult DisplayDraftMessage(string domain, string clubDomain)
        {
            ViewBag.Domain = domain;
            ViewBag.ClubDomain = clubDomain;

            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult DisplayCompleteCapacityMessage(int programId, string programDomain, string seasonDomain, bool isWaitListAvailable, bool disableCapacityRestriction)
        {
            ViewBag.SeasonDomain = seasonDomain;
            ViewBag.ProgramId = programId;
            ViewBag.ProgramDomain = programDomain;
            ViewBag.IsWaitListAvailable = isWaitListAvailable;
            ViewBag.DisableCapacityRestriction = disableCapacityRestriction;

            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult DisplayNoSessionMessage()
        {
            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult DisplayNotAccessibleMessage(RoleCategory role, string message, string returnUrl = "")
        {
            if (string.IsNullOrEmpty(message))
            {
                ViewBag.Message = string.Format("This page is not accessible for {0} role.", role.ToString());
            }
            else
            {
                ViewBag.Message = message;
            }
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [ChildActionOnly]
        [AllowAnonymous]
        public virtual ActionResult GetNotificationWithModel()
        {
            var notifyModel = TempData["NotificationModel"];
            if (notifyModel != null)
            {
                var model = (NotificationItemViewModel)notifyModel;
                return PartialView("_Notification", model);
            }

            return PartialView("_Notification", new NotificationItemViewModel());
        }
    }
}