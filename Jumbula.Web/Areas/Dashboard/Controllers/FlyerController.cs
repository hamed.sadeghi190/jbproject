﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Infrastructure.Flyer;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.WebLogic.UserService;
using Newtonsoft.Json;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class FlyerController : DashboardBaseController
    {
        private readonly ISeasonBusiness _seasonBusiness;
        private readonly IProgramBusiness _programBusiness;
        public FlyerController(ISeasonBusiness seasonBusiness, IProgramBusiness programBusiness)
        {
            _seasonBusiness = seasonBusiness;
            _programBusiness = programBusiness;
        }
        public virtual ActionResult FlyerTemplates()
        {
            return View();
        }

        public virtual ActionResult ClubFlyers()
        {
            return View();
        }
        public virtual ActionResult Template1Preview()
        {
            return View();
        }
        public virtual ActionResult ClassCatalogReportFlyerPreview()
        {
            return View();
        }
        public virtual ActionResult FillFlyerData()
        {
            return View();
        }
        public virtual ActionResult FlyersSetting()
        {
            return View();
        }
        [HttpGet]
        public virtual ActionResult EditFlyer()
        {
            return View();
        }

        [HttpGet]
        public virtual ActionResult CreateEditFlyer()
        {
            var model = new FlyerTemplate1ViewModel();
            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);
            model.Email = GetFlyersSetting(club).Email;
            model.Phone = GetFlyersSetting(club).Phone;
            model.IsGreenColor = club.Domain.Equals("rightatschool") ? false : true;
            model.IsBlueColor = club.Domain.Equals("rightatschool") ? true : false;
            return JsonNet(new { Data = model });
        }

        [HttpPost]
        public virtual ActionResult SaveFlyer(FlyerTemplate1ViewModel model, byte typeFlyer)
        {
            var programSchedules = new List<FlyerProgramItemViewModel>();
            var seasonInfo = Ioc.SeasonBusiness.Get(model.Body.SeasonId);

            var programsOfDayPM = new List<FlyerProgramItemViewModel>();
            var programsOfDayAM = new List<FlyerProgramItemViewModel>();

            programsOfDayPM = BuildScheduleDictionary(model.Body.ClubId, seasonInfo, "PM");
            programsOfDayAM = BuildScheduleDictionary(model.Body.ClubId, seasonInfo, "AM");


            var maxTempPM = programsOfDayPM.Count > 0 ? programsOfDayPM.Max(p => p.Details.Count()) : 0;
            var maxTempAM = programsOfDayAM.Count > 0 ? programsOfDayAM.Max(p => p.Details.Count()) : 0;

            programsOfDayPM = FillEmptyCell(maxTempPM, programsOfDayPM); //create Empty cell by max program in schedule
            programsOfDayAM = FillEmptyCell(maxTempAM, programsOfDayAM);

            model.Body.ProgramSchedulesPM = programsOfDayPM;
            model.Body.ProgramSchedulesAM = programsOfDayAM;


            var newTemplate = new JbFlyer()
            {
                FlyerContent = JsonConvert.SerializeObject(model),
                Name = model.Name,
                FlyerType = typeFlyer
            };

            var result = Ioc.JbFlyerBusiness.SaveFlyer(newTemplate);


            ClubFlyer clubFlyer = new ClubFlyer();
            clubFlyer.SeasonId = model.Body.SeasonId;
            clubFlyer.FlyerId = newTemplate.Id;
            clubFlyer.ClubId = model.Body.ClubId;


            var resultClubFlyer = Ioc.ClubFlyerBusiness.Create(clubFlyer);

            if (result.Status && resultClubFlyer.Status)
            {
                result.Status = true;
            }
            else
            {
                result.Status = false;
            }
            return JsonNet(new JResult { Status = result.Status, Data = newTemplate.Id.ToString() });


        }

        [HttpPost]
        public virtual ActionResult SaveFlyerClassCatalog(FlyerClassCatalogViewModel model, byte typeFlyer)
        {
            var seasonInfo = Ioc.SeasonBusiness.Get(model.Body.SeasonId);
            var flyerPrograms = new List<FlyerProgramItemDetailViewModel>();
            flyerPrograms = BuildPrograms(model.Body.ClubId, seasonInfo);

            var newTemplate = new JbFlyer()
            {
                FlyerContent = JsonConvert.SerializeObject(model),
                Name = model.Name,
                FlyerType = typeFlyer
            };

            var result = Ioc.JbFlyerBusiness.SaveFlyer(newTemplate);


            ClubFlyer clubFlyer = new ClubFlyer();
            clubFlyer.SeasonId = model.Body.SeasonId;
            clubFlyer.FlyerId = newTemplate.Id;
            clubFlyer.ClubId = model.Body.ClubId;


            var resultClubFlyer = Ioc.ClubFlyerBusiness.Create(clubFlyer);

            if (result.Status && resultClubFlyer.Status)
            {
                result.Status = true;
            }
            else
            {
                result.Status = false;
            }
            return JsonNet(new JResult { Status = result.Status, Data = newTemplate.Id.ToString() });


        }

        public virtual JsonNetResult GetAllFlyers(List<GridSort> sort = null)
        {
            var termValue = Request.Params["term"] ?? string.Empty;

            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);

            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

            var newAllFlyersId = new List<int>();

            var allFlyers = new List<ClubFlyer>();
            if (club.IsPartner)
            {
                allFlyers = Ioc.ClubFlyerBusiness.GetPartnerFlyerList(club.Id);
            }

            var data = allFlyers.OrderByDescending(c => c.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList();

            var finalData = data.Select(clubFlyer => new AllFlyers
            {
                Name = clubFlyer.Flyer.Name,
                SeasonName = clubFlyer.Season.Name != SeasonNames.SelectSeason ? Jumbula.Common.Helper.EnumHelper.ToDescription(clubFlyer.Season.Name) : null,
                SchoolName = clubFlyer.Club.Name,
                Id = clubFlyer.Flyer.Id

            }).ToList();


            var total = allFlyers.Count();
            var dataSource = finalData;

            return JsonNet(new { DataSource = dataSource, TotalCount = total });
        }

        public virtual JsonNetResult GetAllTemplates(List<GridSort> sort = null)
        {

            List<FlyerTemplates> finalData = new List<FlyerTemplates>();

            var club = ActiveClub;

            var today = DateTime.UtcNow.AddMinutes(20);
            var dateTime = Ioc.ClubBusiness.GetClubDateTime(ActiveClub.Id, today);

            if (club.Domain.Equals("flexacademies", StringComparison.OrdinalIgnoreCase))
            {
                finalData.Add(new FlyerTemplates
                {
                    Name = "After school classes",
                    Id = 1
                });

                finalData.Add(new FlyerTemplates
                {
                    Name = "Class catalog",
                    Id = 2
                });
            }

            if (club.Domain.Equals("rightatschool", StringComparison.OrdinalIgnoreCase))
            {
                finalData.Add(new FlyerTemplates
                {
                    Name = "After school classes",
                    Id = 3
                });

                finalData.Add(new FlyerTemplates
                {
                    Name = "Class catalog",
                    Id = 4
                });
            }

            if (club.Domain.Equals("boomacademies", StringComparison.OrdinalIgnoreCase))
            {
                finalData.Add(new FlyerTemplates
                {
                    Name = "After school classes",
                    Id = 5
                });
            }

            var dataSource = finalData;

            return JsonNet(new { DataSource = dataSource, TotalCount = 5 });
        }


        public virtual FileResult Generate(int id)
        {

            var jbFlyer = Ioc.JbFlyerBusiness.Get(id);


            var flyerGenerator = new FlyerGenerator();
            byte[] file = null;

            if (jbFlyer.FlyerType == 1) //Generate Pdf for Template
            {
                var model = JsonConvert.DeserializeObject<FlyerClassCatalogViewModel>(jbFlyer.FlyerContent);
                file = flyerGenerator.GenerateFromView(this.ControllerContext, "TemplateClassCatalog1", model);
            }
            else if (jbFlyer.FlyerType == 2)
            {
                var model = JsonConvert.DeserializeObject<FlyerTemplate1ViewModel>(jbFlyer.FlyerContent);
                file = flyerGenerator.GenerateFromView(this.ControllerContext, "TemplateAfterSchoolClasses1", model);
            }
            else if (jbFlyer.FlyerType == 3)
            {
                var model = JsonConvert.DeserializeObject<FlyerTemplate1ViewModel>(jbFlyer.FlyerContent);
                file = flyerGenerator.GenerateFromView(this.ControllerContext, "TemplateAfterSchoolClasses2", model);
            }
            else if (jbFlyer.FlyerType == 4)
            {
                var model = JsonConvert.DeserializeObject<FlyerClassCatalogViewModel>(jbFlyer.FlyerContent);
                file = flyerGenerator.GenerateFromView(this.ControllerContext, "TemplateClassCatalog2", model);
            }
            else if (jbFlyer.FlyerType == 5)
            {
                var model = JsonConvert.DeserializeObject<FlyerTemplate1ViewModel>(jbFlyer.FlyerContent);
                file = flyerGenerator.GenerateFromView(this.ControllerContext, "TemplateAfterSchoolClasses3", model);
            }

            var flyerName = string.Format("{0}{1}", jbFlyer.Name, ".pdf");
            return File(file, "application/pdf", flyerName);
        }

        public virtual JsonNetResult GetAllSchool()
        {
            var club = ActiveClub;

            var schools =
                Ioc.ClubBusiness.GetRelatedClubs(club.Id)
                    .Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.School)
                    .Select(s => new SelectKeyValue<long>() { Text = s.Name, Value = s.Id })
                    .OrderBy(s => s.Text).ToList();

            return JsonNet(schools);
        }

        [HttpGet]
        public virtual JsonNetResult GetClubSeasons(int clubId, string term, bool addPlaceHolder = false)
        {

            var allSeasons = Ioc.SeasonBusiness.GetList(clubId);
            var termValue = Request.Params["term"] ?? string.Empty;
            if (!string.IsNullOrEmpty(termValue))
            {
                allSeasons = allSeasons.Where(c => c.Title.StartsWith(termValue));
            }
            var model = new List<SelectKeyValue<long>>();
            if (addPlaceHolder)
            {
                model.Add(new SelectKeyValue<long>() { Text = "Select season ...", Value = 0 });
            }
            model.AddRange(allSeasons.OrderByDescending(c => c.MetaData.DateCreated).ToList().Select(c => new SelectKeyValue<long>()
            {
                Text = c.Title,
                Value = c.Id
            }).ToList());

            return JsonNet(model, new JsonSerializerSettings());
        }

        [HttpGet]
        public virtual ActionResult GetDataFlyer(int clubId, int seasonId, string name, string Email, string Phone, string Comments, bool IsGreenColor, bool IsBlueColor, string term, bool addPlaceHolder = false)
        {
            if (ModelState.IsValid)
            {
                var modelSetting = new FlyersSettingsViewModel();

                var programsOfDayPM = new List<FlyerProgramItemViewModel>();
                var programsOfDayAM = new List<FlyerProgramItemViewModel>();

                var clubInfo = Ioc.ClubBusiness.Get(clubId);
                var partnerInfo = Ioc.ClubBusiness.Get(this.GetCurrentClubId());
                var season = Ioc.SeasonBusiness.Get(seasonId);
                var programBusiness = Ioc.ProgramBusiness;
                var clubLogo = "";
                var partnerLogo = "";

                modelSetting = GetFlyersSetting(clubInfo);
                if (season.Setting != null)
                {
                    var programInfo = season.Setting.SeasonProgramInfoSetting;
                }

                TimeSpan timePM = new TimeSpan(12, 0, 0); //pm
                TimeSpan timeAM = new TimeSpan(0, 0, 0); //AM

                clubLogo = UrlHelpers.GetClubLogoUrl(clubInfo.Domain, clubInfo.Logo);
                partnerLogo = UrlHelpers.GetClubLogoUrl(partnerInfo.Domain, partnerInfo.Logo);

                programsOfDayPM = BuildScheduleDictionary(clubId, season, "PM");
                programsOfDayAM = BuildScheduleDictionary(clubId, season, "AM");


                var maxTempPM = programsOfDayPM.Count > 0 ? programsOfDayPM.Max(p => p.Details.Count()) : 0;
                var maxTempAM = programsOfDayAM.Count > 0 ? programsOfDayAM.Max(p => p.Details.Count()) : 0;

                programsOfDayPM = FillEmptyCell(maxTempPM, programsOfDayPM); //create Empty cell by max program in schedule
                programsOfDayAM = FillEmptyCell(maxTempAM, programsOfDayAM);

                var programs = Ioc.ProgramBusiness.GetList(clubId, season.Domain).Where(p => p.Status == ProgramStatus.Open).ToList().Where(p => p.SaveType == SaveType.Publish).ToList();

                var minTimeAM = programs.Count != 0 ? programs.SelectMany(p => programBusiness.GetClassDays(p).Where(pr => pr.StartTime.HasValue && pr.StartTime >= timeAM && pr.StartTime < timePM)).Min(pr => pr.StartTime) : null;
                var maxTimeAM = programs.Count != 0 ? programs.SelectMany(p => programBusiness.GetClassDays(p).Where(pr => pr.EndTime.HasValue && pr.EndTime >= timeAM && pr.EndTime < timePM)).Max(pr => pr.EndTime) : null;

                var minTimePM = programs.Count != 0 ? programs.SelectMany(p => programBusiness.GetClassDays(p).Where(pr => pr.StartTime.HasValue && pr.StartTime >= timePM)).Min(pr => pr.StartTime) : null;
                var maxTimePM = programs.Count != 0 ? programs.SelectMany(p => programBusiness.GetClassDays(p).Where(pr => pr.EndTime.HasValue && pr.EndTime >= timePM)).Max(pr => pr.EndTime) : null;


                var startSeason = "";
                var endSeason = "";
                decimal lateFee = 0;
                DateTime lateRegistrationOpenDate = DateTime.UtcNow;
                DateTime lateRegistrationOpenTime = DateTime.UtcNow;

                if (season.Setting != null)
                {
                    var programInfo = season.Setting.SeasonProgramInfoSetting;
                    if (programInfo != null)
                    {
                        startSeason = programInfo.StartDate != null ? programInfo.StartDate.Value.ToString("MMM d").ToUpper() : "";
                        endSeason = programInfo.EndDate != null ? programInfo.EndDate.Value.ToString("MMM d").ToUpper() : "";
                        lateFee = programInfo.LateRegistrationFee;

                        if (programInfo.LateRegistrationOpens.HasValue)
                        {
                            lateRegistrationOpenDate = programInfo.LateRegistrationOpens.Value.Date;
                            lateRegistrationOpenTime = new DateTime(programInfo.LateRegistrationOpens.Value.Ticks);
                        }
                    }

                }

                var schoolDomain = clubInfo.Domain;

                var model = new FlyerTemplate1ViewModel()
                {
                    IsGreenColor = partnerInfo.Domain.Equals("rightatschool") ? false : true,
                    IsBlueColor = partnerInfo.Domain.Equals("rightatschool") ? true : false,
                    Name = name,
                    Header = new BaseFlyerHeaderViewModel()
                    {
                        SchoolLogo = clubLogo,
                        PartnerLogo = partnerLogo
                    },
                    Body = new FlyerTemplate1BodyViewModel()
                    {
                        SchoolName = partnerInfo.Domain.Equals("rightatschool") ? clubInfo.Name : clubInfo.Name.ToUpper(),
                        SchoolDomain = schoolDomain,
                        SeasonName = season.Name != SeasonNames.SelectSeason ? partnerInfo.Domain.Equals("rightatschool") ? season.Name.ToDescription() : season.Name.ToDescription().ToUpper() : null,
                        RegistrationOpenDate = _seasonBusiness.GetGeneralRegOpenDate(season) != null ? DateTimeHelper.FormatShortDateWithoutYear(_seasonBusiness.GetGeneralRegOpenDate(season).Value) : string.Empty,
                        ClubId = clubId,
                        CountPMAndAM = CountPMAndAM(programsOfDayPM, programsOfDayAM),
                        SeasonId = seasonId,
                        SeasonDate = !string.IsNullOrEmpty(startSeason) && !string.IsNullOrEmpty(startSeason) ? $"{startSeason} - {endSeason}" : string.Empty,
                        ProgramSchedulesAM = programsOfDayAM,
                        ProgramSchedulesPM = programsOfDayPM,
                        ClubUrl = GenerateClubUrl(schoolDomain),
                        Comments = Comments,
                        GeneralRegistrationOpens = _seasonBusiness.GetGeneralRegOpenDate(season) != null ? _seasonBusiness.GetGeneralRegOpenDate(season).Value.ToString(Constants.F_DateTimeWithAMPM_AT).ToUpper() : string.Empty,
                        GeneralRegistrationCloses = _seasonBusiness.GetGeneralRegCloseDate(season) != null ? _seasonBusiness.GetGeneralRegCloseDate(season).Value.ToString(Constants.F_DateTimeWithAMPM_AT).ToUpper() : string.Empty,
                        MinMaxTimeAM = minTimeAM != null && maxTimeAM != null ? string.Format("{0} TO {1}", DateTime.Today.Add(minTimeAM.Value).ToString("h:mmtt"), DateTime.Today.Add(maxTimeAM.Value).ToString("h:mmtt")) : null,
                        MinMaxTimePM = minTimePM != null && maxTimePM != null ? string.Format("{0} TO {1}", DateTime.Today.Add(minTimePM.Value).ToString("h:mmtt"), DateTime.Today.Add(maxTimePM.Value).ToString("h:mmtt")) : null,
                    },


                    Footer = new BaseFlyerFooterViewModel()
                    {
                        LateFee = lateFee,
                        Email = modelSetting.Email,
                        Phone = modelSetting.Phone,
                        lateRegistrationOpenDateTime = string.Format("{0} {1}", lateRegistrationOpenDate.ToString("MMM d"), lateRegistrationOpenTime.ToString("h:mmtt"))
                    },
                };


                return JsonNet(model, new JsonSerializerSettings());
            }

            return base.JsonFormResponse();

        }

        [HttpGet]
        public virtual ActionResult GetDataFlyerClassCatalog(int clubId, int seasonId, string name, string Phone, string term, bool addPlaceHolder = false)
        {
            if (ModelState.IsValid)
            {
                var modelSetting = new FlyersSettingsViewModel();
                var flyerPrograms = new List<FlyerProgramItemDetailViewModel>();

                var clubInfo = Ioc.ClubBusiness.Get(clubId);
                var partnerInfo = Ioc.ClubBusiness.Get(this.GetCurrentClubId());
                modelSetting = GetFlyersSetting(clubInfo);

                var season = Ioc.SeasonBusiness.Get(seasonId);
                var programBusiness = Ioc.ProgramBusiness;
                var clubLogo = "";
                var partnerLogo = "";

                if (season.Setting != null)
                {
                    var programInfo = season.Setting.SeasonProgramInfoSetting;
                }

                clubLogo = UrlHelpers.GetClubLogoUrl(clubInfo.Domain, clubInfo.Logo);
                partnerLogo = UrlHelpers.GetClubLogoUrl(partnerInfo.Domain, partnerInfo.Logo);

                var programs = programBusiness.GetList(clubId, season.Domain).Where(p => p.Status == ProgramStatus.Open).ToList().Where(p => p.SaveType == SaveType.Publish).ToList();

                flyerPrograms = BuildPrograms(clubId, season); //Get Programs For Flyer

                var startSeason = "";
                var endSeason = "";
                decimal lateFee = 0;
                DateTime lateRegistrationOpenDate = DateTime.UtcNow;
                DateTime lateRegistrationOpenTime = DateTime.UtcNow;

                if (season.Setting != null)
                {
                    var programInfo = season.Setting.SeasonProgramInfoSetting;
                    if (programInfo != null)
                    {
                        startSeason = programInfo.StartDate != null ? programInfo.StartDate.Value.ToString("MMM d").ToUpper() : "";
                        endSeason = programInfo.EndDate != null ? programInfo.EndDate.Value.ToString("MMM d").ToUpper() : "";
                        lateFee = programInfo.LateRegistrationFee;

                        if (programInfo.LateRegistrationOpens.HasValue)
                        {
                            lateRegistrationOpenDate = programInfo.LateRegistrationOpens.Value.Date;
                            lateRegistrationOpenTime = new DateTime(programInfo.LateRegistrationOpens.Value.Ticks);
                        }
                    }

                }
                var list = new { startTime = "", endTime = "" };

                var schoolDomain = clubInfo.Domain;

                var model = new FlyerClassCatalogViewModel()
                {
                    IsGreenColor = partnerInfo.Domain.Equals("rightatschool") ? false : true,
                    IsBlueColor = partnerInfo.Domain.Equals("rightatschool") ? true : false,
                    Name = name,
                    Header = new BaseFlyerHeaderViewModel()
                    {
                        SchoolLogo = clubLogo,
                        PartnerLogo = partnerLogo
                    },
                    Body = new FlyerClassCatalogBodyViewModel()
                    {
                        SchoolName = partnerInfo.Domain.Equals("rightatschool") ? clubInfo.Name : clubInfo.Name.ToUpper(),
                        SchoolDomain = schoolDomain,
                        SeasonName = season.Name != SeasonNames.SelectSeason ? partnerInfo.Domain.Equals("rightatschool") ? season.Name.ToDescription() : (season.Name.ToDescription().ToUpper()) : null,
                        RegistrationOpenDate = _seasonBusiness.GetGeneralRegOpenDate(season) != null ? DateTimeHelper.FormatShortDateWithoutYear(_seasonBusiness.GetGeneralRegOpenDate(season).Value) : string.Empty,
                        ClubId = clubId,
                        Programs = flyerPrograms,
                        SeasonId = seasonId,
                        SeasonDate = !string.IsNullOrEmpty(startSeason) && !string.IsNullOrEmpty(startSeason) ? $"{startSeason} - {endSeason}" : string.Empty,
                        ClubUrl = GenerateClubUrl(schoolDomain),
                        GeneralRegistrationOpens = _seasonBusiness.GetGeneralRegOpenDate(season) != null ? _seasonBusiness.GetGeneralRegOpenDate(season).Value.ToString(Constants.F_DateTimeWithAMPM_AT).ToUpper() : string.Empty,
                        GeneralRegistrationCloses = _seasonBusiness.GetGeneralRegCloseDate(season) != null ? _seasonBusiness.GetGeneralRegCloseDate(season).Value.ToString(Constants.F_DateTimeWithAMPM_AT).ToUpper() : string.Empty,
                    },

                    Footer = new BaseFlyerFooterViewModel()
                    {
                        Phone = modelSetting.Phone,
                    },
                };


                return JsonNet(model, new JsonSerializerSettings());
            }

            return base.JsonFormResponse();

        }

        private int CountPMAndAM(List<FlyerProgramItemViewModel> ProgramSchedulesPM, List<FlyerProgramItemViewModel> ProgramSchedulesAM)
        {
            var countPMAndAM = 0;
            if (ProgramSchedulesPM != null && ProgramSchedulesAM != null)
            {
                countPMAndAM = ProgramSchedulesPM.Count + ProgramSchedulesAM.Count;
            }
            else
            {
                if (ProgramSchedulesPM == null && ProgramSchedulesAM != null)
                {
                    countPMAndAM = ProgramSchedulesAM.Count;
                }
                else if (ProgramSchedulesAM == null && ProgramSchedulesPM != null)
                {
                    countPMAndAM = ProgramSchedulesPM.Count;
                }
            }
            return countPMAndAM;
        }
        private string GenerateClubUrl(string clubDomain)
        {
            var url = string.Empty;
            var userName = this.GetCurrentUserName();

            var host = Request.Url.Host;
            var scheme = Request.Url.Scheme;


            host = string.Concat(host.Split('.')[1], ".", host.Split('.')[2]);

            url = string.Concat(scheme, "://", clubDomain, ".", host);

            return url;
        }

        private List<FlyerProgramItemViewModel> BuildScheduleDictionary(int clubId, Season seasonInfo, string typeProgram)
        {

            var programSchedules = new List<FlyerProgramItemDetailViewModel>();
            var AllProgramSchedules = new List<FlyerProgramItemViewModel>();


            var dayOfWeeks = new List<DayOfWeek>()
             {
                  DayOfWeek.Sunday,
                  DayOfWeek.Monday,
                  DayOfWeek.Tuesday,
                  DayOfWeek.Wednesday,
                  DayOfWeek.Thursday,
                  DayOfWeek.Friday,
                  DayOfWeek.Saturday,
            };
            var clubInfo = Ioc.ClubBusiness.Get(clubId);



            var programs = Ioc.ProgramBusiness.GetList(clubId, seasonInfo.Domain).Where(p => p.Status == ProgramStatus.Open).ToList().Where(p => p.SaveType == SaveType.Publish);

            var programBusiness = Ioc.ProgramBusiness;

            foreach (var day in dayOfWeeks)
            {
                TimeSpan timePM = new TimeSpan(12, 0, 0); //pm
                TimeSpan timeAM = new TimeSpan(0, 0, 0); //AM

                if (typeProgram == "PM") //Get All Program PM
                {
                    programSchedules = programs != null ? programs.Where(p => programBusiness.GetClassDays(p).Where(pr => pr.StartTime >= timePM && pr.StartTime.HasValue).Select(pr => pr.DayOfWeek).Contains(day)).Select(p =>
                         new FlyerProgramItemDetailViewModel
                         {
                             GradeOrAges = !string.IsNullOrEmpty(programBusiness.GenerateGradeInfoLabelForFlyer(p)) ? programBusiness.GenerateGradeInfoLabelForFlyer(p) : !string.IsNullOrEmpty(programBusiness.GenerateAgeInfoLabel(p)) ? programBusiness.GenerateAgeInfoLabel(p) : null,
                             ProgramName = p.Name,
                             NoClass = GetNOClassDate(p),
                             ProviderName = p.OutSourceSeasonId.HasValue ? p.OutSourceSeason.Club.Name : "",
                             StartTime = programBusiness.GetClassDays(p).FirstOrDefault(pr => pr.DayOfWeek == day && pr.StartTime.HasValue).StartTime.Value,
                             EndTime = programBusiness.GetClassDays(p).FirstOrDefault(pr => pr.DayOfWeek == day).EndTime.Value,
                             ClassDates = programBusiness.GetClassDates(p.ProgramSchedules.First(), TimeOfClassFormation.PM),
                             StrStartTime = (!string.IsNullOrEmpty(programBusiness.GenerateGradeInfoLabelForFlyer(p)) || !string.IsNullOrEmpty(programBusiness.GenerateAgeInfoLabel(p))) ? programBusiness.GetClassDays(p).FirstOrDefault(pr => pr.DayOfWeek == day && pr.StartTime.HasValue).StartTime != null ? string.Format("{0}{1}", " @", DateTime.Today.Add(programBusiness.GetClassDays(p).FirstOrDefault(pr => pr.DayOfWeek == day && pr.StartTime.HasValue).StartTime.Value).ToString("h:mm tt")) : null : programBusiness.GetClassDays(p).FirstOrDefault(pr => pr.DayOfWeek == day && pr.StartTime.HasValue).StartTime != null ? string.Format("{0}{1}", "@", DateTime.Today.Add(programBusiness.GetClassDays(p).FirstOrDefault(pr => pr.DayOfWeek == day && pr.StartTime.HasValue).StartTime.Value).ToString("h:mm tt")) : null,
                             Amount = p.ProgramSchedules.First().Charges.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).First().Amount,

                         })
                      .ToList() : null;
                }
                else  //Get All Program AM
                {
                    programSchedules = programs != null ? programs.Where(p => programBusiness.GetClassDays(p).Where(pr => pr.StartTime >= timeAM && pr.StartTime < timePM && pr.StartTime.HasValue).Select(pr => pr.DayOfWeek).Contains(day)).Select(p =>
                       new FlyerProgramItemDetailViewModel
                       {
                           GradeOrAges = !string.IsNullOrEmpty(programBusiness.GenerateGradeInfoLabelForFlyer(p)) ? programBusiness.GenerateGradeInfoLabelForFlyer(p) : !string.IsNullOrEmpty(programBusiness.GenerateAgeInfoLabel(p)) ? programBusiness.GenerateAgeInfoLabel(p) : null,
                           ProgramName = p.Name,
                           NoClass = GetNOClassDate(p),
                           ProviderName = p.OutSourceSeasonId.HasValue ? p.OutSourceSeason.Club.Name : "",
                           StartTime = programBusiness.GetClassDays(p).FirstOrDefault(pr => pr.DayOfWeek == day && pr.StartTime.HasValue).StartTime.Value,
                           EndTime = programBusiness.GetClassDays(p).FirstOrDefault(pr => pr.DayOfWeek == day).EndTime.Value,
                           ClassDates = programBusiness.GetClassDates(p.ProgramSchedules.First(), TimeOfClassFormation.AM),
                           StrStartTime = (!string.IsNullOrEmpty(programBusiness.GenerateGradeInfoLabelForFlyer(p)) || !string.IsNullOrEmpty(programBusiness.GenerateAgeInfoLabel(p))) ? programBusiness.GetClassDays(p).FirstOrDefault(pr => pr.DayOfWeek == day && pr.StartTime.HasValue).StartTime != null ? string.Format("{0}{1}", " @", DateTime.Today.Add(programBusiness.GetClassDays(p).FirstOrDefault(pr => pr.DayOfWeek == day && pr.StartTime.HasValue).StartTime.Value).ToString("h:mm tt")) : null : programBusiness.GetClassDays(p).FirstOrDefault(pr => pr.DayOfWeek == day && pr.StartTime.HasValue).StartTime != null ? string.Format("{0}{1}", "@", DateTime.Today.Add(programBusiness.GetClassDays(p).FirstOrDefault(pr => pr.DayOfWeek == day && pr.StartTime.HasValue).StartTime.Value).ToString("h:mm tt")) : null,
                           Amount = p.ProgramSchedules.First().Charges.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).First().Amount

                       })
                      .ToList() : null;
                }

                List<int> countClassDates = new List<int>();
                List<Charge> surCharges = new List<Charge>();
                var noClass = new List<string>();

                surCharges = Ioc.ClubBusiness.Calculatesurcharge(clubInfo); //Get all surcharge 

                foreach (var item in programSchedules) //Set all surcharge To Amount and Calculate CountClassDates
                {
                    decimal? tempAmountSurcharge = 0;

                    if (surCharges.Any())
                    {
                        foreach (var surcharge in surCharges)
                        {
                            tempAmountSurcharge += (surcharge.AmountType == ChargeDiscountType.Fixed) ? surcharge.Amount : ((item.Amount * surcharge.Amount) / 100);
                        }
                    }

                    item.Amount += tempAmountSurcharge;

                    if (item.ClassDates != null)
                    {
                        countClassDates.Add(item.ClassDates.Count());

                        noClass.AddRange(item.NoClass);
                    }
                }

                if (programSchedules.Count > 0)
                {
                    var modelDay = new FlyerProgramItemViewModel()
                    {
                        Day = day.ToString().ToUpper(),
                        Session = countClassDates.Distinct().ToList(),
                        Details = programSchedules,
                        NoClass = noClass.Distinct().ToList(),
                    };
                    AllProgramSchedules.Add(modelDay);
                }

            }

            return AllProgramSchedules;
        }

        private List<string> GetNOClassDate(Program program)
        {    
            var noClass = _programBusiness.GetClassHolidayDates(program);

            return noClass.Select(n => DateTimeHelper.FormatShortDateWithoutYearBySlash(n.Start.Date)).ToList();
        }

        private List<FlyerProgramItemViewModel> FillEmptyCell(int max, List<FlyerProgramItemViewModel> programSchedules)
        {

            foreach (var item in programSchedules)
            {
                var countAddEmptyCell = 0;
                if (item.Details.Count != max)
                {
                    countAddEmptyCell = (max - item.Details.Count);
                }

                for (int i = 0; i < countAddEmptyCell; i++)
                {
                    item.Details.Add(
                 new FlyerProgramItemDetailViewModel
                 {
                     GradeOrAges = null,
                     ProgramName = "",
                     Amount = null
                 });
                }
            }

            return programSchedules;

        }

        private List<FlyerProgramItemDetailViewModel> BuildPrograms(int clubId, Season seasonInfo)
        {
            var programSchedules = new List<FlyerProgramItemDetailViewModel>();

            var programBusiness = Ioc.ProgramBusiness;
            var clubInfo = Ioc.ClubBusiness.Get(clubId);

            var programs = programBusiness.GetList(clubId, seasonInfo.Domain).Where(p => p.Status == ProgramStatus.Open).ToList().Where(p => p.SaveType == SaveType.Publish);

            programSchedules = programs != null ? programs.Select(p =>
                  new FlyerProgramItemDetailViewModel
                  {
                      GradeOrAges = !string.IsNullOrEmpty(programBusiness.GenerateGradeInfoLabelForFlyer(p)) ? programBusiness.GenerateGradeInfoLabelForFlyer(p) : !string.IsNullOrEmpty(programBusiness.GenerateAgeInfoLabel(p)) ? programBusiness.GenerateAgeInfoLabel(p) : null,
                      ProgramName = p.Name,
                      Days = (programBusiness.GetClassDays(p).Select(pr => pr.DayOfWeek).ToList()),
                      ProviderName = p.OutSourceSeasonId.HasValue ? p.OutSourceSeason.Club.Name : "",
                      ProgramDescription = Regex.Replace(p.Description, "<(.|\\n)*?>", string.Empty),
                      StrStartTime = programBusiness.GetClassDays(p).Where(o => o.StartTime.HasValue).Min(pr => pr.StartTime) != null ? DateTime.Today.Add(programBusiness.GetClassDays(p).Where(o => o.StartTime.HasValue).Min(pr => pr.StartTime.Value)).ToString("h:mm tt") : null,
                      StrEndTime = programBusiness.GetClassDays(p).Where(o => o.EndTime.HasValue).Max(pr => pr.EndTime) != null ? DateTime.Today.Add(programBusiness.GetClassDays(p).Where(o => o.EndTime.HasValue).Max(pr => pr.EndTime.Value)).ToString("h:mm tt") : null,
                      StartDate = DateTimeHelper.FormatDate(p.ProgramSchedules.Where(pr => !pr.IsDeleted).FirstOrDefault().StartDate),
                      EndDate = DateTimeHelper.FormatDate(p.ProgramSchedules.Where(pr => !pr.IsDeleted).FirstOrDefault().EndDate),
                      ClassDates = programBusiness.GetClassDates(p.ProgramSchedules.FirstOrDefault(), TimeOfClassFormation.Both),
                      CountSession = programBusiness.GetClassDates(p.ProgramSchedules.FirstOrDefault(), TimeOfClassFormation.Both) != null ? programBusiness.GetClassDates(p.ProgramSchedules.FirstOrDefault(), TimeOfClassFormation.Both).Count() : 0,
                      Amount = p.ProgramSchedules.First().Charges.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).First().Amount,
                  }).ToList().OrderBy(p => p.Days.First()).ThenBy(p => p.ProgramName).ToList() : null;

            List<Charge> surCharges = new List<Charge>();
            surCharges = Ioc.ClubBusiness.Calculatesurcharge(clubInfo); //Get All surcharge school and partner

            foreach (var item in programSchedules) //set All surcharge school and partner To Amount
            {
                decimal? tempAmountSurcharge = 0;

                if (surCharges.Any())
                {
                    foreach (var surcharge in surCharges)
                    {
                        tempAmountSurcharge += (surcharge.AmountType == ChargeDiscountType.Fixed) ? surcharge.Amount : ((item.Amount * surcharge.Amount) / 100);
                    }
                }
                item.Amount += tempAmountSurcharge;

            }

            return programSchedules;
        }

        [HttpPost]
        public virtual ActionResult DeleteFlyer(int id)
        {
            OperationStatus result = null;

            var flyer = Ioc.JbFlyerBusiness.Get(id);

            result = Ioc.JbFlyerBusiness.DeleteFlyer(flyer.Id);

            if (result.Status)
            {
                return Json(new JResult { Status = true, Message = "Flyer deleted successfully.", RecordsAffected = 1 });
            }

            return base.JsonFormResponse();
        }

        [HttpGet]
        public virtual ActionResult GetFlyer(int id)
        {
            var flyer = Ioc.JbFlyerBusiness.Get(id);
            var model = new FlyerEditViewModel();

            model.Name = flyer.Name;
            model.Id = flyer.Id;

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult EditFlyer(FlyerEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                var flyer = Ioc.JbFlyerBusiness.Get(model.Id);

                flyer.Name = model.Name;
                flyer.UpdatedDate = DateTime.UtcNow;
                var result = Ioc.JbFlyerBusiness.Update(flyer);

                return Json(new JResult { Status = result.Status, Message = "", RecordsAffected = 1 });
            }

            return base.JsonFormResponse();
        }

        public virtual ActionResult GetFlyersSetting()
        {
            var model = new FlyersSettingsViewModel();
            var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);
            model = GetFlyersSetting(club);
            return JsonNet(model);
        }
        [HttpPost]
        public virtual ActionResult SaveFlyersSetting(FlyersSettingsViewModel model)
        {
            Club partner = null;
            var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);
            PartnerSetting partnerSetting;
            if (!club.IsPartner)
            {
                partner = club.PartnerClub;
                partnerSetting = Ioc.ClubBusiness.GetPartnerSetting(partner.Id);
            }
            else
            {
                partnerSetting = Ioc.ClubBusiness.GetPartnerSetting(club.Id);
            }

            if (club.Setting == null)
            {
                club.Setting = new Core.Domain.ClubSetting();
            }

            var contentEmail = "";
            var ContentPhone = "";

            contentEmail = (model.Email != null) ? model.Email : null;
            partnerSetting.ClubFlyerSetting.ContactEmail = contentEmail;

            ContentPhone = (model.Phone != null) ? model.Phone : null;
            partnerSetting.ClubFlyerSetting.ContactPhone = ContentPhone;


            var serializedSettings = JsonConvert.SerializeObject(partnerSetting);

            //club.IsSettingChanged = true;
            club.SettingSerialized = serializedSettings;

            var res = Ioc.ClubBusiness.Update(club);

            return Json(new JResult { Status = res.Status, Message = res.Message });
        }

        private FlyersSettingsViewModel GetFlyersSetting(Club club)
        {
            var model = new FlyersSettingsViewModel();
            Club partner = null;

            PartnerSetting partnerSetting;
            if (!club.IsPartner)
            {
                partner = club.PartnerClub;
                partnerSetting = Ioc.ClubBusiness.GetPartnerSetting(partner.Id);
            }
            else
            {
                partnerSetting = Ioc.ClubBusiness.GetPartnerSetting(club.Id);
            }


            if (club.Setting != null)
            {
                if (partnerSetting != null && partnerSetting.ClubFlyerSetting != null)
                {
                    model.Email = partnerSetting.ClubFlyerSetting.ContactEmail;
                    model.Phone = partnerSetting.ClubFlyerSetting.ContactPhone;


                }
            }
            return model;
        }
    }
}