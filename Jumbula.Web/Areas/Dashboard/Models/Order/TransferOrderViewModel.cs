﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Core.Model.Order;

namespace SportsClub.Areas.Dashboard.Models
{
    public class TransferChargeDiscountViewModel
    {
        // public List<ChargeDiscountItemViewModel> AllChargeDiscounts { get; set; }

        // public List<ChargeDiscountItemViewModel> SelectedChargeDiscounts { get; set; }

        public List<ChargeDiscountItemViewModel> OrderChargeDiscounts { get; set; }
        public List<OrderInstallmentViewModel> SubscriptionOrderInstallments { get; set; }
    }

    public class TransferOrderViewModel
    {
        public long Id { get; set; }

        public long SourceProgramId { get; set; }

        public string ProgramName { get; set; }

        public decimal OrderAmount { get; set; }

        public decimal PaidAmount { get; set; }

        public decimal EntryFee { get; set; }

        public decimal? TransferFee { get; set; }
        public ProgramTypeCategory ProgramType { get; set; }

        public List<SelectKeyValue<string>> AllPrograms { get; set; }
        public List<TransactionActivitiesViewModel> InstallmentsTransaction { get; set; }
        public long TutionId { get; set; }

        public string SelectedProgram { get; set; }

        public bool RefundOrPay { get; set; }
        public bool OrderIsAutocharge { get; set; }
        public long SelectedProgramId { get; set; }

        public long SelectedTution { get; set; }
        public ProgramSchedule TargetSchedule { get; set; }
        public Charge TargetTution { get; set; }
        public string ItemName { get; set; }
        public Program TargetProgram { get; set; }
        public List<OrderItemFollowupForm> FolloupForms { get; set; }
        public string Gender { get; set; }

        public int Age { get; set; }

        public string PlayerName { get; set; }

        public string ConfirmationId { get; set; }

        public PaymentPlanType PaymentPlanType { get; set; }

        public string TransferMemo { get; set; }

        public bool SendEmail { get; set; }
        public List<SubscriptionProgramDaysViewModel> AllSubscriptionProgramDays { get; set; }
        public List<DayOfWeek> SelectedDays { get; set; }
        public List<DayOfWeek> NewDays { get; set; }
        public DateTime? NewEffectiveDate { get; set; }
        public DateTime? CurrentDesiredStartDate { get; set; }
        public DateTime LastSessionDate { get; set; }
        public DateTime? TodayDate { get; set; }
        public long? SourceTutionId { get; set; }
        public int ProgramDays { get; set; }

        public List<ProgramPartViewModel> ProgramParts { get; set; }
        public decimal? TotalInstallments 
        {
            get
            {
                if (OrderInstallments != null && OrderInstallments.Any())
                {
                    return OrderInstallments.Where(o => !o.IsDeleted).Sum(o => o.Amount);
                }

                return null;
            }
        }

        public bool HasInstallment => OrderInstallments != null && OrderInstallments.Any();

        public List<ChargeDiscountItemViewModel> SourceCharges { get; set; }

        public List<ChargeDiscountItemViewModel> SourceDiscounts { get; set; }

        public List<ChargeDiscountItemViewModel> OrderChargeDiscounts { get; set; }

        public List<OrderInstallmentViewModel> OrderInstallments { get; set; }
        public List<OrderInstallmentViewModel> SubscriptionOrderInstallments { get; set; }

    }

}