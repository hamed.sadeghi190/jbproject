﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('toolboxController', ['$scope', function ($scope) {
        window.app.globalObjects.pageClass = 'toolbox-view';
        window.app.globalObjects.initializeView();
        
        $scope.MC.openBlade({ url: "Dashboard/Blade/Toolbox", order: 1, menu: "TOOLBOX" });
    }]);

})();