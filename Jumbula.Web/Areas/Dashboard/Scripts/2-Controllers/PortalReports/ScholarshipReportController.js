﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('ScholarshipReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        $scope.Model = customReportService.getModelFromStrorage();

        if (is.undefined($scope.Model)) {
            $scope.Model = {};
        }

        $scope.Total = 0;
       
        $http.get(baseUrl + '/JustGetScholarships', { params: { start: $scope.Model.From, end: $scope.Model.To, calucalteTotal: true } })
            .success(function (data, status, headers, config) {
                $scope.Total = data.Total;
                $scope.ClubName = data.ClubName;
                $scope.Date = data.Date;
                prepareGrid();
            });

        $scope.Model.checkedIds = {};
        var detailExportPromises = [];

        var prepareGrid = function () {
            $scope.ScholarshipReportsDataSource = new kendo.data.DataSource({

                type: "json",
                transport: {
                    read: {
                        url: baseUrl + '/JustGetAllScholarships',
                        type: "POST",
                        data: function () {
                            return {
                                start: $scope.Model.From,
                                end: $scope.Model.To
                            };
                        },
                    },
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                resizable: true,
                batch: true,
            });

            $scope.ScholarshipReportGridOptions = {
                excel: {
                    allPages: true,
                    fileName: "Scholarship.xlsx",
                    proxyURL: "/Dashboard/CustomReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.push({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.push({
                        cells: [{
                            value: "Total scholarships: " + $scope.Total
                        }]
                    });                    

                    rows.unshift({
                        cells: [{
                            value: $scope.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Scholarship - " + $scope.ClubName
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.ScholarshipReportsDataSource,
                resizable: true,
                detailTemplate: kendo.template($("#template").html()),
                dataBound: function () {
                    var grid = $("#ScholarshipReportGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);


                    var data = grid.dataSource.data();
                    var isEmptyScholarshipAmount1 = false;
                    var isEmptyScholarshipAmount2 = false;
                    var isEmptyScholarshipAmount3 = false;
                   
                    for (var i = 0 ; i < data.length; i++) {
                        if (data[i].ScholarshipAmount1 != "") {
                            isEmptyScholarshipAmount1 = true;
                        }

                        if (data[i].ScholarshipAmount2 != "") {
                            isEmptyScholarshipAmount2 = true;
                        }

                        if (data[i].ScholarshipAmount3 != "") {
                            isEmptyScholarshipAmount3 = true;
                        }                        
                    }

                    if (!isEmptyScholarshipAmount1) {

                        grid.hideColumn("ScholarshipAmount1");

                    }
                    if (!isEmptyScholarshipAmount2) {

                        grid.hideColumn("ScholarshipAmount2");

                    }
                    if (!isEmptyScholarshipAmount3) {

                        grid.hideColumn("ScholarshipAmount3");

                    }                    

                    var view = this.dataSource.view();
                    for (var i = 0; i < view.length; i++) {
                        if ($scope.Model.checkedIds[view[i].UserId]) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .addClass("k-state-selected")
                            .find(".checkbox")
                            .attr("checked", "checked");
                        }
                    }

                    //Hide expand/collapse row 
                    $(".k-hierarchy-col").remove();
                    $(".k-hierarchy-cell").remove();

                },
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5,
                },
                sortable: true,
                columns: [

                    {
                        template: "<input type='checkbox' class='checkbox' />",
                        width: "30px",
                        headerTemplate: '<input type="checkbox" class="checkAll" />'
                    },
                    {
                        field: "User",
                        title: "Full name",
                        width: "180px",
                    },
                    {
                        field: "ItemName",
                        title: "Program",
                        width: "210px",
                    },
                    {
                        field: "SeasonName",
                        title: "Season",
                        width: "170px",
                    },
                    {
                        field: "Date",
                        title: "Date",
                        width: "150px",
                    },
                    {
                        field: "Confirmation",
                        title: "Confirmation",
                        width: "170px",
                    },
                    {
                        field: "ScholarshipAmount1",
                        title: "Scholarship 1 ($)",
                        width: "210px"
                    },
                    {
                        field: "ScholarshipAmount2",
                        title: "Scholarship 2 ($)",
                        width: "210px",
                    },
                    {
                        field: "ScholarshipAmount3",
                        title: "Scholarship 3 ($)",
                        width: "210px",
                    },
                    {
                        field: "TotalScholarship",
                        title: "Total ($)",
                        width: "150px",
                    }
                ]
            }
        }

        $(document.body).on('click', '#ScholarshipReportGrid table .checkbox', selectRow);
        $(document.body).on('click', '#ScholarshipReportGrid table .checkAll', checkAll);

        function exportChildData(Id, rowIndex) {

            var deferred = $.Deferred();
            detailExportPromises.push(deferred);

            //var rows = [
            //    {
            //        cells: [
            //            // First cell
            //            { value: "Catalog" },
            //            // Second cell
            //            { value: "Date" }
            //        ]
            //    }
            //];

            dataSourceFulldata.filter({ field: "Id", operator: "eq", value: Id });

            var exporter = new kendo.ExcelExporter({
                columns: [
                    {
                        field: "Type",
                        title: "Discount"
                    }, {
                        field: "Fee",
                        title: "Amount"
                    }
                ],
                dataSource: dataSourceFulldata
            });

            exporter.workbook().then(function (book, data) {
                deferred.resolve({
                    masterRowIndex: rowIndex,
                    sheet: book.sheets[0]
                });
            });
        }

        //on click of the checkbox:
        function selectRow() {
            var checked = this.checked,
            row = $(this).closest("tr"),
            grid = $("#ScholarshipReportGrid").data("kendoGrid"),
            dataItem = grid.dataItem(row);
            $scope.Model.checkedIds[dataItem.UserId] = checked;
            if (checked) {
                //-select the row
                row.addClass("k-state-selected");
            } else {
                //-remove selection
                row.removeClass("k-state-selected");
            }
        }

        $scope.MakeCampaign = function () {
            var checked = [];
            //for (var i = 0; $scope.Model.checkedIds.length; i++) {
            for (var i in $scope.Model.checkedIds) {
                if ($scope.Model.checkedIds[i]) {
                    checked.push(i);
                }
            }
            //alert(checked);
            // Redirect to the special action
            if (checked.length > 0) {
                $http.post(baseUrl + "/SendEmailToMembers", { membersIds: checked, reportName: "Scholarship" })
                    .success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            //$scope.MC.openBlade({ url: "Dashboard/Blade/Seasons", order: 0, menu: "SEASONS" });
                            $state.go('EmailCampaignAddStep2', { campaignId: data.Data, campaignType: 'UserList' });
                        } else {
                            jbToast.error(data.Message);
                        }
                    }).error(function (data, status, headers, config) {
                        //alert(data);
                    });
            } else {
                jbToast.error("Please select at least a participant to send mail.");
            }
        };

        function checkAll() {
            var grid = $("#ScholarshipReportGrid").data("kendoGrid");
            var state = $("#ScholarshipReportGrid table > thead > tr > th:nth-child(1) > input").prop("checked");
            var ckbox;
            grid.tbody.find("tr.k-master-row").each(function () {
                var $this = $(this);
                ckbox = $this.find("td .checkbox");
                ckbox.prop("checked", state);
                var dataItem = grid.dataItem($this);
                $scope.Model.checkedIds[dataItem.UserId] = state;
                if (state) {
                    //-select the row
                    $this.addClass("k-state-selected");
                } else {
                    //-remove selection
                    $this.removeClass("k-state-selected");
                }
            });
        }

        function detailInit(e) {
            var detailRow = e.detailRow;
            detailRow.find(".orders").kendoGrid({
                dataSource: {
                    type: "json",
                    transport: {
                        read: {
                            url: baseUrl + '/GetJustScholarshipDetails',
                            type: "POST",
                            data: function () {
                                return {
                                    itemId: e.data.Id
                                };
                            },
                        },
                    },
                    schema: {
                        total: "TotalCount",
                        data: "DataSource"
                    },
                    serverPaging: true,
                    pageSize: 10,
                },
                excelExport: function (e) {
                    e.preventDefault();
                },
                scrollable: false,
                sortable: true,
                pageable: true,
                columns: [
                    {
                        field: "Name",
                        title: "Discount",
                        width: "70px"
                    },
                    {
                        field: "Fee",
                        title: "Amount",
                        width: "110px",
                        //template: "#=kendo.toString(kendo.parseDate(UpdateDate), 'MM/dd/yyyy')#",
                    }
                ]
            });
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#ScholarshipReportGrid").getKendoGrid().saveAsExcel();
        }
    }
    ]);

})();