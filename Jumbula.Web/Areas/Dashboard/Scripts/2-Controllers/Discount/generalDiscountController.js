﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('generalDiscountController', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'discounts-view';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;

        $scope.baseUrl = window.location.origin + "/Dashboard/Discount";

        $scope.discountId = 0;
        if ($stateParams.discountId) {
            $scope.discountId = $stateParams.discountId;
        }

        $scope.pageTitle = "General discount";

        //addd
        $scope.programDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: {
                    url: window.location.origin + "/Dashboard/program/GetListForDropDown?seasonDomain=" + $scope.MC.stateParams.seasonDomain,
                    cache: false,
                    dataType: "json"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        return {
                            term: document.activeElement.value
                        }
                    }
                }
            }
        });
        $scope.programOptions = {
            placeholder: "Select",
            dataTextField: "Name",
            dataValueField: "Id",
            autoBind: false,
            dataSource: $scope.programDataSource
        };
       
        $http.get($scope.baseUrl + '/CreateEditGeneralDiscount?id=' + $scope.discountId + '&seasonDomain=' + $scope.MC.stateParams.seasonDomain).success(function (data, status, headers, config) {
            $scope.Model = data;
            //$scope.discountCategory = $scope.Model.CategoryDescription;

            $scope.nopLabel = "Number of orders";
            $scope.pageTitle = "General discount";


            $scope.applyitems = [
                { text: $scope.Model.ApplyItems[0].Text, value: $scope.Model.ApplyItems[0].Value },
            ];
            var nop = $scope.Model.NOP.Value;
            var selectedIndex = $scope.Model.ApplyType;
            $scope.applyType = $scope.applyitems[selectedIndex];

        });

        $scope.changeApplyType = function () {
            $scope.applyitems = [
              { text: $scope.Model.ApplyItems[0].Text, value: $scope.Model.ApplyItems[0].Value },
            ];
            $scope.applyType = $scope.applyitems[$scope.applyType.value];


        }

        $scope.saveDiscount = function () {
            $scope.Model.ApplyType = $scope.applyType.value;

            $http.post($scope.baseUrl + '/SaveGeneralDiscount', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $state.go('Discounts', { seasonDomain: $scope.MC.stateParams.seasonDomain })
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        };
    }]);
})();