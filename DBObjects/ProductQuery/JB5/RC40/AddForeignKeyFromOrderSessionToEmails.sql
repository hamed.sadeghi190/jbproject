Alter TABLE OrderSessions add [EmailId] [int] NULL

GO 

ALTER TABLE [dbo].[OrderSessions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.OrderSessions_dbo.Emails_EmailId] FOREIGN KEY([EmailId])
REFERENCES [dbo].[Emails] ([Id])
GO
