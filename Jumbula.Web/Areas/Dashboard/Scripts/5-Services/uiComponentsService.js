﻿(function () {
    'use strict';

    angular.module('dashboardApp').service("uiComponentsService", [function () {
        var _model;
        function setModel(Data) {
            _model = Data;
        }
        function getModel() {
            return _model;
        }

        /**********************************
        Tooltip component.
        Author: mostafa naderi.
        Usage:
            Html:
              <div id="tooltipClubLogo" class="jb-tooltip">
                <span ng-click="toggleTooltip('tooltipClubLogo','/Areas/Dashboard/Content/Image/club-logo.jpg')" class="info-icon">i</span>
                <div class="tooltip-content"></div>
              </div>
            Controller:
            $scope.toggleTooltip = function (containerId) {
            if (arguments[3])
                uiComponentsService.toggleTooltip(containerId, arguments[1], arguments[2], arguments[3]);
            else if (arguments[2])
                uiComponentsService.toggleTooltip(containerId, arguments[1], arguments[2]);
            else if (arguments[1])
                uiComponentsService.toggleTooltip(containerId, arguments[1]);
            else
                uiComponentsService.toggleTooltip(containerId);
            }
        ***********************************/
        this.toggleTooltip = function (containerId) {
            var element = angular.element('#' + containerId + ">.tooltip-content");
            if (arguments[1] && !element.hasClass('appended-img')) {
                
                var imgTag = angular.element("<img src='"+arguments[1]+"' />");
             
                angular.element(imgTag).appendTo(element);
                element.addClass('appended-img');
            }
            if (arguments[2] && (arguments[2] == 'left' || arguments[2] == 'center' || arguments[2] == 'right')) {
                element.addClass('tooltip-align-' + arguments[2]);
            }
            if (arguments[3] && (arguments[3] == 'top')) {
                element.addClass('tooltip-valign-' + arguments[3]);
            }
            var allElements = angular.element(".tooltip-content");
            allElements.stop().slideUp(10);
            element.stop().slideToggle(200);

        }

        this.slideDown = function (elementID) {
            var element = angular.element('#' + elementID);
            if(arguments[1])
                element.stop().slideDown(parseInt(arguments[1]));
            else
                element.stop().slideDown();
        }
        this.slideUp = function (elementID) {
            var element = angular.element('#' + elementID);
            if (arguments[1])
                element.stop().slideUp(parseInt(arguments[1]));
            else
                element.stop().slideUp();
        }
        this.slideToggle = function (elementID) {
            var element = angular.element('#' + elementID);
            if (arguments[1])
                element.stop().slideToggle(parseInt(arguments[1]));
            else
                element.stop().slideToggle();
        }
        this.show = function (elementID) {
            var element = angular.element('#' + elementID);
            if (arguments[1])
                element.stop().show(parseInt(arguments[1]));
            else
                element.stop().show();
        }
        this.hide = function (elementID) {
            var element = angular.element('#' + elementID);
            if (arguments[1])
                element.stop().hide(parseInt(arguments[1]));
            else
                element.stop().hide();
        }

    }]);
})();