﻿using Jb.Framework.Common;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IPaymentDetailBusiness
    {
    
        IQueryable<PaymentDetail> GetList();
        OperationStatus Update(PaymentDetail paymentDetail);
        PaymentDetail GetByInvoiceId(int InvoiceId);
        IQueryable<PaymentDetail> GetByInvoiceIds(List<int> InvoiceIds);
        PaymentDetail GetByTransactionId(string transactionId);
        string GetTransactionIdFromResponse(string response);
    }
}
