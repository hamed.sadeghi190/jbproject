﻿

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jumbula.Common.Enums
{
    public enum NutAllergy
    {
        [Description("No")]
        [Display(Name = "No")]
        No = 0,
        [Description("Yes")]
        [Display(Name = "Yes")]
        Yes = 1,
    }
}
