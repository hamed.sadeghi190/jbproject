﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SingOutReportViewModel
    {
        public string ParticipantName { get; set; }
        public string Grade { get; set; }
        public int Number { get; set; }
        public string ReportName { get; set; }
        public string SchoolName { get; set; }
        public string Date { get; set; }
        public string Day { get; set; }
        public string Logo { get; set; }
        public string TuitionName { get; set; }
        public List<ParticipantInfo> participants { get; set; }
    }
    public class ParticipantInfo
    {
        public string ParticipantName { get; set; }
        public string Grade { get; set; }
        public int Number { get; set; }
        public string TuitionName { get; set; }
    }
}