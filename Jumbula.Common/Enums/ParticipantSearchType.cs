﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum ParticipantSearchType : byte
    {
        Name,
        Email,
        [Description("Confirmation ID")]
        ConfirmationId
    }

}
