﻿using System;
using System.Linq;
using FluentValidation;

namespace Jb.Framework.Common.Forms.Validation
{
    public class SignatureValidator : AbstractValidator<JbSignature>
    {
        public SignatureValidator()
        {

            RuleFor(x => x.Value)
                .NotNull()
                .When(w => w.CurrentMode != AccessMode.Design && w.VisibleMode == VisibilityMode.Both && w.Validations.Any(x => x.Type == ElementValidationType.Required))
                .WithMessage("{0}", x => x.SignitureRoleMessage);
           

            RuleFor(x => x.Value)
            .NotEmpty()
            .When(w => w.CurrentMode != AccessMode.Design && w.VisibleMode == VisibilityMode.Both && w.Validations.Any(x => x.Type == ElementValidationType.Required))
                .WithMessage("{0}", x => x.SignitureRoleMessage);

         
            RuleFor(x => x.Value)
            .NotEqual("image/jsignature;base30,")
             .When(w => w.CurrentMode != AccessMode.Design && w.VisibleMode == VisibilityMode.Both && w.Validations.Any(x => x.Type == ElementValidationType.Required))
                .WithMessage("{0}", x => x.SignitureRoleMessage);

            RuleFor(x => x.Value)
             .Must(SignatureLenValidator)
              .When(w => w.CurrentMode != AccessMode.Design && w.VisibleMode == VisibilityMode.Both && w.Validations.Any(x => x.Type == ElementValidationType.Required))
             .WithMessage("{0}", x => x.SignitureRoleMessage);

        }

        private bool SignatureLenValidator(JbSignature textBox, String value)
        {

            if (! string.IsNullOrEmpty(value) && value.Length < 35)
            { return false; }
            return true;
        }
     

    }
}