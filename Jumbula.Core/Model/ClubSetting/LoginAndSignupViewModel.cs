﻿
using System.ComponentModel.DataAnnotations;


namespace Jumbula.Core.Model.ClubSetting
{
    public class LoginAndSignupViewModel
    {
        public string NewFamilyNotification { get; set; }
        public bool IsPartner { get; set; }
    }
 
}