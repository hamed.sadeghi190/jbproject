﻿//using Elmah;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Configuration;
using SportsClub.Domain;
using SportsClub.Infrastructure.Helper;

namespace SportsClub.Infrastructure
{
    /// <summary>
    /// <remarks>
    /// We don't check the validity of the config data.
    /// Such errors will be caught in SendEmail
    /// </remarks>
    /// </summary>
    public static class Email
    {
        private static SmtpClient _client;
        private static bool sendEmailToSupport;
        static Email()
        {
            var config = WebConfigHelper.SmtpMailSettings;
            _client = new SmtpClient(config.Network.Host, config.Network.Port)
                {
                    Credentials = new NetworkCredential(config.Network.UserName, config.Network.Password),
                    EnableSsl = config.Network.EnableSsl
                };


            // Use the following code for development - make sure it is commented out when checking in.....
            //_client = new SmtpClient
            //{
            //    Host = "smtp.gmail.com",
            //    Port = 587,
            //    EnableSsl = true,
            //    DeliveryMethod = SmtpDeliveryMethod.Network,
            //    UseDefaultCredentials = false,
            //    Credentials = new NetworkCredential("mst.soltani@gmail.com", "")
            //};

            sendEmailToSupport = false;
            var jumbulaSettings = WebConfigurationManager.GetSection("jumbula") as NameValueCollection;
            bool.TryParse(jumbulaSettings["SendEmailtoSupport"].ToString(), out sendEmailToSupport);
        }

        public static void SendEmail(MailAddress from, MailAddress to, string subj, string body, bool isBodyHtml, MailAddress bcc = null, List<MailAddress> cc = null, string clubDomain = null, string fileNames = null)
        {
            try
            {
                using (MailMessage mail = new MailMessage(from, to))
                {
                    mail.Subject = subj;
                    mail.Body = body;
                    mail.IsBodyHtml = isBodyHtml;
                    if (bcc != null)
                    {
                        mail.Bcc.Add(bcc);
                    }
                    if (cc != null)
                    {
                        foreach (var item in cc)
                        {
                            mail.CC.Add(item);
                        }
                    }
                    string[] fileNameList;
                    if (!string.IsNullOrEmpty(fileNames))
                    {
                        fileNameList = fileNames.Split('*');

                        StorageManager storageManager = new StorageManager(WebConfigHelper.AzureStorageConnection,
                                                                                   Constants.Path_EmailAttachments);
                        foreach (var fileName in fileNameList)
                        {
                            var path = StorageManager.GetUri(Constants.Path_EmailAttachments, clubDomain, fileName).AbsoluteUri;
                            if (StorageManager.IsExist(path))
                            {
                                var stream = storageManager.DownloadBlobAsStream(path);
                                mail.Attachments.Add(new Attachment(stream, fileName));
                            }
                        }
                    }

                    _client.Send(mail);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static void SendEventQuestion(string email, string subject, string body)
        {
            MailAddress from = new MailAddress(Constants.W_NoReplyEmailAddress, Constants.W_Jumbula);
            MailAddress to = new MailAddress(email, Constants.W_Jumbula);

            Email.SendEmail(from, to, subject, body, true);
        }

        public static void SendClaimClub(string email, string body)
        {
            MailAddress from = new MailAddress(Constants.W_NoReplyEmailAddress, Constants.W_Jumbula);
            MailAddress to = new MailAddress(Constants.W_JumbulaSupportEmail, Constants.W_Jumbula);

            string subject = "Claim Club";

            Email.SendEmail(from, to, subject, body, true);
        }

        public static void SendConfirmEmail(MailAddress from, string to, string subject, string body)
        {

            Email.SendEmail(from, new MailAddress(to), subject, body, true);
        }


        public static void SendConfirmEmailSuccess(MailAddress from, string to, string subject, string body)
        {

            Email.SendEmail(from, new MailAddress(to), subject, body, true);
        }

        public static void SendConfirmFbLoginSuccess(string to, string user, string name, string baseUrl, string token)
        {
            var body = FormatEmail.SendConfirmFbLoginSuccess(user, name, baseUrl, token);
            var subj = string.Format(Constants.F_ConfirmEmailSuccessSubject, name);
            var from = new MailAddress(Constants.W_NoReplyEmailAddress, Constants.W_Jumbula);
            Email.SendEmail(from, new MailAddress(to), subj, body, true);
        }
        public static void SendResetPassword(MailAddress from, string to, string subject, string body)
        {
            // additionalparameters is string that concat at the end of url 

            Email.SendEmail(from, new MailAddress(to), subject, body, true);
        }

        public static void SendResetPasswordSuccess(MailAddress from, string to, string subject, string body)
        {

            Email.SendEmail(from, new MailAddress(to), subject, body, true);
        }

        public static void SendReservationEmail(List<string> toPrimaryPlayerEmail, List<string> toSecondaryPlayerEmail, List<string> toClubOwner, List<string> toOutSourcerCLubOwner, string subject, string body, string clubOwnerName)
        {

            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubOwnerName);
            List<MailAddress> ccList = null;
            if (toSecondaryPlayerEmail != null && toSecondaryPlayerEmail.Count() > 0)
            {
                ccList = toSecondaryPlayerEmail.Select(c => new MailAddress(c)).ToList();
            }
            foreach (var playerEmail in toPrimaryPlayerEmail)
            {
                Email.SendEmail(fromClubOwner, new MailAddress(playerEmail), subject, body, true, null, ccList);
            }

            subject += " from " + string.Join(",", toPrimaryPlayerEmail.ToArray());

            if (sendEmailToSupport)
            {
                Email.SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), subject, body, true);
            }

            List<MailAddress> ccListClubOwner = new List<MailAddress>();
            if (toClubOwner != null)
            {
                var primaryClubEmail = toClubOwner[0];
                toClubOwner.Remove(primaryClubEmail);
                if (toClubOwner.Count() > 0)
                {
                    ccListClubOwner.AddRange(toClubOwner.Select(c => new MailAddress(c)));
                }

                if (toOutSourcerCLubOwner != null && toOutSourcerCLubOwner.Count() > 0)
                {
                    ccListClubOwner.AddRange(toOutSourcerCLubOwner.Select(c => new MailAddress(c)));
                }
                Email.SendEmail(fromClubOwner, new MailAddress(primaryClubEmail), subject, body, true, null, ccListClubOwner);
            }
        }

        public static void SendInstallmentConfirmationEmail(string toPrimaryPlayerEmail, string toSecondaryPlayerEmail, List<string> toClubOwner, string toOutSourcerCLubOwner, string subject, string body, string clubOwnerName)
        {

            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubOwnerName);

            if (!string.IsNullOrEmpty(toSecondaryPlayerEmail))
            {
                Email.SendEmail(fromClubOwner, new MailAddress(toPrimaryPlayerEmail), subject, body, true, null, new List<MailAddress>() { new MailAddress(toSecondaryPlayerEmail) });
            }
            else // just send email to Primary Email
            {
                Email.SendEmail(fromClubOwner, new MailAddress(toPrimaryPlayerEmail), subject, body, true);
            }


            // send to Support if in config set to to true
            if (sendEmailToSupport)
            {
                Email.SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), subject + " from " + toPrimaryPlayerEmail, body, true);
            }

            //send to ClubOwner
            List<MailAddress> ccListClubOwner = new List<MailAddress>();
            if (toClubOwner != null && !string.IsNullOrEmpty(toClubOwner[0]))
            {
                for (int i = 1; i < toClubOwner.Count; i++)
                {
                    ccListClubOwner.Add(new MailAddress(toClubOwner[i]));
                }
                if (!string.IsNullOrEmpty(toOutSourcerCLubOwner))
                {
                    ccListClubOwner.Add(new MailAddress(toOutSourcerCLubOwner));
                }
            }

            Email.SendEmail(fromClubOwner, new MailAddress(toClubOwner[0]), subject + " from " + toPrimaryPlayerEmail, body, true, null, ccListClubOwner);
        }

        public static void SendCashConfirmEmail(string toPrimaryPlayerEmail, string subject, string body, string clubOwnerName)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubOwnerName);
            SendEmail(fromClubOwner, new MailAddress(toPrimaryPlayerEmail), subject, body, true);
        }

        public static void SendConfirmEventReg(string to, string user, string name, Order[] orders, bool bccToSysAdmin, string cid, DateTime orderDate)
        {
            var body = FormatEmail.ConfirmEventReg(user, name, orders, cid, orderDate);
            var subj = Constants.F_ConfirmEventRegSubject;
            var from = new MailAddress(Constants.W_NoReplyEmailAddress, Constants.W_Jumbula);
            Email.SendEmail(from, new MailAddress(to), subj, body, true);
            if (bccToSysAdmin)
            {
                Email.SendEmail(from, new MailAddress(Constants.W_BCC_EmailNotification), subj, body, true);
            }
        }

        public static void SendCancelOrderReg(string to, string user, string name, Order order, bool bccToSysAdmin, int orderID, DateTime orderDate)
        {
            var body = FormatEmail.CancelOrderReg(user, name, order, orderID, orderDate);
            var subj = Constants.F_CancelOrderRegSubject;
            var from = new MailAddress(Constants.W_NoReplyEmailAddress, Constants.W_Jumbula);
            Email.SendEmail(from, new MailAddress(to), subj, body, true);
            if (bccToSysAdmin)
            {
                Email.SendEmail(from, new MailAddress(Constants.W_BCC_EmailNotification), subj, body, true);
            }
        }

        public static void SendClubOwnerEventReg(string to, string confirmationId, DateTime OrderDate, List<Order> orders, string ClubOwnerName)
        {
            var body = FormatEmail.NotifyClubOwnerEventReg(confirmationId, OrderDate, orders, ClubOwnerName);
            var subj = Constants.F_NotifyClubOwnerEventRegSubject;
            var from = new MailAddress(Constants.W_NoReplyEmailAddress, Constants.W_Jumbula);
            Email.SendEmail(from, new MailAddress(to), subj, body, true);
        }

        public static void SendClubOwnerCancelOrder(string to, int orderID, DateTime OrderDate, Order order, string ClubOwnerName)
        {
            var body = FormatEmail.NotifyClubOwnerCancelOrder(orderID, OrderDate, order, ClubOwnerName);
            var subj = Constants.F_NotifyClubOwnerCancelOrderSubject;
            var from = new MailAddress(Constants.W_NoReplyEmailAddress, Constants.W_Jumbula);
            Email.SendEmail(from, new MailAddress(to), subj, body, true);
        }

        public static void SendConfirmNotifyImportedClub(string to, string user, string name, string baseUrl, string token, int tokenIsValidFor)
        {
            var body = FormatEmail.ConfirmNotifyImportedClub(user, name, baseUrl, token, tokenIsValidFor);
            var subj = string.Format(Constants.F_ConfirmNotifyImportedClubSubject, name);
            var from = new MailAddress(Constants.W_NoReplyEmailAddress, Constants.W_Jumbula);
            MailAddress bcc = null;
            if (sendEmailToSupport)
            {
                bcc = new MailAddress(Constants.W_BCC_EmailNotification);
            }

            Email.SendEmail(from, new MailAddress(to), subj, body, true, bcc);
        }

        public static void SendConfirmNotifyImportedClubSuccess(string to, string name)
        {
            var body = FormatEmail.ConfirmNotifyImportedClubSuccess(name);
            var subj = string.Format(Constants.F_ConfirmEmailSuccessSubject, to);
            var from = new MailAddress(Constants.W_NoReplyEmailAddress, Constants.W_Jumbula);
            MailAddress bcc = null;
            if (sendEmailToSupport)
            {
                bcc = new MailAddress(Constants.W_BCC_EmailNotification);
            }
            Email.SendEmail(from, new MailAddress(to), subj, body, true, bcc);
        }

        public static void SendFeedbackToSupport(string idea, string shortSummary, string name, string email)
        {
            var body = FormatEmail.FeedbackToSupport(idea, shortSummary, name, email);
            var subj = string.Format(Constants.F_ReceivedFeedbackToSupportTitle, email);
            var from = new MailAddress(Constants.W_NoReplyEmailAddress, Constants.W_Jumbula);
            Email.SendEmail(from, new MailAddress(Constants.W_JumbulaSupportEmail), subj, body, true);
        }

        public static void SendContactUsToSupport(string name, string email, string subject, string description)
        {
            var body = FormatEmail.ContactUsToSupport(name, email, subject, description);
            var subj = string.Format(Constants.contactUs_Form_New_ContactForm_Received, email);
            var from = new MailAddress(Constants.W_NoReplyEmailAddress, Constants.W_Jumbula);
            Email.SendEmail(from, new MailAddress(Constants.W_JumbulaSupportEmail), subj, body, true);
        }

        public static void SendEmailToRegisteredUsers(string fromMail, string displayName, string to, string cc, string subject, string message, string fileNames = null)
        {
            var from = new MailAddress(fromMail, displayName);
            if (cc == null)
            {
                Email.SendEmail(from, new MailAddress(to), subject, message, true, null, null, displayName, fileNames);
            }
            else
            {
                List<MailAddress> ccList = new List<MailAddress>() { new MailAddress(cc) };
                Email.SendEmail(from, new MailAddress(to), subject, message, true, null, ccList, displayName, fileNames);
            }
        }

        internal static void SendInstallmentReminderEmail(string toPrimaryPlayerEmail, string toSecondaryPlayerEmail, string subject, string body, string clubOwnerName)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubOwnerName);

            if (!string.IsNullOrEmpty(toSecondaryPlayerEmail))
            {
                Email.SendEmail(fromClubOwner, new MailAddress(toPrimaryPlayerEmail), subject, body, true, null, new List<MailAddress>() { new MailAddress(toSecondaryPlayerEmail) });
            }
            else
            {
                Email.SendEmail(fromClubOwner, new MailAddress(toPrimaryPlayerEmail), subject, body, true);
            }

            //if (sendEmailToSupport)
            //{
            Email.SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), subject + " from " + toPrimaryPlayerEmail, body, true);
            //}

        }
    }

    public static class FormatEmail
    {
        private const string ConfirmEventRegLnFormat = "<a href={0}>{1}</a><br />{2}<hr />";

        private static string m_baseUrlPath;
        private static string m_confirmEmail;
        private static string m_confirmEmailSuccess;
        private static string m_confirmFbLoginSuccess;
        private static string m_resetPassword;
        private static string m_resetPasswordSuccess;
        private static string m_confirmEventReg;

        private static string m_cancelOrderReg;
        private static string m_confirmNotifyImportedClub;
        private static string m_confirmNotifyImportedClubSuccess;
        private static string m_NotifyClubOwnerEventReg;
        private static string m_NotifyClubOwnerCancelOrder;
        private static string m_ClaimClub;
        private static string m_EventQuestion;
        private static string m_CashConfirm;
        private static string m_SettlementReport;

        static FormatEmail()
        {
            var url = HttpContext.Current.Request.Url;
            m_baseUrlPath = string.Format(Constants.F_HttpsSchemeAuthority, url.Authority);

            string confirmFbLoginSuccessPath = Path.Combine(TheIo.PhysicalApplicationPath(), Constants.Path_ConfirmFbLoginSuccess);
            string confirmEventRegPath = Path.Combine(TheIo.PhysicalApplicationPath(), Constants.Path_ConfirmEventReg);
            string cancelOrderRegPath = Path.Combine(TheIo.PhysicalApplicationPath(), Constants.Path_CancelOrderReg);
            string confirmNotifyImportedClubPath = Path.Combine(TheIo.PhysicalApplicationPath(), Constants.Path_ConfirmNotifyImportedClub);
            string confirmNotifyImportedClubSuccessPath = Path.Combine(TheIo.PhysicalApplicationPath(), Constants.Path_ConfirmNotifyImportedClubSuccess);
            string notifyClubOwnerEventReg = Path.Combine(TheIo.PhysicalApplicationPath(), Constants.Path_NotifyClubOwnerEventReg);
            string notifyClubOwnerCancelOrder = Path.Combine(TheIo.PhysicalApplicationPath(), Constants.Path_NotifyClubOwnerCancelOrder);
            string claimClub = Path.Combine(TheIo.PhysicalApplicationPath(), Constants.Path_ClaimClub);
            string eventQuestion = Path.Combine(TheIo.PhysicalApplicationPath(), Constants.Path_Event_Question);
            string cashConfirmPath = Path.Combine(TheIo.PhysicalApplicationPath(), Constants.Path_Cash_Confirm);
            string settlementReport = Path.Combine(TheIo.PhysicalApplicationPath(), Constants.Path_Settlement_Report);

            if (File.Exists(confirmFbLoginSuccessPath))
            {
                byte[] buffer = File.ReadAllBytes(confirmFbLoginSuccessPath);
                m_confirmFbLoginSuccess = Encoding.UTF8.GetString(buffer); // HTML files created by Visual Studio are UTF8 encoded
            }
            else
            {
                throw new FileNotFoundException(confirmFbLoginSuccessPath);
            }



            if (File.Exists(cashConfirmPath))
            {
                byte[] buffer = File.ReadAllBytes(cashConfirmPath);
                m_CashConfirm = Encoding.UTF8.GetString(buffer);

            }
            else
            {
                throw new FileNotFoundException(confirmEventRegPath);
            }

            if (File.Exists(confirmEventRegPath))
            {
                byte[] buffer = File.ReadAllBytes(confirmEventRegPath);
                m_confirmEventReg = Encoding.UTF8.GetString(buffer); // HTML files created by Visual Studio are UTF8 encoded

            }
            else
            {
                throw new FileNotFoundException(confirmEventRegPath);
            }

            // load template for cancel event
            if (File.Exists(cancelOrderRegPath))
            {
                byte[] buffer = File.ReadAllBytes(cancelOrderRegPath);
                m_cancelOrderReg = Encoding.UTF8.GetString(buffer); // HTML files created by Visual Studio are UTF8 encoded
            }
            else
            {
                throw new FileNotFoundException(confirmEventRegPath);
            }

            if (File.Exists(confirmNotifyImportedClubPath))
            {
                byte[] buffer = File.ReadAllBytes(confirmNotifyImportedClubPath);
                m_confirmNotifyImportedClub = Encoding.UTF8.GetString(buffer); // HTML files created by Visual Studio are UTF8 encoded
            }
            else
            {
                throw new FileNotFoundException(confirmNotifyImportedClubPath);
            }

            if (File.Exists(confirmNotifyImportedClubSuccessPath))
            {
                byte[] buffer = File.ReadAllBytes(confirmNotifyImportedClubSuccessPath);
                m_confirmNotifyImportedClubSuccess = Encoding.UTF8.GetString(buffer); // HTML files created by Visual Studio are UTF8 encoded
            }
            else
            {
                throw new FileNotFoundException(confirmNotifyImportedClubSuccessPath);
            }

            if (File.Exists(notifyClubOwnerEventReg))
            {
                byte[] buffer = File.ReadAllBytes(notifyClubOwnerEventReg);
                m_NotifyClubOwnerEventReg = Encoding.UTF8.GetString(buffer); // HTML files created by Visual Studio are UTF8 encoded
            }
            else
            {
                throw new FileNotFoundException(notifyClubOwnerEventReg);
            }

            // notify email template for cancel order
            if (File.Exists(notifyClubOwnerCancelOrder))
            {
                byte[] buffer = File.ReadAllBytes(notifyClubOwnerEventReg);
                m_NotifyClubOwnerCancelOrder = Encoding.UTF8.GetString(buffer); // HTML files created by Visual Studio are UTF8 encoded
            }
            else
            {
                throw new FileNotFoundException(notifyClubOwnerCancelOrder);
            }

            // claim club email
            if (File.Exists(claimClub))
            {
                byte[] buffer = File.ReadAllBytes(claimClub);

                // HTML files created by Visual Studio are UTF8 encoded
                m_ClaimClub = Encoding.UTF8.GetString(buffer);
            }
            else
            {
                throw new FileNotFoundException(claimClub);
            }

            // Event question email
            if (File.Exists(eventQuestion))
            {
                byte[] buffer = File.ReadAllBytes(eventQuestion);

                // HTML files created by Visual Studio are UTF8 encoded
                m_EventQuestion = Encoding.UTF8.GetString(buffer);
            }
            else
            {
                throw new FileNotFoundException(eventQuestion);
            }

            // Event question email
            if (File.Exists(settlementReport))
            {
                byte[] buffer = File.ReadAllBytes(settlementReport);

                // HTML files created by Visual Studio are UTF8 encoded
                m_SettlementReport = Encoding.UTF8.GetString(buffer);
            }
            else
            {
                throw new FileNotFoundException(settlementReport);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// The hardcoded strings "user" and "token" must match the action ConfirmEmail.
        /// Do NOT change these values.
        /// </remarks>
        /// <param name="user"></param>
        /// <param name="name"></param>
        /// <param name="baseUrl"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public static string ConfirmEmailUrl(string user, string baseUrl, string token, string returnUrl, string additionalParameter)
        {
            StringBuilder url = new StringBuilder();
            TheIo.AppendNvArg(url, "user", user);
            TheIo.AppendNvArg(url, "token", token);
            TheIo.AppendNvArg(url, "returnUrl", returnUrl);

            // additionalParameter = add this value add the end of url
            url.Append(additionalParameter);

            url.Insert(0, baseUrl + Constants.S_QuestionMark);

            return url.ToString();
        }

        public static string ConfirmEmailSuccess(string name)
        {
            return string.Format(m_confirmEmailSuccess, m_baseUrlPath, name);
        }

        public static string SendConfirmFbLoginSuccess(string user, string name, string baseUrl, string token)
        {
            StringBuilder url = new StringBuilder();
            TheIo.AppendNvArg(url, "user", user);
            TheIo.AppendNvArg(url, "token", token);
            url.Insert(0, baseUrl + Constants.S_QuestionMark);
            return string.Format(m_confirmFbLoginSuccess, m_baseUrlPath, name, url);
        }

        public static string ResetPassword(string user, string name, string baseUrl, string token, string additionalparameter)
        {

            StringBuilder url = new StringBuilder();
            TheIo.AppendNvArg(url, "user", user);
            TheIo.AppendNvArg(url, "token", token);

            // additionalparameter =  add this value at the end of parameter
            url.Append(additionalparameter);

            url.Insert(0, baseUrl + Constants.S_QuestionMark);
            return string.Format(m_resetPassword, m_baseUrlPath, name, url);
        }

        public static string ResetPasswordSuccess(string name)
        {
            return string.Format(m_resetPasswordSuccess, m_baseUrlPath, name);
        }



        public static string GetTemplateOfCashConfirmEmail()
        {
            return m_CashConfirm;
        }

        public static string ConfirmEventReg(string user, string name, Order[] orders, string cid, DateTime orderDate)
        {
            string s = string.Empty;
            foreach (var order in orders)
            {
                string details = "";
                foreach (OrderItem item in order.OrderItems)
                {
                    string ln = m_baseUrlPath + Constants.S_Slash;//+ item.Domain + Constants.S_Slash + item.SubDomain;
                    details += string.Format(ConfirmEventRegLnFormat, ln, item.Name, item.Description.Replace("&lt;br/&gt; ", "-"));
                }

                s = string.Format("Online registration for {0} {1}: {2}", order.FirstName, order.LastName, details);
            }

            return string.Format(m_confirmEventReg,
                m_baseUrlPath,
                cid,
                Utilities.FormatDateWithoutTime(orderDate),
                Utilities.FormatCurrencyWithoutPenny(orders.Sum(m => m.OrderAmount)),
                s);
        }

        public static string CancelOrderReg(string user, string name, Order order, int orderID, DateTime orderDate)
        {
            string s = string.Empty;

            string details = "";
            foreach (OrderItem item in order.OrderItems)
            {
                string ln = m_baseUrlPath + Constants.S_Slash;// +item.Domain + Constants.S_Slash + item.SubDomain;
                details += string.Format(ConfirmEventRegLnFormat, ln, item.Name, item.Description.Replace("&lt;br/&gt; ", "-"));
            }

            s = string.Format("Online registration for {0} {1}: {2}", order.FirstName, order.LastName, details);


            return string.Format(m_cancelOrderReg,
                m_baseUrlPath,
                orderID,
                Utilities.FormatDateWithoutTime(orderDate),
                Utilities.FormatCurrencyWithoutPenny(order.OrderAmount),
                s);
        }


        public static string NotifyClubOwnerEventReg(string confirmationId, DateTime OrderDate, List<Order> orders, string name)
        {
            string s = string.Empty;

            foreach (var order in orders)
            {
                string details = "";
                foreach (OrderItem item in order.OrderItems)
                {
                    string ln = m_baseUrlPath + Constants.S_Slash;// +item.Domain + Constants.S_Slash + item.SubDomain;
                    details += string.Format(ConfirmEventRegLnFormat, ln, item.Name, item.Description.Replace("&lt;br/&gt; ", "-"));
                    //orderTotal += item.TotalPrice;
                }

                s = string.Format("Online registration for {0} {1}: {2}", order.FirstName, order.LastName, details);
            }

            return string.Format(m_NotifyClubOwnerEventReg, m_baseUrlPath, name,
                confirmationId,
                Utilities.FormatDateWithoutTime(OrderDate),
                Utilities.FormatCurrencyWithoutPenny(orders.Sum(p => p.OrderAmount)),
                s);
        }

        public static string NotifyClubOwnerCancelOrder(int orderID, DateTime OrderDate, Order order, string name)
        {
            string s = string.Empty;

            string details = "";
            foreach (OrderItem item in order.OrderItems)
            {
                string ln = m_baseUrlPath + Constants.S_Slash;// +item.Domain + Constants.S_Slash + item.SubDomain;
                details += string.Format(ConfirmEventRegLnFormat, ln, item.Name, item.Description.Replace("&lt;br/&gt; ", "-"));
                //orderTotal += item.TotalPrice;
            }

            s = string.Format("Online registration for {0} {1}: {2}", order.FirstName, order.LastName, details);

            return string.Format(m_NotifyClubOwnerCancelOrder, m_baseUrlPath, name,
                orderID,
                Utilities.FormatDateWithoutTime(OrderDate),
                Utilities.FormatCurrencyWithoutPenny(order.OrderAmount),
                s);
        }

        public static string ConfirmNotifyImportedClub(string user, string name, string baseUrl, string token, int tokenIsValidFor)
        {
            StringBuilder url = new StringBuilder();
            TheIo.AppendNvArg(url, "token", token); // just use token, not user
            url.Insert(0, baseUrl + Constants.S_QuestionMark);
            return string.Format(m_confirmNotifyImportedClub, m_baseUrlPath, name, url, tokenIsValidFor);
        }

        public static string ConfirmNotifyImportedClubSuccess(string name)
        {
            return string.Format(m_confirmNotifyImportedClubSuccess, m_baseUrlPath, name);
        }

        public static string FeedbackToSupport(string idea, string shortSummary, string name, string email)
        {
            return string.Format("<h2>The following feedback has been received</h2> <br /> <strong>Idea:</strong>{0} <br /><strong>Summary:</strong>{1} <br /><strong>From:</strong>{2}  <a href='{3}'>{3}</a>", idea, shortSummary, name, email);
        }

        public static string ContactUsToSupport(string name, string email, string subject, string description)
        {
            return string.Format("<h2>The following contact us has been received</h2> <br /> <strong>Subject:</strong>{2} <br /><strong>Description:</strong>{3} <br /><strong>From:</strong>{0}  <a href='mailto:{1}'>{1}</a>", name, email, subject, description);
        }

        public static string GetTemplateOfClaimClub()
        {
            return m_ClaimClub;
        }

        public static string GetTemplateOfEventQuestion()
        {
            return m_EventQuestion;
        }

        public static string GetTemplateOfSettlementReport()
        {
            return m_SettlementReport;
        }
    }
}
