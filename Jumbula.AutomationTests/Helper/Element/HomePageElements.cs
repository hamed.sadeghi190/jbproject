﻿using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element
{
    public class HomePageElements : BaseElements
    {
        public HomePageElements(IWebDriver driver)
            : base(driver)
        {

        }

        public IWebElement LoginLink
        {
            get
            {
                return Driver.FindElement(By.LinkText("Login / Sign up")); ;
            }
        }
    }
}
