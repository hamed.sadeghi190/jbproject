﻿namespace Jumbula.Common.Enums
{
    public enum EmailSendStatus : byte
    {
        // First time email place in db
        Accepted,
        Pending,
        // Email provider accepted for delivery
        Processed,
        Canceled,
        // Email provider failed to accept email
        Failed
    }
}
