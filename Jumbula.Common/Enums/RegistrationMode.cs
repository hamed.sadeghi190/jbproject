﻿

namespace Jumbula.Common.Enums
{
    public enum RegistrationMode : byte
    {
        Open = 0,
        Restricted = 1
    }
}
