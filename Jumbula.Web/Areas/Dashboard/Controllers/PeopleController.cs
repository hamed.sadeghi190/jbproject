﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Identity;
using Jumbula.Core.Model.CommunicationHistory;
using Jumbula.Core.Model.Generic;
using Jumbula.Core.Model.People;
using Jumbula.Web.Areas.Dashboard.Services;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.Payment;
using Jumbula.Web.WebLogic.UserService;
using Microsoft.AspNet.Identity.Owin;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using Jumbula.Core.Model.Email;
using Constants = Jumbula.Common.Constants.Constants;
using Jumbula.Web.Infrastructure.Flyer;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class PeopleController : DashboardBaseController
    {
        #region Fields
        private readonly IPeopleBusiness _peopleBusiness;
        private readonly IClubBusiness _clubBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;

        private readonly IApplicationUserManager<JbUser, int> _applicationUserManager;
        private readonly IProgramBusiness _programBusiness;
        private readonly ICommunicationHistoryBusiness _communicationHistoryBusiness;
        private readonly IEmailBusiness _emailBusiness;

        #endregion

        #region Constractors
        public PeopleController(IClubBusiness clubBusiness, IApplicationUserManager<JbUser, int> applicationUserManager, IProgramBusiness programBusiness, IPeopleBusiness peopleBusiness, ICommunicationHistoryBusiness communicationHistoryBusiness, IEmailBusiness emailBusiness, IOrderItemBusiness orderItemBusiness)
        {
            _applicationUserManager = applicationUserManager;
            _programBusiness = programBusiness;
            _peopleBusiness = peopleBusiness;
            _clubBusiness = clubBusiness;
            _communicationHistoryBusiness = communicationHistoryBusiness;
            _emailBusiness = emailBusiness;
            _orderItemBusiness = orderItemBusiness;
        }
        #endregion

        public virtual ActionResult AddUser()
        {
            return View();
        }
        public virtual ActionResult DisplayFamilyTakePaymentPaymentMessage()
        {
            return View();
        }

        public virtual ActionResult Families()
        {
            return View();
        }
        public virtual ActionResult FamilyDetail()
        {
            return View();
        }
        public virtual ActionResult FamilyOrders()
        {
            return View();
        }
        public virtual ActionResult FamilyTransactions()
        {
            return View();
        }
        public virtual ActionResult FamilyTakePayment()
        {
            return View();
        }
        public virtual ActionResult ManageFamily()
        {
            return View();
        }
        public virtual ActionResult ConfirmFamilyTakePayment()
        {
            return View();
        }

        public virtual ActionResult Registrant()
        {
            return View();
        }

        public virtual ActionResult ParentRegistration()
        {
            return View();
        }

        public virtual ActionResult RegistrationDetail()
        {
            return View();
        }

        public virtual ActionResult ParentDetail()
        {
            return View();
        }

        public virtual ActionResult ManageInvoicesOrTakePayment()
        {
            return View();
        }
        public virtual ActionResult DependentCareReceiptReport(DependentCareReceiptReportViewModel model)
        {
            var years = new List<int>();

            for (int i = DateTime.Now.Year; i >= DateTime.Now.AddYears(-2).Year; i--)
            {
                years.Add(i);
            }

            ViewBag.Years = years.Select(y =>
            new SelectListItem
            {
                Text = y.ToString(),
                Value = y.ToString()
            })
            .ToList();


            return View(model);
        }
        public virtual ActionResult ConfirmFamilyInvoice()
        {
            return View();
        }
        public virtual ActionResult DisplayInvoiceMessage()
        {
            return View();
        }
        [HttpPost]
        public virtual JsonNetResult GetRegistrantPeople(string term, PeopleSearchType? searchType, string firstName = null, string lastName = null, List<long> seasonIds = null, string seasonName = null, int year = 0, List<long> selectedSchools = null, List<long> programIds = null, List<GridSort> sort = null)
        {
            var clubBusiness = Ioc.ClubBusiness;

            var club = clubBusiness.Get(ActiveClub.Id);

            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);

            var peopleModel = new List<PeopleRegistrantViewModel>();

            IQueryable<OrderItem> clubOrderItems;
            if (club.IsPartner)
            {
                var schools = clubBusiness.GetAllPartnerSchools(club.Id).ToList();
                var schoolIds = schools.Select(s => s.Id).ToList();

                clubOrderItems = Ioc.OrderItemBusiness.GetCompletetCancelOrderItems(schoolIds).Where(c => c.Order.IsLive && c.PlayerId.HasValue && c.Player.Relationship == RelationshipType.Registrant);
            }
            else
            {
                clubOrderItems = Ioc.OrderItemBusiness.GetClubCompletetCancelOrderItems(ActiveClub.Id).Where(c => c.Order.IsLive && c.PlayerId.HasValue && c.Player.Relationship == RelationshipType.Registrant);
            }

            if (!string.IsNullOrEmpty(term))
            {
                term = term.ToLower();
            }

            if (selectedSchools != null && selectedSchools.Any() && selectedSchools.First() != 0)
            {
                clubOrderItems = clubOrderItems.Where(item => selectedSchools.Contains(item.Order.ClubId));
            }

            if (searchType.HasValue)
            {
                switch (searchType.Value)
                {
                    case PeopleSearchType.Email:
                        {
                            if (!string.IsNullOrWhiteSpace(term))
                            {
                                clubOrderItems = clubOrderItems.Where(item => item.Order.User.UserName.ToLower() == term);
                            }
                        }
                        break;
                    case PeopleSearchType.ConfirmationId:
                        {
                            if (!string.IsNullOrWhiteSpace(term))
                            {
                                clubOrderItems = clubOrderItems.Where(item => item.Order.ConfirmationId.Equals(term, StringComparison.OrdinalIgnoreCase));
                            }
                        }
                        break;
                    case PeopleSearchType.Name:
                        {
                            if (!string.IsNullOrWhiteSpace(firstName) || !string.IsNullOrWhiteSpace(lastName))
                            {
                                clubOrderItems = clubOrderItems.Where(item => (string.IsNullOrEmpty(firstName) || item.Player.Contact.FirstName.ToLower().Contains(firstName.ToLower())) && (string.IsNullOrEmpty(lastName) || item.Player.Contact.LastName.ToLower().Contains(lastName.ToLower())));
                            }
                        }
                        break;
                    case PeopleSearchType.Phone:
                        {
                            if (!string.IsNullOrWhiteSpace(term))
                            {
                                term = term.Replace("-", "").Replace("(", "").Replace(")", "");

                                var profilesQuery = Ioc.UserProfileBusiness.GetPlayerProfiles(clubOrderItems.Select(r => r.Order.UserId).ToList());
                                var profileResult = Enumerable.Empty<PlayerProfile>();

                                profileResult = profilesQuery.Where(c => (!string.IsNullOrEmpty(c.Contact.Phone) && c.Contact.Phone.Replace("-", "").Contains(term)) || (!string.IsNullOrEmpty(c.Contact.Cell) && c.Contact.Cell.Replace("-", "").Contains(term)) || (!string.IsNullOrEmpty(c.Contact.Work) && c.Contact.Work.Replace("-", "").Contains(term)));

                                var userIds = profileResult.Select(o => o.UserId).ToList();

                                clubOrderItems = clubOrderItems.Where(u => userIds.Contains(u.Order.UserId));
                            }
                        }
                        break;
                    case PeopleSearchType.Season:
                        {
                            if (seasonIds != null && seasonIds[0] != 0)
                            {
                                clubOrderItems = clubOrderItems.Where(item => item.SeasonId.HasValue && seasonIds.Contains(item.SeasonId.Value));
                            }
                            else if (year != 0)
                            {
                                var season = seasonName + year;
                                season = season.Replace(" ", string.Empty);

                                clubOrderItems = clubOrderItems.Where(item => item.Season.Title.ToLower().Replace(" ", string.Empty).Contains(season.ToLower()));
                            }

                            if (programIds != null && programIds[0] != 0)
                            {
                                clubOrderItems = clubOrderItems.Where(item => programIds.Contains(item.ProgramSchedule.ProgramId));
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            if (!clubOrderItems.Any() && clubOrderItems == null)
            {
                return JsonNet(new { Data = new List<PeopleRegistrantViewModel>(), Total = 0 });
            }

            var groupBy = clubOrderItems.GroupBy(c => new { c.PlayerId, c.Order.ClubId });

            var playerProfiles = groupBy.OrderByDescending(o => o.Key).Skip((page - 1) * pageSize).Take(pageSize).ToList();

            foreach (var profile in playerProfiles)
            {
                var player = profile.ToList().FirstOrDefault().Player;

                peopleModel.Add(new PeopleRegistrantViewModel
                {
                    FullName = player.Contact.FullName,
                    Age = (player.Contact.DoB.HasValue)
                        ? DateTimeHelper.CalculateAge(player.Contact.DoB.Value, DateTime.UtcNow)
                        : 0,
                    Grade = player.Info != null ? player.Info.Grade != 0 ? player.Info.Grade.ToDescription() : string.Empty : string.Empty,
                    UserId = player.UserId,
                    PlayerId = profile.Key.PlayerId.Value,
                    TotalAmount = profile.ToList().Where(i => i.Order.IsLive).Sum(i => i.TotalAmount),
                    ClubName = profile.FirstOrDefault().Order.Club.Name,
                    ClubId = profile.FirstOrDefault().Order.ClubId,
                });
            }

            var dataSource = peopleModel;
            var total = groupBy.Count();

            return JsonNet(new { DataSource = dataSource, TotalCount = total });
        }

        [HttpPost]
        public ActionResult GeneratePdfParticipantLabel(List<string> names, string image, int fontSize)
        {
            dynamic pdfModel = new System.Dynamic.ExpandoObject();

            pdfModel.Data = names;
            pdfModel.Logo = image;
            pdfModel.FontSize = fontSize;

            var flyerGenerator = new FlyerGenerator();
            byte[] file = flyerGenerator.GenerateFromView(this.ControllerContext, "_LogoLastName", pdfModel);
            return File(file, "application/pdf", "Logo.Pdf");
        }

        [HttpGet]
        public virtual JsonNetResult GetAllSchools()
        {
            var clubId = ActiveClub.Id;

            var allSchools = new List<SelectKeyValue<long>>();

            allSchools =
              Ioc.ClubBusiness.GetRelatedClubs(clubId)
                  .Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.School)
                  .Select(c => new SelectKeyValue<long> { Text = c.Name, Value = c.Id })
                  .OrderBy(c => c.Text)
                  .ToList();


            var model = new { AllSchools = allSchools, IsAllSchools = "true" };

            return JsonNet(model);
        }

        public virtual JsonNetResult GetClubSeasonsProvider(int clubId)
        {
            clubId = clubId == 0 ? ActiveClub.Id : clubId;

            List<Season> sortedSeasons = new List<Season>();
            var seasonList = Ioc.SeasonBusiness.GetList().Where(s => s.ClubId == clubId).ToList();
            var sortedSeasonsHasYear = seasonList.Where(s => s.Year.HasValue && s.Year > 0).OrderByDescending(s => s.Year).ThenBy(s => s.Name).ThenByDescending(s => s.MetaData.DateCreated).ToList();
            var sortedSeasonsNotHasYear = seasonList.Where(s => !s.Year.HasValue || s.Year == 0).OrderByDescending(s => s.MetaData.DateCreated).ToList();
            sortedSeasons.AddRange(sortedSeasonsHasYear);
            sortedSeasons.AddRange(sortedSeasonsNotHasYear);

            var model = new List<SelectKeyValue<string>>();

            model.AddRange(sortedSeasons.ToList().Select(c => new SelectKeyValue<string>()
            {
                Text = c.Title,
                Value = c.Id.ToString()
            }).ToList());


            return JsonNet(model);
        }

        public JsonNetResult GetSeasonPrograms(List<long> seasonIds)
        {
            var programs = _programBusiness.GetListBySeasonId(seasonIds);

            var result = programs.Select(p => new SelectKeyValue<long>
            {
                Text = p.Name,
                Value = p.Id
            }).ToList();

            return JsonNet(result);
        }

        public virtual JsonNetResult RegistrationDetails(int userId, int playerId, int clubId = 0, List<GridSort> sort = null)
        {
            clubId = clubId == 0 ? ActiveClub.Id : clubId;
            var club = Ioc.ClubBusiness.Get(clubId);

            var currentClub = Ioc.ClubBusiness.Get(ActiveClub.Id);

            if (currentClub.IsPartner)
                CheckResourceForAccess(club);
            else
            {
                if (clubId != 0 && clubId != ActiveClub.Id)
                    PageNotFound();
            }

            var termValue = Request.Params["term"] ?? string.Empty;

            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);

            var orderItems = Ioc.OrderBusiness.GetClubUserOrderItems(userId, playerId, club.Id);

            IQueryable<OrderItem> filteredOrderItems;

            if (!string.IsNullOrEmpty(termValue))
            {
                if (new Regex(@"[A-Za-z0-9]{4}-[A-Za-z0-9]{2}-[A-Za-z0-9]{4}").Match(termValue.Trim()).Success)
                {
                    filteredOrderItems = orderItems.Where(o => o.Order.ConfirmationId.Equals(termValue));
                }
                else
                {
                    filteredOrderItems =
                        orderItems.Where(
                            item =>
                                item.ProgramSchedule.Program.Name.ToLower().StartsWith(termValue.ToLower()) ||
                                item.ProgramSchedule.Program.Name.ToLower().EndsWith(termValue.ToLower()));
                }
            }
            else
            {
                filteredOrderItems = orderItems;
            }

            var model = (
                filteredOrderItems.OrderByDescending(o => o.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList().Select(orderItem => new RegistrationDetailsViewModel
                {
                    Name = orderItem.ProgramSchedule.Program.Name,
                    PaidAmount = orderItem.TotalAmount,
                    OrderDate = DateTimeHelper.ConvertUtcDateTimeToLocal(orderItem.Order.CompleteDate, club.TimeZone),
                    ConfirmationId = orderItem.Order.ConfirmationId,
                    OrderId = orderItem.Order.Id,
                    ItemStatus = (int)orderItem.ItemStatus,
                    ItemStatusReason = (int)orderItem.ItemStatusReason,
                })).ToList();

            var dataSource = model;
            var totalCount = (!string.IsNullOrEmpty(termValue)) ? filteredOrderItems.Count() : orderItems.Count();

            return JsonNet(new { DataSource = dataSource, TotalCount = totalCount });
        }

        [HttpPost]
        public virtual JsonNetResult GetParentRegistered(string term, PeopleSearchType? searchType, string firstName = null, string lastName = null, string seasonName = null, int year = 0, List<long> selectedSchools = null, List<GridSort> sort = null)
        {
            var clubBusiness = Ioc.ClubBusiness;
            var orderItemBusiness = Ioc.OrderItemBusiness;

            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);

            IQueryable<JbUser> users;
            IQueryable<OrderItem> orderItemsQuery;

            if (club.IsPartner)
            {
                var schools = clubBusiness.GetAllPartnerSchools(club.Id).ToList();
                var schoolIds = schools.Select(s => s.Id).ToList();

                orderItemsQuery = orderItemBusiness.GetCompletetCancelOrderItems(schoolIds).Where(c => c.Order.IsLive);
                users = orderItemsQuery.Select(i => i.Order.User);
            }
            else
            {
                orderItemsQuery = orderItemBusiness.GetClubCompletetCancelOrderItems(ActiveClub.Id).Where(c => c.Order.IsLive);
                users = orderItemsQuery.Select(i => i.Order.User);
            }


            if (!string.IsNullOrEmpty(term))
            {
                term = term.ToLower();
            }

            if (selectedSchools != null && selectedSchools.Any() && selectedSchools.First() != 0)
            {
                var profileResult = Enumerable.Empty<OrderItem>();

                profileResult = orderItemsQuery.Where(c => selectedSchools.Contains(c.Order.ClubId));

                var userIds = profileResult.Select(o => o.Order.UserId).ToList();

                users = users.Where(u => userIds.Contains(u.Id));
            }

            if (searchType.HasValue)
            {
                switch (searchType.Value)
                {
                    case PeopleSearchType.Email:
                        {
                            if (!string.IsNullOrWhiteSpace(term))
                            {
                                users = users.Where(item => item.UserName.ToLower() == term);
                            }
                        }
                        break;
                    case PeopleSearchType.ConfirmationId:
                        {
                            if (!string.IsNullOrWhiteSpace(term))
                            {
                                var userOrderItem = orderItemsQuery.FirstOrDefault(o => o.Order.ConfirmationId.Equals(term, StringComparison.OrdinalIgnoreCase));
                                if (userOrderItem != null)
                                {
                                    var player = userOrderItem.Order.UserId;
                                    users = users.Where(item => item.Id == player);
                                }
                                else
                                {
                                    users = Enumerable.Empty<JbUser>().AsQueryable();
                                }

                            }
                        }
                        break;
                    case PeopleSearchType.Name:
                        {
                            if (!string.IsNullOrWhiteSpace(firstName) || !string.IsNullOrWhiteSpace(lastName))
                            {

                                var profileQuery = Ioc.UserProfileBusiness.GetPlayerProfiles(users.Select(r => r.Id).ToList());
                                var profileResult = Enumerable.Empty<PlayerProfile>();

                                profileResult = profileQuery.Where(item => (string.IsNullOrEmpty(firstName) || item.Contact.FirstName.ToLower().Contains(firstName.ToLower())) && (string.IsNullOrEmpty(lastName) || item.Contact.LastName.ToLower().Contains(lastName.ToLower())));

                                var userIds = profileResult.Select(o => o.UserId).ToList();

                                users = users.Where(u => userIds.Contains(u.Id));
                            }
                        }
                        break;
                    case PeopleSearchType.Phone:
                        {
                            if (!string.IsNullOrWhiteSpace(term))
                            {
                                term = term.Replace("-", "").Replace("(", "").Replace(")", "");

                                var profilesQuery = Ioc.UserProfileBusiness.GetPlayerProfiles(users.Select(r => r.Id).ToList());
                                var profileResult = Enumerable.Empty<PlayerProfile>();

                                profileResult = profilesQuery.Where(c => (!string.IsNullOrEmpty(c.Contact.Phone) && c.Contact.Phone.Replace("-", "").Contains(term)) || (!string.IsNullOrEmpty(c.Contact.Cell) && c.Contact.Cell.Replace("-", "").Contains(term)) || (!string.IsNullOrEmpty(c.Contact.Work) && c.Contact.Work.Replace("-", "").Contains(term)));

                                var userIds = profileResult.Select(o => o.UserId).ToList();


                                users = users.Where(u => userIds.Contains(u.Id));
                            }
                        }
                        break;
                    case PeopleSearchType.Season:
                        {
                            if (year != 0)
                            {
                                var season = seasonName + year;
                                season = season.Replace(" ", string.Empty);

                                var userOrderItem = orderItemsQuery.Where(item => item.Season.Title.ToLower().Replace(" ", string.Empty).Contains(season.ToLower()));
                                if (userOrderItem != null)
                                {
                                    var userIds = userOrderItem.Select(u => u.Order.UserId);
                                    users = users.Where(item => userIds.Contains(item.Id));
                                }
                                else
                                {
                                    users = Enumerable.Empty<JbUser>().AsQueryable();
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            var model = new List<PeopleRegistrantViewModel>();

            var ids = users.Select(u => u.Id).Distinct().ToList();

            if (ids == null && !ids.Any())
            {
                return JsonNet(new { Data = new List<PeopleRegistrantViewModel>(), Total = 0 });
            }

            var profiles = Ioc.PlayerProfileBusiness
                .GetList()
                .Where(p => ids.Contains(p.UserId) && p.Relationship == RelationshipType.Parent && p.Status != PlayerProfileStatusType.Deleted);


            var playerProfiles = profiles
                .OrderByDescending(o => o.Id)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();

            var playerUserIds = playerProfiles.Select(p => p.UserId).ToList();

            var clubUsers = orderItemsQuery.Where(o => playerUserIds.Contains(o.Order.UserId)).Select(p => new
            {
                p.Order.UserId,
                p.Order.Club
            }).ToList();


            if (club.IsPartner)
            {
                var allOrderItems = orderItemsQuery.ToList();

                foreach (var playerProfile in playerProfiles)
                {
                    var clubName = selectedSchools != null && selectedSchools.First() != 0 ?
                        string.Join(", ", clubUsers.Where(a => selectedSchools.Contains(a.Club.Id) && a.UserId == playerProfile.UserId).Select(a => a.Club.Name).Distinct()) :
                            string.Join(", ", clubUsers.Where(a => a.UserId == playerProfile.UserId).Select(a => a.Club.Name).Distinct());


                    model.Add(new PeopleRegistrantViewModel
                    {
                        FullName = playerProfile.Contact.FullName,
                        PhoneNumber = playerProfile.Contact.Phone,
                        Email = playerProfile.Contact.Email,
                        UserId = playerProfile.UserId,
                        PlayerId = playerProfile.Id,
                        ClubName = clubName
                    });
                }
            }
            else
            {
                foreach (var playerProfile in playerProfiles)
                {
                    model.Add(new PeopleRegistrantViewModel
                    {
                        FullName = playerProfile.Contact.FullName,
                        PhoneNumber = playerProfile.Contact.Phone,
                        Email = playerProfile.Contact.Email,
                        UserId = playerProfile.UserId,
                        PlayerId = playerProfile.Id
                    });
                }
            }


            var dataSource = model;
            var totalCount = profiles.Count();

            return JsonNet(new { DataSource = dataSource, TotalCount = totalCount });
        }

        [HttpGet]
        public virtual JsonNetResult GetCurrentYearsFrom2015()
        {
            var club = ActiveClub;
            var years = new List<int>();

            for (int i = DateTime.Now.AddYears(1).Year; i >= 2015; i--)
            {
                years.Add(i);
            }

            var selectYears = years.Select(y =>
            new SelectListItem
            {
                Text = y.ToString(),
                Value = y.ToString()
            })
            .ToList();


            var model = new
            {
                ClubName = ActiveClub.Name,
                Date = Ioc.ClubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt"),//Sep 21, 2016 HH:MM                
                Years = selectYears,
            };

            return JsonNet(model);
        }

        public virtual JsonNetResult GetUserListDetail(int userId, int clubId = 0)
        {
            var model = new FamilyViewModel();
            var clubBusiness = Ioc.ClubBusiness;
            var club = clubBusiness.Get(ActiveClub.Id).IsPartner ? clubBusiness.Get(clubId) : clubBusiness.Get(ActiveClub.Id);

            if (clubBusiness.IsUserBelongToTheClub(club.Id, userId))
            {

                var user = Ioc.UserProfileBusiness.Get(userId);
                var orderItems = Ioc.OrderItemBusiness.GetUserOrderItems(userId, false).Where(c => c.Order.ClubId == club.Id && c.ItemStatus == OrderItemStatusCategories.completed || (c.ItemStatus == OrderItemStatusCategories.changed && c.ItemStatusReason == OrderItemStatusReasons.canceled));
                if (orderItems.Any())
                {
                    model.TotalOrders = orderItems.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).Any() ? orderItems.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).Count() : 0;
                    model.TotalPayments = orderItems.Sum(c => c.PaidAmount);
                }
                model.UserName = user.UserName;
                model.UserId = user.Id;
                model.TagName = user.TagName ?? "";

                model.StatusReason = new List<JbTitleValue>();
                model.StatusReason.Add(new JbTitleValue() { Title = "All", Value = -1 });
                model.StatusReason.AddRange(Enum.GetValues(typeof(OrderItemStatusReasons))
                    .Cast<OrderItemStatusReasons>()
                    .Select(v => new JbTitleValue() { Title = v.ToDescription(), Value = (int)v })
                    .ToList());

                model.Participants = new List<JbTitleValue>();
                model.Participants.Add(new JbTitleValue() { Title = "All", Value = -1 });

                var userPlayers = Ioc.UserProfileBusiness.GetPlayerProfiles(user.Id).Where(p => p.Relationship != RelationshipType.Parent);
                var clubUserPlayers = userPlayers.Where(p => p.OrderItems.Any(i => (i.ItemStatus == OrderItemStatusCategories.completed || i.ItemStatusReason == OrderItemStatusReasons.canceled || i.ItemStatusReason == OrderItemStatusReasons.transferOut) && i.Order.ClubId == club.Id && i.Order.IsLive));

                if (clubUserPlayers.Any())
                {
                    model.Participants.AddRange(clubUserPlayers.Where(c => c.Status != PlayerProfileStatusType.Deleted).Select(t => new JbTitleValue() { Title = (t.Contact != null ? t.Contact.FirstName + " " + t.Contact.LastName : ""), Value = t.Id }).OrderBy(c => c.Title));
                }

            }

            return JsonNet(model);
        }
        public virtual JsonNetResult FamilyDetails(int userId, int clubId = 0)
        {
            var clubBusiness = Ioc.ClubBusiness;
            var club = clubBusiness.Get(ActiveClub.Id).IsPartner ? clubBusiness.Get(clubId) : clubBusiness.Get(ActiveClub.Id);

            var clubUser = club.Users.SingleOrDefault(u => u.UserId == userId);

            if (clubUser != null)
                CheckResourceForAccess(clubUser);
            else
                PageNotFound();

            var user = Ioc.UserProfileBusiness.Get(userId);
            var AccountService = Ioc.AccountingBusiness;
            var OrderItems = Ioc.OrderItemBusiness.GetClubUserOrderItems(club.Id, userId, false, OrderItemStatusCategories.showAll).Where(c => c.ItemStatus == OrderItemStatusCategories.completed || (c.ItemStatus == OrderItemStatusCategories.changed && c.ItemStatusReason == OrderItemStatusReasons.canceled));

            var model = new FamilyViewModel();
            if (user != null)
            {
                var playerProfiles = Ioc.UserProfileBusiness.GetPlayerProfiles(user.Id);

                model.UserId = user.Id;
                model.UserName = user.UserName;
                model.HasParent = playerProfiles.Any(c => c.Relationship == RelationshipType.Parent);
                model.HasParticipant = playerProfiles.Any(c => c.Relationship == RelationshipType.Registrant);
                model.TagName = user.TagName ?? "";
                if (OrderItems.Any())
                {
                    model.Balance = AccountService.OrderItemsBalance(OrderItems);
                    model.Credit = 0;
                    model.TotalOrders = OrderItems.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).Any() ? OrderItems.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).Count() : 0;
                    model.TotalPayments = OrderItems.Sum(c => c.PaidAmount);
                }
                model.Credit = clubBusiness.GetUserCredit(club, user.Id, false);
                model.ClubId = club.Id;
            }

            return JsonNet(model);
        }
        public virtual JsonNetResult GetParentsList(int userId)
        {
            var club = ActiveClub;
            this.EnsureUserIsAdminForClub(club.Domain);

            var user = Ioc.UserProfileBusiness.Get(userId);

            var model = new List<ParentDetailsViewModel>();

            var playerProfiles = Ioc.UserProfileBusiness.GetPlayerProfiles(userId);

            if (user != null && playerProfiles.Any(c => c.Relationship == RelationshipType.Parent && c.Status != PlayerProfileStatusType.Deleted))
            {
                model = playerProfiles.Where(c => c.Relationship == RelationshipType.Parent && c.Status != PlayerProfileStatusType.Deleted).ToList().Select(c => new ParentDetailsViewModel()
                {

                    FirstName = c.Contact != null ? c.Contact.FirstName : "",
                    LastName = c.Contact != null ? c.Contact.LastName : "",
                    EmailAddress = c.Contact != null ? c.Contact.Email : "-",
                    Gender = (c.Gender == GenderCategories.Male || c.Gender == GenderCategories.Female || c.Gender == GenderCategories.GenderNeutral) ? c.Gender.ToDescription() : "-",
                    Phone = c.Contact != null ? (string.IsNullOrEmpty(c.Contact.Phone) ? c.Contact.Cell : c.Contact.Phone) : "-",

                    DOB = (c.Contact != null && c.Contact.DoB.HasValue) ? c.Contact.DoB.Value.ToString(Constants.DefaultDateFormat) : ""

                }).ToList();


            }

            return JsonNet(new { data = model, total = model.Count() }, new Newtonsoft.Json.JsonSerializerSettings(), "");
        }
        public virtual JsonNetResult GetParticipantList(int userId)
        {
            var club = ActiveClub;
            this.EnsureUserIsAdminForClub(club.Domain);

            var user = Ioc.UserProfileBusiness.Get(userId);
            var orders = Ioc.OrderItemBusiness.GetClubUserOrderItems(club.Id, userId, false, OrderItemStatusCategories.showAll).Where(c => c.ItemStatus == OrderItemStatusCategories.completed || (c.ItemStatus == OrderItemStatusCategories.changed && c.ItemStatusReason == OrderItemStatusReasons.canceled));
            var model = new List<PeopleRegistrantViewModel>();

            if (user != null)
            {
                var userPlayers = Ioc.UserProfileBusiness.GetPlayerProfiles(user.Id).Where(p => p.Relationship == RelationshipType.Registrant);
                var playerProfiles = userPlayers.Where(p => p.OrderItems.Any(i => (i.ItemStatus == OrderItemStatusCategories.completed || i.ItemStatusReason == OrderItemStatusReasons.canceled || i.ItemStatusReason == OrderItemStatusReasons.transferOut) && i.Order.ClubId == club.Id && i.Order.IsLive));

                if (playerProfiles.Any(c => c.Relationship == RelationshipType.Registrant && c.Status != PlayerProfileStatusType.Deleted))
                {
                    var AccountService = Ioc.AccountingBusiness;
                    foreach (var participant in playerProfiles.Where(c => c.Status != PlayerProfileStatusType.Deleted))
                    {
                        var participantModel = new PeopleRegistrantViewModel()
                        {
                            FullName =
                                participant.Contact != null
                                    ? string.Format(Constants.F_FullName, participant.Contact.FirstName,
                                        participant.Contact.LastName)
                                    : "",
                            DOB =
                                (participant.Contact != null && participant.Contact.DoB.HasValue)
                                    ? participant.Contact.DoB.Value.ToString(Constants.DefaultDateFormat)
                                    : "",
                            Email = participant.Contact != null ? participant.Contact.Email : "",
                            Gender =
                                (participant.Gender == GenderCategories.Male ||
                                 participant.Gender == GenderCategories.Female || participant.Gender == GenderCategories.GenderNeutral)
                                    ? participant.Gender.ToDescription()
                                    : "-",
                            Graduated = participant.EducationalStatus == EducationalStatus.graduated ? participant.EducationalStatus.ToDescription() : null,
                            PlayerId = participant.Id,
                            Id = participant.Id,
                            UserId = participant.UserId
                        };
                        if (orders.Any())
                        {

                            var playerOrders = orders.Where(c => c.PlayerId == participant.Id);
                            participantModel.Balance = AccountService.OrderItemsBalance(playerOrders);
                            participantModel.TotalOrders =
                                playerOrders.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).Any()
                                    ? playerOrders.Where(c => c.ItemStatus == OrderItemStatusCategories.completed)
                                        .Count()
                                    : 0;
                        }
                        model.Add(participantModel);
                    }


                }

                return JsonNet(new { data = model, total = model.Count() }, new Newtonsoft.Json.JsonSerializerSettings(), "");
            }

            return JsonNet(new { total = 0 });

        }

        public virtual JsonNetResult ParentDetails(int userId, int playerId)
        {
            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

            if (club.IsPartner)
            {
                if (!Ioc.OrderBusiness.UserHasAnyOrderOnPartnerClub(userId, club.Id))
                    HttpNotFound();
            }
            else
            {
                if (!Ioc.OrderBusiness.UserHasAnyOrderOnClub(userId, club.Id))
                    HttpNotFound();
            }

            var user = Ioc.UserProfileBusiness.Get(userId);

            var playerProfiles = Ioc.UserProfileBusiness.GetPlayerProfiles(user.Id);

            var parentProfile =
                playerProfiles.Single(
                    m => m.Relationship == RelationshipType.Parent && m.Id == playerId);

            var model = new ParentDetailsViewModel
            {
                FirstName =
                    parentProfile.Contact.FirstName,
                LastName = parentProfile.Contact.LastName,
                EmailAddress = parentProfile.Contact.Email,
                Age = (parentProfile.Contact.DoB.HasValue) ? DateTimeHelper.CalculateAge(parentProfile.Contact.DoB.Value, DateTime.UtcNow) : 0,
                Gender = parentProfile.Gender.ToDescription(),
                Phone = parentProfile.Contact.Phone,
                CellPhone = parentProfile.Contact.Cell,
                DOB = (parentProfile.Contact.DoB.HasValue) ? parentProfile.Contact.DoB.Value.ToString(Constants.DefaultDateFormatWith3CharMonth) : "",
                NickName = parentProfile.Contact.Nickname,
                Occupation = parentProfile.Contact.Occupation,
                Work = parentProfile.Contact.Work,
                Employer = parentProfile.Contact.Employer
            };

            return JsonNet(model);
        }

        public virtual JsonNetResult GetParentChild(int userId)
        {
            var club = ActiveClub;
            this.EnsureUserIsAdminForClub(club.Domain);

            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);

            var user = Ioc.UserProfileBusiness.Get(userId);

            var model = new List<ParentDetailsViewModel>();

            var playerProfiles = Ioc.UserProfileBusiness.GetPlayerProfiles(user.Id);

            var childs =
                playerProfiles.Where(profile => profile.Relationship == RelationshipType.Registrant && profile.Status != PlayerProfileStatusType.Deleted).AsQueryable();

            foreach (var profile in childs.OrderBy(c => c.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList())
            {
                var newItem = new ParentDetailsViewModel
                {
                    FirstName =
                           profile.Contact.FirstName,
                    LastName = profile.Contact.LastName,
                    EmailAddress =
                           profile.Contact.Email,
                    Age = (profile.Contact.DoB.HasValue) ? DateTimeHelper.CalculateAge(profile.Contact.DoB.Value, DateTime.UtcNow) : 0,
                    Gender = profile.Gender.ToDescription(),
                    Phone = profile.Contact.Phone,
                    CellPhone = profile.Contact.Cell,
                    DOB = (profile.Contact.DoB.HasValue) ? profile.Contact.DoB.Value.ToShortDateString() : null,
                    NickName = profile.Contact.Nickname,
                    Occupation = profile.Contact.Occupation,
                    Work = profile.Contact.Work,
                    Employer = profile.Contact.Employer
                };
                model.Add(newItem);
            }

            var dataSource = model;
            var totalCount = childs.Count();

            return JsonNet(new { DataSource = dataSource, TotalCount = totalCount });
        }

        [HttpPost]
        public virtual JsonNetResult GetFamilies(string term, PeopleSearchType? searchType, string firstName = null, string lastName = null, string seasonName = null, int year = 0, string creditStatus = null, List<GridSort> sort = null, List<long> selectedSchools = null)
        {
            var clubBusiness = Ioc.ClubBusiness;
            var orderItemBusiness = Ioc.OrderItemBusiness;
            var playerProfileBusiness = Ioc.PlayerProfileBusiness;

            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);
            this.EnsureUserIsAdminForClub(club.Domain);

            if (!string.IsNullOrEmpty(term))
            {
                term = term.ToLower();
            }

            var pageSize = 10;
            int.TryParse(Request.Params["PageSize"], out pageSize);
            var page = 0;
            int.TryParse(Request.Params["Page"], out page);

            IQueryable<ClubUser> query;
            List<int> schoolIds = new List<int>();

            if (club.IsPartner)
            {
                var schools = clubBusiness.GetAllPartnerSchools(ActiveClub.Id).ToList();
                schoolIds = schools.Select(s => s.Id).ToList();

                query = schools.SelectMany(c => c.Users).AsQueryable();
            }
            else
            {
                query = clubBusiness.Get(ActiveClub.Id).Users.AsQueryable();
            }

            if (selectedSchools != null && selectedSchools.Any() && selectedSchools.First() != 0)
            {
                query = query.Where(item => selectedSchools.Contains(item.ClubId));
            }

            if (searchType.HasValue)
            {
                switch (searchType.Value)
                {
                    case PeopleSearchType.Email:
                        {
                            if (!string.IsNullOrWhiteSpace(term))
                            {
                                query = query.Where(item => item.User.UserName.ToLower() == term);
                            }
                        }
                        break;
                    case PeopleSearchType.ConfirmationId:
                        {
                            if (!string.IsNullOrWhiteSpace(term))
                            {
                                IQueryable<OrderItem> orderItemsQuery;
                                if (club.IsPartner)
                                {
                                    orderItemsQuery = orderItemBusiness.GetCompletetCancelOrderItems(schoolIds).Where(o => o.Order.IsLive);
                                }
                                else
                                {
                                    orderItemsQuery = orderItemBusiness.GetClubCompletetCancelOrderItems(ActiveClub.Id).Where(o => o.Order.IsLive);
                                }

                                var userOrderItem = orderItemsQuery.FirstOrDefault(o => o.Order.ConfirmationId.Equals(term, StringComparison.OrdinalIgnoreCase));
                                if (userOrderItem != null)
                                {
                                    query = query.Where(item => item.UserId == userOrderItem.Order.UserId && item.ClubId == userOrderItem.Order.ClubId);
                                }
                                else
                                {
                                    query = Enumerable.Empty<ClubUser>().AsQueryable();
                                }
                            }
                        }
                        break;
                    case PeopleSearchType.Name:
                        {
                            if (!string.IsNullOrWhiteSpace(firstName) || !string.IsNullOrWhiteSpace(lastName))
                            {
                                var profilesQuery = Ioc.UserProfileBusiness.GetPlayerProfiles(query.Select(r => r.UserId).ToList());
                                var profileResult = Enumerable.Empty<PlayerProfile>();

                                profileResult = profilesQuery.Where(item => (string.IsNullOrEmpty(firstName) || item.Contact.FirstName.ToLower().Contains(firstName.ToLower())) && (string.IsNullOrEmpty(lastName) || item.Contact.LastName.ToLower().Contains(lastName.ToLower())));

                                var userIds = profileResult.Select(o => o.UserId).ToList();

                                query = query.Where(u => userIds.Contains(u.UserId));
                            }
                        }
                        break;
                    case PeopleSearchType.Credit:
                        {
                            if (!string.IsNullOrWhiteSpace(creditStatus))
                            {
                                switch (creditStatus)
                                {
                                    case "More than zero":
                                        query = query.Where(item => item.Credit > 0).OrderByDescending(o => o.Credit);
                                        break;

                                    case "Equal to zero":
                                        query = query.Where(item => item.Credit == 0);
                                        break;

                                    case "Less than zero":
                                        query = query.Where(item => item.Credit < 0).OrderBy(o => o.Credit);
                                        break;
                                }
                            }
                        }
                        break;
                    case PeopleSearchType.Phone:
                        {
                            if (!string.IsNullOrWhiteSpace(term))
                            {
                                term = term.Replace("-", "").Replace("(", "").Replace(")", "");

                                var profilesQuery = Ioc.UserProfileBusiness.GetPlayerProfiles(query.Select(r => r.UserId).ToList());
                                var profileResult = Enumerable.Empty<PlayerProfile>();

                                profileResult = profilesQuery.Where(o => (!string.IsNullOrEmpty(o.Contact.Phone) && o.Contact.Phone.Replace("-", "").Contains(term)) || (!string.IsNullOrEmpty(o.Contact.Cell) && o.Contact.Cell.Replace("-", "").Contains(term)) || (!string.IsNullOrEmpty(o.Contact.Work) && o.Contact.Work.Replace("-", "").Contains(term)));

                                var userIds = profileResult.Select(o => o.UserId).ToList();

                                query = query.Where(u => userIds.Contains(u.UserId));
                            }
                        }
                        break;
                    case PeopleSearchType.Season:
                        {
                            if (year != 0)
                            {
                                var season = seasonName + year;
                                season = season.Replace(" ", string.Empty);

                                query = query.Where(item => item.Club.Seasons.Any(s => s.Title.ToLower().Replace(" ", string.Empty).Contains(season.ToLower())));

                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            if (query.Any())
            {
                if (club.IsPartner)
                {
                    var total = query.Count();

                    query = query.Skip((page - 1) * pageSize).Take(pageSize > 0 ? pageSize : 10);

                    var model = new List<UserViewModel>();

                    foreach (var item in query.ToList())
                    {
                        var userPlayers = playerProfileBusiness.GetList(item.UserId).Where(p => p.Relationship != RelationshipType.Parent);
                        var clubUserPlayers = userPlayers.Where(p => p.OrderItems.Any(i => (i.ItemStatus == OrderItemStatusCategories.completed || i.ItemStatusReason == OrderItemStatusReasons.canceled || i.ItemStatusReason == OrderItemStatusReasons.transferOut) && i.Order.ClubId == club.Id && i.Order.IsLive));

                        var credit = item.Club.Users.FirstOrDefault(c => item.UserId == c.UserId).Credit;

                        model.Add(new UserViewModel()
                        {
                            Id = item.UserId,
                            Players = clubUserPlayers,
                            UserOrdersItems = orderItemBusiness.GetClubUserOrderItems(item.Club.Id, item.UserId, false).Where(c => c.Order.IsLive && c.ItemStatus == OrderItemStatusCategories.completed || (c.ItemStatus == OrderItemStatusCategories.changed && c.ItemStatusReason == OrderItemStatusReasons.canceled)).ToList(),
                            UserName = item.User.UserName,
                            Credit = credit,
                            ClubName = item.Club.Name,
                            ClubId = item.Club.Id
                        });

                    }
                    return JsonNet(new { Data = model, Total = total });
                }
                else
                {
                    var groupbyQuery = query.OrderBy(q => q.UserId).GroupBy(u => u.UserId);
                    var total = groupbyQuery.Count();

                    groupbyQuery = groupbyQuery.Skip((page - 1) * pageSize).Take(pageSize > 0 ? pageSize : 10);

                    var model = new List<UserViewModel>();

                    foreach (var item in groupbyQuery.ToList())
                    {
                        var credit = club.Users.SingleOrDefault(c => c.UserId == item.ToList().FirstOrDefault().UserId).Credit;

                        var userPlayers = playerProfileBusiness.GetList(item.Key).Where(p => p.Relationship != RelationshipType.Parent);
                        var clubUserPlayers = userPlayers.Where(p => p.OrderItems.Any(i => (i.ItemStatus == OrderItemStatusCategories.completed || i.ItemStatusReason == OrderItemStatusReasons.canceled || i.ItemStatusReason == OrderItemStatusReasons.transferOut) && i.Order.ClubId == club.Id && i.Order.IsLive));

                        model.Add(new UserViewModel()
                        {
                            Id = item.Key,
                            Players = clubUserPlayers,
                            UserOrdersItems = orderItemBusiness.GetClubUserOrderItems(ActiveClub.Id, item.Key, false).Where(c => c.Order.IsLive && c.ItemStatus == OrderItemStatusCategories.completed || (c.ItemStatus == OrderItemStatusCategories.changed && c.ItemStatusReason == OrderItemStatusReasons.canceled)).ToList(),
                            UserName = item.ToList().FirstOrDefault().User.UserName,
                            Credit = credit,
                        });
                    }

                    return JsonNet(new { Data = model, Total = total });
                }
            }


            return JsonNet(new { Data = new List<UserViewModel>(), Total = 0 });
        }

        public virtual JsonNetResult GetFamiliesForDropDown(string term)
        {

            var club = ActiveClub;
            this.EnsureUserIsAdminForClub(club.Domain);

            if (!string.IsNullOrEmpty(term))
            {
                term = term.ToLower();
            }



            IQueryable<ClubUser> query = Ioc.ClubBusiness.Get(ActiveClub.Id).Users.AsQueryable();

            if (!string.IsNullOrEmpty(term))
            {
                term = term.ToLower();
                if (new Regex(Constants.EmailRegex).Match(term).Success)
                {
                    query = query.Where(item => item.User.UserName.ToLower() == term);
                }


            }
            if (query.Any())
            {
                var groupbyQuery = query.GroupBy(u => u.UserId);
                var total = groupbyQuery.Count();
                var model = new List<UserViewModel>();
                foreach (var item in groupbyQuery.ToList())
                {
                    var userOrderItems = Ioc.OrderItemBusiness.GetClubUserOrderItems(ActiveClub.Id, item.Key, null).Where(c => c.ItemStatus == OrderItemStatusCategories.completed || (c.ItemStatus == OrderItemStatusCategories.changed && c.ItemStatusReason == OrderItemStatusReasons.canceled)).ToList();
                    var allPlayers = Ioc.PlayerProfileBusiness.GetList(item.Key);
                    var orderItemIds = userOrderItems.Select(o => o.PlayerId).ToList();

                    model.Add(new UserViewModel()
                    {
                        Id = item.Key,
                        Players = allPlayers.Where(p => orderItemIds.Contains(p.Id)).ToList(),
                        UserOrdersItems = userOrderItems,
                        UserName = item.ToList().FirstOrDefault().User.UserName
                    });
                }

                return JsonNet(new { Data = model, Total = total });
            }

            return JsonNet(new { Data = new List<UserViewModel>(), Total = 0 });
        }

        public virtual ActionResult SubmitManageFamily(FamilyViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.UserName))
                {
                    ModelState.AddModelError("model.UserName", "The email address is required");
                }

                if (ModelState.IsValid)
                {
                    var email = model.UserName.ToLower();
                    var userprofile = Ioc.UserProfileBusiness.Get(email);
                    var clubDomain = ActiveClub.Domain;
                    if (userprofile == null)
                    {
                        userprofile = Ioc.UserProfileBusiness.Get(model.UserId);
                        userprofile.UserName = email;
                        userprofile.Email = email;
                        userprofile.TagName = string.IsNullOrEmpty(model.TagName) ? "" : model.TagName;
                        var res = Ioc.UserProfileBusiness.Update(userprofile);
                        return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });

                    }
                    else if (userprofile.Id == model.UserId)
                    {
                        userprofile.TagName = string.IsNullOrEmpty(model.TagName) ? "" : model.TagName;
                        var res = Ioc.UserProfileBusiness.Update(userprofile);
                        return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
                    }
                    else
                    {
                        ModelState.AddModelError("model.UserName", "The email, you trying to add, already exists in the system");
                    }
                }
                return JsonFormResponse();
            }
            catch (Exception ex)
            {
                return Json(new JResult { Status = false, Message = ex.Message, RecordsAffected = 0 });
            }
        }

        public virtual async Task<ActionResult> CreateUser(UserViewModel model)
        {
            try
            {
                if (model == null)
                {
                    ModelState.AddModelError("model.UserName", "The email address is required");
                }
                if (ModelState.IsValid)
                {
                    var email = model.UserName.ToLower();
                    var userprofile = Ioc.UserProfileBusiness.Get(email);
                    var clubDomain = ActiveClub.Domain;
                    if (userprofile == null)
                    {
                        var pass = Jumbula.Common.Utilities.StringCipher.Encrypt(clubDomain + email, DateTime.UtcNow.ToShortDateString().Replace("/", ""));

                        var user = new JbUser { UserName = model.UserName, Email = model.UserName };

                        var result = _applicationUserManager.Create(user);

                        if (result.Succeeded)
                        {
                            result = await _applicationUserManager.AddToRoleAsync(user.Id, RoleCategory.Parent.ToString());

                            _applicationUserManager.UserTokenProvider = new DataProtectorTokenProvider<JbUser, int>(new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Sample").Create("EmailConfirmation"));
                            string token = _applicationUserManager.GenerateEmailConfirmationTokenAsync(user.Id).Result;

                            if (result.Succeeded)
                            {
                                Ioc.ClubBusiness.AddUserInClubUsers(user.Id, ActiveClub.Id);
                            }
                        }

                        return Json(new JResult { Status = true, Message = "", RecordsAffected = 1 });
                    }
                    else if (!Ioc.ClubBusiness.Get(ActiveClub.Id).Users.Any(c => c.UserId == userprofile.Id))
                    {


                        Ioc.ClubBusiness.AddUserInClubUsers(userprofile.Id, ActiveClub.Id);


                        return Json(new JResult { Status = true, Message = "", RecordsAffected = 1 });
                    }

                    //if a user already exists in the club we don't show validation to admin and we allow they go to families
                    return Json(new JResult { Status = true, Message = "", RecordsAffected = 1 });
                }
                return JsonFormResponse();
            }
            catch (Exception ex)
            {
                return Json(new JResult { Status = false, Message = ex.Message, RecordsAffected = 0 });
            }
        }

        [HttpGet]
        public virtual JsonNetResult GetFamilyOrders(int userId, string confirmationId, string startDate, string endDate, int clubId = 0)
        {
            var model = new List<FamilyOrdersViewModel>();

            var clubBusiness = Ioc.ClubBusiness;
            var club = clubBusiness.Get(ActiveClub.Id).IsPartner ? clubBusiness.Get(clubId) : clubBusiness.Get(ActiveClub.Id);

            var query = Ioc.OrderBusiness.GetClubUserAllOrders(userId, club.Id).Where(o => o.IsLive);

            if (!string.IsNullOrEmpty(confirmationId))
            {
                query = query.Where(c => c.ConfirmationId.ToLower() == confirmationId.ToLower().Trim());
            }

            decimal balance = 0;
            var AccountService = Ioc.AccountingBusiness;

            foreach (var order in query)
            {
                var orderId = order.Id;

                var orderitems = Ioc.OrderItemBusiness.GetCompletetCancelOrderItems(orderId);
                decimal paidAmounts = 0;
                decimal totalAmounts = 0;

                foreach (var item in orderitems)
                {
                    if (item.ProgramTypeCategory == ProgramTypeCategory.Subscription || item.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                    {
                        paidAmounts += item.PaidAmount;
                        totalAmounts += item.TotalAmount;
                    }
                    else if (item.ItemStatusReason != OrderItemStatusReasons.canceled)
                    {
                        paidAmounts += item.PaidAmount;
                        totalAmounts += item.TotalAmount;
                    }
                }
                //Add orderItems that are cancel
                var cancelOrderItems = orderitems.Where(o => o.ItemStatusReason == OrderItemStatusReasons.canceled && o.ProgramTypeCategory != ProgramTypeCategory.Subscription && o.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare);
                decimal totalAmount = 0;
                decimal paidAmount = 0;

                if (cancelOrderItems != null && cancelOrderItems.Any())
                {
                    totalAmount = 0;
                    paidAmount = cancelOrderItems.Where(o => o.ItemStatusReason == OrderItemStatusReasons.canceled).Sum(p => (decimal?)p.PaidAmount) ?? 0;

                    foreach (var cancelItem in cancelOrderItems)
                    {
                        if (cancelItem.ItemStatus == OrderItemStatusCategories.changed && cancelItem.ItemStatusReason == OrderItemStatusReasons.canceled)
                        {
                            decimal amount = 0;
                            var orderChargeDiscount = cancelItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.CancellationFee);
                            if (orderChargeDiscount.Any())
                            {
                                amount = orderChargeDiscount.FirstOrDefault().Amount;
                            }

                            totalAmount = totalAmount + amount;
                        }

                    }
                }

                var cancelItemBalance = totalAmount - paidAmount;
                balance = (totalAmounts - paidAmounts) + cancelItemBalance;

                if (balance > 0)
                {
                    model.Add(new FamilyOrdersViewModel()
                    {
                        OrderDate = Ioc.ClubBusiness.GetClubDateTime(this.GetCurrentClubId(), order.CompleteDate),
                        ConfirmationId = order.ConfirmationId,
                        OrderAmount = totalAmounts,
                        Balance = balance > 0 ? balance : balance <= 0 ? balance : 0,
                        OrderId = order.Id,
                        PaidAmount = paidAmounts,
                        Id = order.Id,
                        InvoiceAmount = 0,
                        UserName = order.User.UserName

                    });
                }
            }
            return JsonNet(model);
        }

        [HttpGet]
        public virtual JsonNetResult GetFamilyOrderItems(int orderid)
        {

            var orderitems = Ioc.OrderItemBusiness.GetCompletetCancelOrderItems(orderid);
            var orderitemModel = new List<FamilyOrderItemsViewModel>();
            var userId = Ioc.OrderBusiness.Get(orderid).UserId;
            var participantName = Ioc.PlayerProfileBusiness.GetList().First(p => p.UserId == userId).Contact.FullName;
            var userName = orderitems.First().Order.User.UserName;

            foreach (var orderItem in orderitems)
            {

                if (orderItem.ProgramScheduleId.HasValue)
                {
                    if ((orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription || orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare) && orderItem.ItemStatus == OrderItemStatusCategories.changed && orderItem.ItemStatusReason == OrderItemStatusReasons.canceled)
                    {
                        var subItem = getCancelSubscriptionItem(orderItem);
                        orderitemModel.Add(subItem);
                    }
                    else
                    {
                        var instalments = orderItem.Installments.Where(i => !i.IsDeleted).ToList();

                        if (instalments.Any())
                        {
                            var paidAmounts = instalments.Sum(p => (decimal?)p.PaidAmount) ?? 0;
                            var totalAmounts = instalments.Sum(a => (decimal?)a.Amount) ?? 0;
                            var sumBalance = totalAmounts - paidAmounts;
                            //orderItem is installment
                            orderitemModel.Add(new FamilyOrderItemsViewModel()
                            {
                                Attendee = orderItem.FirstName + " " + orderItem.LastName,
                                EntryFee = orderItem.TotalAmount,
                                PaidAmount = orderItem.PaidAmount,
                                Balance = sumBalance,
                                Id = orderItem.Id,
                                EntryFeeName = orderItem.EntryFeeName,
                                PaymentplanType = orderItem.PaymentPlanType,
                                InvoiceAmount = sumBalance,
                                ProgramName = orderItem.ProgramTypeCategory != ProgramTypeCategory.Camp ? _programBusiness.GetProgramNameOnlyTuition(orderItem.ProgramSchedule, orderItem.EntryFeeName) : _programBusiness.GetProgramName(orderItem.ProgramSchedule, orderItem.EntryFeeName),
                                UserName = userName,
                                OrderId = orderItem.Order_Id,
                                ProgramScheduleId = orderItem.ProgramScheduleId.Value,
                                ConfirmationId = orderItem.Order.ConfirmationId,
                                UserId = orderItem.Order.UserId,
                                Installments = orderItem.Installments.Where(i => !i.IsDeleted).Select(ins =>
                                   new FamilyOrderItemInstalmentsViewModel()
                                   {
                                       DueDate = ins.InstallmentDate,
                                       Amount = ins.Amount,
                                       PaidAmount = ins.PaidAmount ?? 0,
                                       id = ins.Id,
                                       LastDueDate = ins.PaidDate,
                                       IsInvoiced = ins.TransactionActivities.Any(t => t.PaymentDetail.Status != PaymentDetailStatus.CANCELED && t.TransactionCategory == TransactionCategory.Invoice) ? true : false,
                                   }).ToList(),

                            });
                        }
                        else //orderItem
                        {
                            orderitemModel.Add(new FamilyOrderItemsViewModel()
                            {
                                Attendee = orderItem.FirstName + " " + orderItem.LastName,
                                EntryFee = orderItem.TotalAmount,
                                PaidAmount = orderItem.PaidAmount,
                                Balance = orderItem.TotalAmount - orderItem.PaidAmount,
                                Id = orderItem.Id,
                                EntryFeeName = orderItem.EntryFeeName,
                                UserName = userName,
                                PaymentplanType = orderItem.PaymentPlanType,
                                InvoiceAmount = orderItem.TotalAmount - orderItem.PaidAmount,
                                ProgramName = orderItem.ProgramTypeCategory != ProgramTypeCategory.Camp ? _programBusiness.GetProgramNameOnlyTuition(orderItem.ProgramSchedule, orderItem.EntryFeeName) : _programBusiness.GetProgramName(orderItem.ProgramSchedule, orderItem.EntryFeeName),
                                ProgramScheduleId = orderItem.ProgramScheduleId.Value,
                                OrderId = orderItem.Order_Id,
                                ConfirmationId = orderItem.Order.ConfirmationId,
                                UserId = orderItem.Order.UserId,
                                IsInvoiced = orderItem.TransactionActivities.Any(t => t.PaymentDetail.Status != PaymentDetailStatus.CANCELED && t.TransactionCategory == TransactionCategory.Invoice) ? true : false,
                            });
                        }
                    }

                }
                else //donation
                {
                    orderitemModel.Add(new FamilyOrderItemsViewModel()
                    {
                        Attendee = participantName,
                        EntryFee = orderItem.TotalAmount,
                        PaidAmount = orderItem.PaidAmount,
                        Balance = orderItem.TotalAmount - orderItem.PaidAmount,
                        Id = orderItem.Id,
                        EntryFeeName = orderItem.EntryFeeName,
                        PaymentplanType = orderItem.PaymentPlanType,
                        InvoiceAmount = orderItem.TotalAmount - orderItem.PaidAmount,
                        ProgramScheduleId = 0,
                        UserName = userName,
                        ProgramName = orderItem.Name,
                        OrderId = orderItem.Order_Id,
                        ConfirmationId = orderItem.Order.ConfirmationId,
                        UserId = orderItem.Order.UserId,
                        IsInvoiced = orderItem.TransactionActivities.Any(t => t.PaymentDetail.Status != PaymentDetailStatus.CANCELED && t.TransactionCategory == TransactionCategory.Invoice) ? true : false,
                    });

                }

            }

            return JsonNet(orderitemModel);
        }
        [HttpGet]
        public virtual ActionResult GetInfoForInvoice(int userId, long? orderItemId)
        {
            Club club;

            if (orderItemId.HasValue)
            {
                var orderItem = Ioc.OrderItemBusiness.GetItem(orderItemId.Value);
                club = orderItem.Order.Club;
            }
            else
            {
                club = Ioc.ClubBusiness.Get(ActiveClub.Id);
            }

            var userName = Ioc.UserProfileBusiness.Get(userId).UserName;
            var clubId = club.Id;
            var invoices = Ioc.InvoiceBusiness.GetInvoiceByClubId(clubId).ToList();

            var clubLogo = "";
            var partner = club.PartnerClub;
            var hasPartner = partner != null ? true : false;
            var clubInfo = Ioc.ClubBusiness.GetInvoiceSetting(club);

            clubLogo = clubInfo.logo
                ? UrlHelpers.GetClubLogoUrl(partner.Domain, partner.Logo)
                : UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);

            long invoiceNumber = 0;
            long newInvoiceNumber = 0;

            if (invoices.Any())
            {
                var afterInvoiceNumber = invoices.Last().InvoiceNumber + 1;
                newInvoiceNumber = afterInvoiceNumber;

                while (invoices.Any(c => c.InvoiceNumber == newInvoiceNumber))
                {
                    newInvoiceNumber++;
                }

                invoiceNumber = newInvoiceNumber > 0 ? newInvoiceNumber : afterInvoiceNumber;
            }
            else
            {
                invoiceNumber = 1;
            }
            var infoModel = new InformationFamilyInvoiceViewModel()
            {
                UserName = userName,
                ClubSite = clubInfo.Site,
                InvoiceNumber = invoiceNumber,
                UserId = userId,
                ContactClubEmail = clubInfo.Email,
                ClubAddress = clubInfo.Address,
                ClubUrlLogo = clubLogo,
                ClubName = clubInfo.Name,
                MemberName = club.Name,
                HasPartner = hasPartner
            };

            return JsonNet(infoModel);
        }

        [HttpPost]
        public virtual ActionResult ConfirmInvoice(InformationFamilyInvoiceViewModel infoModel)
        {
            var invoiceNumber = infoModel.InvoiceNumber;
            var invoices = Ioc.InvoiceBusiness.GetInvoiceByClubId(ActiveClub.Id);

            foreach (var item in invoices)
            {
                if (item.InvoiceNumber == invoiceNumber)
                {
                    {
                        ModelState.AddModelError("infoModel.InvoiceNumber", "You've already used this invoice number");

                    }
                    break;
                }
            }
            if (ModelState.IsValid)
            {
                return Json(new JResult { Status = true, Message = string.Empty, RecordsAffected = 0 });
            }
            else
            {
                return base.JsonFormResponse();
            }
        }


        public virtual ActionResult ValidateTakePayment(TakePaymentInvoiceViewModel model)
        {

            if (model.SelectedChargeMethod == ((int)ChargeMethodForInvoice.ReceivedPayment).ToString())
            {
                if (model.SelectedPaymentMethod == "0")
                {
                    ModelState.AddModelError("model.SelectedPaymentMethod", "Please select the payment method");

                }
                if (!model.PaymentDate.HasValue)
                {
                    ModelState.AddModelError("model.PaymentDate", "Payment date is required.");
                }
            }
            if (!ModelState.IsValid)
            {
                return JsonFormResponse();
            }

            return Json(new JResult { Status = true, Message = string.Empty, RecordsAffected = 0 });

        }

        [HttpGet]
        public virtual JsonNetResult GetTakePaymentModel(List<long> orderItemModel, int userId)
        {
            //ActiveClub
            var clubBusiness = Ioc.ClubBusiness;
            var user = Ioc.UserProfileBusiness.Get(userId);

            var listOrderItems = Ioc.OrderItemBusiness.GetList().Where(i => orderItemModel.ToList().Contains(i.Id));
            var fistOrderItem = listOrderItems.FirstOrDefault();
            var club = fistOrderItem != null ? fistOrderItem.Order.Club : clubBusiness.Get(ActiveClub.Domain);
            var paymentMethods = Ioc.ClientPaymentMethodBusiness.Get(club.ClientId.Value);



            var model = new TakePaymentViewModel()
            {
                AmountDue = listOrderItems.Sum(o => o.TotalAmount) - listOrderItems.Sum(o => o.PaidAmount),
                UserName = user.UserName,
                AvailableCredit = clubBusiness.GetUserCredit(club, user.Id, false),
                SendAdminEmail = true,
                IsStripeEnabled = (paymentMethods.EnableStripePayment || paymentMethods.EnableAuthorizePayment) ? true : false,
                IsPaypalEnabled = paymentMethods.EnablePaypalPayment,
                PaymentDate = Ioc.ClubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow),
                InvoiceId = 55,
            };

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult SubmitTakePayment(TakePaymentViewModel model, List<FamilyOrderItemsViewModel> orderitems)
        {
            OperationStatus result = new OperationStatus() { Status = false };
            try
            {
                var orderItemIds = orderitems.Select(o => o.Id).ToList();
                var userId = orderitems.First().UserId;
                var selectedOrderItems = Ioc.OrderItemBusiness.GetList().Where(o => orderItemIds.Contains(o.Id)).ToList();

                var club = selectedOrderItems.FirstOrDefault() != null ? selectedOrderItems.FirstOrDefault()?.Order.Club : Ioc.ClubBusiness.Get(ActiveClub.Domain);
                var clubId = club.Id;
                var token = Guid.NewGuid().ToString("N");
                var currency = club.Currency;

                if (club.PartnerId.HasValue && club.IsSchool)
                {
                    currency = Ioc.ClubBusiness.Get(club.PartnerId.Value).Currency;
                }
                var amountPayable = selectedOrderItems.Sum(o => o.TotalAmount) - selectedOrderItems.Sum(o => o.PaidAmount);

                if (amountPayable <= 0)
                {
                    return Json(new { Status = result.Status, result = "Refresh", Message = "The Balance is zero and you can not make any other payment.", RecordsAffected = 0 });
                }

                var timezone = club.TimeZone > 0 ? club.TimeZone : Jumbula.Common.Enums.TimeZone.UTC;

                if (model.SelectedChargeMethod == ((int)ChargeMethodForInvoice.ReceivedPayment).ToString())
                {
                    decimal totalPaidAmount = 0;
                    var listOderItems = new List<OrderItem>();
                    List<OrderInstallment> selectedInstallment = null;
                    selectedInstallment = new List<OrderInstallment>();
                    foreach (var orderitem in orderitems)
                    {
                        if (orderitem.Installments != null && orderitem.Installments.Count > 0)
                        {

                            foreach (var inst in orderitem.Installments.Where(c => c.Checked))
                            {
                                selectedInstallment.Add(Ioc.InstallmentBusiness.Get(inst.id));
                            }
                        }
                    }
                    if (selectedInstallment.Any())
                    {
                        totalPaidAmount = selectedInstallment.Sum(i => i.Amount);
                    }
                    else
                    {
                        totalPaidAmount = orderitems.Sum(o => o.InvoiceAmount);
                    }

                    if (!model.PaymentDate.HasValue)
                    {
                        model.PaymentDate = DateTime.UtcNow;
                    }
                    var paymentDetail = new PaymentDetail(model.UserName, PaymentDetailStatus.COMPLETED, "", (PaymentMethod)(Convert.ToInt32(model.SelectedPaymentMethod)), currency, string.Empty, string.Empty, string.Empty, JbUserService.GetCurrentUserRoleType());
                    List<TransactionActivity> transactions = null;

                    result = OrderServices.Get().ReceivedPaymentFromMultiOrder(selectedOrderItems, orderitems, totalPaidAmount, paymentDetail, model.Note,
                        DateTimeHelper.ConvertLocalDateTimeToUtc(model.PaymentDate.Value, timezone), model.CheckId, selectedInstallment, transactions, HandleMode.Offline);

                    if (result.Status && (model.SendAdminEmail || model.SendParentEmail))
                    {
                        EmailService.Get(this.ControllerContext).SendReceivedFamilyTakePaymentEmail(selectedOrderItems, orderitems, model.PaymentDate.Value, paymentDetail, userId, clubId, model.SendAdminEmail, model.SendParentEmail);
                    }
                }
                else if (model.SelectedChargeMethod == ((int)ChargeMethodForInvoice.PayNowWithCreditCard).ToString())
                {
                    JbPaymentService paymentService = new JbPaymentService();
                    result = paymentService.TakeAPaymentNowWithStripe(model, currency, orderitems);

                    return Json(new { Status = result.Status, result = "Redirect", url = result.Message, RecordsAffected = 0 });
                }
                else if (model.SelectedChargeMethod == ((int)ChargeMethodForInvoice.PayWithCredit).ToString())
                {
                    decimal totalPaidAmount = 0;
                    TimeZoneInfo localTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timezone.ToDescription());

                    if (!selectedOrderItems.FirstOrDefault().Order.IsLive)
                    {
                        selectedOrderItems.FirstOrDefault().Order.Club.Users.SingleOrDefault(c => c.UserId == userId).TestCredit -= model.AmountDue;
                    }
                    else
                    {
                        selectedOrderItems.FirstOrDefault().Order.Club.Users.SingleOrDefault(c => c.UserId == userId).Credit -= model.AmountDue;
                    }

                    List<OrderInstallment> selectedInstallment = null;
                    selectedInstallment = new List<OrderInstallment>();
                    foreach (var orderitem in orderitems)
                    {
                        if (orderitem.Installments != null && orderitem.Installments.Count > 0)
                        {
                            foreach (var inst in orderitem.Installments.Where(c => c.Checked))
                            {
                                selectedInstallment.Add(Ioc.InstallmentBusiness.Get(inst.id));
                            }
                        }
                    }
                    PaymentDetail paymentDetail = new PaymentDetail(model.UserName, PaymentDetailStatus.COMPLETED, "", PaymentMethod.Credit, currency, string.Empty, string.Empty, string.Empty, JbUserService.GetCurrentUserRoleType());
                    List<TransactionActivity> transactions = null;

                    if (selectedInstallment.Any())
                    {
                        totalPaidAmount = selectedInstallment.Sum(i => i.Amount);
                    }
                    else
                    {
                        totalPaidAmount = orderitems.Sum(o => o.InvoiceAmount);
                    }
                    result = OrderServices.Get().ReceivedPaymentFromMultiOrder(selectedOrderItems, orderitems, totalPaidAmount, paymentDetail, model.Note,
                              DateTimeHelper.ConvertLocalDateTimeToUtc(model.PaymentDate.Value, timezone), model.CheckId, selectedInstallment, transactions, HandleMode.Offline);

                    if (result.Status && (model.SendAdminEmail || model.SendParentEmail))
                    {
                        EmailService.Get(this.ControllerContext).SendReceivedFamilyTakePaymentEmail(selectedOrderItems, orderitems, model.PaymentDate.Value, paymentDetail, userId, clubId, model.SendAdminEmail, model.SendParentEmail);
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(new { Status = result.Status, result = "Refresh", Message = result.ExceptionMessage, RecordsAffected = 0 });

        }

        public virtual JsonNetResult GetTakePaymentMessageModel(bool status)
        {
            var model = new PaymentMessageViewModel() { IsSuceed = status };

            return JsonNet(model);
        }

        [HttpGet]
        public virtual JsonNetResult GetInvoiceMessage(int selectDuedate, long? orderItemId)
        {

            var model = new InvoiceMessageViewModel();
            var today = DateTime.UtcNow;

            var club = new Club();

            if (orderItemId.HasValue)
            {
                var orderItem = Ioc.OrderItemBusiness.GetItem(orderItemId.Value);
                club = orderItem.Order.Club;
            }
            else
            {
                club = Ioc.ClubBusiness.Get(ActiveClub.Id);
            }

            var dateTime = Ioc.ClubBusiness.GetClubDateTime(club.Id, today);
            model.SelectDueDate = selectDuedate;
            if (selectDuedate > 0)
            {

                DateTime duedate = dateTime.AddDays(selectDuedate);
                model.DueDate = duedate;
            }
            if (selectDuedate == 0)
            {

                DateTime duedate = dateTime;
                model.DueDate = duedate;
            }
            return JsonNet(model);
        }
        [HttpPost]
        public virtual ActionResult SubmitSendInvoice(List<FamilyOrderItemsViewModel> model, InformationFamilyInvoiceViewModel infomodel, long? orderItemId)
        {
            OperationStatus result = new OperationStatus() { Status = false };
            try
            {
                decimal amount = 0;
                decimal orderItembalance;
                foreach (var item in model)
                {
                    orderItembalance = item.InvoiceAmount;
                    amount += orderItembalance;
                }

                Club club;

                if (orderItemId.HasValue)
                {
                    var orderItem = Ioc.OrderItemBusiness.GetItem(orderItemId.Value);
                    club = orderItem.Order.Club;
                }
                else
                {
                    club = Ioc.ClubBusiness.Get(ActiveClub.Id);
                }

                PaymentDetail paymentDetail;
                var userId = model.First().UserId;

                var invoices = Ioc.InvoiceBusiness.GetList().Where(c => c.ClubId == club.Id).ToList();

                var invoiceNumber = infomodel.InvoiceNumber;
                var reference = infomodel.Refrense;
                var memo = infomodel.Memo;
                var noteToRecipient = infomodel.NoteToRecipient;
                var emailToCc = infomodel.EmailCc;
                var todayDate = DateTime.UtcNow;
                var clubTime = Ioc.ClubBusiness.GetClubDateTime(club.Id, todayDate);
                var duedate = infomodel.SelectDueDate;
                var user = Ioc.UserProfileBusiness.Get(userId);


                var invoice = new Invoice() { ClubId = club.Id, Amount = amount, UserId = userId, DueDate = duedate, InvoiceNumber = invoiceNumber, Reference = reference, Memo = memo, EmailToCc = emailToCc, NotToRecipient = noteToRecipient, InvoicingDate = clubTime, Status = InvoiceStatus.Unpaid };
                Ioc.InvoiceBusiness.Create(invoice);

                var invoiceHistory = new InvoiceHistory() { InvoiceId = invoice.Id, ActionDate = todayDate, Action = Invoicestatus.Initiated, Description = string.Format(Constants.Invoice_Created, CurrencyHelper.FormatCurrencyWithPenny(amount, club.Currency)) };
                Ioc.InvoiceHistoryBusiness.Create(invoiceHistory);

                paymentDetail = new PaymentDetail() { PaymentMethod = PaymentMethod.Paypal, Status = PaymentDetailStatus.CREATED, InvoiceId = invoice.Id, PayerType = this.GetCurrentUserRoleType() };


                var token = Guid.NewGuid().ToString("N");
                var orderDb = Ioc.OrderBusiness;
                var currency = club.Currency;

                decimal instBalance;

                var clubId = club.Id;
                foreach (var oItem in model)
                {
                    var orderItem = Ioc.OrderItemBusiness.GetItem(oItem.Id);
                    orderItembalance = oItem.InvoiceAmount;
                    if (oItem.PaymentplanType == PaymentPlanType.FullPaid)
                    {


                        orderDb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Invoice, null, DateTime.UtcNow, orderItembalance, orderItem, null, null, token, oItem.CheckId, TransactionCategory.Invoice);
                    }
                    else
                    {
                        var instalments = oItem.Installments;
                        foreach (var inst in instalments)
                        {
                            if (inst.Checked)
                            {
                                instBalance = inst.Amount - (inst.PaidAmount.HasValue ? inst.PaidAmount.Value : 0);
                                orderDb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Invoice, null, DateTime.UtcNow, instBalance, orderItem, inst.id, null, token, oItem.CheckId, TransactionCategory.Invoice);
                            }

                        }

                    }

                    if (orderItem.ProgramScheduleId.HasValue && orderItem.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare && orderItem.ProgramTypeCategory != ProgramTypeCategory.Subscription)
                    {
                        oItem.ProgramName = _programBusiness.GetProgramName(orderItem.ProgramSchedule, orderItem.EntryFeeName);
                    }

                    Ioc.OrderItemBusiness.Update(orderItem);
                }
                EmailService.Get(this.ControllerContext).SendFamilyInvoice(model, token, amount, clubId, userId, invoice);

                result.Status = true;

            }
            catch (Exception e)
            {

                throw e;
            }
            return Json(new { Status = result.Status, result = "Refresh", Message = result.ExceptionMessage, RecordsAffected = 0, model });
        }


        [HttpPost]
        public virtual ActionResult GetDependentCareReceiptReport(int userId, string filterType, int? year, DateTime? startDate, DateTime? endDate, bool printMode = false, int clubId = 0)
        {
            SportsClub.Areas.Dashboard.Models.DependentCareReceiptReportFilterType Type = (SportsClub.Areas.Dashboard.Models.DependentCareReceiptReportFilterType)Enum.Parse(typeof(SportsClub.Areas.Dashboard.Models.DependentCareReceiptReportFilterType), filterType);

            var clubBusiness = Ioc.ClubBusiness;
            var club = clubBusiness.Get(ActiveClub.Id).IsPartner ? clubBusiness.Get(clubId) : clubBusiness.Get(ActiveClub.Id);

            var orderItems = Ioc.OrderItemBusiness.GetUserOrderItems(userId)
                   .Where(i => i.Order.ClubId == club.Id);

            if (Type == SportsClub.Areas.Dashboard.Models.DependentCareReceiptReportFilterType.Year)
            {
                startDate = new DateTime(year.Value, 1, 1);
                endDate = new DateTime(year.Value + 1, 1, 1).AddDays(-1);
            }
            else if (Type == SportsClub.Areas.Dashboard.Models.DependentCareReceiptReportFilterType.DateRange)
            {
                if (!startDate.HasValue && !endDate.HasValue)
                {
                    startDate = new DateTime(DateTime.Now.Year, 1, 1);
                    endDate = DateTime.Now;
                }
                else if (startDate.HasValue && !endDate.HasValue)
                {
                    startDate = startDate.Value;
                    endDate = DateTime.Now;
                }
                else if (!startDate.HasValue && endDate.HasValue)
                {
                    startDate = new DateTime(DateTime.Now.Year, 1, 1);
                    endDate = endDate.Value;
                }
                else
                {
                    startDate = startDate.Value;
                    endDate = endDate.Value;
                }

            }

            orderItems = orderItems.Where(i => i.TransactionActivities.Any(t => t.TransactionStatus == TransactionStatus.Success &&
                             t.TransactionDate >= startDate && t.TransactionDate <= endDate));

            var programBusiness = Ioc.ProgramBusiness;
            var transactionBusiness = Ioc.TransactionActivityBusiness;

            var result = new List<DependentCareReceiptReportItemViewModel>();


            foreach (var item in orderItems)
            {

                var transactions = Type == SportsClub.Areas.Dashboard.Models.DependentCareReceiptReportFilterType.Year ? transactionBusiness.GetAllsucceedTransactionByOrderItemId(item.Id).Where(t => (t.TransactionType == TransactionType.Payment || t.TransactionType == TransactionType.Refund) && t.TransactionDate.Year == year).Any() ?
                                                                      transactionBusiness.GetAllsucceedTransactionByOrderItemId(item.Id).Where(t => (t.TransactionType == TransactionType.Payment || t.TransactionType == TransactionType.Refund) && t.TransactionDate.Year == year) : null :
                                   Type == SportsClub.Areas.Dashboard.Models.DependentCareReceiptReportFilterType.DateRange ? transactionBusiness.GetAllsucceedTransactionByOrderItemId(item.Id).Where(t => (t.TransactionType == TransactionType.Payment || t.TransactionType == TransactionType.Refund) && t.TransactionDate >= startDate && t.TransactionDate <= endDate).Any() ?
                                                                           transactionBusiness.GetAllsucceedTransactionByOrderItemId(item.Id).Where(t => (t.TransactionType == TransactionType.Payment || t.TransactionType == TransactionType.Refund) && t.TransactionDate >= startDate && t.TransactionDate <= endDate) : null : null;

                if (transactions != null)
                {
                    var programScheduleMode = _programBusiness.GetClassScheduleTime(item.ProgramSchedule.Program);

                    foreach (var transaction in transactions)
                    {
                        result.Add(new DependentCareReceiptReportItemViewModel()
                        {
                            StudentName = string.Format("{0}, {1}", item.LastName, item.FirstName),
                            ClassName = item.ProgramTypeCategory != ProgramTypeCategory.Camp ?
                                transaction.TransactionType == TransactionType.Payment ? _programBusiness.GetProgramNameOnlyTuition(item.ProgramSchedule, item.EntryFeeName) : $"Refund - {_programBusiness.GetProgramNameOnlyTuition(item.ProgramSchedule, item.EntryFeeName)}" :
                                transaction.TransactionType == TransactionType.Payment ? _programBusiness.GetProgramName(item.ProgramSchedule, item.EntryFeeName) : $"Refund - {_programBusiness.GetProgramName(item.ProgramSchedule, item.EntryFeeName)}",
                            AmountPaid = transaction.TransactionType == TransactionType.Payment ? (decimal?)transaction.Amount ?? 0 : (decimal?)-transaction.Amount ?? 0,
                            ClassMeetingDates = programBusiness.GetClassDates(item.ProgramSchedule, programScheduleMode) != null ? DateTimeHelper.GetDaysSeperatedByComma(programBusiness.GetClassDates(item.ProgramSchedule, programScheduleMode).Select(c => c.SessionDate).ToList()) : "-",
                            MethodOfPayment = !string.IsNullOrEmpty(transaction.PaymentDetail.PaymentMethod.ToDescription()) ? transaction.PaymentDetail.PaymentMethod.ToDescription() : "-",
                            ProviderName = item.ProgramSchedule.Program.OutSourceSeasonId.HasValue ? item.ProgramSchedule.Program.OutSourceSeason.Club.Name : item.ProgramSchedule.Program.Club.Name,
                            ProviderTaxId = item.ProgramSchedule.Program.OutSourceSeasonId.HasValue ?
                                                                   (item.ProgramSchedule.Program.OutSourceSeason.Club.Setting.TaxIDFEIN) :
                                                                   (item.ProgramSchedule.Program.Club.Setting.TaxIDFEIN),
                            Date = transaction.TransactionDate.ToString(Constants.DefaultDateFormat),
                            ScheduleDate = transaction.InstallmentId.HasValue && transaction.Installment.ProgramSchedulePartId.HasValue ? $"{transaction.Installment.ProgramSchedulePart.StartDate.ToString(Constants.DateTime_Comma)} - {transaction.Installment.ProgramSchedulePart.EndDate.ToString(Constants.DateTime_Comma)}" : $"{item.ProgramSchedule.StartDate.ToString(Constants.DateTime_Comma)} - {item.ProgramSchedule.EndDate.ToString(Constants.DateTime_Comma)}"
                        });
                    }
                }

            }


            if (printMode)
            {
                dynamic model = new System.Dynamic.ExpandoObject();
                model.Date = Ioc.ClubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
                model.Data = result;
                var ClubName = ActiveClub.Name;

                var headerInfo = GetDependentCareReceiptReportDetail(filterType, year, startDate, endDate);
                model.HeaderInfo = headerInfo.Data;
                model.HeaderInfoPartnerName = model.HeaderInfo.Data.PartnerName;
                model.HeaderInfoPartnerAddress = model.HeaderInfo.Data.PartnerAddress;
                model.HeaderInfoPartnerPhone = model.HeaderInfo.Data.PartnerPhone;
                model.HeaderInfoPartnerSite = model.HeaderInfo.Data.PartnerSite;
                model.HeaderInfoPartnerEmail = model.HeaderInfo.Data.PartnerEmail;
                model.HeaderInfoDate = model.HeaderInfo.Data.Date;
                model.Description = model.HeaderInfo.Data.Description;

                model.TotalAmount = string.Format("{0:c2}", result.Sum(s => s.AmountPaid));
                return Pdf(ClubName + " (Dependent care receipt)", "_DependentCareReceiptReportPdfExportView", model);
            }

            return JsonNet(new { DataSource = result, TotalCount = result.Count() });
        }


        [HttpGet]
        public virtual JsonNetResult GetDependentCareReceiptReportDetail(string filterType, int? year, DateTime? startDate, DateTime? endDate)
        {
            var clubBusiness = Ioc.ClubBusiness;
            var club = clubBusiness.Get(ActiveClub.Id);

            var info = club.PartnerId.HasValue ? ((PartnerSetting)club.PartnerClub.Setting)?.PortalParameters != null && ((PartnerSetting)club.PartnerClub.Setting).PortalParameters.ReadDependentCareReportInfoFromMember ? club : Ioc.ClubBusiness.GetPartner(this.GetCurrentClubDomain()) : club;

            var date = "";

            if (filterType == "Year")
            {
                date = year.ToString();
            }
            else if (filterType == "DateRange")
            {
                if (!startDate.HasValue && !endDate.HasValue)
                {
                    date = string.Format("{0} - {1}", new DateTime(DateTime.Now.Year, 1, 1).Date.ToString("M/d/yyyy"), DateTime.Now.Date.ToString("M/d/yyyy"));
                }
                else if (startDate.HasValue && !endDate.HasValue)
                {
                    date = string.Format("{0:M/d/yyyy} - {1}", startDate, DateTime.Now.Date.ToString("M/d/yyyy"));
                }
                else if (!startDate.HasValue && endDate.HasValue)
                {
                    date = string.Format("{0} - {1:M/d/yyyy}", new DateTime(DateTime.Now.Year, 1, 1).Date.ToString("M/d/yyyy"), endDate);
                }
                else if (startDate.HasValue && endDate.HasValue)
                {
                    date = string.Format("{0:M/d/yyyy} - {1:M/d/yyyy}", startDate, endDate);
                }
            }


            var model = new DependentCareReceiptReportItemDetailViewModel()
            {
                PartnerName = !string.IsNullOrEmpty(info.Name) ? info.Name : "",
                PartnerAddress = info.Address != null && !string.IsNullOrEmpty(info.Address.Address) ? info.Address.Address : "",
                PartnerPhone = info.ContactPersons != null && !string.IsNullOrEmpty(info.ContactPersons.FirstOrDefault().Phone) ? PhoneNumberHelper.FormatPhoneNumber(info.ContactPersons.FirstOrDefault().Phone) : "",
                PartnerSite = !string.IsNullOrEmpty(info.Site) ? info.Site.EndsWith("/") ? info.Site.Remove(info.Site.Count() - 1, 1) : info.Site : "",
                PartnerEmail = info.ContactPersons != null && !string.IsNullOrEmpty(info.ContactPersons.FirstOrDefault().Email) ? info.ContactPersons.FirstOrDefault().Email : "",
                Description = info.Setting?.Notifications.Report.DependentCareDescription,
                Date = date
            };

            return JsonNet(new { Data = model });
        }

        [HttpPost]
        public virtual ActionResult SaveEducationalStatus(int playerId)
        {
            var res = Ioc.PlayerProfileBusiness.SetEducationalStatus(playerId);

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }
        [HttpPost]
        public virtual ActionResult CancelGraduate(int playerId)
        {
            var player = Ioc.PlayerProfileBusiness.Get(playerId);
            player.EducationalStatus = null;
            var res = Ioc.PlayerProfileBusiness.Update(player);

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }
        private FamilyOrderItemsViewModel getCancelSubscriptionItem(OrderItem orderItem)
        {
            var model = new FamilyOrderItemsViewModel();
            var instalments = orderItem.Installments.Where(i => !i.IsDeleted).ToList();

            if (orderItem.Mode != OrderItemMode.DropIn)
            {
                if (instalments.Any())
                {
                    var paidAmounts = instalments.Sum(p => (decimal?)p.PaidAmount) ?? 0;
                    var totalAmounts = instalments.Sum(a => (decimal?)a.Amount) ?? 0;
                    var sumBalance = totalAmounts - paidAmounts;
                    //orderItem is installment
                    model = new FamilyOrderItemsViewModel()
                    {
                        Attendee = orderItem.FirstName + " " + orderItem.LastName,
                        EntryFee = orderItem.TotalAmount,
                        PaidAmount = orderItem.PaidAmount,
                        Balance = sumBalance,
                        Id = orderItem.Id,
                        EntryFeeName = orderItem.EntryFeeName,
                        PaymentplanType = orderItem.PaymentPlanType,
                        InvoiceAmount = sumBalance,
                        ProgramName = orderItem.ProgramTypeCategory != ProgramTypeCategory.Camp ? _programBusiness.GetProgramNameOnlyTuition(orderItem.ProgramSchedule, orderItem.EntryFeeName) : _programBusiness.GetProgramName(orderItem.ProgramSchedule, orderItem.EntryFeeName),
                        UserName = orderItem.Order.User.UserName,
                        OrderId = orderItem.Order_Id,
                        ProgramScheduleId = orderItem.ProgramScheduleId.Value,
                        ConfirmationId = orderItem.Order.ConfirmationId,
                        UserId = orderItem.Order.UserId,
                        Installments = orderItem.Installments.Where(i => !i.IsDeleted).Select(ins =>
                           new FamilyOrderItemInstalmentsViewModel()
                           {
                               DueDate = ins.InstallmentDate,
                               Amount = ins.Amount,
                               PaidAmount = ins.PaidAmount != null ? ins.PaidAmount : 0,
                               id = ins.Id,
                               LastDueDate = ins.PaidDate,
                               IsInvoiced = ins.TransactionActivities.Any(t => t.PaymentDetail.Status != PaymentDetailStatus.CANCELED && t.TransactionCategory == TransactionCategory.Invoice) ? true : false,
                           }).ToList(),
                    };
                }
            }
            else
            {
                var balance = orderItem.TotalAmount - orderItem.PaidAmount;

                if (balance > 0)
                {
                    model = new FamilyOrderItemsViewModel()
                    {
                        Attendee = orderItem.FirstName + " " + orderItem.LastName,
                        EntryFee = orderItem.TotalAmount,
                        PaidAmount = orderItem.PaidAmount,
                        Balance = balance,
                        Id = orderItem.Id,
                        EntryFeeName = orderItem.EntryFeeName,
                        PaymentplanType = orderItem.PaymentPlanType,
                        InvoiceAmount = balance,
                        ProgramName = orderItem.ProgramTypeCategory != ProgramTypeCategory.Camp ? _programBusiness.GetProgramNameOnlyTuition(orderItem.ProgramSchedule, orderItem.EntryFeeName) : _programBusiness.GetProgramName(orderItem.ProgramSchedule, orderItem.EntryFeeName),
                        UserName = orderItem.Order.User.UserName,
                        OrderId = orderItem.Order_Id,
                        ProgramScheduleId = orderItem.ProgramScheduleId.Value,
                        ConfirmationId = orderItem.Order.ConfirmationId,
                        UserId = orderItem.Order.UserId,
                    };

                }
            }

            return model;
        }

        [JbAuthorize(JbAction.People_Delinquents_View)]
        [HttpGet]
        public JsonNetResult GetDelinquents()
        {
            var model = new DelinquentListViewModel();

            return JsonNet(model);
        }

        [JbAuthorize(JbAction.People_Delinquents_View)]
        public virtual JsonNetResult GetDelinquentList(PaginationModel paginationModel, DelinquentFilterViewModel filters)
        {
            var result = _peopleBusiness.GetDelinquentList(ActiveClub.Id, filters, paginationModel.Sort, paginationModel.Page, paginationModel.PageSize);

            return JsonNet(result);
        }

        [JbAuthorize(JbAction.People_Delinquents_View)]
        public virtual JsonNetResult GetDelinquentsTotal(PaginationModel paginationModel, DelinquentFilterViewModel filters)
        {
            var result = _peopleBusiness.GetDelinquentsTotal(ActiveClub.Id);

            return JsonNet(result);
        }

        [HttpPost]
        [JbAuthorize(JbAction.People_Delinquents_SendMail)]
        public ActionResult DelinquentEmail(string entityName, long id)
        {
            var club = _clubBusiness.Get(ActiveClub.Domain);

            var result = _peopleBusiness.GetDelinquentEmail(entityName, id, club, this.GetCurrentUserId());

            return JsonNet(result);
        }

        [HttpPost]
        [JbAuthorize(JbAction.People_Delinquents_SendMail)]
        public ActionResult DelinquentEmails(List<DelinquentEmailParameterRelatedEntityItemModel> entities)
        {
            var club = _clubBusiness.Get(ActiveClub.Domain);

            var result = _peopleBusiness.GetDelinquentEmails(entities, club, this.GetCurrentUserId());

            return JsonNet(result);
        }

        [HttpGet]
        public JsonNetResult GetContactCategories()
        {
            var categories = new List<SelectListItem>
            {
                new SelectListItem{Value = ContactCategory.Phone.ToString() , Text = ContactCategory.Phone.ToDescription()},
                new SelectListItem{Value = ContactCategory.Note.ToString() , Text = ContactCategory.Note.ToDescription()},
                new SelectListItem{Value = ContactCategory.Meeting.ToString() , Text = ContactCategory.Meeting.ToDescription()}
            };

            return JsonNet(categories);
        }

        [HttpPost]
        [JbAuthorize(JbAction.People_Delinquents_ViewHistory)]
        public JsonNetResult GetDelinquentViewHistoryList(PaginationModel paginationModel, string relatedEntity, int relatedEntityId, CommunicationHistoryFilterViewModel filters)
        {
            filters.RelatedEntity = relatedEntity;
            filters.RelatedEntityId = relatedEntityId;

            var model = _communicationHistoryBusiness.GetViewHistoryList(paginationModel, filters, ActiveClub.Id);

            return JsonNet(model);
        }

        [JbAuthorize(JbAction.People_ViewHistory_AddHistory)]
        [HttpGet]
        public JsonNetResult GetContact(int? id)
        {
            var model = id != null ? _communicationHistoryBusiness.Get(id.Value, ActiveClub.Id) : new CommunicationHistoryViewModel();

            return JsonNet(model);
        }

        [JbAuthorize(JbAction.People_ViewHistory_AddHistory)]
        [HttpPost]
        public JsonNetResult SaveContact(CommunicationHistoryViewModel model, string relatedEntity, int relatedEntityId)
        {
            CheckEditContactValidation(model);

            if (ModelState.IsValid)
            {
                model.RelatedEntity = relatedEntity;
                model.RelatedEntityId = relatedEntityId;
                model.SenderId = this.GetCurrentUserId();

                return JsonNet(_communicationHistoryBusiness.Save(model));
            }

            return JsonNet(false);
        }

        [HttpPost]
        [JbAuthorize(JbAction.People_ViewHistory_DeleteHistory)]
        public JsonNetResult DeleteContact(int id)
        {
            var res = _communicationHistoryBusiness.Delete(id);

            return JsonNet(res);
        }

        [HttpPost]
        [JbAuthorize(JbAction.People_ViewHistory_ViewEmail)]
        public JsonNetResult EmailPreview(int id)
        {
            var communicationHistory = _communicationHistoryBusiness.Get(id, ActiveClub.Id);

            if (communicationHistory.Category == ContactCategory.Email && communicationHistory.EmailId.HasValue)
            {
                return JsonNet(_emailBusiness.GetPreview(communicationHistory.EmailId.Value));
            }

            return null;
        }

        [HttpPost]
        public virtual ActionResult GetParticipantClassSchedule(int playerId, string printMode)
        {
            var model = _peopleBusiness.GetParticipantClassSchedule(playerId, ActiveClub.Id);

            switch (printMode)
            {
                case "pdf":
                {
                    dynamic pdfModel = model;

                    var flyerGenerator = new FlyerGenerator();
                    byte[] file = flyerGenerator.GenerateFromView(this.ControllerContext, "_ParticipantClassScheduleReportPdf", pdfModel);
                    return File(file, "application/pdf", "ParticipantClassSchedule.Pdf");
                }
                case "excel":
                {
                    var file = _peopleBusiness.GetExcelParticipantClassSchedule(playerId, ActiveClub.Id);

                    //Return the result to the end user
                    return File(file,   //The binary data of the XLS file
                        "application/vnd.ms-excel",//MIME type of Excel files
                        "Sign-out sheet.xls");
                }
            }

            return JsonNet(new { DataSource = model});
        }

       
        #region Validations
        private void CheckEditContactValidation(CommunicationHistoryViewModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Note))
            {
                ModelState.AddModelError(nameof(model.Note), "Memo cannot be empty");
            }

            if (model.Category == 0)
            {
                ModelState.AddModelError(nameof(model.Category), "You should select a type");
            }
        }
        #endregion

    }
}