﻿using Autofac;
using Autofac.Integration.Mvc;
using Jumbula.Bootstrapper;
using Jumbula.Bootstrapper.Modules;
using Jumbula.Web;
using System.Web.Mvc;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(IocConfig), "RegisterDependencies")]

namespace Jumbula.Bootstrapper
{
    public class IocConfig
    {
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterModule<AutofacWebTypesModule>();

            builder.RegisterModule(new IdentityModule());
            builder.RegisterModule(new DataModule());
            builder.RegisterModule(new EmailModule());
            builder.RegisterModule(new ReportBusinessModule());
            builder.RegisterModule(new BusinessModule());
            builder.RegisterModule(new InfrastructureModule());

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
