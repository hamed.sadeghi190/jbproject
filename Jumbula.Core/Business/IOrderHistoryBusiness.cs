﻿using Jb.Framework.Common;
using Jumbula.Common.Enums;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IOrderHistoryBusiness
    {
        OperationStatus Create(OrderHistory orderHistory);
        IQueryable<OrderHistory> GetAllHistoryByOrderId(long orderId);
        IQueryable<OrderHistory> GetAllHistoryByOrderItemId(long orderId, long orderItemId);
        OperationStatus AddOrderHistory(OrderAction action, string description, long orderId, int userId, long? orderItemId=null);
        IQueryable<OrderHistory> GetCancelHistory(long orderId, long orderItemId);
    }
}