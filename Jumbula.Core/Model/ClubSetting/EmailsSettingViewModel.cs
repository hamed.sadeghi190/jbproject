﻿
using System.ComponentModel.DataAnnotations;


namespace Jumbula.Core.Model.ClubSetting
{
    public class EmailsSettingViewModel
    {     
        public string ContentCancellationEmail { get; set; }
        public string ContentAbsentee { get; set; }
        public string ContentConfirmationEmail { get; set; }
        public string DefaultContentConfirmationEmail { get; set; }
        public string DefaultContentCancellationEmail { get; set; }
        public bool IsPartner { get; set; }
    }
}