﻿; (function () {
    'use strict';

    var steps = {
        s1: {
            nextStep: "s2",
            backStep: "",
            stepNumber: 0,
            progress: 0
        },
        s2: {
            nextStep: "s3",
            backStep: "s1",
            stepNumber: 1,
            progress: 15
        },
        s3: {
            nextStep: "s4",
            backStep: "s2",
            stepNumber: 2,
            progress: 60
        },
        s4: {
            nextStep: "s5",
            backStep: "s3",
            stepNumber: 3,
            progress: 90
        },
        s5: {
            nextStep: "submit",
            backStep: "s4",
            stepNumber: 6,
            progress: 100
        },
        s6: {
            nextStep: "submit",
            backStep: "s4",
            stepNumber: 6,
            progress: 100
        },
        s7: {
            nextStep: "submit",
            backStep: "s4",
            stepNumber: 6,
            progress: 100
        }
    },
    formPosted = false;

    var trackingCode = function () {
        var capterra_vkey = 'd829b15f01f901a458f173bf5d1ef3ac',
        capterra_vid = '2100065',
        capterra_prefix = (('https:' == document.location.protocol) ? 'https://ct.capterra.com' : 'http://ct.capterra.com');
        (function() {
            var ct = document.createElement('script'); ct.type = 'text/javascript'; ct.async = true;
            ct.src = capterra_prefix + '/capterra_tracker.js?vid=' + capterra_vid + '&vkey=' + capterra_vkey;
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ct, s);
        })();
    };

    var step = function (targetStep) {
        var forwardStep = targetStep != "submit" ? steps[targetStep].nextStep : "submit",
            shouldValidate = true,
            currentStep = $("#freeTrial .steps .step.active").attr("id");
        if (targetStep != "submit") {
            forwardStep = steps[targetStep].nextStep;
            while (forwardStep != "submit") {
                if (forwardStep == currentStep) {
                    shouldValidate = false;
                    break;
                }
                forwardStep = steps[forwardStep].nextStep;
            }
        }

        if (targetStep != currentStep && shouldValidate) {
            var erroredElements = [];
            switch (currentStep) {
                case "s2":
                    var fTFullName = $("#fTFullName"),
                        fTEmail = $("#fTEmail");

                    $(".field-validation-error").slideUp(300);

                    if (is.empty(fTFullName.val())) {
                        $(".field-validation-error[data-for='fTFullName']").html("<span>Full name is required.</span>").hide(0).slideDown(300);
                        fTFullName.on("blur.validation", function () {
                            $(".field-validation-error[data-for='fTFullName']").slideUp(300);
                        });
                        erroredElements.push(fTFullName);
                    }
                    if (is.empty(fTEmail.val()) || is.not.email(fTEmail.val())) {
                        if (is.empty(fTEmail.val())) {
                            $(".field-validation-error[data-for='fTEmail']").html("<span>Email is required.</span>").hide(0).slideDown(300);
                        } else if (is.not.email(fTEmail.val())) {
                            $(".field-validation-error[data-for='fTEmail']").html("<span>Email is not valid.</span>").hide(0).slideDown(300);
                        }
                        fTEmail.on("blur.validation", function () {
                            $(".field-validation-error[data-for='fTEmail']").slideUp(300);
                        });
                        erroredElements.push(fTEmail);
                    }
                    if (is.not.empty(erroredElements)) {
                        erroredElements[0].select();
                        return;
                    }
                    break;
                case "s3":
                    var fTOrganizationName = $("#fTOrganizationName");

                    $(".field-validation-error").slideUp(300);

                    if (is.empty(fTOrganizationName.val())) {
                        $(".field-validation-error[data-for='fTOrganizationName']").html("<span>Organization name is required.</span>").hide(0).slideDown(300);
                        fTOrganizationName.on("blur.validation", function () {
                            $(".field-validation-error[data-for='fTOrganizationName']").slideUp(300);
                        });
                        erroredElements.push(fTOrganizationName);
                    }
                    if (is.not.empty(erroredElements)) {
                        erroredElements[0].select();
                        return;
                    }
                    break;
                case "s4":
                    if (formPosted) {
                        break;
                    }
                    var form = {
                        FullName: $("#fTFullName").val(),
                        Email: $("#fTEmail").val(),
                        Phone: $("#fTPhone").val(),
                        BizName: $("#fTOrganizationName").val(),
                        BizWeb: $("#fTOrganizationWebsite").val(),
                        Revenue: $("#fTAnnualRevenue").val(),
                        HowHear: $("#fTRefferedFrom").val(),
                        Message: $("#fTMessage").val(),
                    };

                    blockUI();

                    $.ajax({
                        method: "POST",
                        url: "/FreeTrial",
                        data: form
                    }).done(function (data) {
                        formPosted = true;
                        if (data.status) {
                            $("#fTSFullName").html(form.FullName);
                            $("#fTSEmail").html(form.Email);
                            $("#fTSOrganizationNAme").html(form.BizName);
                            trackingCode();
                            step("s5");
                        } else {
						if(data.code == '' || data.code == null || data.code == 'undefined'){
						    step("s7");
						}
						else{
                            switch (data.code.toString()) {
                                case "214":
                                    step("s6");
                                    break;
                                default:
                                    step("s7");
                            }
						  }
                        }
                    }).complete(function () {
                        unblockUI();
                    }).fail(function (a, b, error) {
                        alert(error);
                        return;
                    });
                    return;
            }
        }

        if (targetStep != "submit") {
            $("#freeTrial").css("overflow", "hidden");
            var animatedOut, animatedIn;
            animatedOut = $("#freeTrial .steps .step:not(#" + targetStep + ")").removeClass("active").addClass("animated fadeOutUp").slideUp(300);
            animatedIn = $("#freeTrial .steps #" + targetStep + ".step").addClass("active animated fadeInUp").slideDown(300);
            setTimeout(function () {
                animatedOut.removeClass("animated fadeOutUp");
                animatedIn.removeClass("animated fadeInUp").find("input,button,textarea").first().focus();
                $("#freeTrial").css("overflow", "auto");
            }, 300);
            if (targetStep == "s4") {
                $("#freeTrial footer .form-group .btn#fTNext").text("Submit");
            } else {
                $("#freeTrial footer .form-group .btn#fTNext").text("Next");
            }
            if (targetStep == "s1" || targetStep == "s5" || targetStep == "s6" || targetStep == "s7") {
                $("#freeTrial .steps-counter, #freeTrial .progress, #freeTrial footer").fadeOut(300);
                if (targetStep == "s5") {
                    $("#freeTrial").addClass("last-step");
                } else {
                    $("#freeTrial").removeClass("last-step");
                }
                if (targetStep == "s6") {
                    $("#freeTrial").addClass("warn-step");
                } else {
                    $("#freeTrial").removeClass("warn-step");
                }
                if (targetStep == "s7") {
                    $("#freeTrial").addClass("error-step");
                } else {
                    $("#freeTrial").removeClass("error-step");
                }
            } else {
                $("#freeTrial .steps-counter, #freeTrial .progress, #freeTrial footer").fadeIn(300);
            }

            $("#fTNext").data("target", steps[targetStep].nextStep);
            $("#fTBack").data("target", steps[targetStep].backStep);
            $("#freeTrial .progress .progress-bar").animate({ width: steps[targetStep].progress + "%" }).attr("aria-valuenow", steps[targetStep].progress);
            $("#freeTrial .steps-counter .cstep").text(steps[targetStep].stepNumber);
        }
    }

    $(".stepper[data-target]").click(function (e) {
        e.preventDefault();
        var targetStep = $(this).data("target");
        step(targetStep);
    });

    var openFreeTrialPopUp = function () {
        window.location = "/freetrial";
    }

    var closeFreeTrialPopUp = function () {
        $("body").removeClass("free-trial-opened");
        $("#freeTrial").fadeOut();
        step("s1");
        formPosted = false;
    }

    $(".call-free-trial").click(openFreeTrialPopUp);

    $(".close-free-trial").click(closeFreeTrialPopUp);

    $("#freeTrial .steps .step input[type='email'],\
    #freeTrial .steps .step input[type='tel'],\
    #freeTrial .steps .step input[type='password'],\
    #freeTrial .steps .step input[type='text'],\
    #freeTrial .steps .step textarea\
    #freeTrial .steps .step button").on("keyup", function (e) {
        if (e.which == 13) {
            $("#fTNext").trigger("click");
        }
        if (e.which == 27) {
            closeFreeTrialPopUp();
        }
    });

    $("#freeTrial .steps .step select").each(function () {
        return new SelectFx(this);
    });

})(window);