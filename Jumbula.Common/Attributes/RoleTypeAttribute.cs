﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using System;


namespace Jumbula.Common.Attributes
{
    public class RoleTypeAttribute : Attribute
    {
        internal RoleTypeAttribute(RoleCategoryType roleType)
        {
            RoleType = roleType;
        }

        public RoleCategoryType RoleType { get; private set; }

        public static RoleCategoryType GetRoleType(RoleCategory role)
        {
            return role.GetAttribute<RoleTypeAttribute>().RoleType;
        }
    }
}
