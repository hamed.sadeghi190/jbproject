$(document).ready(function () {
    $('#Grid').mixitup({
        effects: ['fade']
    });

    //Clear btn
    $('.clear').click(function(){
        $(this).prev('.Date').val('');
    });

    //tooltip
    $('.tool-tip').tooltip();

    //help icons on choose template popover
    $('.help-icon').popover();
    $('.help-icon').mouseout(function(){
        $(this).popover("hide");
    });

    ////search bar hide/show
    //$('#search-icon').click(function () {
    //    $("html, body").animate({ scrollTop: 0 }, "slow");
    //    $('#search-bar').slideToggle();
    //    $('#search-close').fadeIn();
    //    $('#search-pointer').fadeToggle();
    //});
    //$('#search-close').click(function () {
    //    $('#search-bar').slideUp(500);
    //    $('#search-close').fadeOut();
    //    $('#search-pointer').fadeOut();
    //});

    // Date
    InitialDateFilters();

    $("#ResetDate").click(function () {
        $("#StartDate, #FinishDate").val("");

        //
        InitialDateFilters();

        blockUI();
        setURL();
    });

});
//hide/show search pointer on page scroll
$(window).bind('scroll', function () {
    if ($('#search-bar').css('display') != 'none') {
        if ($(this).scrollTop() > 1) {
            $('#search-pointer').fadeOut(1);
        }
        else {
            $('#search-pointer').fadeIn(1);
        }
    }
    else {
        $('#search-pointer').hide();
    }
});

// date picker functions
function InitialFinishDate(sDate) {
    var show = false;
    if (sDate == null) {
        sDate = new Date("2012/01/01");
    }
    else {
        show = true;
    }
    $("#FinishDate").datepicker('destroy');
    $("#FinishDate").datepicker({
        minDate: sDate,
        onSelect: function () {
            eDate = new Date($(this).val());
            InitialStartDate(eDate);
        }
    });
    if (show) {
        $("#FinishDate").focus(1, function () {
            return false;
        });
    }
}

function InitialStartDate(eDate) {
    if (eDate == null) {
        eDate = new Date("2099/01/01");
    }
    $("#StartDate").datepicker('destroy');
    $("#StartDate").datepicker({
        maxDate: eDate,
        onSelect: function () {
            sDate = new Date($(this).val());
            InitialFinishDate(sDate);
        }
    });
}
function InitialDateFilters() {
    InitialStartDate(null);
    $("#FinishDate").datepicker({});
}

//nav fixing/absolute position on page scroll
$(document).ready(function () {
    var nav = $(".myjumbulanav");
    var navHeight = nav.height();
    var pos = nav.offset().top;
    var btmHeight = $(".footer").height() + $(".copyright").height() + 15;
    $(window).scroll(function () {
        var windowpos = $(window).scrollTop() + 52;
        if (windowpos >= pos) {
            nav.addClass("fix");
        }
        else {
            nav.removeClass("fix");
        }
        if ($(window).scrollTop() + navHeight + 52 > $(document).height() - btmHeight) {
            nav.removeClass("fix");
            nav.addClass("absolute");
        }
        else {
            nav.removeClass("absolute");
        }
        $(function () {
            showWidth("myjumbulanav", $(".myjumbulanav").width());
            showHeight("myjb-content", $(".myjb-content").height());

        });
//nav fixing/ set equal width
        function showWidth(ele, w) {
            $(".myjumbulanav ul").css({'width': w});

        }
    });
    $(window).resize(function () {
        $(function () {
            showWidth("myjumbulanav", $(".myjumbulanav").width());
        });
//nav fixing/ set equal width
        function showWidth(ele, w) {
            $(".myjumbulanav ul").css({'width': w});
        }
    });
    $(function () {
        showHeight("myjb-content", $(".myjb-content").height());

    });
//nav fixing/ set equal height
    function showHeight(ele, h) {

        var myjumbulanavheight = h - $(".myjumbulatext").height() + 15;


        $(".myjumbulanav").css({'height': myjumbulanavheight });


    }
    // add loading style to buttons
    $('.loading').click( function(){
        $(this).button('loading');
    });

    // choose sport type events template
    $("li.modal-item.camp").click(function(){
        if($(this).hasClass("active")){
            $("li.modal-item").addClass("blur");
            $(this).removeClass("blur");
            $("li.modal-item").addClass("active");
            $(this).removeClass("active");
            $(".div-sports").hide();
            $(".camp-sports").slideToggle();
        }
    });

    $("li.modal-item.tournament").click(function(){
        if($(this).hasClass("active")){
            $("li.modal-item").addClass("blur");
            $(this).removeClass("blur");
            $("li.modal-item").addClass("active");
            $(this).removeClass("active");
            $(".div-sports").hide();
            $(".tournament-sports").slideDown();
        }
    });
    $("li.modal-item.class").click(function(){
        if($(this).hasClass("active")){
            $("li.modal-item").addClass("blur");
            $(this).removeClass("blur");
            $("li.modal-item").addClass("active");
            $(this).removeClass("active");
            $(".div-sports").hide();
            $(".class-sports").slideToggle();
        }
    });

    // fix button deck in image gallery image view
    $(window).scroll(function () {
        if ($(window).scrollTop() > 75) {
            $(".btn-deck").addClass("fix");
        }
        else {
            $(".btn-deck").removeClass("fix");
        }
    });

    //checkbox radio btn classes
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-orange',
        radioClass: 'iradio_square-orange'
    });


    //hide/show usatt id section in add entry lightbox
    $('#NoUscfIdChecked').on('ifChanged', function () {
        if ($(this).is(":checked")) {
            $("#usttSection").slideUp();
            $("#usttRegSection").slideDown();
        }
        else {
            $("#usttSection").slideDown();
            $("#usttRegSection").slideUp();
        }

    });

   
});

