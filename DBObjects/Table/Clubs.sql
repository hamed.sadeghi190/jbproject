If Object_ID('[dbo].[Clubs]') Is Null
BEGIN
	CREATE TABLE [dbo].[Clubs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Domain] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Logo] [nvarchar](128) NULL,
	[Site] [nvarchar](256) NULL,
	[ExternalId] [nvarchar](32) NULL,
	[MeetUpTimes] [nvarchar](max) NULL,
	[ExternalType] [int] NOT NULL,
	[Geography] [geography] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[PaymentAppStatus] [int] NOT NULL,
	[ClubFeeRateCode] [int] NOT NULL,
	[MetaData_DateCreated] [datetime] NOT NULL,
	[MetaData_DateUpdated] [datetime] NOT NULL,
	[Configuration] [nvarchar](max) NULL,
	[Announce] [nvarchar](max) NULL,
	[Address_Id] [int] NULL,
	[IsAffiliator] [bit] NOT NULL,
	[AffiliatorID] [int] NULL,
	[TimeZone] [int] NULL,
	[CoverImage] [nvarchar](128) NULL,	
	[OraganizationType] [int] NOT NULL)
END

GO


--<< DROP COLUMN >>--
declare @constraint_name sysname, @sql nvarchar(max)

select @constraint_name = name 
from sys.default_constraints 
where parent_object_id = object_id('dbo.clubs')
AND type = 'D'
AND parent_column_id = (
    select column_id 
    from sys.columns 
    where object_id = object_id('dbo.clubs')
    and name = 'DisplayEmail'
    )

IF EXISTS(SELECT 1 FROM sys.objects WHERE OBJECT_ID=Object_ID(@constraint_name))
BEGIN
	set @sql = N'alter table dbo.clubs drop constraint ' + @constraint_name
	exec sp_executesql @sql
END
GO

declare @constraint_name sysname, @sql nvarchar(max)

select @constraint_name = name 
from sys.default_constraints 
where parent_object_id = object_id('dbo.clubs')
AND type = 'D'
AND parent_column_id = (
    select column_id 
    from sys.columns 
    where object_id = object_id('dbo.clubs')
    and name = 'DisplayWebsite'
    )

IF EXISTS(SELECT 1 FROM sys.objects WHERE OBJECT_ID=Object_ID(@constraint_name))
BEGIN	
	set @sql = N'alter table dbo.clubs drop constraint ' + @constraint_name
	exec sp_executesql @sql
END
GO

IF EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=Object_ID('[dbo].[Clubs]') AND [name]='DisplayEmail')
BEGIN
	ALTER TABLE [dbo].[Clubs] Drop Column DisplayEmail
END
GO

IF EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=Object_ID('[dbo].[Clubs]') AND [name]='DisplayWebsite')
BEGIN
	ALTER TABLE [dbo].[Clubs] Drop Column DisplayWebsite
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'FK_dbo.Clubs_dbo.JbForms_JbForm_Id')
BEGIN
  ALTER TABLE [Clubs] DROP CONSTRAINT [FK_dbo.Clubs_dbo.JbForms_JbForm_Id];
END

IF EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=Object_ID('[dbo].[Clubs]') AND [name]='JbForm_Id')
BEGIN
	ALTER TABLE [dbo].[Clubs] Drop Column [JbForm_Id]
END
GO


