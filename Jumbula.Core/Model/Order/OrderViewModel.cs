﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Globalization;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Constants = Jumbula.Common.Constants.Constants;
using PaymentConstants = Jumbula.Common.Constants.Payment;
using Jumbula.Core.Model.Form;

namespace Jumbula.Core.Model.Order
{
    public class OrderViewModel : BaseViewModel<Domain.Order, long>
    {
        public OrderViewModel()
        {
            CancelAll = true;
            OrderInfo = new OrderInformationViewModel();
        }
        public OrderMode OrderMode { get; set; }
        public string UserName { get; set; }
        public string OrderDate { get; set; }
        public string ConfirmationId { get; set; }
        public CurrencyCodes Currency { get; set; }
        public decimal OrderAmount { get; set; }
        public OrderInformationViewModel OrderInfo { get; set; }
        public string TotalPrices => CurrencyHelper.FormatCurrencyWithPenny(OrderAmount, Currency);
        public bool CancelAll { get; set; }

        public OrderStatusCategories OrderStatus { get; set; }

        public List<OrderItemsViewModel> OrderItems { get; set; }

        public decimal TotalCancelAmount { get; set; }

        public decimal CancellationFee { get; set; }
        public decimal PaidAmount
        {
            get
            {
                if (OrderItems != null && OrderItems.Count > 0)
                {
                    return OrderItems.Sum(o => o.PaidAmount);
                }
                return 0;
            }

        }

        public bool RefundAutomatically { get; set; }

        public decimal RefundAmount => TotalCancelAmount - CancellationFee;

        public string CancellationNote { get; set; }

        public string Balance => CurrencyHelper.FormatCurrencyWithPenny(OrderAmount - PaidAmount, Currency);
    }


    public class OrderItemsViewModel : BaseViewModel<OrderItem, long>
    {
        public OrderItemsViewModel()
        {
            IsSelected = true;
            CustomRefund = new OrderChargeDiscount();
            OrderInfo = new OrderInformationViewModel();
            Editable = false;
            UpdateForm = false;
        }
        public OrderItemsViewModel(CurrencyCodes currency)
        {
            Currency = currency;
            IsSelected = true;
            CustomRefund = new OrderChargeDiscount();
            Editable = false;
            UpdateForm = false;
        }

        public OrderInformationViewModel OrderInfo { get; set; }
        public long IntallmentId { get; set; }
        public bool Editable { get; set; }
        public bool UpdateForm { get; set; }

        public TournamentScheduleAttribute ChessTournamentAtttribute { get; set; } 

        public string SectionName { get; set; }

        public string ChessScheduleName { get; set; }

        public List<RefundInformation> RefundModes { get; set; }

        public string ConfirmationId { get; set; }
        public string CurrentUserName { get; set; }
        public RoleCategoryType CurrentRoleType { get; set; }
        public int CurrentUserId { get; set; }
        public ClubBaseInfoModel CurrentClub { get; set; }
        public string SelectedForm { get; set; }
        public bool IsSelectedForm { get; set; }
        public List<SelectListItem> Forms { get; set; }
        public List<SelectListItem> AllOrdersThisPlayer { get; set; }
        public List<ParentOrderItemInstallmentsViewModel> Installments { get; set; }
        public List<ParentOrderItemInstallmentsViewModel> AddOnInstallments { get; set; }
        public decimal AmountAutomaticallyTransactions { get; set; }
        public bool EnableInstallmentRefund { get; set; }
        public bool EnableInstallmentTakePayment { get; set; }
        public List<string> SelectedOrders { get; set; }
        public bool IsPartner { get; set; }
        public long ProgramId { get; set; }
        public bool HasProgramScheduleId { get; set; }
        public bool HasRegistraitionForm { get; set; }
        public int UserId { get; set; }
        public bool IsLiveOrder { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal PaidAmount { get; set; }
        public long OrderId { get; set; }
        public string FullName { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public bool IsSelected { get; set; }
        public CurrencyCodes Currency { get; set; }
        public ProgramTypeCategory ProgramType { get; set; }

        public OrderItemMode Mode { get; set; }

        public PaymentPlanStatus PaymentPlanStatus { get; set; }
        public List<DayOfWeek> Days { get; set; }
        public string DaysName
        {
            get
            {
                if (Days != null && Days.Any())
                {
                    return string.Join(", ", Days);
                }
                return string.Empty;
            }
        }

        public bool IsDropIn { get; set; }

        public List<string> DropInSessions { get; set; }

        public string DesiredStartDate { get; set; }
        public string EffectiveTransferredDate { get; set; }
        public string EffectivecanceledDate { get; set; }
        public string TotalAmountStr => CurrencyHelper.FormatCurrencyWithPenny(TotalAmount, Currency);
        public decimal EntryFee { get; set; }
        public bool IsDeletedEntryFee { get; set; }
        public string EntryFeeName { get; set; }
        public string Name { get; set; }
        public string OrderDate { get; set; }
        public bool IsItemCanceled { get; set; }

        public decimal Balance
        {
            get;
            set;

        }
        public OrderItemStatusCategories ItemStatus { get; set; }
        public int IntItemStatus => (int)ItemStatus;
        public OrderItemStatusReasons ItemStatusReason { get; set; }
        public PaymentPlanType PaymentPlanType { get; set; }
        public ICollection<OrderChargeDiscountViewModel> OrderChargeDiscounts
        {
            set
            {

                if (value != null && value.Count > 0)
                {
                    Charges = value.Where(c => c.Subcategory == ChargeDiscountSubcategory.Charge && c.Category != ChargeDiscountCategory.EntryFee).ToList();
                    Charges.ForEach(c => c.OldAmount = c.Amount);

                    Discounts = value.Where(c => c.Subcategory == ChargeDiscountSubcategory.Discount).ToList();
                }

            }
        }
        public List<OrderChargeDiscountViewModel> Charges
        {
            get;
            set;

        }
        public List<OrderChargeDiscountViewModel> Discounts
        {
            get;
            set;
        }
        public bool HasInstallments { get; set; }

        public bool IsFullPaid => !HasInstallments;

        public bool HasAddOnInstallments { get; set; }
        public bool HasTransactions { get; set; }
        public bool IsInstallmentAutoCharge { get; set; }
        public object Gender { get; set; }
        public int Age { get; set; }
        public object Grade { get; set; }
        public string JbFormHtml { get; set; }
        public string ProgramJbFormHtml { get; set; }
        public int? JbFormId { get; set; }
        public OrderChargeDiscount CustomRefund { get; set; }
        public List<FollowUpFormViewModel> FollowupForms { get; set; }

        public List<OrderitemWaiversViewModel> Waivers { get; set; }
        public RefundMode RefundMode { get; set; }

        public bool SelectedToCancel
        {
            get
            {
                return IsSelected || (Charges != null && Charges.Any(o => o.IsSelected));
            }
        }

        public decimal TotalSelectedCharge
        {
            get
            {
                return Charges?.Where(c => c.IsSelected).Sum(c => c.Amount) ?? 0;
            }
        }

    }

    public enum RefundMode : byte
    {
        Automatically,
        Manually,
        AddToCredit
    }

    public class SubscriptionProgramDaysViewModel
    {
        public bool IsChecked { get; set; }
        public string Title { get; set; }

        public DayOfWeek Day { get; set; }
    }
    public class InstallmentViewModel : BaseViewModel<OrderInstallment, long>
    {
        public DateTime InstallmentDate { get; set; }
        public CurrencyCodes Currency { get; set; }
        public string StrInstallmentDate { get; set; }

        public bool IsDeleted { get; set; }
        public string StrPaidDate => PaidDate.HasValue ? PaidDate.Value.ToString(Constants.DefaultDateFormat, new CultureInfo("en-US")) : "";
        public DateTime? PaidDate { get; set; }
        public decimal? PaidAmount { get; set; }
        public decimal Amount { get; set; }

        public InstallmentType Type { get; set; }

        public decimal Balance => !IsDeleted ? Amount - (PaidAmount ?? 0) : PaidAmount ?? 0;
        public decimal OldBalance { get; set; }
        public decimal NewBalance { get; set; }
        public decimal RefundAmount { get; set; }
        public string StrPaidAmount => CurrencyHelper.FormatCurrencyWithPenny(PaidAmount, Currency);
        public string StrAmount => CurrencyHelper.FormatCurrencyWithPenny(Amount, Currency);
        public string StrBalance => CurrencyHelper.FormatCurrencyWithPenny(Balance, Currency);
        public string PaymentStatus => Status == OrderStatusCategories.completed ? "Paid" : Status == OrderStatusCategories.notInitiated || Status == OrderStatusCategories.initiated ? "-" : Status.ToDescription();

        public bool NoticeSent { get; set; }
        public OrderStatusCategories Status { get; set; }
        public bool IsSelected { get; set; }
        public bool IsDeposit { get; set; }

        public ProgramSubscriptionItem Schedule { get; set; }
        public string SchedulePart { get; set; }
        public ProgramTypeCategory ProgramType { get; set; }
        public string StrSchedule
        {
            get
            {
                if (ProgramType == ProgramTypeCategory.Subscription)
                {
                    return SchedulePart;
                }
                else if (ProgramType == ProgramTypeCategory.BeforeAfterCare)
                {
                    return SchedulePart;
                }
                else
                {
                    return null;
                }

            }
        }
        public bool IsBelongTo { get; set; }
        public decimal AmountAutomaticallyTransactions { get; set; }
    }
    public class OrderChargeDiscountViewModel : BaseViewModel<OrderChargeDiscount, long>
    {
        public OrderChargeDiscountViewModel()
        {
            IsSelected = true;
            OldAmount = Amount;
        }
        [MaxLength(512, ErrorMessage = "{0} cannot be longer than 512 characters.")]
        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Amount")]
        public decimal Amount { get; set; }
        public decimal OldAmount { get; set; }
        public string Description { get; set; }
        public ChargeDiscountCategory Category { get; set; }
        public ChargeDiscountSubcategory Subcategory { get; set; }
        public int? OrderId { get; set; }
        public int? OrderItemId { get; set; }
        public long? ChargeId { get; set; }
        public long? DiscountId { get; set; }

        public bool IsSelected { get; set; }

        public bool IsDeleted { get; set; }

    }

    public class TransactionActivitiesViewModel : BaseViewModel<TransactionActivity, long>
    {
        public string Participant { get; set; }
        public string ConfirmationId { get; set; }
        public string SeasonDomain { get; set; }
        public DateTime TransactionDate { get; set; }
        public bool IsAutoCharge { get; set; }
        public string CardNumber { get; set; }
        public string StrTransactionDate => TransactionDate.ToString(Constants.DefaultDateFormat, new CultureInfo("en-US"));
        public string StrTransactionDatePopup => DateTimeHelper.FormatShortDateTimeSeperetedByCamma(TransactionDate); 
        public decimal Amount { get; set; }
        public CurrencyCodes Currency { get; set; }
        public string StrAmount => (TransactionType == TransactionType.Refund) ? CurrencyHelper.FormatCurrencyWithPenny(-1 * Amount, Currency, true) : CurrencyHelper.FormatCurrencyWithPenny(Amount, Currency);
        public string Payment => (TransactionType == TransactionType.Payment) ? CurrencyHelper.FormatCurrencyWithPenny(Amount, Currency) : string.Empty;
        public string Refund => (TransactionType == TransactionType.Refund) ? CurrencyHelper.FormatCurrencyWithPenny(Amount, Currency) : string.Empty;

        public TransactionCategory TransactionCategory { get; set; }

        public string StrCategory => (TransactionCategory == TransactionCategory.Sale) ? "Enrollment" : TransactionCategory == TransactionCategory.Refund && PaymentDetail.PaymentMethod == PaymentMethod.Credit ? "Family credit transfer" : TransactionCategory.ToDescription();
        public long? InstallmentId { get; set; }

        public string Payer => IsAutoCharge ? "Autocharge" :
            PaymentDetail.PayerType == RoleCategoryType.Dashboard ? "Admin" :
            PaymentDetail.PayerType.ToDescription();

        public PaymentPlanType PaymentPlan { get; set; }
        public string InvoiceCreateDate { get; set; }
        public string InvoiceDueDate { get; set; }
        public string InvoicePaidDate { get; set; }
        public long InvoiceNumber { get; set; }
        public string StrDueDate { get; set; }
        public TransactionType TransactionType { get; set; }
        public string Note { get; set; }
        public string CheckId { get; set; }
        public long? OrderId { get; set; }
        public long? OrderItemId { get; set; }
        public long PaymentDetailId { get; set; }

        public HandleMode HandleMode { get; set; }

        public string Token { get; set; }

        public long ProgramId { get; set; }
        public bool EnableUndoRefund { get; set; }

        public PaymentMethod PaymentMethod
        {
            get
            {
                if (PaymentDetail != null)
                {
                    return PaymentDetail.PaymentMethod;
                }
                return PaymentMethod.Cash;
            }
        }

        public string ErrorMessage
        {
            get
            {
                if (PaymentDetail != null)
                {
                    return PaymentDetail.TransactionMessage?.Replace("'", "");
                }
                return "";
            }
        }

        public string StrPaymentMethod => TransactionStatus == TransactionStatus.Invoice ? "-" : PaymentMethod.ToDescription();

        public string TransactionId
        {
            get
            {
                if (PaymentDetail != null && !string.IsNullOrEmpty(PaymentDetail.TransactionId))
                {
                    return PaymentDetail.TransactionId;
                }

                return CheckId ?? "";
            }
        }
        public TransactionStatus TransactionStatus { get; set; }
        public string StrTransactionStatus
        {
            get
            {
                switch (TransactionStatus)
                {
                    case TransactionStatus.Invoice:
                        return TransactionStatus.Pending.ToDescription();
                    case TransactionStatus.Failure:
                        return "Failed";
                    default:
                        return TransactionStatus.ToDescription();
                }
            }
        }
        public PaymentDetailViewModel PaymentDetail { get; set; }

    }

    public class PaymentDetailViewModel : BaseViewModel<PaymentDetail, long>
    {

        public PaymentMethod PaymentMethod { get; set; }
        public string Currency { get; set; }
        public string TransactionMessage { get; set; }
        public RoleCategoryType PayerType { get; set; }
        public string TransactionId { get; set; }
    }

    public class RefundInformation
    {
        public string Title { get; set; }
        public int Priority { get; set; }
        [RegularExpression(Common.Constants.Validation.PriceRegex, ErrorMessage = Common.Constants.Validation.PriceRegexErrorMessage)]
        public decimal? Amount { get; set; }
        public RefundMode Mode { get; set; }
        public bool IsSelected { get; set; }
        public decimal MaxAmount { get; set; }
    }

}