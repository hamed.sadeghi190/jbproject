﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('participantClassSchedulePeopleController', ['$scope', '$http', '$log', '$stateParams', '$state', 'cfpLoadingBar', 'customReportService', function ($scope, $http, $log, $stateParams, $state, cfpLoadingBar, customReportService) {
        window.app.globalObjects.pageClass = 'registerant-people-view';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/People';

        $scope.MC.openBlade({ url: "Dashboard/Blade/People", order: 0, menu: "PEOPLE" });

        $http.post(baseUrl + '/GetParticipantClassSchedule', {playerId: $scope.MC.stateParams.playerId, printMode: ""})
            .success(function (data) {
               
                $scope.Model = data.DataSource;

            }).error(function () {
                //alert(data);
            });

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $http.post(baseUrl + '/GetParticipantClassSchedule', {
                playerId: $scope.MC.stateParams.playerId,
                printMode: "excel"
            }, {
                    headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
                }).success(function (data) {
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    var blob = new Blob([data], { type: "application/excel" });

                    var fileUrl = URL.createObjectURL(blob);
                    a.href = fileUrl;
                    a.download = "ParticipantClassSchedule.xls";
                    a.click();
                    cfpLoadingBar.complete();
                });
        };        

        $scope.saveToPdf = function() {
            cfpLoadingBar.start();
            $http.post(baseUrl + '/GetParticipantClassSchedule', {               
                playerId: $scope.MC.stateParams.playerId,               
                printMode: "pdf"
            }, {
                headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
            }).success(function (data) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                var blob = new Blob([data], { type: "application/pdf" });

                var fileUrl = URL.createObjectURL(blob);
                a.href = fileUrl;
                a.download = "ParticipantClassSchedule.pdf";
                a.click();
                cfpLoadingBar.complete();
                });
        }


    }]);
})();