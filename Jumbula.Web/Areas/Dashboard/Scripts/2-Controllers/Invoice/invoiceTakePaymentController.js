﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('invoiceTakePaymentController', ['$scope', '$http', 'jbToast', '$stateParams', '$state', 'TakePaymentInvoiceService', function ($scope, $http, jbToast, $stateParams, $state, TakePaymentInvoiceService) {
        window.app.globalObjects.pageClass = 'invoice_View';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + "/Dashboard/Invoice";

        var returnedByBack = $state.returnByBack();
        $scope.Model = {};
        if (returnedByBack) {
            $scope.Model = TakePaymentInvoiceService.get();
        }
        if (!returnedByBack || $scope.Model == null) {

            $http.get($scope.baseUrl + '/GetTakePaymentModel?invoiceId=' + $stateParams.invoiceId).success(function (data, status, headers, config) {

                $scope.Model = data;
            });
        }

        $scope.confirmPaymentInvoice = function () {
            $http.post($scope.baseUrl + '/ValidateInvoiceTakePayment', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        if ($scope.Model.SelectedChargeMethod == null || $scope.Model.SelectedChargeMethod == '') {
                            jbToast.error('You must choose the method of payment.');
                        } else {
                            TakePaymentInvoiceService.set($scope.Model);
                            $state.forward('ConfirmTakePaymentInvoice', { invoiceId: $stateParams.invoiceId }, 'TakePaymentInvoice', { invoiceId: $stateParams.invoiceId }, null);
                        }
                    } else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        }

        $scope.cancelAction = function () {
            if ("comesFromOrderItem" in window) {
                if (window.comesFromOrderItem) {
                    window.comesFromOrderItem = false;
                    $state.go("Invoices", null);
                }
            }
            else {
                $state.go("Invoices", null);
            }
        }
        if (returnedByBack) {
            $scope.MC.backQueueItems.pop();
        }
    }])

})();