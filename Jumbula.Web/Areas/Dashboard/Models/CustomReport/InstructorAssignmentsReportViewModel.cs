﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class InstructorAssignmentsReportViewModel
    {
        public string Name { get; set; }

        public string Status { get; set; }

        public string DateOfBackgroundCheck { get; set; }

        public string ClassAssignments { get; set; }
    }
}