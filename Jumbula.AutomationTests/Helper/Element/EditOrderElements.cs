﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element
{
    class EditOrderElements : BaseElements
    {
        public EditOrderElements(IWebDriver driver)
            : base(driver)
        {

        }

        #region Header
        public IWebElement BackLink
        {
            get
            {
                return Driver.FindElement(By.XPath("//a[@class='icon-arrow-left']"));
            }
        }
        public IWebElement PageTitle
        {
            get
            {
                return Driver.FindElement(By.XPath("//div[@class='title']/h1[1]"));
            }
        }

        #endregion Header
        #region Amounts
        public IWebElement OrderAmount
        {
            get
            {
                return Driver.FindElement(By.Id("orderAmount"));
            }
        }
        public IWebElement NewAmount
        {
            get
            {
                return Driver.FindElement(By.Id("newAmount"));
            }
        }
        public IWebElement PaidAmount
        {
            get
            {
                return Driver.FindElement(By.Id("paidAmount"));
            }
        }
        public IWebElement Difference
        {
            get
            {
                return Driver.FindElement(By.Id("difference"));
            }
        }
        public IWebElement RemainingBalance
        {
            get
            {
                return Driver.FindElement(By.Id("remaining"));
            }
        }
        #endregion Amounts
        #region Main info
        public IWebElement Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='bodyPanel']/div[1]/div/div[4]/div/div[2]/h2/span[1]"));
            }
        }
        public IWebElement Confirmation
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='bodyPanel']/div[1]/div/div[4]/div/div[2]/h2/span[2]"));
            }
        }
        #endregion Main info
        #region Step1ChargeDiscount
        public IWebElement Step1ChargeDiscounts(int row, string column, string elm)
        {
            var headers = Driver.FindElements(By.XPath("//*[@id='tbl-ChargeDiscountItem']//th"));
            var index = 1;
            for (int i = 0; i < headers.Count; i++)
            {
                if (headers[i].Text == column)
                {
                    break;
                }
                index++;
            }
            if(!string.IsNullOrEmpty(elm))
            {
                return Driver.FindElement(By.XPath(string.Format("//*[@id='tbl-ChargeDiscountItem']//tr[{0}]/td[{1}]//{2}", row, index, elm)));
            }
            else
            {
                return Driver.FindElement(By.XPath(string.Format("//*[@id='tbl-ChargeDiscountItem']//tr[{0}]/td[{1}]", row, index)));
            }
            
        }
        public  IWebElement Step1ChargeDiscountsAction(int row, string action)
        {
            return Driver.FindElement(By.XPath(string.Format("//*[@id='tbl-ChargeDiscountItem']//tr[{0}]//span[contains(text(),'{1}')]", row, action)));
        }
        public bool Step1ChargeDiscountsActionExist(int row, string action)
        {
            if (Driver.FindElements(By.XPath(string.Format("//*[@id='tbl-ChargeDiscountItem']//tr[{0}]//span[contains(text(),'{1}')]", row, action))).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
         }
        
        
        public IWebElement Step1ChargeDiscountscancelBtn
        {
            get
            {
                return Driver.FindElement(By.Id("CancelEdit"));
            }
        }
        public IWebElement Step1ChargeDiscountsSaveBtn
        {
            get
            {
                return Driver.FindElement(By.Id("SaveAmount"));
            }
        }
        #endregion Step1Step1ChargeDiscount
        #region Step1Session
        public IWebElement Step1Session(int row, string column, string elm)
        {
            var headers = Driver.FindElements(By.XPath("//*[@id='tbl-SessionsItem']//thead//td"));
            var index = 1;
            for (int i = 0; i < headers.Count; i++)
            {
                if (headers[i].Text == column)
                {
                    break;
                }
                index++;
            }
            if(!string.IsNullOrEmpty(elm))
            {
                return Driver.FindElement(By.XPath(string.Format("//*[@id='tbl-SessionsItem']//tbody//tr[{0}]/td[{1}]//{2}", row, index, elm)));
            }
            else
            {
                return Driver.FindElement(By.XPath(string.Format("//*[@id='tbl-SessionsItem']//tbody//tr[{0}]/td[{1}]", row, index)));
            }
        }
        public IWebElement Step1SessionAction(int row, string action)
        {
            return Driver.FindElement(By.XPath(string.Format("//*[@id='tbl-SessionsItem']//tr[{0}]//span[contains(text(),'{1}')]", row, action)));
        }

        #endregion Step1Session
        public IWebElement Step2ChargeDiscounts(int row, string column, string elm)
        {
            var headers = Driver.FindElements(By.XPath("//*[@id='tbl-ReviewChargeDiscount']//thead//td"));
            var index = 1;
            for (int i = 0; i < headers.Count; i++)
            {
                if (headers[i].Text == column)
                {
                    break;
                }
                index++;
            }
            if (!string.IsNullOrEmpty(elm))
            {
                return Driver.FindElement(By.XPath(string.Format("//*[@id='tbl-ReviewChargeDiscount']//tbody//tr[{0}]/td[{1}]//{2}", row, index, elm)));
            }
            else
            {
                return Driver.FindElement(By.XPath(string.Format("//*[@id='tbl-ReviewChargeDiscount']//tbody//tr[{0}]/td[{1}]", row, index)));
            }
        }
        public IWebElement Step2Session(int row, string column, string elm)
        {
            var headers = Driver.FindElements(By.XPath("//*[@id='tbl-ReviewSessions']//thead//td"));
            var index = 1;
            for (int i = 0; i < headers.Count; i++)
            {
                if (headers[i].Text == column)
                {
                    break;
                }
                index++;
            }
            if(!string.IsNullOrEmpty(elm))
            {
                return Driver.FindElement(By.XPath(string.Format("//*[@id='tbl-ReviewSessions']//tbody//tr[{0}]/td[{1}]//{2}", row, index, elm)));
            }
            else
            {
                return Driver.FindElement(By.XPath(string.Format("//*[@id='tbl-ReviewSessions']//tbody//tr[{0}]/td[{1}]", row, index)));
            }
            
        }
           
        #region Buttons
        public IWebElement CancelBtn
        {
            get
            {
                return Driver.FindElement(By.Id("btn-Cancel"));
            }
        }
        public IWebElement ContinueBtn
        {
            get
            {
                return Driver.FindElement(By.Id("btn-Continue"));
            }
        }
        public IWebElement BackBtn
        {
            get
            {
                return Driver.FindElement(By.Id("btn-Back"));
            }
        }
        public IWebElement SubmitBtn
        {
            get
            {
                return Driver.FindElement(By.Id("btn-Submit"));
            }
        }
        #endregion Buttons
        public IWebElement SendEmail
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@id='checkEmail']/following-sibling::span"));
            }
        }
        public bool WarningExists
        {
            get
            {
                var elements = Driver.FindElements(By.TagName("h3"));
                var warning = elements.FirstOrDefault(elm => elm.Text == "Warning!");
                if (warning != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement WarningCancelBtn
        {
            get
            {
                return Driver.FindElement(By.Id("btn-No"));

            }
        }
        public IWebElement WarningYesBtn
        {
            get
            {
                return Driver.FindElement(By.Id("btn-Yes"));
            }
        }
        public IWebElement MessageboxTitle
        {
            get
            {
                return Driver.FindElement(By.ClassName("toast-title"));
            }
        }
        public IWebElement MessageboxBody
        {
            get
            {
                return Driver.FindElement(By.ClassName("toast-message"));
            }
        }
        #region Calendar
        public IWebElement PrevMonth
        {
            get
            {
                return Driver.FindElement(By.XPath("//span[contains(@class,'k-i-arrow-w')]"));
            }
        }
        public IWebElement NextMonth
        {
            get
            {
                return Driver.FindElement(By.XPath("//span[contains(@class,'k-i-arrow-e')]"));
            }
        }
        public IWebElement MonthDate
        {
            get
            {
                return Driver.FindElement(By.XPath("//span[contains(@class,'k-lg-date-format')]"));
            }
        }
        public IReadOnlyCollection<IWebElement> CalendarDay(string date)
        {
          return Driver.FindElements(By.XPath("//input[contains(@data-dodate,'" + date + "')]"));
        }
        public IWebElement CalendarSaveBtn
        {
            get
            {
                return Driver.FindElement(By.XPath("//button[contains(@ng-click,'saveSession')]"));
            }
        }
        public IWebElement CalendarCancelBtn
        {
            get
            {
                return Driver.FindElement(By.XPath("//button[contains(@ng-click,'cancelEditSessionItem')]"));
            }
        }
        #endregion Calendar
    }
}

