﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('FinanceController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();
        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.Model = customReportService.getModelFromStrorage();

        $http.get(baseUrl + '/FillFinanceFilters')
            .success(function (data, status, headers, config) {
                if (is.undefined($scope.Model.Filters) || $scope.Model.ReportName != $scope.MC.stateParams.reportName) {
                    $scope.Model.Filters = {};
                    $scope.Model.Filters.TransactionFilters = {};
                }
                $scope.Model.Filters.TransactionFilters.TransactionCategories = data.TransactionsCategories;
                $scope.Model.Filters.TransactionFilters.PaymentMethods = data.PaymentMethods;
                $scope.Model.Filters.TransactionFilters.PaymentMethod = "";
                $scope.Model.Filters.TransactionFilters.TransactionCategory = [];
                prepareGrid();
            }).error(function (data, status, headers, config) {
            });

        var prepareGrid = function() {

            $scope.seasonTransactionReportsDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport:
                {
                    read: {
                        url: baseUrl + '/FinanceReportRun',
                        type: "POST",
                        data: function() {
                            return {
                                seasonDomain: $scope.MC.stateParams.seasonDomain,
                                filters: $scope.Model.Filters,
                                term: $scope.searchTerm,
                            };
                        },
                    },

                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 100,
                batch: true,
                resizable: true,
            });

            $scope.seasonTransactionReportGridOptions = {
                excel: {
                    allPages: true,
                    fileName: "finance (" + $scope.MC.stateParams.seasonDomain + ").xlsx",
                    proxyURL: "/Dashboard/CustomReport/Save"
                },
                excelExport: function(e) {
                    cfpLoadingBar.complete();
                },
                dataSource: $scope.seasonTransactionReportsDataSource,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        50, 100, 200
                    ],
                    buttonCount: 100
                },
                dataBound: function(e) {
                    customReportService.initializeDoubleScroll();
                },
                columnMenu: true,
                columns: [
                    {
                        field: "FullName",
                        title: "Full name",
                        width: "180px",
                        lockable: true,
                        locked: true,
                    },
                    {
                        field: "ProgramName",
                        title: "Program name",
                        width: "160px",
                        lockable: true,
                        locked: true,
                    },
                    {
                        field: "UserEmail",
                        title: "Account email",
                        width: "160px",
                        lockable: true,
                        locked: true,
                    },
                    {
                        field: "PayerEmail",
                        title: "Payer email",
                        width: "180px",
                        lockable: true,
                        locked: true,
                    },
                    {
                        field: "Date",
                        title: "Transaction date",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "Confirmation",
                        title: "Confirmation",
                        template:
                            "<a style='overflow: hidden;text-overflow: ellipsis;' ng-click='goOrderDetail(#=OrderId#)' class='block'> #= Confirmation # </a>",
                        width: "140px",
                        lockable: true,
                        locked: true,
                    },
                    {
                        field: "TransactionType",
                        title: "Transaction type",
                        width: "140px",
                        lockable: true,
                    },
                    {
                        field: "TransactionCategory",
                        title: "Transaction category",
                        width: "185px",
                        lockable: true,
                    },
                    {
                        field: "CheckId",
                        title: "Transaction ID",
                        width: "180px",
                        lockable: true,
                    },
                    {
                        field: "PaymentMethod",
                        title: "Payment method",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "PaypalHandler",
                        title: "Payment handler",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "Note",
                        title: "Memo",
                    },
                    {
                        field: "Payment",
                        title: "Payment",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "Charge",
                        title: "Deposit",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "Balance",
                        title: "Balance",
                        width: "160px",
                        lockable: true,
                    }
                ]
            }
        }
        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#seasonTransactionReportGrid").getKendoGrid().saveAsExcel();
        }

        $scope.MakeCampaign = function () {
            $http.post(baseUrl + '/FinanceReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain, makeCampaign: true, term: $scope.searchTerm, filters: $scope.Model.Filters })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $scope.MC.openBlade({ url: "Dashboard/Blade/Seasons", order: 0, menu: "SEASONS" });
                        $state.go('EmailCampaignAddStep2', { campaignId: data.Data, campaignType: 'UserList' });
                    } else {
                        jbToast.error(data.Message);
                    }
                });
        }

        $scope.goOrderDetail = function (orderId) {
            customReportService.setModelInStorage($scope.Model);
            customReportService.goOrderDetail($scope.MC.stateParams.seasonDomain, orderId);
        }

        $scope.filterData = function () {
            customReportService.setModelInStorage($scope.Model);
            $scope.seasonTransactionReportGridOptions.dataSource.read();
        }

        $scope.clearFilters = function () {
            $scope.Model.Filters.RegistrationStart = "";
            $scope.Model.Filters.RegistrationEnd = "";
            $scope.Model.Filters.TransactionFilters.TransactionCategory = [];
            $scope.Model.Filters.TransactionFilters.PaymentMethod = {};
            $scope.searchTerm = "";
            $scope.seasonTransactionReportGridOptions.dataSource.read();
        }

        $scope.shareReport = function () {
            $scope.Model.Filters.Term = $scope.searchTerm;
            customReportService.setModelInStorage($scope.Model);
            $state.go('ShareAsAttchment', { seasonDomain: $scope.MC.stateParams.seasonDomain, reportName: "Finance" });
        }
    }]);
})();