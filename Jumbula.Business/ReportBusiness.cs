﻿using Autofac.Features.Indexed;
using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Core.Model.Report;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace Jumbula.Business
{
    public class ReportBusiness : INewReportBusiness
    {
        #region Fields
        private readonly IClubBusiness _clubBusiness;
        private readonly ISeasonBusiness _seasonBusiness;
        private readonly IProgramBusiness _programBusiness;
        private readonly IOrderSessionBusiness _orderSessionBusiness;

        readonly IIndex<ReportName, IReportBuilderBusiness> _report;
        private IReportBuilderBusiness _reportBuilder;
        #endregion

        #region Constractors
        public ReportBusiness(IClubBusiness clubBusiness, ISeasonBusiness seasonBusiness, IProgramBusiness programBusiness, IOrderSessionBusiness orderSessionBusiness, IIndex<ReportName, IReportBuilderBusiness> report)
        {
            _clubBusiness = clubBusiness;
            _seasonBusiness = seasonBusiness;
            _programBusiness = programBusiness;
            _orderSessionBusiness = orderSessionBusiness;
            _report = report;
        }
        #endregion

        #region Private methods
        private IReportBuilderBusiness GetReportBuilder(ReportName reportName)
        {
            _reportBuilder = _report[reportName];

            return _reportBuilder;
        }
        #endregion

        #region Public methods
        public ReportDataModel BindData(ReportName reportName, ClubBaseInfoModel clubBaseInfo, Dictionary<string, object> parameters, PaginationModel paginationModel, List<ReportGridSort> sort = null)
        {
            var reportBuilder = GetReportBuilder(reportName);

            if (reportBuilder == null) return new ReportDataModel(null, null);

            var data = reportBuilder.BindData(parameters, clubBaseInfo, paginationModel, sort);

            return data;

        }

        public ReportModel GetSchema(ReportName reportName, ClubBaseInfoModel clubBaseInfo, Dictionary<string, object> parameters)
        {
            var reportBuilder = GetReportBuilder(reportName);

            if (reportBuilder == null) return new ReportModel(string.Empty, null, null, new ReportGridModel(), null, null);

            var schema = reportBuilder.GetSchema(parameters, clubBaseInfo);

            return schema;
        }

        public ReportExportModel GetPdfModel(ReportName reportName, ClubBaseInfoModel clubBaseInfo, Dictionary<string, object> parameters, PaginationModel paginationModel)
        {
            var reportBuilder = GetReportBuilder(reportName);

            var schema = reportBuilder.GetSchema(parameters, clubBaseInfo);

            paginationModel.Page = 1;
            paginationModel.PageSize = int.MaxValue;

            var data = reportBuilder.BindData(parameters, clubBaseInfo, paginationModel);


            var rows = new List<List<string>>();

            const BindingFlags bindingFlags = BindingFlags.Instance |
                                              BindingFlags.NonPublic |
                                              BindingFlags.Public;

            foreach (var item in data.Data.DataSource)
            {
                var fields = item.GetType().GetFields(bindingFlags);

                var row = (from field in fields select field.GetValue(item) != null ? field.GetValue(item).ToString() : string.Empty).ToList();

                rows.Add(row);
            }

            var pdfModel = new ReportExportModel
            {
                Rows = rows,
                Columns = data.Columns.Select(c => c.Title).ToList(),
                PdfViewName = schema.Exports.Single(e => e.Name == ReportExportType.Pdf.ToString()).Template
            };

            return pdfModel;
        }

        public byte[] GetExcel(ReportName reportName, ClubBaseInfoModel clubBaseInfo, Dictionary<string, object> parameters, PaginationModel paginationModel, ControllerContext controllerContext)
        {
            var reportBuilder = GetReportBuilder(reportName);


            paginationModel.Page = 1;
            paginationModel.PageSize = int.MaxValue;

            var data = reportBuilder.BindData(parameters, clubBaseInfo, paginationModel);


            var rows = new List<List<string>>();

            var bindingFlags = BindingFlags.Instance |
                               BindingFlags.NonPublic |
                               BindingFlags.Public;

            foreach (var item in data.Data.DataSource)
            {
                var fields = item.GetType().GetFields(bindingFlags);

                var row = (from field in fields select field.GetValue(item) != null ? field.GetValue(item).ToString() : string.Empty).ToList();
                rows.Add(row);
            }

            var excelModel = new ReportExportModel
            {
                Rows = rows,
                Columns = data.Columns.Select(c => c.Title).ToList(),
                ReportName = reportName.ToDescription(),
                ClubName = clubBaseInfo.Name,
                Date = _clubBusiness.GetClubDateTime(clubBaseInfo.Id, DateTime.UtcNow).ToString(Constants.DateTime_Comma)
            };

            var outputData = CreateExcelExport(excelModel);

            return outputData;
        }

        public Dictionary<string, object> GetFilter(ClubBaseInfoModel clubBaseInfo, Dictionary<string, object> parameters)
        {
            var filterName = GetParameter(parameters, ReportConstants.FilterName).ToString();

            var filter = (ReportFilterElementName)Enum.Parse(typeof(ReportFilterElementName), filterName);

            var data = new Dictionary<string, object>();

            switch (filter)
            {
                case ReportFilterElementName.Program:
                    {
                        var programs = new List<SelectListItem>();

                        if (GetParameter(parameters, ReportConstants.SeasonIdParam) != null)
                        {
                            var seasonId = long.Parse(GetParameter(parameters, ReportConstants.SeasonIdParam).ToString());
                            programs = GetSeasonPrograms(seasonId, clubBaseInfo.Id, null);
                        }
                        else if (GetParameter(parameters, ReportConstants.SeasonDomainParam) != null)
                        {
                            var seasonDomain = GetParameter(parameters, ReportConstants.SeasonDomainParam).ToString();
                            programs = GetSeasonPrograms(seasonDomain, clubBaseInfo.Id);
                        }

                        data.Add(ReportFilterElementName.Program.ToString(), new { Data = programs });
                    }
                    break;
                case ReportFilterElementName.Session:
                    {
                        var programId = long.Parse(GetParameter(parameters, ReportConstants.ProgramIdParam).ToString());

                        var programSessions = GetProgramSessions(programId);

                        data.Add(ReportFilterElementName.Session.ToString(), new { Data = programSessions });
                    }
                    break;
                case ReportFilterElementName.School:
                    {
                        var partnerId = clubBaseInfo.Id;

                        var partnerSchools = GetPartnerSchools(partnerId);

                        data.Add(ReportFilterElementName.School.ToString(), new { Data = partnerSchools });
                    }
                    break;
                case ReportFilterElementName.Season:
                    {
                        var clubId = GetParameter(parameters, ReportConstants.ClubIdParam) != null ? int.Parse(GetParameter(parameters, ReportConstants.ClubIdParam).ToString()) : GetParameter(parameters, ReportConstants.SchoolIdParam) != null ? int.Parse(GetParameter(parameters, ReportConstants.SchoolIdParam).ToString()) : 0;

                        var schoolSeasons = GetSchoolSeasons(clubId);

                        data.Add(ReportFilterElementName.Season.ToString(), new { Data = schoolSeasons });
                    }
                    break;
                case ReportFilterElementName.CampProgram:
                    {
                        var campPrograms = new List<SelectListItem>();

                        if (GetParameter(parameters, ReportConstants.SeasonIdParam) != null)
                        {
                            var seasonId = long.Parse(GetParameter(parameters, ReportConstants.SeasonIdParam).ToString());
                            campPrograms = GetSeasonPrograms(seasonId, clubBaseInfo.Id, ProgramTypeCategory.Camp);
                        }
                        else if (GetParameter(parameters, ReportConstants.SeasonDomainParam) != null)
                        {
                            var seasonDomain = GetParameter(parameters, ReportConstants.SeasonDomainParam).ToString();
                            campPrograms = GetSeasonPrograms(seasonDomain, clubBaseInfo.Id, ProgramTypeCategory.Camp);
                        }

                        data.Add(ReportFilterElementName.CampProgram.ToString(), new { Data = campPrograms });
                    }
                    break;
                case ReportFilterElementName.Schedule:
                    {
                        var programId = int.Parse(GetParameter(parameters, ReportConstants.ProgramIdParam).ToString());

                        var programSchedules = GetProgramSchedules(programId);

                        data.Add(ReportFilterElementName.Schedule.ToString(), new { Data = programSchedules });
                    }
                    break;
                case ReportFilterElementName.Organization:
                    {
                        var organizations = GetOrganizationList();

                        data.Add(ReportFilterElementName.Organization.ToString(), new { Data = organizations });
                    }
                    break;
                case ReportFilterElementName.ActiveStatus:
                    {
                        var activeStatus = GetActiveStatus();

                        data.Add(ReportFilterElementName.ActiveStatus.ToString(), new { Data = activeStatus });
                    }
                    break;
                case ReportFilterElementName.ProviderAndSchool:
                    {
                        int partnerId = int.Parse(GetParameter(parameters, ReportConstants.PartnerIdParam).ToString());

                        var result = GetSchoolsAndProviders(partnerId);

                        data.Add(ReportFilterElementName.ProviderAndSchool.ToString(), new { Data = result });
                    }
                    break;
            }

            return data;
        }

        public Dictionary<string, object> GetReportHeaders(Dictionary<string, object> parameters, List<ReportHeader> reportHeaders)
        {
            var result = new Dictionary<string, object>();

            foreach (var item in reportHeaders)
            {
                switch (item)
                {
                    case ReportHeader.Date:
                        {
                            var clubId = int.Parse(GetParameter(parameters, ReportConstants.ClubIdParam).ToString());

                            var date = _clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow);

                            result.Add(ReportHeader.Date.ToString(), $"{date.ToString(Constants.DateTime_Comma)} {date.ToString(Constants.DefaultTimeFormat)}");
                        }
                        break;
                    case ReportHeader.SeasonName:
                        {
                            var seasonId = GetParameter(parameters, ReportConstants.SeasonIdParam) != null ? int.Parse(GetParameter(parameters, ReportConstants.SeasonIdParam).ToString()) : 0;
                            var clubId = int.Parse(GetParameter(parameters, ReportConstants.ClubIdParam).ToString());
                            var seasonDomain = GetParameter(parameters, ReportConstants.SeasonDomainParam).ToString();

                            if (seasonId != 0)
                            {
                                var season = _seasonBusiness.Get(seasonId);
                                result.Add(ReportHeader.SeasonName.ToString(), season.Title);
                            }
                            else
                            {
                                var season = _seasonBusiness.GetList().SingleOrDefault(s => s.Domain == seasonDomain && s.ClubId == clubId);

                                if (season != null) result.Add(ReportHeader.SeasonName.ToString(), season.Title);
                            }
                        }
                        break;
                    case ReportHeader.Room:
                        {
                            if (GetParameter(parameters, ReportConstants.ProgramIdParam) != null)
                            {
                                result.Add(ReportHeader.Room.ToString(), string.Empty);
                                break;
                            }

                            var programId = long.Parse(GetParameter(parameters, ReportConstants.ProgramIdParam).ToString());
                            var program = _programBusiness.Get(programId);

                            var room = program.Room ?? string.Empty;

                            result.Add(ReportHeader.Room.ToString(), room);
                        }
                        break;
                    case ReportHeader.ClassDays:
                        {
                            if (GetParameter(parameters, ReportConstants.ProgramIdParam) != null)
                            {
                                result.Add(ReportHeader.ClassDays.ToString(), string.Empty);
                                break;
                            }

                            var programId = long.Parse(GetParameter(parameters, ReportConstants.ProgramIdParam).ToString());
                            var program = _programBusiness.Get(programId);

                            var classDays = string.Join(", ", _programBusiness.GetClassDays(program).Select(d => d.DayOfWeek).ToList());

                            result.Add(ReportHeader.ClassDays.ToString(), classDays);
                        }
                        break;
                }
            }
            return result;
        }


        public object GetParameter(Dictionary<string, object> parameters, string parameterName)
        {
            parameters.TryGetValue(parameterName, out var value);

            return value;
        }

        public byte[] CreateExcelExport(ReportExportModel model)
        {
            var columns = model.Columns;
            var data = model.Rows;

            var workbook = new XSSFWorkbook();

            var sheet = workbook.CreateSheet();

            int rowNumber = 0;

            //Create a header titles
            var header = sheet.CreateRow(rowNumber);
            header.CreateCell(0).SetCellValue($"{model.ReportName} - {model.ClubName}");
            sheet.AutoSizeColumn(0);

            rowNumber += 1;
            header = sheet.CreateRow(rowNumber);
            header.CreateCell(0).SetCellValue(model.Date);
            sheet.AutoSizeColumn(0);

            //Create a header columns
            var boldFont = workbook.CreateFont();
            boldFont.Boldweight = (short)FontBoldWeight.Bold;

            var greyBoldStyle = workbook.CreateCellStyle();
            greyBoldStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyBoldStyle.FillPattern = FillPattern.SolidForeground;
            greyBoldStyle.SetFont(boldFont);
            greyBoldStyle.Alignment = HorizontalAlignment.Left;
            greyBoldStyle.VerticalAlignment = VerticalAlignment.Center;

            rowNumber += 2;
            var columnsHeader = sheet.CreateRow(rowNumber);
            for (int i = 0; i < columns.Count; i++)
            {
                var columnName = columns.ElementAt(i);
                columnsHeader.CreateCell(i).SetCellValue(columnName);
                columnsHeader.GetCell(i).CellStyle = greyBoldStyle;
                sheet.AutoSizeColumn(i);
            }

            //(Optional) freeze the header row so it is not scrolled
            //sheet.CreateFreezePane(0, 1, 0, 1);

            rowNumber += 1;

            foreach (var rowData in data)
            {
                //Create a new row
                var row = sheet.CreateRow(rowNumber++);

                for (int i = 0; i < rowData.Count; i++)
                {
                    var columnData = rowData[i];

                    //Set values for the cells
                    row.CreateCell(i).SetCellValue(columnData);

                    // Auto size columns
                    //sheet.AutoSizeColumn(i);

                    //Set locked column
                    //var lockedCell = workbook.CreateCellStyle();
                    //lockedCell.IsLocked = true;
                    //row.GetCell(i).CellStyle = lockedCell;


                }
            }

            //Write the Workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            return output.ToArray();
        }

        public List<SelectListItem> GetSeasonPrograms(long seasonId, int clubId, ProgramTypeCategory? programTypeCategory)
        {
            var club = _clubBusiness.Get(clubId);

            var season = _seasonBusiness.Get(seasonId);

            List<Program> programs;
            if (programTypeCategory.HasValue)
            {
                programs = _programBusiness
                    .GetListBySeasonId(season.Id)
                    .Where(p => p.Status != ProgramStatus.Deleted && p.LastCreatedPage >= ProgramPageStep.Step4 && p.TypeCategory == programTypeCategory)
                    .OrderBy(p => p.Name)
                    .ToList();
            }
            else
            {
                programs = _programBusiness
                       .GetListBySeasonId(season.Id)
                       .Where(p => p.Status != ProgramStatus.Deleted && p.LastCreatedPage >= ProgramPageStep.Step4)
                       .OrderBy(p => p.Name)
                       .ToList();
            }

            var allPrograms = programs.Select(p => new SelectListItem
            {
                Text = (p.OutSourceSeasonId.HasValue && club.IsProvider) ? p.Name + " (" + p.Club.Name + ")" :
                        (p.OutSourceSeasonId.HasValue && club.IsSchool) ? p.Name + " (" + p.OutSourceSeason.Club.Name + ")" : p.Name,

                Value = p.Id.ToString()
            })
                .OrderBy(s => s.Text).ToList();

            return allPrograms;
        }

        public List<SelectListItem> GetSeasonPrograms(string seasonDomain, int clubId, ProgramTypeCategory programTypeCategory = ProgramTypeCategory.Class)
        {
            var club = _clubBusiness.Get(clubId);

            var season = _seasonBusiness.Get(seasonDomain, clubId);

            var programs = _programBusiness
                .GetListBySeasonId(season.Id)
                .Where(p => p.Status != ProgramStatus.Deleted && p.LastCreatedPage >= ProgramPageStep.Step4 && p.TypeCategory == programTypeCategory)
                .OrderBy(p => p.Name)
                .ToList();

            var allPrograms = programs.Select(p => new SelectListItem
            {
                Text = (p.OutSourceSeasonId.HasValue && club.IsProvider) ? p.Name + " (" + p.Club.Name + ")" :
                        (p.OutSourceSeasonId.HasValue && club.IsSchool) ? p.Name + " (" + p.OutSourceSeason.Club.Name + ")" : p.Name,

                Value = p.Id.ToString()
            })
                .OrderBy(s => s.Text).ToList();

            return allPrograms;
        }

        public List<SelectListItem> GetProgramSessions(long programId)
        {
            var program = _programBusiness.Get(programId);

            var clubDateTime = _clubBusiness.GetClubDateTime(program.Club.Id, DateTime.UtcNow);

            var programSesssions = _programBusiness.GetSessions(program)
                .Where(s => s.End.Date <= clubDateTime.Date)
                .OrderBy(o => o.Start)
                .Select(s => new SelectListItem()
                {
                    Text = s.Start.ToString("ddd, MMM dd, yyyy"),
                    Value = s.Id.ToString(),
                })
                .ToList();

            return programSesssions;
        }

        public List<SelectKeyValue<long>> GetSeasonProgramsTextValue(int clubId, string seasonDomain = "", List<ProgramTypeCategory> typeCategories = null)
        {
            var season = _seasonBusiness.Get(seasonDomain, clubId);
            var programs = _programBusiness.GetListBySeasonId(season.Id)
                .Where(p => p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Status != ProgramStatus.Deleted)
                .SelectMany(p => p.ProgramSchedules.Where(s => s.ScheduleMode != TimeOfClassFormation.Both && !s.IsDeleted))
                .ToList();

            if (typeCategories != null)
            {
                programs = programs.Where(p => typeCategories.Contains(p.Program.TypeCategory)).ToList();
            }

            var allPrograms = programs.Select(p => new SelectKeyValue<long>()
            {
                Text = !string.IsNullOrEmpty(p.Title)
                     ? $"{p.Program.Name} - {p.Title}"
                     : p.Program.Name,
                Value = p.Id
            })
             .OrderBy(s => s.Text).ToList();

            return allPrograms;
        }

        public List<SelectListItem> GetPartnerSchools(int partnerId)
        {
            var allSchools = _clubBusiness
                .GetAllPartnerSchools(partnerId)
                .Select(c => new SelectListItem { Text = c.Name, Value = c.Id.ToString() })
                .OrderBy(c => c.Text).ToList();

            return allSchools;
        }

        public List<SelectListItem> GetSchoolSeasons(int clubId)
        {
            if (clubId == 0)
            {
                return new List<SelectListItem>();
            }

            var allSeasons = _seasonBusiness.GetList(clubId);

            var model = new List<SelectListItem>();

            model.AddRange(allSeasons.OrderByDescending(c => c.MetaData.DateCreated).ToList().Select(c => new SelectListItem()
            {
                Text = c.Title,
                Value = c.Id.ToString()
            }).ToList());

            return model;
        }

        public List<SelectListItem> GetProgramSchedules(long programId)
        {
            var program = _programBusiness.Get(programId);

            var model = program.ProgramSchedules.Where(c => c.IsDeleted == false).Select(c => new SelectListItem()
            {
                Text = !string.IsNullOrEmpty(c.Title) ? c.Title : $"{c.StartDate.ToString(Constants.DateTime_Comma)} - {c.EndDate.ToString(Constants.DateTime_Comma)}",
                Value = c.Id.ToString()
            }).ToList();

            return model;
        }

        public List<SelectListItem> GetOrganizationList()
        {
            var model = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = "All",
                    Value = "-1"
                }
            };

            model.AddRange(_clubBusiness.GetClubTypeList()
                .Where(c => c.ParentId.HasValue)
                .GroupBy(c => new { id = c.ParentId.Value, name = c.ParentType.Name })
                .Select(v => new SelectListItem() { Text = v.Key.name, Value = v.Key.id.ToString() })
                .ToList());

            return model;
        }

        public List<SelectListItem> GetActiveStatus()
        {
            var model = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = ReportConstants.All,
                    Value = "-1"
                },
                new SelectListItem
                {
                    Text = ReportConstants.Active,
                    Value = "0"
                },
                new SelectListItem
                {
                    Text = ReportConstants.Inactive,
                    Value = "1"
                }
            };

            return model;
        }

        public List<SelectListItem> GetIdentifierItems()
        {
            var model = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = ReportConstants.Participant_Name,
                    Value = ReportConstants.Name
                },
                new SelectListItem
                {
                    Text = ReportConstants.Family_Email,
                    Value = ReportConstants.Email
                },
                new SelectListItem
                {
                    Text = ReportConstants.Confirmation_Id,
                    Value = ReportConstants.ConfirmationId
                },
                new SelectListItem
                {
                    Text = ReportConstants.Error_Message,
                    Value = ReportConstants.ErrorMessage
                }
            };

            return model;
        }

        public List<SelectListItem> GetInstallmentPaymentModeItems()
        {
            var model = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = ReportConstants.All,
                    Value = null
                },
                new SelectListItem
                {
                    Text = ReportConstants.Auto_Charge,
                    Value = ReportConstants.AutoChargeField
                },
                new SelectListItem
                {
                    Text = ReportConstants.Auto_Charge_Stopped,
                    Value = ReportConstants.AutoChargeStoppedField
                },
                new SelectListItem
                {
                    Text = ReportConstants.Auto_Charge_Not_Support,
                    Value = ReportConstants.AutoChargeStartedField
                },
                new SelectListItem
                {
                    Text = ReportConstants.Manual,
                    Value = ReportConstants.Manually
                }
            };

            return model;
        }

        public List<SelectListItem> GetSchoolsAndProviders(int partnerId)
        {
            var result = new List<SelectListItem>();
            var schoolsAndProviders = _clubBusiness.GetRelatedClubs(partnerId, false);

            result.AddItemToFirst(DefaultDropdownItem.All);

            result.AddRange(schoolsAndProviders.Select(p => new SelectListItem() { Text = p.Name, Value = p.Id.ToString() })
                .OrderBy(s => s.Text).ToList());

            return result;
        }

        public string GetDayOfWeekWithStartTime(List<ProgramScheduleDay> days, AbbreviatedDayNameMode abbreviatedDayName = AbbreviatedDayNameMode.TwoLetter)
        {
            var daysFormated = new List<string>();
            foreach (var day in days)
            {
                var dayOfWeek = DateTimeHelper.GetDayOfWeekAbbreviation(day.DayOfWeek, abbreviatedDayName);
                var startTime = day.StartTime.HasValue ? DateTime.Today.AddHours(day.StartTime.Value.Hours).AddMinutes(day.StartTime.Value.Minutes).AddSeconds(day.StartTime.Value.Seconds).ToString(Constants.DefaultTimeFormat) : string.Empty;

                daysFormated.Add($"{dayOfWeek}: {startTime}");
            }

            return string.Join(" ", daysFormated);
        }

        public string GetDayOfWeekWithEndTime(List<ProgramScheduleDay> days, AbbreviatedDayNameMode abbreviatedDayName = AbbreviatedDayNameMode.TwoLetter)
        {
            var daysFormated = new List<string>();
            foreach (var day in days)
            {
                var dayOfWeek = DateTimeHelper.GetDayOfWeekAbbreviation(day.DayOfWeek, abbreviatedDayName);
                var endTime = day.EndTime.HasValue ? DateTime.Today.AddHours(day.EndTime.Value.Hours).AddMinutes(day.EndTime.Value.Minutes).AddSeconds(day.EndTime.Value.Seconds).ToString(Constants.DefaultTimeFormat) : string.Empty;

                daysFormated.Add($"{dayOfWeek}: {endTime}");
            }

            return string.Join(" ", daysFormated);
        }

        public List<OrderItem> GetReportOrderItemsWithEffectiveDate(long programScheduleId, DateTime dateTime)
        {
            var orderSessions = _orderSessionBusiness.GetListByDate(programScheduleId, dateTime);

            var orderItems = orderSessions.Select(o => o.OrderItem).ToList()
                .Where(o => o.ItemStatus == OrderItemStatusCategories.completed || (o.ItemStatus == OrderItemStatusCategories.changed && o.Attributes.CancelEffectiveDate > dateTime) || (o.ItemStatus == OrderItemStatusCategories.changed && o.Attributes.TransferEffectiveDate > dateTime))
                .ToList();

            return orderItems;
        }

        public List<OrderItem> GetReportOrderItemsWithEffectiveDate(long programScheduleId)
        {
            var orderSessions = _orderSessionBusiness.GetListBySchedule(programScheduleId);

            var orderItems = orderSessions.Select(o => o.OrderItem).ToList()
                .Where(o => o.ItemStatus == OrderItemStatusCategories.completed || (o.ItemStatus == OrderItemStatusCategories.changed && o.Attributes.CancelEffectiveDate > DateTime.UtcNow) || (o.ItemStatus == OrderItemStatusCategories.changed && o.Attributes.TransferEffectiveDate < DateTime.UtcNow))
                .ToList();

            return orderItems;
        }
        #endregion

    }
}
