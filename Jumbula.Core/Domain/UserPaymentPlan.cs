﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Core.Domain
{

    public class UserPaymentPlan
    {
        [Key, Column(Order = 2)]
        [ForeignKey("User")]
        public int UserId { get; set; }


        [Key, Column(Order = 1)]
        [ForeignKey("PaymentPlan")]
        public long PaymentPlanId { get; set; }

        public virtual PaymentPlan PaymentPlan { get; set; }

        public virtual JbUser User { get; set; }
    }
}
