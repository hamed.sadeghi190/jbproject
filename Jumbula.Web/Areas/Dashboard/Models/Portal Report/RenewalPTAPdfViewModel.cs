﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class RenewalPTAPdfViewModel
    {
        public RenewalPTAPdfViewModel()
        {
            //Schools = new List<Models.SchoolsEnrollmentReportViewModel>();
        }
        public string ClubLogo { get; set; }

        public List<SchoolRenewalReportViewModel> Schools { get; set; }
    }
}