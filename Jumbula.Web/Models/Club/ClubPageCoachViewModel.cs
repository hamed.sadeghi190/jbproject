﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class ClubPageCoachViewModel
    {
        public string Domain { get; set; }
        public string ClubDomain { get; set; }
        public int CategoryId { get; set; }
        public int Id { get; set; }
        public virtual ContactPerson Contact { get; set; }
        public string Biography { get; set; }
        public string ImageUrl { get; set; }
        public bool Default { get; set; }
    }
}