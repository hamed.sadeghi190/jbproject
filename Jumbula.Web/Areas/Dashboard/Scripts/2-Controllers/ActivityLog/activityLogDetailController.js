﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('activityLogDetailController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', 'customReportService', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService, customReportService) {
        window.app.globalObjects.initializeView();
        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value) {
            if (value.Domain === $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        $http.get(window.location.origin + "/Dashboard/ActivityLog/GetDetail?id="+ $stateParams.id).success(function (data) {
            $scope.Model = data;

        }).error(function (data) {
            $scope.MC.handleErrors($scope, data);
        });


    }]);
})();

