﻿

namespace Jumbula.Common.Enums
{
    public enum EventPageStep
    {
        BasicInformation,
        RegSettings,
        ChargeDiscount,
        AdditionalInfo,
        Lastpage
    }
}
