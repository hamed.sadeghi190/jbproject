﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('AdvanceRosterReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'cfpLoadingBar', 'jbToast', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, cfpLoadingBar, jbToast, $timeout) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value) {
            if (value.Domain === $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.programs = [];
        $scope.Model = [];
        $scope.IsFirstRun = false;
        var columns = [];

        $scope.Model = {};
        
        $scope.Model.SeasonDomain = $scope.stateParams.seasonDomain;

        $http.get(baseUrl + '/GetAllSeasonPrograms', { params: { seasonDomain: $scope.MC.stateParams.seasonDomain, reportName: "" } })
            .success(function (data) {
                
                $scope.Model.Programs = data.AllPrograms;
                $scope.Model.IsAllPrograms = data.IsAllPrograms;
                $scope.Model._SelectedPrograms = [];

                var timer = false;

                $scope.updateChosenScope = function () {
                    $("select[chosen]").trigger('chosen:updated');
                };

                $scope.$watch("Model.Programs", function () {
                    if (timer) {
                        $timeout.cancel(timer);
                    }
                    timer = $timeout(function () {
                        $scope.updateChosenScope();
                    }, 1500);
                });

            });

        $scope.updateChosenScope = function () {
            $("select[chosen]").trigger('chosen:updated');
        };

        $http.get(baseUrl + '/GetAdvanceClassRosterReportDetail', { params: { seasonDomain: $stateParams.seasonDomain } }).success(function (data) {
            
            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
            $scope.Model.ReportId = data.ReportId;

        }).error(function (data) {
            jbToast.error("error " + data);
        });


        var manipulateRosterGrid = function () {

            $scope.customReportsDataSource = new kendo.data.DataSource({

                dataType: "json",
                transport:
                {
                    read: {
                        url: baseUrl + "/RunReport",
                        type: "POST",
                        data: function () {
                            return {
                                model: $scope.Model,
                            };
                        }
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: function (response) {

                        $scope.Titles = response.Titles;
                        return response.DataSource;
                    },
                },
                serverPaging: true,
                pageSize: 50,
                batch: true,
                requestStart: function () {
                    kendo.ui.progress($("[kendo-grid]"), true);
                },
                requestEnd: function () {
                    kendo.ui.progress($("[kendo-grid]"), false);
                }
            });


            $scope.oldColumns = [];

            $scope.RosterReportGridOptions = {
                excel: {
                    allPages: true,
                    fileName: "Class roster - advanced.xlsx",
                    proxyURL: baseUrl + "/KendoExport"
                },
                excelExport: function (e) {
                    cfpLoadingBar.complete();
                },
                dataSource: $scope.customReportsDataSource,
                scrollable: true,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                        [
                            5, 10, 20, 30, 50, 100
                        ],
                    buttonCount: 5
                },
                dataBound: function (e) {
                    customReportService.initializeDoubleScroll(e);
                    $scope.oldColumns = $("#RosterReportGrid").data("kendoGrid").columns;
                }
            }

            bindingData();
        }

        $scope.RunReport = function () {

            if (!$scope.IsFirstRun) {
                manipulateRosterGrid();
                $scope.IsFirstRun = true;
            } else {

                $("#RosterReportGrid").data("kendoGrid").setOptions({
                    columns: []
                });

                $scope.RosterReportGridOptions.dataSource.page(1);
                $("#RosterReportGrid").data("kendoGrid").dataSource.read();

                bindingData();
            }

        }

        function bindingData() {
            $(document).bind("ajaxComplete.changeColumns", function () {
                if ($scope.oldColumns && $scope.oldColumns.length > 0) {
                    for (var i = 0; i < $scope.oldColumns.length; i++) {
                        var column = [];
                        column = {
                            field: $scope.oldColumns[i].field,
                            title: $scope.Titles[i].replace(/_/g, " ").trimLeft().replace(/sDash/g, " - ").trimLeft()
                        }
                        columns.push(column);
                    }
                    $("#RosterReportGrid").data("kendoGrid").setOptions({
                        columns: columns
                    });
                }
                $(document).unbind("ajaxComplete.changeColumns");
            });
        }


        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#RosterReportGrid").getKendoGrid().saveAsExcel();
        }

        $scope.EditReport = function () {

            $state.go("RosterReportEdit", { reportId: $scope.Model.ReportId, seasonDomain: $stateParams.seasonDomain });

        }

    }]);
})();