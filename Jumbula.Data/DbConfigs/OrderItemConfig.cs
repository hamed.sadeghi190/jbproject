﻿using System.Data.Entity.ModelConfiguration;
using Jumbula.Core.Domain;

namespace Jumbula.Data.DbConfigs
{
    public class OrderItemConfig : EntityTypeConfiguration<OrderItem>
    {
        public OrderItemConfig()
        {
            HasRequired(i => i.Order).WithMany(c => c.OrderItems).HasForeignKey(i => i.Order_Id);
            HasRequired(i => i.Season).WithMany(s => s.OrderItems).HasForeignKey(i => i.SeasonId).WillCascadeOnDelete(false);
            HasMany(r => r.OrderChargeDiscounts).WithOptional(charge => charge.OrderItem).HasForeignKey(charge => charge.OrderItemId).WillCascadeOnDelete(true);
            HasOptional(i => i.ProgramSchedule).WithMany(p => p.OrderItems).HasForeignKey(i => i.ProgramScheduleId);
            HasOptional(ev => ev.JbForm).WithMany().HasForeignKey(ev => ev.JbFormId);
            HasMany(i => i.OrderSessions).WithRequired(s => s.OrderItem).HasForeignKey(s => s.OrderItemId);
            HasMany(o => o.TransactionActivities).WithOptional(c => c.OrderItem).HasForeignKey(c => c.OrderItemId);
            HasMany(o => o.Installments).WithRequired(i => i.OrderItem).HasForeignKey(i => i.OrderItemId);
            HasOptional(o => o.OrderItemChess);
        }
    }
}
