﻿namespace Jumbula.Common.Constants
{
    public static class ReportConstants
    {
        public const string AutoChargeStoppedWithDateFormat = "Auto charge stopped ({0})";
        public const string AutoChargeStoppedFormat = "Auto charge stopped";
        public const string AutoCharge = "Auto charge";
        public const string Manual = "Manual";

        public const string AutoChargeStoppedMode = "AutoChargeStopped";
        public const string AutoChargeMode = "AutoCharge";
        public const string AutoChargeStartMode = "AutoChargeStarted";
        public const string ManualMode = "Manually";

        public const string Unpaid = "Unpaid";
        public const string FullyPaid = "FullyPaid";
        public const string PartiallyPaid = "PartiallyPaid";
        public const string Suspended = "Suspended";
        public const string Id = "Id";
        public const string Asc = "asc";
        public const string OrderBy = "OrderBy";
        public const string OrderByDescending = "OrderByDescending";
        public const string ThenBy = "ThenBy";
        public const string PlayerProfile = "PlayerProfile";
        public const string Contact = "Contact";
        public const string LastName = "LastName";
        public const string FirstName = "FirstName";
        public const string Organization = "Organization";
        public const string FilterName = "filterName";
        public const string Mode = "Mode";
        public const string Pdf = "Pdf";
        public const string Excel = "Excel";
        public const string EditorTemplates = "EditorTemplates";
        public const string JbReportFilter = "JbReportFilter";
        public const string All = "All";
        public const string Active = "Active";
        public const string Inactive = "Inactive";
        public const string Participant_Name = "Participant name";
        public const string Name = "Name";
        public const string Family_Email = "Family email";
        public const string Email = "Email";
        public const string Confirmation_Id = "Confirmation ID";
        public const string ConfirmationId = "ConfirmationId";
        public const string Error_Message = "Error message";
        public const string ErrorMessage = "ErrorMessage";
        public const string Auto_Charge = "Auto charge";
        public const string Auto_Charge_Stopped = "Auto charge (stopped)";
        public const string Auto_Charge_Not_Support = "Auto charge (not stopped)";
        public const string Manually = "Manually";

        public const string OneFourth = "25%";
        public const string Bold = "bold";

        public const string ReportBuilderSuffix = "ReportBuilder";

        public const string ChargesElement = "Charges";
        public const string OrderItem = "OrderItem";
        public const string StudentName = "StudentName";
        public const string Dismissed = "Dismissed";
        public const string DismissalAfterEnrichment = "Dismissal After Enrichment";
        public const string AfternoonDismissalFromEnrichment = "Afternoon Dismissal From Enrichment";
        public const string DismissalFromEnrichment = "Dismissal From Enrichment";
        public const string MorningProgramArrival = "Morning program arrival";

        #region Field
        public const string IsPickedUpField = "IsPickedUp";
        public const string StudentNameField = "StudentName";
        public const string DismissalFromEnrichmentField = "DismissalFromEnrichment";
        public const string PickupTimeField = "PickupTime";
        public const string PickupNameField = "PickupName";
        public const string NameField = "Name";
        public const string ContactNameField = "ContactName";
        public const string EmailAddressField = "EmailAddress";
        public const string PhoneNumberField = "PhoneNumber";
        public const string AutoChargeField = "AutoCharge";
        public const string AutoChargeStoppedField = "AutoChargeStopped";
        public const string AutoChargeStartedField = "AutoChargeStarted";
        #endregion

        #region Title
        public const string StatusTitle = "Status";
        public const string ParticipantTitle = "Participant";
        public const string DismissalTitle = "Dismissal";
        public const string DismissalTimeTitle = "Dismissal time";
        public const string PickedUpByTitle = "Picked up by";
        public const string NameTitle = "Name";
        public const string ContactNameTitle = "Contact name";
        public const string EmailTitle = "Email";
        public const string PhoneTitle = "Phone";
        #endregion

        #region Params
        public const string SeasonDomainParam = "seasonDomain";
        public const string SeasonIdParam = "seasonId";
        public const string ProgramIdParam = "programId";
        public const string PartnerIdParam = "partnerId";
        public const string ClubIdParam = "clubId";
        public const string SessionIdParam = "sessionId";
        public const string OrderItemIdParam = "orderItemId";
        public const string ScheduleIdParam = "scheduleId";
        public const string EntryFeeIdParam = "entryFeeId";
        public const string OrganizationIdParam = "organizationId";
        public const string ActiveStatusParam = "activeStatus";
        public const string StartDateParam = "startDate";
        public const string EndDateParam = "endDate";
        public const string SchoolIdParam = "schoolId";
        #endregion
    }
}
