﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('eSignatureController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbConfirmation, jbToast) {
        window.app.globalObjects.pageClass = 'ESignature_View';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/ESignature", order: 0, menu: "ESIGNATURE" });
        $scope.baseUrl = window.location.origin + '/Dashboard/ESignature';

        $http.get($scope.baseUrl + '/ESignatureMainPage').success(function (data, status, headers, config) {
            $scope.Model = data;
            $scope.filter.ESignaturType = 'All';

        }).error(function (data, status, headers, config) {
            jbToast.error(data);
        });


        $scope.searchTerm = "";
        $scope.searchCompany = "";
        $scope.filter = {};
        $scope.filter.ESignaturType = "0";
        $scope.filter.typeId = "-1";

        $scope.mainGridDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: $scope.baseUrl + '/GetAllDocumentSign/',
                    contentType: "application/json; charset=utf-8",
                    type: "POST"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        data = $.extend(
                           {
                               sort: null,
                               filter: null,
                               term: $scope.searchTerm,
                               companyTerm: $scope.searchCompany,
                               ESignaturType: $scope.filter.ESignaturType,
                               typeId: $scope.filter.typeId,
                           }, data);
                        return JSON.stringify(data);
                    }
                },

            },
            pageSize: 50,
            batch: true,
            serverPaging: true,
            serverSorting: false,

            schema: {
                data: "data",
                total: "total"
            },
            sortable: false,
        });
        $scope.mainGridOptions = {
            dataSource: $scope.mainGridDataSource,
            sortable: true,
            serverPaging: true,
            scrollable: true,
            columnMenu: true,
            columns: [
            {
                field: "StrCreateDocument",
                title: "Date",
                template: '<div style="color:black;"> #=StrCreateDocument # </div>',
                width: "150px",
                labels: {
                    visible: true,
                },
            },
            {
                field: "Name",
                title: "Document",
                template: '<div style="color:black;"> #=Name # </div>',
                labels: {
                    visible: true,
                },
            }, {
                field: "Company",
                title: "Company",
                template: '<div style="color:black;"> #=Company # </div>',
                labels: {
                    visible: true,
                },
            },
            {
                field: "Status",
                title: "Status",
                template: '<div style="color:black;"> #=Status # </div>',
                labels: {
                    visible: true,
                },
                width: "150px",
            }, {

                field: "StrDeadLine",
                title: "Deadline",
                template: '<div style="color:black;"> #=StrDeadLine # </div>',
                labels: {
                    visible: true,
                },
                width: "150px",
            },
            {
                title: "Action",
                width: "70px",
                headerTemplate: "<span class='actions-title'>Actions</span>",
                template: '<ul kendo-menu style="display: inline-block;overflow: visible" k-direction="\'left\'" k-orientation="\'horizontal\'" k-rebind="\'horizontal\'" k-on-select="">\
                     <li > ... \
                              <ul>\
                                    <li >\
                                           <a class="k-link jbi-show" href="#:DocumentUrl#" download="">View document</a>\
                                    </li>\
                                   #if(data.Status == "Completed"){#\
                                          <li ui-sref="SignDocumentESignature({documentId: #=Id#})" >\
                                          <span class="k-link jbi-show">View signature</span>\
                                    </li>\
                                   #}\
                                     else if(data.Status != "Completed" && IsMember==true){#\
                                        <li  ng-click="GetValidStaffForSign(#=Id#)">\
                                          <span class="k-link jbi-signing-a-contract">Sign document</span>\
                                      </li>\
                                  #}#\
                            </ul>\
                        </li>\
                    </ul>'
            }
            ],
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },

        };

        $scope.GetValidStaffForSign = function (documentId) {
            
            $http.get($scope.baseUrl + '/GetValidStaffForSign').success(function (data, status, headers, config) {
                console.log(data);
                if (data.Status) {

                    $state.go("SignDocumentESignature", { documentId: documentId }, 'ESignature', function () { });
                }
                else {

                    jbToast.error("You are not authorized to sign this document.");
                }
            });
        }

        $scope.AddDocSign = function () {
            if ("comesFromOrderItem" in window) {
                if (window.comesFromOrderItem) {
                    window.comesFromOrderItem = false;
                    $state.go("AddESignature", null);
                }
            }
            else {
                $state.go("AddESignature", null);
            }
        }

        $scope.search = function () {
            $("#userGrid").data("kendoGrid").dataSource.read();
        };
        $scope.clearFilters = function () {

            $scope.searchTerm = "";
            $scope.searchCompany = "";
            $scope.filter.ESignaturType = "0";
            $scope.filter.typeId = "-1";
            $("#userGrid").data("kendoGrid").dataSource.read();
        };
    }])

})();