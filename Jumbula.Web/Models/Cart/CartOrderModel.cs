﻿using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class CartOrderViewModel : BaseViewModel<Order,long>
    {
        public RegistrationType RegistrationType { get; set; }

        public int UserId { get; set; }

        public int CardSecurityCode { get; set; }
        public OrderMode OrderMode { get; set; }

        public string Name { get; set; }

        // if you need just in time Calculate OrderAmount Use this property 
        public decimal CalculateOrderAmount
        {
            get { return TotalAmount; }
        }

        // just int time calc orderAmount ignore negetive
        public decimal CalculateOrderWithoutNegetiveAmount
        {
            get
            {
                var amount = TotalAmount;
                if (amount < 0)
                {
                    amount = 0;
                    return amount;
                }
                else
                {
                    if (OrderInstallments != null && OrderInstallments.Any())
                    {

                        return OrderInstallments.First().Amount;
                    }
                }
                return amount;
            }

        }

        // amount of order

        // [Range(0, int.MaxValue)] 
        public decimal PaidAmount { get; set; }

        // amount paid by payment system, should be the same as the OrderAmount

        // Payment that user should pay now(If payment plan is installment, payable amount if different with total amount otherwise they will have same value)
        public decimal PayableAmount
        {
            get
            {
                if (this.PaymentPlanType == PaymentPlanType.FullPaid)
                {
                    return TotalAmount;
                }
                if (OrderInstallments != null && OrderInstallments.Any())
                {
                    return OrderInstallments.First().Amount;
                }

                return 0;
            }
        }

        public decimal TotalAmount
        {
            get
            {
                
                // add general charges
                decimal price =
                    OrderCharges.Where(charge => charge.Subcategory == ChargeDiscountSubcategory.Charge)
                        .Sum(m => m.Amount);

                // min general discount
               
                price =
                    OrderCharges.Where(
                        discount => discount.Subcategory == ChargeDiscountSubcategory.Discount)
                        .Aggregate(price, (current, discount) => current - discount.Amount);

                // add all the charges
                foreach (var item in OrderItems)
                {
                    // if item has been deleted  
                    if (item.ItemStatus == OrderItemStatusCategories.deleted)
                    {
                        continue;
                    }
                    price += item.GetOrderChargeDiscounts().Sum(c => c.Amount);
                }

                return price;
            }
        }

        [Required]
        public bool UserAnanymous { get; set; }

        // User status at the time he placed the ordoer      


        [Required]
        [StringLength(64)]
        public string ConfirmationId { get; set; }

        // for player reference, uniqueness in question?

        [StringLength(128)]
        public string TransactionId { get; set; }

        [StringLength(128)]
        public string TransactionMessage { get; set; }

        // optional field to capture any messages from payment gateway

        [StringLength(128)]
        public string TimeStamp { get; set; }

        [StringLength(32)]
        public string Currency { get; set; }

        [StringLength(128)]
        public string CardFirstName { get; set; }

        // Name on the card may be different than player

        [StringLength(128)]
        public string CardLastName { get; set; }

        // Name on the card may be different than player

        [StringLength(128)]
        public string CardEmail { get; set; }

        [StringLength(128)]
        public string CardNumber { get; set; }

        public int? PlayerId { get; set; }

        public DateTime OrderDate { get; set; } // time order was placed 

        public DateTime OrderPaidDate { get; set; } // time order was paid for

        public DateTime CardExpireDate { get; set; }

        public PaymentMethod PaymentMethod { get; set; }

        public OrderStatusCategories PaymentStatus { get; set; }

        public PostalAddress BillingAddress { get; set; }

        public ICollection<OrderItem> OrderItems { get; set; }
        public ICollection<OrderInstallment> OrderInstallments { get; set; }

        public List<OrderChargeDiscount> Charges { get; set; }
        public List<OrderChargeDiscount> Discounts { get; set; }

        public ProgramTypeCategory ProgramTypeCategory { get; set; }

        [Required]
        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }

        public virtual Coupon Coupon { get; set; }

        public int? CouponId { get; set; }

        public PaymentPlanType PaymentPlanType { get; set; }

        public ICollection<OrderChargeDiscount> OrderCharges { get; set; }

        #region Constructor

        public CartOrderViewModel(Order order)
        {

        }

        public CartOrderViewModel()
        {
            Charges = new List<OrderChargeDiscount>();
            Discounts = new List<OrderChargeDiscount>();
            OrderItems = new Collection<OrderItem>();
            OrderInstallments = new Collection<OrderInstallment>();
            //SettlementReportOrders = new Collection<SettlementReportOrders>();
            OrderCharges = new HashSet<OrderChargeDiscount>();
        }

        #endregion

        #region Methods





        public Order ToOrder()
        {
            return new Order();
        }


        #endregion

        public JbForm JbForm { get; set; }

    }
}