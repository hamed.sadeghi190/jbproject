﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('familyTakePaymentController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'familyOrderItemsService', 'familyOrdersService', 'TakePaymentInvoiceService', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, familyOrderItemsService, familyOrdersService, TakePaymentInvoiceService) {
        window.app.globalObjects.pageClass = 'seasons-view programs';
        window.app.globalObjects.initializeView();

        $scope.baseUrl = window.location.origin + "/Dashboard/People";
        $scope.MC.stateParams = $stateParams;
        $scope.stateParams = $stateParams;
        $scope.UserName = "";
        $scope.ModelOrder = familyOrdersService.get();
        $scope.OrderitemModel = familyOrderItemsService.get();

        $scope.cancelAction = function () {

            familyOrdersService.set($scope.ModelOrder)
            familyOrderItemsService.set($scope.OrderitemModel);
            $state.back();

        }

        var returnedByBack = $state.returnByBack();
        $scope.Model = {};
       
        $scope.totalAmount = 0;
        for (var i = 0; i < $scope.ModelOrder.length; i++) {

            if ($scope.ModelOrder[i].Checked) {

                $scope.ModelOrder[i].orginalAmount = $scope.ModelOrder[i].InvoiceAmount;
                $scope.totalAmount += $scope.ModelOrder[i].orginalAmount;
            }
        }
        var orderItemModel = [];
        for (var i = 0; i < $scope.ModelOrder.length; i++) {

            if ($scope.ModelOrder[i].Checked) {

                for (var j = 0; j < $scope.OrderitemModel[$scope.ModelOrder[i].Id].length; j++) {

                    if ($scope.OrderitemModel[$scope.ModelOrder[i].Id][j].Checked) {

                        orderItemModel.push($scope.OrderitemModel[$scope.ModelOrder[i].Id][j].Id);
                    }
                }
            }
        }

        if (returnedByBack) {
            $scope.Model = TakePaymentInvoiceService.get();
        }
        if (!returnedByBack || $scope.Model == null) {
        
            $http.get($scope.baseUrl + '/GetTakePaymentModel', { params: { orderItemModel: orderItemModel, userId: $stateParams.userId } }).success(function (data, status, headers, config) {
                $scope.Model = data;
            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }
        $scope.confirmTakePayment = function () {
            $http.post($scope.baseUrl + '/ValidateTakePayment', { model: $scope.Model })
                .success(function (data, status, headers, config) {
            
                    if (data.Status == true) {      
                        if ($scope.Model.SelectedChargeMethod == null || $scope.Model.SelectedChargeMethod == '') {
                            jbToast.error('You must choose the method of payment.');
                        } else {
                            TakePaymentInvoiceService.set($scope.Model);

                            $state.forward('ConfirmFamilyTakePayment', { userId: $stateParams.userId }, 'FamilyTakePayment', { userId: $stateParams.userId }, null);
                        }
                    } else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        }

        $scope.back = function () {
            if ("comesFromOrderItem" in window) {
                if (window.comesFromOrderItem) {
                    window.comesFromOrderItem = false;
                    $state.go("FamilyInvoicesOrTakePayment", { userId: $stateParams.userId, type:'TakePayment'});
                }
            }
            else {
                $state.go("FamilyInvoicesOrTakePayment", { userId: $stateParams.userId, type: 'TakePayment' });
            }
        }

        if (returnedByBack) {
            $scope.MC.backQueueItems.pop();
        }
    }]);
})();

