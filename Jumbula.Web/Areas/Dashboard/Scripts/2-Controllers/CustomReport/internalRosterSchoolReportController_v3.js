﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('internalRosterSchoolReportController_v3', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'county-providers-view';
            window.app.globalObjects.initializeView();

            var baseUrl = window.location.origin + '/Dashboard/CustomReport';

            var timer = false;

            $scope.Model = {};

            $scope.stateParams = $stateParams;
            $scope.MC.stateParams = $stateParams;

            $scope.updateChosenScope = function () {
                $("select[chosen]").trigger('chosen:updated');
            };
            $scope.$watch('AllPrograms', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });

            $http.get(baseUrl + '/GetAllPrograms', { params: { seasonDomain: $scope.stateParams.seasonDomain } }).success(function (data, status, headers, config) {  ///GetClubSeasonsProvider delete
                $scope.AllPrograms = data;

            }).error(function (data, status, headers, config) {
                jbToast.error(data.Message);
            });

            var checkValidation = function () {
                if ($scope.Model.ProgramId == null || $scope.Model.ProgramId.length == 0) {
                    jbToast.error("Please select a program.");
                    return false;
                }
                else {
                    return true;
                }
            }

            $scope.runReport = function () {

                if (checkValidation()) {
                    customReportService.saveSchoolSeasonId($scope.Model.schoolId, $scope.Model.SeasonId);

                    $http.get(baseUrl + '/GetInternalRosterSchoolReportV3', { params: { seasonDomain: $scope.stateParams.seasonDomain, programId: $scope.Model.ProgramId } }).success(function (data, status, headers, config) {
                        $scope.ModelPrograms = data;

                    }).error(function (data, status, headers, config) {
                        jbToast.error("error " + data);
                    });
                }

            }


            $scope.saveToPdf = function () {
                if (checkValidation()) {
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    a.href = baseUrl + '/GenerateInternalRosterSchoolReportPdfV3?seasonDomain=' + $scope.stateParams.seasonDomain + '&programId=' + $scope.Model.ProgramId;
                    a.download = "InternalRosterReport.Pdf";
                    a.click();
                }
            };

            $scope.saveToExcel = function () {
                if (checkValidation()) {
                    cfpLoadingBar.start();
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    a.href = '/Dashboard/CustomReport/GenerateInternalRosterReportExcelV3?seasonDomain=' + $scope.stateParams.seasonDomain + '&programId=' + $scope.Model.ProgramId;
                    a.download = "InternalRosterReport.xlsx";
                    a.click();
                    cfpLoadingBar.complete();
                }
            };
        }
    ]);


})();