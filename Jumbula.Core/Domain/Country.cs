﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class Country: BaseEntity
    {
        public Country()
        {
            States = new HashSet<State>();
        }

        [Required]
        [StringLength(80)]
        public string Name { get; set; }
        [StringLength(2)]
        public string ISO { get; set; }
        [StringLength(80)]
        public string CompleteName { get; set; }
        [StringLength(3)]
        public string ISO3 { get; set; }
        public short? NumCode { get; set; }
       public int PhoneCode { get; set; }
        public virtual ICollection<State> States { get; set; }
    }
}
