﻿using Jb.Framework.Common;
using Jumbula.Core.Business;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class InvoiceBusiness : IInvoiceBusiness
    {
        private readonly IPaymentDetailBusiness _paymentDetailBuiness;
        private readonly ITransactionActivityBusiness _transactionActivityBusiness;
        private readonly IRepository<Invoice> _invoiceRepository;

        public InvoiceBusiness(IPaymentDetailBusiness paymentDetailBuiness, ITransactionActivityBusiness transactionActivityBusiness, IRepository<Invoice> invoiceRepository)
        {
            _paymentDetailBuiness = paymentDetailBuiness;
            _transactionActivityBusiness = transactionActivityBusiness;
            _invoiceRepository = invoiceRepository;
        }
        public IQueryable<Invoice> GetList()
        {
            return _invoiceRepository.GetList();
        }

        public OperationStatus Create(Invoice invoice)
        {
            return _invoiceRepository.Create(invoice);

        }
        public IQueryable<Invoice> GetInvoiceByClubId(int clubId)
        {
            return _invoiceRepository.GetList().Where(i => i.ClubId == clubId);
        }
        public  Invoice Get(int? invoiceId)
        {
            return _invoiceRepository.GetList().SingleOrDefault(p => p.Id == invoiceId);
        }
        public IQueryable<Invoice> GetInvoicesUser(int userId)
        {
            return _invoiceRepository.GetList().Where(p => p.UserId == userId);
        }
        public string GetTokenForInvoice(int invoiceId)
        {
            var paymentDetail= _paymentDetailBuiness.GetByInvoiceId(invoiceId);
            var paymentDetailId = paymentDetail.Id;
            var token = _transactionActivityBusiness.GetList().Where(t => t.PaymentDetailId == paymentDetailId).First().Token;
            return token;
        }
        public List<TransactionActivity> GetInvoiceTransactions(int invoiceId)
        {
            var paymentDetail = _paymentDetailBuiness.GetByInvoiceId(invoiceId);
            var paymentDetailId = paymentDetail.Id;
            var transactions = _transactionActivityBusiness.GetList().Where(t => t.PaymentDetailId == paymentDetailId).ToList();
            return  transactions;
        }
        public OperationStatus UpdateInvoiceTransactions(PaymentDetail paymentDetail)
        {
            paymentDetail.Status = PaymentDetailStatus.CANCELED;
            paymentDetail.TransactionActivities.ToList().ForEach(t => t.TransactionStatus = TransactionStatus.Deleted);
            _paymentDetailBuiness.Update(paymentDetail);
            return new OperationStatus() { Status = true };
        }

        public OperationStatus AddMemoforInvoiceTransactions(int invoiceId, string memo)
        {
            PaymentDetail paymentdetail = _paymentDetailBuiness.GetByInvoiceId(invoiceId);
            paymentdetail.TransactionActivities.ToList().ForEach(t => t.Note = memo);
            _paymentDetailBuiness.Update(paymentdetail);
            return new OperationStatus() { Status = true };
        }

        public OperationStatus Update(Invoice invoice)
        {
            return _invoiceRepository.Update(invoice);
        }
    }

}
