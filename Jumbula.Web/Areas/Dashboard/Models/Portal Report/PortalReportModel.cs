﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public enum PortalReportType
    {
        [Description("Scholarship/Discount")]
        ScholarshipDiscount = 0,
        [Description("Discount")]
        Discount = 1,
        [Description("Scholarship")]
        Scholarship = 2,

        [Description("Catalog update")]
        CatalogUpdate = -1,
        [Description("Title 1 discount")]
        Title1Discount = -2,
        [Description("Country provider")]
        CountryProvider = -3,
        [Description("School member list")]
        SchoolMemberList = -4
    }
}