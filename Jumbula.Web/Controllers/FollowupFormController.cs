﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.WebLogic.UserService;
using Newtonsoft.Json;
using SportsClub.Models;

namespace Jumbula.Web.Controllers
{
    [Authorize(Roles = "Parent, Admin")]
    public class FollowupFormController : JbBaseController
    {
        public virtual ActionResult CompleteForm(int fId, string uId, long orderItemId)
        {
            if (User.Identity.IsCurrentUserExist() && this.GetCurrentUserRole() != RoleCategory.Parent)
            {
                return HttpNotFound();
            }
            var followupForm = Ioc.FollowupBusiness.Get(fId, uId);

            followupForm.JbForm.CurrentMode = AccessMode.Edit;
            followupForm.JbForm.SetElementsCurrentMode(AccessMode.Edit);
            ViewBag.OrderItemId = orderItemId;

            return View("FollowupForm", followupForm);
        }

        [HttpPost]
        public virtual ActionResult SaveUplodedForms(FollowUpFormUploadViewModel model)
        {
            if (model.FileUrlsList != null && model.FileUrlsList.Length > 0)
            {
                var completedForm = Ioc.FollowupBusiness.Get(model.FormId, model.UId);

                completedForm.Status = FollowupStatus.Completed;


                var section = completedForm.JbForm.Elements.FirstOrDefault() as JbSection;

                for (var i = 0; i < model.FileUrlsList.Length; i++)
                {
                    var newelement = new JbUpload();
                    newelement.Title = model.FileNamesList[i];
                    newelement.Value = model.FileUrlsList[i];
                    section.Elements.Add(newelement);

                }
                completedForm.JbForm.JsonElements = JsonConvert.SerializeObject(completedForm.JbForm.Elements);
                Ioc.JbFormBusiness.CreateEdit(completedForm.JbForm);
                Ioc.FollowupBusiness.Update(completedForm);

                if (User.Identity.IsCurrentUserExist() && this.GetCurrentUserRole() == RoleCategory.Parent)
                {
                    return Redirect(@Url.Action("OrderItemFollowUpForm", "Registrant", new { orderItemId = model.OrderItemId }));
                }

                return RedirectToAction("DisplaySucceedOperation", "Cart");
            }
            return RedirectToAction("UploadFollowUpForm", "FollowupForm", new { orderItemId = model.OrderItemId, formId = model.FormId, uId = model.UId });
        }

        public virtual JsonNetResult GetFollowupFormByAdmin(int fId, bool editable)
        {
            var followupFormModel = Ioc.FollowupBusiness.Get(fId);
            if (!editable)
            {
                followupFormModel.JbForm.CurrentMode = AccessMode.ReadOnly;
                followupFormModel.JbForm.SetElementsCurrentMode(AccessMode.ReadOnly);
            }
            else
            {
                followupFormModel.JbForm.CurrentMode = AccessMode.Edit;
                followupFormModel.JbForm.SetElementsCurrentMode(AccessMode.Edit);
            }

            return JsonNet(new
            {
                Id = followupFormModel.Id,
                Guid = followupFormModel.Guid,
                JbFormHtml = RenderPartialViewToString("EditorTemplates/JbForm",
                followupFormModel.JbForm),
                FormName = followupFormModel.FormName,
                FormType = followupFormModel.FormType,
            });
        }

        public virtual ActionResult DownloadForm(int fId, string uId, long orderItemId)
        {
            var completedForm = Ioc.FollowupBusiness.Get(fId, uId);
            var section = completedForm.JbForm.Elements.FirstOrDefault() as JbSection;
            var model = new FollowUpFormDownlodViewModel()
            {
                FormId = fId,
                UId = uId,
                OrderItemId = orderItemId
            };
            model.FollowUpFiles = new List<FollowUpDownlodFiles>();

            foreach (var element in section.Elements)
            {

                model.FollowUpFiles.Add(new FollowUpDownlodFiles()
                {
                    filename = ((JbUpload)element).Title,
                    fileUrl = ((JbUpload)element).Value
                });
            }

            return View("FollowUpDownload", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [JbAudit(JbAction.Family_Complete_FollowupForm)]
        public virtual ActionResult CompleteForm(OrderItemFollowupForm model, long orderItemId)
        {
            ModelState.Remove("FormName");
            ViewBag.OrderItemId = orderItemId;
            if (ModelState.IsValid)
            {
                var completedForm = Ioc.FollowupBusiness.Get(model.Id, model.Guid);
                completedForm.Status = FollowupStatus.Completed;
                completedForm.JbForm = model.JbForm;

                var formResult = Ioc.JbFormBusiness.CreateEdit(model.JbForm);
                var result = Ioc.FollowupBusiness.Update(completedForm);

                SetActivityDescription(Constants.AuditingConstants.AuditingChangeFormat, completedForm.FormName);
                Status = true;

                if (User.Identity.IsCurrentUserExist() && this.GetCurrentUserRole() == RoleCategory.Parent)
                {
                    return Redirect(@Url.Action("OrderItemFollowUpForm", "Registrant", new { orderItemId = orderItemId }));
                }

                return RedirectToAction("DisplaySucceedOperation", "Cart");
            }
            return View("FollowupForm", model);
        }

        public virtual ActionResult UploadFollowUpForm(long orderItemId, int formId, string uId)
        {
            FollowUpFormUploadViewModel model = new FollowUpFormUploadViewModel()
            {
                OrderItemId = orderItemId,
                FormId = formId,
                UId = uId
            };
            ViewBag.OrderItemId = orderItemId;
            ViewBag.FormId = formId;
            return View("FormUpload", model);
        }

        public virtual ActionResult EditOrderForm(JbForm formData, int fId, string uId)
        {
            try
            {
                var completedForm = Ioc.FollowupBusiness.Get(fId, uId);
                completedForm.Status = FollowupStatus.Completed;
                completedForm.JbForm = formData;
                Ioc.JbFormBusiness.CreateEdit(formData);
                Ioc.FollowupBusiness.Update(completedForm);
                return Json(new JResult() { Status = true });
            }
            catch (Exception ex)
            {
                return Json(new JResult() { Status = false, Message = ex.Message });
            }
        }

        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        public virtual ActionResult Print(int id, bool isRegistrationForm = false)
        {
            if (isRegistrationForm == false)
            {
                var model = Ioc.FollowupBusiness.Get(id);
                model.JbForm.CurrentMode = AccessMode.Edit;
                model.JbForm.SetElementsCurrentMode(AccessMode.Print);

                var modelJbForm = model.JbForm;
                return View(modelJbForm);
            }
            else
            {
                var model = Ioc.JbFormBusiness.Get(id);
                model.CurrentMode = AccessMode.Edit;
                model.SetElementsCurrentMode(AccessMode.Print);
                return View(model);
            }
            //var JbFormHtml = RenderPartialViewToString("EditorTemplates/JbForm", model.JbForm);
            //ViewBag.HT = JbFormHtml;
            // return Pdf("testForm", "Print", model, "testHeader");
        }

        public virtual ActionResult RemoveItem(int fId, string uId, long orderItemId, string Url)
        {
            var uploadForm = Ioc.FollowupBusiness.Get(fId, uId);
            var section = uploadForm.JbForm.Elements.FirstOrDefault() as JbSection;

            var list = section.Elements.ToList();

            foreach (var element in list)
            {
                if ((element as JbUpload).Value == Url)
                {
                    //StorageManager sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
                    //sm.DeleteBlob(Url);
                    section.Elements.Remove(element);
                }
            }
            uploadForm.JbForm.JsonElements = JsonConvert.SerializeObject(uploadForm.JbForm.Elements);
            Ioc.JbFormBusiness.CreateEdit(uploadForm.JbForm);

            // Ioc.JbFormBusiness.Update(uploadForm.JbForm);

            //var model = new FollowUpFormDownlodViewModel()
            //{
            //    FormId = fId,
            //    UId = uId,
            //    OrderItemId = orderItemId
            //};
            //model.FollowUpFiles = new List<FollowUpDownlodFiles>();

            //foreach (var element in section.Elements)
            //{

            //    model.FollowUpFiles.Add(new FollowUpDownlodFiles()
            //    {
            //        //ElementId = ((JbUpload)element).ElementId,
            //        filename = ((JbUpload)element).Title,
            //        fileUrl = ((JbUpload)element).Value
            //    });

            //}
            return RedirectToAction("DownloadForm", new { fId = fId, uId = uId, orderItemId = orderItemId });
        }
    }
}