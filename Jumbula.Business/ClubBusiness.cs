﻿
using Jb.Framework.Common;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Model.Club;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Web;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Constants = Jumbula.Common.Constants.Constants;
using System.Web.Mvc;

namespace Jumbula.Business
{
    public class ClubBusiness : IClubBusiness
    {
        private readonly IClubRepository _clubRepository;
        private IUserProfileBusiness _userProfileBusiness;
        private readonly IStorageBusiness _storageBusiness;
        private IFormBusiness _formBusiness;
        private readonly IJbFormBusiness _jbFormBusiness;
        private IOrderBusiness _orderBusiness;
        private ISecurityBusiness _actionBusiness;
        private IAttributeBusiness _attributeBusiness;
        private readonly IEntitiesContext _dataContext;
        private IProgramBusiness _programBusiness;

        public ClubBusiness(IClubRepository clubRepository, IStorageBusiness storageBusiness, IJbFormBusiness jbFormBusiness, IEntitiesContext dataContext)
        {
            _clubRepository = clubRepository;

            _storageBusiness = storageBusiness;
            _jbFormBusiness = jbFormBusiness;

            _dataContext = dataContext;

        }

        public IUserProfileBusiness UserProfileBusiness
        {
            get { return _userProfileBusiness; }
            set { _userProfileBusiness = value; }
        }

        public IProgramBusiness ProgramBusiness
        {
            get { return _programBusiness; }
            set { _programBusiness = value; }
        }
        public IFormBusiness FormBusiness
        {
            get { return _formBusiness; }
            set { _formBusiness = value; }
        }

        public IOrderBusiness OrderBusiness
        {
            get { return _orderBusiness; }
            set { _orderBusiness = value; }
        }

        public ISecurityBusiness ActionBusiness
        {
            get { return _actionBusiness; }
            set { _actionBusiness = value; }
        }

        public IAttributeBusiness AttributeBusiness
        {
            get { return _attributeBusiness; }
            set { _attributeBusiness = value; }
        }

        public Club GetClubByClientId(long clientId)
        {
            return GetList().FirstOrDefault(c => c.ClientId == clientId);
        }
        public bool HasDefaultBillingCard(int id)
        {
            var client = Get(id).Client;
            return (client != null && client.CreditCards != null && client.CreditCards.Any());
        }
        public bool IsClubSchool(ClubType clubtype)
        {
            return (clubtype != null && (clubtype.EnumType == ClubTypesEnum.School) || (clubtype.ParentType != null && clubtype.ParentType.EnumType == ClubTypesEnum.School));
        }

        public bool IsClubSchool(int typeId)
        {
            var clubType = _dataContext.Set<ClubType>().SingleOrDefault(t => t.Id == typeId);

            if (clubType == null)
            {
                return false;
            }

            return IsClubSchool(clubType);
        }

        public bool IsClubProvider(ClubType clubtype)
        {
            return clubtype != null && ((clubtype.EnumType == ClubTypesEnum.Provider) ||
                                        (clubtype.ParentType != null &&
                                         clubtype.ParentType.EnumType == ClubTypesEnum.Provider));
        }

        public IQueryable<JbUser> GetAllUserWithSpecificRoleForClubs(int clubId, RoleCategory role)
        {
            return Get(clubId).ClubStaffs.Where(
                     c => !c.IsDeleted && c.JbUserRole.Role.Name.Equals(role.ToString(), StringComparison.OrdinalIgnoreCase)).Select(c => c.JbUserRole.User).AsQueryable();
        }

        public IQueryable<JbUser> GetAllUserWithSpecificRolesForClubs(int clubId, List<RoleCategory> roles)
        {
            var allRoles = roles.Select(r => r.ToString()).ToList();

            return Get(clubId).ClubStaffs.Where(
                     c => !c.IsDeleted && c.Status != StaffStatus.Pending && allRoles.Contains(c.JbUserRole.Role.Name)).Select(c => c.JbUserRole.User).AsQueryable();
        }

        public IQueryable<JbUser> GetActiveUsersWithSpecificRoles(int clubId, List<RoleCategory> roles)
        {
            var allRoles = roles.Select(r => r.ToString()).ToList();

            return Get(clubId).ClubStaffs.Where(
                     c => !c.IsDeleted && !c.IsFrozen && c.Status == StaffStatus.Active && allRoles.Contains(c.JbUserRole.Role.Name)).Select(c => c.JbUserRole.User).AsQueryable();
        }

        public Club Get(string domain)
        {
            return _clubRepository.Get(c => c.Domain == domain && c.IsDeleted == false);
        }
        public IQueryable<Club> GetList()
        {
            return _clubRepository.GetList().Where(c => c.IsDeleted == false);
        }

        public IQueryable<Club> GetSchools()
        {
            return GetList().Where(c => c.ClubType != null && ((c.ClubType.EnumType == ClubTypesEnum.School) || (c.ClubType.ParentType != null && c.ClubType.ParentType.EnumType == ClubTypesEnum.School)));
        }

        public OperationStatus Delete(int clubId)
        {
            var club = _clubRepository.Get(d => d.Id == clubId);
            club.IsDeleted = true;
            return Update(club);
        }
        public bool IsClubDomainExist(string domain, int clubId = 0)
        {
            if (clubId == 0)
            {
                return _clubRepository.GetList().Any(c => c.Domain.ToLower() == domain.ToLower());
            }
            else
            {
                return _clubRepository.GetList().Any(c => c.Domain.ToLower() == domain.ToLower() && c.Id != clubId);
            }
        }

        public Club Get(int clubId)
        {
            return _clubRepository.Get(c => c.Id == clubId && c.IsDeleted == false);
        }

        public Club GetBaseInfo(string domain)
        {
            return _clubRepository.Get(c => c.Domain.Equals(domain, StringComparison.OrdinalIgnoreCase) && c.IsDeleted == false);
        }

        public IQueryable<ClubType> GetClubTypeList()
        {
            return _dataContext.Set<ClubType>().Select(c => c);
        }
        public IQueryable<ClubType> GetClubTypeListForSchools()
        {
            var schooltypeId = _dataContext.Set<ClubType>().Where(p => p.EnumType == ClubTypesEnum.School).FirstOrDefault().Id;
            return _dataContext.Set<ClubType>().Where(p => p.ParentId == schooltypeId).Select(c => c);
        }
        public IQueryable<ClubType> GetClubTypeListForProvider()
        {
            var schooltypeId = _dataContext.Set<ClubType>().Where(p => p.EnumType == ClubTypesEnum.Provider).FirstOrDefault().Id;
            return _dataContext.Set<ClubType>().Where(p => p.ParentId == schooltypeId).Select(c => c);
        }
        public IQueryable<Club> GetAllPartnerSchools(int partnerId)
        {
            return GetList().Where(c => c.PartnerId == partnerId && (c.ClubType != null &&
                (c.ClubType.EnumType == ClubTypesEnum.School ||
                 (c.ClubType.ParentType != null &&
                  c.ClubType.ParentType.EnumType == ClubTypesEnum.School))));
        }

        public IQueryable<Club> GetAllPartnerProviders(int partnerId)
        {
            return GetList().Where(c => c.PartnerId == partnerId && (c.ClubType != null && (c.ClubType.EnumType == ClubTypesEnum.Provider || (c.ClubType.ParentType != null && c.ClubType.ParentType.EnumType == ClubTypesEnum.Provider))));
        }

        public IQueryable<Club> GetRelatedClubs(int partnerId, bool includeDestricts = true)
        {
            return GetList().Where(c => c.PartnerId == partnerId || c.DistrictId == partnerId && includeDestricts);
        }

        public IQueryable<Club> GetRelatedClubs(string partnerDomain)
        {
            return Get(partnerDomain).RelatedClubs.Where(c => c.IsDeleted == false).AsQueryable();
        }

        public Club GetPartner(string clubDomain)
        {
            var club = Get(clubDomain);

            if (club.PartnerId.HasValue)
            {
                return club.PartnerClub;
            }

            var jumbula = new Club()
            {
                Domain = Constants.W_Jumbula,
                Name = Constants.W_Jumbula,
                Site = Constants.JumbulaDotCom_Url,
                ContactPersons = new List<ContactPerson>()
                {
                    new ContactPerson
                    {
                       Cell = "",
                       Email = Constants.W_JumbulaInfoEmail,
                       Phone = Constants.Jumbula_PhoneNumber,
                    }
                },
                ClubLocations = new List<ClubLocation>()
                {
                    new ClubLocation
                    {
                        Name = "Jumbula location",
                        PostalAddress = new PostalAddress()
                        {
                            Address = Constants.Jumbula_Address
                        }
                    }
                }
            };

            return jumbula;
        }

        public Club GetDistrict(string clubDomain)
        {
            var club = Get(clubDomain);

            if (club.DistrictId.HasValue)
            {
                return club.District;
            }

            return null;
        }
        public PartnerSetting GetPartnerSetting(int partnerId)
        {
            var club = Get(partnerId);

            var serialized = JsonConvert.SerializeObject(new PartnerSetting());

            var partnerSetting = JsonConvert.DeserializeObject<PartnerSetting>(club.SettingSerialized);

            return partnerSetting;
        }
        public ClubSetting GetClubSetting(int clubId)
        {
            var club = Get(clubId);

            var clubSetting = JsonConvert.DeserializeObject<ClubSetting>(club.SettingSerialized);

            return clubSetting;
        }
        public OperationStatus Create(Club club)
        {
            var result = new OperationStatus();
            if (club.Setting == null)
            {
                club.Setting = new ClubSetting();
            }

            club.Setting.AutoChargePolicy = new AutoChargePolicy(true);
            club.Setting.DelinquentPolicy = new DelinquentPolicy();

            club.SettingSerialized = JsonConvert.SerializeObject(club.Setting);
            if (GetList().Any(c => c.Domain.ToLower().Equals(club.Domain.ToLower())))
            {
                result.Status = false;
                result.Message = "A club with this domain exist.";
            }
            else
            {
                result = _clubRepository.Create(club);
            }

            return result;
        }

        public string GetClubLogoURL(string Domain, string logo)
        {

            string logoUri = _storageBusiness.GetUri(Constants.Path_ClubFilesContainer, Domain, logo).AbsoluteUri; //StorageManager.GetUri(Constants.Path_ClubFilesContainer, Domain, logo).AbsoluteUri;

            if (string.IsNullOrEmpty(logo))
            {
                logoUri = Constants.Club_NoSport_Default_Image_Large;

                logoUri = string.Format("{0}://{1}{2}", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.Url.Host, logoUri);
            }

            var rnd = new Random(Environment.TickCount);

            logoUri += "?v=" + rnd.Next();

            return logoUri;
        }

        public List<ClubWaiver> GetWaivers(int id)
        {
            return Get(id).ClubWaivers.Where(w => !w.IsDeleted).ToList();
        }

        public List<ClubWaiver> GetWaivers(IEnumerable<int> ids)
        {

            return _dataContext.Set<ClubWaiver>().Where(e => !e.IsDeleted && ids.Contains(e.Id)).ToList();
        }

        public OperationStatus Update(Club club, List<JbUserRole> deletedUserRoles = null)
        {
            if (club.IsSettingChanged)
            {
                club.SettingSerialized = JsonConvert.SerializeObject(club.Setting);
            }

            if (deletedUserRoles != null && deletedUserRoles.Any())
            {

                club.ClubStaffs.Where(u => deletedUserRoles.Select(r => r.UserId).Contains(u.JbUserRole.UserId)).ToList().ForEach(s => _dataContext.Entry<ClubStaff>(s).State = System.Data.Entity.EntityState.Deleted);

                club.ClubStaffs.Select(c => c.JbUserRole).Where(u => deletedUserRoles.Select(r => r.UserId).Contains(u.UserId)).ToList().ForEach(ur => _dataContext.Entry<JbUserRole>(ur).State = System.Data.Entity.EntityState.Deleted);
            }

            var result = _clubRepository.Update(club);

            if (result.RecordsAffected < 1 && result.Exception == null)
            {
                result.Status = true;
            }

            return result;
        }

        public OperationStatus UpdateWaiver(ClubWaiver clubWaiver)
        {
            var oldClubWaiver = _dataContext.Set<ClubWaiver>().Single(c => c.Id == clubWaiver.Id);

            oldClubWaiver.Name = clubWaiver.Name;
            oldClubWaiver.Text = clubWaiver.Text;
            oldClubWaiver.IsRequired = clubWaiver.IsRequired;
            oldClubWaiver.WaiverConfirmationType = clubWaiver.WaiverConfirmationType;
            int result = _dataContext.SaveChanges();

            return new OperationStatus() { Status = result > 0 };
        }

        public OperationStatus DeleteWaiver(int id)
        {

            ClubWaiver waiver = _dataContext.Set<ClubWaiver>().Single(c => c.Id == id);

            waiver.IsDeleted = true;

            return UpdateWaiver(waiver);
        }

        public OperationStatus CreateWaiver(ClubWaiver clubWaiver)
        {
            var club = _dataContext.Set<Club>().Single(c => c.Id == clubWaiver.Club_Id);

            club.ClubWaivers.Add(clubWaiver);

            int result = _dataContext.SaveChanges();

            return new OperationStatus() { Status = result > 0 };
        }

        public IEnumerable<EmailTemplate> GetTemplate(int clubId, TemplateType temType)
        {
            return _clubRepository.Get(c => c.Id == clubId).Templates.Where(t => t.Type == temType);
        }

        public int UpdateClubMembers(List<AllClubMembers> membersUserIdList)
        {
            var clubIds = new Dictionary<string, int>();

            int Count = 0;

            foreach (var user in membersUserIdList)
            {
                var isExist = _clubRepository.Get(m => m.Domain.Equals(user.ClubDomain)).Users.Any(m => m.User.Id.Equals(user.UserProfileId));

                if (!isExist)
                {
                    int clubId = 0;

                    if (!clubIds.ContainsKey(user.ClubDomain))
                    {
                        clubId = _clubRepository.Get(m => m.Domain.Equals(user.ClubDomain)).Id;
                        clubIds.Add(user.ClubDomain, clubId);
                    }
                    else
                    {
                        clubId = clubIds.Single(m => m.Key == user.ClubDomain).Value;
                    }

                    var clubUser = _dataContext.Set<JbUser>().Single(u => u.Id == user.UserProfileId);

                    _clubRepository.Get(m => m.Domain.Equals(user.ClubDomain)).Users.Add(new ClubUser() { ClubId = clubId, UserId = clubUser.Id });

                    Count++;
                }
            }

            _dataContext.SaveChanges();

            return Count;
        }

        public OperationStatus UpdateClubMembers(string clubDomain, int userId)
        {
            var isExist = _clubRepository.Get(m => m.Domain.Equals(clubDomain)).Users.Any(m => m.UserId.Equals(userId));

            if (!isExist)
            {
                var clubId = _clubRepository.Get(m => m.Domain.Equals(clubDomain)).Id;

                var clubUser = _dataContext.Set<JbUser>().Single(u => u.Id.Equals(userId));

                _clubRepository.Get(m => m.Domain.Equals(clubDomain)).Users.Add(new ClubUser() { ClubId = clubId, UserId = clubUser.Id });

                _dataContext.SaveChanges();

                return new OperationStatus { Status = true };
            }
            else
            {
                return new OperationStatus { Status = false };
            }
        }

        public bool AddUserInClubUsers(Order order)
        {
            var result = true;

            var userId = order.UserId;

            var clubIds = new List<int>();

            clubIds.Add(order.ClubId);

            foreach (var orderItem in order.OrderItems)
            {
                var outSourceClubId = 0;

                if (orderItem.ProgramScheduleId.HasValue)
                {
                    outSourceClubId = orderItem.ProgramSchedule.Program.OutSourceSeasonId.HasValue ? orderItem.ProgramSchedule.Program.OutSourceSeason.ClubId : 0;
                }

                if (outSourceClubId != 0 && !clubIds.Contains(outSourceClubId))
                {
                    clubIds.Add(outSourceClubId);
                }
            }

            var clubs = _clubRepository.GetList(c => clubIds.Contains(c.Id)).ToList();
            var userValid = false;
            foreach (var club in clubs)
            {
                if (!club.Users.Any(c => c.UserId == userId))
                {
                    club.Users.Add(new ClubUser() { ClubId = club.Id, UserId = userId });
                    userValid = true;
                }
            }

            result = _dataContext.SaveChanges() > 0;

            if (!result && userValid)
            {
                throw new Exception(string.Format("User {0}, didn't add to database.", order.User.UserName));
            }

            return result;
        }

        public bool AddUserInClubUsers(int userId, int clubId)
        {
            var club = Get(clubId);

            if (!club.Users.Any(c => c.UserId == userId))
            {
                club.Users.Add(new ClubUser() { ClubId = clubId, UserId = userId });
                return Update(club).Status;
            }

            return true;
        }

        public List<ClubFormTemplate> GetFormTemplates(int clubId, FormType? formType)
        {
            IQueryable<ClubFormTemplate> clubFormTemplates = Get(clubId).ClubFormTemplates.Where(t => !t.IsDeleted).AsQueryable();

            if (!clubFormTemplates.Any(c => c.FormType == FormType.Registration))
            {
                AddDefaultTemplates(clubId);
            }

            if (formType.HasValue)
            {
                clubFormTemplates = clubFormTemplates.Where(t => t.FormType == formType);
            }

            return clubFormTemplates.ToList();
        }

        public OperationStatus AddDefaultTemplates(int clubId)
        {
            var defaultForm = _formBusiness.GetDefaultRegistrationForm(true);
            var defaultSchoolForm = _formBusiness.GetDefaultSchoolRegistrationForm(true);

            var result1 = AddFormTemplate(clubId, defaultForm, Constants.W_Default, FormType.Registration, DefaultFormType.Primary);

            if (result1.Status)
            {
                var result2 = AddFormTemplate(clubId, defaultSchoolForm, Constants.Default_School_Form, FormType.Registration, DefaultFormType.School);
                return result2;
            }

            return result1;
        }

        public OperationStatus AddDefaultChessTeamTemplate(int clubId)
        {
            var jbForm = _formBusiness.GetDefaultTeamTournamnetRegistrationForm();

            return AddFormTemplate(clubId, jbForm, Constants.Default_Team_ChessTournament_Form, FormType.Registration);
        }

        public OperationStatus AddFormTemplate(int clubId, JbForm jbForm, string templateName, FormType formType, DefaultFormType? defaultFormType = null)
        {

            _dataContext.Set<ClubFormTemplate>().Add(
                new ClubFormTemplate
                {
                    ClubId = clubId,
                    DateCreated = DateTime.UtcNow,
                    FormType = formType,
                    JbForm = jbForm,
                    Title = templateName,
                    DefaultFormType = defaultFormType
                });

            int result = _dataContext.SaveChanges();

            return new OperationStatus { Status = result > 0 };
        }

        public OperationStatus AddFormTemplate(long clubId, int jbFormId, string templateName, FormType formType, DefaultFormType? defaultFormType = null)
        {

            var club = _dataContext.Set<Club>().SingleOrDefault(c => c.Id == clubId);

            club.ClubFormTemplates.Add(new ClubFormTemplate { Title = templateName, JbForm_Id = jbFormId, FormType = formType, DateCreated = DateTime.UtcNow, DefaultFormType = defaultFormType });

            var result = _clubRepository.Save();

            return new OperationStatus { Status = result.Status };
        }

        public List<ClubFormTemplate> GetFormTemplates(int clubId, IEnumerable<int> ids, FormType? formType)
        {
            List<ClubFormTemplate> clubFormTemplates = null;

            if (formType.HasValue)
            {
                clubFormTemplates = _dataContext.Set<Club>().SingleOrDefault(s => s.Id == clubId).ClubFormTemplates.Where(t => !t.IsDeleted && ids.Contains(t.Id) && t.FormType == formType).ToList();
            }
            else
            {
                clubFormTemplates = _dataContext.Set<Club>().SingleOrDefault(s => s.Id == clubId).ClubFormTemplates.Where(t => !t.IsDeleted && ids.Contains(t.Id)).ToList();
            }

            return clubFormTemplates;
        }

        public OperationStatus UpdateFromTemplate(ClubFormTemplate formTemplate)
        {
            var oldFromTemplate = _dataContext.Set<ClubFormTemplate>().SingleOrDefault(c => c.Id == formTemplate.Id);

            oldFromTemplate.Title = formTemplate.Title;
            oldFromTemplate.JbForm_Id = formTemplate.Id;
            oldFromTemplate.FormType = formTemplate.FormType;

            int result = _dataContext.SaveChanges();

            return new OperationStatus { Status = result > 0 };
        }

        public OperationStatus DeleteFormTemplate(int id)
        {
            var formTemplate = _dataContext.Set<ClubFormTemplate>().SingleOrDefault(f => f.Id == id);

            formTemplate.IsDeleted = true;

            int result = _dataContext.SaveChanges();

            return new OperationStatus { Status = result > 0 };
        }

        public DateTime GetClubDateTime(Club club, DateTime utcDate)
        {
            var timeZone = club.TimeZone;
            TimeZoneInfo localTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone.ToDescription());
            return TimeZoneInfo.ConvertTimeFromUtc(utcDate, localTimeZone);
        }

        public DateTime GetClubDateTime(int clubId, DateTime utcDate)
        {
            var timeZone = Get(clubId).TimeZone;
            TimeZoneInfo localTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone.ToDescription());
            return TimeZoneInfo.ConvertTimeFromUtc(utcDate, localTimeZone);
        }
        public DateTime GetClubDateTime(string clubDomain, DateTime utcDate)
        {
            var timeZone = Get(clubDomain).TimeZone;
            TimeZoneInfo localTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone.ToDescription());
            return TimeZoneInfo.ConvertTimeFromUtc(utcDate, localTimeZone);
        }


        public JbForm GetDefaultDonationForm()
        {
            var form = new JbForm();
            form.Title = "Donation";
            form.RefEntityName = typeof(Club).Name;
            var section = new JbSection() { CurrentMode = AccessMode.Edit, Name = Constants.Donation.Section_Info_Name, Title = "Personal Information" };

            section.Elements.Add(new JbParagraph() { Name = "FormHeader", Title = "Form Header", Value = "You can show your appreciation for this club and support us by donating!" });
            section.Elements.Add(new JbTextBox() { Title = "First name", Name = "FirstName", CurrentMode = AccessMode.Edit });
            section.Elements.Add(new JbTextBox() { Title = "Last name", Name = "LastName", CurrentMode = AccessMode.Edit });
            form.Elements.Add(section);


            var sectionAmount = new JbSection() { Name = Constants.Donation.Section_Amount_Name, Title = "Donation Payment", CurrentMode = AccessMode.ReadOnly };
            var amount = new JbRadioList() { Title = "Amount", Name = Constants.Donation.Element_Amount_Name, CurrentMode = AccessMode.ReadOnly, IsRequired = true };
            amount.Items.Add(new JbElementItem() { Text = "$50", Value = "50" });
            amount.Items.Add(new JbElementItem() { Text = "$100", Value = "100" });
            amount.Items.Add(new JbElementItem() { Text = "$150", Value = "150" });
            sectionAmount.Elements.Add(amount);
            form.Elements.Add(sectionAmount);

            return form;
        }
        public int GenerateDonationForm()
        {
            var form = GetDefaultDonationForm();
            form.CurrentMode = AccessMode.Design;
            _jbFormBusiness.CreateEdit(form);

            var str = form.JsonElements;

            return form.Id;
        }

        public List<ClubStaff> GetClubsByMember(string userName)
        {
            var clubMembers = this._dataContext.Set<ClubUser>().Where(s => s.User.UserName.Equals(userName, StringComparison.CurrentCultureIgnoreCase));

            return clubMembers.ToList().Select(s =>
               new ClubStaff
               {
                   Id = s.ClubId,
                   Club = s.Club,
                   JbUserRole = new JbUserRole { Role = new JbRole { Name = "Parent" }, User = s.User }
               })
               .ToList();
        }

        public IQueryable<Club> GetClubsByMember(int userId)
        {
            return this._dataContext.Set<ClubUser>().Where(s => s.User.Id == userId).Select(s => s.Club);
        }

        public ClubStaff GetStaff(int staffId)
        {
            return _dataContext.Set<ClubStaff>().SingleOrDefault(s => s.Id == staffId);
        }

        public ClubStaff GetStaff(string userName, int clubId)
        {
            var club = Get(clubId);
            var result =
                  club
                    .ClubStaffs.SingleOrDefault(
                        s => !s.IsDeleted && s.Status == StaffStatus.Active && !s.IsFrozen && s.JbUserRole.User.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase));

            if (club.ClubType.EnumType != ClubTypesEnum.Partner && club.ClubType.EnumType != ClubTypesEnum.SchoolDistrict)
            {
                if (result == null && club.DistrictId.HasValue)
                {
                    result = GetStaffFromDistrict(club, userName);
                }

                if (result == null && club.PartnerId.HasValue)
                {
                    result = GetStaffFromPartner(club, userName);
                }
            }
            else
            {
                if (result == null && club.PartnerId.HasValue)
                {
                    result = GetStaffFromPartner(club, userName);
                }

                if (result == null && club.DistrictId.HasValue)
                {
                    result = GetStaffFromDistrict(club, userName);
                }

            }

            if (result == null && _userProfileBusiness.IsUserInRole(userName, RoleCategory.Support))
            {
                var supportUser = _userProfileBusiness.Get(userName);

                var supportRole = supportUser.Roles.Single(r => r.Role.Name.Equals(RoleCategory.Support.ToString()));

                var role = RoleCategory.Admin;

                if (club.ClubType.EnumType == ClubTypesEnum.Partner)
                {
                    role = RoleCategory.Partner;
                }

                var jbRole = _userProfileBusiness.GetRole(role);

                result = new ClubStaff()
                {
                    RoleOnClub = role,
                    Club = club,
                    ClubId = club.Id,
                    JbUserRole = new JbUserRole()
                    {
                        Role = jbRole,
                        RoleId = jbRole.Id,
                        User = supportUser,
                        UserId = supportUser.Id
                    }
                };
            }

            return result;
        }

        private ClubStaff GetStaffFromPartner(Club club, string userName)
        {
            var parentRoles = new List<string> { RoleCategory.Partner.ToString(), RoleCategory.Admin.ToString(), RoleCategory.Manager.ToString() };

            var partner = GetPartner(club.Domain);

            var partnerStaffs = partner.ClubStaffs.AsQueryable().AsNoTracking();

            var result = partnerStaffs.SingleOrDefault(
                    s => !s.IsDeleted && s.Status == StaffStatus.Active && !s.IsFrozen && s.JbUserRole.User.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase) && parentRoles.Contains(s.JbUserRole.Role.Name));

            return result;
        }

        private ClubStaff GetStaffFromDistrict(Club club, string userName)
        {
            var parentRoles = new List<string> { RoleCategory.Partner.ToString(), RoleCategory.Admin.ToString(), RoleCategory.Manager.ToString(), RoleCategory.OnsiteSupportManager.ToString() };

            var district = GetDistrict(club.Domain);

            var districtStaffs = district.ClubStaffs.AsQueryable().AsNoTracking();

            var result = districtStaffs.SingleOrDefault(
                    s => !s.IsDeleted && s.Status == StaffStatus.Active && !s.IsFrozen && s.JbUserRole.User.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase) && parentRoles.Contains(s.JbUserRole.Role.Name));

            return result;
        }

        public ClubStaff GetStaff(int userRoleId, int clubId)
        {
            return
                Get(clubId)
                    .ClubStaffs.SingleOrDefault(
                        s => s.JbUserRole.Id == userRoleId);
        }

        public IQueryable<ClubStaff> GetStaffs(int clubId)
        {
            return Get(clubId).ClubStaffs.AsQueryable();
        }

        public IQueryable<ClubStaff> GetActiveStaffs(int clubId)
        {
            return GetActiveStaffs(Get(clubId));
        }

        public IQueryable<ClubStaff> GetActiveStaffs(Club club)
        {
            return club.ClubStaffs.Where(s => !s.IsDeleted && s.Status == StaffStatus.Active && !s.IsFrozen).AsQueryable();
        }


        public List<ClubStaff> GetStaffsByUserName(string userName, RoleCategory? currentUserRole = null)
        {
            var clubStaffs = new List<ClubStaff>();
            var memberClubStaffs = new List<ClubStaff>();

            clubStaffs = _dataContext.Set<ClubStaff>()
                .Where(s => s.Status == StaffStatus.Active && !s.IsFrozen && !s.IsDeleted && s.JbUserRole.User.UserName.Equals(userName, StringComparison.CurrentCultureIgnoreCase))
                .ToList()
                .Select(s => new ClubStaff
                {
                    Club = new Club()
                    {
                        Id = s.ClubId,
                        Domain = s.Club.Domain,
                        Name = s.Club.Name,
                        ClubType = s.Club.ClubType,
                        IsDeleted = s.Club.IsDeleted,
                        IsInActive = s.Club.IsInActive
                    },
                    ClubId = s.ClubId,
                    Contact = s.Contact,
                    ContactId = s.ContactId,
                    DateOfBackgroundCheck = s.DateOfBackgroundCheck,
                    IsDeleted = s.IsDeleted,
                    JbUserRole = s.JbUserRole,
                    UserRoleId = s.UserRoleId,
                    Programs = s.Programs,
                    Status = s.Status,

                })
                .ToList();

            var isThisUserisStaffOfAPartner = _dataContext.Set<ClubStaff>().Any(s =>  s.Status == StaffStatus.Active && !s.IsFrozen && !s.IsDeleted && s.JbUserRole.User.UserName.Equals(userName, StringComparison.CurrentCultureIgnoreCase) && s.Club.ClubType != null && ((s.Club.ClubType.EnumType == ClubTypesEnum.Partner)));
            var isThisUserisStaffOfADistrict = _dataContext.Set<ClubStaff>().Any(s => s.Status == StaffStatus.Active && !s.IsFrozen && !s.IsDeleted && s.JbUserRole.User.UserName.Equals(userName, StringComparison.CurrentCultureIgnoreCase) && s.Club.ClubType != null && (s.Club.ClubType.EnumType == ClubTypesEnum.SchoolDistrict));

            if (isThisUserisStaffOfAPartner || isThisUserisStaffOfADistrict)
            {
                var parentRoles = new List<string> { RoleCategory.Partner.ToString(), RoleCategory.Admin.ToString(), RoleCategory.Manager.ToString() };

                if (isThisUserisStaffOfADistrict)
                {
                    parentRoles.Add(RoleCategory.OnsiteSupportManager.ToString());
                }

                var parentStaffs = _dataContext.Set<ClubStaff>().Where(s => s.Status == StaffStatus.Active && !s.IsFrozen && !s.IsDeleted && s.JbUserRole.User.UserName.Equals(userName, StringComparison.CurrentCultureIgnoreCase) && parentRoles.Contains(s.JbUserRole.Role.Name) && s.Club.ClubType != null && ((s.Club.ClubType.EnumType == ClubTypesEnum.Partner || s.Club.ClubType.EnumType == ClubTypesEnum.SchoolDistrict))).ToList();

                foreach (var paerntStaff in parentStaffs)
                {
                    if (paerntStaff != null)
                    {
                        var partner = paerntStaff.Club;

                        var partnerStaff = partner.ClubStaffs.Where(s => s.Status == StaffStatus.Active && !s.IsFrozen && !s.IsDeleted).AsQueryable().AsNoTracking().Single(s => s.JbUserRole.User.UserName.Equals(userName));

                        var members = GetRelatedClubs(partner.Id).AsQueryable().AsNoTracking().ToList();

                        foreach (var member in members)
                        {
                            if (!clubStaffs.Any(s => s.ClubId == member.Id && s.JbUserRole.User.UserName.Equals(userName) && parentRoles.Contains(s.JbUserRole.Role.Name)))
                            {
                                var role = partnerStaff.RoleOnClub == RoleCategory.Partner ? RoleCategory.Admin : partnerStaff.RoleOnClub;

                                var newStaff = new ClubStaff()
                                {
                                    Club = member,
                                    ClubId = member.Id,
                                    Contact = partnerStaff.Contact,
                                    ContactId = partnerStaff.ContactId,
                                    DateOfBackgroundCheck = partnerStaff.DateOfBackgroundCheck,
                                    IsDeleted = partnerStaff.IsDeleted,
                                    JbUserRole = partnerStaff.JbUserRole,
                                    UserRoleId = partnerStaff.UserRoleId,
                                    Programs = null,
                                    Status = partnerStaff.Status,
                                    RoleOnClub = role
                                };

                                memberClubStaffs.Add(newStaff);
                            }
                        }
                    }
                }

            }

            clubStaffs.AddRange(memberClubStaffs);

            if (!clubStaffs.Where(s => s.JbUserRole.User.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase)).Any(s => s.JbUserRole.Role.Name.Equals(RoleCategory.Partner.ToString())))
            {
                clubStaffs.RemoveAll(r => r.Club.ClubType.EnumType == ClubTypesEnum.Partner);
            }

            return clubStaffs;
        }

        public OperationStatus CreateStaff(ClubStaff clubStaff)
        {
            int result = 0;

            var club = Get(clubStaff.ClubId);

            if (!club.ClubStaffs.Where(s => !s.IsDeleted).Any(c => c.JbUserRole.UserId == clubStaff.JbUserRole.UserId))
            {
                club.ClubStaffs.Add(clubStaff);
            }

            result = _dataContext.SaveChanges();

            return new OperationStatus() { Status = result > 0 };
        }

        public OperationStatus CreateUserRole(JbUserRole userRole)
        {
            int result = 0;

            _dataContext.Set<JbUserRole>().Add(userRole);

            if (EnumHelper.ParseEnum<RoleCategory>(userRole.Role.Name) == RoleCategory.Partner && !userRole.User.Roles.Select(r => EnumHelper.ParseEnum<RoleCategory>(r.Role.Name)).Contains(RoleCategory.Admin))
            {
                var adminRoleId = _userProfileBusiness.GetRole(RoleCategory.Admin).Id;
                var adminUserRole = new JbUserRole { UserId = userRole.UserId, RoleId = adminRoleId };
                _dataContext.Set<JbUserRole>().Add(adminUserRole);
            }

            result = _dataContext.SaveChanges();

            return new OperationStatus() { Status = result > 0 };
        }
        public OperationStatus SetStaffActive(ClubStaff clubStaff)
        {
            clubStaff.Status = StaffStatus.Active;

            return Save();
        }

        public bool IsStaffCanChange(ClubStaff clubStaff)
        {
            var result = true;

            if (clubStaff.Club.PartnerId.HasValue)
            {
                return result;
            }

            var clubType = clubStaff.Club.ClubType.EnumType;

            var supperVissiorAdminRole = RoleCategory.Admin;

            if (clubType == ClubTypesEnum.Partner)
            {
                supperVissiorAdminRole = RoleCategory.Partner;
            }

            if (EnumHelper.ParseEnum<RoleCategory>(clubStaff.JbUserRole.Role.Name) == supperVissiorAdminRole)
            {
                var hasClubAnotherAdmin = clubStaff.Club.ClubStaffs.ToList().Any(s => s.Id != clubStaff.Id && !s.IsDeleted && s.Status == StaffStatus.Active && !s.IsFrozen && s.JbUserRole.Role.Name == supperVissiorAdminRole.ToString());

                return hasClubAnotherAdmin;
            }

            return result;
        }

        public bool IsStaffCanChange(int staffId)
        {
            var clubStaff = GetStaff(staffId);

            return IsStaffCanChange(clubStaff);
        }

        public IQueryable<ClubStaff> GetClubInstructors(int clubId)
        {
            return GetStaffs(clubId).Where(c => c.JbUserRole.Role.Name.Equals(RoleCategory.Instructor.ToString()));
        }

        public bool IsInstructorIsInUse(int staffId)
        {
            var result = _programBusiness.GetList().SelectMany(p => p.Instructors).Any(i => i.Id == staffId);

            return result;
        }

        public bool IsStaffIsInUseInAttendance(int staffId)
        {
            var result = _dataContext.Set<ScheduleAttendance>().Any(a => a.ClubStaffId == staffId);

            return result;
        }
        public OperationStatus DeleteStaff(int staffId)
        {
            var staff = _dataContext.Set<ClubStaff>().SingleOrDefault(s => s.Id == staffId);
            staff.IsDeleted = true;
            var result = _dataContext.SaveChanges();

            return new OperationStatus() { Status = result > 0 };
        }

        public int MigrateDefaultSchoolClubFormTemaplates()
        {
            var result = 0;

            var defaultSchoolForm = _formBusiness.GetDefaultSchoolRegistrationForm(true);

            var clubsWithDefaultFormTemplate = GetList().Where(c => c.ClubFormTemplates.Select(ft => ft.Title).ToList().Contains(Constants.W_Default));

            foreach (var club in clubsWithDefaultFormTemplate)
            {
                if (!club.ClubFormTemplates.Select(ft => ft.Title).ToList().Contains(Constants.Default_School_Form))
                {
                    club.ClubFormTemplates.Add(new ClubFormTemplate
                    {
                        ClubId = club.Id,
                        DateCreated = DateTime.UtcNow,
                        FormType = FormType.Registration,
                        JbForm = defaultSchoolForm,
                        Title = Constants.Default_School_Form,
                        DefaultFormType = DefaultFormType.School
                    });
                }
            }

            result = _dataContext.SaveChanges();

            return result;
        }

        public dynamic GetInvoiceSetting(Club club)
        {
            dynamic result = new ExpandoObject();

            bool readFromMember = false;
            Club partner = null;

            var partnerId = club.PartnerId;

            if (partnerId.HasValue)
            {
                partner = club.PartnerClub;
                var partnerSetting = GetPartnerSetting(partner.Id);
                var invoiceSetting = partnerSetting.PortalParameters.ReadInvoiceSettingsfromMember;

                if (invoiceSetting)
                {
                    readFromMember = true;
                }
                if (readFromMember)
                {
                    result.Name = club.Name;
                    result.Site = club.Site;
                    result.logo = false;
                    result.Address = club.ClubLocations.First().PostalAddress.Address;
                    result.Email = club.ContactPersons.FirstOrDefault().Email;
                }
                else
                {
                    result.Name = partner.Name;
                    result.Site = partner.Site;
                    result.logo = true;
                    result.Address = partner.ClubLocations.First().PostalAddress.Address;
                    result.Email = partner.ContactPersons.FirstOrDefault().Email;
                }

            }
            else
            {
                result.Name = club.Name;
                result.Site = club.Site;
                result.logo = false;
                result.Address = club.ClubLocations.First().PostalAddress.Address;
                result.Email = club.ContactPersons.FirstOrDefault().Email;
            }

            return result;
        }

        public bool GetRegistrationFeeSetting(int partnerId)
        {
            var partnerSetting = GetPartnerSetting(partnerId);
            bool readFromMember = partnerSetting.PortalParameters.ReadRegistrationFeeFromMember;

            return readFromMember;
        }
        public dynamic GetHelpInformationSetting(Club club)
        {
            dynamic result = new ExpandoObject();

            //bool readFromMember = false;
            Club partner = null;

            var partnerId = club.PartnerId;

            if (partnerId.HasValue)
            {
                partner = club.PartnerClub;
                if (partner.Domain == "flexacademies")
                {
                    result.Name = club.Name;
                    result.Site = club.Site;
                    result.logo = false;
                    result.Address = club.ClubLocations.First().PostalAddress.Address;
                    result.Email = "info@flexacademies.com";
                    result.Phone = "240-654-0877(DC, MD, VA)";
                    result.PhoneExtension = " 203-408-3900 (CT, NJ, NY)";
                }
                else
                {
                    var partnerSetting = GetPartnerSetting(partner.Id);
                    var HelpInformationFromPartner = partnerSetting.PortalParameters.HelpInformationFromPartner;

                    if (!HelpInformationFromPartner)
                    {
                        result.Name = club.Name;
                        result.Site = club.Site;
                        result.logo = false;
                        result.Address = club.ClubLocations.First().PostalAddress.Address;
                        result.Email = club.ContactPersons.FirstOrDefault().Email;
                        result.Phone = club.ContactPersons.FirstOrDefault().Phone;
                        result.PhoneExtension = club.ContactPersons.FirstOrDefault().PhoneExtension;
                    }
                    else
                    {
                        result.Name = partner.Name;
                        result.Site = partner.Site;
                        result.logo = true;
                        result.Address = partner.ClubLocations.First().PostalAddress.Address;
                        result.Email = partner.ContactPersons.FirstOrDefault().Email;
                        result.Phone = partner.ContactPersons.FirstOrDefault().Phone;
                        result.PhoneExtension = partner.ContactPersons.FirstOrDefault().PhoneExtension;
                    }
                }
            }
            else
            {
                result.Name = club.Name;
                result.Site = club.Site;
                result.logo = false;
                result.Address = club.ClubLocations.First().PostalAddress.Address;
                result.Email = club.ContactPersons.FirstOrDefault().Email;
                result.Phone = club.ContactPersons.FirstOrDefault().Phone;
                result.PhoneExtension = club.ContactPersons.FirstOrDefault().PhoneExtension;
            }

            return result;
        }

        public List<ClubStaff> GetClubsForUser(string userName, RoleCategory? currentUserRole = null)
        {
            var staffs = GetStaffsByUserName(userName, currentUserRole);

            if (staffs.Any())
            {
                staffs.AddRange(GetClubsByMember(userName));

                return staffs.GroupBy(s => s.Club.Id)
                      .Select(group => group.First())
                      .OrderBy(c => c.Club.Name)
                      .ToList();
            }

            return GetClubsByMember(userName).GroupBy(s => s.Club.Id)
                      .Select(group => group.First())
                      .OrderBy(c => c.Club.Name)
                      .ToList();
        }

        public Club GetNotBrandedUserClub(string userName)
        {
            Club result = null;

            var staffs = GetStaffsByUserName(userName).ToList();

            if (staffs.Count() == 1)
            {
                result = staffs.First().Club;

                return result;
            }
            else if (staffs.Count() == 0)
            {
                var clubIds = GetClubsByMember(userName).Select(s => new KeyValuePair<int, string>(s.Id, s.Club.Name)).ToList();

                var lastClubOrder = _orderBusiness.GetLastUserOrder(userName, clubIds.Select(c => c.Key).ToList());

                if (lastClubOrder != null)
                {
                    result = lastClubOrder.Club;
                }
                else
                {
                    result = Get(Constants.DefaultFamilyClubDomain);
                }
            }
            else
            {
                var lastLoggedInClubDomain = _userProfileBusiness.GetLastLoggedinClubDomain(userName);

                if (!string.IsNullOrEmpty(lastLoggedInClubDomain))
                {
                    var staff = staffs.SingleOrDefault(s => s.Club.Domain.Equals(lastLoggedInClubDomain, StringComparison.CurrentCultureIgnoreCase));

                    if (staff != null)
                    {
                        result = staff.Club;
                    }
                    else
                    {
                        result = staffs.Last().Club;
                    }
                }
                else
                {
                    result = staffs.Last().Club;
                }
            }

            return result;
        }

        public List<string> ClubOwnerEmailList(Club club)
        {
            List<string> clubOwnerEmail = new List<string>();


            if (club.Setting != null && club.Setting.NotificationEmails != null && club.Setting.NotificationEmails.Any())
            {
                clubOwnerEmail.AddRange(club.Setting.NotificationEmails);
            }
            else
            {
                clubOwnerEmail.Add(club.ContactPersons.FirstOrDefault().Email);
            }
            return clubOwnerEmail;
        }

        public dynamic GetCatalogSetting(int partnerId)
        {
            dynamic result = new ExpandoObject();
            var partnerSetting = GetPartnerSetting(partnerId);
            var genderSetting = partnerSetting.PortalParameters.ShowGenderForCatalog;
            var priceOptionLable = partnerSetting.PortalParameters.PriceOptionFeeInputBoxLabel;
            var priceoptionNote = partnerSetting.PortalParameters.PriceOptionNote;

            result.ShowGender = genderSetting == true ? true : false;
            result.PriceOptionLable = priceOptionLable;
            result.PriceOptionNote = priceoptionNote;

            return result;
        }

        public Club SetStaffsForNewClub(Club district, IEnumerable<ClubStaff> districtStaffs, List<int> clubStaffIds)
        {
            var listUpdateStaffs = new List<ClubStaff>();
            ClubStaff oldStaff = null;

            foreach (var staffId in clubStaffIds)
            {
                var newStaff = _dataContext.Set<ClubStaff>().SingleOrDefault(s => s.Id == staffId);
                if (districtStaffs != null && districtStaffs.Any())
                {
                    districtStaffs.ToList().ForEach(s => s.IsDeleted = true);
                    oldStaff = districtStaffs.Where(s => s.ContactId == newStaff.ContactId && s.Contact.Title == newStaff.Contact.Title).FirstOrDefault();
                }

                if (oldStaff != null)
                {
                    listUpdateStaffs.Add(oldStaff);
                }
                else
                {
                    district.ClubStaffs.Add(
                   new ClubStaff()
                   {
                       Status = newStaff.Status,
                       ClubId = district.Id,
                       IsDeleted = false,
                       JbUserRole = newStaff.JbUserRole,
                       ContactId = newStaff.ContactId
                   });
                }
            }

            listUpdateStaffs.ForEach(s => s.IsDeleted = false);

            return district;
        }

        public List<Charge> Calculatesurcharge(Club clubInfo)
        {
            List<Charge> surCharges = new List<Charge>();
            if (clubInfo.Charges != null && clubInfo.Charges.Any(c => c.IsDeleted == false && c.Category == ChargeDiscountCategory.Surcharge))
            {
                surCharges.Add(clubInfo.Charges.FirstOrDefault(c => c.IsDeleted == false && c.Category == ChargeDiscountCategory.Surcharge));
            }

            if (clubInfo.PartnerClub != null && clubInfo.PartnerClub.Charges != null && clubInfo.PartnerClub.Charges.Any(c => c.IsDeleted == false && c.Category == ChargeDiscountCategory.PartnerSurcharge))
            {
                surCharges.Add(clubInfo.PartnerClub.Charges.FirstOrDefault(c => c.IsDeleted == false && c.Category == ChargeDiscountCategory.PartnerSurcharge));
            }
            return surCharges;
        }

        public AgendaSettings GetAgendaSettings(Club club)
        {
            AgendaSettings result = null;

            var settings = JsonConvert.DeserializeObject<ClubSetting>(club.SettingSerialized);

            if (settings != null)
            {
                result = settings.Agenda;
            }

            return result;
        }

        public AgendaSettings GetAgendaSettings(int clubId)
        {
            AgendaSettings result = null;

            var club = Get(clubId);

            result = GetAgendaSettings(club);

            return result;
        }


        public OperationStatus SaveAgendaSettings(int clubId, AgendaSettings agendaSettings)
        {
            var result = new OperationStatus();

            var club = Get(clubId);

            if (club.Setting == null)
            {
                club.Setting = new ClubSetting();
            }

            var settings = club.Setting;

            settings.Agenda = agendaSettings;

            club.SettingSerialized = JsonConvert.SerializeObject(settings);

            result = Update(club);

            return result;
        }

        public ClubAttributeValue GetAttribute(Club club, AttributeName attributeName)
        {
            var attribute = GetAttributes(club).SingleOrDefault(s => s.Attribute.Name == attributeName);

            return attribute;
        }

        public List<ClubAttributeValue> GetAttributes(Club club)
        {
            var clubAttributeValues = club.ClubAttributeValues.ToList();

            var clubAttributes = _attributeBusiness.GetList(AttributeReferenceType.Club, club);

            var clubAttributeValueIds = clubAttributeValues.Select(a => a.AttributeId).ToList();

            var attributesWherNotInClubButExistsInClubCustomAttribute = new List<JbAttribute>();

            if (clubAttributeValueIds.Any())
            {
                attributesWherNotInClubButExistsInClubCustomAttribute = clubAttributes.Where(p => !clubAttributeValueIds.Contains(p.Id)).ToList();
            }
            else
            {
                attributesWherNotInClubButExistsInClubCustomAttribute = clubAttributes.ToList();
            }

            var newClubAttributeValues = attributesWherNotInClubButExistsInClubCustomAttribute.Select(a =>
                                       new ClubAttributeValue
                                       {
                                           Attribute = a,
                                           AttributeId = a.Id,
                                           Club = club,
                                           ClubId = club.Id,
                                           Value = null
                                       })
                                          .ToList();

            clubAttributeValues.AddRange(newClubAttributeValues);

            return clubAttributeValues;
        }

        public List<ClubAttributeValue> GetAttributes(int clubId)
        {
            var club = Get(clubId);

            return GetAttributes(club);
        }

        public void SetAttributes(Club club, Dictionary<AttributeName, string> KeyValues, int? partnerId = null)
        {

            if (club.ClubAttributeValues == null)
            {
                club.ClubAttributeValues = new List<ClubAttributeValue>();
            }

            var clubAttributes2 = _attributeBusiness.GetList(AttributeReferenceType.Club, club.Id, partnerId);

            foreach (var item in KeyValues)
            {
                var clubAttribute = club.ClubAttributeValues.SingleOrDefault(p => p.Attribute.Name == item.Key);

                if (clubAttribute != null)
                {
                    clubAttribute.Value = item.Value;
                }
                else
                {
                    var clubAttribute2 = clubAttributes2.SingleOrDefault(s => s.Name == item.Key);

                    if (clubAttribute2 != null)
                    {
                        club.ClubAttributeValues.Add(new ClubAttributeValue
                        {
                            Attribute = clubAttribute2,
                            AttributeId = clubAttribute2.Id,
                            Club = club,
                            ClubId = club.Id,
                            Value = item.Value
                        });
                    }
                }
            }
        }
        public IQueryable<ContactPerson> GetDistrictContact(int districtId)
        {
            return _clubRepository.Get(c => c.Id == districtId).ContactPersons.AsQueryable();
        }
        public IQueryable<ClubSubsidy> GetAllSubsidiesDistrict(int districtId)
        {
            return _dataContext.Set<ClubSubsidy>().Where(c => c.ClubId == districtId);
        }

        public OperationStatus DeleteDistrictSubSidies(List<ClubSubsidy> clubSubsidies)
        {
            _dataContext.Set<ClubSubsidy>().RemoveRange(clubSubsidies);
            return _clubRepository.Save();
        }

        public void UpdateClubs(List<Club> clubs)
        {
            foreach (var club in clubs)
            {
                club.IsSettingChanged = true;
                Update(club);
            }

        }

        public string GetClubNotification(Club club, TypeClubNotification typeNotification)
        {
            string result = string.Empty;

            switch (typeNotification)
            {
                case TypeClubNotification.Confirmation:
                    {
                        result = club.Setting.Notifications.Email.ContentConfirmationEmail;
                    }
                    break;
                case TypeClubNotification.Cancellation:
                    {
                        result = club.Setting.Notifications.Email.ContentCancellationEmail;
                    }
                    break;
                case TypeClubNotification.Absentee:
                    {
                        result = club.Setting.Notifications.Email.ContentAbsentee;
                    }
                    break;
                case TypeClubNotification.Reports:
                    {
                        result = club.Setting.Notifications.Report.DependentCareDescription;
                    }
                    break;
            }


            return result;

        }

        public Charge GetApplicationFee(Club club)
        {
            var nowDateTime = GetClubDateTime(club.Domain, DateTime.UtcNow);
            var charge = new Charge();

            if (club.PartnerId.HasValue)
            {
                var partnerId = club.PartnerId;
                var readRegistrationFeeFromMember = GetRegistrationFeeSetting(partnerId.Value);

                if (readRegistrationFeeFromMember)
                {
                    charge = club.Charges.SingleOrDefault(c => c.Category == ChargeDiscountCategory.ApplicationFee && !c.IsDeleted);
                }
                else
                {
                    charge = club.PartnerClub.Charges.SingleOrDefault(c => c.Category == ChargeDiscountCategory.PartnerApplicationFee && !c.IsDeleted);
                }

            }
            else
            {
                charge = club.Charges.SingleOrDefault(c => c.Category == ChargeDiscountCategory.ApplicationFee && !c.IsDeleted);
            }

            return charge;
        }

        public Charge GetSeasonApplicationFee(Season season)
        {
            var charge = season.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ApplicationFee);

            return charge;
        }
        public List<ClubStaff> GetAddablePartnerStaffsForMember(int clubId)
        {
            var result = new List<ClubStaff>();
            var club = Get(clubId);

            Club partner = null;

            if (club.PartnerId.HasValue)
            {
                partner = club.PartnerClub;
            }

            var managableRoles = _actionBusiness.GetManageableDasboardRolesWithRole(RoleCategory.Admin, club.ClubType);

            managableRoles = managableRoles.Where(r => r != RoleCategory.Admin && r != RoleCategory.Manager && r != RoleCategory.Partner).ToList();

            result = GetActiveStaffs(partner).Where(s => managableRoles.Select(r => r.ToString()).ToList().Contains(s.JbUserRole.Role.Name)).ToList();

            return result;
        }

        public OperationStatus AddPartnerStaffToMember(int clubId, int staffId)
        {
            var result = new OperationStatus();

            var club = Get(clubId);

            var partnerStaff = _dataContext.Set<ClubStaff>().Single(s => s.Id == staffId);

            var alreadyHasThisStaff = club.ClubStaffs.Any(s => !s.IsDeleted && s.JbUserRole.User.UserName.Equals(partnerStaff.JbUserRole.User.UserName, StringComparison.OrdinalIgnoreCase));

            if (!alreadyHasThisStaff)
            {
                club.ClubStaffs.Add(new ClubStaff
                {
                    ClubId = clubId,
                    ContactId = partnerStaff.ContactId,
                    JbUserRole = new JbUserRole
                    {
                        RoleId = partnerStaff.JbUserRole.RoleId,
                        UserId = partnerStaff.JbUserRole.UserId
                    },
                    Status = StaffStatus.Active,
                });
            }

            result = Save();

            return result;
        }

        public bool AdminIsFromPartner(string userName, int clubId)
        {
            var club = Get(clubId);
            Club partnerClub;

            if (club.PartnerId.HasValue)
            {
                partnerClub = club.PartnerClub;
            }
            else
            {
                return false;
            }

            if (_userProfileBusiness.IsUserInRole(userName, RoleCategory.Support))
            {
                return true;
            }

            return GetActiveStaffs(partnerClub.Id).Any(c => !c.IsDeleted && c.JbUserRole.User.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase));
        }

        public bool IsNotPartnerRoleInPartnerClub(int staffId)
        {
            var staff = GetStaff(staffId);

            return IsNotPartnerRoleInPartnerClub(staff);
        }

        public bool IsNotPartnerRoleInPartnerClub(string userName, int clubId)
        {
            var staff = GetStaff(userName, clubId);

            return IsNotPartnerRoleInPartnerClub(staff);
        }

        public bool IsNotPartnerRoleInPartnerClub(ClubStaff staff)
        {
            var club = staff.Club;

            var clubType = club.ClubType.EnumType;
            var staffRole = EnumHelper.ParseEnum<RoleCategory>(staff.JbUserRole.Role.Name);

            if (clubType == ClubTypesEnum.Partner && staffRole != RoleCategory.Partner)
            {
                return true;
            }

            return false;
        }

        public string GetClubLogo(Club club)
        {
            var result = "";

            if (club.IsSchool && club.PartnerId.HasValue)
            {
                result = GetClubLogoURL(club.PartnerClub.Domain, club.PartnerClub.Logo);
            }
            else
            {
                result = GetClubLogoURL(club.Domain, club.Logo);
            }
            return result;
        }

        public decimal GetUserCredit(Club club, int userId, bool isTestMode)
        {
            var clubUser = club.Users.SingleOrDefault(u => u.UserId == userId);

            if (clubUser == null)
            {
                return 0;
            }

            if (isTestMode)
            {
                return clubUser.TestCredit;
            }

            return clubUser.Credit;
        }

        public OperationStatus Save()
        {
            return _clubRepository.Save();
        }

        public List<dynamic> GetMemeberClubs(string clubDomain, string userName, RoleCategory? roleCategory = null)
        {
            var result = new List<dynamic>();

            var clubs = GetClubsForUser(userName, roleCategory).Where(c => !c.Club.Domain.Equals(clubDomain, StringComparison.OrdinalIgnoreCase)).ToList();

            if (clubs.Any())
            {
                foreach (var item in clubs)
                {
                    dynamic resultItem = new ExpandoObject();

                    resultItem.Name = item.Club.Name;
                    resultItem.Domain = item.Club.Domain;
                    resultItem.Role = item.JbUserRole.Role.Name;

                    result.Add(resultItem);
                }
            }

            return result;
        }

        public IQueryable<Club> GetAllSchoolsAndProviders()
        {
            var result =
                 _clubRepository.GetList()
                     .Where(c => c.ClubType.EnumType != ClubTypesEnum.Partner && c.Client.PricePlan.Type != PricePlanType.FreeTrial ||
                     c.ClubType.ParentType.EnumType == ClubTypesEnum.School || c.ClubType.ParentId.Value == (int)ClubTypesEnum.Provider);

            return result;
        }

        public List<SelectListItem> GetKeyValueSchoolsAndProvider()
        {
            var result = GetAllSchoolsAndProviders().Select(s => new SelectListItem() { Text = s.Name, Value = s.Id.ToString() })
                    .OrderBy(s => s.Text).ToList();

            result.AddItemToFirst(DefaultDropdownItem.All);

            return result;
        }
        public List<SelectListItem> GetAllPartners()
        {
            var result = new List<SelectListItem>();

            result =
              _clubRepository.GetList()
                  .Where(c => c.ClubType.EnumType == ClubTypesEnum.Partner)
                  .Select(c => new SelectListItem { Text = c.Name, Value = c.Id.ToString() })
                  .OrderBy(c => c.Text).ToList();

            result.AddItemToFirst(DefaultDropdownItem.All);

            return result;
        }

        public List<SelectKeyValue<int?>> GetPartnerMembers(int partnerId, ClubBaseInfoModel currentClub, string notSelectedOption = "All")
        {
            var result = new List<SelectKeyValue<int?>>();
            var members = GetRelatedClubs(partnerId);

            result.Add(new SelectKeyValue<int?>() { Text = notSelectedOption, Value = null });

            result.Add(new SelectKeyValue<int?>() { Text = currentClub.Name, Value = currentClub.Id, Group = "Partner" });

            result.AddRange(members.ToList().OrderBy(o => o.Name).Select(m =>
                          new SelectKeyValue<int?>
                          {
                              Group = m.IsSchool ? "School" : (m.IsProvider ? "Provider" : "School district"),
                              Text = m.Name,
                              Value = m.Id
                          })
                           .ToList());

            return result;
        }


        public List<SelectKeyValue<string>> GetStaffTitles(List<SelectKeyValue<string>> staffTitles)
        {
            var staffTitleSorted = new List<SelectKeyValue<string>>();
            //TODO: Other is staff type, we have constants for that?
            var titles = staffTitles.Where(t => t.Text != "Other").OrderBy(t => t.Text);

            staffTitleSorted.AddRange(titles);
            staffTitleSorted.Add(staffTitles.FirstOrDefault(t => t.Text == "Other"));

            return staffTitleSorted;
        }

        public List<SelectKeyValue<int>> GetListOfClubStaffs(string clubDomain)
        {
            return Get(clubDomain).ClubStaffs.Select(c => new SelectKeyValue<int>() { Text = c.JbUserRole.User.UserName, Value = c.UserRoleId }).ToList();
        }

        public bool IsUserBelongToTheClub(int clubId, int userId)
        {
            return Get(clubId).Users.Any(c => c.UserId == userId);
        }

        public CurrencyCodes GetClubCurrency(string clubDomain)
        {
            if (!string.IsNullOrEmpty(clubDomain))
            {
                var club = Get(clubDomain);
                if (club != null)
                {
                    return club.Currency;
                }
            }
            return CurrencyCodes.USD;
        }

        public List<SelectKeyValue<int>> GetClubTypes(string parentName = null)
        {
            if (!string.IsNullOrWhiteSpace(parentName))
            {
                return GetClubTypeList()
                    .Where(c => c.ParentId.HasValue && c.ParentType != null &&
                                c.ParentType.Name.ToLower() == parentName.ToLower())
                    .ToList().Select(x => new SelectKeyValue<int>() { Text = x.Name, Value = x.Id }).ToList();
            }

            return GetClubTypeList()
                .Where(e => e.ParentId != null)
                .ToList().Select(x => new SelectKeyValue<int>
                    ()
                { Group = x.ParentType.Name, Text = string.Format(Constants.F_CategoryFormat, x.ParentType.Name, x.Name), Value = x.Id }).ToList();
        }

        public List<SelectKeyValue<int>> GetClubTypesForSchool()
        {
            return GetClubTypeListForSchools()
                .Where(e => e.ParentId != null)
                .ToList().Select(x => new SelectKeyValue<int>
                    ()
                { Text = string.Format(Constants.F_CategoryFormat, x.ParentType.Name, x.Name), Value = x.Id }).ToList();
        }

        public List<SelectKeyValue<int>> GetClubTypesForProvider()
        {
            return GetClubTypeListForProvider()
                .Where(e => e.ParentId != null)
                .ToList().Select(x => new SelectKeyValue<int>
                    ()
                { Text = string.Format(Constants.F_CategoryFormat, x.ParentType.Name, x.Name), Value = x.Id }).ToList();


        }

        public string GetFreeTrialMessage(DateTime createDate)
        {

            var remainDays = Constants.FreeTrialDays - ((DateTime.UtcNow.Date - createDate.Date).TotalDays);
            if (remainDays < 0 || remainDays == 0)
            {
                remainDays = 0;
                return Constants.FreeTrialExpiredMessage;
            }
            else if (remainDays == Constants.FreeTrialDays && createDate > DateTime.UtcNow.AddMinutes(-10))
            {
                return Constants.FreeTrialStartedMessage;
            }
            else
            {
                return string.Format(Constants.FreeTrialMessage, remainDays);
            }
        }

        public string GetAccountOverviewMessage(PricePlan priceplan, DateTime createDate, CurrencyCodes currency)
        {
            if (priceplan.Type == PricePlanType.FreeTrial)
            {
                return GetFreeTrialMessage(createDate);
            }
            if (priceplan.MonthlyCharges > 0)
            {
                return string.Format("We bill you automatically on a monthly basis and email you the invoice.");// Your next invoice will be on {0} for the amount of {1}.",DateTime.UtcNow.ToString(Constants.DefaultDateFormat), Utilities.FormatCurrencyWithPenny(priceplan.MonthlyCharges, currency));
            }
            return "";
        }


        public bool IsExpressPaymentEnabledInOfflineMode(Club club)
        {
            bool result;

            var partnerId = club.PartnerId;

            if (partnerId.HasValue)
            {
                var partner = club.PartnerClub;
                var partnerSetting = GetPartnerSetting(partner.Id);
                result = partnerSetting.EnableExpressPaymentInOfflineMode;
            }
            else
            {
                var clubSetting = GetClubSetting(club.Id);
                result = clubSetting.EnableExpressPaymentInOfflineMode;
            }

            return result;

        }


        public bool EnableViewParentDashboard(Club club)
        {
            bool result;

            var partnerId = club.PartnerId;

            if (partnerId.HasValue)
            {
                var partner = club.PartnerClub;
                var partnerSetting = GetPartnerSetting(partner.Id);
                result = partnerSetting.EnableViewParentDashboard;
            }
            else
            {
                var clubSetting = GetClubSetting(club.Id);
                result = clubSetting.EnableViewParentDashboard;
            }

            return result;

        }

        public bool SendMessageToProviderOnScheduleSession(int partnerId)
        {
            var partnerSettings = GetPartnerSetting(partnerId);

            return partnerSettings.Notifications != null ? partnerSettings.Notifications.Catalog.SendMessageToProviderOnClassSchedule : false;
        }


        #region RasHardcode
        public bool IsRasOrMembers(string domain)
        {
            var rasDomain = Constants.RightAtSchoolDomain;

            if (domain.Equals(rasDomain, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            var club = Get(domain);

            if (club.PartnerId.HasValue && club.PartnerClub.Domain.Equals(rasDomain, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            return false;
        }
        #endregion
    }
}
