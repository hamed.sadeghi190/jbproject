﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element
{
    class ProviderElements : BaseElements
    {
        public ProviderElements(IWebDriver driver)
            : base(driver)
        {

        }
        #region Header
        public IWebElement BackLink
        {
            get
            {
                return Driver.FindElement(By.XPath("//a[@class='icon-arrow-left']"));
            }
        }
        public IWebElement PageTitle
        {
            get
            {
                return Driver.FindElement(By.XPath("//h1[@class='ng-scope']"));
            }
        }

        public IWebElement AddProviderLink
        {
            get
            {
                return Driver.FindElement(By.XPath("//a[@title='Add provider']"));
            }
        }
        #endregion Header

        #region Filter
        public IWebElement FilterType
        {
            get
            {
                return Driver.FindElement(By.XPath("//select[@ng-model='filter.FilterTypes']"));
            }
        }
        public IWebElement NameOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='Name']"));
            }
        }
        public IWebElement DomainOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='Domain']"));
            }
        }
        public IWebElement ExpirationDateOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='Expiration date']"));
            }
        }
        public IWebElement SearchTerm
        {
            get
            {
                return Driver.FindElement(By.Id("searchClubName"));
            }
        }
        public IWebElement StartDateFilter
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@id='filter.startDate']"));
            }
        }
        public IWebElement EndDateFilter
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@id='filter.endDate']"));
            }
        }
        public IWebElement SearchButton
        {
            get
            {
                return Driver.FindElement(By.XPath("//i[@class='jbi-search']"));
            }
        }
        public IWebElement ActiveTypeFilter
        {
            get
            {
                return Driver.FindElement(By.XPath("//select[@ng-model='filter.FilterTypes']"));
            }
        }
        public IWebElement AllOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='All']"));
            }
        }
        public IWebElement ActiveOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='Active']"));
            }
        }
        public IWebElement InactiveOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='Inactive']"));
            }
        }
        public IWebElement ClearFilter
        {
            get
            {
                IList<IWebElement> btns = Driver.FindElements(By.TagName("button"));
                return btns.First(btn => btn.Text == "Clear filters");
            }
        }

        #endregion Filter

        #region GridRow1
        public IWebElement Row1Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[1]/span"));
            }
        }
        public IWebElement Row1Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[2]/span"));
            }
        }
        public IWebElement Row1Admin
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[3]/span"));
            }
        }
        public IWebElement Row1Phone
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[4]/span"));
            }
        }
        public IWebElement Row1Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[5]/span"));
            }
        }
        public IWebElement Row1ExpirationDate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[6]/span"));
            }
        }
        public IWebElement Row1CommRate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[7]/span"));
            }
        }
        public IWebElement Row1County
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[8]/span"));
            }
        }
        public IWebElement Row1Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[9]/ul/li/span"));
            }
        }
        public IWebElement Row1LoginAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[9]/ul/li/div/ul/li[1]/span"));
            }
        }
        public IWebElement Row1EditAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[9]/ul/li/div/ul/li[2]/span"));
            }
        }
        public IWebElement Row1DeleteAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[9]/ul/li/div/ul/li[4]/span"));
            }
        }
        public IWebElement Row1ResetPasswordAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[9]/ul/li/div/ul/li[5]/span"));
            }
        }
        public IWebElement Row1EditUserAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[9]/ul/li/div/ul/li[7]/span"));
            }
        }
        #endregion GridRow1

        #region GridRow2
        public IWebElement Row2Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[1]/span"));
            }
        }
        public IWebElement Row2Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[2]/span"));
            }
        }
        public IWebElement Row2Admin
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[3]/span"));
            }
        }
        public IWebElement Row2Phone
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[4]/span"));
            }
        }
        public IWebElement Row2Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[5]/span"));
            }
        }
        public IWebElement Row2ExpirationDate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[6]/span"));
            }
        }
        public IWebElement Row2CommRate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[7]/span"));
            }
        }
        public IWebElement Row2County
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[8]/span"));
            }
        }
        public IWebElement Row2Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[9]/ul/li/span"));
            }
        }
        public IWebElement Row2LoginAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[9]/ul/li/div/ul/li[1]/span"));
            }
        }
        public IWebElement Row2EditAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[9]/ul/li/div/ul/li[2]/span"));
            }
        }
        public IWebElement Row2DeleteAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[9]/ul/li/div/ul/li[4]/span"));
            }
        }
        public IWebElement Row2ResetPasswordAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[9]/ul/li/div/ul/li[5]/span"));
            }
        }
        public IWebElement Row2EditUserAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[9]/ul/li/div/ul/li[7]/span"));
            }
        }
        #endregion GridRow2

        #region GridRow3
        public IWebElement Row3Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[1]/span"));
            }
        }
        public IWebElement Row3Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[2]/span"));
            }
        }
        public IWebElement Row3Admin
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[3]/span"));
            }
        }
        public IWebElement Row3Phone
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[4]/span"));
            }
        }
        public IWebElement Row3Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[5]/span"));
            }
        }
        public IWebElement Row3ExpirationDate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[6]/span"));
            }
        }
        public IWebElement Row3CommRate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[7]/span"));
            }
        }
        public IWebElement Row3County
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[8]/span"));
            }
        }
        public IWebElement Row3Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[9]/ul/li/span"));
            }
        }
        public IWebElement Row3LoginAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[9]/ul/li/div/ul/li[1]/span"));
            }
        }
        public IWebElement Row3EditAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[9]/ul/li/div/ul/li[2]/span"));
            }
        }
        public IWebElement Row3DeleteAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[9]/ul/li/div/ul/li[4]/span"));
            }
        }
        public IWebElement Row3ResetPasswordAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[9]/ul/li/div/ul/li[5]/span"));
            }
        }
        public IWebElement Row3EditUserAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[9]/ul/li/div/ul/li[7]/span"));
            }
        }
        #endregion GridRow3
    }
}
