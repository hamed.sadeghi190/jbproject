GO

/****** Object:  Table [dbo].[CatalogItems]    Script Date: 3/3/2016 5:56:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CatalogItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[MaterialsNeeded] [nvarchar](500) NULL,
	[AttendeeRestriction_MinAge] [int] NULL,
	[AttendeeRestriction_MaxAge] [int] NULL,
	[AttendeeRestriction_MinGrade] [int] NULL,
	[AttendeeRestriction_MaxGrade] [int] NULL,
	[AttendeeRestriction_Gender] [int] NULL,
	[AttendeeRestriction_RestrictionType] [tinyint] NOT NULL,
	[MetaData_DateCreated] [datetime] NOT NULL,
	[MetaData_DateUpdated] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[ClubId] [int] NOT NULL,
	[ImageFileName] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Catalogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[CatalogItems]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Catalogs_dbo.Clubs_ClubId] FOREIGN KEY([ClubId])
REFERENCES [dbo].[Clubs] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[CatalogItems] CHECK CONSTRAINT [FK_dbo.Catalogs_dbo.Clubs_ClubId]
GO







/****** Object:  Table [dbo].[CatalogSettings]    Script Date: 3/3/2016 6:00:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CatalogSettings](
	[Id] [int] NOT NULL,
	[VirtusCertified] [bit] NOT NULL,
	[CatalogSearchTags_BeforeSchool] [bit] NOT NULL,
	[CatalogSearchTags_AfterSchool] [bit] NOT NULL,
	[CatalogSearchTags_PreSchools] [bit] NOT NULL,
	[CatalogSearchTags_ElementarySchools] [bit] NOT NULL,
	[CatalogSearchTags_MiddleSchools] [bit] NOT NULL,
	[CatalogSearchTags_HighSchools] [bit] NOT NULL,
	[CatalogSearchTags_BannerImage] [nvarchar](max) NULL,
	[WeeklyEmail] [bit] NOT NULL,
	[FairFaxCounty] [bit] NOT NULL,
	[CatalogSearchTags_NoGlobalSearch] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[CatalogSettings] ADD  DEFAULT ((0)) FOR [CatalogSearchTags_BeforeSchool]
GO

ALTER TABLE [dbo].[CatalogSettings] ADD  DEFAULT ((0)) FOR [CatalogSearchTags_AfterSchool]
GO

ALTER TABLE [dbo].[CatalogSettings] ADD  DEFAULT ((0)) FOR [CatalogSearchTags_PreSchools]
GO

ALTER TABLE [dbo].[CatalogSettings] ADD  DEFAULT ((0)) FOR [CatalogSearchTags_ElementarySchools]
GO

ALTER TABLE [dbo].[CatalogSettings] ADD  DEFAULT ((0)) FOR [CatalogSearchTags_MiddleSchools]
GO

ALTER TABLE [dbo].[CatalogSettings] ADD  DEFAULT ((0)) FOR [CatalogSearchTags_HighSchools]
GO

ALTER TABLE [dbo].[CatalogSettings] ADD  DEFAULT ((0)) FOR [WeeklyEmail]
GO

ALTER TABLE [dbo].[CatalogSettings] ADD  DEFAULT ((0)) FOR [FairFaxCounty]
GO

ALTER TABLE [dbo].[CatalogSettings] ADD  DEFAULT ((0)) FOR [CatalogSearchTags_NoGlobalSearch]
GO

ALTER TABLE [dbo].[CatalogSettings]  WITH CHECK ADD  CONSTRAINT [fk_Clubs] FOREIGN KEY([Id])
REFERENCES [dbo].[Clubs] ([Id])
GO

ALTER TABLE [dbo].[CatalogSettings] CHECK CONSTRAINT [fk_Clubs]
GO




/****** Object:  Table [dbo].[CatalogItemOptions]    Script Date: 3/3/2016 6:03:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CatalogItemOptions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Value] [nvarchar](max) NULL,
	[CatalogItemId] [int] NULL,
	[Type] [int] NOT NULL,
	[CatalogSettingId] [int] NULL,
	[Group] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.CatalogItemOptions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[CatalogItemOptions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CatalogItemOptions_dbo.CatalogItems_CatalogItemId] FOREIGN KEY([CatalogItemId])
REFERENCES [dbo].[CatalogItems] ([Id])
GO

ALTER TABLE [dbo].[CatalogItemOptions] CHECK CONSTRAINT [FK_dbo.CatalogItemOptions_dbo.CatalogItems_CatalogItemId]
GO

ALTER TABLE [dbo].[CatalogItemOptions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CatalogItemOptions_dbo.CatalogSettings_CatalogSettingId] FOREIGN KEY([CatalogSettingId])
REFERENCES [dbo].[CatalogSettings] ([Id])
GO

ALTER TABLE [dbo].[CatalogItemOptions] CHECK CONSTRAINT [FK_dbo.CatalogItemOptions_dbo.CatalogSettings_CatalogSettingId]
GO







/****** Object:  Table [dbo].[CatalogItemPriceOptions]    Script Date: 3/3/2016 6:04:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CatalogItemPriceOptions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[DurationTitle] [nvarchar](max) NULL,
	[CatalogItemId] [int] NULL,
	[CatalogSettingId] [int] NULL,
 CONSTRAINT [PK_dbo.CatalogItemPriceOptions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[CatalogItemPriceOptions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CatalogItemPriceOptions_dbo.CatalogItems_CatalogItemId] FOREIGN KEY([CatalogItemId])
REFERENCES [dbo].[CatalogItems] ([Id])
GO

ALTER TABLE [dbo].[CatalogItemPriceOptions] CHECK CONSTRAINT [FK_dbo.CatalogItemPriceOptions_dbo.CatalogItems_CatalogItemId]
GO

ALTER TABLE [dbo].[CatalogItemPriceOptions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CatalogItemPriceOptions_dbo.CatalogSettings_CatalogSettingId] FOREIGN KEY([CatalogSettingId])
REFERENCES [dbo].[CatalogSettings] ([Id])
GO

ALTER TABLE [dbo].[CatalogItemPriceOptions] CHECK CONSTRAINT [FK_dbo.CatalogItemPriceOptions_dbo.CatalogSettings_CatalogSettingId]
GO








/****** Object:  Table [dbo].[CategoryCatalogItems]    Script Date: 3/3/2016 6:06:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CategoryCatalogItems](
	[CatalogItem_Id] [int] NOT NULL,
	[Category_Id] [int] NOT NULL,
 CONSTRAINT [PK_dbo.CategoryCatalogItems] PRIMARY KEY CLUSTERED 
(
	[Category_Id] ASC,
	[CatalogItem_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CategoryCatalogItems]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CatalogItemCategories_dbo.CatalogItems_Catalog_Id] FOREIGN KEY([CatalogItem_Id])
REFERENCES [dbo].[CatalogItems] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[CategoryCatalogItems] CHECK CONSTRAINT [FK_dbo.CatalogItemCategories_dbo.CatalogItems_Catalog_Id]
GO

ALTER TABLE [dbo].[CategoryCatalogItems]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CatalogItemsCategories_dbo.Categories_Category_Id] FOREIGN KEY([Category_Id])
REFERENCES [dbo].[Categories] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[CategoryCatalogItems] CHECK CONSTRAINT [FK_dbo.CatalogItemsCategories_dbo.Categories_Category_Id]
GO














