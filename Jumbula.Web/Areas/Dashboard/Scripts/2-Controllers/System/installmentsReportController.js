﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller("installmentsReportController", ["$scope", "$http", "$log", "$stateParams", "$state", "customReportService", "cfpLoadingBar", "jbToast", "jbConfirmation", "$timeout", function ($scope, $http, $log, $stateParams, $state, customReportService, cfpLoadingBar, jbToast, jbConfirmation, $timeout) {
        window.app.globalObjects.pageClass = "seasons-view order";
        window.app.globalObjects.initializeView();

        $scope.baseUrl = window.location.origin + "/Dashboard/System";
        $scope.IsClearFilter = false;
        $scope.stateParams = $stateParams;
        var timer = false;
        $scope.MC.stateParams = $stateParams;
        $scope.Model = {};

        $scope.searchTerm = "";
        $scope.SearchTypes = [{ Text: "Participant name", Value: "Name" }, { Text: "Family email", Value: "Email" }, { Text: "Confirmation ID", Value: "ConfirmationId" }, { Text: "Error message", Value: "ErrorMessage" }];
        $scope.SearchType = "Name";

        $scope.Model.Status = "Unpaid";

        $scope.Model.SchoolId = 0;
        $scope.Model.SeasonId = 0;
        $scope.Model.ProgramId = 0;
        $scope.HasProgramSchedules = false;

        $http.get($scope.baseUrl + '/GetRangeInstallmentDate').success(function (data) {
            $scope.Model.TempEndDate = data.EndDate;
            $scope.Model.EndDate = data.EndDate;
     
            prepareGrid();

        }).error(function (data) {
            jbToast.error(data.Message);
            });

        $http.get($scope.baseUrl + "/GetAllPartners")
            .success(function (data) {

                $scope.AllPartners = data;
            });
        $http.get($scope.baseUrl + "/GetSchoolsAndProviders")
            .success(function (data) {

                $scope.AllSchools = data;
            });

        $scope.updateChosenScope = function () {
            $("select[chosen]").trigger("chosen:updated");
        };

        $scope.$watch("AllPartners", function () {
            if (timer) {
                $timeout.cancel(timer);
            }
            timer = $timeout(function () {
                $scope.updateChosenScope();
            }, 1500);
        });

        $scope.$watch("AllSchools", function () {
            if (timer) {
                $timeout.cancel(timer);
            }
            timer = $timeout(function () {
                $scope.updateChosenScope();
            }, 1500);
        });


        $http.get($scope.baseUrl + '/GetStripeErrorMessage')
            .success(function (data) {
                $scope.StripeErrorMessage = data;
            });

        $scope.fillSeasonsData = function (schoolId) {

            $http.get($scope.baseUrl + '/GetClubSeasons', { params: { clubId: schoolId, includeAllOption: true  } }).success(function (data) {  ///GetClubSeasonsProvider delete

                $scope.AllSeasons = data;
                $scope.Model.schoolId = schoolId;

            }).error(function (data) {
                jbToast.error(data.Message);
            });
        }

        $scope.$watch("AllSeasons", function () {
            if (timer) {
                $timeout.cancel(timer);
            }
            timer = $timeout(function () {
                $scope.updateChosenScope();
            }, 1500);
        });

        $scope.fillProgramsData = function (seasonId) {

            if (seasonId == null || seasonId == 0) {
                $scope.AllPrograms = [];
                return;
            }
            $http.get($scope.baseUrl + '/GetSeasonPrograms', { params: { seasonId: seasonId } }).success(function (data) {

                $scope.AllPrograms = data;

            }).error(function (data) {
                jbToast.error(data.Message);
            });
        }

        $scope.fillProgramSchedulesData = function (programId) {

            $http.get($scope.baseUrl + '/GetProgramSchedules', { params: { programId: programId } }).success(function (data) {

                $scope.AllProgramSchedules = data.ProgramSchedules;
                $scope.HasProgramSchedules = data.HasProgramSchedules;

            }).error(function (data) {
                jbToast.error(data.Message);
            });
        }

        $scope.$watch("AllPrograms", function () {
            if (timer) {
                $timeout.cancel(timer);
            }
            timer = $timeout(function () {
                $scope.updateChosenScope();
            }, 1500);
        });

        $scope.$watch("AllProgramSchedules", function () {
            if (timer) {
                $timeout.cancel(timer);
            }
            timer = $timeout(function () {
                $scope.updateChosenScope();
            }, 1500);
        });

        var prepareGrid = function () {
            $scope.mainGridDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport:
                    {
                        read: {
                            url: $scope.baseUrl + "/GetInstallments",
                            type: "POST",
                            data: function () {
                                return {
                                    partnerId: $scope.Model.PartnerId,
                                    clubId: $scope.Model.SchoolId,
                                    seasonId: $scope.Model.SeasonId,
                                    programId: $scope.Model.ProgramId,
                                    programScheduleId: $scope.Model.ProgramScheduleId,
                                    Status: $scope.Model.Status,
                                    orderMode: $scope.Model.OrderMode,
                                    start: $scope.Model.StartDate,
                                    mode: $scope.Model.Mode,
                                    endDate: $scope.Model.EndDate,
                                    searchType: $scope.SearchType,
                                    firstName: $scope.Model.firstName,
                                    lastName: $scope.Model.lastName,
                                    term: $scope.Model.searchTerm,
                                    transactionStartDate: $scope.Model.TransactionStartDate,
                                    transactionEndDate: $scope.Model.TransactionEndDate,
                                    customeStripeErrorMessage: $scope.Model.customeStripeErrorMessage,

                                };
                            }
                        }

                    },
                schema: {
                    total: "total",
                    data: function (response) {

                        $scope.TotalPastInstallment = response.TotalPastInstallment;
                        $scope.TotalAmountPastInstallment = response.TotalAmountPastInstallment;
                        $scope.TotalPaidAmountPastInstallment = response.TotalPaidAmountPastInstallment;
                        $scope.TotalBalancePastInstallment = response.TotalBalancePastInstallment;
                        return response.data;
                    }
                },
                sort:
                    [{
                        field: "InstallmentDate",
                        dir: "desc"
                    }],
                serverSorting: true,
                serverPaging: true,
                pageSize: 100,
                resizable: true
            });

            $scope.mainGridOptions = {
                excel: {
                    allPages: true,
                    fileName: "Installments_Report.xlsx",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.push({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.push({
                        cells: [{
                            value: "Total number of installments: " + $scope.TotalPastInstallment
                        }]
                    });
                    rows.push({
                        cells: [{
                            value: "Total amount: " + $scope.TotalAmountPastInstallment
                        }]
                    });
                    rows.push({
                        cells: [{
                            value: "Total paid amount: " + $scope.TotalPaidAmountPastInstallment
                        }]
                    });
                    rows.push({
                        cells: [{
                            value: "Total balance: " + $scope.TotalBalancePastInstallment
                        }]
                    });
                    cfpLoadingBar.complete();
                },
                dataSource: $scope.mainGridDataSource,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                        [
                            5, 10, 20, 30, 50, 100, 200
                        ],
                    buttonCount: 5
                },
                dataBound: function () {
                    var grid = $("#mainGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },
                //columnMenu: true,
                //sortable: true,
                columns: [
                    {
                        field: "Id",
                        title: "Id",
                        width: "100px"
                    },
                    {
                        field: "ClubDomain",
                        title: "Domain",
                        width: "150px"
                    },
                    {
                        field: "Season",
                        title: "Season",
                        width: "120px"
                    },
                    {
                        field: "Program",
                        title: "Program",
                        width: "150px",
                        template: '<span style="white-space: pre-line" title="#=Program# #=ScheduleAndDate#" > #=Program# </span></br><span title="#=Program# #=ScheduleAndDate#">#=ScheduleAndDate#</span>'
                    },
                    {
                        field: "ParticipantName",
                        title: "Participant",
                        width: "150px"
                    },
                    {
                        field: "DueDate",
                        title: "Due date",
                        width: "110px"
                    },
                    {
                        field: "PaidDate",
                        title: "Paid date",
                        width: "110px"
                    },
                    {
                        field: "Amount",
                        title: "Amount",
                        width: "110px"
                    }
                    ,
                    {
                        field: "AmountPaid",
                        title: "Paid",
                        width: "100px"
                    },
                    {
                        field: "Balance",
                        title: "Balance",
                        width: "100px"
                    },
                    {
                        field: "Description",
                        title: "Error message",
                        width: "350px",
                        sortable: false
                    },
                    {
                        field: "Status",
                        title: "Installment status",
                        width: "150px"
                    },
                    {
                        field: "OrderMode",
                        title: "Order mode",
                        width: "130px"
                    },
                    {
                        field: "Stopped",
                        title: "Installment payment mode",
                        width: "200px",
                        sortable: false
                    },
                    {
                        field: "LastDigit",
                        title: "Last 4 digits",
                        width: "130px",
                        sortable: false
                    },
                    {
                        field: "Expiry",
                        title: "Expiration",
                        width: "120px",
                        sortable: false
                    },
                    {
                        field: "TransactionDate",
                        title: "Transaction date",
                        width: "160px",
                        sortable: false
                    },
                    {
                        field: "TransactionId",
                        title: "Transaction ID",
                        width: "160px",
                        sortable: false
                    },

                    {
                        field: "ConfirmationId",
                        title: "Confirmation ID",
                        width: "160px"
                    },
                    {
                        field: "Email",
                        title: "Email",
                        width: "190px"
                    },
                    {
                        title: "Actions",
                        width: "70px",
                        headerTemplate: "<span class='actions-title'>Actions</span>",
                        template: "<ul kendo-menu style='display: inline-block; position: absolute' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                       <li> ... \
                           <ul>#if(Status=='Suspended') {#\<li ng-click='UndoFailedInstallment(#:Id#)'>\
                                         <span class='k-link jbi-pencil'>Undo suspended</span>\
                                   </li>\#}#\
                                     <li ngaction='OrderDetail_View'  >\
                                          <a class='k-link jbi-show' ui_sref='OrderItem({ seasonDomain:\"#= SeasonDomain #\",orderItemId:#= OrderItemId #,clubId:#= ClubId # })' target='_blank' >View details</a>\
                                   </li>\
                           </ul>\
                       </li>\
                   </ul>"
                    }

                ]
            };

        }

        $scope.saveToExcel = function () {
            if ($scope.IsClearFilter) {
                jbToast.error("Please run the report.")
            } else {
                cfpLoadingBar.start();
                $("#mainGrid").getKendoGrid().saveAsExcel();
            }
        };

        $scope.filterData = function () {
            $scope.IsClearFilter = false;
            $scope.mainGridOptions.dataSource.page(1);
            $scope.mainGridOptions.dataSource.read();

        };

        $scope.clearFilters = function () {
            $scope.IsClearFilter = true;
            $scope.Model.PartnerId = "";
            $scope.Model.SchoolId = "";
            //$scope.Model.Status = "Unpaid";
            //$scope.Model.EndDate = $scope.Model.TempEndDate;
            $scope.Model.Status = "";
            $scope.Model.OrderMode = "";
            $scope.Model.UserName = "";
            $scope.Model.StartDate = "";
            $scope.Model.EndDate = "";
            $scope.Model.Mode = "";
            $scope.Model.searchTerm = "";
            $scope.Model.firstName = "";
            $scope.Model.lastName = "";
            $scope.Model.ProgramScheduleId = "";
            $scope.Model.ProgramId = "";
            $scope.Model.SeasonId = "";
            $scope.Model.TransactionStartDate = "";
            $scope.Model.TransactionEndDate = "";
            $scope.TotalBalancePastInstallment = "$0.00";
            $scope.TotalPaidAmountPastInstallment = "$0.00";
            $scope.TotalAmountPastInstallment = "$0.00";
            $scope.TotalPastInstallment = "0";
            $scope.Model.customeStripeErrorMessage = "";
            $scope.mainGridOptions.dataSource.data([]);

        };

        $scope.UndoFailedInstallment = function (installmentId) {
            jbConfirmation
                .yes(function () {
                    $http.post('Dashboard/System/UndoFailedInstallment', { id: installmentId })
                        .success(function (data) {

                            if (data.Status === true) {

                                jbToast.success("Undo suspended installment updated.");
                                $scope.mainGridOptions.dataSource.page(1);
                                $scope.mainGridOptions.dataSource.read();
                            }
                            else {
                                $scope.MC.handleErrors($scope, data);
                            }
                        });
                })
                .confirm();
        };
    }]);
})();