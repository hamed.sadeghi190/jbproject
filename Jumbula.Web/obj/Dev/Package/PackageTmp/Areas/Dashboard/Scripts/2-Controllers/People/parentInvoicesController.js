﻿(function () {
    'use strict';
    angular.module('dashboardApp').controller('parentInvoicesController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbConfirmation, jbToast) {

        window.app.globalObjects.pageClass = 'Members-view';
        //$scope.MC.openBlade({ url: "Dashboard/Blade/SystemClubs", order: 1, menu: "SystemClubs" });

        $scope.baseUrl = window.location.origin + '/Dashboard/People'; 

        $http.get($scope.baseUrl + '/PeopelPage').success(function (data, status, headers, config) {
            $scope.Model = data;

            $scope.filter.InvoiceType = 'All';
        }).error(function (data, status, headers, config) {
            jbToast.error(data);
        });

        $scope.searchTerm = "";
        $scope.filter = {};
        $scope.filter.InvoiceType = "0";
        $scope.filter.typeId = "-1";
        $scope.filter.endDate = "";
        $scope.filter.startDate = "";

        $scope.mainGridDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: $scope.baseUrl + '/GetAllInvoices/',
                    contentType: "application/json; charset=utf-8",
                    type: "POST"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        data = $.extend(
                           {
                               sort: null,
                               filter: null,
                               term: $scope.searchTerm,
                               typeId: $scope.filter.typeId,
                               endDate: $scope.filter.endDate,
                               startDate: $scope.filter.startDate,
                               InvoiceType: $scope.filter.InvoiceType,
                           }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            pageSize: 50,
            batch: true,
            serverPaging: true,
            serverSorting: true,
            schema: {
                data: "data",
                total: "total"
            },
            sortable: true
        });

        $scope.mainGridOptions = {
            dataSource: $scope.mainGridDataSource,
            sortable: true,
            serverPaging: true,
            columns: [
            //    {
            //    field: " ", 
            //    template: '<input type="checkbox" />',
            //    width:"20px"
            //}, 
            {
                field: "StrDate",
                title: "Date",
                
                
            }, {
                field: "InvoiceId",
                title: "InvoiceId",
                width:"300px"
            },
            {
                field: "StrStatus",
                title: "Status",
            }, {
                field: "Recipient",
                title: "Recipient",
                width: "200px",
            }, {

                field: "Amount",
                title: "Amount",
                format: "{0:c2}",
            }, 
              //{
              //    title: "Actions",
              //    width: "70px",
              //    headerTemplate: "<span class='actions-title'>Actions</span>",
              //    template: '<ul kendo-menu style="display: inline-block;" k-direction="\'left\'" k-orientation="\'horizontal\'" k-rebind="\'horizontal\'" k-on-select="">\
              //       <li > ... \
              //                <ul>\
              //                      <li ng-click="openClub(\'#=Domain#\')" >\
              //                            <span class="k-link ">Login</span>\
              //                      </li>\
              //                      <li ui-sref="SystemClubShowPlan({clubId: #=Id#})" >\
              //                            <span class="k-link ">Show Plan</span>\
              //                      </li>\
              //                      <li ui-sref="SystemClubBillingHistory({clubId: #=Id#})" >\
              //                            <span class="k-link ">Billing History</span>\
              //                      </li>\
              //                        <li  ng-click="deleteClub( #=Id# )">\
              //                            <span class="k-link jbi-remove">Delete</span>\
              //                      </li>\
              //              </ul>\
              //          </li>\
              //      </ul>'
              //}
            ],
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
        };

       
        $scope.search = function () {
            $("#mainGrid").data("kendoGrid").dataSource.read();
        };
        $scope.clearFilters = function () {
            $scope.searchTerm = "";
            $scope.filter.InvoiceType = "0";
            $scope.filter.typeId = "-1";
            $scope.filter.subTypeId = "-1";
            $scope.filter.endDate = "";
            $scope.filter.startDate = "";
            $("#mainGrid").data("kendoGrid").dataSource.read();
        };
        
        
        //$scope.deleteClub = function (id) {
        //    jbConfirmation
        //        .yes(function () {
        //            $http.post($scope.baseUrl + '/DeleteClub', { clubId: id }).success(function (data, status, headers, config) {
        //                if (data.Status == true) {

        //                    $scope.mainGridOptions.dataSource.read();

        //                }
        //            });
        //        })
        //        .confirm();
        //};

    }]);
})();

//$scope.onClick = function(){
//    for(var i = 0; i<$scope.kg.dataSource.data().length; i++){
//        var item = $scope.kg.dataSource.at(i);
//        if(item.admin){
//            console.log(item);
//        }
//    }
//}

//$scope.getClick = function(item){
//    if(item.admin){
//        for(var i = 0; i<$scope.kg.dataSource.data().length; i++){
//            var ditem = $scope.kg.dataSource.at(i)
//            if(ditem !== item){
//                ditem.set('admin', false);
//            }
//        }
          	
//    }
//}
//})