﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('RoomAssignmentController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        $scope.Model = {};
        $scope.Model.checkedIds = {};
        $scope.reportName = "";
        var nameModel = customReportService.getModelFromStrorage();
        if (is.not.undefined(nameModel) && nameModel != null) {
            var seasonId = nameModel.seasonId;
            var schoolId = nameModel.schoolId;
            $http.get('Dashboard/PortalReport/GetReoprtNameWithSeason', { params: { schoolId: schoolId, seasonId: seasonId } }).success(function (data, status, headers, config) {
                $scope.reportName = data;
                SetTheGrid();
            }).error(function (data, status, headers, config) {
                SetTheGrid();
                jbToast.error(data.Message);
            });
        }


        $scope.RoomAssignmentDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetRoomAssignments?seasonId=' + $scope.MC.stateParams.seasonId,
                    },

                },
            sort: { field: 'ClassName', dir: 'asc' },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverSorting: false,
            serverPaging: true,
            pageSize: 50,
            //batch: true,
            resizable: true,
        });
        function SetTheGrid() {
            $scope.RoomAssignmentReportGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.reportName + " (Room assigment report)",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },
                excelExport: function (e) {
                    cfpLoadingBar.complete();
                },
                dataSource: $scope.RoomAssignmentDataSource,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5
                },
              
                sortable: { mode: 'single' },
                columns: [
                    {
                        field: "ClassName",
                        title: "Class name",
                        width: "180px",
                    },
                    {
                        field: "SpaceReq",
                        title: "Space Requirements",
                        width: "200px",
                    },
                    {
                        field: "AdditionalComments",
                        title: "Additional comments",
                        width: "200px",
                    },
                    {
                        field: "Duration",
                        title: "Class Duration",
                        width: "200px",
                    },
                    {
                        field: "Sessions",
                        title: "Meeting Dates",
                        width: "200px",
                    },
                    {
                        field: "Min",
                        title: "Minimum",
                        width: "200px",
                    },
                    {
                        field: "Max",
                        title: "Maximum",
                        width: "200px",
                    },
                    {
                        field: "Room",
                        title: "Room Assignment",
                        width: "200px",
                    }
                ],
            }
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#RoomAssignmentReportGrid").getKendoGrid().saveAsExcel();
        }

        $scope.shareReport = function () {
            $state.go('ShareAsAttchment', { seasonId: nameModel.seasonId, reportName: "RoomAssigment" });
        }

    }
    ]);

})();