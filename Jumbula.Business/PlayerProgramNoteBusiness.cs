﻿
using Jb.Framework.Common;
using Jumbula.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class PlayerProgramNoteBusiness : IPlayerProgramNoteBusiness
    {
        private readonly IRepository<PlayerProgramNote> _playerPRogramNoteRepository;
        private readonly IEntitiesContext _dataContext;

        public PlayerProgramNoteBusiness(IRepository<PlayerProgramNote> playerPRogramNoteRepository, IEntitiesContext dataContext)
        {
            _playerPRogramNoteRepository = playerPRogramNoteRepository;
            _dataContext = dataContext;
        }

        public PlayerProgramNote Get(int id)
        {
            return _playerPRogramNoteRepository.GetList().SingleOrDefault(s => s.Id == id && !s.IsDeleted);
        }

        public IQueryable<PlayerProgramNote> GetList()
        {
            return _playerPRogramNoteRepository.GetList();
        }

        public IQueryable<PlayerProgramNote> GetList(Expression<Func<PlayerProgramNote, bool>> predicate)
        {
            return _playerPRogramNoteRepository.GetList(predicate);
        }

        public IQueryable<PlayerProgramNote> GetList(int programId, int clubStaffId, int profileId)
        {
            return GetList().Where(n => n.ProgramId == programId && n.ProfileId == profileId && n.ClubStaffId == clubStaffId);
        }

        public OperationStatus Create(PlayerProgramNote playerProgramNote)
        {
            return Create(playerProgramNote);
        }

        public OperationStatus Update(PlayerProgramNote playerProgramNote)
        {
            return _playerPRogramNoteRepository.Update(playerProgramNote);
        }

        public OperationStatus Delete(List<int> ids)
        {
            var notes = _dataContext.Set<PlayerProgramNote>().Where(p => ids.Contains(p.Id));

            foreach (var item in notes)
            {
                item.IsDeleted = true;
            }

            return _playerPRogramNoteRepository.Save();
        }

        public OperationStatus Update(List<PlayerProgramNote> notes)
        {
            OperationStatus result = new OperationStatus { Status = false };

            var noteIds = notes.Select(m => m.Id);

            var notesForEdit = GetList(p => noteIds.Contains(p.Id));

            foreach (var item in notesForEdit)
            {
                var note = notes.SingleOrDefault(m => m.Id == item.Id);
                item.Id = note.Id;
                item.ClubStaffId = note.ClubStaffId;
                item.Color = note.Color;
                item.MetaData.DateUpdated = DateTime.UtcNow;
                item.Note = note.Note;
                item.Title = note.Title;
                item.ProfileId = note.ProfileId;
                item.ProgramId = note.ProgramId;
            }

            var notesForAdd = notes.Where(m => m.Id == 0);

            notesForAdd.ToList().ForEach(n => n.MetaData = new MetaData() { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow });

            _dataContext.Set<PlayerProgramNote>().AddRange(notesForAdd);

            result.Status = _dataContext.SaveChanges() > 0;

            return result;
        }
    }
}
