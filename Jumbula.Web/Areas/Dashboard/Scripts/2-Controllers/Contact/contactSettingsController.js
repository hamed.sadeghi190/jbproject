﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('contactSettingsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', 'jbRandom', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService, jbRandom) {
        window.app.globalObjects.pageClass = 'catalog-create-view';
        $scope.MC.openBlade({ url: "Dashboard/Blade/Contact", order: 0, menu: "CONTACT" });
        window.app.globalObjects.initializeView();
        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        var initilizeModel = function () {
            $http.get('Dashboard/Contact/GetSettings').success(function (data, status, headers, config) {
                $scope.Model = data;
            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }

        initilizeModel();

        $scope.submit = function () {
           
            $http.post('Dashboard/Contact/SaveSettings', { model: $scope.Model })
                      .success(function (data, status, headers, config) {

                          if (data.Status) {

                              initilizeModel();
                              jbToast.success("Contact settings saved successfully.", "");
                          }
                          else {
                              $scope.MC.handleErrors($scope, data);
                          }

                      });
        }

    }]);
})();
