﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jumbula.Core.Model.Order
{
    public class OrderInformationViewModel
    {
        public object Gender { get; set; }
        public bool HasProgramScheduleId { get; set; }
        public string FullName { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string ConfirmationId { get; set; }
        public decimal OrderAmount { get; set; }

        public string TotalPrices => CurrencyHelper.FormatCurrencyWithPenny(OrderAmount, Currency);
  
        public string DesiredStartDate { get; set; }
        public string EffectiveTransferredDate { get; set; }
        public string EffectiveCanceledDate { get; set; }
        public List<DayOfWeek> Days { get; set; }
        public string DaysName
        {
            get
            {
                if (Days != null && Days.Any())
                {
                    return string.Join(", ", Days);
                }
                return string.Empty;
            }
        }
        public bool IsDropIn { get; set; }
        public ProgramTypeCategory ProgramType { get; set; }
        public OrderItemStatusReasons ItemStatusReson { get; set; }
        public CurrencyCodes Currency { get; set; }
        public decimal Balance { get; set; }

        public decimal TotalAmount { get; set; }

        public decimal PaidAmount { get; set; }

        public decimal AmountDifference { get; set; }

        public bool HasAmountDifference { get; set; }
    }
}
