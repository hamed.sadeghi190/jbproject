﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Campaign;
using Jumbula.Core.Model.Generic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Campaign = Jumbula.Core.Domain.Campaign;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Business
{
    public class CampaignBusiness : ICampaignBusiness
    {
        private readonly IClubBusiness _clubBusiness;
        private readonly IEntitiesContext _dataContext;
        private readonly IRepository<Campaign> _campaignRepository;
        private readonly IProgramBusiness _programBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IPlayerProfileBusiness _playerProfileBusiness;

        public CampaignBusiness(IClubBusiness clubBusiness, IEntitiesContext dataContext, IRepository<Campaign> campaignRepository, IProgramBusiness programBusiness, IOrderItemBusiness orderItemBusiness, IPlayerProfileBusiness playerProfileBusiness)
        {
            _clubBusiness = clubBusiness;
            _dataContext = dataContext;
            _campaignRepository = campaignRepository;
            _programBusiness = programBusiness;
            _orderItemBusiness = orderItemBusiness;
            _playerProfileBusiness = playerProfileBusiness;
        }

        public Campaign Get(int campaignId)
        {
            return _dataContext.Set<Campaign>().Single(c => c.Id == campaignId);
            //return base.Get(campaignId);
        }

        public IQueryable<Campaign> GetAllCampaigns(int clubId)
        {
            return _campaignRepository.GetList().Where(c => c.Club_Id == clubId);
        }

        public OperationStatus SaveCampaignData(Campaign campaign)
        {
            return _campaignRepository.Create(campaign);
        }

        public OperationStatus UpdateCampaign(Campaign campaign)
        {
            try
            {
                _campaignRepository.Update(campaign);

                return new OperationStatus { Status = true };
            }
            catch (Exception)
            {
                throw;
            }
        }

        public OperationStatus UpdateCampaign(Campaign campaign, List<CampaignRecipients> recipients)
        {
            var campaignDb = Get(campaign.Id);
            campaignDb.Recipients.ToList<CampaignRecipients>().ForEach(c => _dataContext.Entry(c).State = EntityState.Deleted);
            campaignDb = campaign;
            campaignDb.Recipients = recipients;
            return _campaignRepository.Update(campaignDb);
        }
        public OperationStatus UpdateCampaignRecipientsEvents(List<CampaignRecipients> recipients)
        {
            var recipientsGroupByCampainId = recipients.GroupBy(r => r.CampaignId).ToList();

            foreach (var group in recipientsGroupByCampainId)
            {
                var campaign = Get(group.First().CampaignId);

                foreach (var recipientInGroup in group)
                {

                    if (recipientInGroup.EmailStatus == 0)
                    {
                        continue;
                    }

                    var campaignRecipientsDb = campaign.Recipients.Where(r => r.EmailAddress.ToLower() == recipientInGroup.EmailAddress.ToLower());

                    if (campaignRecipientsDb != null && campaignRecipientsDb.Any())
                    {
                        foreach (var email in campaignRecipientsDb)
                        {
                            switch (recipientInGroup.EmailStatus)
                            {
                                case EmailStatus.delivered:
                                    email.EmailStatus = EmailStatus.delivered;
                                    break;
                                case EmailStatus.open:
                                    email.IsOpened = recipientInGroup.IsOpened;
                                    email.OpenDate = (!email.OpenDate.HasValue)
                                        ? recipientInGroup.OpenDate
                                        : email.OpenDate;
                                    if (email.EmailStatus < recipientInGroup.EmailStatus)
                                    {
                                        email.EmailStatus = EmailStatus.open;
                                    }
                                    break;
                                case EmailStatus.dropped:
                                    email.EmailStatus = EmailStatus.dropped;
                                    email.Reason = recipientInGroup.Reason;
                                    break;
                                case EmailStatus.click:
                                    email.IsClicked = recipientInGroup.IsClicked;
                                    email.ClickDate = (!email.ClickDate.HasValue)
                                        ? recipientInGroup.ClickDate
                                        : email.ClickDate;
                                    email.EmailStatus = EmailStatus.click;
                                    if (email.IsOpened == false && !email.OpenDate.HasValue)
                                    {
                                        email.IsOpened = true;
                                        email.OpenDate = recipientInGroup.ClickDate;
                                    }
                                    break;
                                case EmailStatus.bounce:
                                    email.EmailStatus = EmailStatus.bounce;
                                    break;
                                case EmailStatus.processed:
                                    email.EmailStatus = EmailStatus.processed;
                                    break;
                                case EmailStatus.deferred:
                                    email.EmailStatus = EmailStatus.deferred;
                                    break;
                                case EmailStatus.spamreport:
                                    email.EmailStatus = EmailStatus.spamreport;
                                    email.Reason = recipientInGroup.Reason;
                                    break;
                                case EmailStatus.unsubscribe:
                                    email.EmailStatus = EmailStatus.unsubscribe;
                                    break;
                            }

                        }
                    }
                }


            }

            return _campaignRepository.Save();
        }

        public int GetCampaignRecipientsNum(int campaignId)
        {
            return Get(campaignId).Recipients.Count;
        }

        public bool IsClicked(int campaignId, int recipientId)
        {
            return Get(campaignId).Recipients.Single(r => r.CampaignId == campaignId && r.Id == recipientId).IsClicked;
        }

        public bool IsOpened(int campaignId, int recipientId)
        {
            return Get(campaignId).Recipients.Single(r => r.CampaignId == campaignId && r.Id == recipientId).IsOpened;
        }

        public DateTime GetOpenDate(int campaignId, int recipientId)
        {
            return Get(campaignId).Recipients.Single(r => r.CampaignId == campaignId && r.Id == recipientId).OpenDate.Value;
        }

        public DateTime GetClickDate(int campaignId, int recipientId)
        {
            return Get(campaignId).Recipients.Single(r => r.CampaignId == campaignId && r.Id == recipientId).ClickDate.Value;
        }

        public CampaignRatesModel GetCampaignRates(int campaignId)
        {
            var campaignRecipients = Get(campaignId).Recipients;

            var total = campaignRecipients.Count();

            var deliveredNum =
                campaignRecipients.Count(
                    c =>
                        c.EmailStatus == EmailStatus.delivered || c.EmailStatus == EmailStatus.click ||
                        c.EmailStatus == EmailStatus.open);

            var openedNum =
                campaignRecipients.Count(c => c.EmailStatus == EmailStatus.open || c.EmailStatus == EmailStatus.click);

            var clickedNum = campaignRecipients.Count(c => c.EmailStatus == EmailStatus.click);

            var openedResultRate = Math.Round((double)100 * openedNum / total, 2);

            var deliveredResultRate = Math.Round((double)100 * deliveredNum / total, 2);

            var clickedResultRate = Math.Round((double)100 * clickedNum / total, 2);

            var lastOpenedDate = campaignRecipients.Max(c => c.OpenDate);
            var lastClickedDate = campaignRecipients.Max(c => c.ClickDate);

            return
                new CampaignRatesModel
                {
                    DeliveredNum = deliveredNum,
                    DeliveredRate = (decimal)deliveredResultRate,
                    OpenedNum = openedNum,
                    OpenedRate = (decimal)openedResultRate,
                    ClickedNum = clickedNum,
                    ClickedRate = (decimal)clickedResultRate,
                    LastClicked = lastClickedDate,
                    LastOpened = lastOpenedDate

                };
        }

        public double GetCampaignClickRate(int campaignId)
        {
            var camp = Get(campaignId);

            var totalNum = camp.Recipients.Count();

            var clickRate = camp.Recipients.Count(c => c.EmailStatus == EmailStatus.click);

            var rate = (double)clickRate * 100 / totalNum;

            return rate;
        }

        public double GetCampaignOpenRate(int campaignId)
        {
            var camp = Get(campaignId);

            var totalNum = camp.Recipients.Count();

            var openRate = camp.Recipients.Count(c => c.EmailStatus == EmailStatus.open || c.EmailStatus == EmailStatus.click);

            var rate = (double)openRate * 100 / totalNum;

            return rate;
        }

        public double GetCampaignEmailMarkAsSpam(int campaignId)
        {
            var camp = Get(campaignId);

            var totalNum = camp.Recipients.Count(c => c.EmailStatus == EmailStatus.delivered || c.EmailStatus == EmailStatus.open || c.EmailStatus == EmailStatus.click);

            var spamRate = camp.Recipients.Count(c => c.EmailStatus == EmailStatus.spamreport);

            double rate = (spamRate * 100) / totalNum;

            return rate;

        }

        public List<object> GetOpenedClickedRateInRang(int campaignId)
        {
            var campRecipients = Get(campaignId).Recipients;

            var startDate = campRecipients.Min(c => c.OpenDate.Value).AddDays(-1);
            var endDate = campRecipients.Max(c => c.OpenDate.Value);

            var result = new List<object>();

            var now = startDate;
            var openRateInDay = 0;
            var clickRateInDay = 0;
            string date;
            DateTime dateTime;
            object obj;

            while (now.Date <= endDate.Date)
            {
                openRateInDay =
                    campRecipients.Count(
                        r =>
                            (r.EmailStatus == EmailStatus.open || r.EmailStatus == EmailStatus.click) &&
                            r.OpenDate.Value.Day == now.Day);

                clickRateInDay =
                    campRecipients.Count(r => r.EmailStatus == EmailStatus.click && r.ClickDate.Value.Day == now.Day);

                date = now.ToString(Constants.DateTime_Comma);

                dateTime = now;

                obj = new { Date = date, Opened = openRateInDay, Clicked = clickRateInDay, DateTime = dateTime };

                result.Add(obj);

                now = now.AddDays(1);
            }

            return result;
        }

        public IQueryable<CampaignRecipients> GetCampaignRecipients(int campaignId)
        {
            return Get(campaignId).Recipients.AsQueryable();
        }

        public OperationStatus DeleteCampaign(int campaignId)
        {
            try
            {
                return _campaignRepository.Delete(campaignId);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public OperationStatus DeleteCampaignRecipient(int campId, int recipientId)
        {
            try
            {
                var campDb = Get(campId);
                var recipient = campDb.Recipients.Single(r => r.Id == recipientId);

                _dataContext.Entry(recipient).State = EntityState.Deleted;

                return _campaignRepository.Save();
            }
            catch (Exception e)
            {
                return new OperationStatus { Status = false, Message = e.Message };
            }
        }

        public OperationStatus AddNewCampaignRecipient(int campId, string email, string firstName, string lastName, RecipeientType type)
        {
            try
            {
                var campDb = Get(campId);

                if (campDb.Recipients.All(r => r.EmailAddress != email))
                {
                    campDb.Recipients.Add(new CampaignRecipients
                    {
                        EmailAddress = email,
                        RecipientName = firstName,
                        RecipientLastName = lastName,
                        RecipeintType = type,
                    });

                    return _campaignRepository.Save();
                }
                return new OperationStatus { Status = false, Message = "Recipient already exists." };
            }
            catch (Exception e)
            {
                return new OperationStatus { Status = false, Message = e.Message };
            }
        }

        public OperationStatus AddNewCampaignRecipients(int campaignId, List<CampaignRecipients> newRecipients)
        {
            try
            {
                var newCcMails = newRecipients.Where(e => e.RecipeintType == RecipeientType.CC).ToList();

                var campDb = Get(campaignId);

                var oldCcMails = campDb.Recipients.Where(e => e.RecipeintType == RecipeientType.CC).ToList();
                if (newCcMails.Count > 0)
                {
                    _dataContext.Set<CampaignRecipients>().RemoveRange(oldCcMails);
                }

                foreach (
                    var newRecipient in
                        newRecipients.Where(
                            newRecipient => campDb.Recipients.All(r => r.EmailAddress != newRecipient.EmailAddress)))
                {
                    campDb.Recipients.Add(newRecipient);
                }

                return _campaignRepository.Save();
            }
            catch (Exception e)
            {
                return new OperationStatus { Status = false, Message = e.Message };
            }
        }

        public OperationStatus DeleteCcRecipients(List<CampaignRecipients> deletedRecipients)
        {
            _dataContext.Set<CampaignRecipients>().RemoveRange(deletedRecipients);
            return _campaignRepository.Save();
        }

        public OperationStatus MakeCampaignOnReport(IEnumerable<ReportRecipient> recipients, string clubDomain, string campaignName)
        {
            if (recipients.Any())
            {
                var campaign = new Campaign
                {
                    CampaignName = string.Format("Campaign - {0}", campaignName),
                    CreateDate = DateTime.UtcNow,
                    Status = CampaignScheduleType.Draft,
                    LastCreatedPage = EmailCampaignStep.Step1,
                    Club_Id = _clubBusiness.Get(clubDomain).Id
                };

                foreach (var recipient in recipients)
                {
                    campaign.Recipients.Add(new CampaignRecipients
                    {
                        EmailAddress = recipient.Email,
                        RecipientName = "",
                        RecipientLastName = recipient.Lastname,
                        IsClicked = false,
                        IsOpened = false,
                        EmailStatus = EmailStatus.processed,
                    });
                }

                campaign.CampaignType = CampaignReciepientsMode.SomePeople;

                var result = SaveCampaignData(campaign);

                return new OperationStatus { Status = result.Status, RecordsAffected = campaign.Id, Data = campaign.Id };
            }

            return new OperationStatus { Status = false, ExceptionMessage = "There is no record in the report." };
        }

        public OperationStatus MakeCampaignOnReport(IEnumerable<string> emails, string clubDomain, string campaignName)
        {
            if (emails.Any())
            {
                var campaign = new Campaign
                {
                    CampaignName = string.Format("Campaign - {0}", campaignName),
                    CreateDate = DateTime.UtcNow,
                    Status = CampaignScheduleType.Draft,
                    LastCreatedPage = EmailCampaignStep.Step1,
                    Club_Id = _clubBusiness.Get(clubDomain).Id
                };

                foreach (var email in emails)
                {
                    campaign.Recipients.Add(new CampaignRecipients
                    {
                        EmailAddress = email,
                        RecipientName = "",
                        RecipientLastName = "",
                        IsClicked = false,
                        IsOpened = false,
                        EmailStatus = EmailStatus.processed,
                    });
                }

                campaign.CampaignType = CampaignReciepientsMode.SomePeople;

                var result = SaveCampaignData(campaign);

                return new OperationStatus { Status = result.Status, RecordsAffected = campaign.Id };
            }
            return new OperationStatus { Status = false, ExceptionMessage = "There is no record in the report." };
        }

        public OperationStatus MakeCampaignOnLottery(List<long> orderItemIds, string clubDomain)
        {
            var recipients = new List<ReportRecipient>();

            var orderItems = _dataContext.Set<OrderItem>().Where(o => orderItemIds.Contains(o.Id));

            var programName = string.Empty;

            if (orderItems.Any(o => o.ProgramScheduleId.HasValue))
                programName = orderItems.First(o => o.ProgramScheduleId.HasValue).ProgramSchedule.Program.Name;

            recipients = orderItems.ToList().DistinctBy(o => o.Order.UserId).Select(o =>
              new ReportRecipient
              {
                  Email = o.Order.User.UserName,
                  Lastname = o.LastName,
              })
              .ToList();

            if (recipients.Any())
            {
                var campaign = new Campaign
                {
                    CampaignName = $"{programName} - Lottery Campaign",
                    CreateDate = DateTime.UtcNow,
                    Status = CampaignScheduleType.Draft,
                    LastCreatedPage = EmailCampaignStep.Step1,
                    Club_Id = _clubBusiness.Get(clubDomain).Id
                };

                foreach (var recipient in recipients)
                {
                    campaign.Recipients.Add(new CampaignRecipients
                    {
                        EmailAddress = recipient.Email,
                        RecipientName = "",
                        RecipientLastName = recipient.Lastname,
                        IsClicked = false,
                        IsOpened = false,
                        EmailStatus = EmailStatus.processed,
                    });
                }

                campaign.CampaignType = CampaignReciepientsMode.SomePeople;

                var result = SaveCampaignData(campaign);

                return new OperationStatus { Status = result.Status, RecordsAffected = campaign.Id };
            }

            return new OperationStatus { Status = false, ExceptionMessage = "You must select at least one order" };
        }

        #region Class confirmation
        public ClassConfirmationCampaignModel GetClassConfirmationModel(int clubId)
        {
            var model = new ClassConfirmationCampaignModel
            {
                ClassDateTime = DateTime.Now.AddDays(1),
            };

            var allSchools = _clubBusiness.GetRelatedClubs(clubId)
                    .Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.School)
                    .Select(c => new SelectKeyValue<long> { Text = c.Name, Value = c.Id })
                    .OrderBy(c => c.Text)
                    .ToList();

            model.AllSchools = allSchools;
            model.IsAllSchools = true;

            var years = new List<int>();

            for (var i = DateTime.Now.AddYears(1).Year; i >= 2015; i--)
            {
                years.Add(i);
            }

            model.AllYears = years.Select(y =>
                new SelectKeyValue<int>()
                {
                    Text = y.ToString(),
                    Value = y
                })
                .ToList();

            model.AllSeasons = EnumHelper.GetEnumList<SeasonNames>()
                .Select(e =>
                new SelectKeyValue<string>
                {
                    Text = e.Text,
                    Value = e.Value
                })
                .ToList();

            return model;
        }

        public PaginatedList<CampaignClassConfirmationItemModel> GetClassConfirmationItems(ClassConfirmationCampaignModel model, int partnerId, int pageIndex, int pageSize)
        {
            var programs = GetClassConfirmationPrograms(model, partnerId);

            var result = programs.Select(i =>
                new CampaignClassConfirmationItemModel
                {
                    ProgramId = i.Id,
                    SchoolName = i.Club.Name,
                    ProgramName = i.Name,
                    RegistrationCount = _orderItemBusiness.GetProgramOrderItems(i.Id).Count()
                })
                .ToPaginatedList(pageIndex, pageSize, programs.Count);

            return result;
        }

        public PaginatedList<CampaignClassConfirmationDetailItemModel> GetClassConfirmationDetails(long programId, int pageIndex, int pageSize)
        {
            var programOrders = _orderItemBusiness.GetProgramOrderItems(programId).Where(o => o.Order.IsLive);

            var totalCount = programOrders.Count();
            var pagedProgramOrders = programOrders.ToPaginatedList(pageIndex, pageSize, totalCount);

            var result = pagedProgramOrders.Select(item =>
                new CampaignClassConfirmationDetailItemModel
                {
                    ParticipantName = $"{item.FirstName} {item.LastName}",
                    Email = item.Order.User.UserName
                })
                .ToPaginatedList(pageIndex, pageSize, totalCount);

            return result;
        }

        public OperationStatus MakeCampaignForConfirmationClass(ClassConfirmationCampaignModel model, int partnerId)
        {
            var orderItemIds = new List<long>();

            var programs = GetClassConfirmationPrograms(model, partnerId);

            foreach (var program in programs)
            {
                orderItemIds.AddRange(_orderItemBusiness.GetProgramOrderItems(program.Id).Where(o => o.Order.IsLive).Select(o => o.Id).ToList());
            }

            var orderItems = _dataContext.Set<OrderItem>().Where(o => orderItemIds.Contains(o.Id));

            if (orderItems.Any())
            {
                var campaign = new Campaign
                {
                    CampaignType = CampaignReciepientsMode.ClassConfirmation,
                    CampaignName = $"Class confirmation campaign - {model.ClassDateTime.Value.ToString(Constants.DefaultDateFormat)}",
                    CreateDate = DateTime.UtcNow,
                    Status = CampaignScheduleType.Draft,
                    LastCreatedPage = EmailCampaignStep.Step1,
                    Club_Id = partnerId,
                    Subject = $"%CLASSNAME% Activity Reminder for %PARTICIPANTNAME%",
                    Body = $@"Dear %PARENTNAME%: <br/>
                             <br/>
                             <b>This is a confirmation that your child, %PARTICIPANTNAME%, has been enrolled in the 
                             %CLASSNAME% class for the {model.SeasonName.ToDescription()} Session and the class will begin %STARTDATE% 
                             and ends %ENDDATE% </b><br/>
                             <br/>
                             Starting a new activity for your child comes with lots of important 
                             questions! We hope to answer those questions in this email, so 
                             please hold onto this important email. <br/>
                             <br/>
                            ---------------------------- <br/>
                            On Site Coordinator:  <br/>
                            %ONSITECOORDINATORNAME% <br/>
                            %ONSITECOORDINATOREMAIL% <br/>
                             <br/>
                            <b>Activity Dates: </b>   <br/>
                            %CLASSDATES% <br/>
                            <b>Room: </b><br/>
                            %ROOM% <br/>
                            <b>Attendance Form:</b><br/>
                            Click <a href='%SCHOOLWEBSITE%'>Here</a><br/>
                            <b>Arrival and Dismissal Information:</b><br/>
                            Click <a href='%INFORMATIONWEBSITE%'>Here</a><br/>
                            <b>Frequently Asked Questions:</b><br/>
                            Click <a href='%FAQWEBSITE%'>Here</a><br/>
                            <br/>
                            Please note: If a class is cancelled due to absence of an instructor or inclement weather, you will be notified by e-mail and a make-up date will be scheduled. Due to space and time restrictions, only one weather related make up will be scheduled.<br/>
                            ----------------------------<br/>
                            <br/>
                            Have any other questions? You can always <a href='%CONTACTUSWEBSITE%'>contact us</a>. We are here to help! <br/>
                            <br/>
                            -	The Flex Academies Team <br/>
                            "
                };

                foreach (var orderItem in orderItems)
                {
                    var parentName = "parent";

                    var user = orderItem.Order.User;
                    var parents = _playerProfileBusiness.GetParents(user.Id);
                    if (parents.Any())
                        parentName = parents.First().Contact.FirstName;

                    var programSchedule = orderItem.ProgramSchedule;
                    var program = programSchedule.Program;

                    var club = programSchedule.Program.Club;

                    var attribute = new ClassConfirmationAttribute()
                    {
                        DayAndDate = model.ClassDateTime.Value.ToString("dddd, MMMM dd"),
                        ClassName = programSchedule.Program.Name,
                        ParticipantName = orderItem.Player.Contact.FullName,
                        ParentName = parentName,
                        StartDate = programSchedule.StartDate.ToString(Constants.DefaultDateFormat),
                        EndDate = programSchedule.EndDate.ToString(Constants.DefaultDateFormat),
                        OnSiteCoordinatorName = club?.Setting?.OnSiteCoordinatorName,
                        OnSiteCoordinatorEmail = club?.Setting?.OnSiteCoordinatorEmail,
                        ClassDates = string.Join("<br/>", _programBusiness.GetClassDates(program.ProgramSchedules.Last(s => !s.IsDeleted), _programBusiness.GetClassScheduleTime(program)).Select(d => d.SessionDate.ToString(Constants.DateWithoutYear)).ToList()),
                        UpcommingClassDates = string.Join("<br/>", _programBusiness.GetClassDates(program.ProgramSchedules.Last(s => !s.IsDeleted), _programBusiness.GetClassScheduleTime(program)).Where(s => s.SessionDate.Date >= model.ClassDateTime.Value.Date).Select(d => d.SessionDate.ToString(Constants.DateWithoutYear)).ToList()),
                        Room = program.Room,
                        SchoolWebsite = "https://flexacademies.com/attendance/",
                        InformationWebsite = $"https://{club.Domain}.jumbula.com/#/arrival-dismissal-weather-absences",
                        FaqWebsite = $"https://{club.Domain}.jumbula.com/#/faq",
                        ContactUsWebsite = "https://flexacademies.com/contact/"
                    };

                    campaign.Recipients.Add(new CampaignRecipients
                    {
                        EmailAddress = user.UserName,
                        RecipientName = "",
                        RecipientLastName = orderItem.Player.Contact.LastName,
                        IsClicked = false,
                        IsOpened = false,
                        EmailStatus = EmailStatus.processed,
                        AttributesSerialized = JsonConvert.SerializeObject(attribute)
                    });
                }

                var result = SaveCampaignData(campaign);

                return new OperationStatus { Status = result.Status, RecordsAffected = campaign.Id };
            }

            return new OperationStatus { Status = false, Message = "Your search criteria does not contain any recipients." };
        }

        private List<Program> GetClassConfirmationPrograms(ClassConfirmationCampaignModel model, int partnerId)
        {
            var result = new List<Program>();

            var minDateTime = model.ClassDateTime.Value.Date;
            var maxDateTime = model.ClassDateTime.Value.Date.AddDays(1).AddSeconds(-1);

            var programs = _programBusiness.GetList().Where(p =>
              p.Club.PartnerId == partnerId &&
              p.Status == ProgramStatus.Open && p.LastCreatedPage == ProgramPageStep.Step4 && p.TypeCategory == ProgramTypeCategory.Class &&
              p.Season.Name == model.SeasonName && p.Season.Year == model.Year);

            if (!model.IsAllSchools)
                programs = programs.Where(p => model.SelectedSchools.Contains(p.ClubId));

            foreach (var program in programs.ToList())
            {
                var programScheduleMode = _programBusiness.GetClassScheduleTime(program);
                var classDates =
                    _programBusiness.GetClassDates(program.ProgramSchedules.Last(s => !s.IsDeleted), programScheduleMode);

                if (classDates.Select(s => s.SessionDate).ToList().Any(s => s >= minDateTime && s <= maxDateTime))
                    result.Add(program);
            }

            return result;
        }
        #endregion

    }
}
