﻿using SportsClub.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;
using Jumbula.Web.Mvc.Attributes;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ClubLocationViewModel
    {
        public ClubLocationViewModel(bool isEditMode)
        {
            //AutoCompleteAddress = new PostalAddressAutoCompleteViewModel(false);
            IsEditMode = isEditMode;
        }

        public ClubLocationViewModel()
        {

        }

        public int? PostalAdderssId { get; set; }

        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [DisplayName("Location name")]
        public string LocationName { get; set; }

        [Required]
        [DisplayName("Location address")]
        [CheckCompleteAddress("")]
        public PostalAddressAutoCompleteViewModel AutoCompleteAddress { get; set; }

        public string ClubDomain { get; set; }

        public bool IsEditMode { get; set; }

        public int? ClubLocationId { get; set; }
    }

    public class DisplayClubLocationViewModel
    {
        public List<ClubLocation> ClubLocations { get; set; }
        public int? SelectedClubLocationId { get; set; }
        public string ClubDomain { get; set; }
        public DisplayClubLocationViewModel(string clubDomain)
        {
            ClubDomain = clubDomain;
        }

        public DisplayClubLocationViewModel()
        {
            
        }

        //public void SeedClubLocation()
        //{
        //    var clubLocations = Ioc.IClubLocationDb.GetClubLocations(ClubDomain);
        //    this.ClubLocations = clubLocations;
        //}
    }
}