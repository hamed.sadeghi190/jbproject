﻿(function () {
    'use strict';

    angular.module('dashboardApp')
        .controller('reportsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', 'modalService', 'trainingService', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbConfirmation, modalService, trainingService, jbToast) {
            window.app.globalObjects.pageClass = 'report-add';
            window.app.globalObjects.initializeView();
            $scope.MC.openBlade({ url: "Dashboard/Blade/Season", order: 1, menu: "SEASONS" });
            $scope.MC.openBlade({ url: "Dashboard/Blade/Reports", order: 2, menu: "SEASONS" });

            $scope.MC.stateParams = $stateParams;


            var baseUrl = window.location.origin + '/Dashboard/CustomReport';

            $scope.reportsDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport:
                    {
                        read: {
                            url: baseUrl + '/GetAllReports?seasonDomain=' + $scope.MC.stateParams.seasonDomain,
                            type: "POST",
                        },
                        parameterMap: function (data, type) {
                            if (type === "read") {
                                return {
                                    term: $scope.searchTerm,
                                    skip: data.skip,
                                    take: data.take,
                                    page: data.page,
                                    pageSize: data.pageSize,
                                    sort: data.sort
                                }
                            }
                        }
                    },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverFiltering: true,
                serverPaging: true,
                pageSize: 10,
            });

            $scope.reportListGridOptions = {
                dataSource: $scope.reportsDataSource,
                pageable: {
                    refresh: true,
                    pageSizes:
                        [
                            5, 10, 20, 30, 50, 100
                        ],
                    buttonCount: 5
                },
                sortable: true,
                columns: [
                    {
                        field: "Title",
                        title: "Report name",
                        template: "<a ui-sref='ReportEdit({reportId: #=Value#, seasonDomain:\"" + $scope.MC.stateParams.seasonDomain + "\"})'  class='block'> #= Title # </a>",
                    }, {
                        field: "Type",
                        title: "Report type",
                        template: "#if(Type == 'Admin') {# <span> Registration </span> #} else if(Type == 'Parent') {# <span> Profile </span> #}#"
                    }, {
                        field: "",
                        title: "",
                        template: "#if(Type == 'Admin') {# <a ui-sref='CustomReportSelectPrograms({seasonDomain: \"" + $scope.MC.stateParams.seasonDomain + "\", reportId: #= Value #, reportType:\"Registration\", reportName:\"#= Title #\"})' class='block'><i class='icon-play on-left'></i>Run</a> #} else if(Type == 'Parent'){# <a ui-sref='CustomReportSelectPrograms({seasonDomain: \"" + $scope.MC.stateParams.seasonDomain + "\", reportId:  #= Value #, reportType:\"Parent\", reportName:\"#= Title #\"})' class='block'><i class='icon-play on-left'></i>Run</a> #}#",
                    }, {
                        title: "Actions",
                        width: "70px",
                        headerTemplate: "<span class='actions-title'>Actions</span>",
                        template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\<li> ... \
				                    <ul><li ui-sref='ReportEdit({reportId: #=Value#, seasonDomain:\"" + $scope.MC.stateParams.seasonDomain + "\"})' class='k-item k-state-default k-first' role='menuitem'>\
						                <span class='k-link jbi-pencil'>Edit</span>\
				                        </li>" +
                            "<li ng-click='deleteReport(#=Value#)' class='k-item k-state-default' role='menuitem'>\
						                <span class='k-link jbi-remove'>Delete</span>\
				                        </li>" +
                            "<li ng-click='showActionCopyToSeason(#=Value#)' class='k-item k-state-default' role='menuitem'>\
						                <span class='k-link jbi-files'>Copy to season</span>\
				                        </li>" +
                            "</ul>\
			                        </li>\
		                      </ul>",
                        attributes: {
                            class: "grid-actions"
                        }
                    }
                ],
            };

            $scope.deleteReport = function (reportId) {
                jbConfirmation.yes(function () {
                    $http.post(baseUrl + '/DeleteReport', { reportId: reportId }).success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            $scope.reportListGridOptions.dataSource.read();
                        }
                    });
                }).confirm();


            }

            $scope.showActionCopyToSeason = function (reportId) {
                $scope.seasonIds = [];
                $scope.currentReportId = reportId;
                $scope.showSeasonMenu();
                $http.get(baseUrl + '/GetLiveSeasonsClub', { params: { seasonDomain: $scope.MC.stateParams.seasonDomain} } )
                    .success(function (data) {

                        $scope.seasons = data;
                    });
            }

            $scope.closeSeasonMenu = function () {
                $scope.isShowSeasonMenu = false;
            }

            $scope.showSeasonMenu = function () {
                $scope.isShowSeasonMenu = true;
            }

            $scope.addSeasonId = function (seasonId) {
                var isExist = $scope.seasonIds.includes(seasonId);

                if (isExist) {
                    var index = $scope.seasonIds.indexOf(seasonId);
                    $scope.seasonIds.splice(index, 1);
                } else {
                    $scope.seasonIds.push(seasonId);
                }
            }

            $scope.submitCopyToSeason = function () {

                if ($scope.seasonIds.length !== 0) {
                    $scope.closeSeasonMenu();

                    $http.get(baseUrl + "/CopyCustomReportToSeason", { params: { sourceReportId: $scope.currentReportId, destinationSeasonIds: $scope.seasonIds}})
                        .success(function (data) {
                            
                            if (data.Status) {
                                jbToast.success("Report copied successfully.");
                            } else {
                                jbToast.error("Error");
                            }

                        });
                }
            }

            $scope.showModal = function () {
                var settingModal = {
                    Type: 'Video',
                    URL: 'https://www.youtube.com/embed/1VANT1xJwWk',
                    Title: 'This Jumbula tutorial video will go over our custom report options.'
                }
                modalService.showModal('fullModal', settingModal);
            }


        }])
        .controller('registrationDataReportsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, jbConfirmation) {
            window.app.globalObjects.pageClass = 'report-add';
            window.app.globalObjects.initializeView();

            $scope.MC.openBlade({ url: "Dashboard/Blade/RegistrationDataReports", order: 3, menu: "SEASONS" });

            $scope.MC.stateParams = $stateParams;
        }])
        .controller('financeReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, jbConfirmation) {
            window.app.globalObjects.pageClass = 'report-add';
            window.app.globalObjects.initializeView();

            $scope.MC.openBlade({ url: "Dashboard/Blade/FinanceReport", order: 3, menu: "SEASONS" });

            $scope.MC.stateParams = $stateParams;
        }])
        .controller('rosterReportsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, jbConfirmation) {
            window.app.globalObjects.pageClass = 'report-add';
            window.app.globalObjects.initializeView();

            $scope.MC.openBlade({ url: "Dashboard/Blade/RosterReports", order: 3, menu: "SEASONS" });

            $scope.MC.stateParams = $stateParams;
        }])
        .controller('capacityReportsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, jbConfirmation) {
            window.app.globalObjects.pageClass = 'report-add';
            window.app.globalObjects.initializeView();

            $scope.MC.openBlade({ url: "Dashboard/Blade/CapacityReports", order: 3, menu: "SEASONS" });

            $scope.MC.stateParams = $stateParams;
        }])
        .controller('subscriptionsReportsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, jbConfirmation) {
            window.app.globalObjects.pageClass = 'report-add';
            window.app.globalObjects.initializeView();

            $scope.MC.openBlade({ url: "Dashboard/Blade/SubscriptionsReports", order: 3, menu: "SEASONS" });

            $scope.MC.stateParams = $stateParams;
        }])
        .controller('medicalReportsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, jbConfirmation) {
            window.app.globalObjects.pageClass = 'report-add';
            window.app.globalObjects.initializeView();

            $scope.MC.openBlade({ url: "Dashboard/Blade/MedicalReports", order: 3, menu: "SEASONS" });

            $scope.MC.stateParams = $stateParams;
        }])
        .controller('operationsReportsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, jbConfirmation) {
            window.app.globalObjects.pageClass = 'report-add';
            window.app.globalObjects.initializeView();

            $scope.MC.openBlade({ url: "Dashboard/Blade/OperationsReports", order: 3, menu: "SEASONS" });

            $scope.MC.stateParams = $stateParams;
        }])
        .controller('mobileAppReportsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, jbConfirmation) {
            window.app.globalObjects.pageClass = 'report-add';
            window.app.globalObjects.initializeView();

            $scope.MC.openBlade({ url: "Dashboard/Blade/MobileAppReports", order: 3, menu: "SEASONS" });

            $scope.MC.stateParams = $stateParams;
        }]);

})();
