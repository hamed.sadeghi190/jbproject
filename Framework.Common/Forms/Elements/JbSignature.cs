﻿using Jb.Framework.Common.Forms.JsonFormatter;
using Jb.Framework.Common.Forms.Validation;
using Newtonsoft.Json;

namespace Jb.Framework.Common.Forms
{
    [FluentValidation.Attributes.Validator(typeof(SignatureValidator))]
    [JsonConverter(typeof(JbElementConverter))]

    public class JbSignature  : JbBaseElement
    {
        public JbSignature()
        {
        }
        private string signitureRoleMessage;
        public string SignitureRoleMessage
        {
            get { return string.IsNullOrEmpty(Title) ? "Signiture is required" : Title + " is required"; }
            set { signitureRoleMessage = value; }
        }
        
        public string Value { get; set; }

        public bool IsRequired { get; set; }
    }
}
