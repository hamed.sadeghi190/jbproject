﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Tests;
using NUnit.Framework;

namespace Jumbula.AutomationTests.Scenarios
{
   public class ContactTestScenario : BaseTestScenario
    {
        [Test]
        public void ViewContacts()
        {
            BaseUrl = DataDriver.Partner2URL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            ExtraAPI.Wait(5000);
            WebDriver.Manage().Window.Maximize();

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginPartner();

            // Go to contacts
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToContacts();

            // Check default setting
            var contactTest = new ContactTest(WebDriver);
            contactTest.ViewContacts();

            WebDriver.Close();
        }

        [Test]
        public void FilterContacts()
        {
            BaseUrl = DataDriver.Partner2URL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            ExtraAPI.Wait(5000);
            WebDriver.Manage().Window.Maximize();

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginPartner();

            // Go to contacts
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToContacts();

            // Check default setting
            var contactTest = new ContactTest(WebDriver);
            contactTest.FilterContacts();

            WebDriver.Close();
        }

        [Test]
        public void AddContact()
        {
            BaseUrl = DataDriver.Partner2URL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            ExtraAPI.Wait(5000);
            WebDriver.Manage().Window.Maximize();

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginPartner();

            // Go to contacts
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToContacts();

            // Add contact
            var contactTest = new ContactTest(WebDriver);
            contactTest.AddContact();

            WebDriver.Close();

        }

        [Test]
        public void EditContact()
        {
            BaseUrl = DataDriver.Partner2URL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            ExtraAPI.Wait(5000);
            WebDriver.Manage().Window.Maximize();

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginPartner();

            // Go to contacts
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToContacts();

            // Edit contact
            var contactTest = new ContactTest(WebDriver);
            ExtraAPI.Wait(10000);
            contactTest.EditContact();

            WebDriver.Close();

        }

        [Test]
        public void DeleteContact()
        {
            BaseUrl = DataDriver.Partner2URL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            ExtraAPI.Wait(5000);
            WebDriver.Manage().Window.Maximize();

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginPartner();

            // Go to contacts
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToContacts();

            // Add contact
            var contactTest = new ContactTest(WebDriver);
            contactTest.DeleteContact();

            WebDriver.Close();

        }

        [Test]
        public void CheckSettings()
        {
            BaseUrl = DataDriver.Partner2URL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            ExtraAPI.Wait(5000);
            WebDriver.Manage().Window.Maximize();

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginPartner();

            // Go to contact Setting
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToContactSetting();

            // Check default setting
            var contactTest = new ContactTest(WebDriver);
            contactTest.CheckSetting();

            WebDriver.Close();
        }
    }
}
