﻿using System;
using FluentAssertions;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.ClubSetting;
using Jumbula.Test.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using Jumbula.Core.Data;

namespace Jumbula.Business.Tests.Unit
{
    [TestClass]
    public class ClubSettingBusinessTest
    {
        #region Fields
        private Mock<IClubBusiness> _mockClubBusiness;

        private AutoChargePolicySettingViewModel _autoChargePolicySettingViewModel = new AutoChargePolicySettingViewModel();
        private DelinquentPolicySettingViewModel _delinquentPolicySettingViewModel = new DelinquentPolicySettingViewModel();
        #endregion 

        #region Constants
        private const int ClubId = 10;
        private const int PartnerId = 1;
        #endregion

        #region Test methods

        [TestMethod]
        public void GetAutoChargePolicy_ClubSettingIsNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(new Club { Setting = null, SettingSerialized = JsonHelper.JsonSerializer(null) });

            // Act
            var result = service.GetAutoChargePolicy(ClubId);

            // Assert
            result.CombinePayments.Should().BeFalse();
            result.SendSummeryEmail.Should().BeFalse();
            result.SuccessAdminEmailTemplate.Should().BeNullOrEmpty();
            result.SuccessParentEmailTemplate.Should().BeNullOrEmpty();
            result.SuccessEmailToAdmin.Should().BeTrue();
            result.TotalAttempts.ElementAt(0).Value.Should().Be("1");
            result.TotalAttempts.ElementAt(1).Value.Should().Be("2");
            result.TotalAttempts.ElementAt(2).Value.Should().Be("3");
            result.TotalAttempts.ElementAt(3).Value.Should().Be("4");
            result.TotalAttempts.ElementAt(4).Value.Should().Be("5");
            result.TryDurationOptions.ElementAt(0).Value.Should().Be("1");
            result.TryDurationOptions.ElementAt(1).Value.Should().Be("2");
            result.TryDurationOptions.ElementAt(2).Value.Should().Be("3");
            result.TryDurationOptions.ElementAt(3).Value.Should().Be("4");
            result.TryDurationOptions.ElementAt(4).Value.Should().Be("5");
            result.TryDurationOptions.ElementAt(5).Value.Should().Be("6");
            result.TryDurationOptions.ElementAt(6).Value.Should().Be("7");
            result.Attempts.Count.Should().Be(5);
            result.Attempts[0].FailedEmailToAdmin.Should().BeFalse();
            result.Attempts[0].FailedEmailToParent.Should().BeFalse();
            result.Attempts[0].Enabled.Should().BeTrue();
            result.Attempts[0].AttemptOrder.Should().Be(1);
            result.Attempts[0].FailedAdminEmailTemplate.Should().BeEmpty();
            result.Attempts[0].FailedParentEmailTemplate.Should().BeEmpty();
            result.Attempts[1].FailedEmailToAdmin.Should().BeFalse();
            result.Attempts[1].FailedEmailToParent.Should().BeFalse();
            result.Attempts[1].TemplateTitle.Should().Be("The first additional attempt");
            result.Attempts[1].Enabled.Should().BeTrue();
            result.Attempts[1].IntervalDays.Should().Be(1.ToString());
            result.Attempts[1].AttemptOrder.Should().Be(2);
            result.Attempts[1].FailedAdminEmailTemplate.Should().BeEmpty();
            result.Attempts[1].FailedParentEmailTemplate.Should().BeEmpty();
            result.Attempts[2].FailedEmailToAdmin.Should().BeFalse();
            result.Attempts[2].FailedEmailToParent.Should().BeFalse();
            result.Attempts[2].TemplateTitle.Should().Be("The second additional attempt");
            result.Attempts[2].Enabled.Should().BeTrue();
            result.Attempts[2].IntervalDays.Should().Be(1.ToString());
            result.Attempts[2].AttemptOrder.Should().Be(3);
            result.Attempts[2].FailedAdminEmailTemplate.Should().BeEmpty();
            result.Attempts[2].FailedParentEmailTemplate.Should().BeEmpty();
            result.Attempts[3].FailedEmailToAdmin.Should().BeFalse();
            result.Attempts[3].FailedEmailToParent.Should().BeFalse();
            result.Attempts[3].TemplateTitle.Should().Be("The third additional attempt");
            result.Attempts[3].Enabled.Should().BeTrue();
            result.Attempts[3].IntervalDays.Should().Be(1.ToString());
            result.Attempts[3].AttemptOrder.Should().Be(4);
            result.Attempts[3].FailedAdminEmailTemplate.Should().BeEmpty();
            result.Attempts[3].FailedParentEmailTemplate.Should().BeEmpty();
            result.Attempts[4].FailedEmailToAdmin.Should().BeFalse();
            result.Attempts[4].FailedEmailToParent.Should().BeFalse();
            result.Attempts[4].TemplateTitle.Should().Be("The fourth additional attempt");
            result.Attempts[4].Enabled.Should().BeFalse();
            result.Attempts[4].IntervalDays.Should().Be(1.ToString());
            result.Attempts[4].AttemptOrder.Should().Be(5);
            result.Attempts[4].FailedAdminEmailTemplate.Should().BeEmpty();
            result.Attempts[4].FailedParentEmailTemplate.Should().BeEmpty();
        }

        [TestMethod]
        public void GetAutoChargePolicy_AutoChargePolicyIsNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(GetCreateClubWithNullAutoChargePolicy());

            // Act
            var result = service.GetAutoChargePolicy(ClubId);

            // Assert
            result.Should().NotBeNull();
        }

        [TestMethod]
        public void GetAutoChargePolicy_AutoChargeAttemptsIsNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(GetCreateClubWithNullAutoChargeAttempts());

            // Act
            var result = service.GetAutoChargePolicy(ClubId);

            // Assert
            result.CombinePayments.Should().BeTrue();
            result.SendSummeryEmail.Should().BeTrue();
            result.SuccessAdminEmailTemplate.Should().Be("Test for SuccessAdminEmailTemplate");
            result.SuccessParentEmailTemplate.Should().Be("Test for SuccessParentEmailTemplate");
            result.SuccessEmailToAdmin.Should().BeTrue();
            result.TotalAttempts.Count.Should().Be(5);
        }

        [TestMethod]
        public void GetAutoChargePolicy_FewAutoChargeAttempts_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(CreateClubWithFewAutoChargeAttempts());

            // Act
            var result = service.GetAutoChargePolicy(ClubId);

            // Assert
            result.CombinePayments.Should().BeTrue();
            result.SendSummeryEmail.Should().BeTrue();
            result.SuccessAdminEmailTemplate.Should().Be("Test for SuccessAdminEmailTemplate");
            result.SuccessParentEmailTemplate.Should().Be("Test for SuccessParentEmailTemplate");
            result.SuccessEmailToAdmin.Should().BeTrue();
            result.TotalAttempts.Count.Should().Be(5);
        }

        [TestMethod]
        public void GetAutoChargePolicy_WithAutoChargePolicy_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(CreateClubWithAutoChargePolicy());

            // Act
            var result = service.GetAutoChargePolicy(ClubId);

            // Assert
            result.CombinePayments.Should().BeTrue();
            result.SendSummeryEmail.Should().BeTrue();
            result.SuccessAdminEmailTemplate.Should().Be("Test for SuccessAdminEmailTemplate");
            result.SuccessParentEmailTemplate.Should().Be("Test for SuccessParentEmailTemplate");
            result.SuccessEmailToAdmin.Should().BeTrue();
            result.TotalAttempts.ElementAt(0).Value.Should().Be("1");
            result.TotalAttempts.ElementAt(1).Value.Should().Be("2");
            result.TotalAttempts.ElementAt(2).Value.Should().Be("3");
            result.TotalAttempts.ElementAt(3).Value.Should().Be("4");
            result.TotalAttempts.ElementAt(4).Value.Should().Be("5");
            result.TryDurationOptions.ElementAt(0).Value.Should().Be("1");
            result.TryDurationOptions.ElementAt(1).Value.Should().Be("2");
            result.TryDurationOptions.ElementAt(2).Value.Should().Be("3");
            result.TryDurationOptions.ElementAt(3).Value.Should().Be("4");
            result.TryDurationOptions.ElementAt(4).Value.Should().Be("5");
            result.TryDurationOptions.ElementAt(5).Value.Should().Be("6");
            result.TryDurationOptions.ElementAt(6).Value.Should().Be("7");
            result.Attempts.Count.Should().Be(5);
            result.Attempts[0].FailedEmailToAdmin.Should().BeTrue();
            result.Attempts[0].FailedEmailToParent.Should().BeTrue();
            result.Attempts[0].Enabled.Should().BeTrue();
            result.Attempts[0].AttemptOrder.Should().Be(1);
            result.Attempts[0].FailedAdminEmailTemplate.Should().Be("Test for try AdminEmailTemplate");
            result.Attempts[0].FailedParentEmailTemplate.Should().Be("Test for try ParentEmailTemplate");
            result.Attempts[1].FailedEmailToAdmin.Should().BeTrue();
            result.Attempts[1].FailedEmailToParent.Should().BeTrue();
            result.Attempts[1].TemplateTitle.Should().Be("The first additional attempt");
            result.Attempts[1].Enabled.Should().BeTrue();
            result.Attempts[1].IntervalDays.Should().Be(3.ToString());
            result.Attempts[1].AttemptOrder.Should().Be(2);
            result.Attempts[1].FailedAdminEmailTemplate.Should().Be("Test for first AdminEmailTemplate");
            result.Attempts[1].FailedParentEmailTemplate.Should().Be("Test for first ParentEmailTemplate");
            result.Attempts[2].FailedEmailToAdmin.Should().BeFalse();
            result.Attempts[2].FailedEmailToParent.Should().BeFalse();
            result.Attempts[2].TemplateTitle.Should().Be("The second additional attempt");
            result.Attempts[2].Enabled.Should().BeTrue();
            result.Attempts[2].IntervalDays.Should().Be(4.ToString());
            result.Attempts[2].AttemptOrder.Should().Be(3);
            result.Attempts[2].FailedAdminEmailTemplate.Should().Be("Test for second AdminEmailTemplate");
            result.Attempts[2].FailedParentEmailTemplate.Should().Be("Test for second ParentEmailTemplate");
            result.Attempts[3].FailedEmailToAdmin.Should().BeFalse();
            result.Attempts[3].FailedEmailToParent.Should().BeFalse();
            result.Attempts[3].TemplateTitle.Should().Be("The third additional attempt");
            result.Attempts[3].Enabled.Should().BeFalse();
            result.Attempts[3].IntervalDays.Should().Be(3.ToString());
            result.Attempts[3].AttemptOrder.Should().Be(4);
            result.Attempts[3].FailedAdminEmailTemplate.Should().Be("Test for third AdminEmailTemplate");
            result.Attempts[3].FailedParentEmailTemplate.Should().Be("Test for third ParentEmailTemplate");
            result.Attempts[4].FailedEmailToAdmin.Should().BeFalse();
            result.Attempts[4].FailedEmailToParent.Should().BeFalse();
            result.Attempts[4].TemplateTitle.Should().Be("The fourth additional attempt");
            result.Attempts[4].Enabled.Should().BeFalse();
            result.Attempts[4].IntervalDays.Should().Be(2.ToString());
            result.Attempts[4].AttemptOrder.Should().Be(5);
            result.Attempts[4].FailedAdminEmailTemplate.Should().BeEmpty();
            result.Attempts[4].FailedParentEmailTemplate.Should().BeEmpty();
        }

        [TestMethod]
        public void SaveAutoChargePolicy_WithAutoChargePolicy_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var createdClub = CreateClubWithAutoChargePolicy();
            _autoChargePolicySettingViewModel = FillAutoChargePolicySettingViewModel();

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(createdClub);
            _mockClubBusiness.Setup(x => x.Update(createdClub, null)).Returns(new Core.Data.OperationStatus { Status = true });

            // Act
            var result = service.SaveAutoChargePolicy(_autoChargePolicySettingViewModel, ClubId);

            // Assert
            result.Status.Should().BeTrue();
        }

        //DelinquentPolicy
        [TestMethod]
        public void GetDelinquentPolicy_ClubSettingIsNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(new Club { Setting = null, SettingSerialized = JsonHelper.JsonSerializer(null) });

            // Act
            var result = service.GetDelinquentPolicy(ClubId);

            // Assert
            result.AutoChargeFailAttempted.Should().BeTrue();
            result.ManualInstallmentDelayed.Should().BeTrue();
            result.CalculateFamilyBalance.Should().BeFalse();
            result.FamilyBalanceNoLimitDate.Should().BeNull();
            result.FamilyBalanceMinPrice.Should().Be(decimal.Zero);
            result.AllowEnrollOnDelinquent.Should().BeFalse();
            result.AllowEnrollTemplate.Should().BeEmpty();
            result.NotAllowEnrollTemplate.Should().BeEmpty();
            result.ExpelChildOnDelinquent.Should().BeFalse();
            result.ExpelChildTemplate.Should().BeEmpty();
            result.ManualPaymentDayOptions.Count.Should().Be(30);
            result.AutoChargeFailAttemptNumber.Should().Be(1.ToString());
            result.ManualInstallmentDelayDays.Should().Be(1.ToString());
        }

        [TestMethod]
        public void GetDelinquentPolicy_DelinquentPolicyIsNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(GetCreateClubWithNullDelinquentPolicy());

            // Act
            var result = service.GetDelinquentPolicy(ClubId);

            // Assert
            result.AutoChargeFailAttempted.Should().BeTrue();
            result.ManualInstallmentDelayed.Should().BeTrue();
            result.CalculateFamilyBalance.Should().BeFalse();
            result.FamilyBalanceNoLimitDate.Should().BeNull();
            result.FamilyBalanceMinPrice.Should().Be(decimal.Zero);
            result.AllowEnrollOnDelinquent.Should().BeFalse();
            result.AllowEnrollTemplate.Should().BeEmpty();
            result.NotAllowEnrollTemplate.Should().BeEmpty();
            result.ExpelChildOnDelinquent.Should().BeFalse();
            result.ExpelChildTemplate.Should().BeEmpty();
            result.ManualPaymentDayOptions.Count.Should().Be(30);
            result.AutoChargeFailAttemptNumber.Should().Be(1.ToString());
            result.ManualInstallmentDelayDays.Should().Be(1.ToString());
        }

        [TestMethod]
        public void GetDelinquentPolicy_WithDelinquentPolicy_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(CreateClubWithDelinquentPolicy());

            // Act
            var result = service.GetDelinquentPolicy(ClubId);

            // Assert
            result.ManualPaymentDayOptions.Count.Should().Be(30);
            //I can`t test this property because it is very complex
            //result.AutoChargeOptions
            result.AutoChargeFailAttempted.Should().BeTrue();
            result.AutoChargeFailAttemptNumber.Should().Be(3.ToString());
            result.ManualInstallmentDelayed.Should().BeFalse();
            result.ManualInstallmentDelayDays.Should().Be(1.ToString());
            result.AllowEnrollOnDelinquent.Should().BeTrue();
            result.AllowEnrollTemplate.Should().BeEmpty();
            result.NotAllowEnrollTemplate.Should().BeEmpty();
            result.ExpelChildOnDelinquent.Should().BeFalse();
            result.ExpelChildTemplate.Should().BeEmpty();
            result.CalculateFamilyBalance.Should().BeTrue();
            result.FamilyBalanceNoLimitDate.Should().Be(new DateTime(2018, 11, 27, 0, 0, 0));
            result.FamilyBalanceMinPrice.Should().Be(5.01M);
        }

        [TestMethod]
        public void SaveDelinquentPolicy_WithDelinquentPolicy_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var createdClub = CreateClubWithDelinquentPolicy();
            _delinquentPolicySettingViewModel = FillDelinquentPolicySettingViewModel();

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(createdClub);
            _mockClubBusiness.Setup(x => x.Update(createdClub, null)).Returns(new Core.Data.OperationStatus { Status = true });

            // Act
            var result = service.SaveDelinquentPolicy(_delinquentPolicySettingViewModel, ClubId);

            // Assert
            result.Status.Should().BeTrue();
        }

        [TestMethod]
        public void ReadDelinquentPolicy_WithoutPartner_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(CreateClubWithDelinquentPolicy);

            // Act
            var result = service.ReadDelinquentPolicy(ClubId);

            // Assert
            result.AutoChargeFailAttemptNumber.Should().Be(3);
            result.ManualInstallmentDelayDays.Should().Be(0);
            result.AllowEnrollOnDelinquent.Should().BeTrue();
            result.AllowEnrollTemplate.Should().BeEmpty();
            result.NotAllowEnrollTemplate.Should().BeEmpty();
            result.ExpelChildOnDelinquent.Should().BeFalse();
            result.ExpelChildTemplate.Should().BeEmpty();
            result.FamilyBalanceNoLimitDate.Should().Be(new DateTime(2018, 11, 27, 0, 0, 0));
            result.FamilyBalanceMinPrice.Should().Be(5.01M);
        }

        [TestMethod]
        public void ReadDelinquentPolicy_PartnerReadFromMember_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(CreateClubWithPartnerAndDelinquentPolicy);
            _mockClubBusiness.Setup(x => x.Get(PartnerId)).Returns(CreatePartnerWithDelinquentPolicyReadFromMember);

            // Act
            var result = service.ReadDelinquentPolicy(ClubId);

            // Assert
            result.AutoChargeFailAttemptNumber.Should().Be(3);
            result.ManualInstallmentDelayDays.Should().Be(0);
            result.AllowEnrollOnDelinquent.Should().BeTrue();
            result.AllowEnrollTemplate.Should().BeEmpty();
            result.NotAllowEnrollTemplate.Should().BeEmpty();
            result.ExpelChildOnDelinquent.Should().BeFalse();
            result.ExpelChildTemplate.Should().BeEmpty();
            result.FamilyBalanceNoLimitDate.Should().Be(new DateTime(2018, 11, 27, 0, 0, 0));
            result.FamilyBalanceMinPrice.Should().Be(5.01M);
        }

        [TestMethod]
        public void ReadDelinquentPolicy_PartnerReadFromPartner_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(CreateClubWithPartnerAndDelinquentPolicy);
            _mockClubBusiness.Setup(x => x.Get(PartnerId)).Returns(CreatePartnerWithDelinquentPolicyReadFromPartner);

            // Act
            var result = service.ReadDelinquentPolicy(ClubId);

            // Assert
            result.AutoChargeFailAttemptNumber.Should().Be(0);
            result.ManualInstallmentDelayDays.Should().Be(1);
            result.AllowEnrollOnDelinquent.Should().BeFalse();
            result.AllowEnrollTemplate.Should().BeEmpty();
            result.NotAllowEnrollTemplate.Should().BeEmpty();
            result.ExpelChildOnDelinquent.Should().BeTrue();
            result.ExpelChildTemplate.Should().BeEmpty();
            result.FamilyBalanceNoLimitDate.Should().Be(new DateTime(2018, 12, 27, 0, 0, 0));
            result.FamilyBalanceMinPrice.Should().Be(4.01M);
        }

        [TestMethod]
        public void ReadDelinquentPolicy_NullSetting_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(CreateClubWithPartnerAndDelinquentPolicy);
            _mockClubBusiness.Setup(x => x.Get(PartnerId)).Returns(new Club { Setting = null });

            // Act
            var result = service.ReadDelinquentPolicy(ClubId);

            // Assert
            result.Should().BeNull();
        }

        [TestMethod]
        public void GetDuplicateEnrollmentPolicy_ClubSettingIsNull_ShouldReturnDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();
            var club = FactoryGirl.Build<Club>(c => c.SettingSerialized = string.Empty);
            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(club);

            // Act
            var result = service.GetDuplicateEnrollmentPolicy(ClubId);

            // Assert
            result.Should().BeEquivalentTo(new DuplicateEnrollmentPolicySettingViewModel());
        }

        [TestMethod]
        public void GetDuplicateEnrollmentPolicy_ClubSettingHasValue_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var club = FactoryGirl.Build<Club>();
            var duplicateEnrollmentPolicy = new DuplicateEnrollmentPolicy();
            var settings = new ClubSetting { DuplicateEnrollmentPolicy = duplicateEnrollmentPolicy };
            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(club);

            // Act
            var result = service.GetDuplicateEnrollmentPolicy(ClubId);

            // Assert
            result.Should().BeEquivalentTo(new DuplicateEnrollmentPolicySettingViewModel());
        }

        [TestMethod]
        public void GetDuplicateEnrollmentPolicy_AllPolicyIsActive_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var club = FactoryGirl.Build<Club>();
            var duplicateEnrollmentPolicy = new DuplicateEnrollmentPolicy
            {
                PreventSameClass = true,
                PreventMultipleCampDropIn = true,
                PreventSameProgramBeforeAfterWeekDay = true,
                PreventSameCampSchedule = true,
                PreventOtherClasses = true
            };
            var settings = new ClubSetting { DuplicateEnrollmentPolicy = duplicateEnrollmentPolicy };
            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(club);

            // Act
            var result = service.GetDuplicateEnrollmentPolicy(ClubId);

            // Assert
            result.PreventMultipleCampDropIn.Should().BeTrue();
            result.PreventOtherClasses.Should().BeTrue();
            result.PreventSameCampSchedule.Should().BeTrue();
            result.PreventSameClass.Should().BeTrue();
            result.PreventSameProgramBeforeAfterWeekDay.Should().BeTrue();
        }

        [TestMethod]
        public void SaveDuplicateEnrollmentPolicy_AllPolicyIsInActive_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var duplicateEnrollmentPolicyViewModel = new DuplicateEnrollmentPolicySettingViewModel();

            var club = FactoryGirl.Build<Club>();
            var duplicateEnrollmentPolicy = new DuplicateEnrollmentPolicy();
            var settings = new ClubSetting { DuplicateEnrollmentPolicy = duplicateEnrollmentPolicy };
            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(club);
            _mockClubBusiness.Setup(x => x.Update(club, null)).Returns(new OperationStatus() { Status = true });

            // Act
            var result = service.SaveDuplicateEnrollmentPolicy(duplicateEnrollmentPolicyViewModel, ClubId);

            // Assert
            result.Status.Should().BeTrue();
        }

        [TestMethod]
        public void SaveDuplicateEnrollmentPolicy_AllPolicyIsActive_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var duplicateEnrollmentPolicyViewModel = new DuplicateEnrollmentPolicySettingViewModel()
            {
                PreventSameClass = true,
                PreventMultipleCampDropIn = true,
                PreventSameProgramBeforeAfterWeekDay = true,
                PreventSameCampSchedule = true,
                PreventOtherClasses = true
            };

            var club = FactoryGirl.Build<Club>();
            var duplicateEnrollmentPolicy = new DuplicateEnrollmentPolicy()
            {
                PreventSameClass = true,
                PreventMultipleCampDropIn = true,
                PreventSameProgramBeforeAfterWeekDay = true,
                PreventSameCampSchedule = true,
                PreventOtherClasses = true
            };
            var settings = new ClubSetting { DuplicateEnrollmentPolicy = duplicateEnrollmentPolicy };
            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(club);
            _mockClubBusiness.Setup(x => x.Update(club, null)).Returns(new OperationStatus() { Status = true });

            // Act
            var result = service.SaveDuplicateEnrollmentPolicy(duplicateEnrollmentPolicyViewModel, ClubId);

            // Assert
            result.Status.Should().BeTrue();
        }

        [TestMethod]
        public void GetEmailsContents_NotificationsIsNull_ShouldReturnEmailsContents()
        {
            // Arrange
            var service = CreateService();
            var club = FactoryGirl.Build<Club>();
            club.Setting = new ClubSetting();
            club.Setting.Notifications = null;
            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(club);

            // Act
            var result = service.GetEmailsContents(ClubId);

            // Assert
            result.Should().BeEquivalentTo(new EmailsSettingViewModel());
        }

        [TestMethod]
        public void GetEmailsContents_ClubSettingHasValue_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var emailNotificationSetting = new EmailNotificationSetting();

            var club = FactoryGirl.Build<Club>();

            var settings = new ClubSetting();
            settings.Notifications.Email = emailNotificationSetting;
            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(club);

            // Act
            var result = service.GetEmailsContents(ClubId);

            // Assert
            result.Should().BeEquivalentTo(new EmailNotificationSetting());
        }

        [TestMethod]
        public void GetEmailsContents_AllEmailsIsActive_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var club = FactoryGirl.Build<Club>();

            var emailNotificationSetting = new EmailNotificationSetting
            {
                ContentAbsentee = "Test Absentee",
                ContentCancellationEmail = "Test Cancellation Email",
                ContentConfirmationEmail = "Test Confirmation Email",
            };

            var settings = new ClubSetting();
            settings.Notifications.Email = emailNotificationSetting;
            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(club);

            // Act
            var result = service.GetEmailsContents(ClubId);

            // Assert
            result.ContentCancellationEmail.Should().Be("Test Cancellation Email");
            result.ContentConfirmationEmail.Should().Be("Test Confirmation Email");
            result.ContentAbsentee.Should().Be("Test Absentee");
        }

        [TestMethod]
        public void SaveEmailsContents_AllEmailIsActive_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var emailNotificationSetting = new EmailNotificationSetting
            {
                ContentAbsentee = "Test Absentee",
                ContentCancellationEmail = "Test Cancellation Email",
                ContentConfirmationEmail = "Test Confirmation Email",
            };

            var emailsSettingViewModel = new EmailsSettingViewModel
            {
                ContentAbsentee = "Test Absentee",
                ContentCancellationEmail = "Test Cancellation Email",
                ContentConfirmationEmail = "Test Confirmation Email",
            };

            var club = FactoryGirl.Build<Club>();

            var settings = new ClubSetting();
            settings.Notifications.Email = emailNotificationSetting;
            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(club);
            _mockClubBusiness.Setup(x => x.Update(club, null)).Returns(new OperationStatus() { Status = true });

            // Act
            var result = service.SaveEmailsContents(emailsSettingViewModel, ClubId);

            // Assert
            result.Status.Should().BeTrue();
        }

        [TestMethod]
        public void GetReportsContents_NotificationsIsNull_ShouldReturnReportsContents()
        {
            // Arrange
            var service = CreateService();
            var club = FactoryGirl.Build<Club>();
            club.Setting = new ClubSetting();
            club.Setting.Notifications = null;
            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(club);

            // Act
            var result = service.GetReportsContents(ClubId);

            // Assert
            result.Should().BeEquivalentTo(new ReportsSettingViewModel());
        }

        [TestMethod]
        public void GetReportsContents_ClubSettingHasValue_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var reportNotificationSetting = new ReportNotificationSetting();

            var club = FactoryGirl.Build<Club>();

            var settings = new ClubSetting();
            settings.Notifications.Report = reportNotificationSetting;
            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(club);

            // Act
            var result = service.GetReportsContents(ClubId);

            // Assert
            result.Should().BeEquivalentTo(new ReportNotificationSetting());
        }

        [TestMethod]
        public void GetReportsContents_AllReportsIsActive_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var club = FactoryGirl.Build<Club>();

            var reportNotificationSetting = new ReportNotificationSetting
            {
                DependentCareDescription = "Test Dependent Care",
            };

            var settings = new ClubSetting();
            settings.Notifications.Report = reportNotificationSetting;
            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(club);

            // Act
            var result = service.GetReportsContents(ClubId);

            // Assert
            result.DependentCareDescription.Should().Be("Test Dependent Care");

        }

        [TestMethod]
        public void SaveReportsContents_AllPolicyIsActive_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var reportNotificationSetting = new ReportNotificationSetting
            {
                DependentCareDescription = "Test Dependent Care",
            };

            var reportsSettingViewModel = new ReportsSettingViewModel
            {
                DependentCareDescription = "Test Dependent Care",
            };

            var club = FactoryGirl.Build<Club>();

            var settings = new ClubSetting();
            settings.Notifications.Report = reportNotificationSetting;
            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(club);
            _mockClubBusiness.Setup(x => x.Update(club, null)).Returns(new OperationStatus() { Status = true });

            // Act
            var result = service.SaveReportsContents(reportsSettingViewModel, ClubId);

            // Assert
            result.Status.Should().BeTrue();
        }

        [TestMethod]
        public void GetCatalogContents_NotificationsIsNull_ShouldReturnCatalogContents()
        {
            // Arrange
            var service = CreateService();
            var club = FactoryGirl.Build<Club>();
            club.Setting = new ClubSetting();
            club.Setting.Notifications = null;

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(club);

            // Act
            var result = service.GetCatalogContents(ClubId);

            // Assert
            result.Should().BeEquivalentTo(new CatalogsSettingViewModel());
        }

        [TestMethod]
        public void GetCatalogContents_ClubSettingHasValue_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var catalogNotificationSetting = new CatalogNotificationSetting();

            var club = FactoryGirl.Build<Club>();

            var settings = new ClubSetting();
            settings.Notifications.Catalog = catalogNotificationSetting;
            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(club);

            // Act
            var result = service.GetCatalogContents(ClubId);

            // Assert
            result.Should().BeEquivalentTo(new CatalogNotificationSetting());
        }

        [TestMethod]
        public void GetCatalogContents_AllCatalogIsActive_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var club = FactoryGirl.Build<Club>();

            var catalogNotificationSetting = new CatalogNotificationSetting
            {
                SendMessageToProviderOnClassSchedule = true,
            };

            var settings = new ClubSetting();
            settings.Notifications.Catalog = catalogNotificationSetting;
            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(club);

            // Act
            var result = service.GetCatalogContents(ClubId);

            // Assert
            result.SendMessageToProviderOnClassSchedule.Should().BeTrue();

        }

        [TestMethod]
        public void SaveCatalogContents_AllCatalogIsActive_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var catalogNotificationSetting = new CatalogNotificationSetting
            {
                SendMessageToProviderOnClassSchedule = true,
            };

            var catalogsSettingViewModel = new CatalogsSettingViewModel
            {
                SendMessageToProviderOnClassSchedule = true,
            };

            var club = FactoryGirl.Build<Club>();

            var settings = new ClubSetting();
            settings.Notifications.Catalog = catalogNotificationSetting;
            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(club);
            _mockClubBusiness.Setup(x => x.Update(club, null)).Returns(new OperationStatus() { Status = true });

            // Act
            var result = service.SaveCatalogContents(catalogsSettingViewModel, ClubId);

            // Assert
            result.Status.Should().BeTrue();
        }

        #endregion

        #region Private methods
        private ClubSettingBusiness CreateService()
        {
            _mockClubBusiness = new Mock<IClubBusiness>();

            return new ClubSettingBusiness(_mockClubBusiness.Object);
        }

        private static Club GetCreateClubWithNullAutoChargePolicy()
        {
            var club = FactoryGirl.Build<Club>();

            var settings = new ClubSetting { AutoChargePolicy = null };

            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            return club;
        }

        private static Club GetCreateClubWithNullAutoChargeAttempts()
        {
            var club = FactoryGirl.Build<Club>();

            var autoChargePolicy = new AutoChargePolicy
            {
                IsCombinePayments = true,
                SendSummeryEmail = true,
                SuccessAdminEmailTemplate = "Test for SuccessAdminEmailTemplate",
                SuccessParentEmailTemplate = "Test for SuccessParentEmailTemplate",
                SuccessEmailToAdmin = true,
                Attempts = null
            };

            var settings = new ClubSetting { AutoChargePolicy = autoChargePolicy };

            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            return club;
        }

        private static Club CreateClubWithFewAutoChargeAttempts()
        {
            var club = FactoryGirl.Build<Club>();

            var autoChargePolicy = new AutoChargePolicy
            {
                IsCombinePayments = true,
                SendSummeryEmail = true,
                SuccessAdminEmailTemplate = "Test for SuccessAdminEmailTemplate",
                SuccessParentEmailTemplate = "Test for SuccessParentEmailTemplate",
                SuccessEmailToAdmin = true,
                Attempts = new System.Collections.Generic.List<AutoChargeAttempt>
                    {
                        new AutoChargeAttempt
                        {
                            AttemptOrder=1,
                            FailedEmailToAdmin=true,
                            FailedEmailToParent=true,
                            AdminEmailTemplate="Test for try AdminEmailTemplate",
                            ParentEmailTemplate="Test for try ParentEmailTemplate"
                        },
                        new AutoChargeAttempt
                        {
                            AttemptOrder= 2,
                            FailedEmailToAdmin=true,
                            FailedEmailToParent=true,
                            IntervalDays=3,
                            AdminEmailTemplate="Test for first AdminEmailTemplate",
                            ParentEmailTemplate="Test for first ParentEmailTemplate",
                            Enabled = true
                        },
                        new AutoChargeAttempt
                        {
                            AttemptOrder= 3,
                            FailedEmailToAdmin=false,
                            FailedEmailToParent=false,
                            IntervalDays=4,
                            AdminEmailTemplate="Test for second AdminEmailTemplate",
                            ParentEmailTemplate="Test for second ParentEmailTemplate",
                            Enabled = true
                        }
                    }
            };

            var settings = new ClubSetting { AutoChargePolicy = autoChargePolicy };

            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            return club;
        }

        private static Club CreateClubWithAutoChargePolicy()
        {
            var club = FactoryGirl.Build<Club>();

            var autoChargePolicy = new AutoChargePolicy
            {
                IsCombinePayments = true,
                SendSummeryEmail = true,
                SuccessAdminEmailTemplate = "Test for SuccessAdminEmailTemplate",
                SuccessParentEmailTemplate = "Test for SuccessParentEmailTemplate",
                SuccessEmailToAdmin = true,
                Attempts = new System.Collections.Generic.List<AutoChargeAttempt>
                    {
                        new AutoChargeAttempt
                        {
                            AttemptOrder=1,
                            FailedEmailToAdmin=true,
                            FailedEmailToParent=true,
                            AdminEmailTemplate="Test for try AdminEmailTemplate",
                            ParentEmailTemplate="Test for try ParentEmailTemplate"
                        },
                        new AutoChargeAttempt
                        {
                            AttemptOrder= 2,
                            FailedEmailToAdmin=true,
                            FailedEmailToParent=true,
                            IntervalDays=3,
                            AdminEmailTemplate="Test for first AdminEmailTemplate",
                            ParentEmailTemplate="Test for first ParentEmailTemplate",
                            Enabled = true
                        },
                        new AutoChargeAttempt
                        {
                            AttemptOrder= 3,
                            FailedEmailToAdmin=false,
                            FailedEmailToParent=false,
                            IntervalDays=4,
                            AdminEmailTemplate="Test for second AdminEmailTemplate",
                            ParentEmailTemplate="Test for second ParentEmailTemplate",
                            Enabled = true
                        },
                        new AutoChargeAttempt
                        {
                            AttemptOrder= 4,
                            FailedEmailToAdmin=false,
                            FailedEmailToParent=false,
                            IntervalDays=3,
                            AdminEmailTemplate="Test for third AdminEmailTemplate",
                            ParentEmailTemplate="Test for third ParentEmailTemplate",
                            Enabled = false
                        },
                        new AutoChargeAttempt
                        {
                            AttemptOrder= 5,
                            FailedEmailToAdmin=false,
                            FailedEmailToParent=false,
                            AdminEmailTemplate="",
                            ParentEmailTemplate="",
                            Enabled = false
                        }
                    }
            };

            var settings = new ClubSetting { AutoChargePolicy = autoChargePolicy };

            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            return club;
        }

        private static AutoChargePolicySettingViewModel FillAutoChargePolicySettingViewModel()
        {
            return new AutoChargePolicySettingViewModel
            {
                CombinePayments = true,
                SendSummeryEmail = true,
                SuccessAdminEmailTemplate = "Test for SuccessAdminEmailTemplate",
                SuccessParentEmailTemplate = "Test for SuccessParentEmailTemplate",
                SuccessEmailToAdmin = true,
                Attempts = new System.Collections.Generic.List<AutoChargeAttemptViewModel> {
                    new AutoChargeAttemptViewModel
                    {
                        AttemptOrder= 1,
                        FailedEmailToAdmin= true,
                        FailedEmailToParent = true,
                        FailedAdminEmailTemplate= "Test for try AdminEmailTemplate",
                        FailedParentEmailTemplate= "Test for try ParentEmailTemplate"
                    },

                    new AutoChargeAttemptViewModel
                    {
                        AttemptOrder= 2,
                        FailedEmailToAdmin= true,
                        FailedEmailToParent = true,
                        IntervalDays=3.ToString(),
                        FailedAdminEmailTemplate= "Test for first AdminEmailTemplate",
                        FailedParentEmailTemplate= "Test for first ParentEmailTemplate",
                        Enabled = true
                    },

                    new AutoChargeAttemptViewModel
                    {
                        AttemptOrder=3,
                        FailedEmailToAdmin=false,
                        FailedEmailToParent = false,
                        IntervalDays=4.ToString(),
                        FailedAdminEmailTemplate= "Test for second AdminEmailTemplate",
                        FailedParentEmailTemplate= "Test for second ParentEmailTemplate",
                        Enabled=true
                    },

                    new AutoChargeAttemptViewModel
                    {
                        AttemptOrder=4,
                        FailedEmailToAdmin=false,
                        FailedEmailToParent = false,
                        IntervalDays=4.ToString(),
                        FailedAdminEmailTemplate= "Test for third AdminEmailTemplate",
                        FailedParentEmailTemplate= "Test for third ParentEmailTemplate",
                        Enabled=false
                    },

                    new AutoChargeAttemptViewModel
                    {
                        AttemptOrder=5,
                        FailedEmailToAdmin=false,
                        FailedEmailToParent = false,
                        Enabled=false,
                        IntervalDays=1.ToString(),
                        FailedAdminEmailTemplate= string.Empty,
                        FailedParentEmailTemplate= string.Empty
                    }
                }
            };
        }

        //DelinquentPolicy
        private static Club GetCreateClubWithNullDelinquentPolicy()
        {
            var club = FactoryGirl.Build<Club>();

            var settings = new ClubSetting { DelinquentPolicy = null };

            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            return club;
        }

        private static Club CreateClubWithDelinquentPolicy()
        {
            var club = FactoryGirl.Build<Club>();

            club.PartnerId = null;

            var delinquent = new DelinquentPolicy
            {
                AutoChargeFailAttemptNumber = 3,
                ManualInstallmentDelayDays = 0,
                AllowEnrollOnDelinquent = true,
                AllowEnrollTemplate = string.Empty,
                NotAllowEnrollTemplate = string.Empty,
                ExpelChildOnDelinquent = false,
                ExpelChildTemplate = string.Empty,
                FamilyBalanceNoLimitDate = new DateTime(2018, 11, 27, 0, 0, 0),
                FamilyBalanceMinPrice = 5.01M
            };

            var settings = new ClubSetting { DelinquentPolicy = delinquent };

            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            return club;
        }

        private static Club CreateClubWithPartnerAndDelinquentPolicy()
        {
            var club = FactoryGirl.Build<Club>();

            club.PartnerId = PartnerId;

            var delinquent = new DelinquentPolicy
            {
                AutoChargeFailAttemptNumber = 3,
                ManualInstallmentDelayDays = 0,
                AllowEnrollOnDelinquent = true,
                AllowEnrollTemplate = string.Empty,
                NotAllowEnrollTemplate = string.Empty,
                ExpelChildOnDelinquent = false,
                ExpelChildTemplate = string.Empty,
                FamilyBalanceNoLimitDate = new DateTime(2018, 11, 27, 0, 0, 0),
                FamilyBalanceMinPrice = 5.01M
            };

            var settings = new ClubSetting { DelinquentPolicy = delinquent };

            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            return club;
        }

        private static Club CreatePartnerWithDelinquentPolicyReadFromMember()
        {
            var club = FactoryGirl.Build<Club>();

            var partnerSetting = new PartnerSetting { PortalParameters = new PartnerSettingPortalParameter { ReadDelinquentPolicyFromMember = true } };

            club.SettingSerialized = JsonHelper.JsonSerializer(partnerSetting);

            return club;
        }

        private static Club CreatePartnerWithDelinquentPolicyReadFromPartner()
        {
            var club = FactoryGirl.Build<Club>();

            var delinquent = new DelinquentPolicy
            {
                AutoChargeFailAttemptNumber = 0,
                ManualInstallmentDelayDays = 1,
                AllowEnrollOnDelinquent = false,
                AllowEnrollTemplate = string.Empty,
                NotAllowEnrollTemplate = string.Empty,
                ExpelChildOnDelinquent = true,
                ExpelChildTemplate = string.Empty,
                FamilyBalanceNoLimitDate = new DateTime(2018, 12, 27, 0, 0, 0),
                FamilyBalanceMinPrice = 4.01M
            };

            var partnerSetting = new PartnerSetting { PortalParameters = new PartnerSettingPortalParameter { ReadDelinquentPolicyFromMember = false }, DelinquentPolicy = delinquent };

            club.SettingSerialized = JsonHelper.JsonSerializer(partnerSetting);

            return club;
        }

        private static DelinquentPolicySettingViewModel FillDelinquentPolicySettingViewModel()
        {
            return new DelinquentPolicySettingViewModel
            {
                AutoChargeFailAttempted = true,
                AutoChargeFailAttemptNumber = 2.ToString(),
                ManualInstallmentDelayed = true,
                ManualInstallmentDelayDays = 5.ToString(),
                AllowEnrollOnDelinquent = true,
                AllowEnrollTemplate = string.Empty,
                NotAllowEnrollTemplate = string.Empty,
                ExpelChildOnDelinquent = true,
                ExpelChildTemplate = string.Empty,
                CalculateFamilyBalance = true,
                FamilyBalanceNoLimitDate = new DateTime(2018, 11, 27, 0, 0, 0),
                FamilyBalanceMinPrice = 5.01M
            };
        }
        #endregion 


    }
}
