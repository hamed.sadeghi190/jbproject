﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('familyCreditCardListReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        $scope.FamilyReportDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetFamilyCreditCardList',
                        type: "POST",
                    },

                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },

            serverSorting: false,
            serverPaging: true,
            pageSize: 50,
            resizable: true,
        });



        $scope.FamilyCreditCardGridOptions = {
            excel: {
                allPages: true,
                fileName: "FamilyPaymentMethodGrid_Report.xlsx",
                proxyURL: "/Dashboard/PortalReport/Save"
            },
            excelExport: function (e) {
                cfpLoadingBar.complete();
            },
            dataSource: $scope.FamilyReportDataSource,
            resizable: true,
            pageable: {
                refresh: true,
                pageSizes:
                [
                    5, 10, 20, 30, 50, 100, 200
                ],
                buttonCount: 5,
            },
            dataBound: function () {
                var grid = $("#familyCreditCardReportGrid").data("kendoGrid");
                customReportService.initializeDoubleScroll(grid);
            },
            columnMenu: true,
            scrollable: true,
            sortable: true,
            columns: [
                 {
                     field: "UserName",
                     title: "Username",
                     width: 250,
                     template: "<span class='block'> #= UserName # </span>"
                 },
                {
                    field: "PlayersName",
                    title: "Participants",
                    width: "300px",
                    attributes: {style: "overflow: initial !important;white-space: normal;word-break: break-word;"},
                },
                   {
                       field: "LastDigits",
                       title: "Method",
                       width: "200px",
                       template: "#= LastDigits #"

                   },
                   {
                       field: "Name",
                       title: "Cardholder name",
                       width: "200px",
                   },
                    {
                        field: "ExpiryDate",
                        title: "Expiration",
                        width: "200px",
                    },
                
            ]
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#familyCreditCardReportGrid").getKendoGrid().saveAsExcel();
        }

    }
    ]);

})();