﻿using System;

using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Helper;
using Constants = Jumbula.Common.Constants.Constants;
using Jumbula.Core.Domain;

namespace SportsClub.Models.Registrant
{
    public class ParentActivityLogItemViewModel
    {
        public ParentActivityLogItemViewModel()
        {

        }

        public ParentActivityLogItemViewModel(JbAudit audit, Jumbula.Common.Enums.TimeZone ? clubTimeZone)
        {
            Id = audit.Id;
            Action = audit.Action;
            Description = audit.Description;
            DateTime = DateTimeHelper.ConvertUtcDateTimeToLocal(audit.DateTime, clubTimeZone).ToString(Constants.DefaultDateTimeFormatWithAMPM);
            UserName = audit.UserName;
            Status = audit.Status;
        }


        public Guid Id { get; set; }

        public string Action { get; set; }

        public string Description { get; set; }

        public string DateTime { get; set; }

        public string UserName { get; set; }

        public bool Status { get; set; }

    }

    public class ParentActivityLogFilter
    {
        public JbAction? Activity { get; set; }

        [Display(Name = "User")]
        public string UserName { get; set; }
    }

    public class ParentActivityLogDetailViewModel
    {
        public ParentActivityLogDetailViewModel()
        {
        }

        public ParentActivityLogDetailViewModel(JbAudit audit, Jumbula.Common.Enums.TimeZone ? clubTimeZone)
        {
            Activity = audit.Action;
            Description = audit.Description;
            DateTime = DateTimeHelper.ConvertUtcDateTimeToLocal(audit.DateTime, clubTimeZone).ToString(Constants.DefaultDateTimeFormatWithAMPM);
            UserName = audit.UserName;
            IpAdress = audit.IpAddress;
            Device = audit.Device;
            Browser = audit.Browser;
            StatusCode = audit.StatusCode.HasValue ? audit.StatusCode.ToString() : "";
            Status = audit.Status ? "OK" : "Fail";
        }

        public string Activity { get; set; }

        public string Description { get; set; }

        public string DateTime { get; set; }

        public string UserName { get; set; }

        public string IpAdress { get; set; }

        public string Device { get; set; }

        public string Browser { get; set; }

        public string StatusCode { get; set; }

        public string Status { get; set; }
    }

}