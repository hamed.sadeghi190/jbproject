﻿using System;
using FluentAssertions;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Jumbula.Common.Enums;
using Jumbula.Core.Model.Order;
using Jumbula.Core.Model.Email;

namespace Jumbula.Business.Tests.Unit
{
    [TestClass]
    public class OrderBusinessTest
    {
        #region Fields

        private Mock<IClubBusiness> _mockClubBusiness;
        private Mock<IOrderHistoryBusiness> _mockOrderHistoryBusiness;
  
        private Mock<IFormBusiness> _mockFormBusiness;
        private Mock<IRepository<Order, long>> _mockOrderRepository;
        private Mock<IRepository<Club>> _mockClubRepository;
        private Mock<IProgramSessionBusiness> _mockProgramSessionBusiness;
        private Mock<IOrderSessionBusiness> _mockOrderSessionBusiness;
        private Mock<IEmailBusiness> _mockEmailBusiness;
        private Mock<IEntitiesContext> _mockDataContext;
        private Mock<IOrderInstallmentBusiness> _mockOrderInstallmentBusiness;
        private Mock<IOrderItemBusiness> _mockOrderItemBusiness;


        #endregion

        #region Test methods
        [TestMethod]
        public void Refund_RefundAmountIsZero_ShouldReturnEqualResult()
        {
            // Arrange
            var service = CreateService();
            var refundAmount = 0m;
            var scheduleDatetime = new DateTime();

            var model = new OrderItemsViewModel
            {
                Id = 3,
                AddOnInstallments = null,
                CurrentUserName = "Support@jumbula.com",
                CurrentRoleType = RoleCategoryType.Dashboard,
            };

            var refundResultMode = new RefundResultModel()
            {
                Status = true,
                ErrorMessage="",
            
            };

            var emailParameter = new RefundEmailParameterModel
            {
                OrderItemId = model.Id == 0 ? (long?)null : model.Id,
                OrderInstallmentId = model.IntallmentId == 0 ? (long?)null : model.IntallmentId,
                RefundedAmount = refundAmount,
                RefundNote = model.CustomRefund.Name,
            };

            _mockOrderItemBusiness.Setup(x => x.Refund(model,ref refundAmount)).Returns(refundResultMode);
            _mockOrderInstallmentBusiness.Setup(x => x.Refund(model,ref refundAmount)).Returns(refundResultMode);

            // Act
            var result = service.Refund(model);

            // Assert
            result.ErrorMessage.Should().Be(refundResultMode.ErrorMessage);
            result.Status.Should().Be(refundResultMode.Status);
            _mockEmailBusiness.Verify(e => e.Send(EmailCategory.RefundOrder, emailParameter, scheduleDatetime), Times.Never);
        }

        #endregion

        #region Private methods
        private OrderBusiness CreateService()
        {
            _mockClubBusiness = new Mock<IClubBusiness>();
            _mockOrderInstallmentBusiness = new Mock<IOrderInstallmentBusiness>();
            _mockOrderSessionBusiness = new Mock<IOrderSessionBusiness>();
            _mockDataContext = new Mock<IEntitiesContext>();
            _mockProgramSessionBusiness = new Mock<IProgramSessionBusiness>();
            _mockFormBusiness = new Mock<IFormBusiness>();
            _mockOrderHistoryBusiness = new Mock<IOrderHistoryBusiness>();
            _mockOrderRepository = new Mock<IRepository<Order, long>>();
            _mockClubRepository = new Mock<IRepository<Club>>();
            _mockOrderItemBusiness = new Mock<IOrderItemBusiness>();
            _mockEmailBusiness = new Mock<IEmailBusiness>();

            var result = new OrderBusiness(_mockClubBusiness.Object,
                                               _mockOrderHistoryBusiness.Object,
                                               _mockFormBusiness.Object,
                                               _mockOrderRepository.Object,
                                               _mockProgramSessionBusiness.Object,
                                               _mockOrderSessionBusiness.Object,
                                               _mockClubRepository.Object, 
                                               _mockDataContext.Object
                                               , _mockOrderInstallmentBusiness.Object,
                                               _mockOrderItemBusiness.Object,
                                               _mockEmailBusiness.Object);

            return result;
        }

        #endregion
    }

}
