﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SchoolDistrictGridViewModel
    {
        public string Name { get; set; }
        public string Domain { get; set; }
        public string FirstProviderId { get; set; }
        public string SecondProviderId { get; set; }
        public int? NCESId { get; set; }
        public int Id { get; set; }
    }
}