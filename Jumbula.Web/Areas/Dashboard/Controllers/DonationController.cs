﻿using System.IO;
using System.Linq;
using System.Web.Mvc;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Controllers;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class DonationController : DashboardBaseController
    {
        // GET: Dashboard/Donation
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual JsonNetResult GetList(string term, int pageSize, int page)
        {
            var club = base.ActiveClub;
            var query = Ioc.OrderItemBusiness.GetList().Where(item => item.ItemStatus == OrderItemStatusCategories.completed && item.Order.ClubId == club.Id && item.OrderChargeDiscounts.Any(charge => !charge.IsDeleted && charge.Category == ChargeDiscountCategory.Donation));

            var model = query.OrderByDescending(r => r.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList().Select(item => new
            {
                Id = item.Id,
                OrderId = item.Order_Id,
                Attendee = item.FirstName.Equals(item.LastName) ? item.FirstName : item.FirstName + " " + item.LastName,
                OrderDate = DateTimeHelper.DateTimeLable(item.Order.CompleteDate, club.TimeZone),
                ConfirmationId = item.Order.ConfirmationId,
                PaymentStatus = item.ItemStatus.ToDescription(),
                Amount = item.TotalAmount,
                AmountStr = CurrencyHelper.FormatCurrencyWithPenny(item.TotalAmount, ActiveClub.Currency)
            })
             .ToList();

            return JsonNet(new { data = model, total = query.Count() }, new Newtonsoft.Json.JsonSerializerSettings(), "");
        }
        public virtual ActionResult Display()
        {
            return View();
        }
        public virtual ActionResult GetDonation(int orderItemId)
        {
            var orderItem = Ioc.OrderItemBusiness.GetItem(orderItemId);
            orderItem.JbForm.CurrentMode = AccessMode.ReadOnly;
            orderItem.JbForm.SetElementsCurrentMode(AccessMode.ReadOnly);

            var model = JsonNet(new
            {
                Editable = false,
                OrderId = orderItem.Order_Id,
                orderItem.Id,
                orderItem.Order.ConfirmationId,
                OrderDate = orderItem.Order.CompleteDate,
                OrderStatus = (int)orderItem.ItemStatus,
                Discounts = orderItem.GetOrderChargeDiscounts().Where(ocd => ocd.Category < 0).Select(ocd => new { ocd.Name, ocd.Amount, ocd.Category }),
                TotalAmount = CurrencyHelper.FormatCurrencyWithPenny(orderItem.TotalAmount, orderItem.Order.Club.Currency),
                JbFormHtml = RenderPartialViewToString("EditorTemplates/JbForm", orderItem.JbForm)
            });

            return model;
        }
        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
    }
}