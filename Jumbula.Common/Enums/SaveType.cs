﻿

namespace Jumbula.Common.Enums
{
    public enum SaveType
    {
        Draft = 0,
        Publish = 1,
    }
}
