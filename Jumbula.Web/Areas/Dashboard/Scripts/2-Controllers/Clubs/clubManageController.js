﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('clubManageController', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'members-manage-view';
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/Clubs", order: 1, menu: "Clubs" });

        $scope.MC.stateParams = $stateParams;

        $scope.baseUrl = window.location.origin + "/Dashboard/Club";
        $scope.pageTitle = "Add Club";
        $scope.clubId = 0;
        $scope.type = $stateParams.type;
        $scope.commisionRateRange = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
        if ($stateParams.clubId) {
            $scope.clubId = $stateParams.clubId;

        }
        if ($scope.clubId > 0)
        {
            $scope.pageTitle = "Edit Club";
        }
        $scope.pageMode = "";
        if ($scope.type == "School") {
            $scope.pageMode = "Manage School"
        } else {
            $scope.pageMode = "Manage Provider"
        }
        $scope.Model = {}

        $scope.clubTypeChanged = function () {
            $http.get('Dashboard/Club/IsClubSchool', { params: { typeId: $scope.Model.SelectedSubType } }).success(function (data, status, headers, config) {
                $scope.Model.IsSchool = data.result;
            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }

        $http.get($scope.baseUrl + '/CreateEditClub?clubId=' + $scope.clubId + '&type=' + $scope.type).success(function (data, status, headers, config) {

            $scope.Model = data;

        });

        $scope.saveClub = function () {
            $http.post($scope.baseUrl + '/SaveClub', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        if ($scope.type == "School") {
                            $state.go('Clubs', { type: 'Schools' });
                        } else {
                            $state.go('Clubs', { type: 'Providers' });
                        }
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        };


    }]);
})();