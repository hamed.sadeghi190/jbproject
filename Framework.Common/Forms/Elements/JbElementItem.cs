﻿

namespace Jb.Framework.Common.Forms
{
    ///<summary>
    ///This is Base Elemnt that using in other elemnts
    /// do not have UI implemention
    ///</summary>
    public class JbElementItem
    {

        public JbElementItem()
        {

        }

        public JbElementItem(string value, string text)
        {
            Value = value;
            Text = text;
        }

        public string Value { get; set; }
        public string Text { get; set; }
        public bool ReadOnly { get; set; }

        public static explicit operator JbElementItem(JbBaseElement jbElement)
        {
            return new JbElementItem(jbElement.Title, jbElement.GetValue().ToString());
        }
    }

    public class JbSelectedItem
    {
        public JbSelectedItem()
        {

        }
        public JbSelectedItem(bool value, string text)
        {
            Value = value;
            Text = text;
        }

        public bool Value { get; set; }
        public string Text { get; set; }

    }
}