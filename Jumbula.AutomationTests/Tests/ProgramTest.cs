﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Helper.Element;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace Jumbula.AutomationTests.Tests
{
    class ProgramTest : BaseTest<ProgramElements>
    {

        BladeElements bladeElementProvider;
        HomePageElements homepageElementProvider;

        public ProgramTest(IWebDriver webDriver)
            :base(webDriver)
        {
            bladeElementProvider = new ElementProvider<BladeElements>().Get(WebDriver);
            homepageElementProvider = new ElementProvider<HomePageElements>().Get(WebDriver);
        }

        public void CreateClass()
        {
            // Step 1
            Elements.ProgramNameTextbox.SendKeys(DataDriver.CalssName);
            Elements.CategoryList.Click();
            Elements.AcademicWritingCategory.Click();
            Elements.DescriptionTextbox.SendKeys("This is a text class created by automation test");

            Actions actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.SaveButton, 0, 500);
            //actions.Click(Elements.SaveButton);
            actions.Perform();
          
            //((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");

            ExtraAPI.Wait(1000);
            //Elements.SaveButton.Click();
        }
    }
}
