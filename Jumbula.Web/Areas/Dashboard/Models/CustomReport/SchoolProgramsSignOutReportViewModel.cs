﻿using System;
using System.Collections.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SchoolProgramsSignOutReportViewModel
    {
        public string ClassName { get; set; }
        public long ClassId { get; set; }      
        public string SchoolName { get; set; }
        public string SeasonName { get; set; }
        public string ClassDays { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public List<string> DatesOfDay { get; set; }
        public string StrTime {
            get {
                return string.Format("{0}-{1}", StartTime, EndTime);
            }
        }
        public string Instructor { get; set; }
        public string Location { get; set; }
        public List<ProgramParticipantListSchoolViewModel> ProgramParticipants { get; set; }
        public List<SignOutSheetReportAdvanceClubStaffViewModel> Staffs { get; set; }
        public string Date { get; set; }
        public string ReportDate { get; set; }
        public string GradesOrAges { get; set; }
        public int MinimumEnrollments { get; set; }
        public int Capacity { get; set; }
        public int Enrolled { get; set; }
        public int Pending { get; set; }
        public int WaitList { get; set; }
    }

}