﻿using Jumbula.Core.Data.DbHelpers;
using System;
using System.Data.Entity;

namespace Jumbula.Data.DbHelpers
{
    public class JbDbFunctions: IJbDbFunctions
    {
        public DateTime? TruncateTime(DateTime? dateValue)
        {
            return DbFunctions.TruncateTime(dateValue);
        }
    }
}
