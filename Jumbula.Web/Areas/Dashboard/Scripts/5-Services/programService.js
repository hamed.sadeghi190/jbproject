﻿(function () {
    'use strict';

    angular.module('jbTools').service("programService", ['$http', function ($http) {

        var _model = {};

        function setModel(data) {
            _model = data;
        }

        function getModel() {
            return _model;
        }

        var categories = function () {

            return new kendo.data.DataSource({
                type: "json",
                serverFiltering: true,
                transport:
                    {
                        read: {
                            url: "Dashboard/Program/GetCategories",
                           
                        },

                        parameterMap: function (data, type) {
                            if (type === "read") {
                                return {
                                    term: document.activeElement.value
                                }
                            }
                        }
                    }
            });
        }

        var saveStep1 = function (saveSuccess, saveError) {
            is.undefined(saveError) ? saveError = function () { console.error("Dashboard/Program/ProgramCreateEditStep1 got error.") } : true;
            $http.post('Dashboard/Program/ProgramCreateEditStep1', { model: _model })
                .success(saveSuccess)
                .error(saveError);
        }

        var getStep1Create = function (getSuccess, getError, params) {
            is.undefined(getError) ? getError = function () { console.error("Dashboard/Program/Create got error.") } : true;
            $http.get('Dashboard/Program/Create', { params: params })
                .success(getSuccess)
                .error(getError);
        }

        var getStep1Edit = function (getSuccess, getError, params) {
            is.undefined(getError) ? getError = function () { console.error("Dashboard/Program/Edit got error.") } : true;
            $http.get('Dashboard/Program/Edit', { params: params })
                .success(getSuccess)
                .error(getError);
        }

        var saveStep2 = function (saveSuccess, saveError) {
            is.undefined(saveError) ? saveError = function () { console.error("Dashboard/Program/ProgramCreateEditStep2 got error.") } : true;
            $http.post('Dashboard/Program/ProgramCreateEditStep2', { model: _model })
                .success(saveSuccess)
                .error(saveError);
        }

        var saveTournamentStep2 = function (saveSuccess, saveError) {
            is.undefined(saveError) ? saveError = function () { console.error("Dashboard/Program/TournamentCreateEditStep2 got error.") } : true;

            $http.post('Dashboard/Program/TournamentCreateEditStep2', { model: _model })
                .success(saveSuccess)
                .error(saveError);
        }

        var getStep2 = function (getSuccess, getError, params) {
            is.undefined(getError) ? getError = function () { console.error("Dashboard/Program/ProgramCreateEditStep2 got error.") } : true;

            $http.get('Dashboard/Program/ProgramCreateEditStep2', { params: params })
                .success(getSuccess)
                .error(getError);
        }


        var getTournamentStep2 = function (getSuccess, getError, params) {
            is.undefined(getError) ? getError = function () { console.error("Dashboard/Program/TournamentCreateEditStep2 got error.") } : true;

            $http.get('Dashboard/Program/TournamentCreateEditStep2', { params: params })
                .success(getSuccess)
                .error(getError);
        }

        var getCalendarStep2 = function (getSuccess, getError, params) {
            is.undefined(getError) ? getError = function () { console.error("Dashboard/Program/CalendarCreateEditStep2 got error.") } : true;

            $http.get('Dashboard/Program/CalendarCreateEditStep2', { params: params })
                .success(getSuccess)
                .error(getError);
        }

        var saveCalendarStep2 = function (saveSuccess, saveError) {
            is.undefined(saveError) ? saveError = function () { console.error("Dashboard/Program/CalendarCreateEditStep2 got error.") } : true;
            $http.post('Dashboard/Program/CalendarCreateEditStep2', { model: _model })
                .success(saveSuccess)
                .error(saveError);
        }

        var getSubscriptionStep2 = function (getSuccess, getError, params) {
            is.undefined(getError) ? getError = function () { console.error("Dashboard/Program/SubscriptionCreateEditStep2 got error.") } : true;

            $http.get('Dashboard/Program/SubscriptionCreateEditStep2', { params: params })
                .success(getSuccess)
                .error(getError);
        }

        var saveSubscriptionStep2 = function (isSubscriptionSchemaChanged, saveSuccess, saveError) {

            is.undefined(saveError) ? saveError = function () { console.error("Dashboard/Program/SubscriptionCreateEditStep2 got error.") } : true;
            $http.post('Dashboard/Program/SubscriptionCreateEditStep2', { model: _model, isSubscriptionSchemaChanged: isSubscriptionSchemaChanged })
                .success(saveSuccess)
                .error(saveError);
        }

        //Add Before & After Care
        var getBeforeAfterCareStep2 = function (getSuccess, getError, params) {
            is.undefined(getError) ? getError = function () { console.error("Dashboard/Program/BeforeAfterCareStep2CreateEditStep2 got error.") } : true;

            $http.get('Dashboard/Program/BeforeAfterCareCreateEditStep2', { params: params })
                .success(getSuccess)
                .error(getError);
        }

        var saveBeforeAfterCareStep2 = function (isSchemaChanged,isSchedulePartsChanged, saveSuccess, saveError) {

            is.undefined(saveError) ? saveError = function () { console.error("Dashboard/Program/BeforeAfterCareCreateEditStep2 got error.") } : true;
            $http.post('Dashboard/Program/BeforeAfterCareCreateEditStep2', { model: _model, isSchemaChanged: isSchemaChanged, isSchedulePartsChanged: isSchedulePartsChanged })
                .success(saveSuccess)
                .error(saveError);
        }

        var saveStep3 = function (saveSuccess, saveError) {
            is.undefined(saveError) ? saveError = function () { console.error("Dashboard/Program/ProgramCreateEditStep3 got error.") } : true;

            $http.post('Dashboard/Program/ProgramCreateEditStep3', { model: _model })
                .success(saveSuccess)
                .error(saveError);
        }

        var setJbFormToProgram = function () {


        }

        return {
            setModel: setModel,
            getModel: getModel,
            saveStep1: saveStep1,
            getStep1Create: getStep1Create,
            getStep1Edit: getStep1Edit,
            saveStep2: saveStep2,
            getStep2: getStep2,
            saveStep3: saveStep3,
            setJbFormToProgram: setJbFormToProgram,
            saveTournamentStep2: saveTournamentStep2,
            getTournamentStep2: getTournamentStep2,
            getCalendarStep2: getCalendarStep2,
            saveCalendarStep2: saveCalendarStep2,
            getSubscriptionStep2: getSubscriptionStep2,
            getBeforeAfterCareStep2:getBeforeAfterCareStep2,
            saveSubscriptionStep2: saveSubscriptionStep2,
            saveBeforeAfterCareStep2: saveBeforeAfterCareStep2
        }
    }]);
})();
