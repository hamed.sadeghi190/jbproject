﻿using SportsClub.Models;
using System.Collections.Generic;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SysAdminClubViewModel
    {
        public List<JbTitleValue> OrganizationTypes { get; set; }
        public List<JbTitleValue> OrganizationSubTypes { get; set; }
        public List<SelectKeyValue<string>> AccountTypes { get; set; }
        public List<SelectKeyValue<string>> PaymentMethods { get; set; }
    }


    public enum ClubAccountType : byte
    {
        All,
        Active,
        FreeTrial
    }
}