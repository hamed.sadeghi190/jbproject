﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class ProgramSession : BaseEntity
    {
        public DateTime StartDateTime { get; set; }

        public DateTime EndDateTime { get; set; }

        public bool IsDeleted { get; set; }

        //[ForeignKey("ProgramSchedulePartId")]
        //public virtual ProgramSchedulePart ProgramSchedulePart { get; set; }
        //public int? ProgramSchedulePartId { get; set; }

        [ForeignKey("ProgramScheduleId")]
        public virtual ProgramSchedule ProgramSchedule { get; set; }
        public long? ProgramScheduleId { get; set; }

        public virtual ICollection<OrderSession> OrderSessions { get; set; }

        public string WeeklyAgenda { get; set; }

        public ProgramSessionAgendaSendStatus? Status { get; set; }

        [ForeignKey("ChargeId")]
        public virtual Charge Charge { get; set; }

        public long? ChargeId { get; set; }
    }

    public class ProgramSchedulePart : BaseEntity
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        //public virtual ICollection<ProgramSession> Sessions { get; set; }

        public bool IsDeleted { get; set; }

        //[ForeignKey("ProgramScheduleId")]
        //public virtual ProgramSchedule ProgramSchedule { get; set; }
        //public long? ProgramScheduleId { get; set; }

        [ForeignKey("ProgramId")]
        public virtual Program Program { get; set; }
        public long? ProgramId { get; set; }
        public DateTime? DueDate { get; set; }

        public virtual ICollection<ProgramSchedulePartCharge> Charges { get; set; }
    }

    public class ProgramSchedulePartCharge : BaseEntity
    {
        [ForeignKey("SchedulePartId")]
        public virtual ProgramSchedulePart SchedulePart { get; set; }

        public int SchedulePartId { get; set; }

        [ForeignKey("ChargeId")]
        public virtual Charge Charge { get; set; }

        public long ChargeId { get; set; }

        public decimal ChargeAmount { get; set; }
    }


    public enum ProgramSessionAgendaSendStatus : byte
    {
        Procecced,
        Stopped
    }
}
