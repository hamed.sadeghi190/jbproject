﻿namespace Jumbula.Core.Model.Payment
{
    public class JbPaymentResult
    {
        public bool Succeed { set; get; }
        public string Message { set; get; }
        public string OrderMode { set; get; }
        public string ClubDomain { set; get; }
        public string UserName { set; get; }
    }
}
