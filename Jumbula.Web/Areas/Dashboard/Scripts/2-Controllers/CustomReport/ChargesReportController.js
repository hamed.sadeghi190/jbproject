﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('ChargesReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.Model = customReportService.getModelFromStrorage();

        var model = $scope.Model;

        $http.post(baseUrl + '/FillBaseFilters', { model: $scope.Model })
            .success(function (data, status, headers, config) {
                if (is.undefined($scope.Model.Filters)) {
                    $scope.Model.Filters = {};
                    $scope.Model.Filters.Status = "regular";
                    $scope.Model.Filters.Tuition = "";
                }
                $scope.Model.Filters.Tuitions = data.Tuitions;
                $scope.Model.Filters.OrderStatus = data.OrderStatus;
            }).error(function (data, status, headers, config) {
            });

        $scope.filterData = function () {
            $("#chargesReportGrid").data("kendoGrid").dataSource.read();
        }

        $scope.clearFilters = function () {
            $scope.Model.Filters = {};
            $scope.searchTerm = "";
            $scope.Model.Filters.RegistrationStart = "";
            $scope.Model.Filters.RegistrationEnd = "";
            $scope.Model.Filters.Status = "regular";
            $scope.Model.Filters.Tuition = "";

            $("#chargesReportGrid").data("kendoGrid").dataSource.read();
            $http.post(baseUrl + '/FillBaseFilters', { model: $scope.Model }).success(function (data, status, headers, config) {
                $scope.Model.Filters.Tuitions = data.Tuitions;
                $scope.Model.Filters.OrderStatus = data.OrderStatus;
            });
        }

        $scope.chargesReportsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/ProgramChargesReportRun',
                        type: "POST",
                        data: function () {
                            return {
                                model: model,
                                term: $scope.searchTerm,
                            };
                        },
                    },

                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverSorting: false,
            serverPaging: true,
            pageSize: 10,
            batch: true,
            resizable: true,
        });

        var columns = [];
        $scope.oldColumns = [];

        $scope.chargesReportGridOptions = {
            excel: {
                allPages: true,
                fileName: "ChargeDiscount (" + $scope.Model.SeasonDomain + ").xlsx",
                proxyURL: "/Dashboard/CustomReport/Save"
            },
            excelExport: function (e) {
                cfpLoadingBar.complete();
            },
            dataSource: $scope.chargesReportsDataSource,
            sortable: { mode: 'single' },
            resizable: true,
            columnMenu: true,
            pageable: {
                refresh: true,
                pageSizes:
                [
                    5, 10, 20, 30, 50, 100
                ],
                buttonCount: 5
            },
            dataBound: function (e) {
                customReportService.initializeDoubleScroll();
                $scope.oldColumns = $("#chargesReportGrid").data("kendoGrid").columns;
            },
            dataBinding: function (e) {

            }
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#chargesReportGrid").getKendoGrid().saveAsExcel();
        }

        $(document).bind("ajaxComplete.changeColumns", function () {
            if ($scope.oldColumns && $scope.oldColumns.length > 0) {
                for (var i = 0; i < $scope.oldColumns.length; i++) {
                    var column = [];
                    if ($scope.oldColumns[i].field == 'Full_Name') {
                        column = {
                            field: $scope.oldColumns[i].field,
                            title: "Full name",
                            template: "<a style='overflow: hidden;text-overflow: ellipsis;' ng-click='goOrderItemDetail(#=OrderItemId#)'  class='block'> #= Full_Name # </a> #if(ItemStatusReason == 1) {# <i class='grid-label label info'>Transferred in</i> #}else if(ItemStatusReason == 2) {# <i class='grid-label label bg-red  fg-white'>Transferred out</i> #}else if(ItemStatusReason == 5) {# <i class='grid-label label bg-red fg-white'>Refund</i> #} else if (ItemStatusReason == 4){# <i class='grid-label label warning'>Canceled</i> #}#"
                        }
                    } else if ($scope.oldColumns[i].field == 'Confirmation') {
                        column = {
                            field: $scope.oldColumns[i].field,
                            template: "<a style='overflow: hidden;text-overflow: ellipsis;' ng-click='goOrderDetail(#=OrderId#)' class='block'> #= Confirmation # </a>",
                            title: "Confirmation"
                        }
                    } else if ($scope.oldColumns[i].field == 'OrderId' || $scope.oldColumns[i].field == 'OrderItemId' || $scope.oldColumns[i].field == 'ItemStatusReason') {
                        column = {
                            field: $scope.oldColumns[i].field,
                            hidden: true
                        }
                    } else {
                        column = {
                            field: $scope.oldColumns[i].field,
                            title: $scope.oldColumns[i].field
                                .replace(/^(_(s)_)/, "(")
                                .replace(/(_(p)_)/, ")")
                                .replace(/_/g, " ").trimLeft()
                        }
                    }
                    columns.push(column);
                }
                $("#chargesReportGrid").data("kendoGrid").setOptions({
                    columns: columns,
                });
            }
            $(document).unbind("ajaxComplete.changeColumns");
        });

        $scope.MakeCampaign = function () {
            $http.post(baseUrl + '/ProgramChargesReportRun', { model: $scope.Model, makeCampaign: true, term: $scope.searchTerm })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $scope.MC.openBlade({ url: "Dashboard/Blade/Seasons", order: 0, menu: "SEASONS" });
                        $state.go('EmailCampaignAddStep2', { campaignId: data.Data, campaignType: 'UserList' });
                    } else {
                        jbToast.error(data.Message);
                    }
                });
        }

        $scope.goOrderDetail = function (orderId) {
            customReportService.setModelInStorage($scope.Model);
            customReportService.goOrderDetail($scope.MC.stateParams.seasonDomain, orderId);
        }

        $scope.goOrderItemDetail = function (orderItemId) {
            customReportService.setModelInStorage($scope.Model);
            customReportService.goOrderItemDetail($scope.MC.stateParams.seasonDomain, orderItemId);
        }

        $scope.shareReport = function () {
            $scope.Model.Filters.Term = $scope.searchTerm;
            customReportService.setModelInStorage($scope.Model);
            $state.go('ShareAsAttchment', { seasonDomain: $scope.MC.stateParams.seasonDomain, reportName: "ChargeDiscount" });
        }
    }]);
})();