

IF OBJECT_ID('[dbo].[Events]') IS NULL
BEGIN
CREATE TABLE [dbo].[Events](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[ClubDomain] [nvarchar](max) NULL,
	[Domain] [nvarchar](max) NULL,
	[SeasonId] [int] NULL,
	[Description] [nvarchar](max) NULL,
	[Note] [nvarchar](max) NULL,
	[EventType] [int] NOT NULL,
	[EventStatus] [int] NOT NULL,
	[TimeZone] [int] NOT NULL,
	[SaveType] [int] NOT NULL,
	[EventAttributes] [nvarchar](max) NULL,
	[ClubLocationId] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[Club_Id] [int] NULL,
	[Instructor] nvarchar(max) NULL
	)
END
GO

---<<ADD PRIMARY KEY>>--
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'PK_dbo.Events')
BEGIN
	ALTER TABLE [dbo].[Events] ADD CONSTRAINT [PK_dbo.Events] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	) 
END
GO

--<< Edit Columns >>--
IF EXISTS(select 1 from sys.columns where OBJECT_ID=OBJECT_ID('[dbo].[Events]') AND name='ClubDomain' AND is_nullable=1)
BEGIN	
	ALTER TABLE [dbo].[Events]
	ALTER COLUMN [ClubDomain] nvarchar(100) not null
END
IF EXISTS(select 1 from sys.columns where OBJECT_ID=OBJECT_ID('[dbo].[Events]') AND name='Domain' AND is_nullable=1)
BEGIN	
	ALTER TABLE [dbo].[Events]
	ALTER COLUMN [Domain] nvarchar(100) not null
END

--<< ADD COLUMNS >>--
IF NOT EXISTS(select 1 from sys.columns where OBJECT_ID=OBJECT_ID('[dbo].[Events]') AND name='LastCreatedPage')
BEGIN
	ALTER TABLE [dbo].[Events]
	ADD [LastCreatedPage] int not null default 4
	print 'add LastCreatedPage column'
	exec('Update [Events] set [LastCreatedPage] = 0 where [Savetype] = 0');
	exec('Update [Events] set [LastCreatedPage] = 4 where [Savetype] = 1');	
END
IF NOT EXISTS(select 1 from sys.columns where OBJECT_ID=OBJECT_ID('[dbo].[Events]') AND name='Instructor')
BEGIN	
	ALTER TABLE [dbo].[Events]
	ADD [Instructor] nvarchar(max) null
END
IF NOT EXISTS(select 1 from sys.columns where OBJECT_ID=OBJECT_ID('[dbo].[Events]') AND name='SeasonId')
BEGIN	
	ALTER TABLE [dbo].[Events]
	ADD [SeasonId] bigint null
END

--<< DROP COLUMNS >>--
IF  EXISTS(select 1 from sys.columns where OBJECT_ID=OBJECT_ID('[dbo].[Events]') AND name='Savetype')
BEGIN	
	EXEC [dbo].[JbDropColumn] @tablename='dbo.Events' ,@fieldname='Savetype';
	print 'drop Savetype column'
END


---<<ADD Foreign Keys>>--
IF NOT EXISTS(select 1 from sys.objects where name='FK_dbo.Events_dbo.ClubLocations_ClubLocationId')
BEGIN
	ALTER TABLE [dbo].[Events]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Events_dbo.ClubLocations_ClubLocationId] FOREIGN KEY([ClubLocationId])
	REFERENCES [dbo].[ClubLocations] ([Id])
	ON DELETE CASCADE
END
GO

IF NOT EXISTS(select 1 from sys.objects where name='FK_dbo.Events_dbo.Clubs_Club_Id')
BEGIN
	ALTER TABLE [dbo].[Events]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Events_dbo.Clubs_Club_Id] FOREIGN KEY([Club_Id])
	REFERENCES [dbo].[Clubs] ([Id])
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name='FK_dbo.Events_dbo.Seasons_SeasonId')
BEGIN
	ALTER TABLE [dbo].[Events]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Events_dbo.Seasons_SeasonId] FOREIGN KEY([SeasonId])
	REFERENCES [dbo].[Seasons] ([Id]);
END
GO


declare @TableName nvarchar(50)='Events' --without dbo.
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'FK_'+@TableName+'_JbForms')
BEGIN
	declare @command nvarchar(500)='ALTER TABLE [dbo].['+@TableName+'] Add JbForm_Id int null';
	exec(@command);
	print 'JbForm_Id column created successfully ';

	set @command='ALTER TABLE [dbo].['+@TableName+']  WITH CHECK ADD  CONSTRAINT [FK_'+@TableName+'_JbForms] FOREIGN KEY([JbForm_Id])
	REFERENCES [dbo].[JbForms] ([Id])
	ALTER TABLE [dbo].['+@TableName+'] CHECK CONSTRAINT [FK_'+@TableName+'_JbForms]';
	exec(@command);
	print 'FK created successfully ';
	END
	ELSE
	BEGIN
		print 'Fk has already existed';
END



--<< ADD CONSTRAINT >>--
IF NOT EXISTS(SELECT 1 FROM sys.objects WHERE name='UK_Events_Domains')
BEGIN
	ALTER TABLE [dbo].[Events] ADD CONSTRAINT [UK_Events_Domains] UNIQUE NONCLUSTERED 
	(
		[ClubDomain] ASC,
		[SeasonId] ASC,
		[Domain] ASC
	)
END