
CREATE TABLE [dbo].[CampaignCallbackDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Email] [nvarchar](max) NULL,
	[EmailStatus] [int] NOT NULL,
	[CampaignId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.CampaignCallbackDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[CampaignCallbackDetails]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CampaignCallbackDetails_dbo.Campaigns_CampaignId] FOREIGN KEY([CampaignId])
REFERENCES [dbo].[Campaigns] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[CampaignCallbackDetails] CHECK CONSTRAINT [FK_dbo.CampaignCallbackDetails_dbo.Campaigns_CampaignId]


