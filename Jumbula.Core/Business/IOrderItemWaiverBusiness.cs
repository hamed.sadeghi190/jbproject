﻿using Jb.Framework.Common;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IOrderItemWaiverBusiness
    {
        OperationStatus Create(OrderItemWaiver orderItemWaivar);
        IQueryable<OrderItemWaiver> GetList();
        OrderItemWaiver Get(int WaiverId);
    }
}
