﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class TeachersPdfViewModel
    {
        public string ParticipantName { get; set; }
        public string TeacherName { get; set; }

        public string ClassName { get; set; }

        public string ClassDays { get; set; }

        public string Room { get; set; }
    }
}