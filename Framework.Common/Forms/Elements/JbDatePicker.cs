﻿using System;
using System.Text;
using System.Threading.Tasks;
using Jb.Framework.Common.Forms.JsonFormatter;
using Newtonsoft.Json;

namespace Jb.Framework.Common.Forms
{

    #region DatePicker

    [JsonConverter(typeof(JbElementConverter))]
    public class JbDatePicker : JbBaseElement
    {
        public DateTime? Value { get; set; }

        public string HelpText { get; set; }
    }

    #endregion

}
