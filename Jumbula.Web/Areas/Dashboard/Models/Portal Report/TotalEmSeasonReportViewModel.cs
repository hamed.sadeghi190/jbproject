﻿using System;
using Jumbula.Common.Enums;

namespace SportsClub.Areas.Dashboard.Models
{
    public class TotalEmSeasonReportViewModel
    {
        public string SchoolSeasonname { get; set; }
        public CurrencyCodes Currency { get; set; }
        /// <summary>
        /// ////
        /// </summary>
        public int TotalRegistrations { get; set; }

       
        public int TotalPaidRegistrations { get; set; }

        public decimal SubtotalClassFeesPaid { get; set; }

        public decimal ProvFundedScholarship { get; set; }    
        public decimal CustomDiscount { get; set; }
        public decimal CustomCharge { get; set; }
        public decimal TotalPaymentsCollected
        {
            get
            {
                return TotalPTAActivityFeesReceived + TotalPTAdonationsreceived + Totalfees + SubtotalClassFeesPaid + TotalLateRegistrationfees + CustomDiscount + CustomCharge;
            }
        }
        public decimal TotalCashPayments { get; set; }
        public decimal TotalPTAfundedScholarship { get; set; }

        public decimal Totalfees { get; set; }


        public decimal Totalproviderdeductions { get; set; }

        public decimal TotalCancellationFees { get; set; }
        public decimal TotalLateRegistrationfees { get; set; }

        public decimal TotalPTAdonationsreceived { get; set; }
        public decimal TotalPTAActivityFeesReceived { get; set; }


        public decimal TotalGrossIncome
        {
            get
            {
                return TotalPaymentsCollected + TotalCashPayments  + TotalPTAfundedScholarship  ;
            }
        }
        public decimal? Totalproviderpayments { get; set; }
        public decimal TotalPTApayments
        {
            get
            {
                return (TotalPTAActivityFeesReceived + TotalPTAdonationsreceived);
            }
        }


        public decimal Totaljumbulafee
        {
            get
            {
                return Convert.ToDecimal(TotalPaymentsCollected * Convert.ToDecimal(0.0085));
            }
        }

        public decimal TotalStripeFees
        {
            get
            {
                return Convert.ToDecimal(TotalPaymentsCollected * Convert.ToDecimal(0.029));
            }
        }

        public decimal? TotalNetEMIncome
        {
            get
            {
                return Math.Abs(TotalGrossIncome) - Math.Abs(Totalproviderpayments.HasValue ? Totalproviderpayments.Value : 0) - Math.Abs(TotalPTApayments) - Math.Abs(Totaljumbulafee) - Math.Abs(TotalStripeFees);
            }
        }

    }

}