﻿using Jb.Framework.Common.Forms;
using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Core.Model.Report;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Infrastructure.Flyer;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.UserService;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using WebGrease.Css.Extensions;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class PortalReportController : DashboardBaseController
    {
        #region Fields
        private readonly object _thisLock = new object();
        private readonly IOrderInstallmentBusiness _orderInstallmentBusiness;
        private readonly IProgramBusiness _programBusiness;
        private readonly IClubBusiness _clubBusiness;
        private readonly IUserCreditCardBusiness _userCreditCardBusiness;
        private readonly ISeasonBusiness _seasonBusiness;
        private readonly INewReportBusiness _reportBusiness;
        private readonly ITransactionActivityBusiness _transactionActivityBusiness;

        #endregion

        #region Constractors
        public PortalReportController(IOrderInstallmentBusiness orderInstallmentBusiness, IClubBusiness clubBusiness, IUserCreditCardBusiness userCreditCardBusiness,
            IProgramBusiness programBusiness, ISeasonBusiness seasonBusiness, INewReportBusiness reportBusiness, ITransactionActivityBusiness transactionActivityBusiness)
        {
            _orderInstallmentBusiness = orderInstallmentBusiness;
            _clubBusiness = clubBusiness;
            _userCreditCardBusiness = userCreditCardBusiness;
            _programBusiness = programBusiness;
            _seasonBusiness = seasonBusiness;
            _reportBusiness = reportBusiness;
            _transactionActivityBusiness = transactionActivityBusiness;
        }
        #endregion

        public virtual ActionResult UpdateDate()
        {
            return View("CatalogUpdateDates");
        }

        public virtual ActionResult Title1Discount()
        {
            return View();
        }

        public virtual ActionResult CountyProviders()
        {
            return View();
        }
        public virtual ActionResult RenewalPTAReports()
        {
            return View();
        }
        public virtual ActionResult ProviderContactInfo()
        {
            return View();
        }
        public virtual ActionResult installmentsReport()
        {
            return View();
        }
        public virtual ActionResult ClientCreditCardsReport()
        {
            return View();
        }
        public virtual ActionResult PricingReport()
        {
            return View();
        }

        public virtual ActionResult FullClassListReport()
        {
            return View();
        }

        public virtual ActionResult AllDiscountReports()
        {
            return View();
        }

        public virtual ActionResult ADM24Data()
        {
            return View();
        }

        public virtual ActionResult InsuranceReport()
        {
            return View();
        }

        public virtual ActionResult ProviderAddressListReport()
        {
            return View();
        }

        public virtual ActionResult MemberListReport()
        {
            return View();
        }

        public virtual ActionResult InstructorListPortalReport()
        {
            return View();
        }

        public virtual ActionResult InsuranceCertificateCheckReport()
        {
            return View();
        }

        public virtual ActionResult ClassProfitsReports()
        {
            return View();
        }

        public virtual ActionResult SeasonSurveyDatesReport()
        {
            return View();
        }

        public virtual ActionResult InstructorAssignmentsPortalReport()
        {
            return View();
        }

        public virtual ActionResult FullSeasonClassListReport()
        {
            return View();
        }

        public virtual ActionResult FollowupReportPortal()
        {
            return View();
        }

        public virtual ActionResult InstructorCheckinPortalReport()
        {
            return View();
        }

        public virtual ActionResult SchoolAddressListReport()
        {
            return View();
        }

        public virtual ActionResult DiscountReport()
        {
            return View();
        }

        public virtual ActionResult DonationsReport()
        {
            return View();
        }

        public virtual ActionResult ParentMailingListReport()
        {
            return View();
        }
        public virtual ActionResult FamilyCreditCardListReport()
        {
            return View();
        }

        public virtual ActionResult BulkDependentCareReceiptReport()
        {
            return View();
        }

        [JbAuthorize(JbAction.ProtalReport_Finance)]
        public virtual ActionResult SchoolSeasonsFinanceReport()
        {

            return View();
        }

        [JbAuthorize(JbAction.ProtalReport_Finance)]
        public virtual ActionResult SchoolSeasonsReport()
        {

            return View();
        }
        [JbAuthorize(JbAction.ProtalReport_Finance)]
        public virtual ActionResult EMSeasonReconciliationReport()
        {

            return View();
        }
        public virtual ActionResult SelectPrograms()
        {

            return View();
        }
        public virtual ActionResult ReportSelectProviders()
        {
            return View();
        }
        public virtual ActionResult ReportSelectSchools()
        {
            return View();
        }
        public virtual ActionResult InvoicesListPortalReport()
        {
            return View();
        }
        [JbAuthorize(JbAction.ProtalReport_Finance)]
        public virtual ActionResult GeneralTransactionsReport()
        {

            return View();
        }
        public virtual ActionResult ReminderInvoicesListFromReport()
        {
            return View();
        }
        public virtual ActionResult InternalRosterReports()
        {
            return View();
        }
        public virtual ActionResult CampRosterReports()
        {
            return View();
        }

        public virtual ActionResult SelectRegistrantsSeason()
        {

            return View();
        }

        public virtual ActionResult SelectEMSeason()
        {

            return View();
        }

        public virtual ActionResult SchoolReconciliationInfoReport()
        {

            return View();
        }

        public virtual ActionResult ReconciliationPrepReport()
        {
            return View();
        }

        public virtual ActionResult MemberTaxIdStatusReport()
        {
            return View();

        }

        public virtual ActionResult SessionStatementDataReport()
        {
            return View();

        }

        public virtual ActionResult ScholarshipReport()
        {
            return View();
        }

        public virtual ActionResult SelectCounty()
        {
            return View();
        }

        public virtual ActionResult CatalogOverviewActivityReport()
        {
            return View();
        }

        public virtual ActionResult CatalogDetailedActivitiesReport()
        {
            return View();
        }

        public virtual ActionResult SelectSeason()
        {
            return View();
        }

        public virtual ActionResult SelectSchoolSeason()
        {
            return View();
        }
        public virtual ActionResult SchoolProgramsListCheckInOut()
        {
            return View();
        }


        public virtual ActionResult SelectProviderSeason()
        {
            return View();
        }

        public virtual ActionResult RoomAssignments()
        {
            return View();
        }

        public virtual ActionResult SeasonProcessingDates()
        {
            return View();
        }

        public virtual ActionResult SelectDisocuntDate()
        {
            return View();
        }

        public virtual ActionResult SchoolMemberList()
        {
            return View();
        }
        public virtual ActionResult CustomReport()
        {
            return View();
        }
        public virtual ActionResult CustomReportAdd()
        {
            return View();
        }
        public virtual ActionResult CustomReportEdit()
        {
            return View();
        }
        public virtual ActionResult CustomReportRun()
        {
            return View();
        }
        public virtual ActionResult CustomReportSelectPrograms()
        {
            return View();
        }
        public virtual ActionResult BaseFilters()
        {
            return View("_BaseFilters");
        }

        public virtual ActionResult GraduatesReports()
        {
            return View();
        }
        public virtual ActionResult OnsitePersonCheckinReport()
        {
            return View();
        }
        public virtual ActionResult EnrollmentReports()
        {
            return View();
        }
        public virtual ActionResult TeachersReport()
        {
            return View();
        }
        public virtual ActionResult TeacherParticipantsClass()
        {
            return View();
        }

        public virtual ActionResult SchoolEnrollmentsReports()
        {
            return View();
        }
        public virtual ActionResult ParticipantByGradeReports()
        {
            return View();
        }
        public virtual JsonNetResult GetAllProviders(string seasonDomain, string reportName)
        {
            var clubId = ActiveClub.Id;

            var allProviders = new List<SelectKeyValue<string>>();

            allProviders =
              _clubBusiness.GetRelatedClubs(clubId)
                  .Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.Provider && !c.IsInActive)
                  .Select(c => new SelectKeyValue<string> { Text = c.Name, Value = c.Id.ToString() })
                  .OrderBy(c => c.Text).ToList();


            var model = new { AllProviders = allProviders, IsAllProviders = "true" };

            return JsonNet(model);
        }
        [HttpGet]
        public virtual JsonNetResult GetAllPartners()
        {
            var result = _clubBusiness.GetAllPartners();

            result.AddItemToFirst(DefaultDropdownItem.All);

            return JsonNet(result);
        }
        [HttpGet]
        public virtual JsonNetResult GetAllSchools()
        {
            var clubId = ActiveClub.Id;

            var allSchools = new List<SelectKeyValue<string>>();

            allSchools =
              _clubBusiness.GetRelatedClubs(clubId)
                  .Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.School)
                  .Select(c => new SelectKeyValue<string> { Text = c.Name, Value = c.Id.ToString() })
                  .OrderBy(c => c.Text).ToList();


            var model = new { AllSchools = allSchools, IsAllSchools = "true" };

            return JsonNet(model);
        }

       [HttpGet]
        public virtual JsonNetResult GetSchoolsAndProviders(int partnerId)
        {
            var result = new List<SelectListItem>();
            var schoolsAndProviders = _clubBusiness.GetRelatedClubs(partnerId, false); //_clubBusiness.GetAllSchoolsAndProvider();

            result.AddItemToFirst(DefaultDropdownItem.All);

            result.AddRange(schoolsAndProviders.Select(p => new SelectListItem() { Text = p.Name, Value = p.Id.ToString() })
                   .OrderBy(s => s.Text).ToList());

            return JsonNet(result);
        }

        public virtual JsonNetResult SelectProgramsList(long seasonId, PaginationModel paginationModel)
        {
            var finances = new List<FinanceReportItemViewModel>();
            var programBusiness = Ioc.ProgramBusiness;

            var coupons = Ioc.CouponBusiness.GetList(seasonId).Where(c => c.Deleted == false && c.IsAllProgram == true);
            var club = _clubBusiness.Get(ActiveClub.Id);

            var orderItems = Ioc.OrderItemBusiness.GetList().Where(c => c.SeasonId == seasonId && c.ProgramScheduleId.HasValue && c.ItemStatus == OrderItemStatusCategories.completed && c.Order.IsLive);

            var groupOrderItem = orderItems.GroupBy(c => c.ProgramSchedule.ProgramId);

            foreach (var itemP in groupOrderItem)
            {
                var ListOrderItems = itemP.ToList();

                var program = Ioc.ProgramBusiness.Get(itemP.Key);

                List<int> cashId = new List<int>();
                List<int> pta = new List<int>();
                List<int> providerFundedId = new List<int>();

                var totalOrderItem = ListOrderItems.Sum(o => o.TotalAmount);

                var transferfee = ListOrderItems.Where(c => c.ItemStatusReason == OrderItemStatusReasons.transferIn).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.TransferFee).Sum(c => Math.Abs(c.Amount));

                var totalCharge = ListOrderItems.SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.PartnerSurcharge || c.Category == ChargeDiscountCategory.Surcharge || c.Category == ChargeDiscountCategory.CustomCharge).Sum(c => c.Amount);

                var totalDiscount = ListOrderItems.SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.CustomDiscount).Sum(c => c.Amount);

                if (program.Coupons != null && program.Coupons.Any(c => c.Name.ToLower().StartsWith("Cash Payment".ToLower())))// Cash Payment to PTA
                {
                    cashId.AddRange(program.Coupons.Where(c => c.Name.ToLower().StartsWith("Cash Payment".ToLower())).Select(c => c.Id));
                }
                if (coupons != null && coupons.Any(c => c.Name.ToLower().StartsWith("Cash Payment".ToLower())))
                {
                    cashId.AddRange(coupons.Where(c => c.Name.ToLower().StartsWith("Cash Payment".ToLower())).Select(c => c.Id));
                }

                if (program.Coupons != null && program.Coupons.Any(c => c.Name.ToLower().StartsWith("PTO-Funded".ToLower())))
                {
                    pta.AddRange(program.Coupons.Where(c => c.Name.ToLower().StartsWith("PTO-Funded".ToLower())).Select(c => c.Id));
                }
                if (coupons != null && coupons.Any(c => c.Name.ToLower().StartsWith("PTO-Funded".ToLower())))
                {
                    pta.AddRange(coupons.Where(c => c.Name.ToLower().StartsWith("PTO-Funded".ToLower())).Select(c => c.Id)); //PTA-Funded 50% Scholarship
                }

                if (program.Coupons != null && program.Coupons.Any(c => c.Name.ToLower().StartsWith("PTA-Funded".ToLower())))
                {
                    pta.AddRange(program.Coupons.Where(c => c.Name.ToLower().StartsWith("PTA-Funded".ToLower())).Select(c => c.Id));
                }
                if (coupons != null && coupons.Any(c => c.Name.ToLower().StartsWith("PTA-Funded".ToLower())))
                {
                    pta.AddRange(coupons.Where(c => c.Name.ToLower().StartsWith("PTA-Funded".ToLower())).Select(c => c.Id)); //PTA-Funded 50% Scholarship
                }

                if (program.Coupons != null && program.Coupons.Any(c => c.Name.StartsWith("Provider-Funded", StringComparison.OrdinalIgnoreCase)))
                {
                    providerFundedId.AddRange(program.Coupons.Where(c => c.Name.ToLower().StartsWith("Provider-Funded".ToLower())).Select(c => c.Id));
                }
                if (coupons != null && coupons.Any(c => c.Name.ToLower().StartsWith("Provider-Funded".ToLower())))
                {
                    providerFundedId.AddRange(coupons.Where(c => c.Name.ToLower().StartsWith("Provider-Funded".ToLower())).Select(c => c.Id));
                }


                finances.Add(new FinanceReportItemViewModel
                {
                    TotalAmtofClassFeesCollected = totalOrderItem - totalCharge - transferfee,
                    Currency = club.Currency,
                    ProviderName = (program.OutSourceSeasonId.HasValue && program.OutSourceSeason != null) ? program.OutSourceSeason.Club.Name : "-",
                    ClassName = (program.Name != null) ? program.Name : "-",
                    TotalRegistrations = ListOrderItems.Any() ? ListOrderItems.Count() : 0,

                    ClassFee = program.ProgramSchedules.First().Charges.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).First().Amount,

                    EMRate = (program.CustomFields.CommisionRate.HasValue && program.CustomFields.CommisionRate != 0) ? program.CustomFields.CommisionRate.Value : (program.OutSourceSeason != null ? program.OutSourceSeason.Club.PartnerCommisionRate : (int?)null),

                    TotalPTAPTO = !pta.Any() ? 0 : ListOrderItems.Where(c => c.OrderChargeDiscounts != null).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Coupon && c.CouponId.HasValue && pta.Contains(c.CouponId.Value)).Sum(c => Math.Abs(c.Amount)),
                    CashPaymentsPTAPTO = !cashId.Any() ? 0 : ListOrderItems.Where(c => c.OrderChargeDiscounts != null).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Coupon && c.CouponId.HasValue && cashId.Contains(c.CouponId.Value)).Sum(c => Math.Abs(c.Amount)),
                    TotalAmtProviderFundedScholarships = !providerFundedId.Any() ? 0 : ListOrderItems.Where(c => c.OrderChargeDiscounts != null).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Coupon && c.CouponId.HasValue && providerFundedId.Contains(c.CouponId.Value)).Sum(c => Math.Abs(c.Amount)),
                    ProviderPenaltiesandNoShowFines = (program.CustomFields != null && program.CustomFields.Noshowpenalty.HasValue) ? (program.CustomFields.Noshowpenalty.Value) : 0,
                    ProviderPenaltiesandLatePenaltyFines = (program.CustomFields != null && program.CustomFields.LateCheckInPenalty.HasValue) ? (program.CustomFields.LateCheckInPenalty.Value) : 0

                });

            }
            return JsonNet(new { DataSource = finances, TotalCount = 0 });
        }

        public virtual JsonNetResult SelectOrderList(long seasonId, int schoolId, PaginationModel paginationModel)
        {
            var schoolregistrant = new List<SeasonRegistrantReportViewModel>();

            var orderItems = Ioc.OrderItemBusiness.GetList().Where(c => c.Order.IsLive && ((c.SeasonId == seasonId && c.ProgramScheduleId.HasValue) || (c.ProgramScheduleId == null && c.Order.ClubId == schoolId && c.SeasonId == seasonId)) && c.Order.ClubId == schoolId && (c.ItemStatus == OrderItemStatusCategories.completed || (c.ItemStatus == OrderItemStatusCategories.changed && c.ItemStatusReason == OrderItemStatusReasons.canceled) || (c.ItemStatusReason == OrderItemStatusReasons.transferOut && c.ItemStatus == OrderItemStatusCategories.changed)));

            var seasonOrder = orderItems.GroupBy(o => o.Order_Id);

            List<int> cashId = new List<int>();
            List<int> pta = new List<int>();
            List<int> providerFundedId = new List<int>();

            foreach (var order in seasonOrder)
            {
                decimal? RefundAmountorder = 0;
                decimal canceltotal = 0;

                var canceledorderItems = Ioc.OrderItemBusiness.GetList().Where(c => c.SeasonId == seasonId && c.Order.ClubId == schoolId && c.Order_Id == order.Key && c.ItemStatusReason == OrderItemStatusReasons.canceled && c.ItemStatus == OrderItemStatusCategories.changed).ToList();

                var cancels = canceledorderItems.SelectMany(i => i.OrderChargeDiscounts).Where(c => c.Category == ChargeDiscountCategory.CancellationFee);

                if (cancels.Any())
                {
                    canceltotal = cancels.Sum(c => (decimal?)c.Amount) ?? 0;
                }

                var orderitemList = order.ToList();
                var coupons = Ioc.CouponBusiness.GetList(seasonId).Where(c => c.Deleted == false && c.IsAllProgram == true);

                foreach (var orderitem in orderitemList)
                {

                    if (orderitem.ProgramScheduleId.HasValue && orderitem.ProgramSchedule.Program.Coupons != null && orderitem.ProgramSchedule.Program.Coupons.Any(c => c.Name.ToLower().StartsWith("Cash Payment".ToLower())))// Cash Payment to PTA
                    {
                        cashId.AddRange(orderitem.ProgramSchedule.Program.Coupons.Where(c => c.Name.ToLower().StartsWith("Cash Payment".ToLower())).Select(c => c.Id));
                    }
                    if (coupons != null && coupons.Any(c => c.Name.ToLower().StartsWith("Cash Payment".ToLower())))
                    {
                        cashId.AddRange(coupons.Where(c => c.Name.ToLower().StartsWith("Cash Payment".ToLower())).Select(c => c.Id));
                    }

                    if (orderitem.ProgramScheduleId.HasValue && orderitem.ProgramSchedule.Program.Coupons != null && orderitem.ProgramSchedule.Program.Coupons.Any(c => c.Name.ToLower().StartsWith("PTO-Funded".ToLower())))
                    {
                        pta.AddRange(orderitem.ProgramSchedule.Program.Coupons.Where(c => c.Name.ToLower().StartsWith("PTO-Funded".ToLower())).Select(c => c.Id));
                    }
                    if (coupons != null && coupons.Any(c => c.Name.ToLower().StartsWith("PTO-Funded".ToLower())))
                    {
                        pta.AddRange(coupons.Where(c => c.Name.ToLower().StartsWith("PTO-Funded".ToLower())).Select(c => c.Id)); //PTA-Funded 50% Scholarship
                    }

                    if (orderitem.ProgramScheduleId.HasValue && orderitem.ProgramSchedule.Program.Coupons != null && orderitem.ProgramSchedule.Program.Coupons.Any(c => c.Name.ToLower().StartsWith("PTA-Funded".ToLower())))
                    {
                        pta.AddRange(orderitem.ProgramSchedule.Program.Coupons.Where(c => c.Name.ToLower().StartsWith("PTA-Funded".ToLower())).Select(c => c.Id));
                    }
                    if (coupons != null && coupons.Any(c => c.Name.ToLower().StartsWith("PTA-Funded".ToLower())))
                    {
                        pta.AddRange(coupons.Where(c => c.Name.ToLower().StartsWith("PTA-Funded".ToLower())).Select(c => c.Id)); //PTA-Funded 50% Scholarship
                    }

                    if (orderitem.ProgramScheduleId.HasValue && orderitem.ProgramSchedule.Program.Coupons != null && orderitem.ProgramSchedule.Program.Coupons.Any(c => c.Name.ToLower().StartsWith("Provider-Funded".ToLower())))
                    {
                        providerFundedId.AddRange(orderitem.ProgramSchedule.Program.Coupons.Where(c => c.Name.ToLower().StartsWith("Provider-Funded".ToLower())).Select(c => c.Id));
                    }
                    if (coupons != null && coupons.Any(c => c.Name.ToLower().StartsWith("Provider-Funded".ToLower())))
                    {
                        providerFundedId.AddRange(coupons.Where(c => c.Name.ToLower().StartsWith("Provider-Funded".ToLower())).Select(c => c.Id));
                    }
                }
                foreach (var orderitem in orderitemList)
                {
                    var orderTransactions = Ioc.TransactionActivityBusiness.GetAllTransactionByOrderId(orderitem.Order.Id).Where(t => t.TransactionType == TransactionType.Refund).ToList();

                    if (orderTransactions.Any())
                    {
                        RefundAmountorder = orderTransactions.Sum(o => o.Amount);
                    }
                }
                try
                {
                    schoolregistrant.Add(new SeasonRegistrantReportViewModel
                    {

                        PTAPTODonationpaid = orderitemList.Where(c => c.ItemStatus == OrderItemStatusCategories.completed && c.ProgramScheduleId == null && c.SeasonId == seasonId).Sum(o => o.TotalAmount),
                        RegistrantName = string.Join(", ", orderitemList.Where(c => c.ProgramScheduleId != null).Select(c => c.FullName).Distinct()),
                        Confirmation = orderitemList.First().Order.ConfirmationId,
                        TotalClassFees = orderitemList.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).Sum(c => (decimal?)c.EntryFee) ?? 0,
                        totalamount = orderitemList.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).Sum(o => o.TotalAmount),
                        RefundAmount = RefundAmountorder,
                        ActivityFeepaid = orderitemList.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).SelectMany(c => c.OrderChargeDiscounts).Where(c => c.Category == ChargeDiscountCategory.Surcharge).Sum(c => (decimal?)Math.Abs(c.Amount)) ?? 0,
                        CashPayment = !cashId.Any() ? 0 : orderitemList.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).SelectMany(c => c.OrderChargeDiscounts).Where(c => c.Category == ChargeDiscountCategory.Coupon && c.CouponId.HasValue && cashId.Contains(c.CouponId.Value)).Sum(c => (decimal?)Math.Abs(c.Amount)) ?? 0,
                        AmtPTAPTOfundedScholarship = !pta.Any() ? 0 : orderitemList.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).SelectMany(c => c.OrderChargeDiscounts).Where(c => c.Category == ChargeDiscountCategory.Coupon && c.CouponId.HasValue && pta.Contains(c.CouponId.Value)).Sum(c => (decimal?)Math.Abs(c.Amount)) ?? 0,
                        ProvFundedScholarship = !providerFundedId.Any() ? 0 : orderitemList.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).SelectMany(c => c.OrderChargeDiscounts).Where(c => c.Category == ChargeDiscountCategory.Coupon && c.CouponId.HasValue && providerFundedId.Contains(c.CouponId.Value)).Sum(c => (decimal?)Math.Abs(c.Amount)) ?? 0,
                        LateRegistrationfees = orderitemList.ToList().Where(c => c.ItemStatus == OrderItemStatusCategories.completed).SelectMany(c => c.OrderChargeDiscounts).Where(c => c.Category == ChargeDiscountCategory.LateRegistrationFee).Any() ? orderitemList.Where(c => c.OrderChargeDiscounts.Any() && c.ItemStatus == OrderItemStatusCategories.completed).SelectMany(c => c.OrderChargeDiscounts).Where(c => c.Category == ChargeDiscountCategory.LateRegistrationFee).Sum(c => (decimal?)Math.Abs(c.Amount)) ?? 0 : 0,
                        feepaid = orderitemList.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).SelectMany(c => c.OrderChargeDiscounts).Where(c => c.Category == ChargeDiscountCategory.PartnerSurcharge).Sum(c => (decimal?)c.Amount) ?? 0,
                        CancellationFeePaid = canceltotal,
                        CustomDiscount = orderitemList.Where(c => c.OrderChargeDiscounts != null && c.ItemStatus == OrderItemStatusCategories.completed).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.CustomDiscount).Sum(c => (decimal?)c.Amount) ?? 0,
                        CustomCharge = orderitemList.Where(c => c.OrderChargeDiscounts != null && c.ItemStatus == OrderItemStatusCategories.completed).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.CustomCharge).Sum(c => (decimal?)c.Amount) ?? 0,

                    });
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("The order with id {0} has problem.", order.Key), ex);
                }

            }

            return JsonNet(new { DataSource = schoolregistrant, TotalCount = 0 });
        }

        public virtual JsonNetResult SelectEMSeasonList(long seasonId, int schoolId, PaginationModel paginationModel)
        {

            var programBusiness = Ioc.ProgramBusiness;

            var totalEMSeasonList = new List<TotalEmSeasonReportViewModel>();

            var allSeasons = Ioc.SeasonBusiness.GetList().Where(s => s.ClubId == schoolId && s.Status != SeasonStatus.Deleted)
                    .Select(s => new SelectKeyValue<long>() { Text = s.Title, Value = s.Id });

            var season = Ioc.SeasonBusiness.Get(seasonId);
            var isTestMode = season.Status == SeasonStatus.Test;
            var seasonName = season.Title.Replace('.', '_');
            var orderItems = Ioc.OrderItemBusiness.GetList().Where(c => c.Order.IsLive && ((c.SeasonId == seasonId && c.ProgramScheduleId.HasValue) || (c.ProgramScheduleId == null && c.Order.ClubId == schoolId && c.SeasonId == seasonId)) && c.Order.ClubId == schoolId && (c.ItemStatus == OrderItemStatusCategories.completed));

            List<int> cashId = new List<int>();
            List<int> pta = new List<int>();
            List<int> providerFundedId = new List<int>();

            decimal totalClassFeeslessProviderFundedScholarships = 0;
            decimal? amtofEMFeetoDeduct = 0;
            decimal providerPenaltiesandNoShowFines = 0;
            decimal providerPenaltiesandLatePenaltyFines = 0;
            var totalRegistrationsProgram = 0;

            var coupons = Ioc.CouponBusiness.GetList(seasonId).Where(c => !c.Deleted && c.IsAllProgram);
            var count = 0;
            foreach (var orderitem in orderItems)
            {
                decimal charge = orderitem.OrderChargeDiscounts.Where(c => !c.IsDeleted && c.Subcategory == ChargeDiscountSubcategory.Charge).Sum(c => Math.Abs(c.Amount));

                decimal cash = 0;
                if (orderitem.ProgramScheduleId.HasValue && orderitem.ProgramSchedule.Program.Coupons != null && orderitem.ProgramSchedule.Program.Coupons.Any(c => c.Name.ToLower().StartsWith("Provider-Funded".ToLower())))
                {
                    cash = orderitem.ProgramSchedule.Program.Coupons.Where(c => c.Name.ToLower().StartsWith("Cash Payment".ToLower())).Sum(c => Math.Abs(c.Amount));
                }
                if (orderitem.PaidAmount == charge + cash)
                {
                    count++;
                }
                if (orderitem.ProgramScheduleId.HasValue && orderitem.ProgramSchedule.Program.Coupons != null && orderitem.ProgramSchedule.Program.Coupons.Any(c => c.Name.ToLower().StartsWith("Cash Payment".ToLower())))// Cash Payment to PTA
                {
                    cashId.AddRange(orderitem.ProgramSchedule.Program.Coupons.Where(c => c.Name.ToLower().StartsWith("Cash Payment".ToLower())).Select(c => c.Id));
                }
                if (coupons != null && coupons.Any(c => c.Name.ToLower().StartsWith("Cash Payment".ToLower())))
                {
                    cashId.AddRange(coupons.Where(c => c.Name.ToLower().StartsWith("Cash Payment".ToLower())).Select(c => c.Id));
                }
                if (orderitem.ProgramScheduleId.HasValue && orderitem.ProgramSchedule.Program.Coupons != null && orderitem.ProgramSchedule.Program.Coupons.Any(c => c.Name.ToLower().StartsWith("PTO-Funded".ToLower())))
                {
                    pta.AddRange(orderitem.ProgramSchedule.Program.Coupons.Where(c => c.Name.ToLower().StartsWith("PTO-Funded".ToLower())).Select(c => c.Id));
                }
                if (coupons != null && coupons.Any(c => c.Name.ToLower().StartsWith("PTO-Funded".ToLower())))
                {
                    pta.AddRange(coupons.Where(c => c.Name.ToLower().StartsWith("PTO-Funded".ToLower())).Select(c => c.Id)); //PTA-Funded 50% Scholarship
                }

                if (orderitem.ProgramScheduleId.HasValue && orderitem.ProgramSchedule.Program.Coupons != null && orderitem.ProgramSchedule.Program.Coupons.Any(c => c.Name.ToLower().StartsWith("PTA-Funded".ToLower())))
                {
                    pta.AddRange(orderitem.ProgramSchedule.Program.Coupons.Where(c => c.Name.ToLower().StartsWith("PTA-Funded".ToLower())).Select(c => c.Id));
                }
                if (coupons != null && coupons.Any(c => c.Name.ToLower().StartsWith("PTA-Funded".ToLower())))
                {
                    pta.AddRange(coupons.Where(c => c.Name.ToLower().StartsWith("PTA-Funded".ToLower())).Select(c => c.Id)); //PTA-Funded 50% Scholarship
                }
                if (orderitem.ProgramScheduleId.HasValue && orderitem.ProgramSchedule.Program.Coupons != null && orderitem.ProgramSchedule.Program.Coupons.Any(c => c.Name.ToLower().StartsWith("Provider-Funded".ToLower())))
                {
                    providerFundedId.AddRange(orderitem.ProgramSchedule.Program.Coupons.Where(c => c.Name.ToLower().StartsWith("Provider-Funded".ToLower())).Select(c => c.Id));
                }
                if (coupons != null && coupons.Any(c => c.Name.ToLower().StartsWith("Provider-Funded".ToLower())))
                {
                    providerFundedId.AddRange(coupons.Where(c => c.Name.ToLower().StartsWith("Provider-Funded".ToLower())).Select(c => c.Id));
                }
            }

            var programs = Ioc.ProgramBusiness.GetSeasonPrograms(seasonId);

            foreach (var program in programs)
            {
                decimal classFeeslessProviderFundedScholarships = 0;
                decimal totalAmtProviderFundedScholarships = 0;
                decimal total = 0;
                decimal classFee = 0;
                int? emRate = 0;

                totalAmtProviderFundedScholarships = programBusiness.GetTotalAmtProviderFundedScholarships(program, coupons.ToList());

                totalRegistrationsProgram = programBusiness.TotalRegistration(program, program.Season.Status == SeasonStatus.Test);

                providerPenaltiesandNoShowFines += (program.CustomFields != null && program.CustomFields.Noshowpenalty.HasValue) ? (program.CustomFields.Noshowpenalty.Value) : 0;
                providerPenaltiesandLatePenaltyFines += (program.CustomFields != null && program.CustomFields.LateCheckInPenalty.HasValue) ? (program.CustomFields.LateCheckInPenalty.Value) : 0;

                classFee = programBusiness.GetEmClassFee(program);
                emRate = programBusiness.GetEMRate(program);
                total = classFee * totalRegistrationsProgram;
                classFeeslessProviderFundedScholarships = total - totalAmtProviderFundedScholarships;

                totalClassFeeslessProviderFundedScholarships += total - totalAmtProviderFundedScholarships;
                amtofEMFeetoDeduct += classFeeslessProviderFundedScholarships * emRate / 100;
            }

            decimal canceltotal = 0;

            var totalclassfees = orderItems.Any() ? orderItems.Sum(c => c.EntryFee) : 0;
            var provfundedscholarship = !providerFundedId.Any() ? 0 : orderItems.ToList().Where(c => c.OrderChargeDiscounts != null).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Coupon && c.CouponId.HasValue && providerFundedId.Contains(c.CouponId.Value)).Sum(c => (decimal?)Math.Abs(c.Amount)) ?? 0;
            var totalPTAfundedscholarship = !pta.Any() ? 0 : orderItems.ToList().Where(c => c.OrderChargeDiscounts != null).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Coupon && c.CouponId.HasValue && pta.Contains(c.CouponId.Value)).Sum(c => (decimal?)Math.Abs(c.Amount)) ?? 0;

            var totalcashpayments = !cashId.Any() ? 0 : orderItems.ToList().Where(c => c.OrderChargeDiscounts != null).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Coupon && c.CouponId.HasValue && cashId.Contains(c.CouponId.Value)).Sum(c => (decimal?)Math.Abs(c.Amount)) ?? 0;

            var canceledorderItems = Ioc.OrderItemBusiness.GetList().Where(c => c.SeasonId == seasonId && c.ItemStatusReason == OrderItemStatusReasons.canceled && c.ItemStatus == OrderItemStatusCategories.changed).ToList();
            var cancels = canceledorderItems.SelectMany(i => i.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.CancellationFee);
            if (cancels.Any())
            {
                canceltotal = cancels.Sum(c => c.Amount);
            }

            var subtotalfee = (totalclassfees - (Math.Abs(provfundedscholarship) + Math.Abs(totalPTAfundedscholarship) + Math.Abs(totalcashpayments))) < 0 ? 0 : (totalclassfees - (Math.Abs(provfundedscholarship) + Math.Abs(totalPTAfundedscholarship) + Math.Abs(totalcashpayments)));

            var donation = orderItems.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).Where(o => o.ProgramSchedule == null && o.SeasonId == seasonId);
            var activity = orderItems.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge);

            totalEMSeasonList.Add(new TotalEmSeasonReportViewModel
            {
                SchoolSeasonname = seasonName,
                TotalRegistrations = orderItems.Any() ? orderItems.Where(o => o.ProgramScheduleId.HasValue).Count() : 0,
                TotalPaidRegistrations = orderItems.Count() - orderItems.Count(i => i.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Coupon)),
                ProvFundedScholarship = provfundedscholarship,
                TotalCashPayments = totalcashpayments,
                TotalPTAfundedScholarship = totalPTAfundedscholarship,
                TotalCancellationFees = canceltotal,
                Totalfees = orderItems.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.PartnerSurcharge).Any() ? orderItems.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).SelectMany(c => c.OrderChargeDiscounts).Where(c => c.Category == ChargeDiscountCategory.PartnerSurcharge).Sum(c => (decimal?)c.Amount) ?? 0 : 0,
                SubtotalClassFeesPaid = subtotalfee,
                TotalLateRegistrationfees = orderItems.ToList().Where(c => c.ItemStatus == OrderItemStatusCategories.completed).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.LateRegistrationFee).Any() ? orderItems.Where(c => c.OrderChargeDiscounts.Any() && c.ItemStatus == OrderItemStatusCategories.completed).SelectMany(c => c.OrderChargeDiscounts).Where(c => c.Category == ChargeDiscountCategory.LateRegistrationFee).Sum(c => (decimal?)Math.Abs(c.Amount)) ?? 0 : 0,
                TotalPTAdonationsreceived = donation.Any() ? donation.Sum(o => o.TotalAmount) : 0,
                TotalPTAActivityFeesReceived = activity.Any() ? activity.Sum(o => o.Amount) : 0,
                Totalproviderdeductions = providerPenaltiesandNoShowFines + providerPenaltiesandLatePenaltyFines,
                Totalproviderpayments = totalClassFeeslessProviderFundedScholarships - amtofEMFeetoDeduct - (providerPenaltiesandNoShowFines + providerPenaltiesandLatePenaltyFines),

                CustomDiscount = orderItems.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.CustomDiscount).Sum(c => (decimal?)c.Amount) ?? 0,

                CustomCharge = orderItems.Where(c => c.ItemStatus == OrderItemStatusCategories.completed).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.CustomCharge).Sum(c => (decimal?)c.Amount) ?? 0,
            });

            return JsonNet(new { DataSource = totalEMSeasonList, TotalCount = 0 });
        }

        public virtual JsonNetResult GetReoprtNameWithSeason(long seasonId, int schoolId)
        {
            var seasonName = Ioc.SeasonBusiness.Get(seasonId).Title.Replace('.', '_');
            var schoolName = _clubBusiness.Get(schoolId).Name.Replace('.', '_');
            var reportTitle = string.Format("{0}_{1}", seasonName, schoolName);
            return JsonNet(reportTitle);
        }

        public virtual ActionResult GetUpdateDateForSubClubs(PaginationModel paginationModel, DateTime? startDate, DateTime? endDate)
        {
            var relatedClub = _clubBusiness.GetRelatedClubs(ActiveClub.Id).Where(c => !c.IsDeleted);

            //var catalogItems = (relatedClub.Where(club => club.CatalogItems.Any())
            //    .Select(club => club.CatalogItems.OrderByDescending(c => c.MetaData.DateUpdated).FirstOrDefault())).ToList();
            if (startDate.HasValue || endDate.HasValue)
            {
                relatedClub = relatedClub.Where(c => c.CatalogItems.Any());
                if (startDate.HasValue)
                {
                    if (endDate.HasValue)
                    {
                        relatedClub =
                            relatedClub.Where(
                                c =>
                                    DbFunctions.TruncateTime(c.CatalogItems.Where(ca => ca.Status != CatalogStatus.Deleted).OrderByDescending(ca => ca.MetaData.DateUpdated)
                                        .FirstOrDefault()
                                        .MetaData.DateUpdated) >= startDate.Value &&
                                   DbFunctions.TruncateTime(c.CatalogItems.Where(ca => ca.Status != CatalogStatus.Deleted).OrderByDescending(ca => ca.MetaData.DateUpdated)
                                        .FirstOrDefault()
                                        .MetaData.DateUpdated) <= endDate.Value);
                    }
                    else
                    {
                        relatedClub =
                            relatedClub.Where(
                                c =>
                                    DbFunctions.TruncateTime(c.CatalogItems.Where(ca => ca.Status != CatalogStatus.Deleted).OrderByDescending(ca => ca.MetaData.DateUpdated)
                                        .FirstOrDefault()
                                        .MetaData.DateUpdated) >= startDate.Value);
                    }
                }
                else if (endDate.HasValue)
                {
                    relatedClub =
                        relatedClub.Where(c => DbFunctions.TruncateTime(c.CatalogItems.Where(ca => ca.Status != CatalogStatus.Deleted).OrderByDescending(ca => ca.MetaData.DateUpdated)
                            .FirstOrDefault()
                            .MetaData.DateUpdated) <= endDate.Value);
                }
            }

            var model = new List<CatalogUpdateViewModel>();

            model.AddRange(relatedClub.OrderBy(c => c.Id).Skip((paginationModel.Page - 1) * paginationModel.PageSize)
                .Take(paginationModel.PageSize).AsNoTracking()
                .Select(club => new CatalogUpdateViewModel
                {
                    ClubId = club.Id,
                    ClubName = club.Name,
                    UpdateDate =
                        (club.CatalogItems.Any())
                            ? club.CatalogItems.Where(ca => ca.Status != CatalogStatus.Deleted).OrderByDescending(c => c.MetaData.DateUpdated)
                                .FirstOrDefault()
                                .MetaData.DateUpdated
                            : (DateTime?)null
                }));
            //model.Where(m => m.UpdateDate.HasValue).ForEach(m => m.UpdateStr = Utilities.ConvertUtcDateTimeToLocal(m.UpdateDate.Value, ActiveClub.TimeZone).ToString("MM/dd/yyyy H:mm"));
            model.Where(m => m.UpdateDate.HasValue).ForEach(m => m.UpdateStr = DateTimeHelper.ConvertUtcDateTimeToLocal(m.UpdateDate.Value, ActiveClub.TimeZone).ToString("MM/dd/yyyy H:mm"));

            return JsonNet(new { DataSource = model, TotalCount = relatedClub.Count() });
        }

        public virtual ActionResult GetClubCatalogDetails(PaginationModel paginationModel, int clubId)
        {
            var clubcatalogs = _clubBusiness.Get(clubId).CatalogItems.Where(ca => ca.Status != CatalogStatus.Deleted);

            var model = clubcatalogs.OrderBy(c => c.Id).Skip((paginationModel.Page - 1) * paginationModel.PageSize)
                .Take(paginationModel.PageSize).Select(c => new CatalogUpdateItemsViewModel
                {
                    CatalogItem = c.Name,
                    UpdateDate = c.MetaData.DateUpdated
                }).ToList();

            model.ForEach(m => m.UpdateDateStr = DateTimeHelper.ConvertUtcDateTimeToLocal(m.UpdateDate, ActiveClub.TimeZone).ToString("MM/dd/yyyy H:mm"));
            return JsonNet(new { DataSource = model, TotalCount = clubcatalogs.Count() });
        }

        [HttpPost]
        public virtual ActionResult GetCatalogDetailedActivitiesReportInfo(PaginationModel paginationModel, SelectedProvidersViewModel model, DateTime? startDate, DateTime? endDate, int activeStatus)
        {
            var clubBusiness = _clubBusiness;

            var result = new List<CatalogListReportViewModel>();

            var providers = _clubBusiness.GetRelatedClubs(ActiveClub.Id).Where(c => !c.IsDeleted);

            if (model._SelectedProviders == null)
            {
                providers = providers.Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.Provider);
            }
            else if (model._SelectedProviders != null)
            {
                var providerIds = model._SelectedProviders;
                providers = providers.Where(c => providerIds.Contains(c.Id));
            }


            switch (activeStatus)
            {
                case 1: //Active
                    providers = providers.Where(c => !c.IsInActive);
                    break;

                case 2: // InActive
                    providers = providers.Where(c => c.IsInActive);
                    break;
            }



            foreach (var club in providers)
            {
                var catalogs = club.CatalogItems.Where(c => c.Status != CatalogStatus.Deleted);

                if (startDate.HasValue)
                {
                    catalogs = catalogs.Where(c => c.MetaData.DateUpdated >= startDate.Value);
                }

                if (endDate.HasValue)
                {
                    endDate = endDate.Value.AddDays(1).Date;
                    catalogs = catalogs.Where(c => c.MetaData.DateUpdated <= endDate.Value);
                }

                var catalogSettings = Ioc.CatalogSettingBusiness.Get(club.Id);
                if (catalogSettings == null)
                {
                    Ioc.CatalogSettingBusiness.Create(club.Id);

                    catalogSettings = Ioc.CatalogSettingBusiness.Get(club.Id);
                }

                foreach (var catalog in catalogs)
                {

                    var priceOptions = GetDefaultPriceOptions(ActiveClub.Id);

                    foreach (var group in priceOptions)
                    {
                        foreach (var price in group.Prices)
                        {
                            foreach (var item in catalog.PriceOptions)
                            {
                                if (price.Title.Equals(item.Title) && group.Title.Equals(item.DurationTitle))
                                {

                                    result.Add(new CatalogListReportViewModel()
                                    {
                                        ProviderName = club.Name,
                                        CatalogName = catalog.Name,
                                        UpdateDate = catalog.MetaData.DateUpdated.ToString(Constants.DateTime_Comma),
                                        MinGrade = catalog.AttendeeRestriction.MinGrade.ToDescription(),
                                        MaxGrade = catalog.AttendeeRestriction.MaxGrade.ToDescription(),
                                        MinEnrollment = catalog.MinimumEnrollment.HasValue ? catalog.MinimumEnrollment.Value : 0,
                                        MaxEnrollment = catalog.MaximumEnrollment.HasValue ? catalog.MaximumEnrollment.Value : 0,
                                        Duration = int.Parse(group.Title.Replace(" minutes", "")),
                                        Week = int.Parse(price.Title.Replace(" - week", "")),
                                        Price = item.Amount,
                                        ActivityCategories = catalog.Categories.Select(c => new CategoryViewModel
                                        {
                                            Name = c.Name,
                                            Id = c.Id
                                        }).ToList(),

                                        ActivityDescription = catalog.Description,
                                        CountiesDistricts = string.Join(", ", catalogSettings.CatalogSettingCounties.Where(c => !c.Maybe).ToList().Select(n => n.County.Name)),
                                    });

                                }

                            }

                        }

                    }
                }

            }

            var dataSource = result.OrderBy(o => o.ProviderName).ThenBy(o => o.CatalogName).Skip((paginationModel.Page - 1) * paginationModel.PageSize).Take(paginationModel.PageSize).ToList();

            return JsonNet(new { DataSource = dataSource, TotalCount = result.Count() });
        }

        private List<CatalogPriceGroupViewModel> GetDefaultPriceOptions(int clubId)
        {
            var catalogPriceOptionGroups = Ioc.CatalogSettingBusiness.GetDefaultCatalogPriceOptions(clubId);

            var priceOptionGropus = catalogPriceOptionGroups.GroupBy(p => p.DurationTitle).Select(b =>
                new CatalogPriceGroupViewModel
                {
                    Title = b.Key,
                    IsDefault = true,
                    Prices = b.Select(p =>
                    new CatalogPriceViewModel
                    {
                        Amount = null,
                        Id = p.Id,
                        IsDefault = true,
                        Title = p.Title,
                        ListAgendas = GetAgendaList(p.Title),
                    })
                    .ToList()
                })
                .ToList();

            return priceOptionGropus;
        }

        private List<AgendaViewModel> GetAgendaList(string priceTitle)
        {
            var result = new List<AgendaViewModel>();

            var weeks = 0;
            try
            {
                int weekLocation = priceTitle.IndexOf("-", StringComparison.Ordinal);
                weeks = int.Parse(priceTitle.Substring(0, weekLocation).Trim());
            }
            catch
            {
                return result;
            }

            for (int i = 1; i <= weeks; i++)
            {
                result.Add(new AgendaViewModel()
                {
                    Title = string.Format("Week {0} parent talking point", i),
                    AgendaWeek = " ",

                });
            }

            return result;

        }

        public virtual ActionResult SendEmailToMembers(int[] membersIds, PortalReportType reportName)
        {
            List<string> memebersUserName;
            if (reportName >= 0)
            {
                memebersUserName = (membersIds.Select(id => Ioc.UserProfileBusiness.Get(id)).Where(contact => contact != null).Select(contact => contact.UserName)).ToList();
            }
            else
            {
                memebersUserName = (membersIds.Select(id => _clubBusiness.Get(id).ContactPersons.FirstOrDefault())
                .Where(contact => contact != null)
                .Select(contact => contact.Email)).ToList();
            }

            var campaignId = 0;
            var result = Ioc.CampaignBusiness.MakeCampaignOnReport(memebersUserName, ActiveClub.Domain, reportName.ToDescription());
            campaignId = result.RecordsAffected;
            return Json(new { result.Status, Data = campaignId, Message = result.ExceptionMessage });
        }

        public virtual ActionResult Title1DiscountProviders(PaginationModel paginationModel, string activeStatus, int? countyId)
        {
            var relatedClubs = _clubBusiness.GetRelatedClubs(ActiveClub.Id);

            if (activeStatus == "Active")
            {
                relatedClubs = relatedClubs.Where(c => c.IsInActive == false);
            }
            else if (activeStatus == "Inactive")
            {
                relatedClubs = relatedClubs.Where(c => c.IsInActive == true);
            }

            var clubWithDiscountTitle = countyId.HasValue
                ? relatedClubs.Where(
                    c =>
                        c.CatalogSetting.HasTitle1SchoolDiscounts && !c.IsDeleted &&
                        c.CatalogSetting.CatalogSettingCounties.Any(county => county.CountyId == countyId))
                : relatedClubs.Where(c => c.CatalogSetting.HasTitle1SchoolDiscounts && !c.IsDeleted);

            var model = clubWithDiscountTitle.OrderBy(c => c.Id)
                .Skip((paginationModel.Page - 1) * paginationModel.PageSize)
                .Take(paginationModel.PageSize).Select(c => new
                {
                    ClubId = c.Id,
                    c.Name,
                    describe = c.CatalogSetting.Title1SchoolDiscountType
                }).AsNoTracking();

            return JsonNet(new { DataSource = model, TotalCount = clubWithDiscountTitle.Count() });
        }

        public virtual ActionResult GetClubCounties(PaginationModel paginationModel, int clubId)
        {
            var clubCounties = _clubBusiness.Get(clubId).CatalogSetting.CatalogSettingCounties;

            var model = clubCounties.OrderBy(c => c.CountyId)
                .Skip((paginationModel.Page - 1) * paginationModel.PageSize)
                .Take(paginationModel.PageSize).Select(c => new
                {
                    County = c.County.Name,
                    Mabye = c.Maybe ? "Potential" : "Current",
                }).ToList();

            return JsonNet(new { DataSource = model, TotalCount = model.Count });
        }

        public virtual ActionResult GetCountryProvider(PaginationModel paginationModel, int countyId)
        {
            var county = Ioc.CountryBusiness.GetCounty(countyId);
            var countyName = string.Empty;
            if (county != null)
            {
                countyName = county.Name;
            }

            var countryProviders =
                _clubBusiness.GetRelatedClubs(ActiveClub.Id).Where(
                    c => !c.IsDeleted &&
                         c.CatalogSetting.CatalogSettingCounties.Any(
                             cc => cc.County.Name.ToLower() == countyName.ToLower()));

            var model = countryProviders.OrderBy(c => c.Id)
                .Skip((paginationModel.Page - 1) * paginationModel.PageSize)
                .Take(paginationModel.PageSize).Select(c => new
                {
                    ClubId = c.Id,
                    Name = c.Name
                }).AsNoTracking();

            return JsonNet(new { DataSource = model, TotalCount = countryProviders.Count() });
        }

        public virtual JsonNetResult GetCounties()
        {
            //var catalogSettings = Ioc.CatalogSettingBusiness.Get(ActiveClub.Id);

            //var model = new CatalogSettingsViewModel();

            var emCounties = new List<string>()
            {
                "Fairfax County", "Arlington County", "Loudoun County", "Prince William County", "Montgomery County", "District of Columbia County"
            };

            var emStates = new List<string>()
            {
                "Virginia", "Maryland", "District of Columbia"
            };

            var model = Ioc.CountryBusiness.GetCounties()
                  .Where(w => emCounties.Contains(w.Name) && emStates.Contains(w.State.Name))
                 .ToList().Select(x => new SelectKeyValue<int>() { Group = x.State.Name, Text = x.Name, Value = x.Id }).ToList();

            model.Remove(model.Single(s => s.Text.Equals("Montgomery County", StringComparison.OrdinalIgnoreCase) && s.Group.Equals("Virginia", StringComparison.OrdinalIgnoreCase)));

            return JsonNet(model);
        }

        [HttpGet]
        public virtual JsonNetResult GetCatalogOverviewActivityReportHeader()
        {
            var club = ActiveClub;
            var model = new
            {
                ClubName = ActiveClub.Name,
                Date = _clubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt")
            };

            return JsonNet(model);
        }

        [HttpGet]
        public virtual ActionResult GetCatalogOverviewActivityReportInfo()
        {
            var clubId = ActiveClub.Id;
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            var allProviders = _clubBusiness.GetRelatedClubs(ActiveClub.Id).Where(c => c.ClubType.ParentType.Name == "Provider").OrderBy(c => c.Name);
            var catalogBusiness = Ioc.CatalogBusiness;
            var data = new List<object>();

            foreach (var provider in allProviders)
            {
                var catalog = catalogBusiness.GetList().Where(c => provider.Id == c.ClubId);

                data.Add(new
                {
                    Name = provider.Name,
                    Status = !provider.IsInActive,
                    CatalogItemCount = catalog.Count(),
                    Domain = string.Format("{0}://{1}.{2}", Request.Url.Scheme, provider.Domain, Request.Url.Host.Substring(Request.Url.Host.IndexOf(".") + 1)),
                    LastUpdateCatalog = catalog.Any() && catalog != null ? catalog.OrderByDescending(c => c.MetaData.DateUpdated).First().MetaData.DateUpdated.ToString("MMM dd, yyyy") : string.Empty
                });

            }


            var dataSource = data.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            return JsonNet(new { DataSource = dataSource, TotalCount = data.Count });

        }

        public virtual JsonNetResult GetSeasons()
        {
            var relatedClubs =
                _clubBusiness.GetRelatedClubs(ActiveClub.Id)
                    .Where(c => !c.IsDeleted);

            var allSeasons =
                relatedClubs.SelectMany(c => c.Seasons)
                    .Where(s => s.Status != SeasonStatus.Deleted)
                    .Select(s => new SelectKeyValue<long>() { Text = s.Title, Value = s.Id });

            return JsonNet(allSeasons);

        }
        [JbAuthorize(JbAction.ProtalReport_Finance)]
        public virtual JsonNetResult GetProvider()
        {

            var model =

                _clubBusiness.GetRelatedClubs(ActiveClub.Id)
                   .ToList()
                   .Where(s => s.IsProvider == true)
                   .Select(s => new SelectKeyValue<string>() { Text = s.Name, Value = s.Id.ToString() });


            return JsonNet(model);


        }


        public virtual JsonNetResult GetClubSeasonsProvider(int clubId)
        {

            var allSeasons = Ioc.SeasonBusiness.GetList(clubId);
            var termValue = Request.Params["term"] ?? string.Empty;
            if (!string.IsNullOrEmpty(termValue))
            {
                allSeasons = allSeasons.Where(c => c.Title.StartsWith(termValue));
            }
            var model = new List<SelectKeyValue<string>>();

            model.AddRange(allSeasons.OrderByDescending(c => c.MetaData.DateCreated).ToList().Select(c => new SelectKeyValue<string>()
            {
                Text = c.Title,
                Value = c.Id.ToString()
            }).ToList());


            return JsonNet(model);
        }
        public virtual JsonNetResult GetAllSchoolsSeasons()
        {
            var club = ActiveClub;
            var relatedClubs =
                 _clubBusiness.GetRelatedClubs(club.Id)
                    .Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.School);

            var allSeasons =
                relatedClubs.SelectMany(c => c.Seasons)
                    .Where(s => s.Status != SeasonStatus.Deleted);

            var distincSeasons =
                allSeasons.Select(
                        s =>
                            new SelectKeyValue<string>()
                            {
                                Text = s.Title,
                                Value = s.Title.ToLower().Replace(" ", string.Empty)
                            }).DistinctBy(c => c.Value).ToList();

            return JsonNet(distincSeasons);
        }

        public virtual JsonNetResult GetAllSchool()
        {
            var club = ActiveClub;

            var schools =
                _clubBusiness.GetRelatedClubs(club.Id)
                    .Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.School)
                    .Select(s => new SelectKeyValue<long>() { Text = s.Name, Value = s.Id })
                    .OrderBy(s => s.Text).ToList();

            return JsonNet(schools);
        }

        public virtual JsonNetResult GetAllTeacher(long seasonId)
        {

            var orderItems = Ioc.OrderItemBusiness.GetList().Where(c => c.SeasonId == seasonId && c.ProgramScheduleId.HasValue && c.ItemStatus == OrderItemStatusCategories.completed && c.Order.IsLive);
            var fromBusiness = Ioc.FormBusiness;
            var teachers = new List<string>();
            var season = Ioc.SeasonBusiness.Get(seasonId);

            var seasonForm = Ioc.SeasonBusiness.GetSeasonRegistrationForm(season);
            var teacherItems = new List<JbElementItem>();
            teacherItems = Ioc.FormBusiness.GetTeacherItems(seasonForm.JbForm);
            if (teacherItems != null)
            {
                foreach (var item in teacherItems)
                {
                    if (item.Value != "Teacher not listed" && item.Value != "")
                    {
                        teachers.Add(item.Value);
                    }

                }
            }



            foreach (var item in orderItems)
            {
                var teacher = fromBusiness.GetTeacher(item.JbForm);
                if (!string.IsNullOrEmpty(teacher))
                {
                    teachers.Add(teacher);
                }

            }
            var Allteachers = new List<SelectKeyValue<string>>();

            Allteachers.Add(new SelectKeyValue<string> { Text = "All teacher", Value = "" });

            Allteachers.AddRange(
                teachers.Distinct(StringComparer.CurrentCultureIgnoreCase).Select(t => new SelectKeyValue<string>
                {
                    Text = t,
                    Value = t
                }).OrderBy(s => s.Text));

            Allteachers.OrderBy(s => s.Text);
            return JsonNet(Allteachers);
        }

        public virtual JsonNetResult GetAllPrograms(long seasonId)
        {
            var programs = Ioc.ProgramBusiness.GetListBySeasonId(seasonId).Where(p => p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4);

            var Allprograms = programs.Select(p => new SelectKeyValue<long>() { Text = p.Name, Value = p.Id })
                    .OrderBy(s => s.Text).ToList();
            return JsonNet(Allprograms);
        }

        public virtual JsonNetResult GetDate()
        {
            var model = new schoolProgramsCheckInOutReportViewModel();
            model.DateProgram = _clubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow);
            return JsonNet(model.DateProgram);
        }

        public virtual JsonNetResult GetRangeInstallmentDate()
        {
            var endDate = _clubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow);
            var startDate = endDate.AddDays(-7);

            var model = new { StartDate = startDate, EndDate = endDate };
            return JsonNet(model);
        }

        public virtual JsonNetResult GetAllProvider()
        {
            var club = ActiveClub;

            var schools =
                _clubBusiness.GetRelatedClubs(club.Id)
                    .Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.Provider)
                    .Select(s => new SelectKeyValue<long>() { Text = s.Name, Value = s.Id })
                    .OrderBy(s => s.Text).ToList();

            return JsonNet(schools);
        }

        public virtual JsonNetResult GetClubSeasons(int clubId, bool includeAllOption = false)
        {
            var result = _seasonBusiness.GetKeyValueClubSeasons(clubId, includeAllOption);
            return JsonNet(result);
        }

        public virtual ActionResult GetAllClubWithCatalogs()
        {
            var relatedClub = _clubBusiness.GetRelatedClubs(ActiveClub.Id).Where(c => !c.IsDeleted && c.CatalogItems.Any()).AsNoTracking();
            var model = new List<object>();
            foreach (var club in relatedClub)
            {
                var clubcatalogs = _clubBusiness.Get(club.Id).CatalogItems.Where(ca => ca.Status != CatalogStatus.Deleted).ToList();
                foreach (var item in clubcatalogs)
                {
                    model.Add(new
                    {
                        ClubId = club.Id,
                        ClubName = club.Name,
                        ClubUpdateDate =
                            (club.CatalogItems.Any())
                                ? club.CatalogItems.Where(ca => ca.Status != CatalogStatus.Deleted)
                                    .OrderByDescending(c => c.MetaData.DateUpdated)
                                    .FirstOrDefault()
                                    .MetaData.DateUpdated.ToString("MM/dd/yyyy H:mm")
                                : "",
                        CatalogItem = item.Name,
                        UpdateDate = item.MetaData.DateUpdated.ToString("MM/dd/yyyy H:mm")
                    });
                }

            }
            return JsonNet(new { DataSource = model, TotalCount = model.Count() });
        }

        public virtual ActionResult GetAllTitle1Discounts()
        {
            var relatedClubs = _clubBusiness.GetRelatedClubs(ActiveClub.Id).AsNoTracking();

            var clubWithDiscountTitle = relatedClubs.Where(c => c.CatalogSetting.HasTitle1SchoolDiscounts && !c.IsDeleted);
            var model = new List<object>();

            foreach (var club in clubWithDiscountTitle)
            {
                var clubCounties = _clubBusiness.Get(club.Id).CatalogSetting.CatalogSettingCounties;
                foreach (var county in clubCounties)
                {
                    model.Add(new
                    {
                        ClubId = club.Id,
                        club.Name,
                        describe = club.CatalogSetting.Title1SchoolDiscountType,
                        County = county.County.Name,
                        Mabye = county.Maybe ? "Potential" : "Current",
                    });
                }
            }
            return JsonNet(new { DataSource = model, TotalCount = clubWithDiscountTitle.Count() });
        }

        public virtual ActionResult GetAllCountryProviders(int countyId)
        {
            var county = Ioc.CountryBusiness.GetCounty(countyId);
            var countyName = string.Empty;
            if (county != null)
            {
                countyName = county.Name;
            }

            var model = new List<object>();
            var countryProviders =
                _clubBusiness.GetRelatedClubs(ActiveClub.Id).Where(
                    c => !c.IsDeleted &&
                         c.CatalogSetting.CatalogSettingCounties.Any(
                             cc => cc.County.Name.ToLower() == countyName.ToLower())).AsNoTracking();

            foreach (var club in countryProviders)
            {
                var clubCounties = _clubBusiness.Get(club.Id).CatalogSetting.CatalogSettingCounties;
                foreach (var clubCounty in clubCounties)
                {
                    model.Add(new
                    {
                        ClubId = club.Id,
                        Name = club.Name,
                        County = clubCounty.County.Name,
                        Mabye = clubCounty.Maybe ? "Potential" : "Current",
                    });
                }

            }
            return JsonNet(new { DataSource = model, TotalCount = countryProviders.Count() });
        }

        [JbAuthorize(JbAction.PortalReport_View)]
        public virtual ActionResult GetAllSchoolMemberList(PaginationModel paginationModel)
        {
            var relatedClubs = _clubBusiness.GetRelatedClubs(ActiveClub.Id);

            var inttypeId = (int)ClubTypesEnum.School;
            var allSchoolMembers = relatedClubs.Where(c => c.ClubType.ParentId.Value == inttypeId);

            var model = new List<object>();

            foreach (var club in allSchoolMembers.OrderBy(c => c.Id)
                .Skip((paginationModel.Page - 1) * paginationModel.PageSize)
                .Take(paginationModel.PageSize))
            {

                var contact = club.ContactPersons != null && club.ContactPersons.Any() ? club.ContactPersons.FirstOrDefault() : new ContactPerson();
                model.Add(new
                {
                    ClubId = club.Id,
                    SchoolName = club.Name,
                    ContactEmail = contact.Email,
                    ContactPerson = contact.FullName
                });

            }

            return JsonNet(new { DataSource = model, TotalCount = allSchoolMembers.Count() });
        }

        public virtual ActionResult GetAllDiscountInDate(DateTime? start, DateTime? end, PaginationModel paginationModel, bool calucalteTotal = false)
        {
            var clubOrderItems =
                GetClubOrderItems(start, end).Where(
                        oi =>
                        oi.OrderChargeDiscounts.Any(d => d.Category < 0) ||
                        oi.TransactionActivities.Any(t => t.PaymentDetail.PaymentMethod == PaymentMethod.Scholarship));

            if (!calucalteTotal)
            {
                var itemList = new List<object>();
                foreach (
                    var item in
                        clubOrderItems.OrderBy(c => c.Id)
                            .Skip((paginationModel.Page - 1) * paginationModel.PageSize)
                            .Take(paginationModel.PageSize))
                {
                    itemList.Add(new
                    {
                        Id = item.Id,
                        ItemName = item.Name,
                        User = item.FullName,
                        UserId = item.Order.UserId,
                        Date = item.Order.CompleteDate.ToString("MMM dd, yyyy"),
                        Confirmation = item.Order.ConfirmationId
                    });
                }

                return JsonNet(new { DataSource = itemList, TotalCount = clubOrderItems.Count() });
            }
            else
            {
                decimal totalDiscounts = 0;
                decimal totalScholarships = 0;

                if (clubOrderItems.Any(o => o.OrderChargeDiscounts.Any(d => !d.IsDeleted && d.Category < 0)))
                {
                    totalDiscounts +=
                        Enumerable.Sum(
                            clubOrderItems.SelectMany(
                                clubOrderItem => clubOrderItem.GetOrderChargeDiscounts().Where(d => d.Category < 0)),
                            item => Math.Abs(item.Amount));
                }

                if (clubOrderItems.Any(c => c.TransactionActivities.Any()))
                {
                    totalScholarships =
                        Enumerable.Sum(
                            clubOrderItems.SelectMany(
                                clubOrderItem =>
                                    clubOrderItem.TransactionActivities.Where(
                                        t => t.PaymentDetail.PaymentMethod == PaymentMethod.Scholarship)),
                            item => Math.Abs(item.Amount));
                }

                var total = Math.Abs(totalDiscounts) + Math.Abs(totalScholarships);
                var clubName = ActiveClub.Name;
                var date = _clubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:MM tt");

                var clubCurrency = _clubBusiness.GetClubCurrency(this.GetCurrentClubDomain());

                return JsonNet(new
                {
                    Total = CurrencyHelper.FormatCurrencyWithPenny(total, clubCurrency),
                    TotalDiscounts = CurrencyHelper.FormatCurrencyWithPenny(Math.Abs(totalDiscounts), clubCurrency),
                    TotalScholar = CurrencyHelper.FormatCurrencyWithPenny(Math.Abs(totalScholarships), clubCurrency),
                    Date = date,
                    ClubName = clubName
                });
            }
        }

        public virtual ActionResult GetDiscountsDetail(long itemId, PaginationModel paginationModel)
        {
            var item = Ioc.OrderItemBusiness.GetItem(itemId);

            var discounts =
                item.GetOrderChargeDiscounts().Where(o => o.Category < 0);
            var discountList = GetDiscountModel(discounts);


            var schoolarshipPayment =
                Ioc.TransactionActivityBusiness.GetAllTransactionByOrderItemId(itemId)
                    .Where(t => t.PaymentDetail.PaymentMethod == PaymentMethod.Scholarship);


            discountList.AddRange(GetScholarshipModel(schoolarshipPayment));

            var totalCount = schoolarshipPayment.Count() + discounts.Count();

            return JsonNet(new { DataSource = discountList, TotalCount = totalCount });
        }

        [HttpPost]
        public virtual JsonNetResult GetDonations(DateTime? start, DateTime? end, int? clubId)
        {
            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);
            var club = clubId.HasValue ? _clubBusiness.Get(clubId.Value) : _clubBusiness.Get(ActiveClub.Id);

            var model = new List<DonationsReportViewModel>();

            if (!start.HasValue && !end.HasValue)
            {
                return JsonNet(new { DataSource = model, TotalCount = 0 });
            }

            var query = GetClubOrderItems(start, end, club.Id).Where(item => item.ItemStatus == OrderItemStatusCategories.completed && item.Order.ClubId == club.Id && !item.ProgramScheduleId.HasValue);

            var playerProfileBussiness = Ioc.PlayerProfileBusiness;

            if (query.Any())
            {

                foreach (var item in query.OrderByDescending(r => r.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList())
                {
                    var participantList = playerProfileBussiness.GetList(item.Order.UserId);

                    model.Add(new DonationsReportViewModel()
                    {
                        ConfirmationId = item.Order.ConfirmationId,
                        ParentFirstName = participantList.Any(c => c.Relationship == RelationshipType.Parent) ? participantList.First(c => c.Relationship == RelationshipType.Parent).Contact.FirstName : "",
                        ParentLastName = participantList.Any(c => c.Relationship == RelationshipType.Parent) ? participantList.First(c => c.Relationship == RelationshipType.Parent).Contact.LastName : "",
                        ParentEmail = participantList.Any(c => c.Relationship == RelationshipType.Parent) ? participantList.First(c => c.Relationship == RelationshipType.Parent).Contact.Email : "",
                        ParticipantFirstName = participantList.Any(c => c.Relationship == RelationshipType.Registrant) ? participantList.First(c => c.Relationship == RelationshipType.Registrant).Contact.FirstName : "",
                        ParticipantLastName = participantList.Any(c => c.Relationship == RelationshipType.Registrant) ? participantList.First(c => c.Relationship == RelationshipType.Registrant).Contact.LastName : "",
                        DonationAmount = CurrencyHelper.FormatCurrencyWithPenny(item.TotalAmount, ActiveClub.Currency),
                        UserEmail = item.Order.User.UserName,
                        Date = item.Order.CompleteDate.ToString("MMM dd, yyyy"),
                        Id = item.Id,
                        HasDetail = item.JbFormId.HasValue
                    });
                }

            }

            return JsonNet(new { DataSource = model, TotalCount = query.Count() });
        }

        public virtual string GetTotalDonations(DateTime? start, DateTime? end, int? clubId)
        {
            decimal totalAmount = 0;

            var club = clubId.HasValue ? _clubBusiness.Get(clubId.Value) : _clubBusiness.Get(ActiveClub.Id);

            var query = GetClubOrderItems(start, end, club.Id).Where(item => item.ItemStatus == OrderItemStatusCategories.completed && item.Order.ClubId == club.Id && !item.ProgramScheduleId.HasValue);
            if (query.Any())
            {
                foreach (var item in query.ToList())
                {
                    totalAmount += item.TotalAmount;
                }

            }
            return CurrencyHelper.FormatCurrencyWithPenny(totalAmount, ActiveClub.Currency);
        }

        [HttpPost]
        [JbAuthorize(JbAction.PortalReport_ParentMailingList)]
        public virtual JsonNetResult GetParentMailingList()
        {
            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);

            var club = ActiveClub;

            var registrants = _clubBusiness.Get(club.Id).Users.AsQueryable();

            var families = Ioc.UserProfileBusiness.GetPlayerProfiles(registrants.Select(r => r.UserId).ToList()).GroupBy(u => u.UserId).OrderBy(o => o.Key).Skip((page - 1) * pageSize).Take(pageSize).ToList();

            var playerProfileBussiness = Ioc.PlayerProfileBusiness;
            var model = new List<ParentMailingListViewModel>();

            if (families.Any())
            {
                foreach (var family in families)
                {
                    var userName = family.First().User.UserName;
                    JbAddress parentAddress = new JbAddress();

                    var parent = family.FirstOrDefault(f => f.Relationship == RelationshipType.Parent);


                    var lastRegistrant = family.LastOrDefault(f => f.Relationship == RelationshipType.Registrant && f.OrderItems.Any(o => o.ItemStatus == OrderItemStatusCategories.completed && o.JbFormId.HasValue));

                    if (lastRegistrant != null)
                    {
                        parentAddress = Ioc.FormBusiness.GetParentAddress(lastRegistrant.OrderItems.Last(o => o.JbFormId.HasValue).JbForm);
                    }

                    if (parentAddress == null)
                    {
                        parentAddress = new JbAddress();
                    }

                    model.Add(new ParentMailingListViewModel
                    {
                        UserName = userName,
                        ParentFirstName = parent != null ? parent.Contact.FirstName : Constants.S_Dash,
                        ParentLastName = parent != null ? parent.Contact.LastName : Constants.S_Dash,
                        AddressLine1 = !string.IsNullOrEmpty(parentAddress.AddressLine1) ? parentAddress.AddressLine1 : Constants.S_Dash,
                        AddressLine2 = !string.IsNullOrEmpty(parentAddress.AddressLine2) ? parentAddress.AddressLine2 : Constants.S_Dash,
                        City = !string.IsNullOrEmpty(parentAddress.City) ? parentAddress.City : Constants.S_Dash,
                        State = !string.IsNullOrEmpty(parentAddress.State) ? parentAddress.State : Constants.S_Dash,
                        Zip = !string.IsNullOrEmpty(parentAddress.Zip) ? parentAddress.Zip : Constants.S_Dash,
                    });
                }
            }

            return JsonNet(new { DataSource = model, TotalCount = Ioc.UserProfileBusiness.GetPlayerProfiles(registrants.Select(r => r.UserId).ToList()).GroupBy(u => u.UserId).Count() });
        }

        [HttpPost]
        public virtual JsonNetResult GetFamilyCreditCardList(string term)
        {
            var pageSize = 10;
            int.TryParse(Request.Params["PageSize"], out pageSize);
            var page = 0;
            int.TryParse(Request.Params["Page"], out page);
            var creditCards = _userCreditCardBusiness;
            var creditCardsByCustomer = creditCards.GetListwithCustomerId();

            IQueryable<ClubUser> query = null;

            var clubUser = _clubBusiness.Get(ActiveClub.Id).Users.AsQueryable();

            if (clubUser.Any())
            {
                var total = clubUser.Count();

                query = clubUser.AsQueryable().OrderBy(c => c.UserId).Skip((page - 1) * pageSize).Take(pageSize > 0 ? pageSize : 10);
                var model = new List<FamilyCreditCardsListViewModel>();

                foreach (var item in query.ToList())
                {
                    Club club = _clubBusiness.Get(ActiveClub.Domain);
                    var paymentMethod = Ioc.ClientPaymentMethodBusiness.Get(club.ClientId.Value);
                    var clubId = _clubBusiness.GetClubByClientId(paymentMethod.Id).Id;

                    var _paymentGateway = Ioc.ClientBusiness.GetPaymentGatewayClub(clubId);

                    var userCreditCard = creditCards.GetActiveUserCreditCards(item.UserId, _paymentGateway);
                    if (userCreditCard != null)
                    {
                        model.Add(new FamilyCreditCardsListViewModel()
                        {
                            Id = item.UserId,
                            Players = Ioc.PlayerProfileBusiness.GetList(item.UserId),
                            UserName = item.User.UserName,
                            Name = userCreditCard.Name,
                            LastDigits = userCreditCard.Brand + " ***" + userCreditCard.LastDigits,
                            Month = userCreditCard.ExpiryMonth,
                            Year = userCreditCard.ExpiryYear,
                            Brand = userCreditCard.Brand,
                            Isdefault = userCreditCard.IsDefault,
                        });
                    }
                    else
                    {
                        model.Add(new FamilyCreditCardsListViewModel()
                        {
                            Id = item.UserId,
                            Players = Ioc.PlayerProfileBusiness.GetList(item.UserId),
                            UserName = item.User.UserName,
                            Name = "-",
                            LastDigits = "-",
                            Month = null,
                            Year = null,
                            Brand = "-",
                            Isdefault = false,
                        });
                    }


                }

                return JsonNet(new { DataSource = model, TotalCount = total });

            }

            return JsonNet(new { Data = new List<UserViewModel>(), Total = 0 });
        }

        [HttpPost]
        public virtual ActionResult GetBulkDependentCareReceiptReport(string filterType, int? year)
        {
            IQueryable<ClubUser> users = _clubBusiness.Get(ActiveClub.Id).Users.AsQueryable();
            DateTime? startDate = null;
            DateTime? endDate = null;

            SportsClub.Areas.Dashboard.Models.DependentCareReceiptReportFilterType Type = (SportsClub.Areas.Dashboard.Models.DependentCareReceiptReportFilterType)Enum.Parse(typeof(SportsClub.Areas.Dashboard.Models.DependentCareReceiptReportFilterType), filterType);

            var orderItemsResult = new List<List<BulkDependentCareReceiptReportViewModel>>();
            var orderItemBusiness = Ioc.OrderItemBusiness;

            var programBusiness = Ioc.ProgramBusiness;
            var transactionBusiness = Ioc.TransactionActivityBusiness;

            if (Type == SportsClub.Areas.Dashboard.Models.DependentCareReceiptReportFilterType.Year)
            {
                startDate = new DateTime(year.Value, 1, 1);
                endDate = new DateTime(year.Value + 1, 1, 1).AddDays(-1);
            }

            foreach (var user in users)
            {
                var orderItems = orderItemBusiness.GetUserOrderItems(user.UserId)
                   .Where(i => i.Order.ClubId == ActiveClub.Id);


                orderItems = orderItems.Where(i => i.TransactionActivities.Any(t => t.TransactionStatus == TransactionStatus.Success &&
                             t.TransactionDate >= startDate && t.TransactionDate <= endDate));

                if (orderItems.Count() != 0)
                {

                    var result = new List<BulkDependentCareReceiptReportViewModel>();

                    foreach (var item in orderItems)
                    {

                        var transactions = Type == SportsClub.Areas.Dashboard.Models.DependentCareReceiptReportFilterType.Year ? transactionBusiness.GetAllsucceedTransactionByOrderItemId(item.Id).Where(t => (t.TransactionType == TransactionType.Payment || t.TransactionType == TransactionType.Refund) && t.TransactionDate.Year == year).Any() ?
                                                                              transactionBusiness.GetAllsucceedTransactionByOrderItemId(item.Id).Where(t => (t.TransactionType == TransactionType.Payment || t.TransactionType == TransactionType.Refund) && t.TransactionDate.Year == year) : null :
                                           Type == SportsClub.Areas.Dashboard.Models.DependentCareReceiptReportFilterType.DateRange ? transactionBusiness.GetAllsucceedTransactionByOrderItemId(item.Id).Where(t => (t.TransactionType == TransactionType.Payment || t.TransactionType == TransactionType.Refund) && t.TransactionDate >= startDate && t.TransactionDate <= endDate).Any() ?
                                                                                   transactionBusiness.GetAllsucceedTransactionByOrderItemId(item.Id).Where(t => (t.TransactionType == TransactionType.Payment || t.TransactionType == TransactionType.Refund) && t.TransactionDate >= startDate && t.TransactionDate <= endDate) : null : null;

                        if (transactions != null)
                        {
                            var programScheduleMode = _programBusiness.GetClassScheduleTime(item.ProgramSchedule.Program);

                            foreach (var transaction in transactions)
                            {
                                if (transaction.Amount != 0)
                                {
                                    result.Add(new BulkDependentCareReceiptReportViewModel()
                                    {
                                        StudentName = string.Format("{0}, {1}", item.LastName, item.FirstName),
                                        ClassName = transaction.TransactionType == TransactionType.Payment ? item.ProgramSchedule.Program.Name : $"Refund - {item.ProgramSchedule.Program.Name}",
                                        AmountPaid = transaction.TransactionType == TransactionType.Payment ? (decimal?)transaction.Amount ?? 0 : (decimal?)-transaction.Amount ?? 0,
                                        ClassMeetingDates = programBusiness.GetClassDates(item.ProgramSchedule, programScheduleMode) != null ? DateTimeHelper.GetDaysSeperatedByComma(programBusiness.GetClassDates(item.ProgramSchedule, programScheduleMode).Select(c => c.SessionDate).ToList()) : "-",
                                        MethodOfPayment = !string.IsNullOrEmpty(transaction.PaymentDetail.PaymentMethod.ToDescription()) ? transaction.PaymentDetail.PaymentMethod.ToDescription() : "-",
                                        ProviderName = item.ProgramSchedule.Program.OutSourceSeasonId.HasValue ? item.ProgramSchedule.Program.OutSourceSeason.Club.Name : item.ProgramSchedule.Program.Club.Name,
                                        ProviderTaxId = item.ProgramSchedule.Program.OutSourceSeasonId.HasValue ?
                                                                               (item.ProgramSchedule.Program.OutSourceSeason.Club.Setting.TaxIDFEIN) :
                                                                               (item.ProgramSchedule.Program.Club.Setting.TaxIDFEIN),
                                        Date = transaction.TransactionDate.ToString(Constants.DefaultDateFormat)
                                    });
                                }
                            }
                        }

                    }

                    orderItemsResult.Add(result);
                }

            }


            //HeaderInfo
            var club = _clubBusiness.Get(ActiveClub.Id);
            var info = club.PartnerId.HasValue ? ((PartnerSetting)club.PartnerClub.Setting)?.PortalParameters != null && ((PartnerSetting)club.PartnerClub.Setting).PortalParameters.ReadDependentCareReportInfoFromMember ? club : club.PartnerClub : club;

            var date = "";

            if (filterType == "Year")
            {
                date = year.ToString();
            }

            if (orderItemsResult.Count == 0)
            {
                orderItemsResult.Add(new List<BulkDependentCareReceiptReportViewModel>());
            }

            var models = new BulkDependentCareReceiptReportPdfExportViewModel();
            models.Data = orderItemsResult;
            models.Date = _clubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
            models.HeaderInfoPartnerName = !string.IsNullOrEmpty(info.Name) ? info.Name : "";
            models.HeaderInfoPartnerAddress = info.Address != null && !string.IsNullOrEmpty(info.Address.Address) ? info.Address.Address : "";
            models.HeaderInfoPartnerPhone = info.ContactPersons != null && !string.IsNullOrEmpty(info.ContactPersons.FirstOrDefault().Phone) ? PhoneNumberHelper.FormatPhoneNumber(info.ContactPersons.FirstOrDefault().Phone) : "";
            models.HeaderInfoPartnerSite = !string.IsNullOrEmpty(info.Site) ? info.Site.EndsWith("/") ? info.Site.Remove(info.Site.Count() - 1, 1) : info.Site : "";
            models.HeaderInfoPartnerEmail = info.ContactPersons != null && !string.IsNullOrEmpty(info.ContactPersons.FirstOrDefault().Email) ? info.ContactPersons.FirstOrDefault().Email : "";
            models.HeaderInfoDate = date;
            models.Description = info.Setting?.Notifications.Report.DependentCareDescription;
            models.TotalAmount = new List<string>();
            foreach (var item in orderItemsResult)
            {
                models.TotalAmount.Add(string.Format("{0:c2}", item.Sum(s => (decimal?)s.AmountPaid) ?? 0));
            }


            // Convert to pdf
            var viewRendered = this.RenderViewToString("_BulkDependentCareReceiptReportPdfExportView", models);

            using (var pdfStream = new MemoryStream())
            {
                var pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 20, 20, 20, 20);
                var pdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, pdfStream);

                pdfDoc.Open();

                using (var strHtml = new StringReader(viewRendered))
                {
                    iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(pdfWriter, pdfDoc, strHtml);
                }

                pdfDoc.Close();

                using (var stream = new MemoryStream(pdfStream.GetBuffer()))
                {
                    byte[] buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, buffer.Length);
                    pdfStream.Close();
                    stream.Close();
                    return File(buffer, "application/pdf", "BulkDependentCareReceipt.pdf");
                }
            }

        }


        [HttpGet]
        public virtual JsonNetResult GetDependentCareReceiptReportDetail(string filterType, int? year)
        {
            var clubDomain = ActiveClub.Domain;
            var clubBusiness = Ioc.ClubBusiness;
            var club = clubBusiness.Get(ActiveClub.Id);

            var info = club.PartnerId.HasValue ? ((PartnerSetting)club.PartnerClub.Setting)?.PortalParameters != null && ((PartnerSetting)club.PartnerClub.Setting).PortalParameters.ReadDependentCareReportInfoFromMember ? club : Ioc.ClubBusiness.GetPartner(clubDomain) : club;

            var date = "";

            if (filterType == "Year")
            {
                date = year.ToString();
            }

            var model = new DependentCareReceiptReportItemDetailViewModel()
            {
                PartnerName = !string.IsNullOrEmpty(info.Name) ? info.Name : "",
                PartnerAddress = info.Address != null && !string.IsNullOrEmpty(info.Address.Address) ? info.Address.Address : "",
                PartnerPhone = info.ContactPersons != null && !string.IsNullOrEmpty(info.ContactPersons.FirstOrDefault().Phone) ? PhoneNumberHelper.FormatPhoneNumber(info.ContactPersons.FirstOrDefault().Phone) : "",
                PartnerSite = !string.IsNullOrEmpty(info.Site) ? info.Site.EndsWith("/") ? info.Site.Remove(info.Site.Count() - 1, 1) : info.Site : "",
                PartnerEmail = info.ContactPersons != null && !string.IsNullOrEmpty(info.ContactPersons.FirstOrDefault().Email) ? info.ContactPersons.FirstOrDefault().Email : "",
                Date = date
            };

            return JsonNet(new { Data = model });
        }


        public virtual ActionResult GetAllScholarshipDiscounts(DateTime? start, DateTime? end)
        {
            //var club = ActiveClub;
            var clubOrderItems =
                GetClubOrderItems(start, end).Where(oi => oi.OrderChargeDiscounts.Any(d => !d.IsDeleted && d.Category < 0) ||
                                                          oi.TransactionActivities.Any(
                                                              t =>
                                                                  t.PaymentDetail.PaymentMethod ==
                                                                  PaymentMethod.Scholarship && t.TransactionStatus == TransactionStatus.Success) &&
                                                                  !oi.Order.IsDraft && oi.ProgramScheduleId.HasValue &&
                                                                   (oi.ItemStatusReason != OrderItemStatusReasons.transferOut &&
                                                                    oi.ItemStatusReason != OrderItemStatusReasons.canceled)).AsNoTracking();

            var itemList = new List<ExpandoObject>();
            foreach (var orderItem in clubOrderItems)
            {
                var item = Ioc.OrderItemBusiness.GetItem(orderItem.Id);

                var discounts =
                    item.GetOrderChargeDiscounts().Where(o => o.Category < 0);
                var discountList = GetDiscountModel(discounts);

                var schoolarshipPayment =
                    Ioc.TransactionActivityBusiness.GetAllTransactionByOrderItemId(orderItem.Id)
                        .Where(t => t.PaymentDetail.PaymentMethod == PaymentMethod.Scholarship && t.TransactionStatus == TransactionStatus.Success).ToList();
                var scholarshipsPaymentList = GetScholarshipModel(schoolarshipPayment).ToList();

                dynamic items = new ExpandoObject();
                items.Id = item.Id;
                items.ItemName = item.Name;
                items.User = item.FullName;
                items.UserId = item.Order.UserId;
                items.SeasonName = item.Season.Title;
                items.Date = item.Order.CompleteDate.ToString("MMM dd, yyyy");
                items.Confirmation = item.Order.ConfirmationId;
                items.TotalDiscount = Math.Abs(discounts.Sum(d => d.Amount)) + Math.Abs(schoolarshipPayment.Sum(s => s.Amount));

                items.DiscountName1 = "";
                items.DiscountAmount1 = "";
                items.DiscountName2 = "";
                items.DiscountAmount2 = "";
                items.DiscountName3 = "";
                items.DiscountAmount3 = "";
                items.DiscountName4 = "";
                items.DiscountAmount4 = "";
                items.DiscountName5 = "";
                items.DiscountAmount5 = "";

                items.ScholarshipAmount1 = "";
                items.ScholarshipAmount2 = "";
                items.ScholarshipAmount3 = "";

                for (int i = 0; i < discountList.Count; i++)
                {
                    if (i == 0)
                    {
                        items.DiscountName1 = discountList[0].Name;
                        items.DiscountAmount1 = discountList[0].Fee;
                    }
                    else if (i == 1)
                    {
                        items.DiscountName2 = discountList[1].Name;
                        items.DiscountAmount2 = discountList[1].Fee;
                    }
                    else if (i == 2)
                    {
                        items.DiscountName3 = discountList[2].Name;
                        items.DiscountAmount3 = discountList[2].Fee;
                    }
                    else if (i == 3)
                    {
                        items.DiscountName4 = discountList[3].Name;
                        items.DiscountAmount4 = discountList[3].Fee;
                    }
                    else if (i == 4)
                    {
                        items.DiscountName5 = discountList[4].Name;
                        items.DiscountAmount5 = discountList[4].Fee;

                    }
                }

                for (int i = 0; i < scholarshipsPaymentList.Count; i++)
                {
                    if (i == 0)
                    {
                        items.ScholarshipAmount1 = scholarshipsPaymentList[0].Fee;
                    }
                    else if (i == 1)
                    {
                        items.ScholarshipAmount2 = scholarshipsPaymentList[1].Fee;
                    }
                    else if (i == 2)
                    {
                        items.ScholarshipAmount3 = scholarshipsPaymentList[2].Fee;
                    }
                }


                itemList.Add(items);
            }

            return JsonNet(new { DataSource = itemList, TotalCount = itemList.Count });
        }

        public virtual ActionResult JustGetDiscounts(DateTime? start, DateTime? end, PaginationModel paginationModel, bool calucalteTotal = false)
        {
            var clubOrderItems = GetClubOrderItems(start, end).Where(oi => oi.OrderChargeDiscounts.Any(d => !d.IsDeleted && d.Category < 0));

            if (!calucalteTotal)
            {
                var itemList = new List<object>();
                foreach (var item in clubOrderItems.OrderBy(c => c.Id).Skip((paginationModel.Page - 1) * paginationModel.PageSize).Take(paginationModel.PageSize))
                {
                    itemList.Add(new
                    {
                        Id = item.Id,
                        ItemName = item.Name,
                        UserId = item.Order.UserId,
                        User = item.FullName,
                        Date = item.Order.CompleteDate.ToString("MMM dd, yyyy"),
                        Confirmation = item.Order.ConfirmationId,
                    });
                }

                return JsonNet(new { DataSource = itemList, TotalCount = clubOrderItems.Count() });
            }
            else
            {
                decimal totalDiscounts = 0;
                if (clubOrderItems.Any(o => o.OrderChargeDiscounts.Any(d => !d.IsDeleted && d.Category < 0)))
                {
                    totalDiscounts +=
                        Enumerable.Sum(
                            clubOrderItems.SelectMany(
                                clubOrderItem => clubOrderItem.OrderChargeDiscounts.Where(d => !d.IsDeleted && d.Category < 0)),
                            item => Math.Abs(item.Amount));
                }
                var clubName = ActiveClub.Name;
                var date = _clubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:MM tt");
                var clubCurrency = _clubBusiness.GetClubCurrency(this.GetCurrentClubDomain());

                return JsonNet(new { Total = CurrencyHelper.FormatCurrencyWithPenny(Math.Abs(totalDiscounts), clubCurrency), ClubName = clubName, Date = date });
            }
        }

        public virtual ActionResult GetJustDiscountDetails(long itemId, PaginationModel paginationModel)
        {
            var item = Ioc.OrderItemBusiness.GetItem(itemId);
            var discounts = item.GetOrderChargeDiscounts().Where(o => o.Category < 0);

            var model = GetDiscountModel(discounts);

            return JsonNet(new { DataSource = model, TotalCount = discounts.Count() });
        }

        public virtual ActionResult JustGetAllDiscounts(DateTime? start, DateTime? end)
        {
            var clubOrderItems =
                GetClubOrderItems(start, end).Where(oi => oi.OrderChargeDiscounts.Any(d => !d.IsDeleted && d.Category < 0)).AsNoTracking();

            var itemList = new List<ExpandoObject>();
            foreach (var orderItem in clubOrderItems)
            {
                var item = Ioc.OrderItemBusiness.GetItem(orderItem.Id);

                var discounts =
                    item.GetOrderChargeDiscounts().Where(o => o.Category < 0);
                var discountList = GetDiscountModel(discounts);

                dynamic items = new ExpandoObject();

                items.Id = item.Id;
                items.ItemName = item.Name;
                items.User = item.FullName;
                items.UserId = item.Order.UserId;
                items.SeasonName = item.Season.Title;
                items.Date = item.Order.CompleteDate.ToString("MMM dd, yyyy");
                items.Confirmation = item.Order.ConfirmationId;
                items.TotalDiscount = Math.Abs(discounts.Sum(d => d.Amount));

                items.DiscountName1 = "";
                items.DiscountAmount1 = "";
                items.DiscountName2 = "";
                items.DiscountAmount2 = "";
                items.DiscountName3 = "";
                items.DiscountAmount3 = "";
                items.DiscountName4 = "";
                items.DiscountAmount4 = "";
                items.DiscountName5 = "";
                items.DiscountAmount5 = "";

                for (int i = 0; i < discountList.Count; i++)
                {
                    if (i == 0)
                    {
                        items.DiscountName1 = discountList[0].Name;
                        items.DiscountAmount1 = discountList[0].Fee;
                    }
                    else if (i == 1)
                    {
                        items.DiscountName2 = discountList[1].Name;
                        items.DiscountAmount2 = discountList[1].Fee;
                    }
                    else if (i == 2)
                    {
                        items.DiscountName3 = discountList[2].Name;
                        items.DiscountAmount3 = discountList[2].Fee;
                    }
                    else if (i == 3)
                    {
                        items.DiscountName4 = discountList[3].Name;
                        items.DiscountAmount4 = discountList[3].Fee;
                    }
                    else if (i == 4)
                    {
                        items.DiscountName5 = discountList[4].Name;
                        items.DiscountAmount5 = discountList[4].Fee;

                    }
                }

                itemList.Add(items);
            }

            return JsonNet(new { DataSource = itemList, TotalCount = itemList.Count });
        }

        public virtual ActionResult JustGetAllScholarships(DateTime? start, DateTime? end)
        {
            var clubOrderItems =
                GetClubOrderItems(start, end)
                    .Where(
                        oi =>
                            oi.TransactionActivities.Any(t => t.PaymentDetail.PaymentMethod == PaymentMethod.Scholarship && t.TransactionStatus == TransactionStatus.Success) &&
                             !oi.Order.IsDraft && oi.ProgramScheduleId.HasValue &&
                             (oi.ItemStatusReason != OrderItemStatusReasons.transferOut &&
                              oi.ItemStatusReason != OrderItemStatusReasons.canceled)).AsNoTracking();

            var itemList = new List<ExpandoObject>();
            foreach (var orderItem in clubOrderItems)
            {
                var item = Ioc.OrderItemBusiness.GetItem(orderItem.Id);
                var schoolarshipPayment =
                    Ioc.TransactionActivityBusiness.GetAllTransactionByOrderItemId(orderItem.Id)
                        .Where(t => t.PaymentDetail.PaymentMethod == PaymentMethod.Scholarship && t.TransactionStatus == TransactionStatus.Success);
                var scholarShipList = GetScholarshipModel(schoolarshipPayment).ToList();

                dynamic items = new ExpandoObject();
                items.Id = item.Id;
                items.ItemName = item.Name;
                items.User = item.FullName;
                items.UserId = item.Order.UserId;
                items.SeasonName = item.Season.Title;
                items.Date = item.Order.CompleteDate.ToString("MMM dd, yyyy");
                items.Confirmation = item.Order.ConfirmationId;
                items.TotalScholarship = Math.Abs(schoolarshipPayment.Sum(d => d.Amount));

                items.ScholarshipAmount1 = "";
                items.ScholarshipAmount2 = "";
                items.ScholarshipAmount3 = "";

                for (int i = 0; i < scholarShipList.Count; i++)
                {
                    if (i == 0)
                    {
                        items.ScholarshipAmount1 = scholarShipList[0].Fee;
                    }
                    else if (i == 1)
                    {
                        items.ScholarshipAmount2 = scholarShipList[1].Fee;
                    }
                    else if (i == 2)
                    {
                        items.ScholarshipAmount3 = scholarShipList[2].Fee;
                    }
                }

                itemList.Add(items);
            }

            return JsonNet(new { DataSource = itemList, TotalCount = itemList.Count });
        }

        public virtual ActionResult JustGetScholarships(DateTime? start, DateTime? end, PaginationModel paginationModel, bool calucalteTotal = false)
        {
            var clubOrderItems = GetClubOrderItems(start, end).Where(oi => oi.TransactionActivities.Any(t => t.PaymentDetail.PaymentMethod == PaymentMethod.Scholarship));

            if (!calucalteTotal)
            {
                var itemList = new List<object>();
                foreach (var item in clubOrderItems.OrderBy(c => c.Id).Skip((paginationModel.Page - 1) * paginationModel.PageSize).Take(paginationModel.PageSize))
                {
                    itemList.Add(new
                    {
                        item.Id,
                        ItemName = item.Name,
                        User = item.FullName,
                        UserId = item.Order.UserId,
                        Date = item.Order.CompleteDate.ToString("MMM dd, yyyy"),
                        Confirmation = item.Order.ConfirmationId
                    });
                }

                return JsonNet(new { DataSource = itemList, TotalCount = clubOrderItems.Count() });
            }
            else
            {
                decimal totalDiscounts = 0;
                if (clubOrderItems.Any(c => c.TransactionActivities.Any()))
                {
                    totalDiscounts +=
                        Enumerable.Sum(
                            clubOrderItems.SelectMany(
                                clubOrderItem =>
                                    clubOrderItem.TransactionActivities.Where(
                                        t => t.PaymentDetail.PaymentMethod == PaymentMethod.Scholarship)),
                            item => Math.Abs(item.Amount));
                }
                var clubName = ActiveClub.Name;
                var date = _clubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:MM tt");
                var clubCurrency = _clubBusiness.GetClubCurrency(this.GetCurrentClubDomain());

                return JsonNet(new { Total = CurrencyHelper.FormatCurrencyWithPenny(Math.Abs(totalDiscounts), clubCurrency), ClubName = clubName, Date = date });
            }
        }

        public virtual ActionResult GetJustScholarshipDetails(long itemId, PaginationModel paginationModel)
        {
            //var item = Ioc.OrderItemBusiness.GetItem(itemId);
            var schoolarshipPayment =
                Ioc.TransactionActivityBusiness.GetAllTransactionByOrderItemId(itemId)
                    .Where(t => t.PaymentDetail.PaymentMethod == PaymentMethod.Scholarship);

            var model = GetScholarshipModel(schoolarshipPayment);

            return JsonNet(new { DataSource = model, TotalCount = schoolarshipPayment.Count() });
        }

        public virtual ActionResult GetProviderContactInfo(long seasonId, PaginationModel paginationModel, ShareAsAttachment model, bool isShare = false)
        {
            var seasonPrograms = Ioc.ProgramBusiness.GetSeasonPrograms(seasonId).Where(p => p.Status != ProgramStatus.Frozen);
            var season = Ioc.SeasonBusiness.Get(seasonId);

            var models = new List<ProviderContactInfoViewModel>();
            var seasonProgramPaging = (!isShare) ?
                seasonPrograms.OrderBy(c => c.Name)
                    .Skip((paginationModel.Page - 1) * paginationModel.PageSize)
                    .Take(paginationModel.PageSize) : seasonPrograms;
            foreach (var program in seasonProgramPaging)
            {
                var providerName = (program.OutSourceSeasonId.HasValue)
                    ? program.OutSourceSeason.Club.Name
                    : string.Empty;

                var fullName = (program.OutSourceSeasonId.HasValue)
                    ? program.OutSourceSeason.Club.ContactPersons.First().FirstName + " " + program.OutSourceSeason.Club.ContactPersons.First().LastName
                    : program.Club.ContactPersons.First().FirstName + " " + program.Club.ContactPersons.First().LastName;

                var contactEmail = (program.OutSourceSeasonId.HasValue)
                    ? program.OutSourceSeason.Club.ContactPersons.First().Email
                    : program.Club.ContactPersons.First().Email;

                var phone = (program.OutSourceSeasonId.HasValue)
                    ? program.OutSourceSeason.Club.ContactPersons.First().Phone
                    : program.Club.ContactPersons.First().Phone;

                ClubStaff instructor1St = null;
                ClubStaff instructor2nd = null;
                if (program.Instructors != null && program.Instructors.Any())
                {
                    instructor1St = program.Instructors.First();
                    if (program.Instructors.Count > 1)
                    {
                        instructor2nd = program.Instructors.Last();
                    }
                }



                models.Add(new ProviderContactInfoViewModel
                {
                    ClassName = program.Name,
                    ProviderName = providerName,
                    FullName = fullName,
                    ContactEmail = contactEmail,
                    Phone = phone,
                    Instructor1St = (instructor1St != null && instructor1St.Contact != null) ? instructor1St.Contact.FullName : "",
                    Phone1StInstructor = (instructor1St != null && instructor1St.Contact != null) ? instructor1St.Contact.Phone : "",
                    Instructor2nd = (instructor2nd != null && instructor2nd.Contact != null) ? instructor2nd.Contact.FullName : "",
                    Phone2ndInstructor = (instructor2nd != null && instructor2nd.Contact != null) ? instructor2nd.Contact.Phone : "",
                });
            }
            models = models.OrderBy(p => p.ClassName).ToList();
            if (isShare)
            {
                // Save in blob.
                var club = _clubBusiness.Get(ActiveClub.Id);
                var exp = new ExportFileHelper();

                var expsch = Ioc.CustomReportBusiness.ExportProviderContactInfo(models.Select(m => new
                {
                    ClassName = m.ClassName,
                    ProviderName = m.ProviderName,
                    FullName = m.FullName,
                    ContactEmail = m.ContactEmail,
                    Phone = m.Phone,
                    Instructor1St = m.Instructor1St,
                    Phone1StInstructor = m.Phone1StInstructor,
                    Instructor2nd = m.Instructor2nd,
                    Phone2ndInstructor = m.Phone2ndInstructor,

                }).ToList<object>());

                MemoryStream output = exp.ExportExcel(expsch);
                var objcetName = GetReoprtNameWithSeason(seasonId, season.ClubId).Data;
                var fileName = (objcetName != null ? objcetName.ToString() : "") + " (Provider contact info).xls";
                using (var stream = new MemoryStream(output.GetBuffer()))
                {
                    //memType = Constants.E_Pdf_MimeType;
                    //fileName = string.Format(fileName, club.Name, RandomHelper.GenerateGuid(),
                    //    Constants.E_ExcelFileExt);

                    var sm = new StorageManager(WebConfigHelper.AzureStorageConnection,
                        Constants.Path_ClubFilesContainer);
                    sm.UploadBlob(club.Domain, fileName, stream, "application/xls");

                }

                var reportLink = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, fileName).AbsoluteUri;

                var templateModel = new EmailTemplateModel
                {
                    HasLogo = true,
                    HasHeader = true,
                    ClubLogo = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo),
                    Body = string.Format("{0} <br/><hr/> <a href=" + reportLink + "> Download Report </a>", model.Message),
                    IsRegisterationEmail = true
                };

                var body = this.RenderViewToString(Constants.Path_EmailTemplate, templateModel);

                Ioc.OldEmailBusiness.SendEmail(club.ContactPersons.First().Email, model.SentTo, model.Subject,
                    body, true);
                return JsonNet(new JResult { Status = true });
            }
            return JsonNet(new { DataSource = models, TotalCount = seasonPrograms.Count() });
        }

        [JbAuthorize(JbAction.ProtalReport_RoomAssignment)]
        public virtual ActionResult GetRoomAssignments(long seasonId, PaginationModel paginationModel, ShareAsAttachment model, bool isShare = false)
        {
            var programs = Ioc.ProgramBusiness.GetSeasonPrograms(seasonId);
            var season = Ioc.SeasonBusiness.Get(seasonId);
            var models = new List<RoomAssignmentViewModel>();
            var spaceReqString = string.Empty;
            var additionalComments = string.Empty;
            var programPaging = (!isShare) ?
                programs.OrderBy(c => c.Name)
                    .Skip((paginationModel.Page - 1) * paginationModel.PageSize)
                    .Take(paginationModel.PageSize) : programs;
            foreach (var program in programPaging)
            {
                if (program.CatalogId.HasValue)
                {
                    var catalog = Ioc.CatalogBusiness.Get(program.CatalogId.Value);
                    var spaceReq = catalog.CatalogItemOptions.Where(o => o.Type == CatalogOptionType.SpaceRequrement).ToList();
                    spaceReqString = spaceReq.Aggregate("",
                        (current, space) => current + string.Format(" {0},", space.Title));

                    spaceReqString = (!string.IsNullOrEmpty(spaceReqString)) ? spaceReqString.Remove(spaceReqString.Length - 1) : string.Empty;
                    //var priceOptions = catalog.PriceOptions.Select(p => new CatalogPriceViewModel
                    //{
                    //    //Amount = p.Amount,
                    //    DurationTitle = p.DurationTitle,
                    //    //Title = p.Title,
                    //}).DistinctBy(c => c.DurationTitle);
                    //classDuration = priceOptions.Aggregate(string.Empty, (current, item) => current + string.Format(" {0},", item.DurationTitle));
                    //classDuration = (!string.IsNullOrEmpty(classDuration)) ? classDuration.Remove(classDuration.Length - 1) : string.Empty;

                    additionalComments = catalog.Detail;
                }

                var classDuration = string.Empty;

                foreach (var programSchedule in program.ProgramSchedules)
                {
                    var scheduleDays = ((ScheduleAttribute)programSchedule.Attributes).Days;
                    foreach (var scheduleDay in scheduleDays)
                    {
                        classDuration += string.Format(" {0} ({1} - {2}),", scheduleDay.DayOfWeek, new DateTime(scheduleDay.StartTime.Value.Ticks).ToString(@"hh:mm tt"), new DateTime(scheduleDay.EndTime.Value.Ticks).ToString(@"hh:mm tt"));
                    }
                }

                classDuration = (!string.IsNullOrEmpty(classDuration)) ? classDuration.Remove(classDuration.Length - 1) : string.Empty;

                var schedule = program.ProgramSchedules.First();
                var max = schedule.Attributes.Capacity;
                var min = schedule.Attributes.MinimumEnrollment;
                var days = ((ScheduleAttribute)schedule.Attributes).Days.Select(d => d.DayOfWeek);
                var day = days.Aggregate(string.Empty, (current, item) => current + string.Format(" {0},", item));
                day = day.Remove(day.Length - 1);
                var room = program.Room;

                var classDate = Ioc.ProgramBusiness.GetClassDates(program.ProgramSchedules.First());
                //var sessions = Ioc.ProgramBusiness.GetSessions(program);
                var sessionsString = classDate.Aggregate(string.Empty,
                    (current, session) => current + string.Format(" {0},", session.SessionDate.ToString(@"MMM dd")));

                sessionsString = sessionsString.Remove(sessionsString.Length - 1);

                var newRow = new RoomAssignmentViewModel
                {
                    ClassName = program.Name,
                    SpaceReq = spaceReqString,
                    Duration = classDuration,
                    Day = day,
                    Sessions = sessionsString,
                    Min = min,
                    Max = max,
                    Room = room,
                    AdditionalComments = additionalComments
                };
                models.Add(newRow);
            }

            models = models.OrderBy(c => c.ClassName).ToList();

            if (isShare)
            {
                // Save in blob.
                var club = _clubBusiness.Get(ActiveClub.Id);
                var exp = new ExportFileHelper();
                var expsch = Ioc.CustomReportBusiness.ExportRoomAssigments(models.ToList<object>());

                MemoryStream output = exp.ExportExcel(expsch);
                var objcetName = GetReoprtNameWithSeason(seasonId, season.ClubId).Data;

                var fileName = (objcetName != null ? objcetName.ToString() : "") + " (Room assignments).xls";
                using (var stream = new MemoryStream(output.GetBuffer()))
                {
                    var sm = new StorageManager(WebConfigHelper.AzureStorageConnection,
                        Constants.Path_ClubFilesContainer);
                    sm.UploadBlob(club.Domain, fileName, stream, "application/xls");

                }

                var reportLink = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, fileName).AbsoluteUri;

                var templateModel = new EmailTemplateModel
                {
                    HasLogo = true,
                    HasHeader = true,
                    ClubLogo = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo),
                    Body = string.Format("{0} <br/><hr/> <a href=" + reportLink + "> Download Report </a>", model.Message),
                    IsRegisterationEmail = true
                };

                var body = this.RenderViewToString(Constants.Path_EmailTemplate, templateModel);

                Ioc.OldEmailBusiness.SendEmail(club.ContactPersons.First().Email, model.SentTo, model.Subject,
                    body, true);
                return JsonNet(new JResult { Status = true });
            }
            return JsonNet(new { DataSource = models, TotalCount = programs.Count() });
        }

        public virtual ActionResult GetSeasonProcessingDates(string seasonTitle, PaginationModel paginationModel)
        {
            var club = ActiveClub;
            var relatedClubs =
                 _clubBusiness.GetRelatedClubs(club.Id)
                    .Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.School);

            var allSeasons =
                relatedClubs.SelectMany(c => c.Seasons)
                    .Where(
                        s =>
                            s.Status != SeasonStatus.Deleted &&
                            s.Title.ToLower().Replace(" ", string.Empty) == seasonTitle);

            var model = new List<object>();

            foreach (var season in allSeasons.OrderBy(c => c.Id).Skip((paginationModel.Page - 1) * paginationModel.PageSize).Take(paginationModel.PageSize))
            {
                model.Add(GetSeasonProcessingModel(season));
            }

            return JsonNet(new { DataSource = model, TotalCount = allSeasons.Count() });
        }

        public virtual ActionResult GetADM24Data(long seasonId, PaginationModel paginationModel)
        {
            var seasonPrograms = Ioc.ProgramBusiness.GetSeasonPrograms(seasonId).Where(p => p.OutSourceSeasonId.HasValue);
            var model = new List<object>();
            var spaceReqString = string.Empty;

            foreach (var program in seasonPrograms.OrderBy(c => c.Id).Skip((paginationModel.Page - 1) * paginationModel.PageSize)
                        .Take(paginationModel.PageSize))
            {
                var providerName = program.OutSourceSeason.Club.Name;

                var catalog = Ioc.CatalogBusiness.Get(program.CatalogId.Value);
                var spaceReq = catalog.CatalogItemOptions.Where(o => o.Type == CatalogOptionType.SpaceRequrement).ToList();
                spaceReqString = spaceReq.Aggregate("",
                    (current, space) => current + string.Format(" {0},", space.Title));

                spaceReqString = (!string.IsNullOrEmpty(spaceReqString)) ? spaceReqString.Remove(spaceReqString.Length - 1) : string.Empty;
                var schedule = program.ProgramSchedules.First(c => c.IsDeleted == false);
                var max = schedule.Attributes.Capacity;
                var days = ((ScheduleAttribute)schedule.Attributes).Days.Select(d => d.DayOfWeek);
                var day = days.Aggregate(string.Empty, (current, item) => current + string.Format(" {0},", item));
                day = day.Remove(day.Length - 1);

                var classTimes = ((ScheduleAttribute)schedule.Attributes).Days.Select(d => d.StartTime);
                List<string> classTimesFormat = new List<string>();
                foreach (var item in classTimes)
                {
                    classTimesFormat.Add(DateTime.Today.Add(item.Value).ToString("h:m tt"));
                }
                var classTime = classTimesFormat.Aggregate(string.Empty, (current, item) => current + string.Format(" {0},", item));
                classTime = classTime.Remove(classTime.Length - 1);

                var classDate = Ioc.ProgramBusiness.GetClassDates(schedule);
                var sessionsString = classDate.Aggregate(string.Empty,
                    (current, session) => current + string.Format(" {0},", session.SessionDate.ToString(@"MMM dd")));
                sessionsString = sessionsString.Remove(sessionsString.Length - 1);
                var clubCurrency = _clubBusiness.GetClubCurrency(this.GetCurrentClubDomain());

                var newRow = new
                {
                    ClassName = program.Name,
                    SpaceReq = spaceReqString,
                    ProviderName = providerName,
                    Day = day,
                    ClassTimes = classTime,
                    Sessions = sessionsString,
                    Max = max,
                    Fee = CurrencyHelper.FormatCurrencyWithPenny(schedule.Charges.First(c => c.Category == ChargeDiscountCategory.EntryFee && c.IsDeleted == false).Amount, clubCurrency)
                };
                model.Add(newRow);

            }

            return JsonNet(new { DataSource = model, TotalCount = seasonPrograms.Count() });
        }

        public virtual ActionResult GetInsuranceReportInfo(PaginationModel paginationModel)
        {
            var relatedClubs = _clubBusiness.GetRelatedClubs(ActiveClub.Id).Where(c => !c.IsInActive && c.ClubType != null && ((c.ClubType.EnumType == ClubTypesEnum.Provider) || (c.ClubType.ParentType != null && c.ClubType.ParentType.EnumType == ClubTypesEnum.Provider)));

            var allProviders =
                relatedClubs.ToList()
                    .Where(
                        c =>
                            c.Setting != null && c.Setting.CertificateOfInsurances != null &&
                            c.Setting.CertificateOfInsurances.CertificateList != null && c.Setting.CertificateOfInsurances.CertificateList.Any());

            var model = new List<object>();

            foreach (var provider in allProviders.OrderBy(c => c.Id).Skip((paginationModel.Page - 1) * paginationModel.PageSize)
                        .Take(paginationModel.PageSize))
            {
                model.Add(new
                {
                    ProviderName = provider.Name,
                    Email = (provider.ContactPersons != null && provider.ContactPersons.Any()) ? provider.ContactPersons.First().Email : string.Empty,
                    Insurance = string.Join(",", provider.Setting.CertificateOfInsurances.CertificateList.Select(c => c.CertificateOfInsuranceType.ToDescription())),
                    Date = provider.Setting.CertificateOfInsurances.PolicyExpirationDate.HasValue
                    ? provider.Setting.CertificateOfInsurances.PolicyExpirationDate.Value.ToString(
                        Constants.DefaultDateFormat)
                    : String.Empty
                });
            }

            return JsonNet(new { DataSource = model, TotalCount = allProviders.Count() });
        }

        [HttpGet]
        public virtual ActionResult GetProviderAddressListReportInfo(DateTime? startDate, DateTime? endDate, bool printMode = false)
        {
            var clubId = ActiveClub.Id;
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            var providers = _clubBusiness.GetRelatedClubs(ActiveClub.Id).Where(c => c.ClubType.ParentId.Value == 3);

            var data = providers.ToList().Select(p => new ProviderAddressListReportViewModel()
            {
                ProviderName = p.Name,
                CommissionRate = p.PartnerCommisionRate,
                Email = p.ContactPersons.SingleOrDefault().Email,
                ContactName = p.ContactPersons != null ? p.ContactPersons.SingleOrDefault().FullName : "-",
                StreetAddress = p.Address != null ? !string.IsNullOrEmpty(p.Address.Address) ? p.Address.Address.Substring(0, p.Address.Address.IndexOf(",")) : "-" : "-",
                City = p.Address != null ? !string.IsNullOrEmpty(p.Address.City) ? p.Address.City : "-" : "-",
                State = p.Address != null ? !string.IsNullOrEmpty(p.Address.State) ? p.Address.State : "-" : "-",
                Zip = p.Address != null ? !string.IsNullOrEmpty(p.Address.Zip) ? p.Address.Zip : "-" : "-",
                AgreementExpirationDate = p.ExpirationDate
            });

            if (startDate != null)
            {
                data =
                   data.Where(
                        oitem =>
                            oitem.AgreementExpirationDate.HasValue && oitem.AgreementExpirationDate.Value >= startDate);
            }
            if (endDate != null)
            {
                var date = endDate;
                date = date.Value.AddHours(24 - date.Value.Hour);
                data =
                    data.Where(
                        oitem =>
                            oitem.AgreementExpirationDate.HasValue && oitem.AgreementExpirationDate.Value < date);
            }

            if (printMode)
            {
                dynamic model = new System.Dynamic.ExpandoObject();
                model.Date = _clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
                model.Data = data;
                var ClubName = ActiveClub.Name;
                model.ClubName = ClubName;

                return Pdf(ClubName + " (ProviderAddressList)", "_ProviderAddressListReportPdfExportView", model);
            }

            var dataSource = data.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            return JsonNet(new { DataSource = dataSource, TotalCount = data.ToList().Count });

        }

        public virtual ActionResult GetReportHeader(int? clubId = null, int? seasonId = null)
        {
            var CurrentClubId = ActiveClub.Id;

            var data = new ReportHeaderViewModel()
            {
                ClubName = ActiveClub.Name,
                Date = _clubBusiness.GetClubDateTime(CurrentClubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt"),//Sep 21, 2016 HH:MM                
            };

            if (clubId.HasValue)
            {
                data.SelectedClubName = _clubBusiness.Get(clubId.Value).Name;
            }

            if (clubId.HasValue && seasonId.HasValue)
            {
                var season = _clubBusiness.Get(clubId.Value).Seasons.First(s => s.Id == seasonId);
                data.SeasonName = (season != null && !string.IsNullOrEmpty(season.Title) ? season.Title : "-");
            }

            data.InvoiceTypes = DropdownHelpers.ToSelectList<InvoiceType>();


            return JsonNet(data);
        }


        [HttpGet]
        public virtual ActionResult GetSchoolAddressListReportInfo(DateTime? startDate, DateTime? endDate, bool printMode = false)
        {
            var clubId = ActiveClub.Id;
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            var schools = _clubBusiness.GetRelatedClubs(ActiveClub.Id).Where(c => c.ClubType.ParentId.Value == 2);

            var data = schools.ToList().Select(p => new SchoolAddressListReportViewModel()
            {
                SchoolName = p.Name,
                CoordinatorName = p.ContactPersons != null ? p.ContactPersons.SingleOrDefault().FullName : "-",
                StreetAddress = p.Address != null ?
                                  (!string.IsNullOrEmpty(p.Address.Address) ?
                                      (p.Address.Address.Contains(",") ? p.Address.Address.Substring(0, p.Address.Address.IndexOf(",")) : p.Address.Address) :
                                  "-") :
                                "-",
                City = p.Address != null ? !string.IsNullOrEmpty(p.Address.City) ? p.Address.City : "-" : "-",
                State = p.Address != null ? !string.IsNullOrEmpty(p.Address.State) ? p.Address.State : "-" : "-",
                Zip = p.Address != null ? !string.IsNullOrEmpty(p.Address.Zip) ? p.Address.Zip : "-" : "-",
                AgreementExpirationDate = p.ExpirationDate
            });

            if (startDate != null)
            {
                data =
                   data.Where(
                        oitem =>
                            oitem.AgreementExpirationDate.HasValue && oitem.AgreementExpirationDate.Value >= startDate);
            }
            if (endDate != null)
            {
                var date = endDate;
                date = date.Value.AddHours(24 - date.Value.Hour);
                data =
                    data.Where(
                        oitem =>
                            oitem.AgreementExpirationDate.HasValue && oitem.AgreementExpirationDate.Value < date);
            }

            if (printMode)
            {
                dynamic model = new System.Dynamic.ExpandoObject();
                model.Date = _clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
                model.Data = data;
                var ClubName = ActiveClub.Name;
                model.ClubName = ClubName;
                return Pdf(ClubName + " (SchoolAddressList)", "_SchoolAddressListReportPdfExportView", model);
            }

            var dataSource = data.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            return Json(new { DataSource = dataSource, TotalCount = data.ToList().Count }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public virtual ActionResult GetMemberListReportInfo(DateTime? startDate, DateTime? endDate, string typeId, string activeStatus, bool printMode = false)
        {
            var clubId = ActiveClub.Id;
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            var relatedClub = _clubBusiness.GetRelatedClubs(ActiveClub.Id);

            if (activeStatus == "1")
            {
                relatedClub = relatedClub.Where(c => c.IsInActive == false);
            }
            else if (activeStatus == "2")
            {
                relatedClub = relatedClub.Where(c => c.IsInActive == true);
            }

            var inttypeId = 0;
            if (!string.IsNullOrEmpty(typeId) && typeId != "-1")
            {
                inttypeId = (int.Parse(typeId));
                relatedClub = relatedClub.Where(c => c.ClubType.ParentId.Value == inttypeId);
            }



            var data = relatedClub.ToList().Select(p => new MemberListReportViewModel()
            {
                Name = !string.IsNullOrEmpty(p.Name) ? p.Name : "-",
                ContactName = p.ContactPersons.Any() && p.ContactPersons != null ? p.ContactPersons.SingleOrDefault().FullName : "-",
                EmailAddress = p.ContactPersons.Any() && p.ContactPersons != null ? p.ContactPersons.SingleOrDefault().Email : "-",
                PhoneNumber = p.ContactPersons.Any() && p.ContactPersons != null ? p.ContactPersons.SingleOrDefault().Phone : "-",
                AgreementExpirationDate = p.ExpirationDate
            });

            if (startDate != null)
            {
                data =
                   data.Where(
                        oitem =>
                            oitem.AgreementExpirationDate.HasValue && oitem.AgreementExpirationDate.Value >= startDate);
            }
            if (endDate != null)
            {
                var date = endDate;
                date = date.Value.AddHours(24 - date.Value.Hour);
                data =
                    data.Where(
                        oitem =>
                            oitem.AgreementExpirationDate.HasValue && oitem.AgreementExpirationDate.Value < date);
            }

            if (printMode)
            {
                dynamic model = new System.Dynamic.ExpandoObject();
                model.Date = _clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
                model.Data = data;
                var ClubName = ActiveClub.Name;
                model.ClubName = ClubName;

                return Pdf(ClubName + " (MemberList)", "_MemberListReportPdfExportView", model);
            }

            var dataSource = data.OrderBy(d => d.Name).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            return JsonNet(new { DataSource = dataSource, TotalCount = data.ToList().Count });

        }

        [HttpGet]
        public virtual ActionResult GetSchoolReconciliationInfoReportInfo(DateTime? startDate, DateTime? endDate, bool printMode)
        {
            var clubId = ActiveClub.Id;
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            var schools =
               _clubBusiness.GetRelatedClubs(clubId)
                   .Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.School)
                   .OrderBy(s => s.Name);

            var data = new List<SchoolReconciliationInfoReportViewModel>();

            foreach (var item in schools)
            {
                data.Add(new SchoolReconciliationInfoReportViewModel()
                {
                    SchoolName = !string.IsNullOrEmpty(item.Name) ? item.Name : "-",
                    AgreementExpirationDate = item.ExpirationDate,
                    IsCashPayments = ((SchoolSetting)item.Setting).RegistrationInfo.ReconciliationDetail != null ?
                                     (((SchoolSetting)item.Setting).RegistrationInfo.ReconciliationDetail.Contains("CashPayments") ? "Yes" : "") : "",
                    IsDonations = ((SchoolSetting)item.Setting).RegistrationInfo.ReconciliationDetail != null ?
                                     (((SchoolSetting)item.Setting).RegistrationInfo.ReconciliationDetail.Contains("Donations") ? "Yes" : "") : "",
                    IsPTAFees = ((SchoolSetting)item.Setting).RegistrationInfo.ReconciliationDetail != null ?
                                     (((SchoolSetting)item.Setting).RegistrationInfo.ReconciliationDetail.Contains("PTAFees") ? "Yes" : "") : "",
                    IsPTAScholarships = ((SchoolSetting)item.Setting).RegistrationInfo.ReconciliationDetail != null ?
                                     (((SchoolSetting)item.Setting).RegistrationInfo.ReconciliationDetail.Contains("PTAScholarships") ? "Yes" : "") : ""
                });
            }


            if (startDate != null)
            {
                data =
                   data.Where(
                        oitem =>
                            oitem.AgreementExpirationDate.HasValue && oitem.AgreementExpirationDate.Value >= startDate).ToList();
            }
            if (endDate != null)
            {
                var date = endDate;
                date = date.Value.AddHours(24 - date.Value.Hour);
                data =
                    data.Where(
                        oitem =>
                            oitem.AgreementExpirationDate.HasValue && oitem.AgreementExpirationDate.Value < date).ToList();
            }

            if (printMode)
            {
                dynamic model = new System.Dynamic.ExpandoObject();
                model.Date = _clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
                model.Data = data;
                var ClubName = ActiveClub.Name;
                model.ClubName = ClubName;

                return Pdf(ClubName + " (School Reconciliation Info)", "_SchoolReconciliationInfoReportPdfExportView", model);
            }

            var dataSource = data.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            return JsonNet(new { DataSource = dataSource, TotalCount = data.ToList().Count });

        }



        [HttpPost]
        public virtual ActionResult GetInstructorListReportInfo(SelectedProvidersViewModel model, bool printMode = false)
        {
            var clubId = ActiveClub.Id;

            var InstructorModel = getInstructorListReportInfo(model);

            var pageSize = 10;
            int.TryParse(Request.Params["PageSize"], out pageSize);
            var page = 0;
            int.TryParse(Request.Params["Page"], out page);
            var InstructorModelPaging = InstructorModel.Skip((page - 1) * pageSize).Take(pageSize > 0 ? pageSize : 10);
            return Json(new { DataSource = InstructorModelPaging, TotalCount = InstructorModel.Count }, JsonRequestBehavior.AllowGet);

        }

        private List<InstructorListPortalReportViewModel> getInstructorListReportInfo(SelectedProvidersViewModel model)
        {
            var clubId = ActiveClub.Id;
            var instructors = new List<ClubStaff>();
            int countSelectedProvider = 0;
            if (model.IsAllProviders)
            {
                countSelectedProvider = -1;
                var allProviders = new List<int>();

                allProviders =
                  _clubBusiness.GetRelatedClubs(clubId).Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.Provider && !c.IsInActive).Select(c => c.Id).ToList();

                foreach (var providerId in allProviders)
                {
                    var clubInstructors = _clubBusiness.GetClubInstructors(providerId).Where(c => !c.IsDeleted).ToList();
                    foreach (var instructor in clubInstructors)
                    {
                        instructors.Add(instructor);
                    }

                }
            }
            else
            {
                foreach (var providerId in model._SelectedProviders)
                {
                    var clubInstructors = _clubBusiness.GetClubInstructors(providerId).Where(c => !c.IsDeleted).ToList();
                    foreach (var instructor in clubInstructors)
                    {
                        instructors.Add(instructor);
                    }
                    countSelectedProvider++;
                }

            }
            var InstructorModel = (instructors.Select(i =>
            new InstructorListPortalReportViewModel()
            {
                Name = (i.Contact != null && !string.IsNullOrEmpty(i.Contact.FullName)) ? i.Contact.FullName : string.Empty,
                CellPhone = i.Contact.Phone != null ? i.Contact.Phone : "-",
                DateOfBackgroundCheck = i.DateOfBackgroundCheck != null ? string.Format("{0:MMM dd, yyyy}", i.DateOfBackgroundCheck) : "-",
                Status = i.IsFrozen ? "Frozen" : i.Status.ToString(),
                ProviderName = i.Club.Name,


            })).ToList();

            return InstructorModel;
        }

        [HttpPost]
        public virtual ActionResult DownloadResult(string data)
        {
            var clubId = ActiveClub.Id;
            dynamic modelPdf = new System.Dynamic.ExpandoObject();
            var model = JsonConvert.DeserializeObject<SelectedProvidersViewModel>(data);

            var result = getInstructorListReportInfo(model);
            var ClubName = ActiveClub.Name;

            modelPdf.Data = result;
            modelPdf.ClubName = ClubName;
            modelPdf.Date = _clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
            //modelPdf.ProviderNames = countSelectedProvider == 1 ? InstructorModel.First().ProviderName : string.Format("{0} ,{1}", InstructorModel.First().ProviderName, "...");

            return Pdf(ClubName + " (InstructorList)", "_InstructorListPortalReportPdfExportView", modelPdf);
        }

        [HttpGet]
        public virtual ActionResult GetInsuranceCertificateCheckReportInfo(DateTime? startDate, DateTime? endDate, bool printMode = false)
        {
            var clubId = ActiveClub.Id;
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            var relatedClubs = _clubBusiness.GetRelatedClubs(ActiveClub.Id).Where(c => !c.IsInActive && c.ClubType != null && ((c.ClubType.EnumType == ClubTypesEnum.Provider) || (c.ClubType.ParentType != null && c.ClubType.ParentType.EnumType == ClubTypesEnum.Provider)));

            var allProviders =
                relatedClubs.ToList()
                    .Where(
                        c =>
                            c.Setting != null && c.Setting.CertificateOfInsurances != null &&
                            c.Setting.CertificateOfInsurances.CertificateList != null && c.Setting.CertificateOfInsurances.CertificateList.Any());

            var data = new List<InsuranceCertificateCheckReportViewModel>();

            foreach (var provider in allProviders)
            {
                data.Add(new InsuranceCertificateCheckReportViewModel()
                {
                    ProviderName = !string.IsNullOrEmpty(provider.Name) ? provider.Name : "-",
                    Email = (provider.ContactPersons != null && provider.ContactPersons.Any()) ? provider.ContactPersons.First().Email : string.Empty,
                    Insurance = string.Join(",", provider.Setting.CertificateOfInsurances.CertificateList.Select(c => c.CertificateOfInsuranceType.ToDescription())),
                    Date = provider.Setting.CertificateOfInsurances.PolicyExpirationDate.HasValue
                    ? provider.Setting.CertificateOfInsurances.PolicyExpirationDate.Value.ToString(
                        Constants.DefaultDateFormat)
                    : String.Empty,
                    AgreementExpirationDate = provider.Setting.CertificateOfInsurances.PolicyExpirationDate
                });
            }

            if (startDate != null)
            {
                data =
                   data.Where(
                        oitem =>
                            oitem.AgreementExpirationDate.HasValue && oitem.AgreementExpirationDate.Value >= startDate).ToList();
            }
            if (endDate != null)
            {
                var date = endDate;
                date = date.Value.AddHours(24 - date.Value.Hour);
                data =
                    data.Where(
                        oitem =>
                            oitem.AgreementExpirationDate.HasValue && oitem.AgreementExpirationDate.Value < date).ToList();
            }

            if (printMode)
            {
                dynamic model = new System.Dynamic.ExpandoObject();
                model.Date = _clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
                model.Data = data;
                var ClubName = ActiveClub.Name;
                model.ClubName = ClubName;

                return Pdf(ClubName + " (Insurance certificate check)", "_InsuranceCertificateCheckReportPdfExportView", model);
            }

            var dataSource = data.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            return JsonNet(new { DataSource = data, TotalCount = data.ToList().Count() });
        }

        [HttpGet]
        public virtual ActionResult GetReconciliationPrepReportInfo(int seasonId, int schoolId, bool printMode = false)
        {
            var clubId = ActiveClub.Id;
            var data = new List<ReconciliationPrepReportViewModel>();
            var school = _clubBusiness.Get(schoolId);
            var programs = Ioc.ProgramBusiness.GetList().Where(p => p.SeasonId == seasonId && p.Status != ProgramStatus.Deleted && p.LastCreatedPage >= ProgramPageStep.Step4 && p.TypeCategory == ProgramTypeCategory.Class);

            foreach (var program in programs)
            {
                var minGrade = program.ProgramSchedules.First().AttendeeRestriction.MinGrade;
                var maxGrade = program.ProgramSchedules.First().AttendeeRestriction.MaxGrade;


                data.Add(new ReconciliationPrepReportViewModel()
                {
                    ProviderName = (program.OutSourceSeasonId.HasValue && program.OutSourceSeason != null) ? program.OutSourceSeason.Club.Name : "-",
                    ClassName = (program.Name != null) ? program.Name : "-",

                    ClassFee = program.ProgramSchedules.First().Charges.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).First().Amount,

                    EMRate = (program.CustomFields.CommisionRate.HasValue && program.CustomFields.CommisionRate != 0) ? program.CustomFields.CommisionRate.Value : (program.OutSourceSeason != null ? program.OutSourceSeason.Club.PartnerCommisionRate : (int?)null),

                    GradeRange = (program.ProgramSchedules.First().AttendeeRestriction.RestrictionType == RestrictionType.Grade) ?
                                 (!string.IsNullOrEmpty(minGrade.ToDescription()) && !string.IsNullOrEmpty(maxGrade.ToDescription())) ? string.Format("{0} - {1}", minGrade.ToDescription(), maxGrade.ToDescription()) :
                                 (!string.IsNullOrEmpty(minGrade.ToDescription()) && string.IsNullOrEmpty(maxGrade.ToDescription())) ? string.Format("Min : {0}", minGrade.ToDescription()) :
                                 (string.IsNullOrEmpty(minGrade.ToDescription()) && !string.IsNullOrEmpty(maxGrade.ToDescription())) ? string.Format("Max : {0}", maxGrade.ToDescription()) : "-" :
                                 "-"
                });

            }

            if (printMode)
            {
                dynamic model = new System.Dynamic.ExpandoObject();
                var clubName = ActiveClub.Name;
                var season = _clubBusiness.Get(schoolId).Seasons.First(s => s.Id == seasonId);

                model.Date = _clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
                model.Data = data;
                model.ClubName = clubName != null ? clubName : "-";
                model.SchoolName = school.Name != null ? school.Name : "-";
                model.SeasonName = (season != null && !string.IsNullOrEmpty(season.Title) ? season.Title : "-");


                return Pdf(clubName + " (Reconciliation Prep)", "_ReconciliationPrepReportPdfExportView", model);
            }

            return JsonNet(new { DataSource = data, TotalCount = data.Count });

        }


        [HttpPost]
        public virtual ActionResult GetMemberTaxIdStatusReportInfo(DateTime? startDate, DateTime? endDate, string typeId, string activeStatus, bool printMode = false)
        {
            var clubId = ActiveClub.Id;
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            var relatedClub = _clubBusiness.GetRelatedClubs(ActiveClub.Id);

            if (activeStatus == "1")
            {
                relatedClub = relatedClub.Where(c => c.IsInActive == false);
            }
            else if (activeStatus == "2")
            {
                relatedClub = relatedClub.Where(c => c.IsInActive == true);
            }

            var inttypeId = 0;
            if (!string.IsNullOrEmpty(typeId) && typeId != "-1")
            {
                inttypeId = (int.Parse(typeId));
                relatedClub = relatedClub.Where(c => c.ClubType.ParentId.Value == inttypeId);
            }


            var data = relatedClub.ToList().Select(p => new MemberTaxIdStatusReportViewModel()
            {
                Name = !string.IsNullOrEmpty(p.Name) ? p.Name : "-",
                TaxIdFEIN = !string.IsNullOrEmpty(p.Setting.TaxIDFEIN) ? p.Setting.TaxIDFEIN : "-",
                TaxIdSSN = !string.IsNullOrEmpty(p.Setting.TaxIDSSN) ? p.Setting.TaxIDSSN : "-",
                Provider1099 = p.Setting.Provider1099,
                AgreementExpirationDate = p.ExpirationDate
            });

            if (startDate != null)
            {
                data =
                   data.Where(
                        oitem =>
                            oitem.AgreementExpirationDate.HasValue && oitem.AgreementExpirationDate.Value >= startDate);
            }
            if (endDate != null)
            {
                var date = endDate;
                date = date.Value.AddHours(24 - date.Value.Hour);
                data =
                    data.Where(
                        oitem =>
                            oitem.AgreementExpirationDate.HasValue && oitem.AgreementExpirationDate.Value < date);
            }

            if (printMode)
            {
                dynamic model = new System.Dynamic.ExpandoObject();
                model.Date = _clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
                model.Data = data;
                var ClubName = ActiveClub.Name;
                model.ClubName = ClubName;

                return Pdf(ClubName + " (MemberTaxIdStatus)", "_MemberTaxIdStatusReportPdfExportView", model);
            }

            var dataSource = data.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            return JsonNet(new { DataSource = dataSource, TotalCount = data.ToList().Count });

        }


        [HttpGet]
        public virtual JsonNetResult GetSessionStatementDataReportHeader()
        {
            var club = ActiveClub;
            var years = new List<int>();

            for (int i = DateTime.Now.AddYears(1).Year; i >= DateTime.Now.AddYears(-4).Year; i--)
            {
                years.Add(i);
            }

            var selectYears = years.Select(y =>
            new SelectListItem
            {
                Text = y.ToString(),
                Value = y.ToString()
            })
            .ToList();


            var model = new
            {
                ClubName = ActiveClub.Name,
                Date = _clubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt"),//Sep 21, 2016 HH:MM                
                Years = selectYears,
            };

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult GetSessionStatementDataReportInfo(string seasonName, string year)
        {
            var clubId = ActiveClub.Id;
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            var season = seasonName + year;
            season = season.Replace(" ", string.Empty);

            var schools =
              _clubBusiness.GetRelatedClubs(clubId)
                  .Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.School)
                  .OrderBy(s => s.Name);

            var seasons = schools.SelectMany(s => s.Seasons).Where(s => s.Title.ToLower().Replace(" ", string.Empty).Contains(season.ToLower()));

            var programQuery = seasons.SelectMany(s => s.Programs.Where(p => p.Status != ProgramStatus.Deleted && p.TypeCategory == ProgramTypeCategory.Class && p.LastCreatedPage >= ProgramPageStep.Step4));
            var programs = programQuery.OrderBy(p => p.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            var programBusiness = Ioc.ProgramBusiness;

            var coupons = Ioc.CouponBusiness.GetList(seasons.Select(s => s.Id).ToList()).Where(c => !c.Deleted && c.IsAllProgram);

            var data = programs.Select(p => new SessionStatementDataReportViewModel()
            {
                SeasonName = !string.IsNullOrEmpty(p.Season.Title) ? p.Season.Title : "-",
                SeasonRosterDueDate = p.Season.Setting != null && p.Season.Setting.SeasonProgramInfoSetting != null ? string.Format("{0:MMM dd, yyyy}", p.Season.Setting.SeasonProgramInfoSetting.MiscellaneousInformationRostersDue) : "-",
                SeasonPaymentsIssuedDate = p.Season.Setting != null && p.Season.Setting.SeasonProgramInfoSetting != null ? string.Format("{0:MMM dd, yyyy}", p.Season.Setting.SeasonProgramInfoSetting.MiscellaneousInformationPaymentsIssued) : " - ",

                ProgramName = !string.IsNullOrEmpty(p.Name) ? p.Name : "-",
                ProviderName = p.OutSourceSeason != null ? p.OutSourceSeason.Club.Name : "-",

                Cost = p.ProgramSchedules.Any() ? p.ProgramSchedules.First().Charges.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).First().Amount : 0,
                TotalOfRegistrations = p.ProgramSchedules.SelectMany(o => o.OrderItems).Where(c => c.ItemStatus == OrderItemStatusCategories.completed && c.Order.IsLive).Any() ? p.ProgramSchedules.SelectMany(o => o.OrderItems).Where(c => c.ItemStatus == OrderItemStatusCategories.completed && c.Order.IsLive).Count() : 0,
                TotalOfProviderFunded = p.ProgramSchedules.SelectMany(o => o.OrderItems)
                             .Where(c => c.ItemStatus == OrderItemStatusCategories.completed && c.Order.IsLive && c.OrderChargeDiscounts.Any())
                             .SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Coupon && c.CouponId.HasValue
                             && Ioc.ProgramBusiness.GetProviderFundedCoupons(p, coupons.ToList()).Select(s => s.Id)
                             .Contains(c.CouponId.Value)).Count(),
                ProviderAddress = p.OutSourceSeason != null ? p.OutSourceSeason.Club.ClubLocations.First().PostalAddress.Address : "-",
                ProviderContactEmail = p.OutSourceSeason != null ? p.OutSourceSeason.Club.ContactPersons.FirstOrDefault().Email : "-",
                ProviderContactName = p.OutSourceSeason != null ? p.OutSourceSeason.Club.ContactPersons.FirstOrDefault().FullName : "-",
                EMRate = string.Format("{0} %", programBusiness.GetEMRate(p))
            })
            .ToList();

            var dataSource = data;
            return JsonNet(new { DataSource = dataSource, TotalCount = programQuery.Count() });

        }


        [HttpGet]
        public virtual ActionResult GetSeasonSurveyDatesReportInfo(string term = "", bool printMode = false)
        {
            var clubId = ActiveClub.Id;
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }


            var schools =
               _clubBusiness.GetRelatedClubs(clubId)
                   .Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.School)
                   .OrderBy(s => s.Name);

            var data = new List<SeasonSurveyDatesReportViewModel>();

            var models = schools.SelectMany(s => s.Seasons).Where(s => s.Title.Contains(term));

            foreach (var item in models)
            {
                data.Add(new SeasonSurveyDatesReportViewModel()
                {
                    Season = item.Title,
                    SurveyOpen = (((SeasonSchoolSetting)item.Setting).SchoolSeasonDetailSetting != null && ((SeasonSchoolSetting)item.Setting).SchoolSeasonDetailSetting.SurveyOpens != null) ? string.Format("{0:MMM dd, yyyy}", ((SeasonSchoolSetting)item.Setting).SchoolSeasonDetailSetting.SurveyOpens) : "-",
                    SurveyClose = (((SeasonSchoolSetting)item.Setting).SchoolSeasonDetailSetting != null && ((SeasonSchoolSetting)item.Setting).SchoolSeasonDetailSetting.SurveyCloses != null) ? string.Format("{0:MMM dd, yyyy}", ((SeasonSchoolSetting)item.Setting).SchoolSeasonDetailSetting.SurveyCloses) : "-",
                    SurveyResultsDue = (((SeasonSchoolSetting)item.Setting).SchoolSeasonDetailSetting != null && ((SeasonSchoolSetting)item.Setting).SchoolSeasonDetailSetting.SurveyResultsDue != null) ? string.Format("{0:MMM dd, yyyy}", ((SeasonSchoolSetting)item.Setting).SchoolSeasonDetailSetting.SurveyResultsDue) : "-",
                });
            }

            if (printMode)
            {
                dynamic model = new System.Dynamic.ExpandoObject();
                model.Date = _clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
                model.Data = data;
                var ClubName = ActiveClub.Name;
                model.ClubName = ClubName;

                return Pdf(ClubName + " (Season survey dates)", "_SeasonSurveyDatesReportPdfExportView", model);
            }

            var dataSource = data.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            return JsonNet(new { DataSource = dataSource, TotalCount = data.ToList().Count() });
        }


        [HttpPost]
        public virtual ActionResult GetInstructorAssignmentsPortalReportInfo(int providerId, int seasonId, bool printMode = false)
        {
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            var seasonBusiness = Ioc.SeasonBusiness;
            var seasonDomain = seasonBusiness.Get(seasonId).Domain;

            var season = seasonBusiness.Get(seasonDomain, providerId);

            var instructors = season.Programs.Where(c => c.Status != ProgramStatus.Deleted).SelectMany(i => i.Instructors).Distinct().ToList();
            var outSourcePrograms = season.OutSourcePrograms;

            instructors.AddRange(outSourcePrograms.Where(c => c.Status != ProgramStatus.Deleted).SelectMany(p => p.Instructors).Distinct().ToList());

            var data = new List<InstructorAssignmentsPortalReportViewModel>();
            foreach (var instructor in instructors.Distinct())
            {
                var programs = instructor.Programs;
                var classAssignemnts = new List<string>();

                foreach (var program in programs)
                {
                    if (program.Status != ProgramStatus.Deleted && (program.Season.Id == season.Id || program.OutSourceSeasonId == season.Id))
                    {
                        if (program.OutSourceSeason != null)
                        {
                            classAssignemnts.Add(program.Name + " (" + program.Club.Name + ")");
                        }
                        else
                        {
                            classAssignemnts.Add(program.Name);
                        }
                    }
                }
                data.Add(new InstructorAssignmentsPortalReportViewModel()
                {
                    Name = instructor.Contact.FullName,
                    Status = instructor.Status.ToString(),
                    DateOfBackgroundCheck = instructor.DateOfBackgroundCheck != null ? string.Format("{0:MMM dd, yyyy}", instructor.DateOfBackgroundCheck) : "-",
                    ClassAssignments = string.Join(", ", classAssignemnts)
                });
            }


            if (printMode)
            {
                dynamic model = new System.Dynamic.ExpandoObject();

                model.Date = _clubBusiness.GetClubDateTime(providerId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
                model.Data = data;
                var clubName = _clubBusiness.Get(providerId).Name;
                model.ClubName = clubName;
                model.SeasonName = season.Title;
                model.ProviderName = season.Club.Name;

                return Pdf(clubName + " (InstructorAssignments)", "_InstructorAssignmentsPortalReportPdfExportView", model);
            }

            var dataSource = data.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            return Json(new { DataSource = dataSource, TotalCount = data.ToList().Count });

        }

        [HttpGet]
        public virtual JsonNetResult GetFullSeasonClassListReportHeader()
        {
            var club = ActiveClub;
            var years = new List<int>();

            for (int i = DateTime.Now.AddYears(1).Year; i >= 2015; i--)
            {
                years.Add(i);
            }

            var selectYears = years.Select(y =>
            new SelectListItem
            {
                Text = y.ToString(),
                Value = y.ToString()
            })
            .ToList();


            var model = new
            {
                ClubName = ActiveClub.Name,
                Date = _clubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt"),//Sep 21, 2016 HH:MM                
                Years = selectYears,
                Seasons = DropdownHelpers.ToSelectList<SeasonNames>()
            };

            model.Seasons.RemoveAt(0);
            model.Seasons.Insert(0, new SelectKeyValue<string>() { Text = "All", Value = "-1" });
            model.Years.Insert(0, new SelectListItem() { Text = "All", Value = "-1" });

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult GetFullSeasonClassListReportInfo(string seasonName, string year, bool printMode = false)
        {
            var clubId = ActiveClub.Id;
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            var season = seasonName + year;
            season = season.Replace(" ", string.Empty);

            var schools =
              _clubBusiness.GetRelatedClubs(clubId)
                  .Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.School)
                  .OrderBy(s => s.Name);

            var seasons = schools.SelectMany(s => s.Seasons).Where(s => s.Title.ToLower().Replace(" ", string.Empty).Contains(season.ToLower()));
            var programQuery = seasons.SelectMany(s => s.Programs.Where(p => p.Status != ProgramStatus.Deleted && p.TypeCategory == ProgramTypeCategory.Class && p.LastCreatedPage >= ProgramPageStep.Step4));
            var programs = new List<Program>();
            if (printMode)
            {
                programs = programQuery.ToList();
            }
            else
            {
                programs = programQuery.OrderBy(p => p.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            var programBusiness = Ioc.ProgramBusiness;

            var data = new List<FullSeasonClassListReportViewModel>();
            foreach (var program in programs)
            {
                var classStatus = new List<string>();
                var attributes = program.ProgramSchedules.Select(p => ((ScheduleAttribute)p.Attributes).Days).ToList();

                foreach (var attribute in attributes)
                {
                    foreach (var day in attribute)
                    {
                        classStatus.Add(day.DayOfWeek.ToString());
                    }
                }

                data.Add(new FullSeasonClassListReportViewModel()
                {
                    SchoolName = !string.IsNullOrEmpty(program.Club.Name) ? program.Club.Name : "-",
                    ClassName = !string.IsNullOrEmpty(program.Name) ? program.Name : "-",
                    ProviderName = program.OutSourceSeason != null ? program.OutSourceSeason.Club.Name : "-",
                    Day = string.Join(", ", classStatus),
                    ClassStatus = programBusiness.GetEmProgramStatus(program, program.Season.Status == SeasonStatus.Test)
                });
            }


            if (printMode)
            {
                dynamic model = new System.Dynamic.ExpandoObject();
                model.Date = _clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
                model.Data = data;
                var ClubName = ActiveClub.Name;
                model.ClubName = ClubName;
                model.SeasonName = string.Format("{0} {1}", seasonName, year);

                return Pdf(ClubName + " (FullSeasonClassList)", "_FullSeasonClassListReportPdfExportView", model);
            }

            var dataSource = data;
            return JsonNet(new { DataSource = dataSource, TotalCount = programQuery.Count() });

        }

        public virtual ActionResult FollowupReportRun(FollowupReportPortal model, string term, bool isExportToExcel = false, bool makeCampaign = false)
        {
            var club = _clubBusiness.Get(ActiveClub.Id);
            var pageSize = (Request.Params["PageSize"] != null) ? int.Parse(Request.Params["PageSize"]) : 100000;
            var page = (Request.Params["Page"] != null) ? int.Parse(Request.Params["Page"]) : 1;

            if (model.IsAllPrograms)
            {
                var clubs = new List<Club>();
                if (model.ClubType == "school")
                {
                    if (model.IsAllClubs == "true")
                    {
                        var partnerId = ActiveClub.Id;
                        clubs = _clubBusiness.GetAllPartnerSchools(partnerId).ToList();
                    }
                    else
                    {
                        var schoolIds = model._SelectedClubs.ToList();
                        clubs = _clubBusiness.GetList().Where(c => schoolIds.Contains(c.Id)).ToList();
                    }
                }
                else if (model.ClubType == "provider")
                {
                    if (model.IsAllClubs == "true")
                    {
                        var partnerId = ActiveClub.Id;
                        clubs = _clubBusiness.GetAllPartnerProviders(partnerId).ToList();
                    }
                    else
                    {
                        var providerIds = model._SelectedClubs.ToList();
                        clubs = _clubBusiness.GetList().Where(c => providerIds.Contains(c.Id)).ToList();
                    }
                }
                else if (model.ClubType == "district")
                {
                    if (model.IsAllClubs == "true")
                    {
                        var partnerId = ActiveClub.Id;
                        var districtIds = _clubBusiness.GetList()
                            .Where(c => c.ClubType.EnumType == ClubTypesEnum.SchoolDistrict &&
                                        c.PartnerId == partnerId && !c.IsDeleted)
                            .Select(c => c.Id)
                            .ToList();

                        clubs = _clubBusiness.GetList().Where(c => districtIds.Contains(c.DistrictId.Value))
                            .ToList();

                    }
                    else
                    {
                        var districtIds = model._SelectedClubs.ToList();
                        clubs = _clubBusiness.GetList().Where(c => districtIds.Contains(c.DistrictId.Value))
                            .ToList();
                    }
                }

                var clubIds = clubs.Select(c => c.Id);
                var programs = Ioc.ProgramBusiness.GetList().Where(p =>
                    clubIds.Contains(p.ClubId) && p.Status != ProgramStatus.Deleted &&
                    p.LastCreatedPage >= ProgramPageStep.Step4 && p.ProgramSchedules.Any());

                if (model.SeasonName != SeasonNames.SelectSeason)
                {
                    programs = programs.Where(p => p.Season.Name == model.SeasonName);
                }

                if (model.Year != -1)
                {
                    programs = programs.Where(p => p.Season.Year == model.Year);
                }

                model._SelectedPrograms = programs.Select(p => p.Id).ToList();

            }

            var allOrderItems = ApplyAllFiltersFollowUpReport(model, ReportType.FollowupForm, term);

            var allData = allOrderItems;

            var data =
                FillFollowupViewModel(allData.OrderBy(o => o.Order.Club.Name).Skip((page - 1) * pageSize).Take(pageSize).ToList());

            if (!isExportToExcel)
            {
                var dataSource = data;
                return JsonNet(new { DataSource = dataSource, TotalCount = allData.Count() });
            }
            else
            {
                var workbook = new HSSFWorkbook();

                var sheet = workbook.CreateSheet();

                //(Optional) set the width of the columns
                sheet.AutoSizeColumn(0);
                sheet.AutoSizeColumn(1);
                sheet.AutoSizeColumn(2);
                sheet.AutoSizeColumn(3);
                sheet.AutoSizeColumn(4);
                sheet.AutoSizeColumn(5);

                //Create a header row
                var headerRow = sheet.CreateRow(0);

                //Set the column names in the header row
                headerRow.CreateCell(0).SetCellValue("Follow-up forms");
                headerRow.CreateCell(1).SetCellValue("Program name");
                headerRow.CreateCell(2).SetCellValue("Full name");
                headerRow.CreateCell(3).SetCellValue("Email");
                headerRow.CreateCell(4).SetCellValue("Regsitration date");
                headerRow.CreateCell(5).SetCellValue("Confirmation");

                //(Optional) freeze the header row so it is not scrolled
                sheet.CreateFreezePane(0, 1, 0, 1);

                int rowNumber = 1;

                //Populate the sheet with values from the grid data
                foreach (var item in data)
                {
                    //Create a new row
                    var row = sheet.CreateRow(rowNumber++);

                    var formDetails = string.Empty;

                    foreach (var form in item.FollowupForms)
                    {
                        var name = form.FormName;
                        var status = form.Status;
                        formDetails += string.Format("{0} {1}, ", name, status);
                    }

                    formDetails = (formDetails.Length > 0)
                        ? formDetails.Remove(formDetails.Length - 2)
                        : formDetails;

                    row.CreateCell(0).SetCellValue(formDetails);

                    //Set values for the cells
                    row.CreateCell(1).SetCellValue(item.ProgramName);
                    row.CreateCell(2).SetCellValue(item.FullName);
                    row.CreateCell(3).SetCellValue(item.Email);
                    row.CreateCell(4).SetCellValue(item.RegDate);
                    row.CreateCell(5).SetCellValue(item.ConfirmationId);
                }

                //Write the workbook to a memory stream
                var output = new MemoryStream();
                workbook.Write(output);

                var reportName = string.Format(Constants.ReportName, "follow-upForm", model.SeasonDomain);

                var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
                string contentType = string.Format("application/vnd.ms-excel");

                // this is a trick using stream you have to return position to 0.
                output.Position = 0;
                sm.UploadBlob(club.Domain, reportName, output, contentType);

                string url = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, reportName).AbsoluteUri;

                output.Flush();
                output.Close();

                return JsonNet(new JResult { Data = url, Status = true });
            }
        }

        private static IQueryable<OrderItem> ApplyAllFiltersFollowUpReport(FollowupReportPortal model, ReportType type, string term, string automaticType = "All")
        {
            var filterable = (model.Filters != null) &&
                             (model.Filters.RegistrationStart.HasValue || model.Filters.RegistrationEnd.HasValue ||
                              model.Filters.Status.HasValue || model.Filters.Tuition.HasValue);
            // Add AsNoTracking
            var allOrderItems =
                Ioc.OrderItemBusiness.GetReportOrderItems(model._SelectedPrograms, OrderItemStatusCategories.changed)
                    .AsNoTracking();

            switch (type)
            {
                case ReportType.Installment:
                    {
                        allOrderItems =
                            allOrderItems.Where(o => o.PaymentPlanType == PaymentPlanType.Installment).Select(o => o);
                        break;
                    }
                case ReportType.FollowupForm:
                    {
                        allOrderItems = allOrderItems.Where(p => p.FollowupForms.Any()).Select(o => o);

                        if (model.Filters != null)
                            switch (model.Filters.FollowupFilter)
                            {
                                case FollowupFormFilter.Completed:
                                    {
                                        allOrderItems =
                                            allOrderItems.Where(
                                                o =>
                                                    o.FollowupForms.Count > 0 &&
                                                    o.FollowupForms.All(f => f.Status == FollowupStatus.Completed))
                                                .Select(p => p);
                                        break;
                                    }
                                case FollowupFormFilter.Uncompleted:
                                    {
                                        allOrderItems =
                                            allOrderItems.Where(
                                                o =>
                                                    o.FollowupForms.Count > 0 &&
                                                    o.FollowupForms.Any(f => f.Status == FollowupStatus.NotFill))
                                                .Select(p => p);
                                        break;
                                    }
                            }

                        break;
                    }
                case ReportType.Balance:
                    {
                        allOrderItems = allOrderItems.Where(o => o.TotalAmount > o.PaidAmount).Select(o => o);

                        allOrderItems = model.Filters != null && (model.Filters.BalanceFilter == BalanceReportFilters.DueDate)
                            ? allOrderItems.Where(p => p.Order.PaymentPlanType == PaymentPlanType.Installment)
                            : allOrderItems;
                        break;
                    }
                case ReportType.ChargeDiscount:
                    {
                        break;
                    }
            }

            if (!string.IsNullOrEmpty(term))
            {
                allOrderItems = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Match(term).Success
                    ? allOrderItems.Where(p => p.Order.User.UserName.ToLower().Equals(term.ToLower()))
                    : allOrderItems.Where(
                        p => p.FirstName.ToLower().StartsWith(term.ToLower()) || p.LastName.ToLower().StartsWith(term.ToLower()));
            }

            if (automaticType == "AutoCharge")
            {
                allOrderItems = allOrderItems.Where(i => i.Order.IsAutoCharge);
            }
            if (automaticType == "NoAutoCharge")
            {
                allOrderItems = allOrderItems.Where(i => !i.Order.IsAutoCharge);
            }


            var allData = (filterable)
                ? ApplyBaseFilters(model.Filters, allOrderItems)
                : allOrderItems.Where(o => o.ItemStatus == OrderItemStatusCategories.completed);

            return allData;
        }

        private IEnumerable<FollowupReportViewModel> FillFollowupViewModel(IEnumerable<OrderItem> orderItems)
        {
            var dataSource = new List<FollowupReportViewModel>();

            var programOrderItems = orderItems.ToList();

            dataSource.AddRange(programOrderItems.Select(orderItem => new FollowupReportViewModel
            {
                ProgramName =
                    (orderItem.ProgramTypeCategory == ProgramTypeCategory.Camp)
                        ? _programBusiness.GetProgramName(orderItem.ProgramSchedule,
                            (orderItem.OrderChargeDiscounts.FirstOrDefault(
                                c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee) != null)
                                ? orderItem.OrderChargeDiscounts.First(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee)
                                    .Name
                                : string.Empty)
                        : _programBusiness.GetProgramName(orderItem.ProgramSchedule.Program.Name,
                            (orderItem.OrderChargeDiscounts.FirstOrDefault(
                                c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee) != null)
                                ? orderItem.OrderChargeDiscounts.First(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee)
                                    .Name
                                : string.Empty),
                FullName = orderItem.FullName,
                FollowupForms = orderItem.FollowupForms.Select(f => new UserFollowupForm
                {
                    FormName = f.FormName,
                    Status = (f.Status == FollowupStatus.Completed) ? "Filled" : "-",
                    FormId = f.Id

                }).ToList(),
                Email = orderItem.Order.User.UserName,
                ConfirmationId = orderItem.Order.ConfirmationId,
                OrderId = orderItem.Order_Id,
                OrderItemId = orderItem.Id,
                RegDate = DateTimeHelper.ConvertUtcDateTimeToLocal(orderItem.Order.CompleteDate, ActiveClub.TimeZone).ToString(Constants.DateTime_Comma),
                ItemStatusReason = (int)orderItem.ItemStatusReason,
                ClubName = orderItem.Order.Club.Name
            }));

            return dataSource;
        }

        [HttpPost]
        public virtual ActionResult GetInstructorCheckinPortalReportInfo(int providerId, int seasonId, bool printMode = false)
        {
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            var programBusiness = Ioc.ProgramBusiness;
            var seasonBusiness = Ioc.SeasonBusiness;
            var seasonDomain = seasonBusiness.Get(seasonId).Domain;

            var season = seasonBusiness.Get(seasonDomain, providerId);

            var programs = season.Programs.Where(c => c.Status != ProgramStatus.Deleted).Distinct().ToList();
            programs.AddRange(season.OutSourcePrograms);

            var programsId = programs.Distinct().Select(p => p.Id).ToList();
            var checkins = Ioc.UserEventBusiness.GetList(programsId).Where(p => p.Program.Status != ProgramStatus.Deleted);

            var data = new List<InstructorCheckinPortalReportViewModel>();
            if (checkins.ToList().Any() && checkins.ToList() != null)
            {
                foreach (var checkin in checkins.ToList())
                {
                    var today = checkin.ClubDateTime.Date;
                    var tomorrow = checkin.ClubDateTime.AddDays(1).Date;
                    var session = checkin.Program.ProgramSchedules.First(p => !p.IsDeleted).Sessions.First(s => (s.Start.Date >= today && s.Start.Date < tomorrow));

                    data.Add(new InstructorCheckinPortalReportViewModel()
                    {
                        SchoolName = checkin.Program.OutSourceSeason != null ? checkin.Program.Club.Name : "-",
                        ProgramName = checkin.Program.Name,
                        SessionDate = session != null ? session.Start.ToString(Constants.DateTime_Comma) : "-",
                        CheckinTime = checkin.ClubDateTime != null ? checkin.ClubDateTime.ToString("h:mm tt") : "-",
                        UserName = GetInstructorNameCheckin(checkin),
                        Address = checkin.Address != null && !string.IsNullOrWhiteSpace(checkin.Address.Address) ? (checkin.Address.Address.Contains("Street,") == true ?
                              checkin.Address.Address.Insert(checkin.Address.Address.IndexOf("Street,") + 7, "\n") : checkin.Address.Address) : "-",
                        GPS = checkin.Address.Lat != 0 && checkin.Address.Lng != 0 ? string.Format("{0}, {1}", checkin.Address.Lat, checkin.Address.Lng) : "-",
                        Distance = checkin.Program.ClubLocation.PostalAddress != null && checkin.Address != null ?
                               ((int)GoogleMap.CalculateDistanceBetweenTwoPoint(checkin.Program.ClubLocation.PostalAddress.Lat, checkin.Program.ClubLocation.PostalAddress.Lng, checkin.Address.Lat, checkin.Address.Lng)).ToString() : "-",

                        CheckinDelay = session != null && checkin.ClubDateTime.TimeOfDay != null ?
                                  (((int)(checkin.ClubDateTime.TimeOfDay - session.Start.TimeOfDay).TotalMinutes)).ToString() : ""
                    });
                }

            }



            if (printMode)
            {
                dynamic model = new System.Dynamic.ExpandoObject();

                model.Date = _clubBusiness.GetClubDateTime(providerId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
                model.Data = data;
                var clubName = ActiveClub.Name;
                model.ClubName = clubName;
                model.SeasonName = season.Title;
                model.ProviderName = season.Club.Name;

                return Pdf(clubName + " (InstructorCheck-in)", "_InstructorCheckinPortalReportPdfExportView", model);
            }

            var dataSource = data.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            return Json(new { DataSource = dataSource, TotalCount = data.ToList().Count });

        }

        [HttpPost]
        public virtual ActionResult GetInvoiceListReportInfo(PaginationModel paginationModel, SelectedSchoolsViewModel model, InvoiceType invoiceType = InvoiceType.All)
        {
            var partnerId = ActiveClub.Id;
            var allInvoices = new List<Invoice>();

            if (model.IsAllSchools)
            {
                allInvoices.AddRange(Ioc.InvoiceBusiness.GetList().Where(i => i.Club.PartnerId == partnerId));
            }
            else
            {
                foreach (var item in model._SelectedSchools)
                {
                    var clubInvoices = Ioc.InvoiceBusiness.GetInvoiceByClubId(item);

                    allInvoices.AddRange(clubInvoices);
                }
            }

            //Filter invoices

            switch (invoiceType)
            {
                case InvoiceType.All:
                    break;
                case InvoiceType.Paid:
                    {
                        allInvoices = allInvoices.Where(i => i.Status == InvoiceStatus.Paid).ToList();
                    }
                    break;
                case InvoiceType.Unpaid:
                    {
                        allInvoices = allInvoices.Where(i => i.Status == InvoiceStatus.Unpaid).ToList();
                    }
                    break;
                case InvoiceType.Canceled:
                    {
                        allInvoices = allInvoices.Where(i => i.Status == InvoiceStatus.Canceled).ToList();
                    }
                    break;
                case InvoiceType.PastDue:
                    {
                        var pastDueInvoices = new List<Invoice>();

                        var today = DateTime.UtcNow;
                        var clubTime = _clubBusiness.GetClubDateTime(ActiveClub.Id, today);

                        foreach (var invoice in allInvoices.Where(i => i.PaidAmount == 0 && i.Status != InvoiceStatus.Canceled))
                        {
                            var date = _clubBusiness.GetClubDateTime(this.GetCurrentClubId(), invoice.InvoicingDate);
                            var dueValue = invoice.DueDate;
                            var dueDate = date.AddDays(dueValue.Value);

                            if (clubTime > dueDate)
                            {
                                pastDueInvoices.Add(invoice);
                            }
                        }

                        allInvoices = pastDueInvoices;
                    }
                    break;
                default:
                    break;
            }

            var counts = allInvoices.Count();
            var pagedResult = allInvoices.OrderBy(c => c.Id).Skip((paginationModel.Page - 1) * paginationModel.PageSize).Take(paginationModel.PageSize);

            var invoiceModel = pagedResult.Select(c => new InvoicesListReportViewModel()
            {
                Date = _clubBusiness.GetClubDateTime(this.GetCurrentClubId(), c.InvoicingDate),
                InvoiceNumber = c.InvoiceNumber,
                Amount = c.Amount,
                Status = c.Status.ToString(),
                Recipient = c.User.UserName.ToLower(),
                DueDateValue = c.DueDate,
                Id = c.Id,
                ClubId = c.ClubId,
                ClubName = c.Club.Name,
                StrAmount = CurrencyHelper.FormatCurrencyWithPenny(c.Amount, c.Club.Currency),
            });

            var datasource = invoiceModel.ToList();
            return JsonNet(new { DataSource = datasource, TotalCount = counts }, new Newtonsoft.Json.JsonSerializerSettings(), "");
        }

        [HttpGet]
        public virtual ActionResult GetInformationForReminder(List<int> invoiceIds)
        {
            var model = new ReminderInvoicesViewModel();

            model.InvoiceIds = invoiceIds;
            model.ClubName = ActiveClub.Name;
            model.Date = _clubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");

            return JsonNet(model);
        }
        [HttpPost]
        public virtual ActionResult SendReminderInvoices(ReminderInvoicesViewModel model)
        {
            OperationStatus result = new OperationStatus() { Status = false };

            var today = DateTime.UtcNow;
            var username = this.GetCurrentUserName();
            var noteToRecipent = model.NoteToRecipients;

            foreach (var item in model.InvoiceIds)
            {
                var invoice = Ioc.InvoiceBusiness.Get(item);

                if (invoice != null)
                {
                    var invoiceHistory = new InvoiceHistory() { InvoiceId = invoice.Id, ActionDate = today, Action = Invoicestatus.Initiated, Description = string.Format(Constants.Invoice_Reminder, username) };
                    invoice.InvoiceHistories.Add(invoiceHistory);
                    Ioc.InvoiceBusiness.Update(invoice);
                    EmailService.Get(this.ControllerContext).SendCancelationAndReminderInvoice(invoice, false, true, noteToRecipent, null);
                }
                else
                {
                    return Json(new { Status = false, Message = result.ExceptionMessage });
                }
            }

            return Json(new { Status = true, Message = result.ExceptionMessage });

        }
        private string GetInstructorNameCheckin(UserEvent checkin)
        {
            return checkin.Program.OutSourceSeason != null ? (checkin.Program.OutSourceSeason.Club.ClubStaffs.SingleOrDefault(s => s.JbUserRole.UserId == checkin.UserId) != null ?
                   checkin.Program.OutSourceSeason.Club.ClubStaffs.SingleOrDefault(s => s.JbUserRole.UserId == checkin.UserId).Contact.FullName : "-")
                   : (checkin.Program.Club.ClubStaffs.SingleOrDefault(s => s.JbUserRole.UserId == checkin.UserId) != null ?
                   checkin.Program.Club.ClubStaffs.SingleOrDefault(s => s.JbUserRole.UserId == checkin.UserId).Contact.FullName : "-");
        }

        [HttpPost]
        public virtual ActionResult Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        private IQueryable<OrderItem> GetClubOrderItems(DateTime? start, DateTime? end, int? clubId = null)
        {
            DateTime startDate;
            DateTime endDate;
            if (!start.HasValue && !end.HasValue)
            {
                startDate = new DateTime(DateTime.Now.Year, 1, 1);
                endDate = DateTime.Now;
            }
            else if (start.HasValue && !end.HasValue)
            {
                startDate = start.Value;
                endDate = DateTime.Now;
            }
            else if (!start.HasValue && end.HasValue)
            {
                startDate = new DateTime(DateTime.Now.Year, 1, 1);
                endDate = end.Value;
            }
            else
            {
                startDate = start.Value;
                endDate = end.Value;
            }

            clubId = clubId.HasValue? clubId.Value : ActiveClub.Id;

            var clubOrderItems =
                Ioc.OrderItemBusiness.GetClubOrderItems(clubId.Value)
                    .Where(
                        oi =>
                            DbFunctions.TruncateTime(oi.Order.CompleteDate) >= startDate &&
                            DbFunctions.TruncateTime(oi.Order.CompleteDate) <= endDate);

            return clubOrderItems;
        }

        private List<PortalreportDiscountModel> GetDiscountModel(IEnumerable<OrderChargeDiscount> discounts)
        {
            var discountList = new List<PortalreportDiscountModel>();

            foreach (var discount in discounts)
            {
                switch (discount.Category)
                {
                    case ChargeDiscountCategory.MultiPerson:
                    case ChargeDiscountCategory.MultiProgram:
                    case ChargeDiscountCategory.MultiSchedule:
                        {
                            discountList.Add(new PortalreportDiscountModel
                            {
                                Name = discount.Discount.Name,
                                Fee = Math.Abs(discount.Amount),
                                Type = "Discount"
                            });
                            break;

                        }
                    case ChargeDiscountCategory.Coupon:
                        {
                            discountList.Add(new PortalreportDiscountModel
                            {
                                Name = discount.Coupon.Name,
                                Fee = Math.Abs(discount.Amount),
                                Type = "Coupon"
                            });
                            break;
                        }
                    case ChargeDiscountCategory.CustomDiscount:
                        {
                            discountList.Add(new PortalreportDiscountModel
                            {
                                Name = "Custom discount",
                                Fee = Math.Abs(discount.Amount),
                                Type = "Discount"
                            });
                            break;
                        }
                }
            }

            return discountList;
        }

        private IEnumerable<PortalreportDiscountModel> GetScholarshipModel(IEnumerable<TransactionActivity> schoolarshipPayments)
        {
            return schoolarshipPayments.Select(payment => new PortalreportDiscountModel
            {
                Name = payment.PaymentDetail.PaymentMethod.ToDescription(),
                Fee = payment.Amount,
                Type = payment.PaymentDetail.PaymentMethod.ToDescription()
            }).ToList();
        }

        private object GetSeasonProcessingModel(Season season)
        {
            var seasonSetting = ((SeasonSchoolSetting)season.Setting);
            if (seasonSetting != null && seasonSetting.SchoolSeasonDetailSetting != null)
            {
                var name = string.Format("{0} {1}", season.Club.Name, season.Title);
                var startDate = season.Setting.SeasonProgramInfoSetting.GeneralRegistrationOpens.HasValue ? season.Setting.SeasonProgramInfoSetting.GeneralRegistrationOpens.Value.ToString(Constants.DateTime_Comma) : "";
                var endDate = season.Setting.SeasonProgramInfoSetting.GeneralRegistrationCloses.HasValue ? season.Setting.SeasonProgramInfoSetting.GeneralRegistrationCloses.Value.ToString(Constants.DateTime_Comma) : "";
                var rosterDueDaate = ((SeasonSchoolSetting)season.Setting).SchoolSeasonDetailSetting.RostersDue.HasValue ? ((SeasonSchoolSetting)season.Setting).SchoolSeasonDetailSetting.RostersDue.Value.ToString(Constants.DateTime_Comma) : "";
                var paymentIssue = ((SeasonSchoolSetting)season.Setting).SchoolSeasonDetailSetting.PaymentsIssued.HasValue ? ((SeasonSchoolSetting)season.Setting).SchoolSeasonDetailSetting.PaymentsIssued.Value.ToString(Constants.DateTime_Comma) : "";
                var regStartTime = "";
                var regEndTime = "";
                if (((SchoolSetting)season.Club.Setting) != null && ((SchoolSetting)season.Club.Setting).RegistrationInfo != null)
                {
                    regStartTime = ((SchoolSetting)season.Club.Setting).RegistrationInfo.RegistrationStartTime.HasValue ? ((SchoolSetting)season.Club.Setting).RegistrationInfo.RegistrationStartTime.Value.ToString("t") : "";
                    regEndTime = ((SchoolSetting)season.Club.Setting).RegistrationInfo.RegistrationEndTime.HasValue ? ((SchoolSetting)season.Club.Setting).RegistrationInfo.RegistrationEndTime.Value.ToString("t") : "";
                }
                var newObj = new
                {
                    Name = name,
                    Start = string.Format("{0} {1}", startDate, regStartTime),
                    End = string.Format("{0} {1}", endDate, regEndTime),
                    Roster = rosterDueDaate,
                    Payment = paymentIssue
                };

                return newObj;
            }

            return new
            {
                Name = string.Format("{0} {1}", season.Club.Name, season.Title),
                Start = "",
                End = "",
                Roster = "",
                Payment = ""
            };
        }

        [HttpGet]
        public virtual ActionResult GetGradeInfo()
        {
            var model = new GradeViewModel();

            model.Grades = DropdownHelpers.ToOrderedSelectList<SchoolGradeType>("Select grade", null);

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult GetAllParticipantByGrade(PaginationModel paginationModel, SchoolGradeType grade)
        {
            var club = ActiveClub;

            var orders = Ioc.OrderBusiness.GetAllOrders().Where(o => o.ClubId == ActiveClub.Id && o.OrderStatus == OrderStatusCategories.completed && o.IsLive);

            var playerOrders = orders.SelectMany(o => o.OrderItems)
            .Where(i => i.ItemStatus == OrderItemStatusCategories.completed || (i.ItemStatus == OrderItemStatusCategories.changed && i.ItemStatusReason == OrderItemStatusReasons.canceled)).GroupBy(o => o.PlayerId).Select(i => i.OrderByDescending(o => o.Id).FirstOrDefault()).ToList();

            var orderItems = AllOrderItemsInSpecialGrade(playerOrders, grade);

            var model = orderItems.OrderByDescending(c => c.Id).Skip((paginationModel.Page - 1) * paginationModel.PageSize).Take(paginationModel.PageSize).ToList().Select(c => new GraduateViewModel()
            {
                ParticipantName = c.FullName,
                UserName = c.Order.User.UserName,
                LastOrderDate = c.Order.CompleteDate,
                PlayerId = c.PlayerId.Value

            });

            var datasource = model.OrderBy(n => n.ParticipantName).ToList();

            return JsonNet(new { DataSource = datasource, TotalCount = datasource.Count() });
        }

        public List<OrderItem> AllOrderItemsInSpecialGrade(List<OrderItem> lastPlayerOrders, SchoolGradeType grade)
        {
            var orderItems = new List<OrderItem>();

            foreach (var lastOrderItem in lastPlayerOrders)
            {
                var formGrade = lastOrderItem.JbForm != null && lastOrderItem.JbForm.Elements.Any(s => s.Name == SectionsName.SchoolSection.ToString()) ?
                lastOrderItem.JbForm.GetElement<JbSection>(SectionsName.SchoolSection.ToString()).Elements.Any(e => e.Name == "Grade") ?
                lastOrderItem.JbForm.GetElement<JbSection>(SectionsName.SchoolSection.ToString()).Elements.SingleOrDefault(e => e.Name == "Grade").GetValue() : null : null;

                if (formGrade != null && Ioc.PlayerProfileBusiness.GetGradeValueFromDescription(formGrade).Equals(grade))
                {
                    orderItems.Add(lastOrderItem);
                }

            }

            return orderItems;
        }
        [HttpPost]
        public virtual ActionResult SaveGraduateds(int[] playerIds)
        {
            var result = new OperationStatus();
            try
            {
                if (playerIds != null)
                {
                    var playerProfiles = Ioc.PlayerProfileBusiness.GetList().Where(p => playerIds.Contains(p.Id)).ToList();

                    result = Ioc.PlayerProfileBusiness.UpdateGraduatedProfiles(playerProfiles);
                }
                return Json(new JResult { Status = result.Status, Message = result.Message });
            }
            catch (Exception)
            {
                return Json(new JResult { Status = result.Status, Message = result.Message });
            }

        }

        [HttpPost]
        public virtual ActionResult GetOnsitePersonCheckinReportInfo(DateTime? startDate, DateTime? endDate, List<int> userIds, int eventType, bool printMode = false)
        {
            var clubId = ActiveClub.Id;
            var clubBusiness = _clubBusiness;

            var userEvents = Ioc.UserEventBusiness.GetList(u => userIds.Contains(u.UserId) && u.ClubId == clubId && !u.ProgramId.HasValue && !u.SessionId.HasValue);

            if (startDate.HasValue)
            {
                userEvents = userEvents.Where(u => u.ClubDateTime >= startDate);
            }

            if (endDate.HasValue)
            {
                var date = endDate;
                date = date.Value.AddHours(24 - date.Value.Hour);
                userEvents = userEvents.Where(u => u.ClubDateTime < date);
            }

            if (eventType == 1)
            {
                userEvents = userEvents.Where(u => u.Type == UserEventType.Checkin);
            }

            if (eventType == 2)
            {
                userEvents = userEvents.Where(u => u.Type == UserEventType.Checkout);
            }

            var data = userEvents.ToList()
                .Select(u => new
                {
                    FullName = clubBusiness.Get(u.ClubId.Value).ClubStaffs.SingleOrDefault(c => c.JbUserRole.UserId == u.UserId).Contact.FullName,
                    Date = u.ClubDateTime.ToString("MMM dd, yyyy hh:mm tt"),
                    Address = u.Address != null && !string.IsNullOrWhiteSpace(u.Address.Address) ? (u.Address.Address.Contains("Street,") == true ?
                                    u.Address.Address.Insert(u.Address.Address.IndexOf("Street,") + 7, "\n") : u.Address.Address) : "-",
                    Type = u.Type
                })
                .ToList();

            if (printMode)
            {
                dynamic model = new System.Dynamic.ExpandoObject();
                model.Date = _clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
                model.Data = data;
                var ClubName = ActiveClub.Name;
                model.ClubName = ClubName;

                return Pdf(ClubName + " (MemberList)", "_MemberListReportPdfExportView", model);
            }

            return JsonNet(new { DataSource = data, TotalCount = data.ToList().Count });

        }

        public JsonNetResult GetOnsitePersonClub(int roleType)
        {
            var model = new List<JbTitleValue>();
            var clubId = this.GetCurrentClubId();

            var staffs = _clubBusiness.Get(clubId).ClubStaffs.Where(c => !c.IsDeleted && c.Status == StaffStatus.Active);

            if (roleType == -1)
            {
                model = staffs.Where(c => c.JbUserRole.Role.Name == RoleCategory.OnsiteCoordinator.ToString() ||
                c.JbUserRole.Role.Name == RoleCategory.OnsiteSupportManager.ToString())
                .Select(c => new JbTitleValue()
                {
                    Title = c.Contact.FullName,
                    Value = c.JbUserRole.UserId
                }).ToList();
            }
            else if (roleType == 1)
            {
                model = staffs.Where(c => c.JbUserRole.Role.Name == RoleCategory.OnsiteCoordinator.ToString())
                .Select(c => new JbTitleValue()
                {
                    Title = c.Contact.FullName,
                    Value = c.JbUserRole.UserId
                }).ToList();
            }
            else if (roleType == 2)
            {
                model = staffs.Where(c => c.JbUserRole.Role.Name == RoleCategory.OnsiteSupportManager.ToString())
                .Select(c => new JbTitleValue()
                {
                    Title = c.Contact.FullName,
                    Value = c.JbUserRole.UserId
                }).ToList();
            }

            return JsonNet(new { Result = model });
        }

        [HttpGet]
        [JbAuthorize(JbAction.Season_View)]
        public virtual JsonNetResult GetEnrollmentInfo()
        {
            var model = new SeasonViewModel();

            return JsonNet(model);
        }

        public HSSFWorkbook CreateExcelEnrollmentReport(SeasonNames name, int year)
        {
            var workbook = new HSSFWorkbook();
            var firstSheet = workbook.CreateSheet("Enrollment Summary Report");

            //Create font bold
            IFont boldFont = workbook.CreateFont();
            boldFont.Boldweight = (short)FontBoldWeight.Bold;

            ICellStyle boldStyle = workbook.CreateCellStyle();
            boldStyle.SetFont(boldFont);

            //Create border
            ICellStyle borderTopStyle = workbook.CreateCellStyle();
            borderTopStyle.BorderTop = BorderStyle.Thin;
            borderTopStyle.TopBorderColor = 256;

            //Create Alignment
            ICellStyle alignmentCenterStyle = workbook.CreateCellStyle();
            alignmentCenterStyle.Alignment = HorizontalAlignment.Center;
            alignmentCenterStyle.SetFont(boldFont);

            //right Alignment
            ICellStyle alignmentRightStyle = workbook.CreateCellStyle();
            alignmentRightStyle.Alignment = HorizontalAlignment.Right;

            ICellStyle lastRowAlignmentRightStyle = workbook.CreateCellStyle();
            lastRowAlignmentRightStyle.Alignment = HorizontalAlignment.Right;
            lastRowAlignmentRightStyle.BorderTop = BorderStyle.Thin;
            lastRowAlignmentRightStyle.TopBorderColor = 256;

            var club = ActiveClub;

            //Create Query By Fillter
            var schools =
                _clubBusiness.GetRelatedClubs(club.Id)
                    .Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.School).ToList();

            schools = schools.Where(s => s.Seasons.Any(se => (year == 0 || (year != 0 && se.Year == year)) &&
            (name == SeasonNames.SelectSeason || (name != SeasonNames.SelectSeason && se.Name == name)))).OrderBy(n => n.Name).ToList();


            //(Optional) set the width of the Columns

            //Create a header row
            var mergeRow = firstSheet.CreateRow(0);
            mergeRow.CreateCell(0).SetCellValue("Enrollments By Accounts " + name);
            mergeRow.GetCell(0).CellStyle = boldStyle;
            mergeRow.GetCell(0).CellStyle = alignmentCenterStyle;

            var cra = new NPOI.SS.Util.CellRangeAddress(0, 0, 0, 2);
            firstSheet.AddMergedRegion(cra);

            var headerRow = firstSheet.CreateRow(1);
            headerRow.CreateCell(0).SetCellValue("Account Name");
            headerRow.CreateCell(1).SetCellValue("Enrollments");
            headerRow.CreateCell(2).SetCellValue("Net");

            for (int i = 0; i < 3; i++)
            {
                headerRow.GetCell(i).CellStyle = boldStyle;

            }
            //Set Style to header
            firstSheet.SetColumnWidth(0, 40 * 256);
            firstSheet.SetColumnWidth(1, 12 * 256);
            firstSheet.SetColumnWidth(2, 15 * 256);

            //(Optional) freeze the header row so it is not scrolled
            //firstSheet.CreateFreezePane(0, 1, 0, 1);

            int rowNumber = 2;
            List<int> listClubIds = new List<int>();
            listClubIds = schools.Select(s => s.Id).ToList();

            var allOrderItems = Ioc.OrderItemBusiness.GetOrderItems(clubIds: listClubIds, isTestMode: false)
                .Where(o => o.ItemStatus == OrderItemStatusCategories.completed).
                Where(o => (year == 0 || (year != 0 && o.Season.Year == year) &&
                (name == SeasonNames.SelectSeason || (name != SeasonNames.SelectSeason && o.Season.Name == name))));

            var count = 0;
            decimal sumSchoolRevenue = 0;
            CurrencyCodes schoolcurrency = CurrencyCodes.USD;
            foreach (var item in schools)
            {
                schoolcurrency = item.Currency;
                var orderItemsSchool = allOrderItems.Where(o => o.Order.ClubId == item.Id);
                var TotalAttendees = orderItemsSchool.Any() ? orderItemsSchool.Count() : 0;

                var schoolDiscounts = orderItemsSchool.SelectMany(c => c.OrderChargeDiscounts)
                    .Where(c => !c.IsDeleted && (c.Subcategory == ChargeDiscountSubcategory.Discount || c.DiscountId != null)
                    && (c.Category != ChargeDiscountCategory.Coupon)).Sum(c => (decimal?)c.Amount) ?? 0;

                var schoolCoupons = orderItemsSchool.SelectMany(c => c.OrderChargeDiscounts)
                    .Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Coupon).Sum(c => (decimal?)c.Amount) ?? 0;

                var schoolAmount = orderItemsSchool.Where(o => o.ItemStatus == OrderItemStatusCategories.completed)
                    .SelectMany(item1 => item1.OrderChargeDiscounts.Where(o => !o.IsDeleted && o.Subcategory == ChargeDiscountSubcategory.Charge)).ToList().Sum(c => (decimal?)c.Amount) ?? 0;

                var schoolRevenue = schoolAmount - Math.Abs(schoolCoupons) - Math.Abs(schoolDiscounts);
                sumSchoolRevenue = sumSchoolRevenue + schoolRevenue;
                count = rowNumber++;
                //Create a new row
                var rowFirstSheet = firstSheet.CreateRow(count);

                //Set values for the cells
                rowFirstSheet.CreateCell(0).SetCellValue(item.Name);
                rowFirstSheet.CreateCell(1).SetCellValue(TotalAttendees);
                rowFirstSheet.CreateCell(2).SetCellValue(CurrencyHelper.FormatCurrencyWithPenny(schoolRevenue, schoolcurrency));

                rowFirstSheet.GetCell(2).CellStyle = alignmentRightStyle;
            }
            var lastRowFirstSheet = firstSheet.CreateRow(count + 1);
            lastRowFirstSheet.CreateCell(0).SetCellValue("Total");
            lastRowFirstSheet.CreateCell(1).SetCellValue(allOrderItems.Count());
            lastRowFirstSheet.CreateCell(2).SetCellValue(CurrencyHelper.FormatCurrencyWithPenny(sumSchoolRevenue, schoolcurrency));

            lastRowFirstSheet.GetCell(2).CellStyle = lastRowAlignmentRightStyle;

            for (int i = 0; i < 2; i++)
            {
                lastRowFirstSheet.GetCell(i).CellStyle = borderTopStyle;
            }

            ISheet sheet1 = null;
            foreach (var school in schools)
            {
                var currency = school.Currency;
                sheet1 = workbook.CreateSheet(school.Name.Replace('/', '-'));


                //(Optional) set the width of the Columns
                sheet1.AutoSizeColumn(0);
                sheet1.SetColumnWidth(0, 40 * 256);
                sheet1.SetColumnWidth(1, 30 * 256);
                sheet1.SetColumnWidth(2, 13 * 256);
                sheet1.SetColumnWidth(3, 20 * 256);
                sheet1.SetColumnWidth(4, 35 * 256);
                sheet1.SetColumnWidth(5, 13 * 256);
                sheet1.SetColumnWidth(6, 13 * 256);
                sheet1.SetColumnWidth(8, 15 * 256);
                sheet1.SetColumnWidth(9, 10 * 256);
                var headerRowSchools = sheet1.CreateRow(0);
                //Set the column names in the header row
                headerRowSchools.CreateCell(0).SetCellValue("Activity");
                headerRowSchools.CreateCell(1).SetCellValue("Grades/Ages");
                headerRowSchools.CreateCell(2).SetCellValue("Day");
                headerRowSchools.CreateCell(3).SetCellValue("Class time");
                headerRowSchools.CreateCell(4).SetCellValue("Provider");
                headerRowSchools.CreateCell(5).SetCellValue("Minimum");
                headerRowSchools.CreateCell(6).SetCellValue("Maximum");
                headerRowSchools.CreateCell(7).SetCellValue("Actual");
                headerRowSchools.CreateCell(8).SetCellValue("Individual price");
                headerRowSchools.CreateCell(9).SetCellValue("price");
                headerRowSchools.CreateCell(10).SetCellValue("Charge");
                headerRowSchools.CreateCell(11).SetCellValue("Discount");
                headerRowSchools.CreateCell(12).SetCellValue("Coupon");
                headerRowSchools.CreateCell(13).SetCellValue("Net");

                for (int i = 0; i < 14; i++)
                {
                    headerRowSchools.GetCell(i).CellStyle = boldStyle;
                }

                var schoolPrograms = school.Seasons.Where(se => ((year == 0 || (year != 0 && se.Year == year))) &&
                (name == SeasonNames.SelectSeason || (name != SeasonNames.SelectSeason && se.Name == name))).
                SelectMany(s => s.Programs).Where(p => p.Status == ProgramStatus.Open).ToList().Where(p => p.SaveType == SaveType.Publish);

                int programNumber = 1;
                var programBusiness = Ioc.ProgramBusiness;
                var programCount = 0;
                decimal classFee;
                decimal sumclassFee = 0;
                decimal price = 0, sumPrice = 0, sumCharge = 0, sumDiscount = 0, sumCoupon = 0, sumRevenue = 0;
                int sumParticipantCount = 0;

                List<OrderItem> programOrderitems = null;
                foreach (var program in schoolPrograms)
                {
                    var gradeOrAges = !string.IsNullOrEmpty(programBusiness.GenerateGradeInfoLabelForFlyer(program)) ? programBusiness.GenerateGradeInfoLabelForFlyer(program) : !string.IsNullOrEmpty(programBusiness.GenerateAgeInfoLabel(program)) ? programBusiness.GenerateAgeInfoLabel(program) : null;
                    var allDaysProgram = programBusiness.GetClassDays(program).Select(pr => pr.DayOfWeek).ToList();
                    var providerName = program.OutSourceSeasonId.HasValue ? program.OutSourceSeason.Club.Name : "";
                    var minimumCapacity = (program.TypeCategory == ProgramTypeCategory.Class) && program.ProgramSchedules.Any(s => !s.IsDeleted) ? (program.ProgramSchedules.Where(s => !s.IsDeleted).First()).Attributes.MinimumEnrollment : 0;
                    var maximumCapacity = (program.TypeCategory == ProgramTypeCategory.Class) && program.ProgramSchedules.Any(s => !s.IsDeleted) ? (program.ProgramSchedules.Where(s => !s.IsDeleted).First()).Attributes.Capacity : 0;
                    programOrderitems = allOrderItems.Where(o => o.ProgramSchedule.ProgramId == program.Id).ToList();
                    var programDiscounts = programOrderitems.SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && (c.Subcategory == ChargeDiscountSubcategory.Discount || c.DiscountId != null) && (c.Category != ChargeDiscountCategory.Coupon)).Sum(c => (decimal?)c.Amount) ?? 0;
                    var programCoupons = programOrderitems.SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Coupon).Sum(c => (decimal?)c.Amount) ?? 0;
                    var programCharges = programOrderitems.SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Subcategory == ChargeDiscountSubcategory.Charge && c.Category != ChargeDiscountCategory.EntryFee).Sum(c => (decimal?)c.Amount) ?? 0;
                    var classFee2 = programOrderitems.Where(o => o.ItemStatus == OrderItemStatusCategories.completed).SelectMany(item => item.OrderChargeDiscounts.Where(o => !o.IsDeleted && o.Subcategory == ChargeDiscountSubcategory.Charge)).ToList().Sum(c => (decimal?)c.Amount) ?? 0;
                    classFee = program.ProgramSchedules.First().Charges.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).First().Amount;
                    var revenue = classFee2 - Math.Abs(programCoupons) - Math.Abs(programDiscounts);
                    var ParticipantCount = programOrderitems.Count();
                    var programDays = new List<string>();
                    var StrStartTime = programBusiness.GetClassDays(program).Where(o => o.StartTime.HasValue).Min(pr => pr.StartTime) != null ? DateTime.Today.Add(programBusiness.GetClassDays(program).Where(o => o.StartTime.HasValue).Min(pr => pr.StartTime.Value)).ToString("h:mm tt") : null;
                    var StrEndTime = programBusiness.GetClassDays(program).Where(o => o.EndTime.HasValue).Max(pr => pr.EndTime) != null ? DateTime.Today.Add(programBusiness.GetClassDays(program).Where(o => o.EndTime.HasValue).Max(pr => pr.EndTime.Value)).ToString("h:mm tt") : null;
                    var classTime = StrStartTime != null && StrEndTime != null ? string.Format("{0} to {1}", StrStartTime, StrEndTime) : "";
                    sumclassFee = sumclassFee + classFee;
                    sumCharge = sumCharge + programCharges;
                    sumDiscount = sumDiscount + Math.Abs(programDiscounts);
                    sumCoupon = sumCoupon + Math.Abs(programCoupons);
                    price = classFee * ParticipantCount;
                    sumPrice = sumPrice + price;
                    sumParticipantCount = sumParticipantCount + ParticipantCount;
                    sumRevenue = sumRevenue + revenue;
                    //Create a new row
                    programCount = programNumber++;
                    var row = sheet1.CreateRow(programCount);

                    //Set values for the cells
                    row.CreateCell(0).SetCellValue(program.Name);
                    row.CreateCell(1).SetCellValue(gradeOrAges);
                    row.CreateCell(2).SetCellValue(programBusiness.FormatDays(allDaysProgram));
                    row.CreateCell(3).SetCellValue(classTime);
                    row.CreateCell(4).SetCellValue(providerName);
                    row.CreateCell(5).SetCellValue(minimumCapacity);
                    row.CreateCell(6).SetCellValue(maximumCapacity);
                    row.CreateCell(7).SetCellValue(ParticipantCount);
                    row.CreateCell(8).SetCellValue(CurrencyHelper.FormatCurrencyWithPenny(classFee, currency));
                    row.CreateCell(9).SetCellValue(CurrencyHelper.FormatCurrencyWithPenny(classFee * ParticipantCount, currency));
                    row.CreateCell(10).SetCellValue(CurrencyHelper.FormatCurrencyWithPenny(programCharges, currency));
                    row.CreateCell(11).SetCellValue(CurrencyHelper.FormatCurrencyWithPenny(Math.Abs(programDiscounts), currency));
                    row.CreateCell(12).SetCellValue(CurrencyHelper.FormatCurrencyWithPenny(Math.Abs(programCoupons), currency));
                    row.CreateCell(13).SetCellValue(CurrencyHelper.FormatCurrencyWithPenny(revenue, currency));

                    for (int i = 8; i < 14; i++)
                    {
                        row.GetCell(i).CellStyle = alignmentRightStyle;
                    }
                }
                var lastRow = sheet1.CreateRow(programCount + 1);

                lastRow.CreateCell(0).SetCellValue("Total");
                lastRow.CreateCell(1);
                lastRow.CreateCell(2);
                lastRow.CreateCell(3);
                lastRow.CreateCell(4);
                lastRow.CreateCell(5);
                lastRow.CreateCell(6);
                lastRow.CreateCell(7).SetCellValue(sumParticipantCount);
                lastRow.CreateCell(8).SetCellValue(CurrencyHelper.FormatCurrencyWithPenny(sumclassFee, currency));
                lastRow.CreateCell(9).SetCellValue(CurrencyHelper.FormatCurrencyWithPenny(sumPrice, currency));
                lastRow.CreateCell(10).SetCellValue(CurrencyHelper.FormatCurrencyWithPenny(sumCharge, currency));
                lastRow.CreateCell(11).SetCellValue(CurrencyHelper.FormatCurrencyWithPenny(sumDiscount, currency));
                lastRow.CreateCell(12).SetCellValue(CurrencyHelper.FormatCurrencyWithPenny(sumCoupon, currency));
                lastRow.CreateCell(13).SetCellValue(CurrencyHelper.FormatCurrencyWithPenny(sumRevenue, currency));

                for (int i = 0; i < 14; i++)
                {
                    lastRow.GetCell(i).CellStyle = borderTopStyle;
                }
                for (int i = 8; i < 14; i++)
                {

                    lastRow.GetCell(i).CellStyle = lastRowAlignmentRightStyle;
                }

            }
            return workbook;
        }


        [HttpGet]
        public virtual FileContentResult GenerateEnrollmentExcel(SeasonNames name, int year)
        {
            var workbook = CreateExcelEnrollmentReport(name, year);

            //Write the Workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            //Return the result to the end user
            return File(output.ToArray(),   //The binary data of the XLS file
                "application/vnd.ms-excel",//MIME type of Excel files
             "School enrollments " + name.ToDescription() + " " + year + ".xls");
            //Suggested file name in the "Save as" dialog which will be displayed to the end user

        }

        [HttpGet]
        [JbAuthorize(JbAction.Season_View)]
        public virtual JsonNetResult GetSchoolsEnrollmentsInfo()
        {
            var model = new SeasonViewModel();

            return JsonNet(model);
        }

        [HttpGet]
        [JbAuthorize(JbAction.Season_View)]
        public virtual JsonNetResult GetRenewalPTARepotInfo()
        {
            var model = new RenewalPTAReportViewModel();

            return JsonNet(model);
        }
        [HttpGet]
        public virtual FileResult GenerateRenewalPTAReportPdf(int schoolId, int year)
        {
            var pdfGenerator = new FlyerGenerator();
            var club = ActiveClub;
            var programBusiness = Ioc.ProgramBusiness;
            //Create Query By Fillter
            var school = _clubBusiness.Get(schoolId);
            var currency = school.Currency;

            var SchoolRenewal = new List<SchoolRenewalReportViewModel>();
            var programsSchool = new List<SchoolProgramsRenewalReportViewModel>();


            var allOrderItems = Ioc.OrderItemBusiness.GetOrderItems(clubId: schoolId, seasonId: null, isTestMode: false)
                .Where(o => o.ItemStatus == OrderItemStatusCategories.completed).Where(o => (year == 0 || (year != 0 && (o.Season.Year == year - 1) &&
                (o.Season.Name == SeasonNames.Fall)
                || ((o.Season.Year == year) && (o.Season.Name == SeasonNames.Winter || o.Season.Name == SeasonNames.Spring || o.Season.Name == SeasonNames.Summer)))));


            var programs = school.Seasons.Where(se => ((year == 0 || (year != 0 && (se.Year == year - 1) &&
            (se.Name == SeasonNames.Fall)
            || ((se.Year == year) && (se.Name == SeasonNames.Winter || se.Name == SeasonNames.Spring || se.Name == SeasonNames.Summer)))))).
             SelectMany(s => s.Programs).Where(p => p.Status == ProgramStatus.Open || p.Status == ProgramStatus.Frozen).ToList().Where(p => p.SaveType == SaveType.Publish);


            var sumEnrollmentCount = 0;
            programsSchool = new List<SchoolProgramsRenewalReportViewModel>();
            foreach (var program in programs)
            {

                if (program != null)
                {
                    var minimumCapacity = (program.TypeCategory == ProgramTypeCategory.Class) && program.ProgramSchedules.Any(s => !s.IsDeleted) ? (program.ProgramSchedules.Where(s => !s.IsDeleted).First()).Attributes.MinimumEnrollment : 0;
                    var maximumCapacity = (program.TypeCategory == ProgramTypeCategory.Class) && program.ProgramSchedules.Any(s => !s.IsDeleted) ? (program.ProgramSchedules.Where(s => !s.IsDeleted).First()).Attributes.Capacity : 0;

                    var programOrderitems = allOrderItems.Where(o => o.ProgramSchedule.ProgramId == program.Id).ToList();
                    var enrollmentCount = programOrderitems.Count();
                    sumEnrollmentCount = sumEnrollmentCount + enrollmentCount;


                    programsSchool.Add(new SchoolProgramsRenewalReportViewModel
                    {
                        ProgramName = program.Name,
                        ProviderName = program.OutSourceSeasonId.HasValue ? program.OutSourceSeason.Club.Name : "",
                        MinimumCapacity = minimumCapacity,
                        MaximumCapacity = maximumCapacity,
                        Enrollment = enrollmentCount,
                        Price = CurrencyHelper.FormatCurrencyWithPenny(program.ProgramSchedules.First().Charges.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).First().Amount, currency),
                        StrProgramSeason = program.Season.Name + " " + program.Season.Year,
                        ProgramSeason = program.Season.Name,
                        ProgramYear = program.Season.Year,
                        IsRun = program.Status == ProgramStatus.Frozen || program.Status == ProgramStatus.Deleted ? "No" : "Yes",
                        Days = (programBusiness.GetClassDays(program).Select(pr => pr.DayOfWeek).ToList()),
                        Grade = programBusiness.GenerateGradeInfoLabelForReport(program)
                    });
                }

            }
            SchoolRenewal.Add(new SchoolRenewalReportViewModel
            {
                SchoolLogo = UrlHelpers.GetClubLogoUrl(school.Domain, school.Logo),
                SchoolName = school.Name,
                Year = year,
                Programs = programsSchool,
                TotalEnrollment = sumEnrollmentCount
            });

            var SchoolRenewalPdf = new RenewalPTAPdfViewModel();

            SchoolRenewalPdf = new RenewalPTAPdfViewModel()
            {
                ClubLogo = UrlHelpers.GetClubLogoUrl(school.PartnerClub.Domain, school.PartnerClub.Logo),
                Schools = SchoolRenewal
            };



            var file = pdfGenerator.GenerateFromView(this.ControllerContext, "SchoolRenewalPTAPdfReports", SchoolRenewalPdf);

            return File(file, "application/pdf", "PTA Annual Renewal " + school.Name + " " + year + ".Pdf");
        }

        [HttpGet]
        public virtual FileResult GenerateEnrollmentPdf(SeasonNames name, int year)
        {
            var pdfGenerator = new FlyerGenerator();
            var club = ActiveClub;

            //Create Query By Fillter
            var schools =
                _clubBusiness.GetRelatedClubs(club.Id)
                    .Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.School).ToList();

            schools = schools.Where(s => s.Seasons.Any(se => (year == 0 || (year != 0 && se.Year == year)) &&
            (name == SeasonNames.SelectSeason || (name != SeasonNames.SelectSeason && se.Name == name)))).OrderBy(n => n.Name).ToList();

            var schoolsEnrollments = new List<SchoolsEnrollmentReportViewModel>();
            var programsSchool = new List<SchoolsProgramsEnrollmentReportViewModel>();

            List<int> listClubIds = new List<int>();
            listClubIds = schools.Select(s => s.Id).ToList();

            var allOrderItems = Ioc.OrderItemBusiness.GetOrderItems(clubIds: listClubIds, isTestMode: false)
                .Where(o => o.ItemStatus == OrderItemStatusCategories.completed).
                Where(o => (year == 0 || (year != 0 && o.Season.Year == year) &&
                (name == SeasonNames.SelectSeason || (name != SeasonNames.SelectSeason && o.Season.Name == name))));


            foreach (var school in schools)
            {
                var programs = school.Seasons.Where(se => ((year == 0 || (year != 0 && se.Year == year))) &&
                        (name == SeasonNames.SelectSeason || (name != SeasonNames.SelectSeason && se.Name == name))).
                      SelectMany(s => s.Programs).Where(p => p.Status == ProgramStatus.Open).ToList().Where(p => p.SaveType == SaveType.Publish);

                var sumEnrollmentCount = 0;
                programsSchool = new List<SchoolsProgramsEnrollmentReportViewModel>();
                foreach (var program in programs)
                {
                    var minimumCapacity = (program.TypeCategory == ProgramTypeCategory.Class) && program.ProgramSchedules.Any(s => !s.IsDeleted) ? (program.ProgramSchedules.Where(s => !s.IsDeleted).First()).Attributes.MinimumEnrollment : 0;
                    var programOrderitems = allOrderItems.Where(o => o.ProgramSchedule.ProgramId == program.Id).ToList();
                    var enrollmentCount = programOrderitems.Count();
                    sumEnrollmentCount = sumEnrollmentCount + enrollmentCount;


                    programsSchool.Add(new SchoolsProgramsEnrollmentReportViewModel
                    {
                        ProgramName = program.Name,
                        MinimumCapacity = minimumCapacity,
                        Enrollment = enrollmentCount,
                    });
                }
                schoolsEnrollments.Add(new SchoolsEnrollmentReportViewModel
                {
                    SchoolLogo = UrlHelpers.GetClubLogoUrl(school.Domain, school.Logo),
                    SchoolName = school.Name,
                    SeasonName = name,
                    Year = year,
                    Programs = programsSchool,
                    TotalEnrollment = sumEnrollmentCount
                });
            }
            var SchoolsEnrollments = new SchoolEnrollmentsPdfViewModel();
            if (schools.Count != 0)
            {
                SchoolsEnrollments = new SchoolEnrollmentsPdfViewModel()
                {
                    ClubLogo = UrlHelpers.GetClubLogoUrl(schools.FirstOrDefault().PartnerClub.Domain, schools.FirstOrDefault().PartnerClub.Logo),
                    Schools = schoolsEnrollments
                };
            }


            var file = pdfGenerator.GenerateFromView(this.ControllerContext, "SchoolEnrollmentsPdfReports", SchoolsEnrollments);

            return File(file, "application/pdf", "Enrollments by school " + name.ToDescription() + " " + year + ".Pdf");
        }

        public virtual JsonNetResult SelectTeacherParticipantsClass(long seasonId, int schoolId, string teacherName, PaginationModel paginationModel)
        {
            var model = CreateModelTeachers(seasonId, schoolId, teacherName);

            var datasource = model.Skip((paginationModel.Page - 1) * paginationModel.PageSize).Take(paginationModel.PageSize).ToList();
            return JsonNet(new { DataSource = datasource, TotalCount = model.Count() });
        }

        private List<TeachersReportViewModel> CreateModelTeachers(long seasonId, int schoolId, string teacherName)
        {
            var orderItems = Ioc.OrderItemBusiness.GetList().Where(c => c.SeasonId == seasonId && c.ProgramScheduleId.HasValue && c.ItemStatus == OrderItemStatusCategories.completed && c.Order.IsLive);
            var fromBusiness = Ioc.FormBusiness;
            var programBusiness = Ioc.ProgramBusiness;
            var model = new List<TeachersReportViewModel>();

            if (orderItems.Any())
            {
                model = orderItems.OrderByDescending(c => c.Id).ToList().Select(c => new TeachersReportViewModel()
                {
                    ParticipantName = string.Format("{0}, {1}", fromBusiness.GetPlayerLastName(c.JbForm), fromBusiness.GetPlayerFirstName(c.JbForm)),
                    TeacherName = c.JbForm != null ? fromBusiness.GetTeacher(c.JbForm) : "",
                    ClassName = c.ProgramSchedule != null ? !string.IsNullOrEmpty(c.ProgramSchedule.Program.Name) ? c.ProgramSchedule.Program.Name : "-" : "",
                    ClassDays = c.ProgramSchedule != null ? programBusiness.FormatDays(programBusiness.GetClassDays(c.ProgramSchedule.Program).Select(pr => pr.DayOfWeek).ToList()) : "",
                    Room = c.ProgramSchedule != null ? c.ProgramSchedule.Program.Room : "",
                }).OrderBy(n => n.TeacherName).OrderBy(n => n.ClassName).ToList();
            }


            if (!string.IsNullOrEmpty(teacherName))
            {
                model = model.Where(t => t.TeacherName.ToLower() == teacherName.ToLower()).OrderBy(n => n.ParticipantName).ToList();
            }

            return model;
        }

        [HttpGet]
        public virtual FileResult GenerateTeacherPdf(long seasonId, int schoolId, string teacherName)
        {
            var pdfGenerator = new FlyerGenerator();
            var seasonBusiness = Ioc.SeasonBusiness;

            var school = _clubBusiness.Get(schoolId);
            var Partner = _clubBusiness.Get(ActiveClub.Id);

            var seasonName = seasonBusiness.Get(seasonId).Name;

            var model = CreateModelTeachers(seasonId, schoolId, teacherName);

            var PdfModel = model.Select(c => new TeachersPdfViewModel()
            {
                ParticipantName = c.ParticipantName,
                TeacherName = c.TeacherName,
                ClassName = c.ClassName,
                ClassDays = c.ClassDays,
                Room = c.Room
            }).ToList();

            var TeachersList = new TeachersListPdfViewModel()
            {
                SchoolLogo = UrlHelpers.GetClubLogoUrl(school.Domain, school.Logo),
                ClubLogo = UrlHelpers.GetClubLogoUrl(Partner.Domain, Partner.Logo),
                Teachers = PdfModel,
                SchoolName = school.Name,
                Date = _clubBusiness.GetClubDateTime(Partner.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt")
            };

            var file = pdfGenerator.GenerateFromView(this.ControllerContext, "TeacherPdfReports", TeachersList);

            return File(file, "application/pdf", "Teacher List_" + school.Name + " " + seasonName.ToDescription() + ".Pdf");
        }

        public virtual ActionResult ProgramsListSignOutSheetReport(long seasonId, int schoolId, long? programId, DateTime date)
        {
            var programBusiness = Ioc.ProgramBusiness;
            var programs = programBusiness.GetListBySeasonId(seasonId).Where(p => p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4);
            var model = new List<schoolProgramsCheckInOutReportViewModel>();

            model = CreateModelSignOutSheetReport(seasonId, schoolId, programId, date);

            return JsonNet(model);
        }

        private List<ProgramParticipantListViewModel> GetAllParticipant(Program program, DateTime date)
        {
            var orderItemBusiness = Ioc.OrderItemBusiness;
            var programBusiness = Ioc.ProgramBusiness;
            var formBusiness = Ioc.FormBusiness;
            var programOrderItems = new List<OrderItem>();
            date = date.Date;

            if (program.TypeCategory == ProgramTypeCategory.BeforeAfterCare || program.TypeCategory == ProgramTypeCategory.Camp || program.TypeCategory == ProgramTypeCategory.Subscription)
            {
                programOrderItems = Ioc.OrderItemBusiness.GetProgramOrderItems(program.Id, OrderItemStatusCategories.showAll)
                    .ToList()
                    .Where(o => o.ItemStatus == OrderItemStatusCategories.completed || (o.ItemStatus == OrderItemStatusCategories.changed && o.Attributes.CancelEffectiveDate > date) || (o.ItemStatus == OrderItemStatusCategories.changed && o.Attributes.TransferEffectiveDate > date))
                    .ToList();
            }
            else
            {
                programOrderItems = Ioc.OrderItemBusiness.GetProgramOrderItems(program.Id).ToList();
            }

            var model = new List<ProgramParticipantListViewModel>();
            var num = 1;

            model = programOrderItems.ToList().Select(o => new ProgramParticipantListViewModel()
            {
                ParticipantName = string.Format("{0}, {1}", formBusiness.GetPlayerLastName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerLastName(o.JbForm), formBusiness.GetPlayerFirstName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerFirstName(o.JbForm)),
                TeacherName = o.JbForm != null ? formBusiness.GetTeacher(o.JbForm) == "" ? "-" : formBusiness.GetTeacher(o.JbForm) : "",
                Phone = formBusiness.GetParentPhone(o.JbForm) == "" ? "-" : formBusiness.GetParentPhone(o.JbForm),
                TransportHome = o.JbForm != null ? formBusiness.GetDismissalFromEnrichment(o.JbForm) == "" ? "-" : formBusiness.GetDismissalFromEnrichment(o.JbForm) : "",
                Grade = formBusiness.GetPlayerGradeAsString(o.JbForm) != null ? formBusiness.GetPlayerGradeAsString(o.JbForm) == "Kindergarten" ? "K" : formBusiness.GetPlayerGradeAsString(o.JbForm) : string.Empty,
            }).OrderBy(n => n.ParticipantName).ToList();

            foreach (var participant in model)
            {
                participant.Number = num;
                num++;
            }

            return model;
        }

        private List<schoolProgramsCheckInOutReportViewModel> CreateModelSignOutSheetReport(long seasonId, int schoolId, long? programId, DateTime date)
        {
            var programBusiness = Ioc.ProgramBusiness;
            var programs = new List<Program>();
            date = date.Date;
            var tommorow = date.AddDays(1);

            if (programId != null)
            {
                programs = programBusiness.GetListBySeasonId(seasonId).Where(p => p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Id == programId).ToList();
            }
            else
            {
                programs = programBusiness.GetListBySeasonId(seasonId).Where(p => p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4).ToList();
            }

            var model = new List<schoolProgramsCheckInOutReportViewModel>();

            model = programs != null ? programs.ToList().Select(p => new schoolProgramsCheckInOutReportViewModel()
            {
                ClassName = !string.IsNullOrEmpty(p.Name) ? p.Name : "-",
                ClassId = p.Id,
                ClassDays = programBusiness.GetClassDays(p) != null || programBusiness.GetClassDays(p).Any() ? programBusiness.FormatDays(programBusiness.GetClassDays(p).Select(pr => pr.DayOfWeek).ToList()) : "",
                StartTime = programBusiness.GetClassDays(p).FirstOrDefault().StartTime.HasValue ? DateTime.Today.Add(programBusiness.GetClassDays(p).FirstOrDefault().StartTime.Value).ToString("h:mm tt") : "",
                EndTime = programBusiness.GetClassDays(p).FirstOrDefault().EndTime.HasValue ? DateTime.Today.Add(programBusiness.GetClassDays(p).FirstOrDefault().EndTime.Value).ToString("h:mm tt") : "",
                DatesOfDay = GetListDates(p, schoolId, date),
                Location = p.Room,
                SchoolName = p.Season.Club.Name,
                SeasonName = p.Season.Domain,
                ProgramParticipants = GetAllParticipant(p, date).Count > 0 ? GetAllParticipant(p, date) : null,
                Instructor = programBusiness.GetInstractors(p),

            }).OrderBy(n => n.ClassName).ToList() : null;



            return model;
        }
        private List<string> GetListDates(Program program, int schoolId, DateTime selectedDate)
        {
            var programBusiness = Ioc.ProgramBusiness;
            var result = programBusiness.GetClassDays(program).Select(pr => pr.DayOfWeek).Count() > 1 ?
             programBusiness.GetDatesBetweenTwoIntervals(schoolId, GetDate(selectedDate, program.ProgramSchedules.FirstOrDefault().StartDate, program.ProgramSchedules.FirstOrDefault().EndDate), program.ProgramSchedules.FirstOrDefault().EndDate).Where(d => d.DayOfWeek == programBusiness.GetClassDays(program).FirstOrDefault().DayOfWeek && d.DayOfWeek <= programBusiness.GetClassDays(program).LastOrDefault().DayOfWeek).OrderBy(da => da.Date).ToList().Select(ds => ds.Date.ToString("MM/dd")).ToList().Count > 4 ?
             programBusiness.GetDatesBetweenTwoIntervals(schoolId, GetDate(selectedDate, program.ProgramSchedules.FirstOrDefault().StartDate, program.ProgramSchedules.FirstOrDefault().EndDate), program.ProgramSchedules.FirstOrDefault().EndDate).Where(d => d.DayOfWeek == programBusiness.GetClassDays(program).FirstOrDefault().DayOfWeek && d.DayOfWeek <= programBusiness.GetClassDays(program).LastOrDefault().DayOfWeek).OrderBy(da => da.Date).ToList().Select(ds => ds.Date.ToString("MM/dd")).ToList().GetRange(0, 4) :
             programBusiness.GetDatesBetweenTwoIntervals(schoolId, GetDate(selectedDate, program.ProgramSchedules.FirstOrDefault().StartDate, program.ProgramSchedules.FirstOrDefault().EndDate), program.ProgramSchedules.FirstOrDefault().EndDate).Where(d => d.DayOfWeek == programBusiness.GetClassDays(program).FirstOrDefault().DayOfWeek && d.DayOfWeek <= programBusiness.GetClassDays(program).LastOrDefault().DayOfWeek).OrderBy(da => da.Date).ToList().Select(ds => ds.Date.ToString("MM/dd")).ToList() :
             programBusiness.GetDatesBetweenTwoIntervals(schoolId, GetDate(selectedDate, program.ProgramSchedules.FirstOrDefault().StartDate, program.ProgramSchedules.FirstOrDefault().EndDate), program.ProgramSchedules.FirstOrDefault().EndDate).Where(d => d.DayOfWeek == programBusiness.GetClassDays(program).FirstOrDefault().DayOfWeek).ToList().OrderBy(da => da.Date).ToList().Select(ds => ds.Date.ToString("MM/dd")).ToList().Count > 4 ?
             programBusiness.GetDatesBetweenTwoIntervals(schoolId, GetDate(selectedDate, program.ProgramSchedules.FirstOrDefault().StartDate, program.ProgramSchedules.FirstOrDefault().EndDate), program.ProgramSchedules.FirstOrDefault().EndDate).Where(d => d.DayOfWeek == programBusiness.GetClassDays(program).FirstOrDefault().DayOfWeek).ToList().OrderBy(da => da.Date).ToList().Select(ds => ds.Date.ToString("MM/dd")).ToList().GetRange(0, 4) :
             programBusiness.GetDatesBetweenTwoIntervals(schoolId, GetDate(selectedDate, program.ProgramSchedules.FirstOrDefault().StartDate, program.ProgramSchedules.FirstOrDefault().EndDate), program.ProgramSchedules.FirstOrDefault().EndDate).Where(d => d.DayOfWeek == programBusiness.GetClassDays(program).FirstOrDefault().DayOfWeek).ToList().OrderBy(da => da.Date).ToList().Select(ds => ds.Date.ToString("MM/dd")).ToList();

            return result;
        }
        private DateTime GetDate(DateTime SelectedDate, DateTime StartDate, DateTime EndDate)
        {
            if (SelectedDate < StartDate)
            {
                SelectedDate = StartDate;
            }
            //if (SelectedDate > EndDate)
            //{
            //    SelectedDate = EndDate;
            //}
            return SelectedDate;
        }
        [HttpGet]
        public virtual FileResult GenerateSignOutSheetPdf(long seasonId, int schoolId, long? programId, DateTime date)
        {
            var pdfGenerator = new FlyerGenerator();
            var seasonBusiness = Ioc.SeasonBusiness;

            var school = _clubBusiness.Get(schoolId);
            var Partner = _clubBusiness.Get(ActiveClub.Id);

            var seasonName = seasonBusiness.Get(seasonId).Name;

            var model = CreateModelSignOutSheetReport(seasonId, schoolId, programId, date);

            var PdfModel = new SignOutSheetPdfViewModel()
            {
                SchoolLogo = UrlHelpers.GetClubLogoUrl(school.Domain, school.Logo),
                ClubLogo = UrlHelpers.GetClubLogoUrl(Partner.Domain, Partner.Logo),
                Programs = model,
                //Date = _clubBusiness.GetClubDateTime(Partner.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt")
            };

            var file = pdfGenerator.GenerateFromView(this.ControllerContext, "SignOutSheetPdfReports", PdfModel);

            return File(file, "application/pdf", "Sign-Out Sheet_" + school.Name + " " + seasonName.ToDescription() + ".Pdf");
        }

        [HttpGet]
        public virtual FileContentResult GenerateSignOutSheetExcel(long seasonId, int schoolId, long? programId, DateTime date)
        {
            // var pdfGenerator = new FlyerGenerator();
            var seasonBusiness = Ioc.SeasonBusiness;

            var school = _clubBusiness.Get(schoolId);
            var Partner = _clubBusiness.Get(ActiveClub.Id);

            var seasonName = seasonBusiness.Get(seasonId).Name;

            var model = CreateModelSignOutSheetReport(seasonId, schoolId, programId, date);


            var workbook = new HSSFWorkbook();

            var sheet = workbook.CreateSheet();


            //Define Style
            ICellStyle alignmentCenterStyle = workbook.CreateCellStyle();
            alignmentCenterStyle.Alignment = HorizontalAlignment.Center;
            alignmentCenterStyle.VerticalAlignment = VerticalAlignment.Center;

            var boldFont = workbook.CreateFont();
            boldFont.Boldweight = (short)FontBoldWeight.Bold;

            var greyBoldStyle = workbook.CreateCellStyle();
            greyBoldStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyBoldStyle.FillPattern = FillPattern.SolidForeground;
            greyBoldStyle.SetFont(boldFont);
            greyBoldStyle.Alignment = HorizontalAlignment.Left;
            greyBoldStyle.VerticalAlignment = VerticalAlignment.Center;

            var greyStyleLeftAlignment = workbook.CreateCellStyle();
            greyStyleLeftAlignment.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyStyleLeftAlignment.FillPattern = FillPattern.SolidForeground;
            greyStyleLeftAlignment.Alignment = HorizontalAlignment.Left;
            greyStyleLeftAlignment.VerticalAlignment = VerticalAlignment.Center;

            var greyStyleCenterAlignment = workbook.CreateCellStyle();
            greyStyleCenterAlignment.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyStyleCenterAlignment.FillPattern = FillPattern.SolidForeground;
            greyStyleCenterAlignment.Alignment = HorizontalAlignment.Center;
            greyStyleCenterAlignment.VerticalAlignment = VerticalAlignment.Center;
            //greyStyleCenterAlignment.BorderBottom = BorderStyle.MEDIUM;


            sheet.SetDefaultColumnStyle(0, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(1, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(2, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(3, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(4, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(5, alignmentCenterStyle);


            var rowNumber = 0;
            var itemNumber = 0;

            foreach (var item in model)
            {
                //**** Header ****////
                var headerRow = sheet.CreateRow(rowNumber);

                if (itemNumber != 0)
                {
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                }
                itemNumber++;

                // set default column size and color
                headerRow.CreateCell(0).SetCellValue(" ");
                headerRow.CreateCell(1).SetCellValue(" ");
                headerRow.CreateCell(2).SetCellValue(" ");
                headerRow.CreateCell(3).SetCellValue(" ");
                headerRow.CreateCell(4).SetCellValue(" ");
                headerRow.CreateCell(5).SetCellValue(" ");
                headerRow.GetCell(0).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(2).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(3).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(4).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(5).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(0).Sheet.SetColumnWidth(0, 10 * 256);
                headerRow.GetCell(1).Sheet.SetColumnWidth(1, 50 * 256);
                headerRow.GetCell(2).Sheet.SetColumnWidth(2, 20 * 256);
                headerRow.GetCell(3).Sheet.SetColumnWidth(3, 10 * 256);
                headerRow.GetCell(4).Sheet.SetColumnWidth(4, 10 * 256);
                headerRow.GetCell(5).Sheet.SetColumnWidth(5, 10 * 256);

                //Set the column names in the header row
                headerRow.CreateCell(1).SetCellValue(item.ClassName);
                headerRow.CreateCell(6).SetCellValue(item.SchoolName);
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).Sheet.SetColumnWidth(0, 50 * 256);
                headerRow.GetCell(1).CellStyle = greyBoldStyle;
                headerRow.GetCell(6).CellStyle = greyBoldStyle;

                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue("Days of week");
                headerRow.CreateCell(2).SetCellValue(item.ClassDays);
                headerRow.CreateCell(6).SetCellValue("Check - In/Out Sheet");
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(2).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(6).CellStyle = greyBoldStyle;


                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue("Time");
                headerRow.CreateCell(2).SetCellValue(item.StrTime);
                headerRow.CreateCell(6).SetCellValue(item.SeasonName);
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(2).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(6).CellStyle = greyBoldStyle;


                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue("Instructor");
                headerRow.CreateCell(2).SetCellValue(item.Instructor);
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(2).CellStyle = greyStyleLeftAlignment;

                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue("Location");
                headerRow.CreateCell(2).SetCellValue(item.Location);
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(2).CellStyle = greyStyleLeftAlignment;

                //line space between header and content
                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);


                if (item.ProgramParticipants == null)
                {
                    continue;
                }

                //*** Content ***//
                // static header
                rowNumber++;
                var row = sheet.CreateRow(rowNumber);
                row.CreateCell(0).SetCellValue("#");
                row.GetCell(0).CellStyle = greyStyleCenterAlignment;
                row.GetCell(0).Sheet.SetColumnWidth(0, 10 * 256);
                row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 0, 0));

                row.CreateCell(1).SetCellValue("Student name");
                row.CreateCell(2).SetCellValue("Teacher name");
                row.CreateCell(3).SetCellValue("Grade");
                row.GetCell(2).CellStyle = greyStyleCenterAlignment;
                row.GetCell(1).CellStyle = greyStyleCenterAlignment;
                row.GetCell(3).CellStyle = greyStyleCenterAlignment;
                var lastCell = 4;
                for (int i = 0; i < item.DatesOfDay.Count(); i++)
                {
                    row.CreateCell(lastCell + i).SetCellValue(item.DatesOfDay[i]);
                    row.GetCell(lastCell + i).CellStyle = greyStyleCenterAlignment;
                    row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber, lastCell + i, lastCell + i + 1));
                    lastCell += 1;
                }


                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                row.CreateCell(1).SetCellValue("Student phone");
                row.CreateCell(2).SetCellValue("Transport home");
                row.CreateCell(3).SetCellValue("");
                row.GetCell(1).CellStyle = greyStyleCenterAlignment;
                row.GetCell(2).CellStyle = greyStyleCenterAlignment;
                row.GetCell(3).CellStyle = greyStyleCenterAlignment;
                lastCell = 4;
                for (int i = 0; i < item.DatesOfDay.Count(); i++)
                {
                    row.CreateCell(lastCell + i).SetCellValue("IN");
                    row.GetCell(lastCell + i).CellStyle = greyStyleCenterAlignment;
                    row.CreateCell(lastCell + i + 1).SetCellValue("OUT");
                    row.GetCell(lastCell + i + 1).CellStyle = greyStyleCenterAlignment;
                    lastCell += 1;
                }


                //Content
                foreach (var participant in item.ProgramParticipants)
                {
                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(participant.Number);
                    row.GetCell(0).CellStyle = alignmentCenterStyle;
                    row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 0, 0));

                    row.CreateCell(1).SetCellValue(participant.ParticipantName);
                    row.CreateCell(2).SetCellValue(participant.TeacherName);
                    row.CreateCell(3).SetCellValue(participant.Grade);
                    row.GetCell(3).CellStyle = alignmentCenterStyle;
                    row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 3, 3));


                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.CreateCell(1).SetCellValue(participant.Phone);
                    row.CreateCell(2).SetCellValue(participant.TransportHome);
                    row.CreateCell(3).SetCellValue("");
                    lastCell = 4;
                    for (int i = 0; i < item.DatesOfDay.Count(); i++)
                    {
                        row.CreateCell(lastCell + i).SetCellValue("");
                        row.CreateCell(lastCell + i + 1).SetCellValue("");

                        row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber - 1, rowNumber, lastCell, lastCell));
                        lastCell++;
                        row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber - 1, rowNumber, lastCell, lastCell));
                        lastCell++;
                    }
                }


                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                rowNumber++;
                row = sheet.CreateRow(rowNumber);

            }


            //Write the Workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            //Return the result to the end user
            return File(output.ToArray(),   //The binary data of the XLS file
                "application/vnd.ms-excel",//MIME type of Excel files
             "ReportPrograms.xls");
        }

        [HttpGet]
        public virtual ActionResult GetInternalRosterReport(long seasonId, int schoolId, long? programId)
        {
            var programBusiness = Ioc.ProgramBusiness;
            var programs = programBusiness.GetListBySeasonId(seasonId).Where(p => p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4);
            var model = new List<InternalRosterReportsViewModel>();
            var orderItemBusiness = Ioc.OrderItemBusiness;

            model = CreateModelInternalRosterReport(seasonId, schoolId, programId);

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult GetCampRosterReport(long programId, long? scheduleId, decimal? entryFeeId)
        {
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }
            var model = new List<IDictionary<string, Object>>();
            var total = 0;

            var titles = new List<string>();

            model = CreateModelCampRosterReport(programId, page, pageSize, ref total, ref titles, scheduleId, entryFeeId);

            return JsonNet(new { DataSource = model, TotalCount = total, Titles = titles });
        }

        private List<IDictionary<string, Object>> CreateModelCampRosterReport(long programId, int page, int pageSize, ref int total, ref List<string> titles, long? scheduleId, decimal? entryFeeId)
        {
            var query = Ioc.OrderItemBusiness.GetProgramOrderItems(programId, OrderItemStatusCategories.completed);

            if (scheduleId.HasValue && scheduleId.Value > 0)
            {
                query = query.Where(item => item.ProgramScheduleId == scheduleId);
            }

            if (entryFeeId.HasValue && entryFeeId.Value > 0)
            {
                query = query.Where(oitem => oitem.OrderChargeDiscounts.Any(cd => !cd.IsDeleted && cd.ChargeId == entryFeeId));
            }

            var orderItemPaging = query
                .GroupBy(o => o.PlayerId.Value)
                .Select(o => o.FirstOrDefault())
                .OrderBy(o => o.Player.Contact.LastName)
                .ThenBy(o => o.Player.Contact.FirstName)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();

            var playerProfileBusiness = Ioc.PlayerProfileBusiness;

            var userIds = orderItemPaging.Select(o => o.Order.UserId).ToList();

            var allParticipants = playerProfileBusiness.GetAllParticipantsProfile(userIds);
            var allParents = playerProfileBusiness.GetAllParentsProfile(userIds);
            var allEmergencyContacts = playerProfileBusiness.GetAllEmergencyFamily(userIds);
            var allAuthorizePickups = playerProfileBusiness.GetAllAuthorizePickupFamily(userIds);

            var model = new List<IDictionary<string, Object>>();

            var schedules = new List<ProgramSchedule>();
            if (scheduleId.HasValue && scheduleId.Value > 0)
            {
                schedules.Add(Ioc.ProgramBusiness.GetSchedule(scheduleId.Value));
            }
            else
            {
                schedules.AddRange(Ioc.ProgramBusiness.Get(programId).ProgramSchedules);
            }

            foreach (var item in orderItemPaging)
            {
                var participant = allParticipants.FirstOrDefault(a => a.Id == item.PlayerId.Value);
                var parent1 = allParents.Where(a => a.UserId == item.Order.UserId).Count() >= 1 ? allParents.Where(a => a.UserId == item.Order.UserId).ElementAt(0) : null;
                var parent2 = allParents.Where(a => a.UserId == item.Order.UserId).Count() >= 2 ? allParents.Where(a => a.UserId == item.Order.UserId).ElementAt(1) : null;
                var emergency = allEmergencyContacts.FirstOrDefault(a => a.Family.UserId == item.Order.UserId);
                var authorize1 = allAuthorizePickups.Where(a => a.Family.UserId == item.Order.UserId) != null && allAuthorizePickups.Where(a => a.Family.UserId == item.Order.UserId).Count() >= 1 ? allAuthorizePickups.Where(a => a.Family.UserId == item.Order.UserId).ElementAt(0) : null;
                var authorize2 = allAuthorizePickups.Where(a => a.Family.UserId == item.Order.UserId) != null && allAuthorizePickups.Where(a => a.Family.UserId == item.Order.UserId).Count() >= 2 ? allAuthorizePickups.Where(a => a.Family.UserId == item.Order.UserId).ElementAt(1) : null;

                var newItem = new ExpandoObject() as IDictionary<string, Object>;

                newItem.Add("Participant", participant != null && participant.Contact != null ? string.Format("{0}, {1}", participant.Contact.LastName, participant.Contact.FirstName) : string.Empty);

                titles.Add("Participant");

                for (int i = 0; i < schedules.Count; i++)
                {
                    var schedule = schedules.ElementAt(i);
                    if (item.Player.OrderItems.Any(o => o.ProgramScheduleId.Value == schedule.Id && o.ItemStatus == OrderItemStatusCategories.completed))
                    {
                        var title = !string.IsNullOrEmpty(schedule.Title) ? string.Format("{0} ({1}-{2})", schedule.Title, schedule.StartDate.ToString(Constants.DefaultDateFormat), schedule.EndDate.ToString(Constants.DefaultDateFormat))
                            : string.Format("{0}-{1}", schedule.StartDate.ToString(Constants.DefaultDateFormat), schedule.EndDate.ToString(Constants.DefaultDateFormat));

                        titles.Add(title);

                        title = string.Format("_{0}", schedule.Id);

                        var value = string.Join(", ", item.Player.OrderItems.Where(o => o.ProgramScheduleId.Value == schedule.Id && o.ItemStatus == OrderItemStatusCategories.completed).Select(o => o.EntryFeeName));

                        newItem.Add(title, value);
                    }
                    else
                    {
                        var title = !string.IsNullOrEmpty(schedule.Title) ? string.Format("{0} ({1}-{2})", schedule.Title, schedule.StartDate.ToString(Constants.DefaultDateFormat), schedule.EndDate.ToString(Constants.DefaultDateFormat))
                            : string.Format("{0}-{1}", schedule.StartDate.ToString(Constants.DefaultDateFormat), schedule.EndDate.ToString(Constants.DefaultDateFormat));

                        titles.Add(title);

                        title = string.Format("_{0}", schedule.Id);

                        var value = string.Empty;

                        newItem.Add(title, value);
                    }
                }

                newItem.Add("Gender", participant != null && participant.Contact != null ? (participant.Gender == GenderCategories.Male || participant.Gender == GenderCategories.Female || participant.Gender == GenderCategories.GenderNeutral) ? participant.Gender.ToDescription() : string.Empty : string.Empty);
                newItem.Add("Grade", participant != null && participant.Info != null ? participant.Info.Grade != 0 ? participant.Info.Grade.ToDescription() : string.Empty : string.Empty);
                newItem.Add("Parent_1", parent1 != null && parent1.Contact != null ? string.Format("{0}, {1}", parent1.Contact.LastName, parent1.Contact.FirstName) : string.Empty);
                newItem.Add("Phone_1", parent1 != null && parent1.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(parent1.Contact.Phone) : string.Empty);
                newItem.Add("Email_1", parent1 != null && parent1.Contact != null ? parent1.Contact.Email : string.Empty);
                newItem.Add("Parent_2", parent2 != null && parent2.Contact != null ? string.Format("{0}, {1}", parent2.Contact.LastName, parent2.Contact.FirstName) : string.Empty);
                newItem.Add("Phone_2", parent2 != null && parent2.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(parent2.Contact.Phone) : string.Empty);
                newItem.Add("Email_2", parent2 != null && parent2.Contact != null ? parent2.Contact.Email : string.Empty);
                newItem.Add("Emergency_contact", emergency != null && emergency.Contact != null ? string.Format("{0}, {1}", emergency.Contact.LastName, emergency.Contact.FirstName) : string.Empty);
                newItem.Add("Emergency_contact_phone", emergency != null && emergency.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(emergency.Contact.Phone) : string.Empty);
                newItem.Add("Authorize_adult_1", authorize1 != null && authorize1.Contact != null ? string.Format("{0}, {1}", authorize1.Contact.LastName, authorize1.Contact.FirstName) : string.Empty);
                newItem.Add("Authorize_adult_1_phone", authorize1 != null && authorize1.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(authorize1.Contact.Phone) : string.Empty);
                newItem.Add("Authorize_adult_2", authorize2 != null && authorize2.Contact != null ? string.Format("{0}, {1}", authorize2.Contact.LastName, authorize2.Contact.FirstName) : string.Empty);
                newItem.Add("Authorize_adult_2_phone", authorize2 != null && authorize2.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(authorize2.Contact.Phone) : string.Empty);
                newItem.Add("Medical_condition_SSLASH_special_needs", participant != null && participant.Info != null ? participant.Info.SpecialNeeds : string.Empty);
                newItem.Add("Allergies", participant != null && participant.Info != null ? participant.Info.Allergies : string.Empty);

                titles.Add("Gender");
                titles.Add("Grade");
                titles.Add("Parent 1");
                titles.Add("Phone 1");
                titles.Add("Email 1");
                titles.Add("Parent 2");
                titles.Add("Phone 2");
                titles.Add("Email 2");
                titles.Add("Emergency contact");
                titles.Add("Phone");
                titles.Add("Authorize adult 1");
                titles.Add("Phone");
                titles.Add("Authorize adult 2");
                titles.Add("Phone");
                titles.Add("Medical condition/special needs");
                titles.Add("Allergies");

                model.Add(newItem);
            }

            total = query.GroupBy(o => o.PlayerId.Value).Count();

            return model;
        }

        [HttpGet]
        public virtual FileResult GenerateInternalRosterReportPdf(long seasonId, int schoolId, long? programId)
        {
            var pdfGenerator = new FlyerGenerator();
            var seasonBusiness = Ioc.SeasonBusiness;

            var school = _clubBusiness.Get(schoolId);
            var Partner = _clubBusiness.Get(ActiveClub.Id);

            var seasonName = seasonBusiness.Get(seasonId).Name;

            var model = CreateModelInternalRosterReport(seasonId, schoolId, programId);

            var PdfModel = new InternalRosterReportsPdfViewModel()
            {
                SchoolLogo = UrlHelpers.GetClubLogoUrl(school.Domain, school.Logo),
                ClubLogo = UrlHelpers.GetClubLogoUrl(Partner.Domain, Partner.Logo),
                Programs = model,
            };

            var file = pdfGenerator.GenerateFromView(this.ControllerContext, "InternalRosterReportsPdf", PdfModel);

            return File(file, "application/pdf", "Internal Roster_" + school.Name + " " + seasonName.ToDescription() + ".Pdf");
        }
        public virtual ActionResult FillFinanceFilters()
        {
            var clubId = ActiveClub.Id;

            var allTransctionsCategories = new List<SelectKeyValue<string>>
            {
                new SelectKeyValue<string>
                {
                    Text = "All",
                    Value = ""
                }
            };

            allTransctionsCategories.AddRange(
                Enum.GetValues(typeof(TransactionCategory))
                    .Cast<TransactionCategory>()
                    .Select(s => new SelectKeyValue<string>
                    {
                        Text = s.ToDescription(),
                        Value = s.ToString()
                    }));

            var allPaymentMethods = new List<SelectKeyValue<string>>
            {
                new SelectKeyValue<string>
                {
                    Text = "All",
                    Value = ""
                }
            };

            allPaymentMethods.AddRange(Enum.GetValues(typeof(PaymentMethod))
                .Cast<PaymentMethod>()
                .Select(s => new SelectKeyValue<string>
                {
                    Text = s.ToDescription(),
                    Value = s.ToString()
                }));


            var allMembers = new List<SelectKeyValue<string>> {
                new SelectKeyValue<string>
                {
                    Text = "All",
                    Value = ""
                }
            };

            allMembers.AddRange(
              _clubBusiness.GetRelatedClubs(clubId)
                  .Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.School || c.ClubType.ParentId.Value == (int)ClubTypesEnum.Provider)
                  .Select(c => new SelectKeyValue<string> { Text = c.Name, Value = c.Id.ToString() })
                  .OrderBy(c => c.Text).ToList());

            var groupBy = new List<SelectKeyValue<bool>>
            {
                new SelectKeyValue<bool>()
                {
                    Text = "Each order item",
                    Value = false
                },
                new SelectKeyValue<bool>()
                {
                    Text = "Entire order",
                    Value = true
                }
            };

            return JsonNet(new { TransactionsCategories = allTransctionsCategories, PaymentMethods = allPaymentMethods, AllMembers = allMembers, GroupBy = groupBy });
        }
        public virtual ActionResult GeneralReportRun(int? memberId, ReportFilters filters, bool makeCampaign = false, bool isGroupBy = false, bool isExcelExport = false)
        {
            var club = ActiveClub;
            //var season = Ioc.SeasonBusiness.Get(seasonDomain, club.Id);

            var pageSize = (Request.Params["PageSize"] != null) ? int.Parse(Request.Params["PageSize"]) : 100000;
            var page = (Request.Params["Page"] != null) ? int.Parse(Request.Params["Page"]) : 1;

            var transactions = Enumerable.Empty<TransactionActivity>().AsQueryable();
            if (memberId.HasValue)
            {
                transactions = Ioc.TransactionActivityBusiness.GetAllClubTransactionByClubId(memberId.Value);
            }
            else
            {
                transactions = Ioc.TransactionActivityBusiness.GetList().Where(t => t.Club.PartnerId == club.Id
                && t.TransactionStatus == TransactionStatus.Success && t.Order.IsLive);
            }


            if (filters != null)
            {
                var timezone = club.TimeZone;

                if (filters.RegistrationStart.HasValue)
                {
                    var startDate = DateTimeHelper.ConvertLocalDateTimeToUtc(filters.RegistrationStart.Value, timezone.Value);

                    transactions =
                        transactions.Where(
                            t =>
                                t.TransactionDate >= startDate);
                }

                if (filters.RegistrationEnd.HasValue)
                {
                    var endDate = DateTimeHelper.ConvertLocalDateTimeToUtc(filters.RegistrationEnd.Value, timezone.Value);

                    transactions =
                        transactions.Where(
                            t =>
                                t.TransactionDate <= endDate);
                }

                if (filters.TransactionFilters != null)
                {
                    if (filters.TransactionFilters.TransactionCategory.HasValue)
                    {
                        transactions =
                            transactions.Where(
                                    t => t.TransactionCategory == filters.TransactionFilters.TransactionCategory.Value)
                                .Select(t => t);
                    }
                    if (filters.TransactionFilters.PaymentMethod.HasValue)
                    {
                        transactions =
                            transactions.Where(
                                    t => t.PaymentDetail.PaymentMethod == filters.TransactionFilters.PaymentMethod.Value)
                                .Select(t => t);
                    }
                }

            }

            var groupbyTransactions = new List<IGrouping<long, TransactionActivity>>();
            var modelTransactions = new List<IGrouping<long, TransactionActivity>>();

            if (!isGroupBy)
            {
                var sortTransactions = transactions.OrderBy(t => t.TransactionDate);
                groupbyTransactions = sortTransactions.GroupBy(t => t.Id).ToList().OrderBy(i => i.First().TransactionDate).ToList();
                modelTransactions = groupbyTransactions.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                groupbyTransactions = transactions.GroupBy(t => t.PaymentDetailId).ToList();
                modelTransactions = groupbyTransactions.OrderBy(g => g.Key).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            var model = FillFinanceModel(modelTransactions);
            var dataSource = model;

            if (isExcelExport)
            {
                var columns = new List<string>() { "Organiziation", "Name", "Season", "Program", "Account email", "Transaction date", "Payer email", "Confirmation", "Transaction type", "Transaction category", "Transaction ID", "Payment method", "Payment handler", "Memo", "Payment", "Deposit"}; 

                var rows = new List<List<string>>();

                var bindingFlags = BindingFlags.Instance |
                                   BindingFlags.NonPublic |
                                   BindingFlags.Public;

                foreach (var item in dataSource)
                {
                    var fields = item.GetType().GetFields(bindingFlags);

                    var row = (from field in fields select field.GetValue(item) != null ? field.GetValue(item).ToString() : string.Empty).ToList();
                    rows.Add(row);
                }

                var excelModel = new ReportExportModel
                {
                    Columns = columns,
                    Rows = rows,
                    ClubName = ActiveClub.Name,
                    ReportName = "Transaction report"
                };
                var file = _reportBusiness.CreateExcelExport(excelModel);

                return File(file, FileTypes.ExcelXlsxMimeType);
            }

            return JsonNet(new { DataSource = dataSource, TotalCount = groupbyTransactions.Count() });
        }
        private List<FinancePortalViewModel> FillFinanceModel(IEnumerable<IGrouping<long, TransactionActivity>> transactions)
        {
            var model = (from @group in transactions
                         let activity = @group.ToList()
                         select new FinancePortalViewModel
                         {
                             Note = activity.First().Note,
                             Confirmation = activity.First().Order.ConfirmationId,
                             Charge =
                                 (activity.First().TransactionCategory != TransactionCategory.Refund &&
                                  activity.First().TransactionCategory != TransactionCategory.Cancellation)
                                     ? activity.Sum(c => c.Amount)
                                     : 0,
                             Payment =
                                 (activity.First().TransactionCategory == TransactionCategory.Refund ||
                                  activity.First().TransactionCategory == TransactionCategory.Cancellation)
                                     ? activity.Sum(c => c.Amount)
                                     : 0,
                             Date =
                                 DateTimeHelper.ConvertUtcDateTimeToLocal(activity.First().TransactionDate, ActiveClub.TimeZone)
                                     .ToString(Constants.DateTime_Comma),
                             PaymentMethod = activity.First().PaymentDetail.PaymentMethod,
                             TransactionType = activity.First().TransactionType,
                             TransactionCategory = activity.First().TransactionCategory,
                             SeasonName = activity.First().Season.Title,
                             CheckId =
                                 (!string.IsNullOrEmpty(activity.First().PaymentDetail.TransactionId))
                                     ? activity.First().PaymentDetail.TransactionId
                                     : (!string.IsNullOrEmpty(activity.First().CheckId)) ? activity.First().CheckId : "-",
                             UserEmail = activity.First().Order.User.UserName,
                             ClubName = activity.First().Club.Name,
                             PayerEmail = activity.First().PaymentDetail.PayerEmail,
                             PaypalHandler =
                                 (activity.First().PaymentDetail.PaymentMethod == PaymentMethod.Paypal &&
                                  activity.First().PaymentDetail.PaypalIPN.HasValue())
                                     ? "Jumbula"
                                     : (activity.First().PaymentDetail.PaymentMethod == PaymentMethod.Paypal &&
                                        !activity.First().PaymentDetail.PaypalIPN.HasValue())
                                         ? "Organization"
                                         : "-",
                             FullName =
                                 Regex.Replace(
                                     activity.Where(a => a.OrderItemId.HasValue && a.OrderItem != null)
                                         .Aggregate(string.Empty,
                                             (current, transactionActivity) =>
                                                 current + string.Format("{0}, ", transactionActivity.OrderItem.FullName))
                                         .Trim(), "\\,$", ""),
                             ProgramName = Regex.Replace(
                                     activity.Where(a => a.OrderItemId.HasValue && a.OrderItem != null)
                                         .Aggregate(string.Empty,
                                             (current, transactionActivity) =>
                                                 current + string.Format("{0}, ", transactionActivity.OrderItem.ProgramSchedule != null ? transactionActivity.OrderItem.ProgramSchedule.Program.Name : "Donation"))
                                         .Trim(), "\\,$", ""),
                         }).ToList();

            return model;
        }


        public virtual ActionResult GeneralReportSubscriptionRun(int? memberId, ReportFilters filters, bool makeCampaign = false, bool isGroupBy = false, bool isExcelExport = false, bool isShare = false, ShareAsAttachment shareModel = null)
        {
            var club = ActiveClub;
            //var season = Ioc.SeasonBusiness.Get(seasonDomain, club.Id);

            var pageSize = (Request.Params["PageSize"] != null) ? int.Parse(Request.Params["PageSize"]) : 100000;
            var page = (Request.Params["Page"] != null) ? int.Parse(Request.Params["Page"]) : 1;

            var transactions = Enumerable.Empty<TransactionActivity>().AsQueryable();

            if (memberId.HasValue)
            {
                transactions = Ioc.TransactionActivityBusiness.GetAllClubTransactionByClubId(memberId.Value);
            }
            else
            {
                transactions = Ioc.TransactionActivityBusiness.GetList().Where(t => t.Club.PartnerId == club.Id
                && t.TransactionStatus == TransactionStatus.Success && t.Order.IsLive);
            }


            if (filters != null)
            {
                var timezone = club.TimeZone;

                if (filters.RegistrationStart.HasValue)
                {
                    var startDate = DateTimeHelper.ConvertLocalDateTimeToUtc(filters.RegistrationStart.Value, timezone.Value);

                    transactions =
                       transactions.Where(
                           t =>
                               t.TransactionDate >= startDate);
                }

                if (filters.RegistrationEnd.HasValue)
                {
                    var endDate = filters.RegistrationEnd.Value.AddDays(1).AddSeconds(-1);

                    endDate = DateTimeHelper.ConvertLocalDateTimeToUtc(endDate, timezone.Value);

                    transactions =
                       transactions.Where(
                           t =>
                               t.TransactionDate <= endDate);
                }

                if (filters.TransactionFilters != null)
                {
                    if (filters.TransactionFilters.TransactionCategory.HasValue)
                    {
                        transactions =
                            transactions.Where(
                                t => t.TransactionCategory == filters.TransactionFilters.TransactionCategory.Value)
                                .Select(t => t);
                    }
                    if (filters.TransactionFilters.PaymentMethod.HasValue)
                    {
                        transactions =
                            transactions.Where(
                                t => t.PaymentDetail.PaymentMethod == filters.TransactionFilters.PaymentMethod.Value)
                                .Select(t => t);
                    }
                }
            }

            var groupbyTransactions = new List<IGrouping<long, TransactionActivity>>();
            var modelTransactions = new List<IGrouping<long, TransactionActivity>>();

            if (!isGroupBy)
            {
                var sortTransactions = transactions.OrderBy(t => t.TransactionDate);
                groupbyTransactions = sortTransactions.GroupBy(t => t.Id).ToList();
                modelTransactions = groupbyTransactions.OrderBy(i => i.First().TransactionDate).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                groupbyTransactions = transactions.GroupBy(t => t.PaymentDetailId).ToList();
                modelTransactions = groupbyTransactions.OrderBy(g => g.Key).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            var model = FillFinanceSubscriptionModel(modelTransactions, isGroupBy);
            var dataSource = model;

            if (isExcelExport)
            {
                var columns = new List<string>() { "Organiziation", "Name", "Season", "Program", "Program type", "Schedule name", "Account email", "Transaction date", "Payer email", "Confirmation", "Transaction type", "Transaction category", "Transaction ID", "Payment method", "Payment handler", "Memo", "Payment", "Deposit", "List price", "Discount", "Discount name", "Fees", "Fee name", "Start date", "End date", "Program start date", "Program end date" };

                var rows = new List<List<string>>();

                var bindingFlags = BindingFlags.Instance |
                                   BindingFlags.NonPublic |
                                   BindingFlags.Public;

                foreach (var item in dataSource)
                {
                    var fields = item.GetType().GetFields(bindingFlags);

                    var row = (from field in fields select field.GetValue(item) != null ? field.GetValue(item).ToString() : string.Empty).ToList();
                    rows.Add(row);
                }

                var excelModel = new ReportExportModel
                {
                    Columns = columns,
                    Rows = rows,
                    ClubName = ActiveClub.Name,
                    ReportName = "Transaction report"
                };

                var byteFile = _reportBusiness.CreateExcelExport(excelModel);

                if (isShare)
                {
                    var fileName = $"SubscriptionTransactionReport{DateTime.UtcNow.ToString().Replace("/", string.Empty).Replace(":", string.Empty).Replace(" ", string.Empty)}.xls";
                    using (var stream = new MemoryStream(byteFile))
                    {
                        var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
                        sm.UploadBlob(club.Domain, fileName, stream, "application/xls");

                    }

                    var reportLink = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, fileName).AbsoluteUri;

                    var templateModel = new EmailTemplateModel
                    {
                        HasLogo = true,
                        HasHeader = true,
                        ClubLogo = _clubBusiness.GetClubLogoURL(club.Domain, club.Logo),
                        Body = string.Format("{0} <br/><hr/> <a href=" + reportLink + "> Download Report </a>", shareModel.Message),
                        IsRegisterationEmail = true
                    };

                    var body = this.RenderViewToString(Constants.Path_EmailTemplate, templateModel);

                    Ioc.OldEmailBusiness.SendEmail(_clubBusiness.Get(ActiveClub.Id).ContactPersons.First().Email, shareModel.SentTo, shareModel.Subject, body, true);

                    return JsonNet(new JResult { Status = true });
                }

                return File(byteFile, FileTypes.ExcelXlsxMimeType);
            }

            return JsonNet(new { DataSource = dataSource, TotalCount = groupbyTransactions.Count() });
        }
        private List<FinanceSubscriptionViewModel> FillFinanceSubscriptionModel(IEnumerable<IGrouping<long, TransactionActivity>> transactions, bool isGroupBy)
        {
            var transactionActivityBusiness = Ioc.TransactionActivityBusiness;
            var model = (from @group in transactions
                         let activity = @group.ToList()
                         select new FinanceSubscriptionViewModel
                         {
                             Note = activity.First().Note,
                             Confirmation = activity.First().Order.ConfirmationId,
                             Charge =
                                 (activity.First().TransactionCategory != TransactionCategory.Refund &&
                                  activity.First().TransactionCategory != TransactionCategory.Cancellation)
                                     ? activity.Sum(c => c.Amount)
                                     : 0,
                             Payment =
                                 (activity.First().TransactionCategory == TransactionCategory.Refund ||
                                  activity.First().TransactionCategory == TransactionCategory.Cancellation)
                                     ? activity.Sum(c => c.Amount)
                                     : 0,
                             Date =
                                 DateTimeHelper.ConvertUtcDateTimeToLocal(activity.First().TransactionDate, ActiveClub.TimeZone)
                                     .ToString(Constants.DateTime_Comma),
                             PaymentMethod = activity.First().PaymentDetail.PaymentMethod,
                             StrTransactionType = activity.First().TransactionType == TransactionType.Payment ? "Deposit" : "Refund",
                             TransactionCategory = activity.First().TransactionCategory,
                             SeasonName = activity.First().Season.Title,
                             CheckId =
                                 (!string.IsNullOrEmpty(activity.First().PaymentDetail.TransactionId))
                                     ? activity.First().PaymentDetail.TransactionId
                                     : (!string.IsNullOrEmpty(activity.First().CheckId)) ? activity.First().CheckId : "-",
                             UserEmail = activity.First().Order.User.UserName,
                             ClubName = activity.First().Club.Name,
                             PayerEmail = activity.First().PaymentDetail.PayerEmail,
                             PaypalHandler =
                                 (activity.First().PaymentDetail.PaymentMethod == PaymentMethod.Paypal &&
                                  activity.First().PaymentDetail.PaypalIPN.HasValue())
                                     ? "Jumbula"
                                     : (activity.First().PaymentDetail.PaymentMethod == PaymentMethod.Paypal &&
                                        !activity.First().PaymentDetail.PaypalIPN.HasValue())
                                         ? "Organization"
                                         : "-",
                             FullName =
                                 Regex.Replace(
                                     activity.Where(a => a.OrderItemId.HasValue && a.OrderItem != null)
                                         .Aggregate(string.Empty,
                                             (current, transactionActivity) =>
                                                 current + string.Format("{0}, ", transactionActivity.OrderItem.FullName))
                                         .Trim(), "\\,$", ""),
                             ProgramName = Regex.Replace(
                                     activity.Where(a => a.OrderItemId.HasValue && a.OrderItem != null)
                                         .Aggregate(string.Empty,
                                             (current, transactionActivity) =>
                                                 current + string.Format("{0}, ", transactionActivity.OrderItem.ProgramSchedule != null ? transactionActivity.OrderItem.ProgramSchedule.Program.Name : "Donation"))
                                         .Trim(), "\\,$", ""),
                             ProgramScheduleTitle = transactionActivityBusiness.GetProgramScheduleTitle(activity),

                             ListPrice = transactionActivityBusiness.GetTransactionPrice(activity),

                             Discount = transactionActivityBusiness.CalculateSubscriptionChargeDiscount(activity, true, !isGroupBy),

                             DiscountName = transactionActivityBusiness.GetOrderChargeDiscountName(activity, true, isGroupBy),

                             Fees = transactionActivityBusiness.CalculateSubscriptionChargeDiscount(activity, false, isGroupBy),

                             FeeName = transactionActivityBusiness.GetOrderChargeDiscountName(activity, false, isGroupBy),

                             StartDate = !isGroupBy ? activity.First().Installment != null && activity.First().Installment.Type == InstallmentType.Noraml && activity.First().Installment.ProgramSchedulePart != null ? activity.First().Installment.ProgramSchedulePart.StartDate.ToString(Constants.DateTime_Comma) : string.Empty :
                                 activity.Count(a => a.InstallmentId.HasValue && a.Installment.Type == InstallmentType.Noraml && a.Installment.ProgramSchedulePart != null) == 1 ? activity.First(a => a.InstallmentId.HasValue && a.Installment.Type == InstallmentType.Noraml && a.Installment.ProgramSchedulePart != null).Installment.ProgramSchedulePart.StartDate.ToString(Constants.DateTime_Comma) : "Various",

                             EndDate = !isGroupBy ? activity.First().Installment != null && activity.First().Installment.Type == InstallmentType.Noraml && activity.First().Installment.ProgramSchedulePart != null ? activity.First().Installment.ProgramSchedulePart.EndDate.ToString(Constants.DateTime_Comma) : string.Empty :
                                 activity.Count(a => a.InstallmentId.HasValue && a.Installment.Type == InstallmentType.Noraml && a.Installment.ProgramSchedulePart != null) == 1 ? activity.First(a => a.InstallmentId.HasValue && a.Installment.Type == InstallmentType.Noraml && a.Installment.ProgramSchedulePart != null).Installment.ProgramSchedulePart.EndDate.ToString(Constants.DateTime_Comma) : "Various",
                             ProgramStartDate = transactionActivityBusiness.GetStartDateOrEndDateProgram(activity, true),
                             ProgramEndDate = transactionActivityBusiness.GetStartDateOrEndDateProgram(activity, false),
                             ProgramType = transactionActivityBusiness.GetProgramType(activity)

                         }).ToList();

            decimal amount = 0;
            foreach (var item in model)
            {
                amount = (item.TransactionCategory == TransactionCategory.Refund ||
                          item.TransactionCategory == TransactionCategory.Cancellation)
                    ? amount - item.Payment
                    : amount + item.Charge;
            }

            return model;
        }

        private List<InternalRosterReportsViewModel> CreateModelInternalRosterReport(long seasonId, int schoolId, long? programId)
        {
            var programBusiness = Ioc.ProgramBusiness;
            var programs = new List<Program>();
            if (programId != null)
            {
                programs = programBusiness.GetListBySeasonId(seasonId).Where(p => p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Id == programId).ToList();
            }
            else
            {
                programs = programBusiness.GetListBySeasonId(seasonId).Where(p => p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4).ToList();
            }
            var model = new List<InternalRosterReportsViewModel>();
            var orderItemBusiness = Ioc.OrderItemBusiness;

            model = programs != null ? programs.ToList().Select(p => new InternalRosterReportsViewModel()
            {
                ClassName = !string.IsNullOrEmpty(p.Name) ? p.Name : "-",
                GradesOrAges = !string.IsNullOrEmpty(programBusiness.GenerateGradeInfoLabelForFlyer(p)) ? programBusiness.GenerateGradeInfoLabelForFlyer(p) : !string.IsNullOrEmpty(programBusiness.GenerateAgeInfoLabel(p)) ? programBusiness.GenerateAgeInfoLabel(p) : null,
                ClassId = p.Id,
                Pending = orderItemBusiness.GetList().Where(o => o.ProgramSchedule.ProgramId == p.Id && o.ProgramScheduleId.HasValue && o.ItemStatus == OrderItemStatusCategories.initiated && o.Order.IsLive).Count(),
                WaitList = p.WaitLists.Count(w => w.IsLive == (w.Program.Season.Status == SeasonStatus.Live)),
                Enrolled = orderItemBusiness.GetList().Where(o => o.ProgramSchedule.ProgramId == p.Id && o.ProgramScheduleId.HasValue && o.ItemStatus == OrderItemStatusCategories.completed && o.Order.IsLive).Count(),
                MinimumEnrollments = (p.TypeCategory == ProgramTypeCategory.Class) && p.ProgramSchedules.Any(s => !s.IsDeleted) ? (p.ProgramSchedules.Where(s => !s.IsDeleted).First()).Attributes.MinimumEnrollment : 0,
                Capacity = (p.TypeCategory == ProgramTypeCategory.Class) && p.ProgramSchedules.Any(s => !s.IsDeleted) ? (p.ProgramSchedules.Where(s => !s.IsDeleted).First()).Attributes.Capacity : 0,
                ClassDays = programBusiness.GetClassDays(p) != null || programBusiness.GetClassDays(p).Any() ? programBusiness.FormatDays(programBusiness.GetClassDays(p).Select(pr => pr.DayOfWeek).ToList()) : "",
                StartTime = programBusiness.GetClassDays(p).FirstOrDefault().StartTime.HasValue ? DateTime.Today.Add(programBusiness.GetClassDays(p).FirstOrDefault().StartTime.Value).ToString("h:mm tt") : "",
                EndTime = programBusiness.GetClassDays(p).FirstOrDefault().EndTime.HasValue ? DateTime.Today.Add(programBusiness.GetClassDays(p).FirstOrDefault().EndTime.Value).ToString("h:mm tt") : "",
                StartDate = p.ProgramSchedules.FirstOrDefault().StartDate,
                EndDate = p.ProgramSchedules.FirstOrDefault().StartDate.AddDays(28),
                ProgramDate = string.Format("{0} - {1}", p.ProgramSchedules.FirstOrDefault().StartDate.ToString("MM/dd/yyyy"), p.ProgramSchedules.FirstOrDefault().EndDate.ToString("MM/dd/yyyy")),
                Location = p.Room,
                SchoolName = p.Season.Club.Name,
                SeasonName = p.Season.Domain,

                ProgramParticipants = GetAllParticipantForInternalRosterReport(p).Count > 0 ? GetAllParticipantForInternalRosterReport(p) : null,
                Instructor = programBusiness.GetInstractors(p),

            }).OrderBy(n => n.ClassName).ToList() : null;

            return model;
        }

        private List<InternalRosterReportsParticipantsViewModel> GetAllParticipantForInternalRosterReport(Program program)
        {
            var orderItemBusiness = Ioc.OrderItemBusiness;
            var programBusiness = Ioc.ProgramBusiness;
            var formBusiness = Ioc.FormBusiness;
            var programOrderItems = orderItemBusiness.GetList().Where(o => o.ProgramSchedule.ProgramId == program.Id && o.ProgramScheduleId.HasValue && o.ItemStatus == OrderItemStatusCategories.completed && o.Order.IsLive);
            var model = new List<InternalRosterReportsParticipantsViewModel>();
            var num = 1;

            model = programOrderItems.ToList().Select(o => new InternalRosterReportsParticipantsViewModel()
            {
                ParticipantName = string.Format("{0}, {1}", formBusiness.GetPlayerLastName(o.JbForm), formBusiness.GetPlayerFirstName(o.JbForm)),
                Parent1Name = o.JbForm != null ? formBusiness.GetParent1Name(o.JbForm) == "" ? "-" : formBusiness.GetParent1Name(o.JbForm) : "",
                AuthorizedPickup1Name = o.JbForm != null ? formBusiness.GetAuthorizedPickup1Name(o.JbForm) == "" ? "-" : formBusiness.GetAuthorizedPickup1Name(o.JbForm) : "",
                EmergencyContactName = o.JbForm != null ? formBusiness.GetEmergencyContactName(o.JbForm) == "" ? "-" : formBusiness.GetEmergencyContactName(o.JbForm) : "",
                Parent1Phone = o.JbForm != null ? formBusiness.GetParent1Phone(o.JbForm) == "" ? "-" : formBusiness.GetParent1Phone(o.JbForm) : "",
                AuthorizedPickup1Phone = o.JbForm != null ? formBusiness.GetAuthorizedPickup1Phone(o.JbForm) == "" ? "-" : formBusiness.GetAuthorizedPickup1Phone(o.JbForm) : "",
                EmergencyContactPhone = o.JbForm != null ? formBusiness.GetEmergencyContactPhone(o.JbForm) == "" ? "-" : formBusiness.GetEmergencyContactPhone(o.JbForm) : "",
                Parent1Email = o.JbForm != null ? formBusiness.GetParent1Email(o.JbForm) == "" ? "-" : formBusiness.GetParent1Email(o.JbForm) : "",
                AuthorizedPickup1Email = o.JbForm != null ? formBusiness.GetAuthorizedPickup1Email(o.JbForm) == "" ? "-" : formBusiness.GetAuthorizedPickup1Email(o.JbForm) : "",
                EmergencyContactEmail = o.JbForm != null ? formBusiness.GetEmergencyContactEmail(o.JbForm) == "" ? "-" : formBusiness.GetEmergencyContactEmail(o.JbForm) : "",
                Allergies = o.JbForm != null ? formBusiness.GetAllergies(o.JbForm) : "",
                MedicalConditions = o.JbForm != null ? formBusiness.GetMedicalConditions(o.JbForm) : "",
                TransportHome = o.JbForm != null ? formBusiness.GetDismissalFromEnrichment(o.JbForm) == "" ? "-" : formBusiness.GetDismissalFromEnrichment(o.JbForm) : "",
                GradeOrAge = formBusiness.GetPlayerGradeOrAgeAsString(o.JbForm) != null ? formBusiness.GetPlayerGradeOrAgeAsString(o.JbForm) == "Kindergarten" ? "K" : formBusiness.GetPlayerGradeOrAgeAsString(o.JbForm) : string.Empty,
            }).OrderBy(n => n.ParticipantName).ToList();
            foreach (var participant in model)
            {
                participant.Number = num;
                num++;
            }

            return model;
        }
        [HttpPost]
        public virtual JsonNetResult GetClassProfits(SeasonNames name, int year, PaginationModel paginationModel)
        {
            var club = ActiveClub;
            var currency = club.Currency;
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            var clubIds = _clubBusiness.GetSchools().Where(c => c.PartnerId == club.Id).Select(c => c.Id).ToList();
            var programs = _programBusiness.GetList(clubIds).Where(p => p.Season.Name == name && p.Season.Year == year && p.LastCreatedPage == ProgramPageStep.Step4);
            programs = programs.Where(p => (p.ProgramSchedules.SelectMany(o => o.OrderItems).Where(c => c.ItemStatus == OrderItemStatusCategories.completed && c.Order.IsLive != (p.Season.Status == SeasonStatus.Test)).Any() ?
            p.ProgramSchedules.SelectMany(o => o.OrderItems).Where(c => c.ItemStatus == OrderItemStatusCategories.completed && c.Order.IsLive != (p.Season.Status == SeasonStatus.Test)).Count() : 0) > 0).Include("ProgramSchedules.Charges").Include("ProgramSchedules.Charges.ProgramSessions");

            var counts = programs.Count();

            var allModel = GetClassProfits(programs.ToList());

            var totalParentFee = CurrencyHelper.FormatCurrencyWithPenny(allModel.Sum(a => a.ParentFee), currency, true);
            var totalGrossIncome = CurrencyHelper.FormatCurrencyWithPenny(allModel.Sum(a => a.GrossIncome), currency, true);
            var PTAFees = CurrencyHelper.FormatCurrencyWithPenny(allModel.Sum(a => a.PTAFee), currency, true);
            var totalPTAFee = CurrencyHelper.FormatCurrencyWithPenny(allModel.Sum(a => a.TotalPTAFee), currency, true);
            var totalGrossPTAFee = CurrencyHelper.FormatCurrencyWithPenny(allModel.Sum(a => a.GrossPTAFee), currency, true);
            var totalVendorFee = CurrencyHelper.FormatCurrencyWithPenny(allModel.Sum(a => a.VendorFee), currency, true);
            var totalGrossVendorFee = CurrencyHelper.FormatCurrencyWithPenny(allModel.Sum(a => a.GrossVendorFee), currency, true);
            var totalPayToVendor = CurrencyHelper.FormatCurrencyWithPenny(allModel.Sum(a => a.PaytoVendor), currency, true);
            var totalFXAProfit = CurrencyHelper.FormatCurrencyWithPenny(allModel.Sum(a => a.FXAProfit), currency, true);
            var totalLeadGen = CurrencyHelper.FormatCurrencyWithPenny(allModel.Sum(a => a.LeadGen), currency, true);
            var totalMgmtFee = CurrencyHelper.FormatCurrencyWithPenny(allModel.Sum(a => a.MgmtFee), currency, true);


            var model = allModel.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            return JsonNet(new
            {
                DataSource = model,
                TotalCount = counts,
                TotalParentFee = totalParentFee,
                TotalGrossIncome = totalGrossIncome,
                NumberPTAFees = PTAFees,
                TotalPTAFee = totalPTAFee,
                TotalGrossPTAFee = totalGrossPTAFee,
                TotalVendorFee = totalVendorFee,
                TotalGrossVendorFee = totalGrossVendorFee,
                TotalPayToVendor = totalPayToVendor,
                TotalFXAProfit = totalFXAProfit,
                TotalLeadGen = totalLeadGen,
                TotalMgmtFee = totalMgmtFee
            });
        }

        private List<ClassProfitsViewModel> GetClassProfits(List<Program> programs)
        {
            var model = new List<ClassProfitsViewModel>();

            if (programs != null || programs.Any())
            {

                if (programs != null || programs.Any())
                {
                    model = programs.OrderByDescending(p => p.Id).ToList().Select(p => new ClassProfitsViewModel()
                    {
                        SchoolName = p.Club.Name,
                        ProviderName = p.OutSourceSeasonId.HasValue ? p.OutSourceSeason.Club.Name : "",
                        ClassName = !string.IsNullOrEmpty(p.Name) ? p.Name : "-",
                        Students = _programBusiness.TotalRegistration(p, p.Season.Status == SeasonStatus.Test),
                        NumberOfWeeks = _programBusiness.GetProgramWeeksNumber(p),
                        Days = _programBusiness.GetClassDaysSeperatedByCamma(p),
                        PTAFee = getPTAFee(p),
                        VendorFee = GetVendorFee(p),
                        PercentLeadGen = getPercentLeadGen(p),
                        ParentFee = p.ProgramSchedules.Any() ? p.ProgramSchedules.First().Charges.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).First().Amount : 0,
                    }).OrderBy(n => n.ProviderName).ToList();
                }
            }

            return model;
        }
        private decimal getPTAFee(Program program)
        {
            try
            {
                decimal result = 0;

                var pTAFeeAttr = Ioc.ProgramBusiness.GetAttribute(program, AttributeName.PTAFee);
                var pTAFee = "";

                if (pTAFeeAttr != null)
                {
                    pTAFee = pTAFeeAttr.Value;
                }

                try
                {
                    result = decimal.Parse(pTAFee);
                }
                catch { }

                if (result == 0)
                {
                    var clubPtaFee = getClubPTAFee(program.Club);
                    try
                    {
                        result = decimal.Parse(clubPtaFee);
                    }
                    catch { }
                }

                return result;

            }
            catch
            {
                throw new Exception(string.Format("Error in get pate program {0}", program.Id));
            }
        }
        private decimal getPercentLeadGen(Program program)
        {
            try
            {
                decimal result = 0;

                var leadGenerationAttr = Ioc.ProgramBusiness.GetAttribute(program, AttributeName.LeadGeneration);
                var leadGeneration = "";

                if (leadGenerationAttr != null)
                {
                    leadGeneration = leadGenerationAttr.Value;
                }

                try
                {
                    result = decimal.Parse(leadGeneration);
                }
                catch { }

                if (result == 0)
                {
                    var clubleadGenerationFee = getClubLeadGenerationFee(program.Club);
                    try
                    {
                        result = decimal.Parse(clubleadGenerationFee);
                    }
                    catch { }
                }

                return result;

            }
            catch
            {
                throw new Exception(string.Format("Error in get pate program {0}", program.Id));
            }
        }

        private string getClubPTAFee(Club club)
        {
            var clubAttribute = _clubBusiness.GetAttribute(club, AttributeName.PTAFee);
            var PTAFee = "";

            if (clubAttribute != null)
            {
                PTAFee = clubAttribute.Value;
            }
            return PTAFee;
        }
        private string getClubLeadGenerationFee(Club club)
        {
            var clubAttribute = _clubBusiness.GetAttribute(club, AttributeName.LeadGeneration);
            var leadGeneration = "";

            if (clubAttribute != null)
            {
                leadGeneration = clubAttribute.Value;
            }
            return leadGeneration;
        }

        private decimal GetVendorFee(Program program)
        {
            var programBusiness = Ioc.ProgramBusiness;
            var CountWeekCatalog = program.CatalogId.HasValue ? program.Catalog.PriceOptions.Select(r => r.Title.Substring(0, 1)).ToList() : null;
            var CountWeekClass = GetWeek(program.ProgramSchedules.FirstOrDefault()); //programBusiness.GetClassDates(program.ProgramSchedules.First(), TimeOfClassFormation.Both) != null ? programBusiness.GetClassDates(program.ProgramSchedules.First(), TimeOfClassFormation.Both).ToList().Count() : 0;
            var StartTime = programBusiness.GetClassDays(program).FirstOrDefault(pr => pr.StartTime.HasValue).StartTime.Value;
            var EndTime = programBusiness.GetClassDays(program).FirstOrDefault(pr => pr.EndTime.HasValue).EndTime.Value;

            TimeSpan span = EndTime - StartTime;
            double totalMinutes = span.TotalMinutes;

            if (CountWeekCatalog != null)
            {
                var count = program.CatalogId.HasValue ? program.Catalog.PriceOptions.Where(r => !string.IsNullOrEmpty(r.DurationTitle) && r.DurationTitle.Contains(totalMinutes.ToString()) && r.Title.Contains(CountWeekClass.ToString())) != null ? program.Catalog.PriceOptions.Where(r => !string.IsNullOrEmpty(r.DurationTitle) && r.DurationTitle.Contains(totalMinutes.ToString()) && r.Title.Contains(CountWeekClass.ToString())).Count() : 0 : 0;
                var amount = count > 0 ? program.Catalog.PriceOptions.Where(r => !string.IsNullOrEmpty(r.DurationTitle) && r.DurationTitle.Contains(totalMinutes.ToString()) && r.Title.Contains(CountWeekClass.ToString())).FirstOrDefault().Amount : 0;
                return amount;
            }
            else { return 0; }
        }

        private int GetWeek(ProgramSchedule programSchedule)
        {
            var StartDate = programSchedule.StartDate;
            var EndDate = programSchedule.EndDate;
            var allDates = new List<DateTime>();
            var sessions = Ioc.ProgramBusiness.GetClassDates(programSchedule, TimeOfClassFormation.Both);
            var CountSession = sessions != null ? sessions.Count() : 0;

            var listDates = new List<DateTime>();

            if (sessions != null)
            {
                listDates.AddRange(sessions.Select(s => s.SessionDate));
            }

            allDates = GetClassHolidayDates(programSchedule.Program.Club, listDates);


            return allDates != null ? allDates.Count() : 0;
        }

        public List<DateTime> GetClassHolidayDates(Club club, List<DateTime> classdates)
        {
            List<string> holidayDates = club.Setting.ListOfHolidayDates;
            List<string> holidayAMDates = club.Setting.ListOfHolidayAMDates;
            List<string> holidayPMDates = club.Setting.ListOfHolidayPMDates;

            if (classdates != null && classdates.Any())
            {
                classdates = classdates.OrderBy(c => c).ToList();

                if (holidayDates != null && holidayDates.Any())
                {
                    classdates.RemoveAll(c => holidayDates.Where(d => d != string.Empty).Select(d => DateTime.Parse(d)).ToList().Contains(c.Date));
                }

                if (holidayAMDates != null && holidayAMDates.Any())
                {
                    classdates.RemoveAll(c => holidayAMDates.Where(d => d != string.Empty).Select(d => DateTime.Parse(d)).ToList().Contains(c.Date));
                }

                if (holidayPMDates != null && holidayPMDates.Any())
                {
                    classdates.RemoveAll(c => holidayPMDates.Where(d => d != string.Empty).Select(d => DateTime.Parse(d)).ToList().Contains(c.Date));
                }
            }

            return classdates;
        }

        [HttpPost]
        public virtual JsonNetResult GetAllReports()
        {
            var pageSize = Request.Params["PageSize"] != null ? int.Parse(Request.Params["PageSize"]) : 10;
            var page = Request.Params["Page"] != null ? int.Parse(Request.Params["Page"]) : 1;

            var club = _clubBusiness.Get(ActiveClub.Id);

            var reports = club.IsPartner ? Ioc.ReportBusiness.GetReports(ActiveClub.Domain) : Ioc.ReportBusiness.GetReports(ActiveClub.Domain).Where(r => !r.SeasonId.HasValue);

            var dataSource = reports.Where(r => r.EventType == EventRoasterType.Custom).Select(m => new { Title = m.Name.Replace("'", "&apos;"), Value = m.Id, Type = m.CustomType }).OrderByDescending(m => m.Value).Skip((page - 1) * pageSize).Take(pageSize).ToList();

            return JsonNet(new { DataSource = dataSource, TotalCount = reports.Count() });
        }
        public virtual JsonResult CreateEditCustomReport(int? id, CustomReportType? customType)
        {
            var club = ActiveClub;

            if (!id.HasValue)
            {
                var model = new CustomReportPortalProgramViewModel(club.Id, customType.Value);

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var reportTemplate = Ioc.ReportBusiness.GetReport(id.Value);

                //UpdateMoreInfoSection(reportTemplate.JbForm, club.Id);
                var model = new ProgramReportViewModel
                {
                    Description = reportTemplate.Description,
                    Id = reportTemplate.Id,
                    JbForm = reportTemplate.JbForm,
                    Name = reportTemplate.Name,
                    SeasonId = reportTemplate.SeasonId,
                    HasHeader = reportTemplate.HasHeader,
                    ReportType = reportTemplate.EventType,
                    CustomReportType = reportTemplate.CustomType,

                    FollowupForms = reportTemplate.FollowupForms.Select(f =>
                            new ProgramReportFollowupItemViewModel
                            {
                                JbForm = f,
                                Title = f.Title,
                                Id = f.Id,
                                IsSelected = true
                            })
                            .ToList()
                };

                var clubFollowupForms = Ioc.ReportBusiness.GetFolloupFormReports(ActiveClub.Id);

                var reportBusiness = Ioc.ReportBusiness;

                foreach (var item in clubFollowupForms.Where(f => !model.FollowupForms.Any(o => o.Title.Equals(f.Title))))
                {
                    item.Id = 0;

                    model.FollowupForms.Add(new ProgramReportFollowupItemViewModel
                    {
                        JbForm = reportBusiness.GenerateReportFollowupForm(item),
                        Title = item.Title,
                        Id = 0,
                        IsSelected = false
                    });
                }

                if (model.CustomReportType == CustomReportType.Parent)
                {
                    model.FollowupForms = null;
                }

                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public virtual ActionResult CreateEditCustomReport(CustomReportPortalProgramViewModel model)
        {
            if (model.JbForm.Elements.All(e => !((JbSection)e).Elements.Any(i => ((JbCheckBox)i).Value)))
            {
                ModelState.AddModelError("model.NoSelection", "Please check one or more option(s) in report.");
            }

            ModelState.Remove("model.JbForm.CreatedDate");
            ModelState.Remove("model.JbForm.LastModifiedDate");

            for (int i = 0; i < model.FollowupForms.Count; i++)
            {
                ModelState.Remove(string.Format("model.FollowupForms[{0}].JbForm.CreatedDate", i));
                ModelState.Remove(string.Format("model.FollowupForms[{0}].JbForm.LastModifiedDate", i));
            }

            if (ModelState.IsValid)
            {
                var reporttemp = new EventRoaster
                {
                    Name = model.Name,
                    Description = model.Description,
                    JbForm_Id = model.JbForm.Id,
                    JbForm = model.JbForm,
                    SeasonId = model.SeasonId,
                    ClubDomain = ActiveClub.Domain,
                    LastUpdateDate = DateTime.Now,
                    HasHeader = model.HasHeader,
                    CustomType = model.CustomReportType
                };

                reporttemp.JbForm.JsonElements = JsonHelper.JsonSerializer(model.JbForm.Elements);

                foreach (var item in model.FollowupForms.Where(f => f.IsSelected))
                {
                    item.JbForm.JsonElements = JsonHelper.JsonSerializer(item.JbForm.Elements);

                    item.JbForm.RefEntityName = "Reports";
                    item.JbForm.Title = item.Title;
                }

                reporttemp.JbForm.RefEntityName = "Reports";

                reporttemp.FollowupForms = model.FollowupForms.Where(f => f.IsSelected).Select(f => f.JbForm).ToList();

                if (model.Id == 0)
                {
                    reporttemp.JbForm.CreatedDate = DateTime.UtcNow;
                    Ioc.ReportBusiness.Create(reporttemp);
                    model.Id = reporttemp.Id;
                }
                else
                {
                    reporttemp.JbForm.LastModifiedDate = DateTime.UtcNow;
                    reporttemp.Id = model.Id;
                    reporttemp.EventType = model.ReportType;
                    Ioc.ReportBusiness.Update(reporttemp);
                    Ioc.JbFormBusiness.CreateEdit(reporttemp.JbForm);
                    model.Id = reporttemp.Id;

                }

                return JsonNet(new JResult { Data = model.Id.ToString(), Status = true });
            }

            return JsonFormResponse();
        }

        [HttpPost]
        public virtual ActionResult RunReportCustomReport(CustomReportPortalModel model, bool makeCampaign = false, bool isExportExcel = false)
        {
            var club = _clubBusiness.Get(ActiveClub.Domain);

            var pageSize = (Request.Params["PageSize"] != null) ? int.Parse(Request.Params["PageSize"]) : 100000;
            var page = (Request.Params["Page"] != null) ? int.Parse(Request.Params["Page"]) : 1;

            var filterable = (model.Filters != null) &&
                             (model.Filters.RegistrationStart.HasValue || model.Filters.RegistrationEnd.HasValue ||
                              model.Filters.Status.HasValue || model.Filters.Tuition.HasValue || model.Filters._SelectedPrograms != null);

            if (club.IsPartner)
            {
                if (model.IsAllPrograms)
                {
                    var clubs = new List<Club>();
                    if (model.ClubType == "school")
                    {
                        if (model.IsAllClubs == "true")
                        {
                            var partnerId = ActiveClub.Id;
                            clubs = _clubBusiness.GetAllPartnerSchools(partnerId).ToList();
                        }
                        else
                        {
                            var schoolIds = model._SelectedClubs.ToList();
                            clubs = _clubBusiness.GetList().Where(c => schoolIds.Contains(c.Id)).ToList();
                        }
                    }
                    else if (model.ClubType == "provider")
                    {
                        if (model.IsAllClubs == "true")
                        {
                            var partnerId = ActiveClub.Id;
                            clubs = _clubBusiness.GetAllPartnerProviders(partnerId).ToList();
                        }
                        else
                        {
                            var providerIds = model._SelectedClubs.ToList();
                            clubs = _clubBusiness.GetList().Where(c => providerIds.Contains(c.Id)).ToList();
                        }
                    }
                    else if (model.ClubType == "district")
                    {
                        if (model.IsAllClubs == "true")
                        {
                            var partnerId = ActiveClub.Id;
                            var districtIds = _clubBusiness.GetList()
                                .Where(c => c.ClubType.EnumType == ClubTypesEnum.SchoolDistrict &&
                                            c.PartnerId == partnerId && !c.IsDeleted)
                                .Select(c => c.Id)
                                .ToList();

                            clubs = _clubBusiness.GetList().Where(c => districtIds.Contains(c.DistrictId.Value))
                                .ToList();

                        }
                        else
                        {
                            var districtIds = model._SelectedClubs.ToList();
                            clubs = _clubBusiness.GetList().Where(c => districtIds.Contains(c.DistrictId.Value))
                                .ToList();
                        }
                    }

                    var clubIds = clubs.Select(c => c.Id);
                    var programs = Ioc.ProgramBusiness.GetList().Where(p =>
                        clubIds.Contains(p.ClubId) && p.Status != ProgramStatus.Deleted &&
                        p.LastCreatedPage >= ProgramPageStep.Step4 && p.ProgramSchedules.Any());

                    if (model.SeasonName != SeasonNames.SelectSeason)
                    {
                        programs = programs.Where(p => p.Season.Name == model.SeasonName);
                    }

                    if (model.Year != -1)
                    {
                        programs = programs.Where(p => p.Season.Year == model.Year);
                    }

                    model._SelectedPrograms = programs.Select(p => p.Id).ToList();

                }
            }
            else
            {
                var programs = Ioc.ProgramBusiness.GetList().Where(p =>
                    p.ClubId == club.Id && p.Status != ProgramStatus.Deleted &&
                    p.LastCreatedPage >= ProgramPageStep.Step4 && p.ProgramSchedules.Any());

                if (model.SeasonName != SeasonNames.SelectSeason)
                {
                    programs = programs.Where(p => p.Season.Name == model.SeasonName);
                }

                if (model.Year != -1)
                {
                    programs = programs.Where(p => p.Season.Year == model.Year);
                }

                model._SelectedPrograms = programs.Select(p => p.Id).ToList();
            }

            var eventRoaster = Ioc.ReportBusiness.GetReport(model.ReportId);

            var allOrderItems = Ioc.OrderItemBusiness
                .GetReportOrderItems(model._SelectedPrograms, OrderItemStatusCategories.showAll)
                .Where(o => o.Order.IsLive)
                .Include("Order");

            allOrderItems = filterable
                ? ApplyBaseFilters(model.Filters, allOrderItems)
                : allOrderItems.Where(o => o.ItemStatus == OrderItemStatusCategories.completed);

            var orderItemPaging = allOrderItems.OrderBy(o => o.Id)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .Include("ProgramSchedule.Program")
                .Include("OrderChargeDiscounts")
                .Include("Order.User")
                .Include("Order.Club")
                .ToList();


            if (makeCampaign)
            {
                var recipients =
                    allOrderItems.Select(o => new ReportRecipient
                    {
                        Email = o.Order.User.UserName,
                        Lastname = o.LastName,
                        //FirstName = o.FirstName
                    })
                    .Distinct()
                    .ToList();

                var result = MakeCampaign(recipients, club.Domain, model.ReportName);

                return Json(new { result.Status, Data = result.RecordsAffected, Message = result.ExceptionMessage });
            }

            var finalData = new List<Dictionary<string, object>>();
            var titles = new List<string>();

            switch (eventRoaster.CustomType)
            {
                case CustomReportType.Parent:
                    {
                        finalData = Ioc.CustomReportBusiness.GetCustomReportParentInfo(eventRoaster, orderItemPaging, ref titles, true);
                        break;
                    }
            }

            if (filterable)
            {
                page = 1;
            }

            if (isExportExcel)
            {
                var rows = new List<List<string>>();

                foreach (var row in finalData)
                {
                    rows.Add(row.Select(r => r.Value != null ? r.Value.ToString() : string.Empty).ToList());
                }

                var newTitles = new List<string>();

                for (int i = 0; i < finalData[0].Count; i++)
                {
                    newTitles.Add(titles.ElementAt(i));
                }

                newTitles = newTitles.Select(t => t.Replace("_", " ").Replace("sDash", " ")).ToList();

                var excelModel = new ReportExportModel
                {
                    Columns = newTitles,
                    Rows = rows,
                    ClubName = ActiveClub.Name,
                    ReportName = eventRoaster.Name
                };
                var file = _reportBusiness.CreateExcelExport(excelModel);

                return File(file, FileTypes.ExcelXlsxMimeType);
            }

            var dataSource = finalData;
            return JsonNet(new { DataSource = dataSource, TotalCount = allOrderItems.Count(), Titles = titles });
        }

        public virtual JsonNetResult GetAllClubs()
        {
            var clubId = ActiveClub.Id;

            var allClubs = new List<SelectKeyValue<long>>();

            allClubs =
              _clubBusiness.GetRelatedClubs(clubId)
                  .Where(c => c.ClubType.ParentId.Value == (int)ClubTypesEnum.Provider || c.ClubType.ParentId.Value == (int)ClubTypesEnum.School)
                  .Select(c => new SelectKeyValue<long> { Text = c.Name, Value = c.Id })
                  .OrderBy(c => c.Text).ToList();


            var model = new { AllClubs = allClubs };

            return JsonNet(model);
        }
        public virtual JsonNetResult GetAllCampClubs(long seasonId)
        {
            var programs = Ioc.ProgramBusiness.GetListBySeasonId(seasonId).Where(p => p.Status != ProgramStatus.Deleted && p.LastCreatedPage >= ProgramPageStep.Step4 && p.TypeCategory == ProgramTypeCategory.Camp);

            var Allprograms = programs.Select(p => new SelectKeyValue<long>() { Text = p.Name, Value = p.Id })
                    .OrderBy(s => s.Text).ToList();
            return JsonNet(Allprograms);
        }

        public virtual JsonNetResult GetCustomReportHeader(int reportId, long? scheduleId)
        {
            var model = new object();

            if (scheduleId.HasValue)
            {
                model = getReportHeaderBySchedule(reportId, scheduleId.Value);
            }
            else
            {
                model = getReportHeader(reportId);
            }

            return JsonNet(model);

        }
        private object getReportHeader(int reportId)
        {
            object result = null;
            var report = Ioc.ReportBusiness.GetReport(reportId);

            result = new
            {
                ReportName = report.Name,
                Date = _clubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:MM tt")
            };

            return result;
        }

        private object getReportHeader(int reportId, long programId)
        {
            object result = null;

            var programBusiness = Ioc.ProgramBusiness;

            var program = programBusiness.Get(programId);

            var report = Ioc.ReportBusiness.GetReport(reportId);

            if (report.HasHeader)
            {
                result = new
                {
                    ProgramName = program.Name,
                    RoomAssignment = program.Room ?? string.Empty,
                    Days = string.Join(", ", programBusiness.GetClassDays(program).Select(d => d.DayOfWeek).ToList()),
                    Date = _clubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:MM tt")
                };
            }

            return result;
        }

        private object getReportHeaderBySchedule(int reportId, long scheduleId)
        {
            object result = null;

            var programBusiness = Ioc.ProgramBusiness;

            var program = programBusiness.GetByScheduleId(scheduleId);

            var report = Ioc.ReportBusiness.GetReport(reportId);

            result = new
            {
                ProgramName = program.Name,
                RoomAssignment = program.Room ?? string.Empty,
                Days = string.Join(", ", programBusiness.GetClassDays(program).Select(d => d.DayOfWeek).ToList()),
                Date = _clubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:MM tt")
            };

            return result;
        }

        private static IQueryable<OrderItem> ApplyBaseFilters(ReportFilters filters, IQueryable<OrderItem> filteredOrderItems)
        {
            var isInFilter = false;

            if (filters._SelectedPrograms != null)
            {
                filteredOrderItems = Ioc.OrderItemBusiness.GetReportOrderItems(filters._SelectedPrograms, OrderItemStatusCategories.showAll);
            }

            if (filters.RegistrationStart.HasValue)
            {
                if (filters.RegistrationEnd.HasValue)
                {
                    var startDate = filters.RegistrationStart.Value;
                    var endDate = filters.RegistrationEnd.Value;
                    filteredOrderItems =
                        filteredOrderItems
                            .Where(
                                item =>
                                    DbFunctions.TruncateTime(item.Order.CompleteDate) >=
                                    startDate &&
                                    DbFunctions.TruncateTime(item.Order.CompleteDate) <=
                                    endDate);

                }
                else
                {
                    filteredOrderItems =
                        filteredOrderItems.Where(
                                item =>
                                    DbFunctions.TruncateTime(item.Order.CompleteDate) >=
                                    filters.RegistrationStart.Value);

                }

                isInFilter = true;
            }
            else if (filters.RegistrationEnd.HasValue)
            {
                filteredOrderItems =
                    filteredOrderItems
                        .Where(
                            item =>
                                DbFunctions.TruncateTime(item.Order.CompleteDate) <=
                                filters.RegistrationEnd.Value);

            }

            if (filters.Tuition.HasValue && filteredOrderItems.Any())
            {
                filteredOrderItems =
                    filteredOrderItems.SelectMany(
                        orderItem =>
                            orderItem.OrderChargeDiscounts.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee),
                        (orderItem, charge) => new { orderItem, charge })
                        .Where(t => t.charge.ChargeId == filters.Tuition.Value)
                        .Select(t => t.orderItem);
            }
            else if (filters.Tuition.HasValue && !isInFilter)
            {
                filteredOrderItems =
                    filteredOrderItems.SelectMany(
                        orderItem =>
                            orderItem.OrderChargeDiscounts.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee),
                        (orderItem, charge) => new { orderItem, charge })
                        .Where(t => t.charge.ChargeId == filters.Tuition.Value)
                        .Select(t => t.orderItem);
                isInFilter = true;
            }

            if (filters.Status.HasValue)
            {
                if (filters.Status.Value == OrderItemStatusReasons.regular)
                {
                    filteredOrderItems =
                        filteredOrderItems.Where(
                            item =>
                                item.ItemStatus == OrderItemStatusCategories.completed);

                }
                else if (filters.Status.Value == OrderItemStatusReasons.showAll)
                {
                    filteredOrderItems =
                        filteredOrderItems.Where(
                            item =>
                                item.ItemStatus == OrderItemStatusCategories.completed ||
                                item.ItemStatus == OrderItemStatusCategories.changed);

                }
                else
                {
                    filteredOrderItems =
                    filteredOrderItems.Where(
                            item =>
                                item.ItemStatusReason == filters.Status)
                        .Select(p => p);
                }
            }
            else if (filters.Status.HasValue && !isInFilter)
            {
                if (filters.Status.Value == OrderItemStatusReasons.regular)
                {
                    filteredOrderItems =
                        filteredOrderItems.Where(
                            n => n.ItemStatus == OrderItemStatusCategories.completed);
                }
                else if (filters.Status.Value == OrderItemStatusReasons.showAll)
                {
                    filteredOrderItems =
                        filteredOrderItems.Where(
                            item =>
                                item.ItemStatus == OrderItemStatusCategories.completed ||
                                item.ItemStatus == OrderItemStatusCategories.changed);
                }
                else
                {
                    filteredOrderItems =
                        filteredOrderItems.Where(
                            n => n.ItemStatusReason == filters.Status);
                }
            }

            return filteredOrderItems;
        }

        private static OperationStatus MakeCampaign(IEnumerable<ReportRecipient> recipients, string clubDomain, string reportName)
        {
            var result = Ioc.CampaignBusiness.MakeCampaignOnReport(recipients.DistinctBy(c => c.Email).ToList(), clubDomain, reportName);
            return result;
        }

        private IEnumerable<RunProgramReportViewModel> GetProgramReportModel(EventRoaster eventRoaster, IEnumerable<OrderItem> allOrderItems)
        {
            var reportmodel = new List<RunProgramReportViewModel>();
            var ordersList = new List<List<OrderItem>>();



            foreach (var group in allOrderItems.GroupBy(o => o.ProgramSchedule.ProgramId))
            {
                var orderItems = group.ToList();
                var program = Ioc.ProgramBusiness.Get(group.Key);
                var programName = program.Name;

                if (orderItems.Any())
                {
                    ordersList.Add(orderItems);
                }
                var roomNumber = program.Room;
                var instructor = string.Empty;
                var dayOfWeek = string.Join(", ", Ioc.ProgramBusiness.GetClassDays(program).Select(d => d.DayOfWeek).ToList());
                if (program.Instructors != null && program.Instructors.Any(c => c.Contact != null))
                {
                    instructor = string.Join(", ", program.Instructors.Select(c => string.Format(Constants.F_FullName, c.Contact.FirstName, c.Contact.LastName)));
                }
                var schoolName = program.Club.Name;

                if (ordersList.Any())
                {
                    reportmodel.Add(new RunProgramReportViewModel(programName, eventRoaster, ordersList, instructor, roomNumber, dayOfWeek, schoolName));
                }

                ordersList.Clear();
            }

            return reportmodel;
        }

        public virtual JsonNetResult FillBaseFilters(CustomReportPortalModel model, string reportType = "Admin")
        {
            var club = ActiveClub;

            if (model.IsAllPrograms)
            {
                var clubs = new List<Club>();
                if (model.ClubType == "school")
                {
                    if (model.IsAllClubs == "true")
                    {
                        var partnerId = ActiveClub.Id;
                        clubs = _clubBusiness.GetAllPartnerSchools(partnerId).ToList();
                    }
                    else
                    {
                        var schoolIds = model._SelectedClubs.ToList();
                        clubs = _clubBusiness.GetList().Where(c => schoolIds.Contains(c.Id)).ToList();
                    }
                }
                else if (model.ClubType == "provider")
                {
                    if (model.IsAllClubs == "true")
                    {
                        var partnerId = ActiveClub.Id;
                        clubs = _clubBusiness.GetAllPartnerProviders(partnerId).ToList();
                    }
                    else
                    {
                        var providerIds = model._SelectedClubs.ToList();
                        clubs = _clubBusiness.GetList().Where(c => providerIds.Contains(c.Id)).ToList();
                    }
                }
                else if (model.ClubType == "district")
                {
                    if (model.IsAllClubs == "true")
                    {
                        var partnerId = ActiveClub.Id;
                        var districtIds = _clubBusiness.GetList()
                            .Where(c => c.ClubType.EnumType == ClubTypesEnum.SchoolDistrict && c.PartnerId == partnerId && !c.IsDeleted)
                            .Select(c => c.Id)
                            .ToList();

                        clubs = _clubBusiness.GetList().Where(c => districtIds.Contains(c.DistrictId.Value)).ToList();
                    }
                    else
                    {
                        var districtIds = model._SelectedClubs.ToList();
                        clubs = _clubBusiness.GetList().Where(c => districtIds.Contains(c.DistrictId.Value)).ToList();
                    }
                }

                var clubIds = clubs.Select(c => c.Id);
                var programs = Ioc.ProgramBusiness.GetList().Where(p => clubIds.Contains(p.ClubId) && p.Status != ProgramStatus.Deleted && p.LastCreatedPage >= ProgramPageStep.Step4 && p.ProgramSchedules.FirstOrDefault() != null);

                if (model.SeasonName != SeasonNames.SelectSeason)
                {
                    programs = programs.Where(p => p.Season.Name == model.SeasonName);
                }

                if (model.Year != -1)
                {
                    programs = programs.Where(p => p.Season.Year == model.Year);
                }

                model._SelectedPrograms = programs.Select(p => p.Id).ToList();

            }

            if (!model.IsAllPrograms && reportType == "Parent")
            {
                var programBusiness = Ioc.ProgramBusiness;
                for (int i = 0; i < model._SelectedPrograms.Count; i++)
                {
                    var schedule = model._SelectedPrograms[i];
                    model._SelectedPrograms[i] = Ioc.ProgramBusiness.GetByScheduleId(schedule).Id;
                }
                model._SelectedPrograms = model._SelectedPrograms.Distinct().ToList();
            }

            var tuitionsList = new List<SelectKeyValue<string>>();
            var orderStatusList = new List<SelectKeyValue<string>>();
            Program programDb;

            tuitionsList.Add(new SelectKeyValue<string>
            {
                Text = "All",
                Value = ""
            });
            if (model._SelectedPrograms != null)
            {
                foreach (var programId in model._SelectedPrograms)
                {
                    programDb = Ioc.ProgramBusiness.Get(programId);

                    var tuitions =
                        programDb.ProgramSchedules.Where(s => !s.IsDeleted).SelectMany(
                            programschedule =>
                                programschedule.Charges.Where(
                                    c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee));

                    tuitionsList.AddRange(
                        tuitions.Select(t => new SelectKeyValue<string>
                        {
                            Text = string.Format("{0}", _programBusiness.GetProgramNameCustomReport(t.ProgramSchedule, t.Name)),
                            Value = t.Id.ToString()
                        }));
                }
            }

            orderStatusList.AddRange(
                Enum.GetValues(typeof(OrderItemStatusReasons))
                    .Cast<OrderItemStatusReasons>()
                    .Select(s => new SelectKeyValue<string>
                    {
                        Text = s.ToDescription(),
                        Value = s.ToString()
                    }));

            var allStatus = orderStatusList.Single(o => o.Text == "All");
            orderStatusList.RemoveAll(o => o.Text == "All");
            orderStatusList.Insert(0, allStatus);

            return JsonNet(new { Tuitions = tuitionsList, OrderStatus = orderStatusList });
        }

        [HttpPost]
        public virtual ActionResult ShareReport(ReportType type, ShareAsAttachment model, CustomReportPortalModel selectedModel, string term, string automaticType)
        {
            var club = _clubBusiness.Get(ActiveClub.Domain);

            var expsch = new ExportDocumnetSchema();

            var exp = new ExportFileHelper();

            const string fileNameFormat = "{0}({1}_{2}){3}.xlsx";

            var fileName = string.Empty;

            var titles = new List<string>();

            if (club.IsPartner)
            {
                if (selectedModel.IsAllPrograms)
                {
                    var clubs = new List<Club>();
                    if (selectedModel.ClubType == "school")
                    {
                        if (selectedModel.IsAllClubs == "true")
                        {
                            var partnerId = ActiveClub.Id;
                            clubs = _clubBusiness.GetAllPartnerSchools(partnerId).ToList();
                        }
                        else
                        {
                            var schoolIds = selectedModel._SelectedClubs.ToList();
                            clubs = _clubBusiness.GetList().Where(c => schoolIds.Contains(c.Id)).ToList();
                        }
                    }
                    else if (selectedModel.ClubType == "district")
                    {
                        if (selectedModel.IsAllClubs == "true")
                        {
                            var partnerId = ActiveClub.Id;
                            var districtIds = _clubBusiness.GetList()
                                .Where(c => c.ClubType.EnumType == ClubTypesEnum.SchoolDistrict &&
                                            c.PartnerId == partnerId && !c.IsDeleted)
                                .Select(c => c.Id)
                                .ToList();

                            clubs = _clubBusiness.GetList().Where(c => districtIds.Contains(c.DistrictId.Value))
                                .ToList();
                        }
                        else
                        {
                            var districtIds = selectedModel._SelectedClubs.ToList();
                            clubs = _clubBusiness.GetList().Where(c => districtIds.Contains(c.DistrictId.Value))
                                .ToList();
                        }
                    }

                    var clubIds = clubs.Select(c => c.Id);
                    var programs = Ioc.ProgramBusiness.GetList().Where(p =>
                        clubIds.Contains(p.ClubId) && p.Status != ProgramStatus.Deleted &&
                        p.LastCreatedPage >= ProgramPageStep.Step4 && p.ProgramSchedules.FirstOrDefault() != null);

                    if (selectedModel.SeasonName != SeasonNames.SelectSeason)
                    {
                        programs = programs.Where(p => p.Season.Name == selectedModel.SeasonName);
                    }

                    if (selectedModel.Year != -1)
                    {
                        programs = programs.Where(p => p.Season.Year == selectedModel.Year);
                    }

                    selectedModel._SelectedPrograms = programs.Select(p => p.Id).ToList();

                }
            }
            else
            {
                var programs = Ioc.ProgramBusiness.GetList().Where(p =>
                    p.ClubId == club.Id && p.Status != ProgramStatus.Deleted &&
                    p.LastCreatedPage >= ProgramPageStep.Step4 && p.ProgramSchedules.Any());

                if (selectedModel.SeasonName != SeasonNames.SelectSeason)
                {
                    programs = programs.Where(p => p.Season.Name == selectedModel.SeasonName);
                }

                if (selectedModel.Year != -1)
                {
                    programs = programs.Where(p => p.Season.Year == selectedModel.Year);
                }

                selectedModel._SelectedPrograms = programs.Select(p => p.Id).ToList();
            }

            switch (type)
            {
                case ReportType.CustomParentPortal:
                    {
                        var isFilterable = false;
                        if (selectedModel.Filters != null)
                        {
                            isFilterable = selectedModel.Filters.RegistrationStart.HasValue ||
                                           selectedModel.Filters.RegistrationEnd.HasValue ||
                                           selectedModel.Filters.Tuition.HasValue ||
                                           selectedModel.Filters.Status.HasValue;
                        }
                        var eventRoaster = Ioc.ReportBusiness.GetReport(selectedModel.ReportId);

                        fileName = string.Format(fileNameFormat, club.Name,
                            eventRoaster.Name.Replace(" ", "") +
                            DateTimeHelper.ConvertUtcDateTimeToLocal(DateTime.UtcNow, ActiveClub.TimeZone)
                                .ToString("MMM dd, yyyy"), selectedModel.SeasonName,
                            RandomHelper.GenerateGuid());

                        var allOrderItems = Ioc.OrderItemBusiness.GetReportOrderItems(selectedModel._SelectedPrograms, OrderItemStatusCategories.showAll);

                        allOrderItems = isFilterable
                            ? ApplyBaseFilters(selectedModel.Filters, allOrderItems)
                            : allOrderItems;

                        var orderItems = allOrderItems.OrderBy(o => o.Id).ToList();

                        var data = Ioc.CustomReportBusiness.GetCustomReportParentInfo(eventRoaster, orderItems, ref titles, true, true);

                        expsch = Ioc.CustomReportBusiness.CustomReportToExcel(data);
                        break;
                    }
            }

            // Save in blob.
            MemoryStream output;

            switch (type)
            {
                case ReportType.CustomParentPortal:
                    {
                        output = exp.ExportExcelDicData(expsch);
                        break;
                    }
                case ReportType.ChargeDiscount:
                    {
                        output = exp.ExportExcelDicData(expsch);
                        break;
                    }
                default:
                    {
                        output = exp.ExportExcel(expsch);
                        break;
                    }
            }

            var file = File(output.ToArray(), FileTypes.ExcelXlsxMimeType);
            
            using (var stream = new MemoryStream(file.FileContents))
            {
                var sm = new StorageManager(WebConfigHelper.AzureStorageConnection,
                    Constants.Path_ClubFilesContainer);
                sm.UploadBlob(club.Domain, fileName, stream, "application/xlsx");
            }

            var reportLink = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, fileName).AbsoluteUri;

            var templateModel = new EmailTemplateModel
            {
                HasLogo = true,
                HasHeader = true,
                ClubLogo = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo),
                Body = string.Format("{0} <br/><hr/> <a href=" + reportLink + "> Download Report </a>",
                    model.Message),
                IsRegisterationEmail = true
            };

            var body = this.RenderViewToString(Constants.Path_EmailTemplate, templateModel);

            Ioc.OldEmailBusiness.SendEmail(club.ContactPersons.FirstOrDefault().Email, model.SentTo, model.Subject,
                body, true);

            return JsonNet(new JResult { Status = true });
        }

        public virtual ActionResult DeleteReport(int reportId)
        {
            var result = Ioc.ReportBusiness.Delete(reportId);
            return JsonNet(new JResult { Status = result.Status, Data = result.ExceptionMessage });
        }

        public JsonNetResult GetAllPartnerSchoolDistricts()
        {
            var partnerId = ActiveClub.Id;
            var schoolDistrict = _clubBusiness.GetList()
                .Where(c => c.ClubType.EnumType == ClubTypesEnum.SchoolDistrict && c.PartnerId == partnerId && !c.IsDeleted)
                .Select(c => new SelectKeyValue<string> { Text = c.Name, Value = c.Id.ToString() })
                .OrderBy(c => c.Text)
                .ToList();

            var model = new { AllDisctricts = schoolDistrict, IsAllDistricts = "true" };
            return JsonNet(model);
        }

        [HttpPost]
        public virtual JsonNetResult GetAllSeasonProgramsCustomReport(CustomReportPortalModel model)
        {
            var club = ActiveClub;
            var customReportBusiness = Ioc.CustomReportBusiness;

            IQueryable<Program> programs = null;

            if (model.IsAllPrograms)
            {
                var clubs = new List<Club>();
                if (model.ClubType == "school")
                {
                    if (model.IsAllClubs == "true")
                    {
                        var partnerId = ActiveClub.Id;
                        clubs = _clubBusiness.GetAllPartnerSchools(partnerId).ToList();
                    }
                    else
                    {
                        var schoolIds = model._SelectedClubs.ToList();
                        clubs = _clubBusiness.GetList().Where(c => schoolIds.Contains(c.Id)).ToList();
                    }
                }
                else if (model.ClubType == "provider")
                {
                    if (model.IsAllClubs == "true")
                    {
                        var partnerId = ActiveClub.Id;
                        clubs = _clubBusiness.GetAllPartnerProviders(partnerId).ToList();
                    }
                    else
                    {
                        var providerIds = model._SelectedClubs.ToList();
                        clubs = _clubBusiness.GetList().Where(c => providerIds.Contains(c.Id)).ToList();
                    }
                }
                else if (model.ClubType == "district")
                {
                    if (model.IsAllClubs == "true")
                    {
                        var partnerId = ActiveClub.Id;
                        var districtIds = _clubBusiness.GetList()
                            .Where(c => c.ClubType.EnumType == ClubTypesEnum.SchoolDistrict && c.PartnerId == partnerId && !c.IsDeleted)
                            .Select(c => c.Id)
                            .ToList();

                        clubs = _clubBusiness.GetList().Where(c => districtIds.Contains(c.DistrictId.Value)).ToList();
                    }
                    else
                    {
                        var districtIds = model._SelectedClubs.ToList();
                        clubs = _clubBusiness.GetList().Where(c => districtIds.Contains(c.DistrictId.Value)).ToList();
                    }
                }

                var clubIds = clubs.Select(c => c.Id);
                programs = Ioc.ProgramBusiness.GetList().Where(p => clubIds.Contains(p.ClubId) && p.Status != ProgramStatus.Deleted && p.LastCreatedPage >= ProgramPageStep.Step4 && p.ProgramSchedules.FirstOrDefault() != null);

                if (model.SeasonName != SeasonNames.SelectSeason)
                {
                    programs = programs.Where(p => p.Season.Name == model.SeasonName);
                }

                if (model.Year != -1)
                {
                    programs = programs.Where(p => p.Season.Year == model.Year);
                }

            }

            var allPrograms = new List<SelectKeyValue<object>>();

            foreach (var program in programs)
            {
                allPrograms.Add(new SelectKeyValue<object>()
                {
                    Text = string.Format("{0} ({1})", program.Name, program.Club.Name),
                    Value = program.Id
                });
            }

            allPrograms = allPrograms.OrderBy(a => a.Text).ToList();

            var result = new { AllPrograms = allPrograms, IsAllPrograms = "true" };

            return JsonNet(result);
        }

        public ActionResult GetStripeErrorMessage()
        {
            var result = _orderInstallmentBusiness.GetStripeErrorMessage();

            result.AddItemToFirst(DefaultDropdownItem.All);
            result.AddItemAtEnd(DefaultDropdownItem.NotListed);

            return JsonNet(result);
        }

        [HttpPost]
        public virtual JsonNetResult GetInstallments(string term, PeopleSearchType? searchType, int? partnerId, int? schoolId, long? seasonId, long? programId, long? programScheduleId, int? OrderMode, DateTime? start, DateTime? endDate, DateTime? transactionStartDate, DateTime? transactionEndDate, string UserName, string mode, string Status, PaginationModel paginationModel, string customeStripeErrorMessage, List<GridSort> sort = null, string firstName = null, string lastName = null)
        {
            Club club;
            int? clubId = null;
            var activeClubId = ActiveClub.Id;
            club = _clubBusiness.Get(activeClubId);

            if (club.IsSchool || club.IsProvider)
            {
                clubId = ActiveClub.Id;
            }
            else
            {
                partnerId = ActiveClub.Id;
                if (schoolId.HasValue && schoolId != 0)
                {
                    clubId = schoolId;
                }
            }

            var startDate = (DateTime?)null;
            var dataSource = new List<InstallmentsViewModel>();
            var end = (DateTime?)null;

            if (endDate.HasValue)
            {
                end = endDate.Value;
            }

            if (start.HasValue)
            {
                startDate = start.Value;
            }

            if (!string.IsNullOrEmpty(term))
            {
                term = term.ToLower();
            }

            var installments = _orderInstallmentBusiness.GetAllInstallments(startDate, end);

            if (transactionEndDate.HasValue || transactionStartDate.HasValue)
            {
                var transactionEnd = transactionEndDate?.Date;
                var transactionStart = transactionStartDate?.Date;

                installments = _orderInstallmentBusiness.GetInstallmentOfTransactionDate(transactionStart, transactionEnd, installments);
            }

            if (clubId.HasValue)
            {
                installments = installments.Where(c => c.OrderItem.Order.ClubId == clubId);
            }
            else if (partnerId.HasValue)
            {
                installments = installments.Where(c => c.OrderItem.Order.Club.PartnerId == partnerId);
            }

            if (seasonId.HasValue && seasonId != 0)
            {
                installments = installments.Where(c => c.OrderItem.SeasonId == seasonId);
            }

            if (programId.HasValue && programId != 0)
            {
                installments = installments.Where(c => c.OrderItem.ProgramSchedule.ProgramId == programId);
            }

            if (programScheduleId.HasValue && programScheduleId != 0)
            {
                installments = installments.Where(c => c.OrderItem.ProgramSchedule.Id == programScheduleId);
            }

            if (OrderMode.HasValue)
            {
                installments = installments.Where(c => (int)c.OrderItem.Order.OrderMode == OrderMode);
            }

            if (!string.IsNullOrEmpty(UserName))
            {
                installments = installments.Where(c => c.OrderItem.Order.User.UserName == UserName);
            }

            installments = _orderInstallmentBusiness.GetInstallmentsByStatus(Status, installments);
            installments = _orderInstallmentBusiness.GetInstallmentsByMode(mode, installments);

            installments = _orderInstallmentBusiness.GetInstallmentsByParticipantInfo(searchType, installments, term, customeStripeErrorMessage, firstName, lastName);

            var pagedInstallments = installments.AsEnumerable().OrderByDescending(i => i.InstallmentDate).Pagination(paginationModel);

            foreach (var installment in pagedInstallments)
            {
                var userCreditCards = _userCreditCardBusiness.GetUserCreditCards(installment.OrderItem.Order.User.Id);
                var defaultCreditCard = userCreditCards?.FirstOrDefault(c => c.IsDefault);
                var installmentLastDigit = defaultCreditCard != null ? defaultCreditCard.LastDigits : string.Empty;
                //Get Installment payment details of payment
                var installmentTransaction = _transactionActivityBusiness.GetTransactionsForInstallmentReport(installment);

                var installmentPaymentDetail = installmentTransaction != null ? installmentTransaction.PaymentDetail : null;

                dataSource.Add(new InstallmentsViewModel()
                {
                    Id = installment.Id,
                    ClubDomain = installment.OrderItem.Order.Club != null ? installment.OrderItem.Order.Club.Domain : null,
                    Season = installment.OrderItem.Season.Title,
                    Program = installment.OrderItem.ProgramSchedule.Program.Name,
                    ScheduleAndDate = _programBusiness.GetProgramDateWithScheduleTitleAndSchedulePart(installment.OrderItem.ProgramSchedule, installment.ProgramSchedulePart),
                    ParticipantName = installment.OrderItem.FirstName + " " + installment.OrderItem.LastName,
                    TransactionDate = installmentTransaction != null ? DateTimeHelper.GetIsoDateFormat(installmentTransaction.TransactionDate) : string.Empty,
                    Stopped = _orderInstallmentBusiness.GetInstallmentPaymentMode(installment),
                    TransactionId = installmentPaymentDetail != null ? installmentPaymentDetail.TransactionId : string.Empty,
                    OrderMode = installment.OrderItem.Order.OrderMode.ToDescription(),
                    LastDigit = installmentLastDigit,
                    ConfirmationId = installment.OrderItem.Order.ConfirmationId,
                    SendReminder = installment.NoticeSent,
                    Expiry = defaultCreditCard != null ? string.Format(Constants.ExpiryDateFormat, defaultCreditCard.ExpiryMonth, defaultCreditCard.ExpiryYear) : string.Empty,
                    Amount = CurrencyHelper.FormatCurrencyWithPenny(installment.Amount, installment.OrderItem.Order.Club.Currency),
                    AmountPaid = CurrencyHelper.FormatCurrencyWithPenny(installment.PaidAmount, installment.OrderItem.Order.Club.Currency),
                    Balance = CurrencyHelper.FormatCurrencyWithPenny(installment.Amount - (installment.PaidAmount.HasValue ? installment.PaidAmount.Value : 0), installment.OrderItem.Order.Club.Currency),
                    DueDate = DateTimeHelper.GetIsoDateFormat(installment.InstallmentDate),
                    PaidDate = installment.PaidDate != null ? DateTimeHelper.GetIsoDateFormat(installment.PaidDate.Value) : null,
                    Phone = installment.OrderItem.Order.User.PhoneNumber,
                    Email = installment.OrderItem.Order.User.Email,
                    Status = _orderInstallmentBusiness.GetInstallmentStatus(installment),
                    Description = installmentPaymentDetail != null ? installmentPaymentDetail.TransactionMessage : string.Empty,
                    SeasonDomain = installment.OrderItem.Season.Domain,
                    ClubId = installment.OrderItem.Order.Club.Id,
                    OrderItemId = installment.OrderItem.Id,
                });
            }

            decimal totalAmount = 0;
            decimal totalPaidAmount = 0;
            decimal totalBalance = 0;

            if (installments.Any())
            {
                totalAmount = installments.Sum(i => i.Amount);
                totalPaidAmount = installments.Sum(i => i.PaidAmount.HasValue ? i.PaidAmount.Value : 0);
                totalBalance = installments.Sum(i => i.Amount - (i.PaidAmount.HasValue ? i.PaidAmount.Value : 0));
            }

            var clubCurrency = Ioc.ClubBusiness.GetClubCurrency(this.GetCurrentClubDomain());
            return JsonNet(new { data = dataSource.OrderBy(c => c.User), total = installments.Count(), TotalPastInstallment = installments.Count(), TotalAmountPastInstallment = CurrencyHelper.FormatCurrencyWithPenny(Math.Abs(totalAmount), clubCurrency), TotalPaidAmountPastInstallment = CurrencyHelper.FormatCurrencyWithPenny(Math.Abs(totalPaidAmount), clubCurrency), TotalBalancePastInstallment = CurrencyHelper.FormatCurrencyWithPenny(Math.Abs(totalBalance), clubCurrency), IsSchoolOrProvider = club.IsSchool || club.IsProvider ? true : false });
        }

        public virtual ActionResult UndoFailedInstallment(long id)
        {
            var res = _orderInstallmentBusiness.UndoFailedInstallment(id);

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }

        [HttpPost]
        public virtual JsonNetResult GetClientCreditCards(int? partnerId, int? schoolId, bool? isDefault, string UserName, string mode, int? lastOrderMode, DateTime? orderDate, DateTime? installmentDate, DateTime? installmentEndDate, PaginationModel paginationModel)
        {
            var club = _clubBusiness.Get(ActiveClub.Id);
            int? clubId = null;
            var dataSource = new List<CreditCardsListViewModel>();
            var orders = Ioc.OrderBusiness.GetAllOrdersNeedCreditCard();
            var isSchoolOrProvider = club.IsSchool || club.IsProvider ? true : false;

            if (club.IsSchool || club.IsProvider || (!club.IsPartner))
            {
                clubId = ActiveClub.Id;
            }
            else
            {
                partnerId = ActiveClub.Id;
                if (schoolId.HasValue)
                {
                    clubId = schoolId;
                }
            }

            if (clubId.HasValue)
            {
                club = _clubBusiness.Get(clubId.Value);
                orders = orders.Where(o => o.ClubId == clubId);
            }
            else if (partnerId.HasValue)
            {
                club = _clubBusiness.Get(partnerId.Value);
                orders = orders.Where(o => o.Club.PartnerId == partnerId);
            }

            if (orderDate.HasValue)
            {
                orders = orders.Where(c => c.CompleteDate >= orderDate.Value);
            }

            var orderOrderItems = Ioc.OrderItemBusiness.GetComplatedOrderitems().Where(o => orders.Contains(o.Order) && o.ItemStatus == OrderItemStatusCategories.completed);
            var orderItemInstallments = _orderInstallmentBusiness.GetOrderItemInstallments(orderOrderItems);

            if (installmentDate.HasValue && installmentEndDate.HasValue)
            {
                orderItemInstallments = orderItemInstallments.Where(i => i.InstallmentDate >= installmentDate.Value && i.InstallmentDate <= installmentEndDate.Value);
            }
            else if (installmentDate.HasValue)
            {
                orderItemInstallments = orderItemInstallments.Where(i => i.InstallmentDate >= installmentDate.Value);
            }
            else if (installmentEndDate.HasValue)
            {
                orderItemInstallments = orderItemInstallments.Where(i => i.InstallmentDate <= installmentEndDate.Value);
            }

            var _paymentGateway = Ioc.ClientBusiness.GetPaymentGatewayClub(club.Id);
            var users = orderItemInstallments.OrderByDescending(u => u.OrderItem.Order.CompleteDate).Select(c => c.OrderItem.Order.User).Distinct();
            var userIds = users.Select(u => u.Id).ToList();
            var userCreditCards = _userCreditCardBusiness.GetUserCreditCards(userIds).Where(c => c.TypePaymentGateway == _paymentGateway);

            var secondUserIds = new List<int>();

            if (!string.IsNullOrEmpty(UserName))
            {
                userCreditCards = userCreditCards.Where(c => c.User.UserName.Contains(UserName.Trim()));
                userIds = userCreditCards.Select(c => c.UserId).ToList();
                users = users.Where(u => userIds.Contains(u.Id));
            }

            if (!string.IsNullOrEmpty(mode))
            {
                if (mode == "Preferred") // Preferrd
                {
                    userCreditCards = userCreditCards.Where(c => c.IsDefault);
                    userIds = userCreditCards.Select(c => c.UserId).ToList();
                    users = users.Where(u => userIds.Contains(u.Id));
                }
                else if (mode == "NotPreferred") // have Credit card but no Preferrd and no Credit card
                {
                    var temp = new List<JbUser>();
                    var hasCreditCardUserIds = userCreditCards.Select(u => u.UserId).ToList();
                    temp.AddRange(Ioc.UserProfileBusiness.GetList(userIds).Where(u => !hasCreditCardUserIds.Contains(u.Id)));

                    userIds = new List<int>();
                    var groupUserCedit = userCreditCards.GroupBy(c => c.UserId);

                    foreach (var group in groupUserCedit)
                    {
                        var userCredit = group.ToList();

                        if (!userCredit.Any(c => c.IsDefault))
                        {
                            userIds = userCredit.Select(c => c.UserId).ToList();
                        }
                    }

                    temp.AddRange(users.Where(u => userIds.Contains(u.Id)));

                    users = temp.AsQueryable();
                }
                else if (mode == "NoCard") // no Credit card
                {
                    var hasCreditCardUserIds = userCreditCards.Select(u => u.UserId).ToList();
                    users = Ioc.UserProfileBusiness.GetList(userIds).Where(u => !hasCreditCardUserIds.Contains(u.Id));

                    userIds = users.Select(c => c.Id).ToList();
                }
                else if (mode == "HasCardNotPreferred") // have Credit card but no Preferrd
                {
                    var groupUserCedit = userCreditCards.GroupBy(c => c.UserId);
                    userIds = new List<int>();
                    foreach (var group in groupUserCedit)
                    {
                        var userCredit = group.ToList();

                        if (!userCredit.Any(c => c.IsDefault))
                        {
                            userIds = userCredit.Select(c => c.UserId).ToList();
                        }
                    }

                    users = users.Where(u => userIds.Contains(u.Id));
                }
            }

            var pagedUsers = users.Pagination(paginationModel);

            foreach (var item in pagedUsers)
            {
                var userCards = _userCreditCardBusiness.GetUserCreditCards(item.Id, _paymentGateway).OrderByDescending(c => c.IsDefault).ToList();
                var lastOrder = orders.Where(o => o.UserId.Equals(item.Id)).OrderByDescending(o => o.Id).First();
                var userOrdersClub = orders.Where(o => o.UserId.Equals(item.Id)).Select(o => o.Club).Distinct();
                var userClubDomains = GetUserClubs(userOrdersClub);

                var firstUserCard = userCards.Count() > 0 ? userCards[0] : new UserCreditCard();
                var secondUserCard = userCards.Count() > 1 ? userCards[1] : new UserCreditCard();
                var thirdUserCard = userCards.Count() > 2 ? userCards[2] : new UserCreditCard();
                var lastUserOrder = Ioc.OrderBusiness.GetLastUserOrder(item.Id);
                bool hasError = false;
                bool hasNoCreditCardError = false;

                if (userCards.Any() && userCards.Count() > 0)
                {
                    if (!userCards.Any(c => c.IsDefault))
                    {
                        hasError = true;
                    }
                }
                else
                {
                    hasNoCreditCardError = true;
                }

                dataSource.Add(new CreditCardsListViewModel()
                {
                    User = item.UserName,
                    HasError = hasError,
                    HasNoCreditCardError = hasNoCreditCardError,
                    userCardsCount = userCards.Count(),
                    LastOrderDate = lastOrder.CompleteDate,
                    ClubDomain = userClubDomains,
                    CustomerId = firstUserCard.CustomerId,
                    LastDigits = firstUserCard.LastDigits,
                    Expiry = !string.IsNullOrEmpty(firstUserCard.ExpiryMonth) && !string.IsNullOrEmpty(firstUserCard.ExpiryYear) ? string.Format("{0}/{1}", firstUserCard.ExpiryMonth, firstUserCard.ExpiryYear) : string.Empty,
                    Name = firstUserCard.Name,
                    Brand = firstUserCard.Brand,
                    IsDefault = firstUserCard.IsDefault,
                    TypeCreditcard = firstUserCard.TypeCreditCard != null ? firstUserCard.TypeCreditCard.ToDescription() : string.Empty,
                    CreateDate = firstUserCard.CreatedDate != null ? DateTimeHelper.GetIsoDateTimeFormat(firstUserCard.CreatedDate.Value) : string.Empty,
                    UpdateDate = firstUserCard.UpdatedDate != null ? DateTimeHelper.GetIsoDateTimeFormat(firstUserCard.UpdatedDate.Value) : string.Empty,
                    DefaultDate = firstUserCard.DefaultDate != null ? DateTimeHelper.GetIsoDateTimeFormat(firstUserCard.DefaultDate.Value) : string.Empty,
                    LastOrderMode = lastUserOrder.OrderMode.ToDescription(),
                    CustomerId2 = secondUserCard.CustomerId,
                    LastDigits2 = secondUserCard.LastDigits,
                    Expiry2 = !string.IsNullOrEmpty(secondUserCard.ExpiryMonth) && !string.IsNullOrEmpty(secondUserCard.ExpiryYear) ? string.Format("{0}/{1}", secondUserCard.ExpiryMonth, secondUserCard.ExpiryYear) : string.Empty,
                    Name2 = secondUserCard.Name,
                    Brand2 = secondUserCard.Brand,
                    IsDefault2 = secondUserCard.IsDefault,
                    TypeCreditcard2 = secondUserCard.TypeCreditCard != null ? secondUserCard.TypeCreditCard.ToDescription() : string.Empty,
                    CreateDate2 = secondUserCard.CreatedDate != null ? DateTimeHelper.GetIsoDateTimeFormat(secondUserCard.CreatedDate.Value) : string.Empty,
                    UpdateDate2 = secondUserCard.UpdatedDate != null ? DateTimeHelper.GetIsoDateTimeFormat(secondUserCard.UpdatedDate.Value) : string.Empty,
                    DefaultDate2 = secondUserCard.DefaultDate != null ? DateTimeHelper.GetIsoDateTimeFormat(secondUserCard.DefaultDate.Value) : string.Empty,
                    CustomerId3 = thirdUserCard.CustomerId,
                    LastDigits3 = thirdUserCard.LastDigits,
                    Expiry3 = !string.IsNullOrEmpty(thirdUserCard.ExpiryMonth) && !string.IsNullOrEmpty(thirdUserCard.ExpiryYear) ? string.Format("{0}/{1}", thirdUserCard.ExpiryMonth, thirdUserCard.ExpiryYear) : string.Empty,
                    Name3 = thirdUserCard.Name,
                    Brand3 = thirdUserCard.Brand,
                    IsDefault3 = thirdUserCard.IsDefault,
                    TypeCreditcard3 = thirdUserCard.TypeCreditCard != null ? thirdUserCard.TypeCreditCard.ToDescription() : string.Empty,
                    CreateDate3 = thirdUserCard.CreatedDate != null ? DateTimeHelper.GetIsoDateTimeFormat(thirdUserCard.CreatedDate.Value) : string.Empty,
                    UpdateDate3 = thirdUserCard.UpdatedDate != null ? DateTimeHelper.GetIsoDateTimeFormat(thirdUserCard.UpdatedDate.Value) : string.Empty,
                    DefaultDate3 = thirdUserCard.DefaultDate != null ? DateTimeHelper.GetIsoDateTimeFormat(thirdUserCard.DefaultDate.Value) : string.Empty,
                });
            }

            return JsonNet(new { data = dataSource, total = users.Count(), IsSchool = isSchoolOrProvider });
        }

        private string GetUserClubs(IQueryable<Club> clubs)
        {
            if (clubs != null && clubs.Any())
            {
                return string.Join(", ", clubs.Select(c => c.Domain));
            }
            return string.Empty;
        }

        public virtual JsonNetResult GetSeasonPrograms(long seasonId)
        {
            var result = _programBusiness.GetKeyValueSeasonPrograms(seasonId);
            return JsonNet(result);
        }
        public virtual JsonNetResult GetProgramSchedules(long programId)
        {
            var result = _programBusiness.GetProgramSchedules(programId);
            return JsonNet(result);
        }

        [HttpPost]
        public virtual JsonNetResult GetPricingReport(CustomReportPortalModel model)
        {
            var pageSize = (Request.Params["PageSize"] != null) ? int.Parse(Request.Params["PageSize"]) : 100000;
            var page = (Request.Params["Page"] != null) ? int.Parse(Request.Params["Page"]) : 1;

            var clubs = new List<Club>();
            if (model.ClubType == "school")
            {
                if (model.IsAllClubs == "true")
                {
                    var partnerId = ActiveClub.Id;
                    clubs = _clubBusiness.GetAllPartnerSchools(partnerId).ToList();
                }
                else
                {
                    var schoolIds = model._SelectedClubs.ToList();
                    clubs = _clubBusiness.GetList().Where(c => schoolIds.Contains(c.Id)).ToList();
                }
            }
            else if (model.ClubType == "provider")
            {
                if (model.IsAllClubs == "true")
                {
                    var partnerId = ActiveClub.Id;
                    clubs = _clubBusiness.GetAllPartnerProviders(partnerId).ToList();
                }
                else
                {
                    var providerIds = model._SelectedClubs.ToList();
                    clubs = _clubBusiness.GetList().Where(c => providerIds.Contains(c.Id)).ToList();
                }
            }
            else if (model.ClubType == "district")
            {
                if (model.IsAllClubs == "true")
                {
                    var partnerId = ActiveClub.Id;
                    var districtIds = _clubBusiness.GetList()
                        .Where(c => c.ClubType.EnumType == ClubTypesEnum.SchoolDistrict && c.PartnerId == partnerId && !c.IsDeleted)
                        .Select(c => c.Id)
                        .ToList();

                    clubs = _clubBusiness.GetList().Where(c => districtIds.Contains(c.DistrictId.Value)).ToList();
                }
                else
                {
                    var districtIds = model._SelectedClubs.ToList();
                    clubs = _clubBusiness.GetList().Where(c => districtIds.Contains(c.DistrictId.Value)).ToList();
                }
            }

            var clubIds = clubs.Select(c => c.Id);

            var programs = Ioc.ProgramBusiness.GetList().Where(p => clubIds.Contains(p.ClubId) && p.Status != ProgramStatus.Deleted && p.ProgramSchedules.FirstOrDefault() != null && p.Season.Status == SeasonStatus.Live);

            if (model.Filters.Status == OrderItemStatusReasons.regular)
            {
                programs = programs.Where(p => p.LastCreatedPage >= ProgramPageStep.Step4);
            }
            else if (model.Filters.Status == OrderItemStatusReasons.draft)
            {
                programs = programs.Where(p => p.LastCreatedPage < ProgramPageStep.Step4);
            }

            if (model.SeasonName != SeasonNames.SelectSeason)
            {
                programs = programs.Where(p => p.Season.Name == model.SeasonName);
            }

            if (model.Year != -1)
            {
                programs = programs.Where(p => p.Season.Year == model.Year);
            }

            var result = new List<PricingReportViewModel>();

            var programPaginate = programs.OrderBy(p => p.Club.Name).ThenBy(p => p.OutSourceSeason.Club.Name).Skip((page - 1) * pageSize).Take(pageSize).ToList();

            foreach (var program in programPaginate)
            {
                var clubName = program.Club.Name;
                var uniqueId = program.UniqueKey;
                var programName = program.Name;
                var providerName = program.OutSourceSeason != null ? program.OutSourceSeason.Club.Name : string.Empty;

                var activity = string.Empty;
                if (program.Club.Charges != null && program.Club.Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge))
                {
                    var surCharge = program.Club.Charges.First(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge);

                    if (surCharge.AmountType == ChargeDiscountType.Fixed)
                    {
                        activity = $"${surCharge.Amount}";
                    }
                    else
                    {
                        var tuitions = program.ProgramSchedules.FirstOrDefault().Charges.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee);

                        if (tuitions.Any() && tuitions != null)
                        {
                            var average = tuitions.Sum(t => t.Amount) / tuitions.Count();
                            var percent = average * surCharge.Amount / 100;

                            activity = $"${percent}";
                        }
                    }
                }

                var parentPrice = string.Empty;
                if (program.ProgramSchedules.FirstOrDefault() != null && program.ProgramSchedules.FirstOrDefault().Charges != null && program.ProgramSchedules.FirstOrDefault().Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee))
                {
                    var tuitions = program.ProgramSchedules.FirstOrDefault().Charges.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee);
                    var tuitionNames = tuitions.Select(t => t.Name);
                    var tuitionAmounts = tuitions.Select(t => t.Amount);

                    if (tuitionNames.Count() > 1)
                    {
                        var tuitionList = new List<string>();

                        for (int i = 0; i < tuitionNames.Count(); i++)
                        {
                            tuitionList.Add($"{tuitionNames.ElementAt(i)}: ${tuitionAmounts.ElementAt(i)}");
                        }

                        parentPrice = string.Join(", \n", tuitionList);
                    }
                    else
                    {
                        parentPrice = $"${tuitionAmounts.First().ToString()}";
                    }
                }

                var vendorPrice = string.Empty;
                if (program.Catalog != null)
                {
                    var vendorPriceList = new List<string>();

                    var priceOptions = GetDefaultPriceOptions(program.ClubId);

                    foreach (var price in program.Catalog.PriceOptions)
                    {
                        vendorPriceList.Add($"{price.DurationTitle} / {price.Title} ${price.Amount}");
                    }

                    vendorPrice = string.Join(", \n", vendorPriceList);
                }

                result.Add(new PricingReportViewModel
                {
                    ClubName = clubName,
                    UniqueId = uniqueId,
                    ProgramName = programName,
                    ProviderName = providerName,
                    Activity = activity,
                    ParentPrice = parentPrice,
                    VendorPrice = vendorPrice
                });
            }



            return JsonNet(new { DataSource = result, TotalCount = programs.Count() });
        }

        [HttpPost]
        public virtual JsonNetResult GetFullClassListReport(CustomReportPortalModel model)
        {
            var pageSize = (Request.Params["PageSize"] != null) ? int.Parse(Request.Params["PageSize"]) : 100000;
            var page = (Request.Params["Page"] != null) ? int.Parse(Request.Params["Page"]) : 1;

            var clubs = new List<Club>();
            if (model.ClubType == "school")
            {
                if (model.IsAllClubs == "true")
                {
                    var partnerId = ActiveClub.Id;
                    clubs = _clubBusiness.GetAllPartnerSchools(partnerId).ToList();
                }
                else
                {
                    var schoolIds = model._SelectedClubs.ToList();
                    clubs = _clubBusiness.GetList().Where(c => schoolIds.Contains(c.Id)).ToList();
                }
            }
            else if (model.ClubType == "provider")
            {
                if (model.IsAllClubs == "true")
                {
                    var partnerId = ActiveClub.Id;
                    clubs = _clubBusiness.GetAllPartnerProviders(partnerId).ToList();
                }
                else
                {
                    var providerIds = model._SelectedClubs.ToList();
                    clubs = _clubBusiness.GetList().Where(c => providerIds.Contains(c.Id)).ToList();
                }
            }
            else if (model.ClubType == "district")
            {
                if (model.IsAllClubs == "true")
                {
                    var partnerId = ActiveClub.Id;
                    var districtIds = _clubBusiness.GetList()
                        .Where(c => c.ClubType.EnumType == ClubTypesEnum.SchoolDistrict && c.PartnerId == partnerId && !c.IsDeleted)
                        .Select(c => c.Id)
                        .ToList();

                    clubs = _clubBusiness.GetList().Where(c => districtIds.Contains(c.DistrictId.Value)).ToList();
                }
                else
                {
                    var districtIds = model._SelectedClubs.ToList();
                    clubs = _clubBusiness.GetList().Where(c => districtIds.Contains(c.DistrictId.Value)).ToList();
                }
            }

            var clubIds = clubs.Select(c => c.Id);
            var programs = Ioc.ProgramBusiness.GetList().Where(p => clubIds.Contains(p.ClubId) && p.Status != ProgramStatus.Deleted && p.ProgramSchedules.FirstOrDefault() != null && p.TypeCategory == ProgramTypeCategory.BeforeAfterCare && p.Season.Status == SeasonStatus.Live);

            if (model.SeasonName != SeasonNames.SelectSeason)
            {
                programs = programs.Where(p => p.Season.Name == model.SeasonName);
            }

            if (model.Year != -1)
            {
                programs = programs.Where(p => p.Season.Year == model.Year);
            }

            var result = new List<Dictionary<string, object>>();
            var titles = new List<string>();

            var programSchedules = programs.SelectMany(p => p.ProgramSchedules).ToList();
            var programSchedulesPaginate = programSchedules.OrderBy(p => p.Program.Name).Skip((page - 1) * pageSize).Take(pageSize).ToList();

            foreach (var schedule in programSchedulesPaginate)
            {
                var row = new Dictionary<string, object>();

                row.Add("SchoolName", schedule.Program.Club.Name);
                titles.Add("School");
                row.Add("ProgramId", schedule.Program.Id);
                titles.Add("Program ID");
                row.Add("ProgramName", schedule.Program.Name);
                titles.Add("Program");
                row.Add("ProgramType", schedule.ScheduleMode == TimeOfClassFormation.AM ? "Before school" : schedule.ScheduleMode == TimeOfClassFormation.PM ? "After school" : schedule.ScheduleMode == TimeOfClassFormation.Both ? "Combo" : string.Empty);
                titles.Add("Program type");
                row.Add("ScheduleTitle", schedule.Title);
                titles.Add("Title");
                row.Add("ScheduleStartDate", schedule.StartDate.ToString(Constants.DateTime_Comma));
                titles.Add("Start date");
                row.Add("ScheduleEndDate", schedule.EndDate.ToString(Constants.DateTime_Comma));
                titles.Add("End date");

                var days = ((ScheduleAfterBeforeCareAttribute)schedule.Attributes).Days;

                for (int i = 0; i < 7; i++)
                {
                    var day = days.SingleOrDefault(d => (int)d.DayOfWeek == i);
                    var startTime = day != null && day.StartTime.HasValue ? DateTime.Today.AddHours(day.StartTime.Value.Hours).AddMinutes(day.StartTime.Value.Minutes).AddSeconds(day.StartTime.Value.Seconds).ToString(Constants.DefaultTimeFormat) : string.Empty;
                    var endTime = day != null && day.EndTime.HasValue ? DateTime.Today.AddHours(day.EndTime.Value.Hours).AddMinutes(day.EndTime.Value.Minutes).AddSeconds(day.EndTime.Value.Seconds).ToString(Constants.DefaultTimeFormat) : string.Empty;

                    switch (i)
                    {
                        case 0:
                            {
                                row.Add("SundayStartTime", startTime);
                                titles.Add("Sunday start");

                                row.Add("SundayEndTime", endTime);
                                titles.Add("Sunday end");
                            }
                            break;
                        case 1:
                            {
                                row.Add("MondayStartTime", startTime);
                                titles.Add("Monday start");

                                row.Add("MondayEndTime", endTime);
                                titles.Add("Monday end");
                            }
                            break;
                        case 2:
                            {
                                row.Add("TuesdayStartTime", startTime);
                                titles.Add("Tuesday start");

                                row.Add("TuesdayEndTime", endTime);
                                titles.Add("Tuesday end");
                            }
                            break;
                        case 3:
                            {
                                row.Add("WednesdayStartTime", startTime);
                                titles.Add("Wednesday start");

                                row.Add("WednesdayEndTime", endTime);
                                titles.Add("Wednesday end");
                            }
                            break;
                        case 4:
                            {
                                row.Add("ThursdayStartTime", startTime);
                                titles.Add("Thursday start");

                                row.Add("ThursdayEndTime", endTime);
                                titles.Add("Thursday end");
                            }
                            break;
                        case 5:
                            {
                                row.Add("FridayStartTime", startTime);
                                titles.Add("Friday start");

                                row.Add("FridayEndTime", endTime);
                                titles.Add("Friday end");
                            }
                            break;
                        case 6:
                            {
                                row.Add("SaturdayStartTime", startTime);
                                titles.Add("Saturday start");

                                row.Add("SaturdayEndTime", endTime);
                                titles.Add("Saturday end");
                            }
                            break;
                    }

                }

                row.Add("ComboPart1", schedule.ScheduleMode == TimeOfClassFormation.Both && schedule.Program.ProgramSchedules.Any(s => s.ScheduleMode == TimeOfClassFormation.AM) ? "After school" : string.Empty);
                titles.Add("Combo part 1");

                row.Add("ComboPart2", schedule.ScheduleMode == TimeOfClassFormation.Both && schedule.Program.ProgramSchedules.Any(s => s.ScheduleMode == TimeOfClassFormation.PM) ? "Before school" : string.Empty);
                titles.Add("Combo part 2");

                result.Add(row);

            }

            return JsonNet(new { DataSource = result, TotalCount = programSchedules.Count(), Titles = titles });
        }
    }
}