﻿namespace SportsClub.Areas.Dashboard.Models
{
    public class PricingReportViewModel
    {
        public string ClubName { get; set; }
        public string UniqueId { get; set; }
        public string ProgramName { get; set; }
        public string ProviderName { get; set; }
        public string Activity { get; set; }
        public string VendorPrice { get; set; }
        public string ParentPrice { get; set; }

    }
}