﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class ShareAsAttachment
    {
        public string SentTo { get; set; }

        public string Subject { get; set; }

        public string Message { get; set; }

        public string CustomMessage { get; set; }

    }
}