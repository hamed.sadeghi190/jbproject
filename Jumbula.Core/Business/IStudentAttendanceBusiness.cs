﻿using Jb.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IStudentAttendanceBusiness
    {
        OperationStatus Create(StudentAttendance studentAttendance);
        StudentAttendance Get(int id);
        IQueryable<StudentAttendance> GetList(Expression<Func<StudentAttendance, bool>> predicate);
        IQueryable<StudentAttendance> GetList();
        IQueryable<StudentAttendance> GetList(List<long> orderSessionId);
        OperationStatus Update(StudentAttendance studentAttendance);
        OperationStatus UpdateAttendances(List<StudentAttendance> attendances);
        OperationStatus UpdateDismissal(List<StudentAttendance> attendances);
        AttendanceStatus? GetStudentStatus(int profileId, long scheduleId, int sessionId);
    }
}
