﻿using Jumbula.Common.Enums;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class SecurityBusiness : ISecurityBusiness
    {
        private readonly IClubBusiness _clubBusiness;
        public SecurityBusiness(IClubBusiness clubBusiness)
        {
            _clubBusiness = clubBusiness;
        }

        public List<ActionRole> GetActionRoles()
        {
            return new List<ActionRole>
            {
                new ActionRole
                {
                    Action = JbAction.AdminDashboard_View,
                    Roles = new List<RoleCategory> { RoleCategory.SysAdmin, RoleCategory.Support, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.Instructor,RoleCategory.OnsiteCoordinator,RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager, RoleCategory.RestrictedManager, RoleCategory.Accountant, RoleCategory.Partner }
                },
                new ActionRole
                {
                    Action = JbAction.Home_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.Instructor,RoleCategory.OnsiteCoordinator,RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager, RoleCategory.RestrictedManager, RoleCategory.Partner }
                },
               //Season
                new ActionRole
                {
                    Action = JbAction.Season_View,
                    Roles = new List<RoleCategory> { RoleCategory.SysAdmin, RoleCategory.Support, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.Instructor,RoleCategory.OnsiteCoordinator,RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.SeasonOverview_View,
                    Roles = new List<RoleCategory> {  RoleCategory.SysAdmin,RoleCategory.Support, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager, RoleCategory.RestrictedManager }
                },

                new ActionRole
                {
                    Action = JbAction.Season_SwitchMode,
                    Roles = new List<RoleCategory> {  RoleCategory.SysAdmin,RoleCategory.Support, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Season_Edit,
                    Roles = new List<RoleCategory> { RoleCategory.SysAdmin, RoleCategory.Support, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Season_Add,
                    Roles = new List<RoleCategory> {  RoleCategory.SysAdmin,RoleCategory.Support, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Toolbox_view,
                    Roles = new List<RoleCategory> {  RoleCategory.Support, RoleCategory.Owner, RoleCategory.Admin , RoleCategory.Partner}
                },
                new ActionRole
                {
                    Action = JbAction.Season_Delete,
                    Roles = new List<RoleCategory> {  RoleCategory.SysAdmin, RoleCategory.Support, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Season_Archive,
                    Roles = new List<RoleCategory> {  RoleCategory.SysAdmin, RoleCategory.Support, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Setup_View,
                    Roles = new List<RoleCategory> {   RoleCategory.SysAdmin,RoleCategory.Support, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.Instructor,RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Season_AssignRoom,
                    Roles = new List<RoleCategory> {   RoleCategory.SysAdmin,RoleCategory.Support, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Owner, RoleCategory.SchoolContributor, RoleCategory.RestrictedManager }
                },
                  new ActionRole
                {
                    Action = JbAction.Season_AssignInstructor,
                    Roles = new List<RoleCategory> {  RoleCategory.SysAdmin, RoleCategory.Support, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Owner, RoleCategory.Contributor, RoleCategory.RestrictedManager }
                },
                //Campaign
                new ActionRole
                {
                    Action = JbAction.Campaign_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager,RoleCategory.RestrictedManager }
                },
                 new ActionRole
                {
                    Action = JbAction.CampaignRecipients_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Member_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                new ActionRole
                {
                    Action = JbAction.Subsidies_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                 new ActionRole
                {
                    Action = JbAction.Subsidy_Add,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                 new ActionRole
                {
                    Action = JbAction.Districts_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                 new ActionRole
                {
                    Action = JbAction.District_Add,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                new ActionRole
                {
                    Action = JbAction.People_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.Partner }
                },
                new ActionRole
                {
                    Action = JbAction.Settings_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager}
                },
                new ActionRole
                {
                    Action = JbAction.Help_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor,RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Campaign_Email_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager,RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Campaign_EmailTemplates_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager,RoleCategory.RestrictedManager }
                },
                 new ActionRole
                {
                    Action = JbAction.CampaignClassConfirmation_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Partner}
                },
                new ActionRole
                {
                    Action = JbAction.Families_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.Partner }
                },
                new ActionRole
                {
                    Action = JbAction.PeopleRegistrations_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.Partner }
                },
                new ActionRole
                {
                    Action = JbAction.Parents_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.Partner }
                },
                new ActionRole
                {
                    Action = JbAction.Settings_Account_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Settings_Account_Save,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Settings_Partner_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Partner}
                },
                new ActionRole
                {
                    Action = JbAction.Settings_Partner_Save,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Partner}
                },

                //Staff
                new ActionRole
                {
                    Action = JbAction.Staff_View,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Staff_Add,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager  }
                },
                 new ActionRole
                {
                    Action = JbAction.Staff_Add_From_Partner,
                    Roles = new List<RoleCategory>{  RoleCategory.Support,  RoleCategory.Admin }
                },
                new ActionRole
                {
                    Action = JbAction.Staff_Edit,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Staff_Delete,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                   new ActionRole
                {
                    Action = JbAction.Staff_Freeze,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                   new ActionRole
                {
                    Action = JbAction.Staff_Active,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Staff_ResendInvitation,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager}
                },
                 //Invoice
                  new ActionRole
                {
                    Action = JbAction.invoice_View,
                    Roles = new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.SchoolContributor , RoleCategory.OnsiteCoordinator }
                },
                  new ActionRole
                {
                    Action = JbAction.Flyer_View,
                    Roles = new List<RoleCategory>{ RoleCategory.Support,  RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Partner, RoleCategory.OnsiteSupportManager }
                },
                  //ESignature
                   new ActionRole
                {
                    Action = JbAction.ESignature_View,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.Partner, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                  new ActionRole
                {
                    Action = JbAction.ESignature_Add,
                    Roles = new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Partner}
                },
                  new ActionRole
                {
                    Action = JbAction.ESignature_Setting,
                    Roles = new List<RoleCategory>{ RoleCategory.Support,  RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                //Payment settings
                new ActionRole
                {
                    Action = JbAction.Settings_PaymentSettings_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Settings_PaymentSettings_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Settings_Checkout_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Settings_Checkout_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager }
                },
                  new ActionRole
                {
                    Action = JbAction.Settings_Appearance_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Settings_Appearance_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager }
                },
                  new ActionRole
                {
                    Action = JbAction.DonationForm_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager }
                },

                //Account settings
                new ActionRole
                {
                    Action = JbAction.AccountSetting_Overview_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                  new ActionRole
                {
                    Action = JbAction.AccountSetting_Overview_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager }
                },
                   new ActionRole
                {
                    Action = JbAction.AccountSetting_Insurance_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                  new ActionRole
                {
                    Action = JbAction.AccountSetting_Insurance_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                   new ActionRole
                {
                    Action = JbAction.AdvanceSetting_BackgroundCheck_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                   new ActionRole
                {
                    Action = JbAction.LogoSetting_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                  new ActionRole
                {
                    Action = JbAction.AdvanceSetting_BackgroundCheck_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.AccountSetting_Billing_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.AccountSetting_Billing_History_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
                  new ActionRole
                {
                    Action = JbAction.AccountSetting_Billing_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager }
                },
                    new ActionRole
                {
                    Action = JbAction.AccountSetting_Billing_Add,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager }
                },
                      new ActionRole
                {
                    Action = JbAction.AccountSetting_Billing_Delete,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager }
                },
                           new ActionRole
                {
                    Action = JbAction.AccountSetting_Billing_Activate,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.AccountSetting_ChangePlan_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                  new ActionRole
                {
                    Action = JbAction.AccountSetting_ChangePlan_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager }
                },
                  new ActionRole
                {
                    Action = JbAction.Account_Overview_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Account_Subscription_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Account_Subscription_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Settings_Advance_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Settings_MobileApp_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                  new ActionRole
                {
                    Action = JbAction.Settings_Notifications_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                  new ActionRole
                {
                    Action = JbAction.Settings_Holidays_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Settings_School_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                  new ActionRole
                {
                    Action = JbAction.Settings_School_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.SchoolSetting_Info_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                   new ActionRole
                {
                    Action = JbAction.SchoolSetting_Info_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.SchoolSetting_ProgramInfo_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                 new ActionRole
                {
                    Action = JbAction.SchoolSetting_ProgramInfo_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.SchoolSetting_OnSiteInfo_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                  new ActionRole
                {
                    Action = JbAction.SchoolSetting_OnSiteInfo_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.SchoolSetting_RegistrationInfo_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                 new ActionRole
                {
                    Action = JbAction.SchoolSetting_RegistrationInfo_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.SchoolSetting_AutoChargePolicy_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Admin, RoleCategory.Manager }
                },
                new ActionRole
                {
                    Action = JbAction.SchoolSetting_AutoChargePolicy_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Admin, RoleCategory.Manager }
                },
                new ActionRole
                {
                    Action = JbAction.SchoolSetting_DuplicateEnrollment_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Admin, RoleCategory.Manager }
                },
                new ActionRole
                {
                    Action = JbAction.SchoolSetting_DuplicateEnrollment_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Admin, RoleCategory.Manager }
                },

                   new ActionRole
                {
                    Action = JbAction.SchoolSetting_DelinquentPolicy_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Admin, RoleCategory.Manager }
                },
                new ActionRole
                {
                    Action = JbAction.SchoolSetting_DelinquentPolicy_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Admin, RoleCategory.Manager }
                },

                   new ActionRole
                {
                    Action = JbAction.SchoolSetting_ProgramPolicy_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Admin, RoleCategory.Manager }
                },
                //Season School settings
                new ActionRole
                {
                    Action = JbAction.Season_SchoolAdvancedSettings_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Season_SchoolSetting_GeneralInfo_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support,  RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager }
                },
                  new ActionRole
                {
                    Action = JbAction.Season_SchoolSetting_GeneralInfo_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support,  RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Season_SchoolSetting_DetailInfo_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support,  RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Season_SchoolSetting_DetailInfo_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support,  RoleCategory.Owner, RoleCategory.Admin }
                },
                 new ActionRole
                {
                    Action = JbAction.Season_SchoolSetting_PaymentPlan,
                    Roles = new List<RoleCategory> { RoleCategory.Support,  RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Season_SchoolSetting_ScheduleInfo_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
                  new ActionRole
                {
                    Action = JbAction.Season_SchoolSetting_ScheduleInfo_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager }
                },
                  new ActionRole
                {
                    Action = JbAction.Season_SchoolSetting_RegistrationInfo_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager }
                },
                  new ActionRole
                {
                    Action = JbAction.Season_SchoolSetting_RegistrationInfo_Save,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.OnsiteSupportManager, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Settings_General_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Settings_General_Save,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.ViewJumbulaHomeSettings,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },

                new ActionRole
                {
                    Action = JbAction.Settings_JbHomeSite,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner, RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },

                new ActionRole
                {
                    Action = JbAction.Member_Add,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.ProtalReport_Finance,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner }
                },
                new ActionRole
                {
                    Action = JbAction.PortalReport_View,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.SysAdmin,RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.PortalReport_UpdateReport,
                   Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.PortalReport_CustomReport,
                   Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.PortalReport_Catalog,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                new ActionRole
                {
                    Action = JbAction.PortalReport_Title1Discount,
                   Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.PortalReport_FiarFaxCounty,
                   Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
                 new ActionRole
                {
                    Action = JbAction.PortalReport_Pricing,
                   Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
                  new ActionRole
                {
                    Action = JbAction.PortalReport_FullClassList,
                   Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.PortalReport_CatalogOverviewActivity,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.PortalReport_CatalogList,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.PortalReport_SchoolMemberList,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                new ActionRole
                {
                    Action = JbAction.PortalReport_Discount,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.PortalReport_Graduate,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
                 new ActionRole
                {
                    Action = JbAction.PortalReport_OnsitePersonCheckin,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
               new ActionRole
                {
                    Action = JbAction.PortalReport_Donations,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.PortalReport_ParentMailingList,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.PortalReport_CustomReportClub,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
                 new ActionRole
                {
                    Action = JbAction.PortalReport_FamilyCreditCard,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
                  new ActionRole
                {
                    Action = JbAction.PortalReport_BulkDependentCareReceipt,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.ProtalReport_Planning,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                new ActionRole
                {
                    Action = JbAction.ProtalReport_Form,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                 new ActionRole
                {
                    Action = JbAction.PortalReport_Enrollments,
                    Roles = new List<RoleCategory> { RoleCategory.Support , RoleCategory.Partner ,RoleCategory.Admin}
                },
                    new ActionRole
                {
                    Action = JbAction.PortalReport_Rosters,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                new ActionRole
                {
                    Action = JbAction.ProtalReport_ProviderAddressList,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                new ActionRole
                {
                    Action = JbAction.ProtalReport_SchoolAddressList,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                new ActionRole
                {
                    Action = JbAction.ProtalReport_MemberList,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                new ActionRole
                {
                    Action = JbAction.ProtalReport_InstructorList,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                new ActionRole
                {
                    Action = JbAction.ProtalReport_InsuranceCertificateCheck,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                new ActionRole
                {
                    Action = JbAction.ProtalReport_SeasonSurveyDates,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                new ActionRole
                {
                    Action = JbAction.ProtalReport_InstructorAssignments,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                new ActionRole
                {
                    Action = JbAction.ProtalReport_FullSeasonClassList,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                new ActionRole
                {
                    Action = JbAction.ProtalReport_Followup,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                new ActionRole
                {
                    Action = JbAction.ProtalReport_InstructorCheckin,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                  new ActionRole
                {
                    Action = JbAction.PortalReport_SchoolInvoices,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner}
                },
                     new ActionRole
                {
                    Action = JbAction.PortalReport_Installments,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager}
                },
                new ActionRole
                {
                    Action = JbAction.PortalReport_ClientCreditCards,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager}
                },
                new ActionRole
                {
                    Action = JbAction.ProtalReport_ProviderContact,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.ProtalReport_RoomAssignment,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.ProgramsAndOrders_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Orders_ConfirmationId_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager}
                },
                 new ActionRole
                {
                    Action = JbAction.Orders_Balance_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager}
                },
                new ActionRole
                {
                    Action = JbAction.Orders_Balance_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager}
                },
                  new ActionRole
                {
                    Action = JbAction.Orders_EmailParticipant,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager}
                },
                 new ActionRole
                {
                    Action = JbAction.Orders_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                  new ActionRole
                {
                    Action = JbAction.ConfigureRosterReport_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Order_Edit,
                    Roles =new List<RoleCategory> {RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.Partner }
                },
                new ActionRole
                {
                    Action = JbAction.Order_Transfer,
                    Roles =new List<RoleCategory>{RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.Partner }
                },
                new ActionRole
                {
                    Action = JbAction.Order_TakePayment,
                    Roles =new List<RoleCategory>{RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.Partner }
                },
                 new ActionRole
                {
                    Action = JbAction.Order_Refund,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.Partner }
                },
                 new ActionRole
                {
                    Action = JbAction.Order_History,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Report_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Program_List_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Program_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Program_Draft_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Owner, RoleCategory.OnsiteSupportManager }
                },
                   new ActionRole
                {
                    Action = JbAction.Program_TotalRevenue_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support,  RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Owner, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Coupon_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Coupon_Upload,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Discount_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager , RoleCategory.RestrictedManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Charges_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager , RoleCategory.RestrictedManager }
                },
                   new ActionRole
                {
                    Action = JbAction.Charges_Add,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager , RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.PaymentPlan_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.ReigstrationEmailTemplate_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Program_Add,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Program_Edit,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Program_Delete,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager , RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Program_Freeze,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Program_Copy,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Program_RespondInvitation,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Coupon_Add,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Discount_Add,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.PaymentPlan_Add,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Forms_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                   new ActionRole
                {
                    Action = JbAction.GaTrackingId_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Partner, RoleCategory.Support, RoleCategory.Admin }
                },
                new ActionRole
                {
                    Action = JbAction.FollowUpForms_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.ApplicationFee_Add,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Campaign_Template_Add,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Partner,RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Campaign_Email_Send,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager  }
                },


                //Catalog
                 new ActionRole
                {
                    Action = JbAction.Catalogs_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor,RoleCategory.Partner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                  new ActionRole
                {
                    Action = JbAction.Catalog_Search,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Partner, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Catalogs_ViewItem,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Catalog_ViewAll,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                 new ActionRole
                {
                    Action = JbAction.CatalogSettings_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.CatalogItem_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.CatalogItem_Add,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.CatalogItem_Archive,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.CatalogItem_Hide,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.CatalogItem_Edit,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.CatalogItem_Delete,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.CatalogItem_Invite,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.CatalogSettings_Save,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                   new ActionRole
                {
                    Action = JbAction.SeasonOverview_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.TotalSale_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                 new ActionRole
                {
                    Action = JbAction.TotalAmount_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.TotalRegistration_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.DailySale_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.CapacityReport_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor,RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.SaleDistribution_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.AttendanceReports_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor,RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.ProgramCalendar_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor,RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.ProgramCalendar_Action,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin }
                },
                new ActionRole
                {
                    Action = JbAction.Tour_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Offline_Register,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager }
                },
                 new ActionRole
                {
                    Action = JbAction.OrderDetail_View,
                    Roles =new List<RoleCategory>{RoleCategory.SysAdmin, RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.Partner }
                },

                // Program session
                 new ActionRole
                {
                    Action = JbAction.ProgramSession_List_View,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Owner, RoleCategory.Instructor, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.ProgramSession_Edit,
                    Roles = new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.ProgramSession_Delete,
                    Roles = new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Waitlist_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.SchoolContributor}
                },
                 new ActionRole
                {
                    Action = JbAction.Program_AssignInstructors,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager, RoleCategory.RestrictedManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Program_AssignRoom,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Overview_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Clubs_View,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.SysAdmin }
                },
                 new ActionRole
                {
                    Action = JbAction.Clubs_Add,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.SysAdmin }
                },
                //Reports
                 new ActionRole
                {
                    Action = JbAction.Reports_RegistrationData,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_Finance,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_Capacities,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_CapacitiesGraph,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_Custom,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_MobileApp,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                  new ActionRole
                {
                    Action = JbAction.Reports_Subscription,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager}
                },
                   new ActionRole
                {
                    Action = JbAction.Reports_Medical,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager}
                },
                   new ActionRole
                {
                    Action = JbAction.Reports_Operations,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager}
                },
                      new ActionRole
                {
                    Action = JbAction.Reports_SignOut,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                      new ActionRole
                 {
                    Action = JbAction.Reports_InternalUse,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                      new ActionRole
                 {
                    Action = JbAction.Reports_Attendance,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Reports_PerRegistration,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_Checkin,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_SignOutSheet,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Reports_AttendanceSheetAdvanced,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_DismissalInformation,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_DailyDismissal,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_ProviderContactInformation,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_InstructorList,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_InstructorAssignments,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_ParentEmailsList,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_RoomAssignmentsAdmin,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.SchoolContributor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_StudentAttendance,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager}
                },
                new ActionRole
                {
                    Action = JbAction.Reports_StudentSummaryAttendance,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager}
                },
                new ActionRole
                {
                    Action = JbAction.Reports_CustomAttendance,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager}
                },
                new ActionRole
                {
                    Action = JbAction.Reports_Rosters,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_Rosters_Admin,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager  }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_CampRoster,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner,RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager  }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_Rosters_Instructor,
                    Roles =new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Owner, RoleCategory.SchoolContributor, RoleCategory.Contributor, RoleCategory.Instructor, RoleCategory.OnsiteCoordinator, RoleCategory.OnsiteSupportManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Reports_Finance_School_Provider,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                 new ActionRole
                {
                    Action = JbAction.Reports_Finance_Coupon_Coordinator,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager,RoleCategory.SchoolContributor,RoleCategory.OnsiteCoordinator, RoleCategory.RestrictedManager }
                },
                  new ActionRole
                {
                    Action = JbAction.Reports_Finance_Provider,
                    Roles =new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_SystemFinance,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.SysAdmin }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_SystemFinance_Transaction,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.SysAdmin }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_SystemFinance_Revenue,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.SysAdmin }
                },
                 new ActionRole
                {
                    Action = JbAction.Reports_SystemFinance_Installments,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.SysAdmin }
                },
                new ActionRole
                {
                    Action = JbAction.Reports_SystemFinance_RevenuMatrix,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.SysAdmin }
                },
                  new ActionRole
                {
                    Action = JbAction.Reports_System_ClientCreditCards,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.SysAdmin }
                },
                      new ActionRole
                {
                    Action = JbAction.Reports_System_AutoCharge,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.SysAdmin }
                },
                  new ActionRole
                {
                    Action = JbAction.Reports_System_Installments,
                    Roles = new List<RoleCategory> { RoleCategory.Support, RoleCategory.SysAdmin }
                },

                new ActionRole
                {
                    Action = JbAction.Messaging_View,
                    Roles = new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Partner, RoleCategory.Admin,RoleCategory.Manager,RoleCategory.Owner, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.RestrictedManager }
                },
                new ActionRole
                {
                    Action = JbAction.WeeklyAgenda_View,
                    Roles = new List<RoleCategory>{ RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager }
                },
                new ActionRole
                {
                    Action = JbAction.WeeklyAgenda_Settings_View,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager }
                },
                new ActionRole
                {
                    Action = JbAction.WeeklyAgenda_Settings_Save,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Admin,RoleCategory.Manager }
                },
                 //
                new ActionRole
                {
                    Action = JbAction.Contact_View,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Partner }
                },
                new ActionRole
                {
                    Action = JbAction.Contact_Add,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Partner }
                },
                new ActionRole
                {
                    Action = JbAction.ContactSettings_View,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Partner }
                },
                new ActionRole
                {
                    Action = JbAction.ParentDashboard_View,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Partner, RoleCategory.Admin}
                },
                new ActionRole
                {
                    Action = JbAction.People_Delinquents_View,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Partner, RoleCategory.Admin}
                },
                new ActionRole
                {
                    Action = JbAction.People_Delinquents_SendMail,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Partner, RoleCategory.Admin}
                },
                new ActionRole
                {
                    Action = JbAction.People_Delinquents_ViewHistory,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Partner, RoleCategory.Admin}
                },
                new ActionRole
                {
                    Action = JbAction.People_ViewHistory_AddHistory,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Partner, RoleCategory.Admin}
                },
                new ActionRole
                {
                    Action = JbAction.People_ViewHistory_EditHistory,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Partner, RoleCategory.Admin}
                },
                new ActionRole
                {
                    Action = JbAction.People_ViewHistory_DeleteHistory,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Partner, RoleCategory.Admin}
                },
                new ActionRole
                {
                    Action = JbAction.People_ViewHistory_ViewEmail,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Partner, RoleCategory.Admin}
                },

                new ActionRole
                {
                    Action = JbAction.ActivityLog_View,
                    Roles = new List<RoleCategory>{  RoleCategory.Support, RoleCategory.Partner}
                },

            };
        }

        public List<ActionRole> GetActionRoles(List<RoleCategory> roles)
        {
            return GetActionRoles();
        }

        public List<ActionRole> GetActionRoles(RoleCategory role)
        {
            return GetActionRoles().Where(a => a.Roles.Contains(role)).ToList();
        }

        public List<RoleCategory> GetRolesOfAction(JbAction jbAction)
        {
            return GetActionRoles().Single(a => a.Action == jbAction).Roles;
        }

        public List<RoleCategory> GetRolesOfActions(List<JbAction> jbActions)
        {
            return GetActionRoles().Where(a => jbActions.Contains(a.Action)).SelectMany(s => s.Roles).Distinct().ToList();
        }

        public List<RoleCategory> GetManageableDasboardRolesWithRole(RoleCategory managerRole, ClubType clubtype)
        {
            var result = new List<RoleCategory>();

            var isClubSchool = false;
            var isClubProvider = false;
            var isIndividualClub = clubtype.EnumType == ClubTypesEnum.SportClub ||
                                   clubtype.EnumType == ClubTypesEnum.Individual_3;

            isClubSchool = _clubBusiness.IsClubSchool(clubtype);


            isClubProvider = _clubBusiness.IsClubProvider(clubtype);


            if (managerRole == RoleCategory.Partner)
            {
                result.AddRange(new List<RoleCategory>() { RoleCategory.Partner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.OnsiteSupportManager, RoleCategory.OnsiteCoordinator, RoleCategory.RestrictedManager });
            }

            if (managerRole == RoleCategory.Owner)
            {
                result.AddRange(new List<RoleCategory>() { RoleCategory.Owner, RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.RestrictedManager });
            }

            if (managerRole == RoleCategory.RestrictedManager)
            {
                result.AddRange(new List<RoleCategory>() { RoleCategory.Instructor });
            }

            if (managerRole == RoleCategory.Admin)
            {
                result.AddRange(new List<RoleCategory>() { RoleCategory.Admin, RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.RestrictedManager });

                if (!isClubSchool)
                {
                    if (clubtype.EnumType == ClubTypesEnum.SchoolDistrict)
                    {
                        result.Add(RoleCategory.OnsiteSupportManager);
                    }
                }

                if (isClubProvider || isClubSchool)
                {
                    result.Add(RoleCategory.OnsiteSupportManager);
                    result.Add(RoleCategory.OnsiteCoordinator);

                }

                if (isIndividualClub)
                {
                    result.Add(RoleCategory.OnsiteCoordinator);
                }

                result.Add(RoleCategory.Instructor);
            }

            if (managerRole == RoleCategory.Manager)
            {
                result.AddRange(new List<RoleCategory>() { RoleCategory.Manager, RoleCategory.Contributor, RoleCategory.RestrictedManager });
            }


            if (managerRole == RoleCategory.SchoolContributor)
            {
                result.AddRange(new List<RoleCategory>() { RoleCategory.Contributor });
            }
            if (managerRole == RoleCategory.OnsiteCoordinator)
            {
                result.AddRange(new List<RoleCategory>() { RoleCategory.OnsiteCoordinator });
            }
            if (managerRole == RoleCategory.OnsiteSupportManager)
            {
                result.AddRange(new List<RoleCategory>() { RoleCategory.OnsiteSupportManager });
            }

            if (isClubSchool)
            {
                result.Add(RoleCategory.SchoolContributor);
            }

            return result;
        }
    }
}