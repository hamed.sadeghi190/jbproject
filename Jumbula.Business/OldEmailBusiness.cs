﻿using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Business
{
    public class OldEmailBusiness : IOldEmailBusiness
    {
        private readonly bool _sendEmailToSupport;
        private readonly string _host;
        private readonly int _port;
        private readonly bool _enableSsl;
        private readonly NetworkCredential _networkCredential;
        private readonly IStorageBusiness _storageBusiness;
        private readonly IPlayerProfileBusiness _playerProfileBusiness;
        private readonly IEntitiesContext _dataContext;
        private readonly IRepository<Email> _emailRepository;

        public OldEmailBusiness(bool sendEmailToSupport, SmtpSection config, IStorageBusiness storageBusiness, IPlayerProfileBusiness playerProfileBusiness, IEntitiesContext dataContext, IRepository<Email> emailRepository)
        {

            _sendEmailToSupport = sendEmailToSupport;
            _storageBusiness = storageBusiness;
            _playerProfileBusiness = playerProfileBusiness;
            _emailRepository = emailRepository;
            _dataContext = dataContext;

            if (!System.Web.HttpContext.Current.Request.IsLocal)
            {
                _host = config.Network.Host;
                _port = config.Network.Port;
                _enableSsl = config.Network.EnableSsl;
                _networkCredential = new NetworkCredential(config.Network.UserName, config.Network.Password);
            }
            else
            {
                _host = "smtp.gmail.com";
                _port = 587;
                _enableSsl = true;
                _networkCredential = new NetworkCredential("jumbulatestemail@gmail.com", "jumbulatest12345678");
            }
        }

        public void SendEmail(int clubId, string resiver, string subject, string content)
        {
            throw new NotImplementedException();
        }

        public void SendEmail(MailAddress from, MailAddress to, string subj, string body, bool isBodyHtml, MailAddress bcc = null, List<MailAddress> cc = null, string clubDomain = null, string fileNames = null)
        {
            var storageManager = _storageBusiness;

            try
            {
                using (SmtpClient smtpclient = new SmtpClient(_host, _port))
                {
                    smtpclient.Credentials = _networkCredential;
                    smtpclient.EnableSsl = _enableSsl;

                    using (MailMessage mail = new MailMessage(from, to))
                    {
                        mail.Subject = subj;
                        mail.Body = body;

                        mail.IsBodyHtml = isBodyHtml;

                        if (bcc != null)
                        {
                            mail.Bcc.Add(bcc);
                        }
                        if (cc != null)
                        {
                            foreach (var item in cc)
                            {
                                mail.CC.Add(item);
                            }
                        }
                        string[] fileNameList;
                        if (!string.IsNullOrEmpty(fileNames))
                        {
                            fileNameList = fileNames.Split('*');

                            foreach (var fileName in fileNameList)
                            {
                                var path = storageManager.GetUri(Constants.Path_EmailAttachments, clubDomain, fileName).AbsoluteUri;
                                if (storageManager.IsExist(path))
                                {
                                    var stream = storageManager.DownloadBlobAsStream(path, string.Empty);
                                    mail.Attachments.Add(new Attachment(stream, fileName));
                                }
                            }
                        }

                        smtpclient.Send(mail);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SendEmail(MailAddress from, MailAddress to, MailAddress replyTo, string subj, string body, bool isBodyHtml)
        {
            var storageManager = _storageBusiness;

            try
            {
                using (SmtpClient smtpclient = new SmtpClient(_host, _port))
                {
                    smtpclient.Credentials = _networkCredential;
                    smtpclient.EnableSsl = _enableSsl;

                    using (MailMessage mail = new MailMessage(from, to))
                    {
                        mail.Subject = subj;
                        mail.Body = body;
                        mail.IsBodyHtml = isBodyHtml;
                        mail.ReplyTo = replyTo;

                        smtpclient.Send(mail);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void SendEmailAsync(MailAddress from, MailAddress to, MailAddress replyTo, string subj, string body, bool isBodyHtml)
        {
            var storageManager = _storageBusiness;

            try
            {
                using (SmtpClient smtpclient = new SmtpClient(_host, _port))
                {
                    smtpclient.Credentials = _networkCredential;
                    smtpclient.EnableSsl = _enableSsl;

                    using (MailMessage mail = new MailMessage(from, to))
                    {
                        mail.Subject = subj;
                        mail.Body = body;
                        mail.IsBodyHtml = isBodyHtml;
                        mail.ReplyTo = replyTo;

                        EventWaitHandle handle = new EventWaitHandle(false, EventResetMode.AutoReset);

                        smtpclient.SendAsync(mail, Guid.NewGuid());

                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public void SendReservationEmail(List<string> toPrimaryPlayerEmail, List<string> toSecondaryPlayerEmail, List<string> toClubOwner, List<string> toOutSourcerCLubOwner, string subject, string body, string clubOwnerName)
        {

            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubOwnerName);
            List<MailAddress> ccList = null;
            if (toSecondaryPlayerEmail != null && toSecondaryPlayerEmail.Count() > 0)
            {
                ccList = toSecondaryPlayerEmail.Select(c => new MailAddress(c)).ToList();
            }
            foreach (var playerEmail in toPrimaryPlayerEmail)
            {
                SendEmail(fromClubOwner, new MailAddress(playerEmail), subject, body, true, null, ccList);
            }

            subject += " for " + string.Join(",", toPrimaryPlayerEmail.ToArray());

            if (_sendEmailToSupport)
            {
                SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), subject, body, true);
            }

            List<MailAddress> ccListClubOwner = new List<MailAddress>();
            if (toClubOwner != null && toClubOwner.Any())
            {
                var primaryClubEmail = toClubOwner[0];
                toClubOwner.Remove(primaryClubEmail);
                if (toClubOwner.Count() > 0)
                {
                    ccListClubOwner.AddRange(toClubOwner.Select(c => new MailAddress(c)));
                }

                if (toOutSourcerCLubOwner != null && toOutSourcerCLubOwner.Count() > 0)
                {
                    ccListClubOwner.AddRange(toOutSourcerCLubOwner.Select(c => new MailAddress(c)));
                }

                SendEmail(fromClubOwner, new MailAddress(primaryClubEmail), subject, body, true, null, ccListClubOwner);
            }
        }

        public void SendInstallmentConfirmationEmail(string primaryPlayerEmail, List<string> clubOwnerEmail, string outSourcerClubOwnerEmail, string emailSubject, string body, string clubName)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubName);

            SendEmail(fromClubOwner, new MailAddress(primaryPlayerEmail), emailSubject, body, true, null, null);

            emailSubject += " from " + primaryPlayerEmail;

            if (_sendEmailToSupport)
            {
                SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, body, true);
            }
            List<MailAddress> ccListClubOwner = new List<MailAddress>();
            if (clubOwnerEmail != null)
            {
                var primaryClubEmail = clubOwnerEmail[0];
                clubOwnerEmail.Remove(primaryClubEmail);
                if (clubOwnerEmail.Count() > 0)
                {
                    ccListClubOwner.AddRange(clubOwnerEmail.Select(c => new MailAddress(c)));
                }


                SendEmail(fromClubOwner, new MailAddress(primaryClubEmail), emailSubject, body, true, null, ccListClubOwner);
            }
        }
        public void SendRegisterErrorEmail(string primaryPlayerEmail, List<string> clubOwnerEmail, string outSourcerClubOwnerEmail, string emailSubject, string body, string clubName)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubName);

            List<MailAddress> ccListClubOwner = new List<MailAddress>();
            if (clubOwnerEmail != null)
            {
                var primaryClubEmail = clubOwnerEmail[0];
                clubOwnerEmail.Remove(primaryClubEmail);
                if (clubOwnerEmail.Count() > 0)
                {
                    ccListClubOwner.AddRange(clubOwnerEmail.Select(c => new MailAddress(c)));
                }


                SendEmail(fromClubOwner, new MailAddress(primaryClubEmail), emailSubject, body, true, null, ccListClubOwner);
            }
        }
        public void SendClientBillingNotification(List<string> clubOwnerEmail, string emailSubject, string body)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, "Jumbula");

            if (_sendEmailToSupport)
            {
                SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, body, true);
            }
            List<MailAddress> ccListClubOwner = new List<MailAddress>();
            if (clubOwnerEmail != null)
            {
                var primaryClubEmail = clubOwnerEmail[0];
                clubOwnerEmail.Remove(primaryClubEmail);
                if (clubOwnerEmail.Count() > 0)
                {
                    ccListClubOwner.AddRange(clubOwnerEmail.Select(c => new MailAddress(c)));
                }


                SendEmail(fromClubOwner, new MailAddress(primaryClubEmail), emailSubject, body, true, null, ccListClubOwner);
            }

        }
        public void SendECheckClearedNotification(List<string> clubOwnerEmail, string emailSubject, string body, string clubName)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubName);

            if (_sendEmailToSupport)
            {
                SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, body, true);
            }
            List<MailAddress> ccListClubOwner = new List<MailAddress>();
            if (clubOwnerEmail != null)
            {
                var primaryClubEmail = clubOwnerEmail[0];
                clubOwnerEmail.Remove(primaryClubEmail);
                if (clubOwnerEmail.Count() > 0)
                {
                    ccListClubOwner.AddRange(clubOwnerEmail.Select(c => new MailAddress(c)));
                }


                SendEmail(fromClubOwner, new MailAddress(primaryClubEmail), emailSubject, body, true, null, ccListClubOwner);
            }

        }
        public void SendPreApprovalEmail(string primaryPlayerEmail, List<string> clubOwnerEmail, string emailSubject, string body, string clubName)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubName);



            SendEmail(fromClubOwner, new MailAddress(primaryPlayerEmail), emailSubject, body, true, null, null);

            emailSubject += " from " + primaryPlayerEmail;

            if (_sendEmailToSupport)
            {
                SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, body, true);
            }
            List<MailAddress> ccListClubOwner = new List<MailAddress>();
            if (clubOwnerEmail != null)
            {
                var primaryClubEmail = clubOwnerEmail[0];
                clubOwnerEmail.Remove(primaryClubEmail);
                if (clubOwnerEmail.Count() > 0)
                {
                    ccListClubOwner.AddRange(clubOwnerEmail.Select(c => new MailAddress(c)));
                }


                SendEmail(fromClubOwner, new MailAddress(primaryClubEmail), emailSubject, body, true, null, ccListClubOwner);
            }

        }

        public void SendProviderRespondToInvitation(List<string> clubOwnerEmail, string emailSubject, string body, string clubName)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubName);


            if (_sendEmailToSupport)
            {
                SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, body, true);
            }
            List<MailAddress> ccListClubOwner = new List<MailAddress>();
            if (clubOwnerEmail != null)
            {
                var primaryClubEmail = clubOwnerEmail[0];
                clubOwnerEmail.Remove(primaryClubEmail);
                if (clubOwnerEmail.Count() > 0)
                {
                    ccListClubOwner.AddRange(clubOwnerEmail.Select(c => new MailAddress(c)));
                }


                SendEmail(fromClubOwner, new MailAddress(primaryClubEmail), emailSubject, body, true, null, ccListClubOwner);
            }
        }
        public void SendInstallmentReminderEmail(string primaryPlayerEmail, List<string> clubOwnerEmail, string emailSubject, string body, string clubName)
        {
            try
            {
                var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubName);

                SendEmail(fromClubOwner, new MailAddress(primaryPlayerEmail), emailSubject, body, true, null, null);

                emailSubject += " for " + primaryPlayerEmail;

                if (_sendEmailToSupport)
                {
                    SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, body, true);
                }
                List<MailAddress> ccListClubOwner = new List<MailAddress>();
                if (clubOwnerEmail == null) return;
                var primaryClubEmail = clubOwnerEmail[0];
                clubOwnerEmail.Remove(primaryClubEmail);
                if (clubOwnerEmail.Any())
                {
                    ccListClubOwner.AddRange(clubOwnerEmail.Select(c => new MailAddress(c)));
                }

                SendEmail(fromClubOwner, new MailAddress(primaryClubEmail), emailSubject, body, true, null, ccListClubOwner);
            }
            catch (FormatException ex)
            {
                var clubOwnerEmails = clubOwnerEmail != null ? String.Join(", ", clubOwnerEmail.ToArray()) : "clubOwnerEmail is Null";
                var message = $"{primaryPlayerEmail} {clubOwnerEmails} {emailSubject} {body} {clubName}";
                LogHelper.LogException(ex, message);
            }
            catch (Exception ex)
            {

                var clubOwnerEmails = clubOwnerEmail != null ? String.Join(", ", clubOwnerEmail.ToArray()) : "clubOwnerEmail is Null";
                var message = $"{primaryPlayerEmail} {clubOwnerEmails} {emailSubject} {body} {clubName}";
                LogHelper.LogException(ex, message);
            }
        }

        public void SendEmail(string from, string to, string subj, string body, bool isBodyHtml, bool sendToSupport = false)
        {

            MailAddress sendTo = new MailAddress(to);
            MailAddress sendFrom = new MailAddress(from);

            SendEmail(sendFrom, sendTo, subj, body, isBodyHtml);

            if (_sendEmailToSupport && sendToSupport)
            {
                SendEmail(sendFrom, new MailAddress(Constants.W_BCC_EmailNotification), subj, body, isBodyHtml);
            }

        }

        public void SendConfirmationEmail(MailAddress from, MailAddress to, string subject, string body)
        {
            SendEmail(from.ToString(), to.Address, subject, body, true);
        }

        public void SendErrorMessages(string primaryemail, string body)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, "Scheduler");
            SendEmail(fromClubOwner, new MailAddress(primaryemail), "Scheduler Result", body, true);
        }
        public void SendUpdateRasInstallment(string primaryemail, string body)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, "RAS payment plan scheduler");
            SendEmail(fromClubOwner, new MailAddress(primaryemail), "RAS payment plan scheduler", body, true);
        }
        public void SendInvoiceForAdminEmail(string primaryPlayerEmail, List<string> clubOwnerEmail, string emailSubject, string body, string clubName)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubName);

            if (_sendEmailToSupport)
            {
                SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, body, true);
            }
            List<MailAddress> ccListClubOwner = new List<MailAddress>();
            if (clubOwnerEmail != null)
            {
                var primaryClubEmail = clubOwnerEmail[0];
                clubOwnerEmail.Remove(primaryClubEmail);
                if (clubOwnerEmail.Count() > 0)
                {
                    ccListClubOwner.AddRange(clubOwnerEmail.Select(c => new MailAddress(c)));
                }


                SendEmail(fromClubOwner, new MailAddress(primaryClubEmail), emailSubject, body, true, null, ccListClubOwner);
            }

        }

        public void SendTakePaymentForAdminEmail(string primaryPlayerEmail, List<string> clubOwnerEmail, string emailSubject, string body, string clubName)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubName);

            if (_sendEmailToSupport)
            {
                SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, body, true);
            }
            List<MailAddress> ccListClubOwner = new List<MailAddress>();
            if (clubOwnerEmail != null)
            {
                var primaryClubEmail = clubOwnerEmail[0];
                clubOwnerEmail.Remove(primaryClubEmail);
                if (clubOwnerEmail.Count() > 0)
                {
                    ccListClubOwner.AddRange(clubOwnerEmail.Select(c => new MailAddress(c)));
                }


                SendEmail(fromClubOwner, new MailAddress(primaryClubEmail), emailSubject, body, true, null, ccListClubOwner);
            }

        }

        public void SendTakePaymentForParentEmail(string primaryPlayerEmail, string emailSubject, string body, string clubName, List<MailAddress> cc = null)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubName);

            SendEmail(fromClubOwner, new MailAddress(primaryPlayerEmail), emailSubject, body, true, null);

            if (_sendEmailToSupport)
            {
                SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, body, true);
            }


        }
        public void SendInvoiceForParentEmail(string primaryPlayerEmail, string emailSubject, string body, string clubName, List<MailAddress> cc = null)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubName);

            SendEmail(fromClubOwner, new MailAddress(primaryPlayerEmail), emailSubject, body, true, null, new List<MailAddress>(cc));

            if (_sendEmailToSupport)
            {
                SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, body, true);
            }


        }


        public void SendInvoicePaymentForAdminEmail(string primaryPlayerEmail, List<string> clubOwnerEmail, string outSourcerClubOwnerEmail, string emailSubject, string body, string clubName)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubName);

            if (_sendEmailToSupport)
            {
                SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, body, true);
            }
            List<MailAddress> ccListClubOwner = new List<MailAddress>();
            if (clubOwnerEmail != null)
            {
                var primaryClubEmail = clubOwnerEmail[0];
                clubOwnerEmail.Remove(primaryClubEmail);
                if (clubOwnerEmail.Count() > 0)
                {
                    ccListClubOwner.AddRange(clubOwnerEmail.Select(c => new MailAddress(c)));
                }


                SendEmail(fromClubOwner, new MailAddress(primaryClubEmail), emailSubject, body, true, null, ccListClubOwner);
            }

        }
        public void SendInvoicePaymentForParentEmail(string primaryPlayerEmail, List<string> clubOwnerEmail, string outSourcerClubOwnerEmail, string emailSubject, string body, string clubName, List<MailAddress> cc = null)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubName);

            SendEmail(fromClubOwner, new MailAddress(primaryPlayerEmail), emailSubject, body, true, null, new List<MailAddress>(cc));


            if (_sendEmailToSupport)
            {
                SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, body, true);
            }

        }

        public void SendCancellationOrReminderEmailToAdmin(string primaryPlayerEmail, List<string> clubOwnerEmail, string emailSubject, string body, string clubName)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubName);


            //SendEmail(fromClubOwner, new MailAddress(primaryPlayerEmail), emailSubject, body, true, null, null);

            if (_sendEmailToSupport)
            {
                SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, body, true);
            }

            List<MailAddress> ccListClubOwner = new List<MailAddress>();
            if (clubOwnerEmail != null)
            {
                var primaryClubEmail = clubOwnerEmail[0];
                clubOwnerEmail.Remove(primaryClubEmail);
                if (clubOwnerEmail.Count() > 0)
                {
                    ccListClubOwner.AddRange(clubOwnerEmail.Select(c => new MailAddress(c)));
                }


                SendEmail(fromClubOwner, new MailAddress(primaryClubEmail), emailSubject, body, true, null, ccListClubOwner);
            }
        }
        public void SendCancellationOrReminderEmailToParent(string primaryPlayerEmail, string emailSubject, string body, string clubName, List<MailAddress> cc = null)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubName);

            SendEmail(fromClubOwner, new MailAddress(primaryPlayerEmail), emailSubject, body, true, null, new List<MailAddress>(cc));

            if (_sendEmailToSupport)
            {
                SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, body, true);
            }
        }

        public void SendWaitListForAdmin(MailAddress fromClubOwner, List<string> clubOwnerEmail, string emailSubject, string body)
        {

            List<MailAddress> ccListClubOwner = new List<MailAddress>();
            if (clubOwnerEmail != null)
            {
                var primaryClubEmail = clubOwnerEmail[0];
                clubOwnerEmail.Remove(primaryClubEmail);
                if (clubOwnerEmail.Count() > 0)
                {
                    ccListClubOwner.AddRange(clubOwnerEmail.Select(c => new MailAddress(c)));
                }

                SendEmail(fromClubOwner, new MailAddress(primaryClubEmail), emailSubject, body, true, null, ccListClubOwner);

            }

        }

        public void SendEmailForErrorSendGrid(string emailSubject, string body, string clubName)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubName);


            if (_sendEmailToSupport)
            {
                SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, body, true);
            }
        }

        public void SendWeeklyAgendaEmail(List<MailAddress> receivers, MailAddress sender, string subject, string body)
        {

            foreach (var mail in receivers)
            {
                SendEmail(sender, mail, sender, subject, body, true);
            }





        }

        public void SendProgramParentsEmail(List<JbUser> programParents, MailAddress sender, string subject, string body, IEnumerable<OrderItem> programOrderItems)
        {
            IQueryable<PlayerProfile> parentChildren = null;
            var parentEmail = "";
            var tempbody = body;
            foreach (var parent in programParents)
            {
                parentEmail = parent.UserName;
                var playerparent = _playerProfileBusiness.GetList().Where(p => p.UserId == parent.Id);
                parentChildren = playerparent.Where(u => u.Relationship == RelationshipType.Registrant);
                var programParentChildren = parentChildren.ToList().Where(p => programOrderItems.ToList().Any(o => o.PlayerId == p.Id)).ToList();

                foreach (var child in programParentChildren)
                {
                    tempbody = GetParametersFromContact(body, "", "", child.Contact.FullName, "", "", "");

                    SendEmail(sender, new MailAddress(parentEmail), sender, subject, tempbody, true);
                }
            }



        }

        public OperationStatus Create(Email email)
        {
            return _emailRepository.Create(email);
        }

        public OperationStatus Create(List<Email> emails)
        {
            _dataContext.Set<Email>().AddRange(emails);

            return _emailRepository.Save();
        }

        public IQueryable<Email> GetList()
        {
            return _emailRepository.GetList();
        }


        public void SendSubscriptionForAdminEmail(string primaryPlayerEmail, List<string> clubOwnerEmail, string emailSubject, string body, string clubName)
        {
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, clubName);
            SendEmail(fromClubOwner, new MailAddress(primaryPlayerEmail), emailSubject, body, true, null);

            if (_sendEmailToSupport)
            {
                SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, body, true);
            }
            List<MailAddress> ccListClubOwner = new List<MailAddress>();
            if (clubOwnerEmail != null)
            {
                var primaryClubEmail = clubOwnerEmail[0];
                clubOwnerEmail.Remove(primaryClubEmail);
                if (clubOwnerEmail.Count() > 0)
                {
                    ccListClubOwner.AddRange(clubOwnerEmail.Select(c => new MailAddress(c)));
                }


                SendEmail(fromClubOwner, new MailAddress(primaryClubEmail), emailSubject, body, true, null, ccListClubOwner);
            }

        }

        public string GetParametersFromContact(string body, string className, string seasonName, string childName, string clubName, string providerName, string partnerName)
        {
            Regex x = new Regex("(\\%)(.*?)(\\%)");

            var parameters = x.Split(body).ToList();
            foreach (var item in parameters)
            {
                body = FineWithParemetersInContent(body, item, className, seasonName, childName, clubName, providerName, partnerName);
            }
            return body;
        }

        public string FineWithParemetersInContent(string body, string param, string className, string seasonName, string childName, string clubName, string providerName, string partnerName)
        {
            switch (param.ToLower())
            {
                case "classname":
                    body = SetClassNameInContent(body, className);
                    return body;
                case "seasonname":
                    body = SetSeasonNameInContent(body, seasonName);
                    return body;
                case "childname":
                    body = SetChildNameInContent(body, childName);
                    return body;
                case "organizationname":
                    body = SetClubNameInContent(body, clubName);
                    return body;
                case "providername":
                    body = SetProviderNameInContent(body, providerName);
                    return body;
                case "partnername":
                    body = SetPartnerNameInContent(body, partnerName);
                    return body;
                default:
                    return body;
            }
        }
        private string SetClassNameInContent(string body, string className)
        {
            body = !string.IsNullOrEmpty(className) ? Regex.Replace(body, "%ClassName%", className, RegexOptions.IgnoreCase) : body;
            return body;
        }

        private string SetSeasonNameInContent(string body, string seasonName)
        {
            body = !string.IsNullOrEmpty(seasonName) ? Regex.Replace(body, "%SeasonName%", seasonName, RegexOptions.IgnoreCase) : body;
            return body;
        }

        private string SetChildNameInContent(string body, string childName)
        {
            body = !string.IsNullOrEmpty(childName) ? Regex.Replace(body, "%ChildName%", childName, RegexOptions.IgnoreCase) : body;
            return body;
        }

        private string SetClubNameInContent(string body, string clubname)
        {
            body = !string.IsNullOrEmpty(clubname) ? Regex.Replace(body, "%ORGANIZATIONNAME%", clubname, RegexOptions.IgnoreCase) : body;
            return body;
        }
        private string SetProviderNameInContent(string body, string providername)
        {
            body = !string.IsNullOrEmpty(providername) ? Regex.Replace(body, "%providername%", providername, RegexOptions.IgnoreCase) : body;
            return body;
        }
        private string SetPartnerNameInContent(string body, string partnerName)
        {
            body = !string.IsNullOrEmpty(partnerName) ? Regex.Replace(body, "%partnername%", partnerName, RegexOptions.IgnoreCase) : body;
            return body;
        }

    }
}
