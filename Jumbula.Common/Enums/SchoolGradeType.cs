﻿

using Jumbula.Common.Attributes;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jumbula.Common.Enums
{
    public enum SchoolGradeType
    {
        [Abbreviation("None")]
        [EnumOrder(1)]
        [Description("Not in school")]
        [Display(Name = "Not in school")]
        NotInSchool = 1,

        [Abbreviation("Pre")]
        [EnumOrder(4)]
        [Description("Preschool")]
        [Display(Name = "Preschool")]
        PreSchool = 2,

        [Abbreviation("Pre-3")]
        [EnumOrder(2)]
        [Description("Preschool - 3")]
        [Display(Name = "Preschool - 3")]
        PreSchool3 = 20,

        [Abbreviation("Pre-4")]
        [EnumOrder(3)]
        [Description("Preschool - 4")]
        [Display(Name = "Preschool - 4")]
        PreSchool4 = 21,

        [Abbreviation("K")]
        [EnumOrder(5)]
        [Description("Kindergarten")]
        [Display(Name = "Kindergarten")]
        Kindergarten = 3,

        [Abbreviation("1")]
        [EnumOrder(6)]
        [Description("1")]
        [Display(Name = "1")]
        S1 = 4,

        [Abbreviation("2")]
        [EnumOrder(7)]
        [Description("2")]
        [Display(Name = "2")]
        S2 = 5,

        [Abbreviation("3")]
        [EnumOrder(8)]
        [Description("3")]
        [Display(Name = "3")]
        S3 = 6,

        [Abbreviation("4")]
        [EnumOrder(9)]
        [Description("4")]
        [Display(Name = "4")]
        S4 = 7,

        [Abbreviation("5")]
        [EnumOrder(10)]
        [Description("5")]
        [Display(Name = "5")]
        S5 = 8,

        [Abbreviation("6")]
        [EnumOrder(11)]
        [Description("6")]
        [Display(Name = "6")]
        S6 = 9,

        [Abbreviation("7")]
        [EnumOrder(12)]
        [Description("7")]
        [Display(Name = "7")]
        S7 = 10,

        [Abbreviation("8")]
        [EnumOrder(13)]
        [Description("8")]
        [Display(Name = "8")]
        S8 = 11,

        [Abbreviation("9")]
        [EnumOrder(14)]
        [Description("9")]
        [Display(Name = "9")]
        S9 = 12,

        [Abbreviation("10")]
        [EnumOrder(15)]
        [Description("10")]
        [Display(Name = "10")]
        S10 = 13,

        [Abbreviation("11")]
        [EnumOrder(16)]
        [Description("11")]
        [Display(Name = "11")]
        S11 = 14,

        [Abbreviation("12")]
        [EnumOrder(17)]
        [Description("12")]
        [Display(Name = "12")]
        S12 = 15,

        [Abbreviation("College")]
        [EnumOrder(18)]
        [Description("College")]
        [Display(Name = "College")]
        College = 16
    }
}
