﻿using System.Collections.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SelectedProvidersViewModel
    {
        public SelectedProvidersViewModel()
        {
            // do some things good.
        }

        public List<int> _SelectedProviders { get; set; }

        public int ReportId { get; set; }

        public string ReportName { get; set; }

        public string SeasonDomain { get; set; }

        public bool IsAllProviders { get; set; }

        public string Term { get; set; }

        public IncludeForms IncludeForms { get; set; }

        public ReportFilters Filters { get; set; }
    }

}