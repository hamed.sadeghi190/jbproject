﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('overviewController', ['$scope', '$http', '$stateParams', function ($scope, $http, $stateParams) {
        window.app.globalObjects.initializeView();
        $scope.MC.closeAllBlades();

        window.app.globalObjects.pageClass = 'dashboard-overview';

        $scope.MC.stateParams = $stateParams;
        $scope.Model = {};
    
        var baseUrl = window.location.origin + "/Dashboard/Overview";
        $scope.Model.FreeTrialMessage = '';
        $scope.Model.IsFreeTrialAccount = false;
        $scope.Model.HasPartner = false;
 
        $scope.gridHeight = 406;

       
        $scope.dismissSchedulingDemoPopup = function () { $scope.ShowSchedulingDemo = false; }

        $http.get(window.location.origin + "/Dashboard/OverView/GetAccountInfo")
            .success(function (data, status, headers, config) {
                $scope.Model.FreeTrialMessage = data.FreeTrialMessage;
                $scope.Model.IsFirstTime = data.IsFirstTime;
                $scope.Model.IsFreeTrialAccount = data.IsFreeTrialAccount;
                $scope.Model.HasPartner = data.HasPartner;

                if ($scope.Model.IsFreeTrialAccount && $scope.Model.IsFirstTime) {
                    $scope.ShowSchedulingDemo = true;
                }
               
                if (data.HasPartner) {
                    $scope.gridHeight = 550;
                    getAllPartnerSchools();
                }

                $scope.SearchTypes = data.SearchTypes;

                $scope.SearchType = 'Name';
                prepareGrid();
            });



        $http.get(window.location.origin + "/Dashboard/OverView/GetTotalSales")
            .success(function (data, status, headers, config) {
                $scope.Model.Totals = data;

                var totalSaleOptions = {
                    useEasing: true,
                    useGrouping: true,
                    separator: ',',
                    decimal: '.',
                    prefix: '$',
                    suffix: ''
                };
                var totalSaleCountUp = new countUp("totalSale", 0, $scope.Model.Totals.TotalSales, 2, 2.5, totalSaleOptions);
                totalSaleCountUp.start();

                var totalAttendeesOptions = {
                    useEasing: true,
                    useGrouping: true,
                    separator: ',',
                    decimal: '.',
                    prefix: '',
                    suffix: ''
                };
                var totalAttendeesCountUp = new countUp("totalAttendees", 0, $scope.Model.Totals.TotalAttendees, 0, 2.5, totalAttendeesOptions);
                totalAttendeesCountUp.start();
            });

        var prepareGrid = function () {
            var recentRegistrationDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport:
                    {
                        read: {
                            url: baseUrl + '/GetRecentRegistration',
                            cache: false
                        },
                        parameterMap: function (data, type) {
                            if (type === "read") {
                                return {
                                    term: $scope.searchTerm,
                                    searchType: $scope.SearchType,
                                    firstName: $scope.firstName,
                                    lastName: $scope.lastName,
                                    skip: data.skip,
                                    take: data.take,
                                    page: data.page,
                                    pageSize: data.pageSize,
                                    sort: data.sort,
                                    clubIds: $scope.Model._SelectedSchools
                                }
                            }
                        }
                    },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverFiltering: true,
                serverSorting: true,
                serverPaging: true,
                pageSize: 10,
            });

            $scope.recentRegistration = {
                dataSource: recentRegistrationDataSource,
                height: $scope.gridHeight - 100,
                scrollable: {
                    virtual: true
                },
                dataBound: function (e) {
                    var grid = $("#grid").data("kendoGrid");

                    if ($scope.Model.HasPartner) {
                        grid.showColumn("ClubName");
                    }
                },
                sortable: true,
                columns: [
                    {
                        field: "AttendeeName",
                        title: "Participant",
                        width: "100px",
                        template: "#= AttendeeName # #if(ItemStatusReason == 1) {# <i class='grid-label label info'>Transferred in</i> #}else if(ItemStatusReason == 2) {# <i class='grid-label label bg-red  fg-white'>Transferred out</i> #}else if(ItemStatusReason == 5) {# <i class='grid-label label bg-red fg-white'>Refund</i> #} else if (ItemStatusReason == 4){# <i class='grid-label label warning'>Canceled</i> #}#",
                        sortable: false
                    },
                    {
                        field: "ClubName",
                        title: "Member",
                        width: "90px",
                        sortable: false,
                        hidden: true
                    },
                    {
                        field: "ProgramName",
                        title: "Program",
                        width: "95px",
                        sortable: false
                    },
                    {
                        field: "Schedule",
                        title: "Schedule",
                        width: "120px",
                        sortable: false
                    },
                    {
                        field: "TuitionName",
                        title: "Tuition label",
                        width: "110px",
                        sortable: false
                    },
                    {
                        field: "DateTimeLable",
                        title: "Date",
                        template: "#= (kendo.toString( kendo.parseDate(DateTimeLable) , 'MMM/dd/yyyy')) == null ? DateTimeLable : kendo.toString( kendo.parseDate(DateTimeLable) , 'MMM/dd/yyyy') #",
                        width: "105px",
                        sortable: false
                    }, {
                        field: "ConfirmationId",
                        title: "Confirmation",
                        width: "120px",
                        sortable: false
                    },
                    {
                        title: "Actions",
                        width: "70px",
                        headerTemplate: "<span class='actions-title'>Actions</span>",
                        template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\<li> ... \
				    <ul>" +
                            "<li ngaction='OrderDetail_View' ui_sref='OrderItem({ seasonDomain:\"#= SeasonDomain #\",orderItemId:#= OrderItemId #,clubId:#= ClubId # })' class='k-item k-state-default k-first' role='menuitem'>\
						    <span class='k-link jbi-angle-right'>View detail</span>\
				      </li></ul>\
			            </li>\
		            </ul>",
                        attributes: {
                            class: "grid-actions"
                        },
                        sortable: false
                    }
                ]
            };
        }

        var recentMessagesDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: '/Message/GetRecentMessages',
                        cache: false
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: $scope.searchTerm,
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: data.sort
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverFiltering: true,
            serverSorting: true,
            serverPaging: true,
            pageSize: 10,
        });

        $scope.recentMessages = {
            dataSource: recentMessagesDataSource,
            height: $scope.gridHeight,
            scrollable: {
                virtual: true
            },
            dataBound: function () {
                //alert();
                $("#messagesGrid").find(".k-header").hide();
                $scope.TotalMessages = $scope.recentMessages.dataSource._total;
            },
            //sortable: true,
            columns: [
                {
                    template: "<a title='#= ClassName #', ui_sref='Messaging({ chatId:\"#= ChatId #\"})'>#= ClassName #</a>",
                },
                {
                    template: "<span title='#= SchoolSeasonName #', ng-class=\"{'text-bold':  #= IsUnread #}\">#= SchoolSeasonName #</span>",
                },
                {
                    template: "<span title='#= Day #', ng-class=\"{'text-bold': #= IsUnread #}\">#= Day #</span>",
                },
                {
                    template: "<span title='#= Grades #', ng-class=\"{'text-bold': #= IsUnread #}\">#= Grades #</span>",
                }
            ]
        };

        var schoolsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: '/Dashboard/Club/GetProvidersSchools',
                        cache: false
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: $scope.searchTerm,
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: data.sort
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverFiltering: true,
            serverSorting: true,
            serverPaging: true,
            pageSize: 10,
        });


        $scope.schools = {
            dataSource: schoolsDataSource,
            height: 400,
            scrollable: {
                virtual: true
            },
            dataBound: function () {
                $("#schoolsGrid").find(".k-header").hide();
            },
            columns: [
                {
                    template: "<p>#= Name #</p>",
                },
                {
                    template: "<p>#= County #</p>",
                },
            ]
        };


        $scope.search = function () {
            $("#grid").data("kendoGrid").dataSource.read();
        };

        function getAllPartnerSchools() {
            $http.get(window.location.origin + '/Dashboard/Club/GetAllMembers')
                .success(function (data, status, headers, config) {
                    $scope.AllMembers = data.AllMembers;
                    $scope.Model._SelectedSchools = [''];
                });
        }


    }]);
    angular.module('dashboardApp').controller('campaignsOverviewController', ['$scope', '$http', function ($scope, $http) {
        window.app.globalObjects.initializeView();

        window.app.globalObjects.pageClass = 'campaign-view';

        $scope.MC.openBlade({ url: "Dashboard/Blade/Campaigns", order: 1, menu: "CAMPAIGN" });
    }]);
})();