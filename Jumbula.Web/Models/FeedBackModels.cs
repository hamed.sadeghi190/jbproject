﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace SportsClub.Models
{
    public class GeneralFeedBackModel
    {
        [Required(ErrorMessage = "The Describe idea field is required.")]
        [DisplayName("Describe your idea")]
        public string DescribeIdea { get; set; }

        [DisplayName("Give a short summary")]
        public string ShortSummary { get; set; }

        [Required(ErrorMessage = "The Email address field is required.")]
        [EmailAddress]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "The email must be a valid e-mail address.")]
        [DisplayName("What is your email address?")]
        public string Email { get; set; }

        [DisplayName("What is your name?")]
        public string Name { get; set; }
    }
}