﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace Jumbula.Business.EmailBuilder
{
    public class RefundOrderEmailBuilder : BaseEmailBuilder<RefundEmailParameterModel>
    {
        #region Contstructors

        public RefundOrderEmailBuilder(IClubBusiness clubBusiness,
            IOrderItemBusiness orderItemBusiness,
            IOrderInstallmentBusiness orderInstallmentBusiness, IProgramBusiness programBusiness)
        {
            _clubBusiness = clubBusiness;
            _orderItemBusiness = orderItemBusiness;
            _orderInstallmentBusiness = orderInstallmentBusiness;
            _programBusiness = programBusiness;
        }

        #endregion

        #region Fields 
        private Club _club;

        private readonly IClubBusiness _clubBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IOrderInstallmentBusiness _orderInstallmentBusiness;
        private readonly IProgramBusiness _programBusiness;

        private OrderItem _orderItem;
        private OrderInstallment _orderInstallment;
        private decimal _refundedAmount;
        private string _entryFeeName;
        private string _name;
        private string _refundNote;
        private string _fullName;
        private bool _hasProgramScheduleId;
        private List<string> _clubOwnerEmails;
        #endregion

        #region Constants
        private const string AdminSubCategory = "Admin";
        private const string ParentSubCategory = "Parent";
        private const string Subject = "Refund confirmation";
        private const string AdminSubjectSplicer = "for";
        #endregion

        #region Properties
        public override EmailCategory Category => EmailCategory.RefundOrder;

        public override string TemplateName => "ReservationNotification";

        public override EmailCallerType CallerType => EmailCallerType.User;

        public override EmailSendPriority EmailSendPriority => EmailSendPriority.Normal;

        public override IEnumerable<string> SubCategories => new List<string> { AdminSubCategory, ParentSubCategory };
        #endregion

        #region Public Methods

        public override void Initiate(RefundEmailParameterModel parameters)
        {

            if (parameters.OrderItemId.HasValue)
            {
                _orderItem = _orderItemBusiness.GetItem(parameters.OrderItemId.Value);
            }
            else if (parameters.OrderInstallmentId.HasValue)
            {
                _orderInstallment = _orderInstallmentBusiness.Get(parameters.OrderInstallmentId.Value);
                _orderItem = _orderInstallment.OrderItem;
            }

            _club = _orderItem.Order.Club;
            _refundedAmount = parameters.RefundedAmount;
            _entryFeeName = _orderItem.EntryFeeName;
            if (_orderItem.Start != null)
                if (_orderItem.End != null)
                    _name = _orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare
                        ? _programBusiness.GetProgramNameWithScheduleTitleAndDate(_orderItem, _orderItem.EntryFeeName)
                        : _programBusiness.GetProgramName(_orderItem.ProgramSchedule.Program.Name,
                            _orderItem.Start.Value,
                            _orderItem.End.Value, _orderItem.EntryFeeName);
            _refundNote = parameters.RefundNote;
            _fullName = _orderItem.FullName;
            _hasProgramScheduleId = _orderItem.ProgramScheduleId.HasValue;

            _clubOwnerEmails = _clubBusiness.ClubOwnerEmailList(_club);
        }

        public override void InitiatePreview(RefundEmailParameterModel parameters)
        {

        }

        public override object BuildBodyModel(string subCategory, EmailBodyGenerationMode previewMode = EmailBodyGenerationMode.Normal)
        {
            var description = _hasProgramScheduleId ? _name : _entryFeeName;


            var result = new RefundEmailBodyModel
            {
                AppliedText =
                    $"{CurrencyHelper.FormatCurrencyWithPenny(_refundedAmount, _club.Currency)} has been refunded.",
                ClubName = _club.Name,
                ClubCurrency = _club.Currency,
                ClubEmail = _club.ContactPersons.Any() ? _club.ContactPersons.First().Email : string.Empty,
                ClubPhone = _club.ContactPersons.Any() ? PhoneNumberHelper.FormatPhoneNumber(_club.ContactPersons.First().Phone) : string.Empty,
                ClubDomain = _club.Domain,
                RefundNote = _refundNote,
                OrderConfirmationId = _orderItem.Order.ConfirmationId,
                OrderPaidDate = string.Format("{0} ({1})", DateTimeHelper.ConvertUtcDateTimeToLocal(DateTime.UtcNow, _club.TimeZone).ToString(Constants.DefaultDateTimeFormat), _club.TimeZone.ToString()),
                PaidAmount = CurrencyHelper.FormatCurrencyWithPenny(_refundedAmount, _club.Currency, true),
                PlayerFullName = _fullName,
                ClubImageUrl = _clubBusiness.GetClubLogo(_club),
                BodyMailTitle = Constants.PartialRefunReservationBodyTitle,
                ReservationItems = new List<RefundOrderEmailBodyItemModel>
                {
                    new RefundOrderEmailBodyItemModel(_club.Currency)
                    {
                            PlayerFullName = _hasProgramScheduleId ? _fullName : _name,
                            EventName = string.Format("{0}", description),
                            Price =CurrencyHelper.FormatCurrencyWithPenny(_refundedAmount, _club.Currency),
                    }
                },

            };

            return result;
        }

        public override MailAddress GetFrom(string subCategory = null)
        {
            return new MailAddress(Constants.W_NoReplyEmailAddress, _club.Name);
        }

        public override string GetFromDisplayName(string subCategory = null)
        {
            return _club.Name;
        }

        public override List<CommunicationHistory> GetHistories()
        {
            return new List<CommunicationHistory>
            {
                new CommunicationHistory{ RelatedEntity = nameof(OrderItem), RelatedEntityId = _orderItem.Id}
            };
        }

        public override string GetReplyTo(string subCategory = null)
        {
            return Constants.W_NoReplyEmailAddress;
        }

        public override string GetSubject(string subCategory = null)
        {
            var result = Subject;

            if (subCategory == AdminSubCategory)
                result = $"{result} {AdminSubjectSplicer} {_orderItem.Order.User.UserName}";

            return result;
        }

        public override IEnumerable<MailAddress> GetTos(string subCategory = null)
        {
            var result = new List<MailAddress>();

            if (subCategory == ParentSubCategory)
                result.Add(new MailAddress(_orderItem.Order.User.UserName));

            if (subCategory == AdminSubCategory)
                result = _clubOwnerEmails.Select(e => new MailAddress(e)).ToList();


            return result;
        }

        #endregion
    }
}
