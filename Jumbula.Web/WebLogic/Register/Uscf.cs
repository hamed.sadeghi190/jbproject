﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using HtmlAgilityPack;

namespace Jumbula.Web.WebLogic.Register
{
    public class Uscf
    {
        public string[] UscfMemSearch(string term)
        {
            try
            {
                string url = "http://www.uschess.org/msa/thin3.php?";

                var webGet = new HtmlWeb(); //use using to dispose?

                var document = webGet.Load(url + term, "GET");

                string[] names = new string[] { "memid", "memexpdt", "memname", "rating1", "rating2", "memfideid", "rating3" };
                string[] values = new string[7];

                for (int i = 0; i < names.Length; ++i)
                {
                    HtmlNode input = document.DocumentNode.SelectSingleNode(string.Format("//input[@name='{0}']", names[i]));

                    if (input != null)
                    {
                        values[i] = input.Attributes["value"].Value;
                    }

                    else // format has changed, do what?
                    {
                    }
                }

                // If Uscf Id was wrong, the call returns "Non-Member" after input memexpdt and all values except memid are empty.

                if (string.IsNullOrEmpty(values[1].Trim()))
                {
                    values[1] = "Non-Member";
                }

                return values;
            }

            catch (Exception e)
            {
                throw e;
            }
        }

        public List<UscfIdSearch> UscfIdSearch(string term)
        {
            try
            {
                string url = "http://www.uschess.org/msa/thin2.php";

                HtmlWeb.PreRequestHandler handler = delegate(HttpWebRequest request)
                {
                    AddPostData(request, term);
                    return true;
                };

                var webGet = new HtmlWeb(); //use using to dispose?

                webGet.PreRequest += handler;

                ///var document = webGet.Load("http://www.uschess.org/msa/thin3.php?13930217", "GET");
                var document = webGet.Load(url, "POST");

                HtmlNodeCollection trs = document.DocumentNode.SelectNodes("/html[1]/body[1]/table[2]/tr");

                var records = new List<string>();
                var recs = new List<UscfIdSearch>();

                // Nothing to do

                if (trs.Count == 0)
                {

                }

                // One row
                // Be careful, it might <td colspan="10">No matches found</td>
                // Otherwise if we have 3 <td>'s we have good records

                else if (trs.Count == 1)
                {
                    HtmlNodeCollection tds = trs[0].SelectNodes("td");

                    // A good record
                    // Id, first name last name, expiration rating

                    if (tds.Count == 3)
                    {
                        ProcessUscfId(recs, tds);
                    }
                }

                // More than one row
                // Always good records

                else
                {
                    foreach (var tr in trs)
                    {
                        ProcessUscfId(recs, tr.SelectNodes("td"));
                    }

                }


                return recs;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void ProcessUscfId(List<UscfIdSearch> records, HtmlNodeCollection tds)
        {
            if (tds.Count == 3) // Id, first name last name, expiration rating
            {
                string id = tds[0].InnerHtml;
                string name = tds[1].InnerHtml;
                //string expRat = tdNodes[2].InnerHtml;

                char[] mask = new char[] { '\n', ' ', '\t' };
                id.Trim(mask);
                name.Trim(mask);

                // string[] args = new string[] { id, name };

                records.Add(new UscfIdSearch { Name = name, Id = id });
                //records.Add(args);

            }
        }

        private void AddPostData(HttpWebRequest request, string term)
        {
            StringBuilder sb = new StringBuilder(1024); // 1024 good size?
            AppendArg(sb, "memfn", "");
            AppendArg(sb, "memln", term);
            AppendArg(sb, "memstate", "");
            AppendArg(sb, "mode", "Search");

            byte[] buff = Encoding.UTF8.GetBytes(sb.ToString());
            request.ContentLength = buff.Length;
            request.ContentType = "application/x-www-form-urlencoded";

            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(buff, 0, buff.Length);
            }
        }

        private void AppendArg(StringBuilder sb, string argLabel, string arg)
        {
            const char AmperSign = '&';  // move and merge with other files
            const char EqualSign = '=';


            if (sb.Length != 0)
            {
                sb.Append(AmperSign);
            }

            sb.Append(argLabel).Append(EqualSign).Append(HttpUtility.UrlEncode(arg));
        }
    }

    public class UscfIdSearch
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }

    public class UscfMemSearch
    {
        public string Id { get; set; }
        public string Expiration { get; set; }
        public string Name { get; set; }
        public string RatingReg { get; set; }
        public string RatingQuick { get; set; }
        public string IdFide { get; set; }
        public string RatingFide { get; set; }
    }
}