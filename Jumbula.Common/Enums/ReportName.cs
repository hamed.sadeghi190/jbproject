﻿using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum ReportName
    {
        [Description("Daily dismissal")]
        DailyDismissal,

        [Description("Camp roster")]
        CampRoster,

        [Description("Camp roster")]
        CampRosterPortal,

        [Description("Member list")]
        MemberList,

        [Description("Installment")]
        InstallmentPortal,

        [Description("Sign-Out Sheet")]
        SignOutSheet
    }
}
