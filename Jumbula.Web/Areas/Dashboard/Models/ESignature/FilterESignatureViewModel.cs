﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class FilterESignatureViewModel
    {
        public List<SelectKeyValue<string>> ESignatureTypes { get; set; }
    }
    public enum ESignatureType
    {
        All,
        [Description("In progress")]
        Inprogress,
        [Description("Completed")]
        Completed,
        [Description("Declined")]
        Declined,
        [Description("Voided")]
        Voided
    }
}