﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class GeneralDiscountViweModel : BaseViewModel<Discount, long>
    {
        public GeneralDiscountViweModel()
        {
            ApplyType = 0;
            //IsAllProgram = true;
            GeneralAmountType = SeedGeneralAmountType();

        }
        [MaxLength(200, ErrorMessage = "{0} cannot be longer than 200 characters.")]
        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        public ChargeDiscountCategory Category { get; set; }
        public string CategoryDescription
        {
            get { return Category.ToDescription(); }
        }
        public string Description { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Amount")]
        public decimal Amount { get; set; }
        public EligibleMode EligibleMode { get; set; }

        public SelectKeyValue<string> SelectedBenefit { get; set; }

        public List<SelectKeyValue<string>> BenefitItems { get; set; }
        public long SeasonId { get; set; }

        public bool IsDeleted { get; set; }

        [Required(ErrorMessage = "Select at least one program.")]
        public List<string> SelectedPrograms { get; set; }

        public List<SelectKeyValue<string>> ProgramsList { get; set; }
        public List<SelectKeyValue<string>> NopItems { get; set; }

        public SelectKeyValue<string> NOP { get; set; }
        public List<SelectKeyValue<string>> ConditionProgram { get; set; }
        public List<SelectKeyValue<string>> ApplyItems { get; set; }
        public SelectKeyValue<string> SelectedConditionProgram { get; set; }
        public List<SelectKeyValue<string>> ConditionParticipent { get; set; }
        public SelectKeyValue<string> SelectedConditionParticipent { get; set; }
        public int ApplyType { get; set; }
        public string SeasonDomain { get; set; }
        public List<SelectKeyValue<string>> IsAllPrograms { get; set; }
        public SelectKeyValue<string> SelectedProgramMode { get; set; }
        public List<SelectKeyValue<string>> GeneralAmountType { get; set; }
        public List<SelectKeyValue<string>> AllProgramSeason { get; set; }

        public SelectKeyValue<string> SelectedAmountType { get; set; }
        List<SelectKeyValue<string>> SeedGeneralAmountType()
        {
            var result = new List<SelectKeyValue<string>>();
            result.Add(new SelectKeyValue<string>() { Text = "Select", Value = "-1" });
            foreach (ChargeDiscountType payMethod in Enum.GetValues(typeof(ChargeDiscountType)))
            {
                result.Add(new SelectKeyValue<string>() { Text = payMethod.ToDescription(), Value = ((int)payMethod).ToString() });
            }

            return result;
        }
          
    }
    public enum ProgramConditions : byte
    {
        [Description("Select")]
        SelectCondition = 0,
        [Description("No condition")]
        NoCondition = 1,
        //[Description("Same program")]
        //SameProgram = 1,
        //[Description("Different programs")]
        //DifferentPrograms = 2,
    }
    public enum ParticipentConditions : byte
    {
        [Description("Select")]
        SelectCondition = 0,
        [Description("No condition")]
        NoCondition = 1,
        //[Description("Same participant")]
        //SameParticipant = 1,
        //[Description("Different participants (siblings)")]
        //DifferentParticipants = 2,
    }
}