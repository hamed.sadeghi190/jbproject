﻿using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public interface IProgramModel
    {
        long Id { get; set; }

        string ClubDomain { get; set; }

        string SeasonDomain { get; set; }

        string Domain { get; set; }

        string Name { get; set; }

        ProgramStatus Status { get; set; }

        ProgramRegStatus RegStatus { get; set; }

        ProgramRunningStatus RunningStatus { get; set; }

        ProgramTypeCategory TypeCategory { get; set; }
    }
}
