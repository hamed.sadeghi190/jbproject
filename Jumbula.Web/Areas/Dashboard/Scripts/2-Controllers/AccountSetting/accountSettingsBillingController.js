﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('accountSettingsBillingController', ['$scope', '$http', 'jbToast', '$stateParams', '$state', function ($scope, $http, jbToast, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'account-settings-view';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/AccountSettings", order: 2, menu: "SETTINGS" });
        $scope.baseUrl = window.location.origin + "/Dashboard/AccountSetting";

        $scope.planType = 'Add';
        $scope.cardId = 0;
        $scope.showIsDefault = false;
        $scope.monthRange = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        var year = new Date().getFullYear()-2000;
        $scope.yearRange = [year, year + 1, year + 2, year + 3, year + 4, year + 5, year + 6, year + 7, year + 8, year + 9, year + 10, year + 11, year + 12, year + 13, year + 14, year + 15, year + 16, year + 17, year + 18, year + 19, year + 20];
        if ($stateParams.type) {
            $scope.planType = $stateParams.type;
        }
        if ($stateParams.cardId) {
            $scope.cardId = $stateParams.cardId;
        }
        $http.get($scope.baseUrl + '/CreateEditClientCard?planType=' + $scope.planType + '&id=' + $scope.cardId).success(function (data, status, headers, config) {
            $scope.Model = data;

            $scope.showIsDefault = (!$scope.Model.IsDefault);

        });
        function checkValue() {
            var emptyTextBoxes = $('.bb-input-style .input-control > input').filter(function () {
                return this.value != "";
            });
            emptyTextBoxes.addClass('filled');
        };
        $('.bb-input-style .input-control > input').focusout(function () {
            if ($(this).val() != '') {
                $(this).addClass('filled');
            } else {
                $(this).removeClass('filled');
            }
        });
        $scope.saveCard = function () {

            $http.post($scope.baseUrl + '/CreateEditClientCard', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status) {
                        jbToast.success("The billing info updated successfully.", "");
                        $state.go('AccountSettingBilling');
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        };



    }])

})();