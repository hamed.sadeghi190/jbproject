﻿using System.Collections.Generic;
using Jumbula.Common.Constants;
using Jumbula.Common.Helper;

namespace Jumbula.Core.Model.Report
{
    public class ReportColumn
    {
        public static List<ReportGridColumn> DailyDismissalReport(string seasonDomain)
        {
            return new List<ReportGridColumn>
            {
                new ReportGridColumn
                {
                    Field = ReportConstants.IsPickedUpField,
                    Title = ReportConstants.StatusTitle
                },
                new ReportGridColumn
                {
                    Field = ReportConstants.StudentNameField,
                    Title = ReportConstants.ParticipantTitle,
                    Template = LinkHelper.UiSrefBuilder(ReportConstants.OrderItem, new Dictionary<string, object>{{ReportConstants.SeasonDomainParam, seasonDomain}, {ReportConstants.OrderItemIdParam, "#=Id#"}}, ReportConstants.StudentName),
                    SortPath = $"{ReportConstants.PlayerProfile}.{ReportConstants.Contact}.{ReportConstants.LastName}|{ReportConstants.PlayerProfile}.{ReportConstants.Contact}.{ReportConstants.FirstName}"
                },
                new ReportGridColumn
                {
                    Field = ReportConstants.DismissalFromEnrichmentField,
                    Title = ReportConstants.DismissalTitle
                },
                new ReportGridColumn
                {
                    Field = ReportConstants.PickupTimeField,
                    Title = ReportConstants.DismissalTimeTitle
                },
                new ReportGridColumn
                {
                    Field = ReportConstants.PickupNameField,
                    Title = ReportConstants.PickedUpByTitle
                }
            };
        }

        public static List<ReportGridColumn> MemberListReport()
        {
            return new List<ReportGridColumn>
            {
                new ReportGridColumn
                {
                    Field = ReportConstants.NameField,
                    Title = ReportConstants.NameTitle
                },
                new ReportGridColumn
                {
                    Field = ReportConstants.ContactNameField,
                    Title = ReportConstants.ContactNameTitle
                },
                new ReportGridColumn
                {
                    Field = ReportConstants.EmailAddressField,
                    Title = ReportConstants.EmailTitle
                },
                new ReportGridColumn
                {
                    Field = ReportConstants.PhoneNumberField,
                    Title = ReportConstants.PhoneTitle
                }
            };
        }
    }
}
