﻿using System;

namespace Jumbula.Common.Exceptions
{
    public class JumbulaCartSessionExpiredException : JumbulaDbException
    {
        public JumbulaCartSessionExpiredException(Exception e)
            : base(e)
        {

        }
    }
}
