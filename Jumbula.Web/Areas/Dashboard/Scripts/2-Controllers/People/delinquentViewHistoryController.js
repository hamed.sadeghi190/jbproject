﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('delinquentViewHistoryController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation) {

        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;
        $scope.RelatedEntity = $scope.MC.stateParams.RelatedEntity;
        $scope.RelatedEntityId = $scope.MC.stateParams.RelatedEntityId;

        $http.get(window.location.origin + '/Dashboard/People/GetContactCategories').success(function (data) {

            $scope.Categories = data;
        }).error(function (data) {
            $scope.MC.handleErrors($scope, data);
        });



        $scope.viewHistoryDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport: {
                read:
                    {
                        url: window.location.origin + '/Dashboard/People/GetDelinquentViewHistoryList/',
                        contentType: "application/json; charset=utf-8",
                        type: "POST"
                    },
                parameterMap: function (data, operation) {
                    if (operation === "read") {
                        data = $.extend(
                            {
                                relatedEntity: $scope.MC.stateParams.RelatedEntity,
                                relatedEntityId: $scope.MC.stateParams.RelatedEntityId,
                                filters: $scope.Filters
                            },
                            data);

                        return JSON.stringify(data);
                    }
                    return null;
                }
            },
            schema:
                {
                    total: "TotalCount",
                    data: "DataSource"
                },
            sort:
                [{
                    field: "Id",
                    dir: "asc"
                }],
            pageSize: 50,
            batch: true,
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true,
            requestStart: function () {
                kendo.ui.progress($('[kendo-grid]'), true);
            },
            requestEnd: function () {
                kendo.ui.progress($('[kendo-grid]'), false);
            }
        });

        $scope.viewHistoryGridOptions = {
            dataSource: $scope.viewHistoryDataSource,
            sortable: true,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                        5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            columns: [
                {
                    field: "Date",
                    title: "Date",
                    sortable: true,
                    width: "120px"
                },
                {
                    field: "Category",
                    title: "Type",
                    sortable: true,
                    width: "60px"
                },
                {
                    field: "Sender",
                    title: "Sender",
                    sortable: true,
                    width: "110px"
                },
                {
                    field: "DeliverDateTime",
                    title: "Delivered",
                    sortable: true,
                    width: "120px"
                },
                {
                    field: "OpenDateTime",
                    title: "Opened",
                    sortable: true,
                    width: "120px"
                },
                {
                    field: "Note",
                    title: "Memo",
                    sortable: true,
                    width: "250px",
                    template: "<span title='#=Note#' > #=Note# </span>"
                },
                {
                    field: "",
                    title: "Actions",
                    width: "70px",
                    headerTemplate: "<span class='actions-title'>Actions</span>",
                    template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                                <li> ... \
                                    <ul>\
                                #if(data.Category==='Email'){#\
                                  <li ngaction='People_ViewHistory_ViewEmail' ng-click='PreviewEmail(#= Id #)'>\
                                                <sp0an class='k-link jbi-email'>View email</span>\
                                        </li>\
                                  #}#\
                                #if(data.Category!=='Email'){#\
                                  <li ngaction='People_ViewHistory_EditHistory' ng-click='EditHistory(#= Id #)'>\
                                                <span class='k-link jbi-edit-document'>Edit history</span>\
                                        </li>\
                                  #}#\
                                #if(data.Category!=='Email'){#\
                                  <li ngaction='People_ViewHistory_DeleteHistory' ng-click='DeleteHistory(#= Id #)'>\
                                                <span class='k-link jbi-delete-trash-1'>Delete history</span>\
                                        </li>\
                                  #}#\
                                    </ul>\
                                </li>\
                              </ul>"
                }
            ]
        };

        $scope.reflective = function () {
            $scope.viewHistoryGridOptions.dataSource.read();
        }

        $scope.AddHistory = function () {
            $http.get(window.location.origin + '/Dashboard/People/GetContact').success(function (data) {

                $scope.Model = data;
            }).error(function (data) {
                $scope.MC.handleErrors($scope, data);
            });

            $scope.isShowEditPopup = true;
        }

        $scope.EditHistory = function (id) {
            $http.get(window.location.origin + '/Dashboard/People/GetContact?id=' + id).success(function (data) {

                $scope.Model = data;
            }).error(function (data) {
                $scope.MC.handleErrors($scope, data);
            });

            $scope.isShowEditPopup = true;
        }

        $scope.DeleteHistory = function (id) {
            jbConfirmation.yes(function () {
                $http.post('Dashboard/People/DeleteContact', { id: id }).success(function (data) {
                    if (data.Status === true) {
                        jbToast.success("History was deleted successfully.", "");
                        $scope.reflective();
                    } else {
                        $scope.MC.handleErrors($scope, data);
                    }
                }).error(function (data) {
                    $scope.MC.handleErrors($scope, data);
                });
            }).confirm();
        }

        $scope.dismissEditPopup = function () {
            $scope.isShowEditPopup = false;
        }

        $scope.save = function () {
            $http.post("Dashboard/People/SaveContact", { model: $scope.Model, relatedEntity: $scope.RelatedEntity, relatedEntityId: $scope.RelatedEntityId })
                .success(function (data) {

                    if (data.Status) {
                        $scope.errors = null;
                        jbToast.success("History was updated successfully.", "");

                        $scope.reflective();
                        $scope.isShowEditPopup = false;
                    } else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        }

        $scope.PreviewEmail = function (id) {
            $http.post("Dashboard/People/EmailPreview", { id: id })
                .success(function (response) {
                    $scope.previewText2 = response;

                    $scope.isShowPreviewPopup = true;
                });
        }

        $scope.dismissPreviewPopup = function () {
            $scope.isShowPreviewPopup = false;
        }
    }]);
})();

