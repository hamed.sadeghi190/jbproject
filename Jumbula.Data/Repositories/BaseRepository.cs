﻿using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Common.Exceptions;
using Jumbula.Common.Utilities;
using Jumbula.Core.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Jumbula.Data.Repositories
{

    public class BaseRepository<TEntity> : IRepository<TEntity>
       where TEntity : BaseEntity
    {

        //TODO why method are virtual, the repo method should not override at repo level
        //TODO Why so many commented out lines?

        public BaseRepository(IEntitiesContext context)
        {
            Context = context;
            Entity = Context.Set<TEntity>();
        }

        protected IEntitiesContext Context { get; }

        protected IDbSet<TEntity> Entity { get; }


        public TEntity Get(int id, params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            return Get(x => x.Id == id, includeExpressions);
        }

        public virtual TEntity Get(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            if (predicate == null)
            {
                throw new ApplicationException("Predicate value must be passed to Get<T>.");
            }
            if (includeExpressions.Any())
            {
                var set = includeExpressions.Aggregate<Expression<Func<TEntity, object>>, IQueryable<TEntity>>
                        (Entity, (current, expression) => current.Include(expression));

                return set.SingleOrDefault(predicate);
            }
            return Entity.SingleOrDefault(predicate);

        }

        public virtual IQueryable<TEntity> GetList(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            try
            {
                return GetList(includeExpressions).Where(predicate);
            }
            catch (Exception e)
            {
                throw new JumbulaDbException(e);
            }
        }

        public virtual IQueryable<TEntity> GetList<TKey>(Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, TKey>> orderBy, params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            try
            {
                return GetList(predicate, includeExpressions).OrderBy(orderBy);
            }
            catch (Exception e)
            {
                throw new JumbulaDbException(e);
            }
        }

        public virtual IQueryable<TEntity> GetList<TKey>(Expression<Func<TEntity, TKey>> orderBy, params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            try
            {
                return GetList(includeExpressions).OrderBy(orderBy);
            }
            catch (Exception e)
            {
                throw new JumbulaDbException(e);
            }
        }

        public virtual IQueryable<TEntity> GetList(params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            try
            {
                if (includeExpressions.Any())
                {
                    var set = includeExpressions.Aggregate<Expression<Func<TEntity, object>>, IQueryable<TEntity>>
                       (Entity, (current, expression) => current.Include(expression));

                    return set;
                }
                return Entity;
            }
            catch (Exception e)
            {
                throw new JumbulaDbException(e);
            }
        }

        public PaginatedList<TEntity> GetList(int pageIndex, int pageSize, Expression<Func<TEntity, int>> keySelector, OrderBy orderBy = OrderBy.Ascending, params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            return GetList(pageIndex, pageSize, keySelector, null, orderBy, includeExpressions);
        }

        public PaginatedList<TEntity> GetList(int pageIndex, int pageSize, Expression<Func<TEntity, int>> keySelector, Expression<Func<TEntity, bool>> predicate, OrderBy orderBy, params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            var entities = FilterQuery(keySelector, predicate, orderBy, includeExpressions);
            var total = entities.Count();// entities.Count() is different than pageSize
            entities = entities.Paginate(pageIndex, pageSize);
            return entities.ToPaginatedList(pageIndex, pageSize, total);
        }

        private IQueryable<TEntity> FilterQuery(Expression<Func<TEntity, int>> keySelector, Expression<Func<TEntity, bool>> predicate, OrderBy orderBy,
            Expression<Func<TEntity, object>>[] includeExpressions)
        {

            var entities = GetList(predicate, includeExpressions);
            entities = (orderBy == OrderBy.Ascending)
                ? entities.OrderBy(keySelector)
                : entities.OrderByDescending(keySelector);
            return entities;
        }


        public virtual OperationStatus Save()
        {
            OperationStatus opStatus = new OperationStatus { Status = true };

            try
            {
                opStatus.Status = Context.SaveChanges() > 0;
            }
            catch (Exception exp)
            {
                opStatus = OperationStatus.CreateFromException("Error saving " + typeof(TEntity) + ".", exp);
                opStatus.Exception = exp;
            }

            return opStatus;
        }

        public virtual OperationStatus Create(TEntity entity)
        {
            OperationStatus opStatus = new OperationStatus { Status = true };

            try
            {

                Entity.Add(entity);
                opStatus.Status = Context.SaveChanges() > 0;
            }
            catch (Exception exp)
            {
                opStatus = OperationStatus.CreateFromException("Error creating " + typeof(TEntity) + ".", exp);
                opStatus.Exception = exp;
            }


            return opStatus;
        }

        public OperationStatus Create(IEnumerable<TEntity> entities)
        {
            OperationStatus opStatus = new OperationStatus { Status = true };

            try
            {
                foreach (var entity in entities)
                {
                    Entity.Add(entity);
                }

                opStatus.Status = Context.SaveChanges() > 0;
            }
            catch (Exception exp)
            {
                opStatus = OperationStatus.CreateFromException("Error creating " + typeof(TEntity) + ".", exp);
                opStatus.Exception = exp;
            }


            return opStatus;
        }

        public virtual OperationStatus Delete(int id)
        {
            OperationStatus opStatus;

            try
            {
                opStatus = DeleteWhere(d => d.Id == id);
            }
            catch (Exception exp)
            {
                opStatus = OperationStatus.CreateFromException("Error creating " + typeof(TEntity) + ".", exp);
                opStatus.Exception = exp;
            }

            return opStatus;
        }

        public virtual OperationStatus Delete(TEntity entity)
        {
            OperationStatus opStatus = new OperationStatus { Status = true };

            try
            {
                Entity.Remove(entity);
                opStatus.Status = Context.SaveChanges() > 0;
            }
            catch (Exception exp)
            {
                opStatus = OperationStatus.CreateFromException("Error creating " + typeof(TEntity) + ".", exp);
                opStatus.Exception = exp;
            }

            return opStatus;
        }


        public virtual OperationStatus DeleteWhere(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            var opStatus = new OperationStatus { Status = true };
            try
            {
                IQueryable<TEntity> query = Context.Set<TEntity>();
                if (filter != null)
                {
                    query = query.Where(filter);
                }

                query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

                foreach (var entity in query)
                {
                    Context.Entry(entity).State = EntityState.Deleted;
                }
                var count = Context.SaveChanges();
                opStatus.Status = count > 0;
                opStatus.RecordsAffected = count;
                return opStatus;
            }
            catch (Exception exp)
            {
                return OperationStatus.CreateFromException("Error creating " + typeof(TEntity) + ".", exp);
            }
        }
        public virtual OperationStatus Update(TEntity entity)
        {

            OperationStatus opStatus = new OperationStatus { Status = true };

            if (entity == null)
            {
                throw new ArgumentException("Cannot add a null entity.");
            }

            var entry = Context.Entry(entity);

            var set = Context.Set<TEntity>();
            TEntity attachedEntity = set.Local.SingleOrDefault(e => e.Id.Equals(entity.Id));  // You need to have access to key

            if (attachedEntity != null)
            {
                var attachedEntry = Context.Entry(attachedEntity);
                attachedEntry.CurrentValues.SetValues(entity);
            }
            else
            {
                entry.State = EntityState.Modified; // This should attach entity
            }
            try
            {
                opStatus.Status = Context.SaveChanges() > 0;

            }
            catch (Exception ex)
            {
                opStatus = OperationStatus.CreateFromException("Error updating " + typeof(TEntity) + ".", ex);
                opStatus.Exception = ex;
            }

            return opStatus;
        }

        public OperationStatus ExecuteStoreCommand(string cmdText, params object[] parameters)
        {
            //var opStatus = new OperationStatus { Status = true };

            //try
            //{
            //    //opStatus.RecordsAffected = DataContext.ExecuteStoreCommand(cmdText, parameters);
            //    //TODO: [Papa] = Have not tested this yet.
            //    opStatus.RecordsAffected = DataContext.Database.ExecuteSqlCommand(cmdText, parameters);
            //}
            //catch (Exception exp)
            //{
            //    OperationStatus.CreateFromException("Error executing store command: ", exp);
            //}
            //return opStatus;

            throw new NotImplementedException();
        }

        public void Dispose()
        {
        }

        public virtual void Dispose(bool disposing)
        {
        }
    }


    //public class BaseRepository<TEntity, TPrimaryKey> : IRepository<TEntity, TPrimaryKey>
    //  where TEntity : class, IBaseEntity<TPrimaryKey>
    //   where TPrimaryKey : struct
    public class BaseRepository<TEntity, TPrimaryKey> : IRepository<TEntity, TPrimaryKey>
        where TEntity : BaseEntity<TPrimaryKey>
        where TPrimaryKey : struct
    {

        //TODO why method are virtual, the repo method should not override at repo level
        //TODO Why so many commented out lines?
        private readonly IEntitiesContext _context;

        public BaseRepository(IEntitiesContext context)
        {
            _context = context;
            Entity = _context.Set<TEntity>();
        }

        protected IDbSet<TEntity> Entity { get; }


        public TEntity Get(TPrimaryKey id, params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            return Get(x => x.Id.Equals(id), includeExpressions);
        }

        public virtual TEntity Get(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            if (predicate == null)
            {
                throw new ApplicationException("Predicate value must be passed to Get<T>.");
            }
            if (includeExpressions.Any())
            {
                var set = includeExpressions.Aggregate<Expression<Func<TEntity, object>>, IQueryable<TEntity>>
                        (Entity, (current, expression) => current.Include(expression));

                return set.SingleOrDefault(predicate);
            }

            return Entity.SingleOrDefault(predicate);

        }

        public virtual IQueryable<TEntity> GetList(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            try
            {
                return GetList(includeExpressions).Where(predicate);
            }
            catch (Exception e)
            {
                throw new JumbulaDbException(e);
            }
        }

        public virtual IQueryable<TEntity> GetList<TKey>(Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, TKey>> orderBy, params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            try
            {
                return GetList(predicate, includeExpressions).OrderBy(orderBy);
            }
            catch (Exception e)
            {
                throw new JumbulaDbException(e);
            }
        }

        public virtual IQueryable<TEntity> GetList<TKey>(Expression<Func<TEntity, TKey>> orderBy, params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            try
            {
                return GetList(includeExpressions).OrderBy(orderBy);
            }
            catch (Exception e)
            {
                throw new JumbulaDbException(e);
            }
        }

        public virtual IQueryable<TEntity> GetList(params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            try
            {
                if (includeExpressions.Any())
                {
                    var set = includeExpressions.Aggregate<Expression<Func<TEntity, object>>, IQueryable<TEntity>>
                       (Entity, (current, expression) => current.Include(expression));

                    return set;
                }
                return Entity;
            }
            catch (Exception e)
            {
                throw new JumbulaDbException(e);
            }
        }





        public virtual OperationStatus Save()
        {
            OperationStatus opStatus = new OperationStatus { Status = true };

            try
            {
                opStatus.Status = _context.SaveChanges() > 0;
            }
            catch (Exception exp)
            {
                opStatus = OperationStatus.CreateFromException("Error saving " + typeof(TEntity) + ".", exp);
                opStatus.Exception = exp;
            }

            return opStatus;
        }

        public virtual OperationStatus Create(TEntity entity)
        {
            OperationStatus opStatus = new OperationStatus { Status = true };

            try
            {
                // var metaData = entity.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public)
                //.SingleOrDefault(p => p.PropertyType == typeof(MetaData));
                // if (metaData != null)
                //     metaData.SetValue(entity, new MetaData() { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow });

                Entity.Add(entity);
                opStatus.Status = _context.SaveChanges() > 0;
            }
            catch (Exception exp)
            {
                opStatus = OperationStatus.CreateFromException("Error creating " + typeof(TEntity) + ".", exp);
                opStatus.Exception = exp;
            }


            return opStatus;
        }

        public OperationStatus Create(IEnumerable<TEntity> entities)
        {
            OperationStatus opStatus = new OperationStatus { Status = true };

            try
            {
                foreach (var entity in entities)
                {
                    Entity.Add(entity);
                }

                opStatus.Status = _context.SaveChanges() > 0;
            }
            catch (Exception exp)
            {
                opStatus = OperationStatus.CreateFromException("Error creating " + typeof(TEntity) + ".", exp);
                opStatus.Exception = exp;
            }


            return opStatus;
        }

        public virtual OperationStatus Delete(TPrimaryKey id)
        {
            OperationStatus opStatus;

            try
            {
                opStatus = DeleteWhere(d => d.Id.Equals(id));
            }
            catch (Exception exp)
            {
                opStatus = OperationStatus.CreateFromException("Error creating " + typeof(TEntity) + ".", exp);
                opStatus.Exception = exp;
            }

            return opStatus;
        }

        public virtual OperationStatus Delete(TEntity entity)
        {
            OperationStatus opStatus = new OperationStatus { Status = true };

            try
            {
                Entity.Remove(entity);
                opStatus.Status = _context.SaveChanges() > 0;
            }
            catch (Exception exp)
            {
                opStatus = OperationStatus.CreateFromException("Error creating " + typeof(TEntity) + ".", exp);
                opStatus.Exception = exp;
            }

            return opStatus;
        }


        public virtual OperationStatus DeleteWhere(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            var opStatus = new OperationStatus { Status = true };
            try
            {
                IQueryable<TEntity> query = _context.Set<TEntity>();
                if (filter != null)
                {
                    query = query.Where(filter);
                }

                query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

                foreach (var entity in query)
                {
                    _context.Entry(entity).State = EntityState.Deleted;
                }

                var count = _context.SaveChanges();
                opStatus.Status = count > 0;
                opStatus.RecordsAffected = count;
                return opStatus;
            }
            catch (Exception exp)
            {
                return OperationStatus.CreateFromException("Error creating " + typeof(TEntity) + ".", exp);
            }
        }

        public virtual OperationStatus Update(TEntity entity)
        {

            OperationStatus opStatus = new OperationStatus { Status = true };

            if (entity == null)
            {
                throw new ArgumentException("Cannot add a null entity.");
            }

            var entry = _context.Entry(entity);

            var set = _context.Set<TEntity>();
            TEntity attachedEntity = set.Local.SingleOrDefault(e => e.Id.Equals(entity.Id));  // You need to have access to key

            if (attachedEntity != null)
            {
                var attachedEntry = _context.Entry(attachedEntity);
                attachedEntry.CurrentValues.SetValues(entity);
            }
            else
            {
                entry.State = EntityState.Modified; // This should attach entity
            }
            try
            {
                opStatus.Status = _context.SaveChanges() > 0;

            }
            catch (Exception ex)
            {
                opStatus = OperationStatus.CreateFromException("Error updating " + typeof(TEntity) + ".", ex);
                opStatus.Exception = ex;
            }

            return opStatus;
        }

        public OperationStatus ExecuteStoreCommand(string cmdText, params object[] parameters)
        {
            //var opStatus = new OperationStatus { Status = true };

            //try
            //{
            //    //opStatus.RecordsAffected = DataContext.ExecuteStoreCommand(cmdText, parameters);
            //    //TODO: [Papa] = Have not tested this yet.
            //    opStatus.RecordsAffected = DataContext.Database.ExecuteSqlCommand(cmdText, parameters);
            //}
            //catch (Exception exp)
            //{
            //    OperationStatus.CreateFromException("Error executing store command: ", exp);
            //}
            //return opStatus;

            throw new NotImplementedException();
        }






        public void Dispose()
        {

        }

        public virtual void Dispose(bool disposing)
        {
        }


    }
}
