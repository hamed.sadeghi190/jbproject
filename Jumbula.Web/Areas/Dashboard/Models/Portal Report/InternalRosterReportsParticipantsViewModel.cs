﻿namespace SportsClub.Areas.Dashboard.Models
{
    public class InternalRosterReportsParticipantsViewModel
    {
        public string ParticipantName { get; set; }
        public string Parent1Name { get; set; }
        public string AuthorizedPickup1Name { get; set; }
        public string EmergencyContactName { get; set; }
        public string Parent1Phone { get; set; }
        public string AuthorizedPickup1Phone { get; set; }
        public string EmergencyContactPhone { get; set; }
        public string Parent1Email { get; set; }
        public string AuthorizedPickup1Email { get; set; }
        public string EmergencyContactEmail { get; set; }
        public string TransportHome { get; set; }
        public int Number { get; set; }
        public string Allergies { get; set; }
        public string MedicalConditions { get; set; }
        
        public string GradeOrAge { get; set; }
    }
}