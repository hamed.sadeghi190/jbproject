﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Report;
using Jumbula.Core.Model.Report.Filter;
using System.Collections.Generic;
using System.Linq;

namespace Jumbula.Business.ReportBuilders
{
    public class DailyDismissalReportBuilderBusiness : BaseReportBuilder
    {
        #region Fields
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IFormBusiness _formBusiness;
        private readonly IProgramBusiness _programBusiness;
        private readonly INewReportBusiness _reportBusiness;
        private readonly ISeasonBusiness _seasonBusiness;

        private string _seasonDomain;
        private Season _season;
        private long _programId;
        private int _sessionId;
        #endregion

        #region Constractors
        public DailyDismissalReportBuilderBusiness(IOrderItemBusiness orderItemBusiness, IFormBusiness formBusiness, IProgramBusiness programBusiness, INewReportBusiness reportBusiness, ISeasonBusiness seasonBusiness)
        {
            _orderItemBusiness = orderItemBusiness;
            _formBusiness = formBusiness;
            _programBusiness = programBusiness;
            _reportBusiness = reportBusiness;
            _seasonBusiness = seasonBusiness;
        }
        #endregion

        #region Public methods
        public override ReportName ReportName => ReportName.DailyDismissal;
        public override PdfTemplateName? PdfTemplate => PdfTemplateName.Default;
        public override ExcelTemplateName? ExcelTemplate => ExcelTemplateName.Default;

        public override void Initiate()
        {
            _seasonDomain = GetParameter(Parameters, ReportConstants.SeasonDomainParam).ToString();
            _season = _seasonBusiness.Get(_seasonDomain, ClubBaseInfo.Id);
        }

        public override List<ReportHeader> GetHeaders()
        {
            return new List<ReportHeader>() { ReportHeader.Date, ReportHeader.SeasonName };
        }

        public override List<IJbReportFilterElement> GetFilters()
        {
            var programFilter = new JbDropDownReportFilter()
            {
                Id = 0,
                Name = ReportFilterElementName.Program,
                Title = ReportFilterElementName.Program.ToDescription(),
                Type = ReportFilterType.DropDown,
                ActionFilter = ReportFilterAction.PrepareData,
                TargetFilter = ReportFilterElementName.Session,
                IsRequired = true,
                ParameterName = ReportConstants.ProgramIdParam,
                Data = _reportBusiness.GetSeasonPrograms(_season.Id, ClubBaseInfo.Id, null),
                FontWeight = ReportConstants.Bold
            };

            var sessionFilter = new JbDropDownReportFilter()
            {
                Id = 1,
                Name = ReportFilterElementName.Session,
                Title = ReportFilterElementName.Session.ToDescription(),
                Type = ReportFilterType.DropDown,
                ActionFilter = ReportFilterAction.NoAction,
                TargetFilter = null,
                IsRequired = true,
                ParameterName = ReportConstants.SessionIdParam,
                FontWeight = ReportConstants.Bold
            };

            return new List<IJbReportFilterElement>() { programFilter, sessionFilter };
        }

        public override List<ReportGridColumn> GetColumns()
        {
            return new List<ReportGridColumn>
            {
                new ReportGridColumn
                {
                    Field = ReportConstants.IsPickedUpField,
                    Title = ReportConstants.StatusTitle
                },
                new ReportGridColumn
                {
                    Field = ReportConstants.StudentNameField,
                    Title = ReportConstants.ParticipantTitle,
                    Template = LinkHelper.UiSrefBuilder(ReportConstants.OrderItem, new Dictionary<string, object>{{ReportConstants.SeasonDomainParam, _seasonDomain}, {ReportConstants.OrderItemIdParam, "#=Id#"}}, ReportConstants.StudentName),
                    SortPath = $"{ReportConstants.PlayerProfile}.{ReportConstants.Contact}.{ReportConstants.LastName}|{ReportConstants.PlayerProfile}.{ReportConstants.Contact}.{ReportConstants.FirstName}"
                },
                new ReportGridColumn
                {
                    Field = ReportConstants.DismissalFromEnrichmentField,
                    Title = ReportConstants.DismissalTitle
                },
                new ReportGridColumn
                {
                    Field = ReportConstants.PickupTimeField,
                    Title = ReportConstants.DismissalTimeTitle
                },
                new ReportGridColumn
                {
                    Field = ReportConstants.PickupNameField,
                    Title = ReportConstants.PickedUpByTitle
                }
            };
        }

        public override PaginatedList<object> GetData()
        {
            if (GetParameter(Parameters, ReportConstants.ProgramIdParam) == null || GetParameter(Parameters, ReportConstants.SessionIdParam) == null)
            {
                return new PaginatedList<object>(new List<object>(), 0, 0, 0);
            }

            _seasonDomain = GetParameter(Parameters, ReportConstants.SeasonDomainParam).ToString();
            _programId = long.Parse(GetParameter(Parameters, ReportConstants.ProgramIdParam).ToString());
            _sessionId = int.Parse(GetParameter(Parameters, ReportConstants.SessionIdParam).ToString());

            var orderItems = _orderItemBusiness.GetProgramOrderItems(_programId).ToList();

            var sessionAttendances = _programBusiness.GetSessionAttendance(_programId, _sessionId);

            var sortPath = Sort != null ? Sort.First().sortPath : ReportConstants.Id;
            var sortDirection = Sort != null ? Sort.First().dir == ReportConstants.Asc ? ReportConstants.OrderBy : ReportConstants.OrderByDescending : ReportConstants.OrderBy;

            var data = sessionAttendances
                .AsQueryable()
                .OrderBy(sortPath, sortDirection)
                .Paginate(PaginationModel.Page, PaginationModel.PageSize)
                .Select(s => MapDailyDismissalReportViewModel(s, orderItems));

            var result = data.ToPaginatedList<object>(PaginationModel.Page, PaginationModel.PageSize, sessionAttendances.Count);

            return result;
        }
        #endregion

        #region Private methods
        private string GetDismissalFromEnrichmentJbForm(OrderItem orderItem)
        {
            return _formBusiness.GetFormValue(orderItem.JbForm, SectionsName.SchoolSection, ElementsName.DismissalFromEnrichment, new List<string> { ReportConstants.DismissalAfterEnrichment, ReportConstants.AfternoonDismissalFromEnrichment, ReportConstants.DismissalFromEnrichment });
        }


        private DailyDismissalReportBuilderViewModel MapDailyDismissalReportViewModel(ScheduleAttendance sessionAttendance, List<OrderItem> orderItems)
        {
            var orderItem = orderItems.LastOrDefault(o => o.PlayerId != null && o.PlayerId == sessionAttendance.PlayerProfile.Id);
            var player = sessionAttendance.PlayerProfile;

            return new DailyDismissalReportBuilderViewModel()
            {
                Id = orderItem?.Id ?? 0,
                IsPickedUp = sessionAttendance.IsPickedUp ? ReportConstants.Dismissed : string.Empty,
                StudentName = player?.Contact != null ? $"{player.Contact.LastName}, {player.Contact.FirstName}" : string.Empty,
                DismissalFromEnrichment = player != null && orderItem != null ? GetDismissalFromEnrichmentJbForm(orderItem) : string.Empty,
                PickupTime = sessionAttendance.Pickup != null ? sessionAttendance.Pickup.LocalDateTime.ToString(Constants.DefaultTimeFormat) : string.Empty,
                PickupName = sessionAttendance.Pickup != null ? sessionAttendance.Pickup.FullName : string.Empty,
            };
        }
        #endregion

    }

}
