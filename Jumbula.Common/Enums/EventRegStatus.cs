﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum EventRegStatus
    {
        [Description("not started")]
        NotStarted = 0,
        Open = 1,
        Closed = 2,
    }

}
