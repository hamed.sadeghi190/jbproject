﻿namespace Jumbula.Common.Enums
{
    public enum EmailScheduleType : byte
    {
        ImmediateSend,
        Scheduled,
    }
}
