﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('tourController', ['$scope', '$http', '$sce', '$stateParams', function ($scope, $http, $sce, $stateParams) {
        window.app.globalObjects.initializeView();

        window.app.globalObjects.pageClass = 'tour';

        $scope.MC.stateParams = $stateParams;

        $scope.AllTours = {
            Tours: [
                 {
                     //YouTubeId: "8f0sO7EvIUE",
                     YouTubeId: "m7pXgjqU0JE",
                     Title: "A brief introduction to Jumbula",
                     Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                 },
                {
                    YouTubeId: "-Bcnga0qMNI",
                    Title: "How to create a class",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "TCSFh4ymbUA",
                    Title: "How to create a custom discount?",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },

                {
                    YouTubeId: "B0UffieZ5X4",
                    Title: "How to create a multi-program discount?",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "cVi7DX2jLN4",
                    Title: "How to create a sibling discount?",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "N-qV9zX-gE4",
                    Title: "Add a season",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "DGdj5ffjOyk",
                    Title: "Add a program",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "IWSsuJgR7Bc",
                    Title: "Add a class",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "OIFkYcKXgzo",
                    Title: "Payment methods & checkout settings",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "ckVWzIMX8DQ",
                    Title: "View & test a program",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "fGq5O4HbEnA",
                    Title: "Coupon setup",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "l_6Oe4WZT38",
                    Title: "Discount setup",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "Yq4kx0SYXTo",
                    Title: "Payment plan setup",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "9G1VoeuBOOQ",
                    Title: "Single vs multiple registrations",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "7QvtcLlZ0Cc",
                    Title: "Class Setup",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "DVd9-qb01UI",
                    Title: "Camp Setup",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "EGkt6Zf_67E",
                    Title: "How to edit a program or class",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                 {
                     YouTubeId: "Yq4kx0SYXTo&t=4s",
                     Title: "How to set up a payment plan",
                     Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                 },
                  {
                      YouTubeId: "fGq5O4HbEnA&t",
                      Title: "How to set up a coupon",
                      Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "UY2kJSd-wKg",
                    Title: "Invoicing",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "OHYyUQugl78",
                    Title: "Payment methods",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                }, {
                    YouTubeId: "JCQtNd8U__g",
                    Title: "Attendance mobile app",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "34Y8kQwygOs",
                    Title: "Jumbula home site tool",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                }
            ]
        };

        for (var i = 0; i < $scope.AllTours.Tours.length; i++) {

            var currentTour = $scope.AllTours.Tours[i];

            if ($scope.MC.stateParams.tourId == currentTour.YouTubeId) {
                $scope.Model = {
                    YouTubeId: currentTour.YouTubeId,
                    Title: currentTour.Title,
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                };
            }
        }

        $scope.trustSrc = function (src) {
            return $sce.trustAsResourceUrl(src);
        }

        $scope.generateYouTubeUrl = function (id) {
            return $scope.trustSrc("//youtube.com/embed/" + id + "?autohide=1&color=white&rel=0&theme=dark");
        };
    }]);
})();