﻿using SportsClub.Db;
using SportsClub.Domain;
using SportsClub.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using SportsClub.Infrastructure.Helper;

namespace SportsClub.Models
{
    public enum RegisterPageMode
    {
        Create,
        EditFromCart,
        EditFromOrder
    }

    public class RegisterTableTennisTourneyViewModel
    {
        // for icc
        public string EditModeIdentifire { get; set; }

        public int ProfileId { get; set; }

        //public PlayerProfile Profile { get; set; }

        // indicate user that logged in is adult or not
        public bool IsAdultLoggedInUser { get; set; }

        [Display(Name = "Last Name")]
        public string Name { get; set; }

        public string PlayerImageUrl { get; set; }

        public PlayerProfileComplitionFormContactViewModel Contact { get; set; }

        public RegisterTTTourneyPageViewModel TourneyInfo { get; set; }
        // store events by an specific sorting
        public List<RegisterTTTourneyEventViewModels> SortedEvents { get; set; }

        public PolicyViewModel Proof { get; set; }
        public PolicyViewModel General { get; set; }
        public PolicyViewModel Refund { get; set; }
        public PolicyViewModel Waiver { get; set; }
        public PolicyViewModel MedicalRelease { get; set; }
        public DonationViewModel Donation { get; set; }

        [AllowHtml]
        public string Rules { get; set; }

        // System assigned fields set after postback
        public PlayerProfile Player { get; set; }
        public string ItemId { get; set; }
        public string ItemName { get; set; }
        public string Domain { get; set; }
        public string ClubDomain { get; set; }

        public bool IsOutsourcerEvent { get; set; }
        public string OutSourcerClubDomain { get; set; }
        public string OutsourcerClubName { get; set; }

        public decimal total { get; set; }

        [Display(Name = "Club affiliation")]
        public string ClubAffiliation { get; set; }

        public RegisterPageMode PageMode { get; set; }

        //need when owner of club wants to register
        public string FirstName { get; set; }

        public string LastName { get; set; }


        // if player was not adult and doesent have parent it set to true
        public bool ParentRequired { get; set; }


        // this Property for dropdownlist
        [Display(Name = "Registering for")]
        public int SelectedProfileId { get; set; }

        // this Property for dropdownlist get it value from playerprofile view model
        public IEnumerable<SelectListItem> PlayerLists { get; set; }

        // view model for insert parent
        public AddPlayerProfileViewModel Parent { get; set; }

        public GenderCategories Gender { get; set; }

        public OptionalDateOfBirthViewModel DateOFbirthday { get; set; }

        public string PrimaryPlayerEmail { get; set; }

        public bool ProcessingFee { get; set; }

        public bool RegistrationFee { get; set; }

        public bool IsNotCurrentCartDomain
        {
            get
            {
                // Get cart
                //var cartDb = Ioc.ICartDbService;

                //// Get domain of orders in the cart(if there is no order in cart return empty)
                //string currentDomain = cartDb.CurrentDomain(HttpContext.Current);

                //if (!string.IsNullOrEmpty(currentDomain))
                //{
                //    return !(currentDomain == this.ClubDomain);
                //}

                return false;
            }
        }

        public RegisterTableTennisTourneyViewModel(TTTourney ttTourney, bool isOflineEntry = false)
        {
            if (!isOflineEntry)
            {
                // indecate logged in user is adult or not
                var loggedUserContact = Ioc.IPlayerProfileDbService.GetByUserId(TheMembership.GetCurrentUserId())
                                                 .Single(x => x.Type == PlayerProfileType.Primary)
                                                 .Contact;
                IsAdultLoggedInUser = Utilities.CalculateAge(loggedUserContact.DoB.Value) >= Constants.PlayerProfile_Min_Age;
            }


            Donation = new DonationViewModel(ttTourney.Donation);

            TourneyInfo = new RegisterTTTourneyPageViewModel(ttTourney);

            SortedEvents = new List<RegisterTTTourneyEventViewModels>();

            List<TTTourneyEvent> listOpenRating = ttTourney.TTTourney_Id.Where(p => p.EventType == TTTourneyEventType.Ratings && p.Attendtype == TTTourneyAttendtype.Open).ToList();
            List<TTTourneyEvent> listRating = ttTourney.TTTourney_Id.Where(p => p.EventType == TTTourneyEventType.Ratings && p.Attendtype == TTTourneyAttendtype.Rating).OrderByDescending(p => p.Rating.Length).ThenByDescending(p => p.Rating).ToList();
            List<TTTourneyEvent> listUnratedRating = ttTourney.TTTourney_Id.Where(p => p.EventType == TTTourneyEventType.Ratings && p.Attendtype == TTTourneyAttendtype.Unrated).ToList();
            foreach (var item in listOpenRating)
            {
                SortedEvents.Add(new RegisterTTTourneyEventViewModels(item));
            }
            foreach (var item in listRating)
            {
                SortedEvents.Add(new RegisterTTTourneyEventViewModels(item));
            }
            foreach (var item in listUnratedRating)
            {
                SortedEvents.Add(new RegisterTTTourneyEventViewModels(item));
            }

            //
            foreach (var item in ttTourney.TTTourney_Id.Where(p => p.EventType == TTTourneyEventType.Seniors).OrderByDescending(p => p.Age).ToList())
            {
                SortedEvents.Add(new RegisterTTTourneyEventViewModels(item));
            }
            foreach (var item in ttTourney.TTTourney_Id.Where(p => p.EventType == TTTourneyEventType.Cadets_Juniors || p.EventType == TTTourneyEventType.YoungMen_Women).OrderByDescending(p => p.Age).ToList())
            {
                SortedEvents.Add(new RegisterTTTourneyEventViewModels(item));
            }
            foreach (var item in ttTourney.TTTourney_Id.Where(p => p.EventType == TTTourneyEventType.Doubles).OrderBy(p => p.StartDate).ToList())
            {
                SortedEvents.Add(new RegisterTTTourneyEventViewModels(item));
            }

            // policies
            InitialPolicies(ttTourney);

            // Notes
            Rules = ttTourney.Discription;

            //
            USATTMembership = new List<ChargeDiscountViewModel>();
            //foreach (var item in ttTourney.ChargeDiscounts.Where(p => p.Category == ChargeDiscountCategories.USATTMembership))
            //{
            //    USATTMembership.Add(new ChargeDiscountViewModel(item, false));
            //}

            // Realize event is outsourcer or not
            IsOutsourcerEvent = !string.IsNullOrEmpty(ttTourney.OutSourcerClubDomain);

            if (IsOutsourcerEvent)
            {
                OutSourcerClubDomain = ttTourney.OutSourcerClubDomain;

                //Get outsourcer club domain from db
                OutsourcerClubName = Ioc.IClubDbService.GetClubName(OutSourcerClubDomain);
            }
        }

        public void InitialPolicies(TTTourney ttTourney)
        {
            Refund = new PolicyViewModel();
            Waiver = new PolicyViewModel();
            MedicalRelease = new PolicyViewModel();
            General = new PolicyViewModel();
            Proof = new PolicyViewModel();
            
            SeedPolicies();


            foreach (var item in ttTourney.Policys)
            {
                switch (item.Category)
                {
                    case PolicyCategories.Refund:
                        {
                            Refund.Set(item);
                            break;
                        }
                    case PolicyCategories.Waiver:
                        {
                            Waiver.Set(item);
                            break;
                        }
                    case PolicyCategories.MedicalRelease:
                        {
                            MedicalRelease.Set(item);
                            break;
                        }
                    case PolicyCategories.General:
                        {
                            General.Set(item);
                            break;
                        }
                    case PolicyCategories.Proof:
                        {
                            Proof.Set(item);
                            break;
                        }
                    default:
                        break;
                }
            }
        }

        public RegisterTableTennisTourneyViewModel()
        {
            Donation = new DonationViewModel();
        }

        private void SeedPolicies()
        {
            IStandardPolicyDb policyDb = Ioc.IStandardPolicyDbService;
            StandardPolicy[] policies = policyDb.Get();

            // Note: PolicyCategories start @ index 1, must reduce by one

            Refund = new PolicyViewModel() { Category = PolicyCategories.Refund, Name = policies[(int)PolicyCategories.Refund - 1].Name, Desc = policies[(int)PolicyCategories.Refund - 1].Description, IsChecked = false };
            Waiver = new PolicyViewModel() { Category = PolicyCategories.Waiver, Name = policies[(int)PolicyCategories.Waiver - 1].Name, Desc = policies[(int)PolicyCategories.Waiver - 1].Description, IsChecked = false };
            MedicalRelease = new PolicyViewModel() { Category = PolicyCategories.MedicalRelease, Name = policies[(int)PolicyCategories.MedicalRelease - 1].Name, Desc = policies[(int)PolicyCategories.MedicalRelease - 1].Description, IsChecked = false };
            General = new PolicyViewModel() { Category = PolicyCategories.General, Name = policies[(int)PolicyCategories.General - 1].Name, Desc = policies[(int)PolicyCategories.General - 1].Description, IsChecked = false };
            Proof = new PolicyViewModel() { Category = PolicyCategories.Proof, Name = policies[(int)PolicyCategories.Proof - 1].Name, Desc = policies[(int)PolicyCategories.Proof - 1].Description, IsChecked = false };
        }

        public TableTennisProfileInfoViewModel Sport { get; set; }

        public string UsTTId { get; set; }
        public string UsTTRating { get; set; }

        public DateTime? UsTTExpirationDate { get; set; }

        // hard coded variables
        [Display(Name = "I do not have a USATT membership")]
        public bool NoUscfIdChecked { get; set; }

        //For filling USATT
        [DisplayName("Provide USATT membership?")]
        public ICollection<ChargeDiscountViewModel> USATTMembership { get; set; }

        //to declare which one selected
        public int? SelectedUSATTMembership { get; set; }



        [Display(Name = "One year USATT membership fee ($49)")]
        public bool OneYearChecked { get; set; }
        [Display(Name = "Three year USATT membership fee ($130)")]
        public bool ThreeYearChecked { get; set; }
        [Display(Name = "One year USATT membership fee for junior under 18 ($25)")]
        public bool OneYearJuniorChecked { get; set; }
        //

        public bool HasUnderDiscount
        {
            get
            {
                return EligibleForUnderDiscount();
            }
        }

        bool EligibleForUnderDiscount()
        {
            if (Player != null && Player.Contact.DoB.HasValue)
            {
                if (Utilities.CalculateAge(Player.Contact.DoB.Value) < Constants.F_Tournaments_TT_Policy_UnderDiscount_Year)
                {
                    return true;
                }
            }

            // profile is not loaded
            return false;
            //throw new Exception();
        }
    }

    public class RegisterTTTourneyEventViewModels
    {
        public dynamic Charges { get; set; }

        public dynamic Under15Discount { get; set; }

        public string Lable { get; set; }

        [AllowHtml]
        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int EventID { get; set; }

        public string DisplayEventDate
        {
            get
            {
                DateTime startDate = Convert.ToDateTime(StartDate);
                DateTime endDate = Convert.ToDateTime(EndDate);
                return GetSchMonDayName(startDate, endDate);
            }
        }

        public /*TimeSpan?*/string Time { get; set; }

        public bool IsChecked { get; set; }

        public decimal EF { get; set; }

        public TTTourneyEventType EventType { get; set; }

        [Display(Name = "Partner name")]
        public string PartnerName { get; set; }

        public Nullable<int> CapacityLeft
        {
            get
            {
                return Ioc.ITTTourneyDbService.GetLeftCapacity(EventID);
            }

        }

        public RegisterTTTourneyEventViewModels(TTTourneyEvent ttEvent)
        {
            Lable = Ioc.ITTTourneyDbService.GetEventName(ttEvent.EventType, ttEvent.Attendtype, ttEvent.Age, ttEvent.RoundType, ttEvent.Rating, ttEvent.Gender);
            try
            {
                Lable = Lable.Substring(0, Lable.Length - 1 - ttEvent.RoundType.ToString().Length);
            }
            catch
            {

            }
            StartDate = ttEvent.StartDate;
            EndDate = ttEvent.EndDate;
            EF = ttEvent.EF;
            EventType = ttEvent.EventType;
            Time = Utilities.FormatTimeSpan(ttEvent.Time);
            EventID = ttEvent.Id;
        }

        public string GetSchMonDayName(DateTime start, DateTime end)
        {
            string s = start.ToString("MMMM ");
            s = s + start.Day;

            if (end.Subtract(start).Days > 0)
            {
                if (start.Month == end.Month)
                {
                    s = s + Constants.S_Dash + end.Day;
                }
                else
                {
                    s = s + Constants.S_Dash + end.ToString(" MMMM ") + end.Day;
                }
            }

            return s;
        }

        public RegisterTTTourneyEventViewModels()
        {
            Charges = new List<ChargeDiscount>();
        }

        public bool IsCanceled { get; set; }
    }

    public class RegisterTTTourneyPageViewModel
    {
        public string LogoUri { get; set; }
        public string Name { get; set; }
        public string SubTitle1 { get; set; }
        public string SubTitle2 { get; set; }

        public string ClubName { get; set; }
        public string ClubDomain { get; set; }
        public string Domain { get; set; }
        public string PdfFileName { get; set; }
        public string AddressName { get; set; }
        public Nullable<DateTime> StartDate { get; set; }
        public Nullable<DateTime> EndDate { get; set; }
        public EventStatusCategories EventStatus { get; set; }
        public int CategoryId { get; set; }
        //public string DisplayDate { get; set; }

        public PostalAddress PostalAddress { get; set; }

        public GoogleMapsViewModel Map { get; set; }

        public RegisterTTTourneyPageViewModel(TTTourney ttTourney)
        {
            Name = ttTourney.Name;
            SubTitle1 = ttTourney.SubTitle1;
            SubTitle2 = ttTourney.SubTitle2;

            ClubDomain = ttTourney.ClubDomain;
            Domain = ttTourney.Domain;
            ClubBasicInfo basicClubInfo = Ioc.IClubDbService.GetBasicInfo(ClubDomain);
            ClubName = basicClubInfo.Name;
            //SportCategory = SportCategories.TableTennis;

            //DisplayDate = ttTourney.DisplayDate;
            PostalAddress = ttTourney.ClubLocation.PostalAddress;
            AddressName = ttTourney.AddressName;

            PdfFileName = ttTourney.FileName;

            StartDate = ttTourney.TTTourney_Id.Select(m => m.StartDate).Min();
            EndDate = ttTourney.TTTourney_Id.Select(m => m.EndDate).Max();

            EventStatus = ttTourney.EventStatus;

            Map = new GoogleMapsViewModel()
            {
                Name = AddressName,
                PostalAddress = PostalAddress
            };

            LogoUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, ClubDomain, basicClubInfo.Logo).AbsoluteUri;

            if (string.IsNullOrEmpty(basicClubInfo.Logo) || !StorageManager.IsExist(LogoUri))
            {
                if (CategoryId.Equals(CategoryModel.ChessId))
                {
                    LogoUri = Constants.Club_Chess_Default_Image_Large;
                }
                else if(CategoryId.Equals(CategoryModel.TableTennisId))
                {
                    LogoUri = Constants.Club_TableTennis_Default_Image_Large;
                }
                else
                {
                    LogoUri = Constants.Club_NoSport_Default_Image_Large;
                }
            }
        }

        public RegisterTTTourneyPageViewModel()
        {

        }
    }
}
