﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('BulkDependentCareReceiptReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        var model = {};
        $scope.filter = {};

        $scope.Model = customReportService.getModelFromStrorage();

        $scope.Model.year = [];
        var year = new Date().getFullYear();
        for (var i = year; i >= year - 2; i--) {

            $scope.Model.year.push({ Value: i, Title: i.toString() });
        }

        $scope.filter.year = year;
        $scope.filter.filterType = "Year";

        $scope.viewReport = function () {

            $http.post(baseUrl + '/GetBulkDependentCareReceiptReport', {
                filterType: $scope.filter.filterType,
                year: $scope.filter.year,
            }, {
                headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
            }).success(function (data) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                var blob = new Blob([data], { type: "application/pdf" });

                var fileURL = URL.createObjectURL(blob);
                a.href = fileURL;
                a.download = "BulkDependentCareReceipt.pdf";
                a.click();
            });

        }
    }
    ]);

})();