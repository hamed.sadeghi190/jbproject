﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('DonationsReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();
        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        var totalAmount;

        $scope.Model = customReportService.getModelFromStrorage();

        $scope.TotalAmount = 0;

        if ($scope.MC.account.isPartner) {

            $scope.ClubTypes = [{ Value: "school", Title: "Schools" }, { Value: "provider", Title: "Providers" }];
            $scope.Model.ClubType = "school";

            $scope.updateChosenScope = function () {
                $("select[chosen]").trigger('chosen:updated');
            };

            getAllSchools();

            $scope.getClubs = function(clubType) {

                if (clubType === "school") {

                    getAllSchools();

                } else if (clubType === "provider") {

                    getAllProviders();
                }
            }
        }

        function getAllSchools() {
            $http.get(baseUrl + '/GetAllSchools')
                .success(function (data) {
                    $scope.AllClubs = data.AllSchools;

                    var timer = false;

                    $scope.updateChosenScope = function () {
                        $("select[chosen]").trigger('chosen:updated');
                    };

                    $scope.$watch("AllClubs", function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });
                });
        }

        function getAllProviders() {
            $http.get(baseUrl + "/GetAllProvider")
                .success(function (data) {
                    $scope.AllClubs = data;
                    var timer = false;

                    $scope.updateChosenScope = function () {
                        $("select[chosen]").trigger('chosen:updated');
                    };

                    $scope.$watch('AllClubs', function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });
                });
        }


        function getTotalDonations() {
            $http.get(baseUrl + '/GetTotalDonations', { params: { start: $scope.Model.UpdateFrom, end: $scope.Model.UpdateTo, clubId: $scope.Model.ClubId } }).success(function (data, status, headers, config) {

                $scope.TotalAmount = data;
                totalAmount = data;

            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }

        //getTotalDonations();
        var isFirstRun = false;
        var prepareGrid = function () {

            isFirstRun = true;

            $scope.DonationsReportDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport:
                    {
                        read: {
                            url: baseUrl + '/GetDonations',
                            type: "POST",
                            data: function () {
                                return {
                                    end: $scope.Model.UpdateTo,
                                    start: $scope.Model.UpdateFrom,
                                    clubId: $scope.Model.ClubId
                                };
                            },
                        },

                    },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },

                serverSorting: false,
                serverPaging: true,
                pageSize: 10,
                resizable: true,
            });

            $scope.DonationsReportGridOptions = {
                excel: {
                    allPages: true,
                    fileName: "Donation_Report.xlsx",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },
                excelExport: function (e) {
                    cfpLoadingBar.complete();

                    var rows = e.workbook.sheets[0].rows;
                    rows.push({
                        cells: [{
                            value: ""
                        }]
                    });
                    if (totalAmount != null) {
                        rows.push({
                            cells: [{
                                value: "Total: " + totalAmount
                            }]
                        });
                    }
                },
                dataSource: $scope.DonationsReportDataSource,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                        [
                            5, 10, 20, 30, 50, 100, 200
                        ],
                    buttonCount: 5,
                },
                dataBound: function () {
                    var grid = $("#installmentReportGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },
                scrollable: true,
                sortable: { mode: 'single' },
                columns: [

                    {
                        field: "Date",
                        title: "Date",
                        width: "200px"
                    },
                    {
                        field: "ConfirmationId",
                        title: "Confirmation",
                        width: "200px",
                        template: " #if(HasDetail){#\
                                     <a ui_sref='DonationDiplay({orderItemId:  #=Id#})'  class='block'> #= ConfirmationId # </a>\
                                  #}\
                                  else{#\
                                     <span> #= ConfirmationId #</span>\
                                   #}#"
                    },
                    {
                        field: "UserEmail",
                        title: "User email",
                        width: "200px"
                    },
                    {
                        field: "ParentFirstName",
                        title: "Parent first name",
                        width: "200px"
                    },
                    {
                        field: "ParentLastName",
                        title: "Last name",
                        width: "200px"
                    },
                    {
                        field: "ParentEmail",
                        title: "Email",
                        width: "200px"
                    },
                    {
                        field: "ParticipantFirstName",
                        title: "Participant first name",
                        width: "200px"
                    },
                    {
                        field: "ParticipantLastName",
                        title: "Last name",
                        width: "200px"
                    },
                    {
                        field: "DonationAmount",
                        title: "Amount",
                        width: "200px"
                    }
                ]
            }
        }

        $scope.filterData = function () {

            if (isDatesValid()) {

                getTotalDonations();
                if (!isFirstRun) {
                    prepareGrid()
                } else {
                    $scope.DonationsReportGridOptions.dataSource.read();
                }
            }

        }

        $scope.clearFilters = function () {
            $scope.Model.UpdateFrom = "";
            $scope.Model.UpdateTo = "";
            $scope.DonationsReportGridOptions.dataSource.read();
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#DonationsReportGrid").getKendoGrid().saveAsExcel();
        }

        function isDatesValid() {
            if (($scope.Model.UpdateFrom == null || $scope.Model.UpdateFrom == '') && ($scope.Model.UpdateTo == null || $scope.Model.UpdateTo == '')) {
                jbToast.error("Please enter start date and end date");
                return false;
            }

            if ($scope.Model.UpdateFrom == null || $scope.Model.UpdateFrom == '') {
                jbToast.error("Please enter start date");
                return false;
            }

            if ($scope.Model.UpdateTo == null || $scope.Model.UpdateTo == '') {
                jbToast.error("Please enter end date");
                return false;
            }
            return true;
        }
    }
    ]);

})();