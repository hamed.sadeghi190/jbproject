﻿
using Jb.Framework.Common;
using Jumbula.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Common.Constants;
using Jumbula.Common.Helper;

namespace Jumbula.Business
{
    public class TransactionActivityBusiness : ITransactionActivityBusiness
    {
        private readonly IRepository<TransactionActivity, long> _transactionActivityRepository;
        private readonly IEntitiesContext _dataContext;

        public TransactionActivityBusiness(IRepository<TransactionActivity, long> transactionActivityRepository, IEntitiesContext dataContext)
        {
            _transactionActivityRepository = transactionActivityRepository;
            _dataContext = dataContext;
        }

        public IQueryable<TransactionActivity> GetAllTransactionByOrderId(long orderId)
        {
            return _transactionActivityRepository.GetList().Where(o => o.OrderId == orderId);
        }

        public IQueryable<TransactionActivity> GetAllTransactionBySeasonId(long seasonId)
        {
            return _transactionActivityRepository.GetList().Where(o => o.TransactionStatus == TransactionStatus.Success && o.SeasonId == seasonId && o.Order.IsLive == (o.Season.Status == SeasonStatus.Live));
        }
        public IQueryable<TransactionActivity> GetAllClubTransactionByUserId(int userId, int clubId)
        {
            return _transactionActivityRepository.GetList().Where(o => o.TransactionStatus == TransactionStatus.Success && o.ClubId == clubId && o.Order.UserId == userId && o.Order.IsLive && (o.OrderItem.ItemStatus == OrderItemStatusCategories.completed || (o.OrderItem.ItemStatus == OrderItemStatusCategories.changed && o.OrderItem.ItemStatusReason == OrderItemStatusReasons.canceled)));
        }
        public IQueryable<TransactionActivity> GetAllClubTransactionByClubId(int clubId, bool isLive = true)
        {
            return _transactionActivityRepository.GetList().Where(o => o.TransactionStatus == TransactionStatus.Success && o.ClubId == clubId && o.Order.IsLive == isLive);
        }
        public IQueryable<TransactionActivity> GetAllTransactionByOrderItemId(long orderItemId)
        {
            return _transactionActivityRepository.GetList().Where(o => o.OrderItemId == orderItemId);
        }
        public IQueryable<TransactionActivity> GetAllsucceedTransactionByOrderId(long orderId)
        {
            return _transactionActivityRepository.GetList().Where(o => o.OrderId == orderId && o.TransactionStatus == TransactionStatus.Success);
        }
        public IQueryable<TransactionActivity> GetAllsucceedTransactionByOrderItemId(long orderItemId)
        {
            return _transactionActivityRepository.GetList().Where(o => o.OrderItemId == orderItemId && o.TransactionStatus == TransactionStatus.Success);
        }
        public decimal GetPaidAmountForOrder(long orderId)
        {
            var paymentList = _transactionActivityRepository.GetList().Where(o => o.OrderId == orderId && o.TransactionStatus == TransactionStatus.Success && o.TransactionType == TransactionType.Payment);
            decimal payment = paymentList != null && paymentList.Count() > 0 ? paymentList.Sum(p => p.Amount) : 0;
            var refundList = _transactionActivityRepository.GetList().Where(o => o.OrderId == orderId && o.TransactionStatus == TransactionStatus.Success && o.TransactionType == TransactionType.Refund);
            decimal refund = refundList != null && refundList.Count() > 0 ? refundList.Sum(p => p.Amount) : 0;
            return payment - refund;
        }
        public decimal GetPaidAmountForOrderItem(long orderItemId)
        {
            var paymentList = _transactionActivityRepository.GetList().Where(o => o.OrderItemId == orderItemId && o.TransactionStatus == TransactionStatus.Success && o.TransactionType == TransactionType.Payment);
            decimal payment = paymentList != null && paymentList.Count() > 0 ? paymentList.Sum(p => p.Amount) : 0;
            var refundList = _transactionActivityRepository.GetList().Where(o => o.OrderItemId == orderItemId && o.TransactionStatus == TransactionStatus.Success && o.TransactionType == TransactionType.Refund);
            decimal refund = refundList != null && refundList.Count() > 0 ? refundList.Sum(p => p.Amount) : 0;
            return payment - refund;
        }

        public decimal GetPaidAmountForInstallment(long InstallmentId)
        {
            var paymentList = _transactionActivityRepository.GetList().Where(o => o.InstallmentId == InstallmentId && o.TransactionStatus == TransactionStatus.Success && o.TransactionType == TransactionType.Payment);
            decimal payment = paymentList != null && paymentList.Count() > 0 ? paymentList.Sum(p => p.Amount) : 0;
            var refundList = _transactionActivityRepository.GetList().Where(o => o.InstallmentId == InstallmentId && o.TransactionStatus == TransactionStatus.Success && o.TransactionType == TransactionType.Refund);
            decimal refund = refundList != null && refundList.Count() > 0 ? refundList.Sum(p => p.Amount) : 0;
            return payment - refund;
        }
        public void SetOrderTransaction(Order order, TransactionCategory transactionCategory, TransactionType transactionType, PaymentDetail paymentDetail, TransactionStatus transactionStatus, DateTime? date = null)
        {
            if (date == null)
            {
                date = DateTime.UtcNow;
            }

            if (order != null && order.RegularOrderItems.Any())
            {

                foreach (var orderItem in order.RegularOrderItems)
                {

                    decimal transactionAmount = orderItem.TotalAmount;

                    if (orderItem.Installments.Any())
                    {
                        transactionAmount = orderItem.Installments.FirstOrDefault().Amount;
                    }


                    if (orderItem.TransactionActivities == null)
                    {
                        orderItem.TransactionActivities = new List<TransactionActivity>();
                    }
                    orderItem.TransactionActivities.Add(new TransactionActivity()
                    {
                        Amount = transactionAmount,
                        TransactionCategory = transactionCategory,
                        ClubId = order.ClubId,
                        Order = order,
                        OrderItem = orderItem,
                        Installment = (orderItem.Installments != null && orderItem.Installments.Any()) ? orderItem.Installments.FirstOrDefault() : null,
                        PaymentDetail = paymentDetail,
                        TransactionDate = date.Value,
                        TransactionStatus = transactionStatus,
                        TransactionType = transactionType,
                        SeasonId = orderItem.SeasonId,
                        MetaData = new MetaData
                        {
                            DateCreated = DateTime.UtcNow,
                            DateUpdated = DateTime.UtcNow
                        }
                    });

                }
            }
        }

        public void SetOrderItemTransaction(OrderItem orderItem, TransactionCategory transactionCategory, TransactionType transactionType, PaymentDetail paymentDetail, TransactionStatus transactionStatus, decimal transactionAmount, DateTime? date = null)
        {

            if (orderItem != null)
            {

                if (orderItem.TransactionActivities == null)
                {
                    orderItem.TransactionActivities = new List<TransactionActivity>();
                }
                orderItem.TransactionActivities.Add(new TransactionActivity()
                {
                    Amount = transactionAmount,
                    TransactionCategory = transactionCategory,
                    ClubId = orderItem.Order.ClubId,
                    Order = orderItem.Order,
                    OrderItem = orderItem,
                    Installment = (orderItem.Installments != null && orderItem.Installments.Any()) ? orderItem.Installments.FirstOrDefault() : null,
                    PaymentDetail = paymentDetail,
                    TransactionDate = date.Value,
                    TransactionStatus = transactionStatus,
                    TransactionType = transactionType,
                    SeasonId = orderItem.SeasonId,
                    MetaData = new MetaData
                    {
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow
                    }
                });


            }
        }

        public void SetInstallmentTransaction(OrderInstallment orderInstallment, TransactionCategory transactionCategory, TransactionType transactionType, PaymentDetail paymentDetail, TransactionStatus transactionStatus, decimal transactionAmount, DateTime? date = null)
        {
            if (orderInstallment != null)
            {
                if (orderInstallment.TransactionActivities == null)
                {
                    orderInstallment.TransactionActivities = new List<TransactionActivity>();
                }
                orderInstallment.TransactionActivities.Add(new TransactionActivity()
                {
                    Amount = transactionAmount,
                    TransactionCategory = transactionCategory,
                    ClubId = orderInstallment.OrderItem.Order.ClubId,
                    OrderId = orderInstallment.OrderItem.Order_Id,
                    OrderItemId = orderInstallment.OrderItemId,
                    InstallmentId = orderInstallment.Id,
                    PaymentDetail = paymentDetail,
                    TransactionDate = date.Value,
                    TransactionStatus = transactionStatus,
                    TransactionType = transactionType,
                    SeasonId = orderInstallment.OrderItem.SeasonId,
                    MetaData = new MetaData
                    {
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow
                    }
                });


            }
        }

        public IQueryable<TransactionActivity> GetInvoicedByToken(string token)
        {
            return _transactionActivityRepository.GetList().Where(t => t.Token == token && t.TransactionStatus == TransactionStatus.Invoice);
        }
        public IQueryable<TransactionActivity> GetDraftByToken(string token)
        {
            return _transactionActivityRepository.GetList().Where(t => t.Token == token && t.TransactionStatus == TransactionStatus.Draft);
        }
        public IQueryable<TransactionActivity> GetTransactionsByToken(string token)
        {
            return _transactionActivityRepository.GetList().Where(t => t.Token == token);
        }

        public bool IsPayKeyExist(string payKey)
        {
            return _transactionActivityRepository.GetList().Any(t => t.PaymentDetail.PayKey == payKey);
        }

        public bool DeleteTransaction(long transactionId)
        {
            var result = false;
            var transaction = _dataContext.Set<TransactionActivity>().SingleOrDefault(t => t.Id == transactionId);
            if (transaction != null)
            {
                transaction.TransactionStatus = TransactionStatus.Deleted;
                transaction.MetaData.DateUpdated = DateTime.UtcNow;
                result = _dataContext.SaveChanges() > 0;
            }
            return result;
        }


        public PaymentDetail GetPaymentDetail(string transactionId)
        {
            return _dataContext.Set<PaymentDetail>().SingleOrDefault(t => t.TransactionId == transactionId);
        }
        public TransactionActivity GetTransactions(long paymentDetailId)
        {
            return _transactionActivityRepository.GetList().FirstOrDefault(t => t.PaymentDetailId == paymentDetailId);
        }

        public OperationStatus UpdateTransactions(List<TransactionActivity> transactions, PaymentDetail paymentDetail)
        {

            foreach (var item in transactions)
            {
                item.PaymentDetail.Currency = paymentDetail.Currency;
                item.PaymentDetail.PayerEmail = paymentDetail.PayerEmail;
                item.PaymentDetail.PaymentMethod = paymentDetail.PaymentMethod;
                item.PaymentDetail.PaypalIPN = paymentDetail.PaypalIPN;
                item.PaymentDetail.Status = paymentDetail.Status;
                item.PaymentDetail.PayKey = paymentDetail.PayKey;
                item.PaymentDetail.TransactionId = paymentDetail.TransactionId;
                item.PaymentDetail.TransactionMessage = paymentDetail.TransactionMessage;
                item.PaymentDetail.CreditCardId = paymentDetail.CreditCardId;
                item.TransactionStatus = TransactionStatus.Pending;
            }

            var result = _dataContext.SaveChanges() > 0;

            return new OperationStatus { Status = result };
        }

        public OperationStatus Update(TransactionActivity transactionActivity)
        {
            return _transactionActivityRepository.Update(transactionActivity);
        }

        public IQueryable<TransactionActivity> GetList()
        {
            return _transactionActivityRepository.GetList();
        }

        public TransactionActivity GetTransactionsForInstallmentReport(OrderInstallment installment)
        {
            return installment.TransactionActivities.Any() ? installment.TransactionActivities.LastOrDefault(t => t.TransactionType == TransactionType.Payment
           && (t.TransactionStatus == TransactionStatus.Success || t.TransactionStatus == TransactionStatus.Failure)) : null;
        }

        public decimal CalculateSubscriptionChargeDiscount(List<TransactionActivity> transactions, bool isDiscount, bool isEntireOrder)
        {
            decimal result = 0;

            decimal finalPartAmount = 0;
            decimal amount = 0;
            foreach (var transaction in transactions.Where(t => t.TransactionType == TransactionType.Payment))
            {
                if (transaction.OrderItem.Mode == OrderItemMode.Noraml && transaction.OrderItem.PaymentPlanType == PaymentPlanType.Installment)
                {
                    var countOrderItemInstallment = transaction.OrderItem.Installments.Where(i => !i.IsDeleted && i.Type == InstallmentType.Noraml).Count();

                    var orderItemChargeDiscount = transaction.OrderItem.GetOrderChargeDiscounts().Where(c => c.Category != ChargeDiscountCategory.EntryFee);
                    var HasAppLicationFee = orderItemChargeDiscount.Any(c => c.Category == ChargeDiscountCategory.ApplicationFee);

                    decimal orginalAmount = 0;

                    if (transaction.InstallmentId.HasValue && transaction.Installment.Type == InstallmentType.Noraml && transaction.OrderItem.OrderItemScheduleParts.FirstOrDefault(a => !a.IsDeleted && a.ProgramPartId == transaction.Installment.ProgramSchedulePartId) == null)
                    {
                        break;
                    }
                    else
                    {
                        if (transaction.InstallmentId.HasValue && transaction.Installment.Type == InstallmentType.Noraml)
                        {
                            orginalAmount = transaction.OrderItem.OrderItemScheduleParts.FirstOrDefault(a => !a.IsDeleted && a.ProgramPartId == transaction.Installment.ProgramSchedulePartId).PartAmount;
                        }
                        else if (transaction.OrderItem.OrderChargeDiscounts.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ApplicationFee) != null)
                        {
                            orginalAmount = transaction.OrderItem.OrderChargeDiscounts.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ApplicationFee).Amount;
                        }
                        else
                        {
                            break;
                        }
                    }

                    finalPartAmount = orginalAmount;
                    var installmentType = transaction.Installment != null ? transaction.Installment.Type : InstallmentType.Noraml;

                    foreach (var discount in orderItemChargeDiscount.Where(c => c.Subcategory == ChargeDiscountSubcategory.Discount))
                    {
                        if (discount.CouponId.HasValue)
                        {
                            if (discount.Coupon.CouponCalculateType == CalculationType.TotalAmount)
                            {
                                if (discount.Coupon.AmounType == ChargeDiscountType.Percent)
                                {
                                    var percentAmount = (discount.Coupon.Amount * orginalAmount) / 100;

                                    finalPartAmount -= percentAmount;
                                    result += percentAmount;
                                }
                                else
                                {
                                    var fixAmount = (discount.Coupon.Amount);
                                    finalPartAmount -= fixAmount;
                                    result += fixAmount;
                                }
                            }
                            else if (installmentType == InstallmentType.Noraml)
                            {
                                if (discount.Coupon.AmounType == ChargeDiscountType.Percent)
                                {
                                    var percentAmount = (discount.Coupon.Amount * orginalAmount) / 100;

                                    finalPartAmount -= percentAmount;
                                    result += percentAmount;
                                }
                                else
                                {
                                    var fixAmount = (discount.Coupon.Amount);
                                    finalPartAmount -= fixAmount;
                                    result += fixAmount;
                                }
                            }
                        }
                        else if (discount.DiscountId.HasValue && installmentType == InstallmentType.Noraml)
                        {
                            if (discount.Discount.AmountType == ChargeDiscountType.Percent)
                            {
                                var percentAmount = (discount.Discount.Amount * orginalAmount) / 100;

                                finalPartAmount -= percentAmount;

                                result += percentAmount;
                            }
                            else
                            {
                                var fixAmount = (discount.Discount.Amount);
                                finalPartAmount -= fixAmount;
                                result += fixAmount;
                            }
                        }
                        else if (installmentType == InstallmentType.Noraml)
                        {
                            var instAmount = countOrderItemInstallment > 0 ? Math.Abs(discount.Amount) / countOrderItemInstallment : Math.Abs(discount.Amount);
                            finalPartAmount -= instAmount;
                            result += instAmount;
                        }
                    }

                    if (!isDiscount)
                    {

                        result = isEntireOrder ? amount : 0;
                        var charges = orderItemChargeDiscount.Where(c => c.Category != ChargeDiscountCategory.EntryFee && c.Subcategory == ChargeDiscountSubcategory.Charge);

                        if (installmentType == InstallmentType.Noraml)
                        {
                            countOrderItemInstallment = HasAppLicationFee ? countOrderItemInstallment + 1 : countOrderItemInstallment;
                            foreach (var charge in charges)
                            {
                                decimal surChargeAmount = 0;
                                if (charge.Category == ChargeDiscountCategory.Surcharge)
                                {
                                    if (transaction.OrderItem.Order.Club.Charges != null && transaction.OrderItem.Order.Club.Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge))
                                    {
                                        var clubSurcharge = transaction.OrderItem.Order.Club.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge);

                                        if (clubSurcharge.AmountType == ChargeDiscountType.Fixed)
                                        {
                                            surChargeAmount = countOrderItemInstallment > 0 ? charge.Amount / countOrderItemInstallment : charge.Amount;
                                            result += surChargeAmount;
                                        }
                                        else
                                        {
                                            var percentSurCharge = clubSurcharge.Amount;
                                            surChargeAmount = finalPartAmount > 0 ? (finalPartAmount * percentSurCharge) / 100 : 0;
                                            result += surChargeAmount;
                                        }
                                    }
                                    else //if club remove surCharge from setting
                                    {
                                        surChargeAmount = countOrderItemInstallment > 0 ? charge.Amount / countOrderItemInstallment : charge.Amount; ;
                                        result += surChargeAmount;
                                    }
                                }
                                else if (charge.Category == ChargeDiscountCategory.PartnerSurcharge)
                                {
                                    if (transaction.OrderItem.Order.Club.PartnerClub != null && transaction.OrderItem.Order.Club.PartnerClub.Charges != null && transaction.OrderItem.Order.Club.PartnerClub.Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.PartnerSurcharge))
                                    {
                                        var partnerSurcharge = transaction.OrderItem.Order.Club.PartnerClub.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.PartnerSurcharge);
                                        if (partnerSurcharge.AmountType == ChargeDiscountType.Fixed)
                                        {
                                            surChargeAmount = countOrderItemInstallment > 0 ? charge.Amount / countOrderItemInstallment : charge.Amount; ;
                                            result += surChargeAmount;
                                        }
                                        else
                                        {
                                            var percentSurCharge = partnerSurcharge.Amount;
                                            surChargeAmount = finalPartAmount > 0 ? (finalPartAmount * percentSurCharge) / 100 : 0;
                                        }
                                    }
                                    else //if partner remove surCharge from setting
                                    {
                                        surChargeAmount = countOrderItemInstallment > 0 ? charge.Amount / countOrderItemInstallment : charge.Amount; ;
                                        result += surChargeAmount;
                                    }
                                }
                                else if (charge.Category == ChargeDiscountCategory.CustomCharge)
                                {
                                    var chargeAmount = countOrderItemInstallment > 0 ? charge.Amount / countOrderItemInstallment : charge.Amount; ;

                                    result += chargeAmount;
                                }
                            }
                        }
                        else
                        {
                            var appCharges = charges.Where(c => c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.Surcharge || c.Category == ChargeDiscountCategory.PartnerSurcharge);
                            foreach (var charge in appCharges)
                            {
                                decimal surChargeAmount = 0;
                                if (charge.Category == ChargeDiscountCategory.Surcharge)
                                {
                                    if (transaction.OrderItem.Order.Club.Charges != null && transaction.OrderItem.Order.Club.Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge))
                                    {
                                        var clubSurcharge = transaction.OrderItem.Order.Club.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge);

                                        if (clubSurcharge.AmountType == ChargeDiscountType.Fixed)
                                        {
                                            surChargeAmount = countOrderItemInstallment > 0 ? charge.Amount / (countOrderItemInstallment + 1) : charge.Amount; ;
                                            result += surChargeAmount;
                                            amount += surChargeAmount;
                                        }
                                        else
                                        {
                                            var percentSurCharge = clubSurcharge.Amount;
                                            surChargeAmount = finalPartAmount > 0 ? (finalPartAmount * percentSurCharge) / 100 : 0;
                                            result += surChargeAmount;
                                            amount += surChargeAmount;
                                        }
                                    }
                                    else //if club remove surCharge from setting
                                    {
                                        surChargeAmount = countOrderItemInstallment > 0 ? charge.Amount / (countOrderItemInstallment + 1) : charge.Amount;
                                        result += surChargeAmount;
                                        amount += surChargeAmount;
                                    }
                                }
                                else if (charge.Category == ChargeDiscountCategory.PartnerSurcharge)
                                {
                                    if (transaction.OrderItem.Order.Club.PartnerClub != null && transaction.OrderItem.Order.Club.PartnerClub.Charges != null && transaction.OrderItem.Order.Club.PartnerClub.Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.PartnerSurcharge))
                                    {
                                        var partnerSurcharge = transaction.OrderItem.Order.Club.PartnerClub.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.PartnerSurcharge);
                                        if (partnerSurcharge.AmountType == ChargeDiscountType.Fixed)
                                        {
                                            surChargeAmount = countOrderItemInstallment > 0 ? charge.Amount / (countOrderItemInstallment + 1) : charge.Amount;
                                            result += surChargeAmount;
                                            amount += surChargeAmount;
                                        }
                                        else
                                        {
                                            var percentSurCharge = partnerSurcharge.Amount;
                                            surChargeAmount = finalPartAmount > 0 ? (finalPartAmount * percentSurCharge) / 100 : 0;
                                            amount += surChargeAmount;
                                        }
                                    }
                                    else //if partner remove surCharge from setting
                                    {
                                        surChargeAmount = countOrderItemInstallment > 0 ? charge.Amount / (countOrderItemInstallment + 1) : charge.Amount;
                                        result += surChargeAmount;
                                        amount += surChargeAmount;
                                    }
                                }
                                else
                                {
                                    amount += charge.Amount;
                                    result += amount;
                                }
                            }
                        }
                    }
                }
                else
                {
                    var orderChargeDiscount = transaction.OrderItem.GetOrderChargeDiscounts().Where(c => c.Category != ChargeDiscountCategory.EntryFee);
                    if (isDiscount)
                    {
                        foreach (var discount in orderChargeDiscount.Where(c => c.Subcategory == ChargeDiscountSubcategory.Discount))
                        {
                            result += discount.Amount;
                        }
                    }
                    else
                    {
                        foreach (var charge in orderChargeDiscount.Where(c => c.Subcategory == ChargeDiscountSubcategory.Charge))
                        {
                            result += charge.Amount;
                        }
                    }
                }

            }

            return Math.Floor(Math.Abs(result) * 100) / 100;
        }

        public decimal GetTransactionPrice(List<TransactionActivity> transactions)
        {
            decimal result = 0;

            foreach (var transaction in transactions)
            {
                var orderItemScheduleParts = transaction.OrderItem.OrderItemScheduleParts.Where(p => !p.IsDeleted);
                decimal amount = 0;
                if (transaction.TransactionType == TransactionType.Payment)
                {
                    if (transaction.InstallmentId.HasValue)
                    {
                        if (transaction.Installment.Type == InstallmentType.Noraml && transaction.OrderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                        {
                            if (transaction.OrderItem.OrderItemScheduleParts.FirstOrDefault(a => !a.IsDeleted && a.ProgramPartId == transaction.Installment.ProgramSchedulePartId) == null)
                            {
                                break;
                            }
                            else
                            {
                                amount = transaction.OrderItem.OrderItemScheduleParts.FirstOrDefault(a => !a.IsDeleted && a.ProgramPartId == transaction.Installment.ProgramSchedulePartId).PartAmount;
                                result += amount;
                            }
                        }
                        else
                        {
                            amount = transaction.Amount;
                            result += amount;
                        }
                    }
                    else
                    {
                        amount = transaction.OrderItem.ProgramScheduleId.HasValue && transaction.OrderItem.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee) ?
                            transaction.OrderItem.OrderChargeDiscounts.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).Amount : transaction.Amount;
                        result += amount;
                    }
                }
                else
                {
                    amount = transaction.Amount;
                    result += amount;
                }
            }

            return Math.Floor(result * 100) / 100;
        }

        public string GetOrderChargeDiscountName(List<TransactionActivity> transactions, bool isDiscount, bool isEntireOrder)
        {
            var result = string.Empty;
            var names = new List<string>();

            var hasApplicationFee = transactions.Where(t => t.InstallmentId.HasValue && t.Installment.Type == InstallmentType.AddOn).ToList();

            foreach (var transaction in transactions)
            {
                if (transaction.OrderItem.Mode == OrderItemMode.Noraml && transaction.OrderItem.PaymentPlanType == PaymentPlanType.Installment)
                {
                    if (transaction.OrderItem.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare)
                    {
                        break;
                    }
                    if (isDiscount)
                    {
                        var discounts = transaction.OrderItem.GetOrderChargeDiscounts().Where(c => c.Category != ChargeDiscountCategory.EntryFee && c.Subcategory == ChargeDiscountSubcategory.Discount);
                        foreach (var item in discounts)
                        {
                            if (transaction.InstallmentId.HasValue && transaction.Installment.Type == InstallmentType.Noraml)
                            {
                                names.Add(item.Name);
                            }
                            else if (item.CouponId.HasValue && item.Coupon.CouponCalculateType == CalculationType.TotalAmount)
                            {
                                names.Add(item.Name);
                            }
                        }
                    }
                    else
                    {
                        var charges = transaction.OrderItem.GetOrderChargeDiscounts().Where(c => c.Category != ChargeDiscountCategory.EntryFee && c.Subcategory == ChargeDiscountSubcategory.Charge);
                        if (transaction.InstallmentId.HasValue && transaction.Installment.Type == InstallmentType.Noraml)
                        {
                            foreach (var item in charges)
                            {
                                if (isEntireOrder && item.Category == ChargeDiscountCategory.ApplicationFee && hasApplicationFee.Count > 0)
                                {
                                    names.Add(item.Name);
                                }
                                else if (item.Category != ChargeDiscountCategory.ApplicationFee)
                                {
                                    names.Add(item.Name);
                                }
                            }
                        }
                        else
                        {
                            var appCharges = charges.Where(c => c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.Surcharge || c.Category == ChargeDiscountCategory.PartnerSurcharge);

                            foreach (var item in appCharges)
                            {
                                if (item.Category == ChargeDiscountCategory.ApplicationFee && hasApplicationFee.Count > 0)
                                {
                                    names.Add(item.Name);
                                }
                            }
                        }
                    }
                }
                else
                {
                    var orderChargeDiscount = transaction.OrderItem.GetOrderChargeDiscounts().Where(c => c.Category != ChargeDiscountCategory.EntryFee);
                    if (isDiscount)
                    {
                        foreach (var discount in orderChargeDiscount.Where(c => c.Subcategory == ChargeDiscountSubcategory.Discount))
                        {
                            names.Add(discount.Name);
                        }
                    }
                    else
                    {
                        foreach (var charge in orderChargeDiscount.Where(c => c.Subcategory == ChargeDiscountSubcategory.Charge))
                        {
                            names.Add(charge.Name);
                        }
                    }
                }
            }

            result = string.Join(", ", names.ToList().Distinct());

            return result;
        }

        public string GetProgramScheduleTitle(List<TransactionActivity> transactions)
        {
            var result = string.Empty;

            var scheduleNames = new List<string>();

            foreach (var transaction in transactions)
            {
                if (transaction.OrderItem.ProgramScheduleId.HasValue)
                {
                    if (transaction.OrderItem.ProgramSchedule.Title != null)
                    {
                        scheduleNames.Add(transaction.OrderItem.ProgramSchedule.Title);
                    }
                    else
                    {
                        scheduleNames.Add("-");
                    }
                }
                else
                {
                    scheduleNames.Add("-");
                }
            }

            return string.Join(", ", scheduleNames.ToList().Distinct());
        }

        public string GetProgramType(List<TransactionActivity> transactions)
        {
            var programTypes = new List<string>();

            foreach (var transaction in transactions)
            {
                var programTypeCategory = transaction.OrderItem.ProgramScheduleId.HasValue ? transaction.OrderItem.ProgramSchedule.Program.TypeCategory.ToDescription().Replace("Program", string.Empty).Replace("care", "Care") : string.Empty;

                switch (transaction.OrderItem.Mode)
                {
                    case OrderItemMode.PunchCard:
                        programTypes.Add($"{programTypeCategory} - {OrderItemMode.PunchCard.ToDescription()}");
                        break;

                    case OrderItemMode.DropIn:
                        programTypes.Add($"{programTypeCategory} - {OrderItemMode.DropIn.ToDescription()}");
                        break;

                    default:
                        programTypes.Add($"{programTypeCategory}");
                        break;
                }
            }

            return string.Join(", ", programTypes.ToList().Distinct());
        }

        public string GetStartDateOrEndDateProgram(List<TransactionActivity> transactions, bool isStartDate)
        {
            var result = string.Empty;

            var dates = new List<string>();

            foreach (var transaction in transactions)
            {
                if (transaction.OrderItem.ProgramScheduleId.HasValue)
                {
                    var date = isStartDate ? transaction.OrderItem.ProgramSchedule.StartDate.ToString(Constants.DateTime_Comma) : transaction.OrderItem.ProgramSchedule.EndDate.ToString(Constants.DateTime_Comma);

                    if (!string.IsNullOrWhiteSpace(date))
                    {
                        dates.Add(date);
                    }
                }
            }

            var finalDates = dates.ToList().Distinct();

            result = finalDates.ToList().Count() == 1 ? finalDates.First() : "Various";

            return result;
        }

        public IEnumerable<TransactionActivity> GetPaymentTransactions(OrderInstallment installment)
        {
            return installment.TransactionActivities.Where(t => t.PaymentDetail != null &&
                         !string.IsNullOrEmpty(t.PaymentDetail.PayKey)
                         && t.TransactionType == TransactionType.Payment
                         && (t.TransactionStatus == TransactionStatus.Success || t.TransactionStatus == TransactionStatus.Pending));
        }

        public IEnumerable<TransactionActivity> GetRefundAutomaticallyTransactions(OrderInstallment installment)
        {
            return installment.TransactionActivities.Where(t => t.PaymentDetail != null
                && !string.IsNullOrEmpty(t.PaymentDetail.PayKey)
                && t.TransactionType == TransactionType.Refund
                && (t.TransactionStatus == TransactionStatus.Success || t.TransactionStatus == TransactionStatus.Pending));
        }

        public IEnumerable<TransactionActivity> GetPaymentTransactions(OrderItem orderItem)
        {
            return orderItem.TransactionActivities.Where(t => t.PaymentDetail != null &&
                         !string.IsNullOrEmpty(t.PaymentDetail.PayKey)
                         && t.TransactionType == TransactionType.Payment
                         && (t.TransactionStatus == TransactionStatus.Success || t.TransactionStatus == TransactionStatus.Pending));
        }

        public IEnumerable<TransactionActivity> GetRefundAutomaticallyTransactions(OrderItem orderItem)
        {
            return orderItem.TransactionActivities.Where(t => t.PaymentDetail != null
                && !string.IsNullOrEmpty(t.PaymentDetail.PayKey)
                && t.TransactionType == TransactionType.Refund
                && (t.TransactionStatus == TransactionStatus.Success || t.TransactionStatus == TransactionStatus.Pending));
        }

        public bool EnableUndoRefundToOrder(ClubUser clubUser, TransactionActivity transactionActivity)
        {
            var orderItem = transactionActivity.OrderItem;

            if (orderItem.Order.IsLive && clubUser.Credit == 0) return false;

            if (!orderItem.Order.IsLive && clubUser.TestCredit == 0) return false;

            var totalUndoAmount = orderItem.TransactionActivities.Where(s => s.TransactionCategory == TransactionCategory.FamilyCreditTransfer && s.PaymentDetail.RelatedTransactionId == transactionActivity.PaymentDetail.TransactionId).Sum(t => t.Amount);

            var enableUndo = transactionActivity.Amount > totalUndoAmount;
            var isRefund = transactionActivity.TransactionType == TransactionType.Refund;
            var isRefundToFamily = transactionActivity.PaymentDetail.PaymentMethod == PaymentMethod.Credit;
            var isAdminPayerType = transactionActivity.PaymentDetail.PayerType == RoleCategoryType.Dashboard;

            if (isRefund && isRefundToFamily && isAdminPayerType && enableUndo) return true;

            return false;
        }

        public string GeneratGuidTransactionId()
        {
            var guidTransactionId = Payment.SignJumbulaToTransactionId;
            guidTransactionId += Guid.NewGuid().ToString("n");

            if (GetList().Where(t => t.HandleMode == HandleMode.Offline).Select(t => t.PaymentDetail).Any(o => o.TransactionId.Equals(guidTransactionId)))
            {
                guidTransactionId = GeneratGuidTransactionId();
            }

            return guidTransactionId;
        }
    }
}
