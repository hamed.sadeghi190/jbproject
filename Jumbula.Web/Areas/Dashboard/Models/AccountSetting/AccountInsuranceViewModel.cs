﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Mvc.UIHelpers;

namespace SportsClub.Areas.Dashboard.Models
{
    public class AccountInsuranceViewModel
    {
        public AccountInsuranceViewModel()
        {
            Certificates = new List<InsuranceCertificateViewModel>();
          
        }
        public int NumberOfCertificates
        {
            get
            {
                if(Certificates!=null && Certificates.Any())
                {
                    return Certificates.Count;
                }
                return 0;
            }
        }
        public List<SelectKeyValue<int>> CertificateTypes { get
            {
                return DropdownHelpers.ToSelectListWithIntValue<CertificateOfInsuranceTypes>().OrderBy(n=>n.Text).ToList();
            }
        }
        public List<InsuranceCertificateViewModel> Certificates { get; set; }
        public DateTime? PolicyExpirationDate { get; set; }

        
    }
   
    public class InsuranceCertificateViewModel
    {
        public InsuranceCertificateViewModel()
        {
          
            SelectedCertificate = 0;
        }
        public List<string> CertificateFileURLs { get; set; }



        public List<string> CertificateFileNames { get; set; }

     
        public int NumberOfCertificateFiles
        {
            get
            {
                if (CertificateFileNames != null && CertificateFileNames.Any())
                {
                    return CertificateFileNames.Count;
                }
                return 0;
            }
        }
       
        public int SelectedCertificate { get; set; }
        public List<SelectKeyValue<int>> CertificateTypes
        {
            get
            {
                return DropdownHelpers.ToSelectListWithIntValue<CertificateOfInsuranceTypes>();
            }
        }

    }
   
}