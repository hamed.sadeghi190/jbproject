﻿using System;
using System.Collections.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class schoolProgramsCheckInOutReportViewModel
    {
        public string ClassName { get; set; }
        public long ClassId { get; set; }
        public string SchoolName { get; set; }
        public string SeasonName { get; set; }
        public string ClassDays { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public DateTime DateProgram { get; set; }
        public List<string> DatesOfDay { get; set; }
        public List<DateTime> Dates { get; set; }
        public string StrTime
        {
            get
            {
                return string.Format("{0}-{1}", StartTime, EndTime);
            }
        }
        public string Instructor { get; set; }
        public string Location { get; set; }
        public List<ProgramParticipantListViewModel> ProgramParticipants { get; set; }

    }

}