﻿using Jb.Framework.Common;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IStripeWebhookBusiness
    {
        OperationStatus Create(StripeWebhook stripeWebhook);
      
    }
}
