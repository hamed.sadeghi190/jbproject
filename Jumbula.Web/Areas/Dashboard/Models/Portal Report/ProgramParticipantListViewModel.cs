﻿using System;
using System.Collections.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ProgramParticipantListViewModel
    {
        public string ParticipantName { get; set; }
        public string TeacherName { get; set; }
        public string Phone { get; set; }
        public string TransportHome { get; set; }
        public string Grade { get; set; }  
        public int Number { get; set; }
    }
}