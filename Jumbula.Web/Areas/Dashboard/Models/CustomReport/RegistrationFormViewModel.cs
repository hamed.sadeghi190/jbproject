﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;


namespace SportsClub.Areas.Dashboard.Models.CustomReport
{
    public class RegistrationFormViewModel
    {
        public string ProgramName { get; set; }

        public CurrencyCodes Currency { get; set; }
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string ParticipantName { get; set; }

        public int Age { get; set; }

        public PostalAddress PostalAddress { get; set; }

        public decimal PaidAmount { get; set; }

        public DateTime RegistrationDate { get; set; }

        public List<Charges> Charges { get; set; }

        public JbForm RegForm { get; set; }

        public List<OrderItemWaiver> Waivers { get; set; }

        public List<OrderItemFollowupForm> Followups { get; set; }

        public string RegConfirmation { get; set; }
    }
}