﻿using Jb.Framework.Common.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class ParticipantViewModel
    {
    
        public int Id { get; set; }
        public int Age { get; set; }
        public int PlayerId { get; set; }
        public PlayerInfoViewModel Info { get; set; }

        [Display(Name = "First name")]
        [Required(ErrorMessage = "{0} is required.")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete first name.")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        [Required(ErrorMessage = "{0} is required.")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete last name.")]
        public string LastName { get; set; }
        public PageStep PageStep { get; set; }

        [Display(Name = "Date of birth")]
        public string DOB { get; set; }
        public string FullName { get; set; }

        [EmailAddress(ErrorMessage = "Email is not a valid email address.")]
        [Display(Name = "Email")]
        public string Email { get; set; }
        public int UserId { get; set; }
        public int ContactId { get; set; }

        [Display(Name = "Gender")]
        public GenderCategories Gender { get; set; }
      

        public PageMode PageMode { get; set; }
        public RelationshipType PlayerType { get; set; }

      
    }

  
}