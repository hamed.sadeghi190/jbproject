
IF OBJECT_ID('[dbo].[OrderCharges]') IS NULL
BEGIN
	CREATE TABLE [dbo].[OrderCharges](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[Amount] [decimal](18, 2) NOT NULL,
		[Description] [nvarchar](256) NULL,
		[OrderId] [int] NULL,
		[OrderItemId] [int] NULL,
		[ChargeDiscountId] [int] NULL,
		[Category] [int] NOT NULL default(0),
		[Subcategory] [int] NOT NULL default(0))
END
GO

---<<ADD PRIMARY KEY>>--
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'PK_dbo.OrderCharges')
BEGIN
	ALTER TABLE [dbo].[OrderCharges] ADD CONSTRAINT [PK_dbo.OrderCharges] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	) 
END
GO

IF NOT EXISTS(select 1 from sys.objects where name='FK_dbo.OrderCharges_dbo.OrderItems_OrderItemId')
BEGIN
	ALTER TABLE [dbo].[OrderCharges]  WITH CHECK ADD  CONSTRAINT [FK_dbo.OrderCharges_dbo.OrderItems_OrderItemId] FOREIGN KEY([OrderItemId])
	REFERENCES [dbo].[OrderItems] ([Id])

	ALTER TABLE [dbo].[OrderCharges] CHECK CONSTRAINT [FK_dbo.OrderCharges_dbo.OrderItems_OrderItemId]
END
GO

IF NOT EXISTS(select 1 from sys.objects where name='FK_dbo.OrderCharges_dbo.Orders_OrderId')
BEGIN
	ALTER TABLE [dbo].[OrderCharges]  WITH CHECK ADD  CONSTRAINT [FK_dbo.OrderCharges_dbo.Orders_OrderId] FOREIGN KEY([OrderId])
	REFERENCES [dbo].[Orders] ([Id])

	ALTER TABLE [dbo].[OrderCharges] CHECK CONSTRAINT [FK_dbo.OrderCharges_dbo.Orders_OrderId]
END
GO


--// Remove Realations
IF EXISTS(select 1 from sys.objects where name='FK_dbo.ChargeDiscounts_dbo.OrderItems_OrderItem_Id1')
BEGIN
	Alter Table [dbo].[ChargeDiscounts]
	Drop Constraint [FK_dbo.ChargeDiscounts_dbo.OrderItems_OrderItem_Id1]
END

IF EXISTS(select 1 from sys.objects where name='FK_dbo.ChargeDiscounts_dbo.OrderItems_OrderItem_Id')
BEGIN
	Alter Table [dbo].[ChargeDiscounts]
	Drop Constraint [FK_dbo.ChargeDiscounts_dbo.OrderItems_OrderItem_Id]
END

IF EXISTS(select 1 from sys.objects where name='FK_dbo.ChargeDiscounts_dbo.Orders_Order_Id1')
BEGIN
	Alter Table [dbo].[ChargeDiscounts]
	Drop Constraint [FK_dbo.ChargeDiscounts_dbo.Orders_Order_Id1]
END

IF EXISTS(select 1 from sys.objects where name='FK_dbo.ChargeDiscounts_dbo.Orders_Order_Id')
BEGIN
	Alter Table [dbo].[ChargeDiscounts]
	Drop Constraint [FK_dbo.ChargeDiscounts_dbo.Orders_Order_Id]
END


--<<order history >>--
IF OBJECT_ID('[dbo].[OrderHistories]') IS NULL
BEGIN
CREATE TABLE [dbo].[OrderHistories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
	[ActionDate] [datetime] NOT NULL,
	[Action] [tinyint] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[UserId] [int] NOT NULL default(0))
END
GO

---<<ADD PRIMARY KEY>>--
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'PK_dbo.OrderHistories')
BEGIN
	ALTER TABLE [dbo].[OrderHistories] ADD CONSTRAINT [PK_dbo.OrderHistories] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	) 
END
GO


IF NOT EXISTS(select 1 from sys.objects where name='FK_dbo.OrderHistories_dbo.Orders_OrderId')
BEGIN
	ALTER TABLE [dbo].[OrderHistories]  WITH CHECK ADD  CONSTRAINT [FK_dbo.OrderHistories_dbo.Orders_OrderId] FOREIGN KEY([OrderId])
	REFERENCES [dbo].[Orders] ([Id])
	ON DELETE CASCADE

	ALTER TABLE [dbo].[OrderHistories] CHECK CONSTRAINT [FK_dbo.OrderHistories_dbo.Orders_OrderId]
END
GO


