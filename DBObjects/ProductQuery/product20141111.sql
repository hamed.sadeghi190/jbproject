-----Create Table------
IF OBJECT_ID('[dbo].[EntityForms]') IS NULL
BEGIN
	CREATE TABLE [dbo].[EntityForms](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[JbFormId] [int] NOT NULL,
		[EventId] [int] NULL)
END
------<< ADD PRIMARY KEY >>----
IF NOT EXISTS(SELECT 1 FROM sys.objects WHERE NAME='PK_EntityForms')
BEGIN
	ALTER TABLE [dbo].[EntityForms] ADD CONSTRAINT [PK_EntityForms] PRIMARY KEY (Id DESC)
END
GO


IF NOT EXISTS (SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.JbForms') AND name='CreatedDate')
BEGIN
	ALTER TABLE [dbo].[JbForms]
	ADD CreatedDate datetime NULL default(getdate());
	EXEC('update [dbo].[JbForms] set CreatedDate=getdate()')
	ALTER TABLE [dbo].[JbForms]
	Alter Column CreatedDate datetime NOT NULL;
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.JbForms') AND name='LastModifiedDate')
BEGIN
	ALTER TABLE [dbo].[JbForms]
	ADD LastModifiedDate datetime NULL;
	EXEC('update [dbo].[JbForms] set LastModifiedDate=getdate()')
	ALTER TABLE [dbo].[JbForms]
	Alter Column LastModifiedDate datetime NOT NULL;
END
GO

IF NOT EXISTS(select 1 from sys.columns where OBJECT_ID=OBJECT_ID('[dbo].[Events]') AND name='Instructor')
BEGIN	
	ALTER TABLE [dbo].[Events]
	ADD [Instructor] nvarchar(max) null
END


-----<< ADD COLUMNS >>----
IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=OBJECT_ID('[dbo].[OrderInstallments]') AND NAME='NoticeSent')
BEGIN
	ALTER TABLE [dbo].[OrderInstallments] 
	ADD [NoticeSent] [bit] Null DEFAULT(0);
	
	DECLARE @InstallmentDate nvarchar(20)='2014-10-31';
	DECLARE @cmd nvarchar(100)='update [dbo].[OrderInstallments] set [NoticeSent]=1 where paymentstatus=3 or InstallmentDate<='+@InstallmentDate;
	EXEC(@cmd);

	SET @cmd='update [dbo].[OrderInstallments] set [NoticeSent]=0 where NoticeSent is null';
	EXEC(@cmd);
	
	ALTER TABLE [dbo].[OrderInstallments] 
	ALTER COLUMN [NoticeSent] [bit] NOT Null;
END


--<< ALTER COLUMN >>--
IF EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=Object_ID('[dbo].[Orders]') AND NAME='PlayerId')
BEGIN
	ALTER TABLE [dbo].[Orders] 
	ALTER COLUMN [PlayerId] [int] NULL
END
Go